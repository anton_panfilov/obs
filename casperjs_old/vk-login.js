function parse_str(str, array) {
    var strArr = String(str)
            .replace(/^&/, '')
            .replace(/&$/, '')
            .split('&'),
        sal = strArr.length,
        i, j, ct, p, lastObj, obj, lastIter, undef, chr, tmp, key, value,
        postLeftBracketPos, keys, keysLen,
        fixStr = function(str) {
            return decodeURIComponent(str.replace(/\+/g, '%20'));
        };

    if (!array) {
        array = this.window;
    }

    for (i = 0; i < sal; i++) {
        tmp = strArr[i].split('=');
        key = fixStr(tmp[0]);
        value = (tmp.length < 2) ? '' : fixStr(tmp[1]);

        while (key.charAt(0) === ' ') {
            key = key.slice(1);
        }
        if (key.indexOf('\x00') > -1) {
            key = key.slice(0, key.indexOf('\x00'));
        }
        if (key && key.charAt(0) !== '[') {
            keys = [];
            postLeftBracketPos = 0;
            for (j = 0; j < key.length; j++) {
                if (key.charAt(j) === '[' && !postLeftBracketPos) {
                    postLeftBracketPos = j + 1;
                } else if (key.charAt(j) === ']') {
                    if (postLeftBracketPos) {
                        if (!keys.length) {
                            keys.push(key.slice(0, postLeftBracketPos - 1));
                        }
                        keys.push(key.substr(postLeftBracketPos, j - postLeftBracketPos));
                        postLeftBracketPos = 0;
                        if (key.charAt(j + 1) !== '[') {
                            break;
                        }
                    }
                }
            }
            if (!keys.length) {
                keys = [key];
            }
            for (j = 0; j < keys[0].length; j++) {
                chr = keys[0].charAt(j);
                if (chr === ' ' || chr === '.' || chr === '[') {
                    keys[0] = keys[0].substr(0, j) + '_' + keys[0].substr(j + 1);
                }
                if (chr === '[') {
                    break;
                }
            }

            obj = array;
            for (j = 0, keysLen = keys.length; j < keysLen; j++) {
                key = keys[j].replace(/^['"]/, '')
                    .replace(/['"]$/, '');
                lastIter = j !== keys.length - 1;
                lastObj = obj;
                if ((key !== '' && key !== ' ') || j === 0) {
                    if (obj[key] === undef) {
                        obj[key] = {};
                    }
                    obj = obj[key];
                } else {
                    // To insert new dimension
                    ct = -1;
                    for (p in obj) {
                        if (obj.hasOwnProperty(p)) {
                            if (+p > ct && p.match(/^\d+$/g)) {
                                ct = +p;
                            }
                        }
                    }
                    key = ct + 1;
                }
            }
            lastObj[key] = value;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////

var casper = require('casper').create({
    pageSettings: {
        loadImages:  true,
        loadPlugins: true,
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
    }
});

var vk_app_id   = '0';
var vk_scope    = 'notify,friends,photos,groups,notifications,status,offline,wall';
var vk_v        = '5.34';
var login       = '';
var pass        = '';

if(casper.cli.raw.get('app_id') && casper.cli.raw.get('app_id').length){
    vk_app_id = casper.cli.raw.get('app_id');
}

if(casper.cli.raw.get('scope') && casper.cli.raw.get('scope').length){
    vk_scope = casper.cli.raw.get('scope');
}

if(casper.cli.raw.get('v') && casper.cli.raw.get('v').length){
    vk_v = casper.cli.raw.get('v');
}

if(casper.cli.raw.get('login') && casper.cli.raw.get('login').length){
    login = casper.cli.raw.get('login');
}

if(casper.cli.raw.get('pass') && casper.cli.raw.get('pass').length){
    pass = casper.cli.raw.get('pass');
}

var link = 'https://oauth.vk.com/authorize?redirect_uri=https://oauth.vk.com/blank.html&response_type=token&display=page&' +
    'client_id=' + decodeURIComponent(vk_app_id)  + '&' +
    'scope='     + decodeURIComponent(vk_scope)   + '&' +
    'v='         + decodeURIComponent(vk_v);

var link2 = '';
var link3 = '';

casper.start(link, function() {
    this.fill('form#login_submit', {
        email: login,
        pass:  pass
    }, true);
});

casper.then(function() {
    link2 = this.getCurrentUrl();
    this.click('#install_allow', true);
});

casper.then(function() {
    link3 = this.getCurrentUrl();

    var hash = this.getCurrentUrl().split('#')[1];
    var res = new Object();
    parse_str(hash, res);

    var access_token = res.access_token;
    var user_id = res.user_id;

    if(access_token == undefined){
        access_token = '';
    }

    if(user_id == undefined){
        user_id = '';
    }

    this.echo('{"link":"' + link + '","link2":"' + link2 + '","link3":"' + link3 + '","access_token":"' + access_token + '","user_id":"' + user_id + '"}');
});

casper.run();