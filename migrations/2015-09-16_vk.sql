CREATE TABLE vk_photos (
  `id` INT NOT NULL AUTO_INCREMENT,
  `create` TIMESTAMP NULL,
  `type` INT(10) NOT NULL,
  `item` INT(10) NULL,
  `vk_owner` BIGINT(20) NOT NULL,
  `vk_group` BIGINT(20) NOT NULL,
  `vk_photo` BIGINT(20) NOT NULL,
  `caption` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `type_and_item` (`type` ASC, `item` ASC));

CREATE TABLE `obs`.`vk_groups` (
  `id` INT NOT NULL,
  `label` VARCHAR(255) NULL,
  `vk_group` BIGINT(20) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `cron_processes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pid` bigint(20) DEFAULT NULL,
  `filename` char(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filename` (`filename`)
) ENGINE=MEMORY AUTO_INCREMENT=68546 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `cron_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_finish` tinyint(1) NOT NULL DEFAULT '0',
  `start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `filename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `runtime` float DEFAULT NULL,
  `memory_mb` float DEFAULT NULL,
  `memory_peak_mb` float DEFAULT NULL,
  `use_db` float DEFAULT NULL,
  `use_cpu` float DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `last_error` text COLLATE utf8_bin,
  `db_profiler` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `filename` (`filename`),
  KEY `is_finish` (`is_finish`)
) ENGINE=InnoDB AUTO_INCREMENT=329474 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE vk_photos
CHANGE COLUMN `vk_group` `vk_album` BIGINT(20) NOT NULL ;

ALTER TABLE `obs`.`vk_photos`
ADD INDEX `vk` (`vk_photo` ASC, `vk_owner` ASC),
ADD INDEX `album` (`vk_album` ASC);

ALTER TABLE `obs`.`vk_photos`
DROP COLUMN `type`,
ADD COLUMN `photo` INT(10) NULL AFTER `create`,
ADD COLUMN `item_type` INT(10) NULL AFTER `item`,
DROP INDEX `type_and_item` ,
ADD INDEX `type_and_item` (`item_type` ASC, `item` ASC),
ADD INDEX `photo` (`photo` ASC);

ALTER TABLE `obs`.`vk_photos`
CHANGE COLUMN `caption` `caption` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL DEFAULT NULL ;

SELECT * FROM obs.vk_photos;ALTER TABLE `obs`.`vk_api_logs`
ADD COLUMN `url` VARCHAR(255) NULL AFTER `response`,
ADD COLUMN `runtime` INT(10) NULL AFTER `url`;

ALTER TABLE `obs`.`vk_api_logs`
CHANGE COLUMN `runtime` `runtime` FLOAT NULL DEFAULT NULL ;
