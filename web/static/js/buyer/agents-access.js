document.agentAccess = function(buyer, agent, share){
    jQuery('#access_' + buyer + '_' + agent)
        .attr('disabled', 'disabled')
        .css('background', '#efe1b3');

    jQuery.getJSON(
        '/account/buyers/set-agent-access',
        {
            'buyer': buyer,
            'agent': agent,
            'share': share
        },
        function(data){
            var el = jQuery('#access_' + buyer + '_' + agent)
                .val(data.share)
                .removeAttr('disabled');

            if(data.reason.length){
                alert('error: ' + data.reason);
                el.css('background', '#c26565');
            }
            else {
                el.css('background', '');
            }

        }
    )
}