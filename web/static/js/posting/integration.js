document.updateSources = function(){
    if(jQuery('#integration_class').val().length){
        jQuery('#integration_file_source').html('<br><center><h1>Loading...</h1><br></center>');
    }
    else {
        jQuery('#integration_file_source').html('');
    }
    console.log(1);
    jQuery.getJSON(
        '/account/buyers/postings/integration-sources',
        {
            'product': document.posting_product,
            'class': jQuery('#integration_class').val()
        },
        function(data){
            console.log(2);
            if(data.class == jQuery('#integration_class').val()){
                if(data.class.length){
                    jQuery('#integration_file_source').html(data.sources).show();
                }
                else {
                    jQuery('#integration_file_source').html('').hide();
                }
            }
        }
    );
}

jQuery(function(){
    jQuery('#integration_class').change(function(){
        document.updateSources();
    });
});