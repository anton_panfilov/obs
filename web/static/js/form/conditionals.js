document.ap_form_conditionals = {
    conditionals: {},

    run: function(name){
        /*
         * 1. проверить если ли такой кондишан
         * 2. если есть, проверить скрытое это поле или нет
         * 3. если скрытое, то все зависимости надо скрыть
         */
        if(this.conditionals[name] != undefined){
            this.conditionals[name].run();
        }
    },

    addConditional: function(name, conditional){
        this.conditionals[name] = conditional;
    },

    in_array: function(value, array){
        for(var i in array){
            if(array[i] == value){
                return true;
            }
        }
        return false;
    },

    runArray: function(settings, value, isCurrentElementShow){
        var all = [];

        for(var i in settings){
            for(var j in settings[i][1]){
                if(!this.in_array(settings[i][1][j], all)){
                    all.push(settings[i][1][j])
                }
            }
        }

        if(isCurrentElementShow){
            // show and hide
            var show = [];

            for(i in settings){
                if(this.in_array(value, settings[i][0])){
                    for(var j in settings[i][1]){
                        if(!this.in_array(settings[i][1][j], show)){
                            show.push(settings[i][1][j]);
                        }
                    }
                }
            }

            for(var i in show){
                this.show(show[i]);
            }

            for(var i in all){
                if(!this.in_array(all[i], show)){
                    this.hide(all[i]);
                }
            }
        }
        else {
            // hide all
            for(var i in all){
                this.hide(all[i]);
            }
        }
    },

    runNotArray: function(settings, value, isCurrentElementShow){
        var all = [];

        for(var i in settings){
            for(var j in settings[i][1]){
                if(!this.in_array(settings[i][1][j], all)){
                    all.push(settings[i][1][j])
                }
            }
        }

        if(isCurrentElementShow){
            // show and hide
            var show = [];

            for(i in settings){
                if(!this.in_array(value, settings[i][0])){
                    for(var j in settings[i][1]){
                        if(!this.in_array(settings[i][1][j], show)){
                            show.push(settings[i][1][j]);
                        }
                    }
                }
            }

            for(var i in show){
                this.show(show[i]);
            }

            for(var i in all){
                if(!this.in_array(all[i], show)){
                    this.hide(all[i]);
                }
            }
        }
        else {
            // hide all
            for(var i in all){
                this.hide(all[i]);
            }
        }
    },

    hide: function(name){
        jQuery('#' + name).attr('disabled', 'disabled');
        jQuery('#ap_form_tr_' + name).hide();

        if(this.conditionals[name] != undefined){
            this.conditionals[name].show = false;
        }

        this.run(name);
    },

    show: function(name){
        jQuery('#' + name).removeAttr('disabled');
        jQuery('#ap_form_tr_' + name).show();

        if(this.conditionals[name] != undefined){
            this.conditionals[name].show = true;
        }

        this.run(name);
    }
}