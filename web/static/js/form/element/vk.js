document.formVkAddText = function(name, txtToAdd){
    var caretPos = document.getElementById(name).selectionStart;
    var textAreaTxt = jQuery("#" + name).val();

    jQuery("#" + name)
        .val(
            textAreaTxt.substring(0, caretPos) +
            txtToAdd +
            textAreaTxt.substring(caretPos)
        )
        .focus()
        .selectRange(caretPos + txtToAdd.length);
}