document.ap_form_element_autocomplite = {
    msec: 300,
    data: {},
    current: null,
    searchString: null,

    setDataTxt: function(name, label){
        if(this.data[name] == undefined){
            this.data[name] = new Object();
        }
        this.data[name].txt = label;
    },

    setParams: function(name, params){
        if(this.data[name] == undefined){
            this.data[name] = new Object();
        }
        this.data[name].params = params;
    },

    run: function(name, string){
        if(
            this.current == name &&
            this.searchString == string &&
            this.data[name].timestamp <= Date.now() - this.msec &&
            this.data[name].load != true
        ){
            this.data[name].load = true;

            this.message(name, "Loading. Please Wait...", 'icon-refresh');

            var currentString = string;
            var currentName = name;

            jQuery.getJSON(
                document.location,
                {
                    ap_form_element_autocomplite_name: name,
                    s: string,
                    p: this.data[name].params // TODO: добававить параметры
                },
                function(data){
                    document.ap_form_element_autocomplite.data[currentName].load = false;

                    // jQuery('#' + currentName + '_dbg').html(document.ap_form_element_autocomplite.searchString + " " + currentString);

                    if(document.ap_form_element_autocomplite.current == currentName){
                        if(document.ap_form_element_autocomplite.searchString != currentString){
                            document.ap_form_element_autocomplite.search(
                                document.ap_form_element_autocomplite.current
                            );
                        }
                        else {
                            var isFound = false;
                            jQuery('#' + name + '_DivContent').html('');

                            // Если текущее значение выбранно из списка, то оно по запросу может не найтись, при этом его надо добавить
                            var cnV = jQuery('#' + currentName).val();

                            if(
                                cnV != '' &&
                                document.ap_form_element_autocomplite.data[currentName].txt &&
                                data[cnV] == undefined
                            ){
                                data[cnV] = document.ap_form_element_autocomplite.data[currentName].txt;
                            }

                            for (key in data) {
                                isFound = true;
                                jQuery('#' + name + '_DivContent').append("<a onclick=\"document.ap_form_element_autocomplite.select("+
                                    "'" + key + "', '" + data[key] + "'" +
                                ")\">" +
                                    data[key].replace(new RegExp(currentString, 'i'), function (m) { return "<i>" + m + "</i>"; }) +
                                "</a>");
                            }

                            if(!isFound){
                                document.ap_form_element_autocomplite.message(currentName, "Not Found");
                            }
                            else {
                                jQuery('#' + name + '_DivContent').append(
                                    "<a onclick=\"document.ap_form_element_autocomplite.select('', '')\">"+
                                        "<strong>Clear the Field</strong>"+
                                    "</a>"
                                );

                                document.ap_form_element_autocomplite.message(currentName);
                            }
                        }
                    }
                }
            )
        }
    },

    select: function(key, value){
        var name = document.ap_form_element_autocomplite.current;

        jQuery("#" + name).val(key);
        jQuery("#" + name + "_txt").val(value);

        this.setDataTxt(name, value);
        this.exitNow(name);
    },

    message: function(name, message, imgClass){
        if(message || imgClass){
            jQuery('#' + name + '_info').show();
            jQuery('#' + name + '_message').html(message);

            /*
            if(imgClass){
                jQuery('#' + name + '_img').setClass(imgClass).show();
            }
            else {
                jQuery('#' + name + '_img').hide();
            }
            */
        }
        else {
            jQuery('#' + name + '_info').hide();
        }
    },

    search: function(name){
        jQuery("#" + name + "_mainDiv").show();
        jQuery('#' + name + '_DivContent').html('');

        if(this.data[name] == undefined){
            this.data[name] = new Object();
        }

        this.data[name].timestamp = Date.now();

        this.current = name;
        this.searchString = jQuery('#' + name + '_txt').val();

        // если строка, которая была установленна автоматически отличается от строки введенной ручками, удалить бекграундное значение
        if(this.data[name].txt != this.searchString){
            this.data[name].txt = '';
            jQuery("#" + name).val('');
        }

        if(this.data[name].load != true){
            this.message(name, "Input...", 'icon-pencil');
            setTimeout(
                function(){
                    document.ap_form_element_autocomplite.run(
                        document.ap_form_element_autocomplite.current,
                        document.ap_form_element_autocomplite.searchString
                    );
                },
                document.ap_form_element_autocomplite.msec
            );

        }
    },

    exit: function(name){
        var currentName = name;
        setTimeout(function(){
            document.ap_form_element_autocomplite.exitNow(currentName);
        }, 200);
    },

    exitNow: function(name){
        jQuery("#" + name + "_mainDiv").hide();
    }
}