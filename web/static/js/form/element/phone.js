document.ap_form_element_phone = {
    codes: {
        "ad": ["Andorra", "376"],
        "ae": ["United Arab Emirates", "971"],
        "af": ["Afghanistan", "93"],
        "ag": ["Antigua And Barbuda", "1"],
        "ai": ["Anguilla", "1"],
        "al": ["Albania", "355"],
        "am": ["Armenia", "374"],
        "an": ["Netherlands Antilles", "599"],
        "ao": ["Angola", "244"],
        "aq": ["Antarctica", "672"],
        "ar": ["Argentina", "54"],
        "as": ["American Samoa", "684"],
        "at": ["Austria", "43"],
        "au": ["Australia", "61"],
        "aw": ["Aruba", "297"],
        "ax": ["Aland Islands", "358"],
        "az": ["Azerbaijan", "994"],
        "ba": ["Bosnia And Herzegovina", "387"],
        "bb": ["Barbados", "1"],
        "bd": ["Bangladesh", "880"],
        "be": ["Belgium", "32"],
        "bf": ["Burkina Faso", "226"],
        "bg": ["Bulgaria", "359"],
        "bh": ["Bahrain", "973"],
        "bi": ["Burundi", "257"],
        "bj": ["Benin", "229"],
        "bm": ["Bermuda", "1"],
        "bn": ["Brunei Darussalam", "673"],
        "bo": ["Bolivia", "591"],
        "br": ["Brazil", "55"],
        "bs": ["Bahamas", "1"],
        "bt": ["Bhutan", "975"],
        "bv": ["Bouvet Island", "0"],
        "bw": ["Botswana", "267"],
        "by": ["Belarus", "375"],
        "bz": ["Belize", "501"],
        "ca": ["Canada", "1"],
        "cd": ["The Democratic Republic Of The Congo", "243"],
        "cf": ["Central African Republic", "236"],
        "cg": ["Congo", "242"],
        "ch": ["Switzerland", "41"],
        "ci": ["Cote D'Ivoire", "225"],
        "ck": ["Cook Islands", "682"],
        "cl": ["Chile", "56"],
        "cm": ["Cameroon", "237"],
        "cn": ["China", "86"],
        "co": ["Colombia", "57"],
        "cr": ["Costa Rica", "506"],
        "cs": ["Serbia And Montenegro", "381"],
        "cu": ["Cuba", "53"],
        "cv": ["Cape Verde", "238"],
        "cy": ["Cyprus", "357"],
        "cz": ["Czech Republic", "420"],
        "de": ["Germany", "49"],
        "dj": ["Djibouti", "253"],
        "dk": ["Denmark", "45"],
        "dm": ["Dominica", "1"],
        "do": ["Dominican Republic", "1"],
        "dz": ["Algeria", "213"],
        "ec": ["Ecuador", "593"],
        "ee": ["Estonia", "372"],
        "eg": ["Egypt", "20"],
        "er": ["Eritrea", "291"],
        "es": ["Spain", "34"],
        "et": ["Ethiopia", "251"],
        "fi": ["Finland", "358"],
        "fj": ["Fiji", "679"],
        "fk": ["Falkland Islands (Malvinas)", "500"],
        "fm": ["Federated States Of Micronesia", "691"],
        "fo": ["Faroe Islands", "298"],
        "fr": ["France", "33"],
        "ga": ["Gabon", "241"],
        "gb": ["United Kingdom", "44"],
        "gd": ["Grenada", "1"],
        "ge": ["Georgia", "995"],
        "gf": ["French Guiana", "594"],
        "gh": ["Ghana", "233"],
        "gi": ["Gibraltar", "350"],
        "gl": ["Greenland", "299"],
        "gm": ["Gambia", "220"],
        "gn": ["Guinea", "224"],
        "gp": ["Guadeloupe", "590"],
        "gq": ["Equatorial Guinea", "240"],
        "gr": ["Greece", "30"],
        "gs": ["South Georgia And The South Sandwich Islands", "995"],
        "gt": ["Guatemala", "502"],
        "gu": ["Guam", "1"],
        "gw": ["Guinea-Bissau", "245"],
        "gy": ["Guyana", "592"],
        "hk": ["Hong Kong", "852"],
        "hn": ["Honduras", "504"],
        "hr": ["Croatia", "385"],
        "ht": ["Haiti", "509"],
        "hu": ["Hungary", "36"],
        "id": ["Indonesia", "62"],
        "ie": ["Ireland", "353"],
        "il": ["Israel", "972"],
        "im": ["Isle of Man", "44"],
        "in": ["India", "91"],
        "io": ["British Indian Ocean Territory", "246"],
        "iq": ["Iraq", "964"],
        "ir": ["Islamic Republic Of Iran", "98"],
        "is": ["Iceland", "354"],
        "it": ["Italy", "39"],
        "je": ["Jersey", "44"],
        "jm": ["Jamaica", "1"],
        "jo": ["Jordan", "962"],
        "jp": ["Japan", "81"],
        "ke": ["Kenya", "254"],
        "kg": ["Kyrgyzstan", "996"],
        "kh": ["Cambodia", "855"],
        "ki": ["Kiribati", "686"],
        "km": ["Comoros", "269"],
        "kn": ["Saint Kitts And Nevis", "1"],
        "kr": ["Republic Of Korea", "82"],
        "kw": ["Kuwait", "965"],
        "ky": ["Cayman Islands", "1"],
        "kz": ["Kazakhstan", "7"],
        "la": ["Lao People'S Democratic Republic", "856"],
        "lb": ["Lebanon", "961"],
        "lc": ["Saint Lucia", "1"],
        "li": ["Liechtenstein", "423"],
        "lk": ["Sri Lanka", "94"],
        "lr": ["Liberia", "231"],
        "ls": ["Lesotho", "266"],
        "lt": ["Lithuania", "370"],
        "lu": ["Luxembourg", "352"],
        "lv": ["Latvia", "371"],
        "ly": ["Libyan Arab Jamahiriya", "218"],
        "ma": ["Morocco", "212"],
        "mc": ["Monaco", "377"],
        "md": ["Republic Of Moldova", "373"],
        "me": ["Montenegro", "381"],
        "mg": ["Madagascar", "261"],
        "mh": ["Marshall Islands", "692"],
        "mk": ["The Former Yugoslav Republic Of Macedonia", "389"],
        "ml": ["Mali", "223"],
        "mm": ["Myanmar", "95"],
        "mn": ["Mongolia", "976"],
        "mo": ["Macao", "853"],
        "mp": ["Northern Mariana Islands", "1"],
        "mq": ["Martinique", "596"],
        "mr": ["Mauritania", "222"],
        "ms": ["Montserrat", "1"],
        "mt": ["Malta", "356"],
        "mu": ["Mauritius", "230"],
        "mv": ["Maldives", "960"],
        "mw": ["Malawi", "265"],
        "mx": ["Mexico", "52"],
        "my": ["Malaysia", "60"],
        "mz": ["Mozambique", "258"],
        "na": ["Namibia", "264"],
        "nc": ["New Caledonia", "687"],
        "ne": ["Niger", "227"],
        "nf": ["Norfolk Island", "672"],
        "ng": ["Nigeria", "234"],
        "ni": ["Nicaragua", "505"],
        "nl": ["Netherlands", "31"],
        "no": ["Norway", "47"],
        "np": ["Nepal", "977"],
        "nr": ["Nauru", "674"],
        "nu": ["Niue", "0"],
        "nz": ["New Zealand", "64"],
        "om": ["Oman", "968"],
        "pa": ["Panama", "507"],
        "pe": ["Peru", "51"],
        "pf": ["French Polynesia", "689"],
        "pg": ["Papua New Guinea", "675"],
        "ph": ["Philippines", "63"],
        "pk": ["Pakistan", "92"],
        "pl": ["Poland", "48"],
        "pr": ["Puerto Rico", "1"],
        "ps": ["Palestinian Territory, Occupied", "970"],
        "pt": ["Portugal", "351"],
        "pw": ["Palau", "680"],
        "py": ["Paraguay", "595"],
        "qa": ["Qatar", "974"],
        "re": ["Reunion", "262"],
        "ro": ["Romania", "40"],
        "rs": ["Serbia", "381"],
        "ru": ["Russian Federation", "7"],
        "rw": ["Rwanda", "250"],
        "sa": ["Saudi Arabia", "966"],
        "sb": ["Solomon Islands", "677"],
        "sc": ["Seychelles", "248"],
        "sd": ["Sudan", "249"],
        "se": ["Sweden", "46"],
        "sg": ["Singapore", "65"],
        "si": ["Slovenia", "386"],
        "sk": ["Slovakia", "421"],
        "sl": ["Sierra Leone", "232"],
        "sm": ["San Marino", "378"],
        "sn": ["Senegal", "221"],
        "so": ["Somalia", "252"],
        "sr": ["Suriname", "597"],
        "st": ["Sao Tome And Principe", "239"],
        "sv": ["El Salvador", "503"],
        "sy": ["Syrian Arab Republic", "963"],
        "sz": ["Swaziland", "268"],
        "tc": ["Turks And Caicos Islands", "1"],
        "td": ["Chad", "235"],
        "tf": ["French Southern Territories", "596"],
        "tg": ["Togo", "228"],
        "th": ["Thailand", "66"],
        "tj": ["Tajikistan", "992"],
        "tk": ["Tokelau", "690"],
        "tl": ["Timor-Leste", "670"],
        "tm": ["Turkmenistan", "993"],
        "tn": ["Tunisia", "216"],
        "to": ["Tonga", "676"],
        "tr": ["Turkey", "90"],
        "tt": ["Trinidad And Tobago", "1"],
        "tv": ["Tuvalu", "688"],
        "tw": ["Taiwan", "886"],
        "tz": ["United Republic Of Tanzania", "255"],
        "ua": ["Ukraine", "380"],
        "ug": ["Uganda", "256"],
        "um": ["United States Minor Outlying Islands", "1"],
        "us": ["United States", "1"],
        "uy": ["Uruguay", "598"],
        "uz": ["Uzbekistan", "998"],
        "va": ["Holy See (Vatican City State)", "39"],
        "vc": ["Saint Vincent And The Grenadines", "1"],
        "ve": ["Venezuela", "58"],
        "vg": ["Virgin Islands, British", "1"],
        "vi": ["Virgin Islands, U.S.", "340"],
        "vn": ["Viet Nam", "84"],
        "vu": ["Vanuatu", "678"],
        "wf": ["Wallis And Futuna", "681"],
        "ws": ["Samoa", "684"],
        "ye": ["Yemen", "967"],
        "yt": ["Mayotte", "269"],
        "za": ["South Africa", "27"],
        "zm": ["Zambia", "260"],
        "zw": ["Zimbabwe", "263"]
    },

    Code2Country : function(phone){
        var country = "";

        switch (phone.substr(1, 4)){
            case '7840':
            case '7940':
                country = 'ab'; // Abkhazia
                break;
            case '1684':
                country = 'as'; // American Samoa
                break;
            case '1264':
                country = 'ai'; // Anguilla
                break;
            case '1268':
                country = 'ag'; // Antigua and Barbuda
                break;
            case '1242':
                country = 'bs'; // Bahamas
                break;
            case '1246':
                country = 'bb'; // Barbados
                break;
            case '1441':
                country = 'bm'; // Bermuda
                break;
            case '1284':
                country = 'vg'; // British Virgin Islands
                break;
            case '1204':
            case '1250':
            case '1306':
            case '1403':
            case '1416':
            case '1418':
            case '1450':
            case '1506':
            case '1514':
            case '1519':
            case '1604':
            case '1613':
            case '1705':
            case '1709':
            case '1780':
            case '1807':
            case '1819':
            case '1867':
            case '1902':
            case '1905':
                country = 'ca'; // Canada
                break;
            case '1345':
                country = 'ky'; // Cayman Islands
                break;
            case '1767':
                country = 'dm'; // Dominica
                break;
            case '1809':
                country = 'do'; // Dominican Republic
                break;
            case '1473':
                country = 'gd'; // Grenada
                break;
            case '1671':
                country = 'gu'; // Guam
                break;
            case '1876':
                country = 'jm'; // Jamaica
                break;
            case '1664':
                country = 'ms'; // Montserrat
                break;
            case '1670':
                country = 'mp'; // Northern Mariana Islands
                break;
            case '1869':
                country = 'kn'; // Saint Kitts and Nevis
                break;
            case '1758':
                country = 'lc'; // Saint Lucia
                break;
            case '1599':
                country = 'mf'; // Saint Martin
                break;
            case '1784':
                country = 'vc'; // Saint Vincent and the Grenadines
                break;
            case '1868':
                country = 'tt'; // Trinidad and Tobago
                break;
            case '1649':
                country = 'tc'; // Turks and Caicos Islands
                break;
            case '1340':
                country = 'vi'; // US Virgin Islands
                break;
            default:
                switch ( phone.substr( 1, 3 ) )
                {
                    case '355':
                        country = 'al'; // Albania
                        break;
                    case '213':
                        country = 'dz'; // Algeria
                        break;
                    case '376':
                        country = 'ad'; // Andorra
                        break;
                    case '244':
                        country = 'ao'; // Angola
                        break;
                    case '672':
                        country = 'aq'; // Antarctica
                        break;
                    case '374':
                        country = 'am'; // Armenia
                        break;
                    case '297':
                        country = 'aw'; // Aruba
                        break;
                    case '994':
                        country = 'az'; // Azerbaijan
                        break;
                    case '973':
                        country = 'bh'; // Bahrain
                        break;
                    case '880':
                        country = 'bd'; // Bangladesh
                        break;
                    case '375':
                        country = 'by'; // Belarus
                        break;
                    case '501':
                        country = 'bz'; // Belize
                        break;
                    case '229':
                        country = 'bj'; // Benin
                        break;
                    case '975':
                        country = 'bt'; // Bhutan
                        break;
                    case '591':
                        country = 'bo'; // Bolivia
                        break;
                    case '387':
                        country = 'ba'; // Bosnia and Herzegovina
                        break;
                    case '267':
                        country = 'bw'; // Botswana
                        break;
                    case '673':
                        country = 'bn'; // Brunei
                        break;
                    case '359':
                        country = 'bg'; // Bulgaria
                        break;
                    case '226':
                        country = 'bf'; // Burkina Faso
                        break;
                    case '257':
                        country = 'bi'; // Burundi
                        break;
                    case '855':
                        country = 'kh'; // Cambodia
                        break;
                    case '237':
                        country = 'cm'; // Cameroon
                        break;
                    case '238':
                        country = 'cv'; // Cape Verde
                        break;
                    case '236':
                        country = 'cf'; // Central African Republic
                        break;
                    case '235':
                        country = 'td'; // Chad
                        break;
                    case '269':
                        country = 'km'; // Comoros
                        break;
                    case '682':
                        country = 'ck'; // Cook Islands
                        break;
                    case '506':
                        country = 'cr'; // Costa Rica
                        break;
                    case '385':
                        country = 'hr'; // Croatia
                        break;
                    case '357':
                        country = 'cy'; // Cyprus
                        break;
                    case '420':
                        country = 'cz'; // Czech Republic
                        break;
                    case '243':
                        country = 'cd'; // Democratic Republic of the Congo
                        break;
                    case '253':
                        country = 'dj'; // Djibouti
                        break;
                    case '670':
                        country = 'tl'; // East Timor
                        break;
                    case '593':
                        country = 'ec'; // Ecuador
                        break;
                    case '503':
                        country = 'sv'; // El Salvador
                        break;
                    case '240':
                        country = 'gq'; // Equatorial Guinea
                        break;
                    case '291':
                        country = 'er'; // Eritrea
                        break;
                    case '372':
                        country = 'ee'; // Estonia
                        break;
                    case '251':
                        country = 'et'; // Ethiopia
                        break;
                    case '500':
                        country = 'fk'; // Falkland Islands
                        break;
                    case '298':
                        country = 'fo'; // Faroe Islands
                        break;
                    case '679':
                        country = 'fj'; // Fiji
                        break;
                    case '358':
                        country = 'fi'; // Finland
                        break;
                    case '689':
                        country = 'pf'; // French Polynesia
                        break;
                    case '241':
                        country = 'ga'; // Gabon
                        break;
                    case '220':
                        country = 'gm'; // Gambia
                        break;
                    case '995':
                        country = 'ge'; // Georgia
                        break;
                    case '233':
                        country = 'gh'; // Ghana
                        break;
                    case '350':
                        country = 'gi'; // Gibraltar
                        break;
                    case '299':
                        country = 'gl'; // Greenland
                        break;
                    case '502':
                        country = 'gp'; // Guatemala
                        break;
                    case '224':
                        country = 'gn'; // Guinea
                        break;
                    case '245':
                        country = 'gw'; // Guinea-Bissau
                        break;
                    case '592':
                        country = 'gy'; // Guyana
                        break;
                    case '509':
                        country = 'ht'; // Haiti
                        break;
                    case '504':
                        country = 'hn'; // Honduras
                        break;
                    case '852':
                        country = 'hk'; // Hong Kong
                        break;
                    case '354':
                        country = 'is'; // Iceland
                        break;
                    case '964':
                        country = 'iq'; // Iraq
                        break;
                    case '353':
                        country = 'ie'; // Ireland
                        break;
                    case '972':
                        country = 'il'; // Israel
                        break;
                    case '225':
                        country = 'ci'; // Ivory Coast
                        break;
                    case '962':
                        country = 'jo'; // Jordan
                        break;
                    case '254':
                        country = 'ke'; // Kenya
                        break;
                    case '686':
                        country = 'ki'; // Kiribati
                        break;
                    case '965':
                        country = 'kw'; // Kuwait
                        break;
                    case '996':
                        country = 'kg'; // Kyrgyzstan
                        break;
                    case '856':
                        country = 'la'; // Laos
                        break;
                    case '371':
                        country = 'lv'; // Latvia
                        break;
                    case '961':
                        country = 'lb'; // Lebanon
                        break;
                    case '266':
                        country = 'ls'; // Lesotho
                        break;
                    case '231':
                        country = 'lr'; // Liberia
                        break;
                    case '218':
                        country = 'ly'; // Libya
                        break;
                    case '423':
                        country = 'li'; // Liechtenstein
                        break;
                    case '370':
                        country = 'lt'; // Lithuania
                        break;
                    case '352':
                        country = 'lu'; // Luxembourg
                        break;
                    case '853':
                        country = 'mo'; // Macau
                        break;
                    case '389':
                        country = 'mk'; // Macedonia
                        break;
                    case '261':
                        country = 'mg'; // Madagascar
                        break;
                    case '265':
                        country = 'mw'; // Malawi
                        break;
                    case '960':
                        country = 'mv'; // Maldives
                        break;
                    case '223':
                        country = 'ml'; // Mali
                        break;
                    case '356':
                        country = 'mt'; // Malta
                        break;
                    case '692':
                        country = 'mh'; // Marshall Islands
                        break;
                    case '222':
                        country = 'mr'; // Mauritania
                        break;
                    case '230':
                        country = 'mu'; // Mauritius
                        break;
                    case '269':
                        country = 'yt'; // Mayotte
                        break;
                    case '691':
                        country = 'fm'; // Micronesia
                        break;
                    case '373':
                        country = 'md'; // Moldova
                        break;
                    case '377':
                        country = 'mc'; // Monaco
                        break;
                    case '976':
                        country = 'mn'; // Mongolia
                        break;
                    case '382':
                        country = 'me'; // Montenegro
                        break;
                    case '212':
                        country = 'ma'; // Morocco
                        break;
                    case '258':
                        country = 'mz'; // Mozambique
                        break;
                    case '264':
                        country = 'na'; // Namibia
                        break;
                    case '674':
                        country = 'nr'; // Nauru
                        break;
                    case '977':
                        country = 'np'; // Nepal
                        break;
                    case '599':
                        country = 'an'; // Netherlands Antilles
                        break;
                    case '687':
                        country = 'nc'; // New Caledonia
                        break;
                    case '505':
                        country = 'ni'; // Nicaragua
                        break;
                    case '227':
                        country = 'ne'; // Niger
                        break;
                    case '234':
                        country = 'ng'; // Nigeria
                        break;
                    case '683':
                        country = 'nu'; // Niue
                        break;
                    case '850':
                        country = 'kp'; // North Korea
                        break;
                    case '968':
                        country = 'om'; // Oman
                        break;
                    case '680':
                        country = 'pw'; // Palau
                        break;
                    case '507':
                        country = 'pa'; // Panama
                        break;
                    case '675':
                        country = 'pg'; // Papua New Guinea
                        break;
                    case '595':
                        country = 'py'; // Paraguay
                        break;
                    case '351':
                        country = 'pt'; // Portugal
                        break;
                    case '139':
                        country = 'pr'; // Puerto Rico
                        break;
                    case '974':
                        country = 'qa'; // Qatar
                        break;
                    case '242':
                        country = 'cg'; // Republic of the Congo
                        break;
                    case '250':
                        country = 'rw'; // Rwanda
                        break;
                    case '590':
                        country = 'bl'; // Saint Barthelemy
                        break;
                    case '290':
                        country = 'sh'; // Saint Helena
                        break;
                    case '508':
                        country = 'pm'; // Saint Pierre and Miquelon
                        break;
                    case '685':
                        country = 'ws'; // Samoa
                        break;
                    case '378':
                        country = 'sm'; // San Marino
                        break;
                    case '239':
                        country = 'st'; // Sao Tome and Principe
                        break;
                    case '966':
                        country = 'sa'; // Saudi Arabia
                        break;
                    case '221':
                        country = 'sn'; // Senegal
                        break;
                    case '381':
                        country = 'rs'; // Serbia
                        break;
                    case '248':
                        country = 'sc'; // Seychelles
                        break;
                    case '232':
                        country = 'sl'; // Sierra Leone
                        break;
                    case '421':
                        country = 'sk'; // Slovakia
                        break;
                    case '386':
                        country = 'si'; // Slovenia
                        break;
                    case '677':
                        country = 'sb'; // Solomon Islands
                        break;
                    case '252':
                        country = 'so'; // Somalia
                        break;
                    case '249':
                        country = 'sd'; // Sudan
                        break;
                    case '597':
                        country = 'sr'; // Suriname
                        break;
                    case '268':
                        country = 'sz'; // Swaziland
                        break;
                    case '963':
                        country = 'sy'; // Syria
                        break;
                    case '886':
                        country = 'tw'; // Taiwan
                        break;
                    case '992':
                        country = 'tj'; // Tajikistan
                        break;
                    case '255':
                        country = 'tz'; // Tanzania
                        break;
                    case '228':
                        country = 'tg'; // Togo
                        break;
                    case '690':
                        country = 'tk'; // Tokelau
                        break;
                    case '676':
                        country = 'to'; // Tonga
                        break;
                    case '216':
                        country = 'tn'; // Tunisia
                        break;
                    case '993':
                        country = 'tm'; // Turkmenistan
                        break;
                    case '688':
                        country = 'tv'; // Tuvalu
                        break;
                    case '256':
                        country = 'ug'; // Uganda
                        break;
                    case '380':
                        country = 'ua'; // Ukraine
                        break;
                    case '971':
                        country = 'ae'; // United Arab Emirates
                        break;
                    case '598':
                        country = 'uy'; // Uruguay
                        break;
                    case '998':
                        country = 'uz'; // Uzbekistan
                        break;
                    case '678':
                        country = 'vu'; // Vanuatu
                        break;
                    case '681':
                        country = 'wf'; // Wallis and Futuna
                        break;
                    case '967':
                        country = 'ye'; // Yemen
                        break;
                    case '260':
                        country = 'zm'; // Zambia
                        break;
                    case '263':
                        country = 'zw'; // Zimbabwe
                        break;
                    default:
                        switch ( phone.substr( 1, 2 ) )
                        {
                            case '93':
                                country = 'af'; // Afghanistan
                                break;
                            case '54':
                                country = 'ar'; // Argentina
                                break;
                            case '61':
                                country = 'au'; // Australia
                                break;
                            case '43':
                                country = 'at'; // Austria
                                break;
                            case '32':
                                country = 'be'; // Belgium
                                break;
                            case '55':
                                country = 'br'; // Brazil
                                break;
                            case '95':
                                country = 'mm'; // Burma (Myanmar)
                                break;
                            case '56':
                                country = 'cl'; // Chile
                                break;
                            case '86':
                                country = 'cn'; // China
                                break;
                            case '61':
                                country = 'cx'; // Christmas Island
                                break;
                            case '61':
                                country = 'cc'; // Cocos (Keeling) Islands
                                break;
                            case '57':
                                country = 'co'; // Colombia
                                break;
                            case '53':
                                country = 'cu'; // Cuba
                                break;
                            case '45':
                                country = 'dk'; // Denmark
                                break;
                            case '20':
                                country = 'eg'; // Egypt
                                break;
                            case '33':
                                country = 'fr'; // France
                                break;
                            case '49':
                                country = 'de'; // Germany
                                break;
                            case '30':
                                country = 'gr'; // Greece
                                break;
                            case '39':
                                country = 'va'; // Holy See (Vatican City)
                                break;
                            case '36':
                                country = 'hu'; // Hungary
                                break;
                            case '91':
                                country = 'in'; // India
                                break;
                            case '62':
                                country = 'id'; // Indonesia
                                break;
                            case '98':
                                country = 'ir'; // Iran
                                break;
                            case '44':
                                country = 'im'; // Isle of Man
                                break;
                            case '39':
                                country = 'it'; // Italy
                                break;
                            case '81':
                                country = 'jp'; // Japan
                                break;
                            case '76':
                            case '77':
                                country = 'kz'; // Kazakhstan
                                break;
                            case '60':
                                country = 'my'; // Malaysia
                                break;
                            case '52':
                                country = 'mx'; // Mexico
                                break;
                            case '31':
                                country = 'nl'; // Netherlands
                                break;
                            case '64':
                                country = 'nz'; // New Zealand
                                break;
                            case '47':
                                country = 'no'; // Norway
                                break;
                            case '92':
                                country = 'pk'; // Pakistan
                                break;
                            case '51':
                                country = 'pe'; // Peru
                                break;
                            case '63':
                                country = 'ph'; // Philippines
                                break;
                            case '64':
                                country = 'pn'; // Pitcairn Islands
                                break;
                            case '48':
                                country = 'pl'; // Poland
                                break;
                            case '40':
                                country = 'ro'; // Romania
                                break;
                            case '65':
                                country = 'sg'; // Singapore
                                break;
                            case '27':
                                country = 'za'; // South Africa
                                break;
                            case '82':
                                country = 'kr'; // South Korea
                                break;
                            case '34':
                                country = 'es'; // Spain
                                break;
                            case '94':
                                country = 'lk'; // Sri Lanka
                                break;
                            case '46':
                                country = 'se'; // Sweden
                                break;
                            case '41':
                                country = 'ch'; // Switzerland
                                break;
                            case '66':
                                country = 'th'; // Thailand
                                break;
                            case '90':
                                country = 'tr'; // Turkey
                                break;
                            case '44':
                                country = 'gb'; // United Kingdom
                                break;
                            case '58':
                                country = 've'; // Venezuela
                                break;
                            case '84':
                                country = 'vn'; // Vietnam
                                break;
                            default:
                                switch ( phone.substr( 1, 1 ) )
                                {
                                    case '7':
                                        country = 'ru'; // Russia
                                        break;
                                    case '1':
                                        country = 'us'; // United States
                                        break;
                                    default:
                                        break;
                                }
                                break;
                        }
                        break;
                }
                break;
        }

        return country;
    },

    renderCountry: function(name){
        var number = jQuery('#' + name).val();
        number = number.replace(/\D/g, '');

        var country = this.Code2Country("+" + number);

        if(country.length > 0){
            jQuery('#form_phone_flag_' + name)
                .attr('class', 'flag flag-' + country)
                .css('visibility', 'visible');

            if(this.codes.hasOwnProperty(country)){
                jQuery('#form_phone_label_' + name)
                    .html(
                        this.codes[country][0] +
                        " <span style='color: #999; font-size: 80%'>(+" + this.codes[country][1] + ")</span>"
                    );
            }
            else {
                if(number.length > 3){
                    jQuery('#form_phone_label_' + name)
                        .html("<span style='color: #DD4B39'>Unknown Country</span>");
                }
                else {
                    jQuery('#form_phone_label_' + name)
                        .html('');
                }
            }
        }
        else {
            console.log(country.length);

            jQuery('#form_phone_flag_' + name)
                .css('visibility', 'hidden');

            jQuery('#form_phone_label_' + name)
                .html('');
        }
    }
}