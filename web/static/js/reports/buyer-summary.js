with(reportSummary=function(role){
    this.setRole(role);
    this.setCurrentPlot(jQuery.cookie('buyer_summary_report_chart'));
}){
    // объявляем и инициализируем свойства
    prototype.data = new Object();
    prototype.role = '';
    prototype.url = '';
    prototype.periodType = 'days';

    // массив ролей и досупных им графиков
    prototype.plotTypes = {
        '1': [
            {name:  'none',         title: 'None'},
            {name:  'epl',          title: 'IPL'},
            {name:  'convert',      title: 'Convert'},
            {name:  'value',        title: 'Value'}
        ],
        '13': [
            {name:  'none',         title: 'None'},
            {name:  'epl',          title: 'IPL'},
            {name:  'convert',      title: 'Convert'},
            {name:  'value',        title: 'Value'}
        ]
    };

    prototype.plotCurrent = '';


    // получить массив типов графиков, которые доступны этому типу пользователя
    prototype.getPlotTypes = function(){
        for(p in this.plotTypes){
            if(p == this.role){
                return this.plotTypes[p];
            }
        }
        return new Array();
    }

    // найти plot по умолчанию, для текущего типа пользователя
    prototype.setCurrentPlot = function(nameOFnull){
        var a = this.getPlotTypes();

        if(a.length){
            this.plotCurrent = a[0];

            for(var i = 0; i < a.length; i++){
                if(a[i].name == nameOFnull){
                    this.plotCurrent = a[i];
                }
            }
        }
        else {
            this.plotCurrent = '';
        }
    }



    prototype.selectChart = function(value){
        this.setCurrentPlot(value);
        this.renderPlot('');

        jQuery.cookie('buyer_summary_report_chart', value, {expires:9999, path: window.location.pathname});
    }

    // render Plot Selecter
    prototype.renderPlotSelecter = function(){
        var a = this.getPlotTypes();
        if(a.length > 1){
            jQuery('#summaryReport_Plot_Selecter').show().html('<div class="input-prepend" style="margin: 0">' +
                '<span class="add-on" style="background: #CCC; border: 0;"> <i class="icon-bar-chart"></i> </span>' +
                '<select id="summaryReport_Plot_Selecter_Select" style="height: 32px"></select>' +
            '</div>');
            var selectDom = jQuery('#summaryReport_Plot_Selecter_Select');

            // тут надо проверить подходит ли onChange для всех браузеров, или для каких то браузеров надо отлавливать подругому изменнеие option в select
            selectDom.attr({
                onChange: "document.summaryReport.selectChart(this.value);"
            });

            for(var i = 0; i < a.length; i++){
                var option = jQuery('<option></option>').appendTo(selectDom).attr({
                    value: a[i].name
                }).html(a[i].title);

                if(a[i].name == this.plotCurrent.name){
                    option.attr({
                        selected: 'selected'
                    });
                }
            }
        }
        else {
            jQuery('#summaryReport_Plot_Selecter').hide();
        }
    }

    prototype.setData = function(data){
        this.data = data;
    }

    prototype.setPeriodType = function(url){
        this.periodType = url;
    }

    prototype.getURL = function(type, dateFrom, dateTill){
        return this.url
            .split('{type}').join(type)
            .split('{dateFrom}').join(dateFrom)
            .split('{dateTill}').join(dateTill);
    }

    prototype.setRole = function(role){
        this.role = role;
    }

    prototype.setShowClicks = function(showClicks){
        this.showClicks = showClicks;
    }


    prototype.getStringOneDayReport = function(value, type, date, styleDefault, styleNull){
        return this.getStringReport_Abstract(value, type, date, date, styleDefault, styleNull);
    }

    prototype.getStringAllDaysReport = function(value, type, styleDefault, styleNull){
        return this.getStringReport_Abstract(value, type, this.data[this.data.length-1].dateYmd, this.data[0].dateYmd, styleDefault, styleNull);
    }

    prototype.getStringReport_Abstract = function(value, type, date1, date2, styleDefault, styleNull){
        if(value != 0){
            var result = "<a style='"+styleDefault+"' href='" + this.getURL(type, date1, date2) + "'>" + value + "</a>";
        }
        else {
            var result = "<span style='"+styleNull+"'>0</span>";
        }

        return result;
    }

    prototype.optionsPlot = {};
    prototype.optionsOverview = {};

    // рисование графиков
    prototype.renderPlot = function(idContaner){
        if(idContaner == ''){
            idContaner = this.plotIDContaner;
        }
        else{
            this.plotIDContaner = idContaner;
            this.renderPlotSelecter();
        }

        this.optionsPlot = {
            xaxis: {
                mode: "time",
                "lines": {"show": "true"},
                "points": {"show": "true"}
            },

            series: {
                stack: null,

                lines: {
                    show: false,
                    steps: false,
                    lineWidth: 1
                },

                bars: {
                    align: "center",
                    show: true,
                    lineWidth: 0,
                    barWidth: 3600000*22
                },

                points: {
                    show: false
                },

                shadowSize: 2
            },

            grid: {
                hoverable: true,
                clickable: true,
                backgroundColor: '#FCFCFC',
                markings: weekendAreas
            },

            legend: {
                show: true,
                position: 'nw', //"ne" or "nw" or "se" or "sw"
                backgroundOpacity: 0.4
            },

            yaxis: { min: 0},
            y2axis: { min: 0 }

        };

        this.optionsOverview = {
            series: {
                lines: { show: true, lineWidth: 1 },
                shadowSize: 0
            },
            xaxis: {
                mode: "time"
            },
            grid: {
                backgroundColor: '#FCFCFC',
                borderWidth: 1,
                borderColor: '#AAA'
            },
            legend: {
                show: false
            },
            yaxis: { ticks: [], min: 0, autoscaleMargin: 0.1 },
            y2axis: { ticks: [], min: 0 },
            selection: {
                mode: "x"
            }
        };

        if(this.periodType == 'hours'){
            this.optionsPlot.xaxis.minTickSize = [1, "hour"];
            this.optionsPlot.series.bars.barWidth = 3600000*0.9;

            this.optionsOverview.xaxis.minTickSize = [1, "day"];
        }
        else {
            this.optionsPlot.xaxis.minTickSize = [1, "day"];
            this.optionsOverview.xaxis.minTickSize = [1, "month"];
        }

        if(this.data.length >= 7 && this.plotCurrent != ''){
            if(this.plotCurrent.name == 'epl'){
                this.optionsPlot.series.lines.show = true;
                this.optionsPlot.series.points.show = true;

                // Расчет данных для графика с доходом вебмастера и конвертом
                var epl = new Array();

                var j = 0;
                for(var i = this.data.length - 1; i >= 0; i-- ){
                    epl[j] = [this.data[i].flortTime, this.data[i].epl];

                    j++;
                }

                this.dataArray = [
                    {
                        data: epl,
                        label: 'Average Earnings Per Lead',
                        text1: "",
                        text2: " RUR",
                        color: "#666"
                    }
                ];
            }
            else if(this.plotCurrent.name == 'convert'){
                this.optionsPlot.series.lines.show = true;
                this.optionsPlot.series.points.show = true;
                this.optionsPlot.series.stack = 1;
                this.optionsPlot.series.varRound = 1;

                // Расчет данных для графика админа
                var postsSold   = new Array();
                var postsReject = new Array();
                var postsError  = new Array();

                var j = 0;
                for(var i = this.data.length - 1; i >= 0; i-- ){
                    postsSold[j]  = [
                        this.data[i].flortTime,
                        this.data[i].posts_sold
                    ];

                    postsReject[j] = [
                        this.data[i].flortTime,
                        this.data[i].posts_reject
                    ];

                    postsError[j] = [
                        this.data[i].flortTime,
                        parseInt(this.data[i].posts_all) - (parseInt(this.data[i].posts_sold) + parseInt(this.data[i].posts_reject))
                    ];

                    j++;
                }

                // Массив данных
                this.dataArray = [
                    {
                        data: postsSold,
                        label: "Sales",
                        text1: "",
                        text2: " <small>leads</small>",
                        color: "#3B3"
                    },
                    {
                        data: postsReject,
                        label: "Rejects",
                        text1: "",
                        text2: " <small>leads</small>",
                        color: "#666"
                    }
                    ,
                    {
                        data: postsError,
                        label: "Errors",
                        text1: "",
                        text2: " <small>leads</small>",
                        color: "#e02222"
                    }
                ];
            }
            else if(this.plotCurrent.name == 'value'){
                this.optionsPlot.series.lines.show = true;
                this.optionsPlot.series.points.show = true;

                // Расчет данных для графика с доходом вебмастера и конвертом
                var value = new Array();

                var j = 0;
                for(var i = this.data.length - 1; i >= 0; i-- ){
                    value[j] = [this.data[i].flortTime, this.data[i].posts_money];

                    j++;
                }

                this.dataArray = [
                    {
                        data: value,
                        label: 'Value',
                        text1: "",
                        text2: " RUR",
                        color: "#3B3"
                    }
                ];
            }

            this.renderPlot_Create();
        }
        else {
            jQuery("#"+idContaner+'_pre').hide();
        }
    }

    // непосредсвенно отрисовка графиков, отделено для того что бы при изменение размера окна,
    // можно было просто перерисовать график из уже готовых массивов данных.
    prototype.renderPlot_Create = function(){
        if(this.plotCurrent.name == 'none'){
            jQuery('#summaryReport_Plot').hide();
            jQuery('#summaryReport_Plot_Overview').hide();
            return;
        }
        jQuery('#summaryReport_Plot').show();
        jQuery('#summaryReport_Plot_Overview').show();

        idContaner = this.plotIDContaner;

        if(this.data.length >= 7 && this.plotCurrent != ''){
            my = this;

            // Расчет ширины при изменение размеров окна
            jQuery("#" + this.plotIDContaner).hide();
            jQuery("#" + this.plotIDContaner + "_Overview").hide();

            var SpaceWidth = document.getElementById(this.plotIDContaner + '_pre').clientWidth;

            jQuery("#" + this.plotIDContaner).show();
            if(this.data.length > 50) jQuery("#" + this.plotIDContaner + "_Overview").show();

            jQuery("#" + this.plotIDContaner).css("width", SpaceWidth - 20);
            jQuery("#" + this.plotIDContaner + "_Overview").css("width", SpaceWidth - 100);

            if(this.data.length > 50){
                // Если выборка больше 50 дней, добавляем возможность маштабировать график
                // Расшируние опций основного графика и расчет точек масштаба по умолчаию
                var SelMax = this.data[0].flortTime;
                var SelMin = this.data[30].flortTime;

                this.optionsPlot.selection = { mode: "x" };

                this.optionsPlot.xaxis.min = SelMin;
                this.optionsPlot.xaxis.max = SelMax;
            }

            // Массив данных
            dataArray = this.dataArray;
            optionsPlot = this.optionsPlot;
            optionsOverview = this.optionsOverview;

            // Рисовать рафик
            var plot = jQuery.plot(jQuery("#" + this.plotIDContaner), dataArray, optionsPlot);

            // Всплывающие суммы, при наведении на узлы графика
            var previousPoint = null;
            jQuery("#" + idContaner).bind("plothover", function (event, pos, item) {

                jQuery("#x").text(pos.x.toFixed(2));
                jQuery("#y").text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.datapoint) {
                        previousPoint = item.datapoint;

                        jQuery("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2) - item.datapoint[2].toFixed(2);

                        var val = y;
                        var text1 = '';
                        var text2 = '';

                        if(item['series']['text1'] != undefined){
                            text1 = item['series']['text1'];
                        }

                        if(item['series']['text2'] != undefined){
                            text2 = item['series']['text2'];
                        }

                        if(item['series']['varRound'] == 1){
                            val = Math.round(val);
                        }

                        showTooltip(
                            item.pageX,
                            item.pageY,
                            item.series.label + ":<br><nobr><b>" + text1 + val + text2 + "</b></nobr>"
                        );
                    }
                }
                else {
                    jQuery("#tooltip").remove();
                    previousPoint = null;
                }

            });

            // Рисование второго графика если он нужен
            if(this.data.length > 50){
                jQuery("#" + this.plotIDContaner + "_Overview").show();

                // Рисование второго графика
                var overview = jQuery.plot(
                    jQuery("#" + this.plotIDContaner + "_Overview"),
                    dataArray,
                    optionsOverview
                );

                // Масштабирование по умолчанию
                overview.setSelection({ xaxis: { from: SelMin, to: SelMax }});


                // События для перерисовывание графика при масштабировании
                jQuery("#" + this.plotIDContaner).bind("plotselected", function (event, ranges) {
                    // do the zooming
                    plot = jQuery.plot(
                        jQuery("#" + idContaner),
                        dataArray,
                        jQuery.extend(true, {}, optionsPlot, {
                            xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to }
                        })
                    );

                    overview.setSelection(ranges, true);
                });

                jQuery("#"+this.plotIDContaner+"_Overview").bind("plotselected", function (event, ranges) {
                    plot.setSelection(ranges);
                });
            }
        }
        else {
            jQuery("#"+this.plotIDContaner+'_pre').hide();
        }
    }
}

// Рисование дива ы котором показывается дполнительная информация при наведении на график
function showTooltip(x, y, contents) {
    jQuery('<div id="tooltip">' + contents + '</div>').css( {
        position: 'absolute',
        fontSize: '10px',
        display: 'none',
        top: y + 20,
        left: x - 25,
        border: '1px solid #fdd',
        padding: '7px',
        'background-color': '#fee',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}

// Раскарашивание суббот в другой цвет, что бы выходные было видно на графике
// Воскресенья раскрашивать не надо, поскольку при этом кривая плохо смотрится
function weekendAreas(axes) {
    var markings = [];
    var d = new Date(axes.xaxis.min);
    // go to the first Saturday
    d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
    d.setUTCSeconds(0);
    d.setUTCMinutes(0);
    d.setUTCHours(0);
    var i = d.getTime();
    do {
        // when we don't set yaxis, the rectangle automatically
        // extends to infinity upwards and downwards
        markings.push({
            xaxis: {
                from: i - (0.5 * 24 * 60 * 60 * 1000),
                to: i + (1.5 * 24 * 60 * 60 * 1000)
            },
            color: "#F3F3F3"
        });
        i += 7 * 24 * 60 * 60 * 1000;
    } while (i < axes.xaxis.max);

    return markings;
}