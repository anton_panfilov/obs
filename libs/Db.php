<?php

class Db {

    /**************************************************************************/

    /**
     * @var Db_Connection_Abstract[]
     */
    static protected $connections = [];

    /**
     * @param $name
     * @param null $config
     * @return Db_Connection_Mysqli|Db_Connection_Pdo
     */
    static public function get($name, $config = null){
        if(!isset(self::$connections[$name])){
            /**
             * @var $config Conf_Db
             */
            self::$connections[$name] = Db_Connection::create($config);
        }

        return self::$connections[$name];
    }

    /**************************************************************************/

    static protected $site          = 1;

    /**
     * @return Db_Connection_Mysqli|Db_Connection_Pdo
     */
    static public function site(){
        return self::get(self::$site, Conf::mysql_site());
    }

    /**************************************************************************/

    /**
     * @return Db_Connection_Abstract[]
     */
    static public function getAllCurrentConnections(){
        return self::$connections;
    }

    /**
     * Получить общую информацию из профайлера
     *
     * @return array
     */
    static public function getProfilerSummaryInfo(){
        $all = [];
        if(count($connections = self::getAllCurrentConnections())){
            foreach($connections as $id => $connect) {
                $p = $connect->getProfiler();
                $all[$id] = array(
                    'id'                     => $id,
                    'database_name'          => $connect->getDatabaseName(),
                    'database_engine_driver' => strtoupper($connect->getEngine()) . '/' . strtoupper($connect->getDriver()),
                    'querys_count'           => $p->getQueriesCount(),
                    'runtime'                => $p->getRuntime(),
                    'connected_time'         => $p->getConnectedTime(),
                    'lond_query_time'        => $p->getLongQueryTime(),
                    'lond_query'             => $p->getLongQuery()
                );
            }
        }

        return $all;
    }

    static public function getDatabaseRuntime(){
        $runtime = 0;

        if(count($connections = self::getAllCurrentConnections())){
            foreach($connections as $id => $connect) {
                $runtime += $connect->getProfiler()->getRuntime();
            }
        }

        return $runtime;
    }
}