<?php

class Email_AccountSMTP extends Db_Model {
    static protected $_table = 'email_smtp_accounts';
    static protected $_structure = [
        'email',
        'name',
        'host',
        'port',
        'smtp_secure',
        'auth',
        'username',
        'password',
    ];

    protected function getDatabase(){
        return Db::site();
    }

    /*********************************/

    public $id;
    public $email;
    public $name;
    public $host;
    public $port = 25;
    public $smtp_secure = '';
    public $auth = 0;
    public $username;
    public $password;

}