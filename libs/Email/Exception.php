<?php

class Email_Exception extends Exception {
    public function errorMessage(){
        return '<strong>' . $this->getMessage() . "</strong><br />\n";
    }
}