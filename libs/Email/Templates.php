<?php

class Email_Templates {
    /**
     * Восстанвоение пароля
     *
     * @param $email
     * @param $name
     * @param $link
     *
     * @return bool
     * @throws Email_Exception
     * @throws Exception
     */
    static public function passwordRecovery($email, $name, $link){
        return (new Email_Object(1, [
            'name' => $name,
            'link' => $link,
        ]))
            ->addTo($email, $name)
            ->send();
    }

    /**
     * Отправка логина и пароля при ручном создании пользователя
     *
     * @param $email
     * @param $name
     * @param $login
     * @param $password
     * @param $role
     *
     * @return bool
     * @throws Email_Exception
     * @throws Exception
     */
    static public function createUser($email, $name, $login, $password, $role){
        return (new Email_Object(4, [
            'name'     => $name,
            'role'     => (new User_Role())->getLabel($role),
            'username' => $login,
            'password' => $password,
            'link'     => Http_Url::getCurrentScheme() . '://' . Conf::main()->domain_account . "/sign-in",
        ]))
            ->addTo($email, $name)
            ->send();
    }
}