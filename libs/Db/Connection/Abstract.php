<?php


abstract class Db_Connection_Abstract {

    protected $connect;

    protected $database_name;

    /**
     * @var Db_Profiler
     */
    protected $profiler;

    protected $quotes = '"';

    protected $driver = "pdo";

    protected $engine;
    /**********************************************************************************/
    /*************************************************/
    public function getDriver(){
        return $this->driver;
    }
    public function getEngine(){
        return $this->engine;
    }
    /*************************************************/

    abstract public function __construct(Conf_Db $conf);


    /**********************************************************************************/

    /**
     * @return Db_Profiler
     */
    abstract public function getProfiler();

    abstract public function getDatabaseName();

    /**********************************************************************************/

    /**
     * @param string $from
     * @param array $columns
     * @return Db_Statment_Select_Abstract
     */
    abstract public function select($from = null, $columns = array());

    /**********************************************************************************/

    /**
     * Экранирование строки
     *
     * @param $value
     *
     * @return string
     */
    abstract public function escape($value);

    /**
     * Сформировать строку запроса из подготовленной строки и параметров
     *
     * @param $string
     * @param $params
     *
     * @return string
     *
     * @throws Exception
     */
    abstract public function bind($string, $params);

    /**********************************************************************************/

    /**
     * Выполнить запрос
     *
     * @param $query
     *
     * @return bool|mysqli_result|PDOStatement
     *
     * @throws Exception
     */
    abstract public function query($query);

    /**********************************************************************************/

    abstract public function transactionStart();

    abstract public function transactionCommit();

    abstract public function transactionRollback();

    /**********************************************************************************/

    /**
     * @param $table
     * @param $data
     * @throws Exception
     */
    abstract public function insert($table, array $data);

    /**
     * @param $table
     * @param $data
     * @throws Exception
     */
    abstract public function replace($table, array $data);

    abstract public function insertMulti($table, array $data, $count = 500, $ignore = false);

    /**
     * @return int
     */
    abstract public function lastInsertId();

    /**
     * @param $table
     * @param array $data
     * @param null $where
     * @param array|mixed $whereParams
     *
     * @return int Affected Rows
     *
     * @throws Exception
     */
    abstract public function update($table, array $data, $where = null, $whereParams = []);

    abstract public function delete($table, $where = null, $whereParams = []);

    /**
     * @param $query
     * @param array|mixed $params
     *
     * @return mysqli_result|PDOStatement
     *
     * @throws Exception
     */
    abstract public function fetch($query, $params = []);

    /**
     * Получить одно значение
     *
     * @param $query
     * @param array|mixed $params
     *
     * @return string|bool
     *
     * @throws Exception
     */
    abstract public function fetchOne($query, $params = []);

    /**
     * Получить строку
     *
     * @param $query
     * @param array|mixed $params
     *
     * @return array|bool
     *
     * @throws Exception
     */
    abstract public function fetchRow($query, $params = []);

    /**
     * Получить первую колонку
     *
     * @param $query
     * @param array|mixed $params
     *
     * @return array
     *
     * @throws Exception
     */
    abstract public function fetchCol($query, $params = []);

    /**
     * Получить все данные в массиве
     *
     * @param $query
     * @param array|mixed $params
     *
     * @return array
     *
     * @throws Exception
     */
    abstract public function fetchAll($query, $params = []);

    /**
     * Получить первую колонку в ключ, второе значение в значение
     *
     * @param $query
     * @param array|mixed $params
     *
     * @return array
     *
     * @throws Exception
     */
    abstract public function fetchPairs($query, $params = []);

    /**
     * Первое поле ставиться в ключ массива
     * При дублирующихся ключах, значение идущее позднее не учитываеться
     *
     * @param $query
     * @param array|mixed $params
     *
     * @return array
     *
     * @throws Exception
     */
    abstract public function fetchUnique($query, $params = []);


    static protected function getTimezone($market){
        return $market ?
            Lead_Market::getTimezone($market) :
            Conf::main()->timezone;
    }
} 