<?php

class Site{
    static protected $rightMenuBlock;
    static protected $rightTitleBlock;
    static protected $topBlock;
    static protected $bottomBlock;

    /*************************************************/

    static public function setRightTitleBlock($text){
        self::$rightTitleBlock = $text;
    }

    static public function isRightTitleBlock(){
        return strlen(self::$rightTitleBlock);
    }

    static public function renderRightTitleBlock(){
        return self::$rightTitleBlock;
    }

    /*************************************************/

    static public function setRightMenuBlock($text){
        self::$rightMenuBlock = $text;
    }

    static public function isRightMenuBlock(){
        return strlen(self::$rightMenuBlock);
    }

    static public function renderRightMenuBlock(){
        return self::$rightMenuBlock;
    }

    /*************************************************/

    static public function setTopBlock($text){
        self::$topBlock = $text;
    }

    static public function isTopBlock(){
        return strlen(self::$topBlock);
    }

    static public function renderTopBlock(){
        return self::$topBlock;
    }

    /*************************************************/

    static public function setBottomBlock($text){
        self::$bottomBlock = $text;
    }

    static public function isBottomBlock(){
        return strlen(self::$bottomBlock);
    }

    static public function renderBottomBlock(){
        return self::$bottomBlock;
    }

    /*************************************************/

    static public function addJSCode($code){
        Controller_Boot::controllerParamSet('SiteAddJSCode',
            Controller_Boot::controllerParamGet('SiteAddJSCode') . "\n" . $code
        );
    }

    /**
     * Добавить один или несколько CSS файлов
     * Если добавить один файл больше одного раза, то вторая и последующие добавления будут проигнорированны
     *
     * Варианты адресации:
     * 1. Абсолютная
     *    - Файл с другого домена Ex: `http://css.com/main.css`
     *    - Абсоляюный путь, относительно корня сайта. Ex: `/css/calendar.css`
     *
     * 2. Относительная для библиотеки AP
     *  Можно указывать относительные пути, которые будет ссылатся на файлы из библиотеки AP
     *  Ex: `jquery/jquery.flot.css`
     *
     * @param string|array $files
     * @param bool $addToEnd
     */
    static public function addCSS($files, $addToEnd = true){
        Controller_Boot::controllerParamSet(
            'SiteCSSArray',
            self::addFileAbstract(
                $files,
                Controller_Boot::controllerParamGet('SiteCSSArray', []),
                '/static/css/',
                $addToEnd
            )
        );
    }

    static public function addJS($files, $addToEnd = true){
        Controller_Boot::controllerParamSet(
            'SiteJSArray',
            self::addFileAbstract(
                $files,
                Controller_Boot::controllerParamGet('SiteJSArray', []),
                '/static/js/',
                $addToEnd
            )
        );
    }

    static protected function addFileAbstract($newFiles, $currentFiles, $root, $addToEnd){
        $addToStart = array();

        if(is_string($newFiles))$newFiles = array($newFiles);

        if(is_array($newFiles) && count($newFiles)){
            foreach($newFiles as $file){
                if(substr($file, 0, 7) != "http://" && substr($file, 0, 8) != "https://" && substr($file, 0, 1) != "/"){
                    $file = "{$root}{$file}";
                }
                if(strlen($file) && !in_array($file, $currentFiles)){
                    if($addToEnd){
                        $currentFiles[] = $file;
                    }
                    else {
                        $addToStart[] = $file;
                    }
                }
            }
        }

        // Если файлы надо было добавить в начало, то добавляем
        if(count($addToStart)){
            $currentFiles = array_merge($addToStart, $currentFiles);
        }

        return $currentFiles;
    }

    static public function getPublicSiteLink(){
        return "/";
    }

    static public function refresh(){
        header("location: {$_SERVER['REQUEST_URI']}");
    }

    /**
     * Получить ссылку на статичный контент
     *
     * @param $path
     *
     * @return string
     */
    static public function staticLink($path){
        return "/static/" . $path;
        return "http://" . Conf::main()->domain_static . $path;
    }

    /////////////////////////////////////////////

    static protected $uniqueInt = 1;

    /**
     * @return int
     */
    static public function getUniqueInt(){
        self::$uniqueInt = self::$uniqueInt + 1;

        return self::$uniqueInt;
    }
}