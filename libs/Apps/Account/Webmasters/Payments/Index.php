<?php

class Apps_Account_Webmasters_Payments_Index extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('account/payments');
    }

    function postLoad(){
        $menu = new View_Panel_Menu();

        $menu->add('left_menu_settings',            '/account/webmasters/payments/',                'fa-cog');
        $menu->add('left_menu_paysLog',             '/account/webmasters/payments/history',         'fa-list');
        $menu->add('left_menu_nextPayment',         '/account/webmasters/payments/next-payment',    'fa-clock-o');

        if(!UserAccess::isWebmaster()){
            $menu->add('left_menu_settingsLog',     '/account/webmasters/payments/changes-history', 'fa-history');
        }

        $this->addToSidebar(
            $menu->render()
        );

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('title_settings', "/account/webmasters/payments/", 'icon-credit-card');
        }
    }

    function indexAction(){
        $this->setTitle("title_settings");

        if(UserAccess::isWebmaster()){
            echo (new View_Page_Payments())->render();
        }
        else {
            $form = new Form();
            $form->setMethodGET();
            $form->addButton("btn_webmaster_show");
            $form->addElement_Webmaster("id");

            if($form->isValidAndPost()){
                echo $form->render();
                echo "<br><br>";
                echo new View_Page_Payments_Webmaster($form->getValue('id'));
            }
            else{
                echo $form->render();
            }
        }
    }

    function historyAction(){
        $this->setTitle('title_paysLog');
        echo Webmaster_Payments_Settings::renderPaysHistory();
    }

    function changesHistoryAction(){
        $this->setTitle('title_settingsLog');
        echo Webmaster_Payments_Settings::renderHistory();
    }

    function nextPaymentAction(){
        $this->setTitle('title_settingsNextPayment');
        echo Webmaster_Payments_Settings::renderNextPayment();
    }
}