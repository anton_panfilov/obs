<?php

class Apps_Account_Webmasters_Payments_Payment extends Controller_Abstract {
    protected $payment_data;

    function preLoad(){
        $this->payment_data = Db::site()->fetchRow("SELECT * FROM `webmaster_payments_pays_history` WHERE id=?", isset($_GET['id']) ? $_GET['id'] : 0);

        if($this->payment_data){
            // todo более жестки права для агентов
            // проверка на права доступа
            if(
                UserAccess::isAdmin() ||
                (
                    UserAccess::isAgentWebmaster()
                )
                ||
                (
                    UserAccess::isWebmaster() &&
                    $this->payment_data['webmaster'] == Users::getCurrent()->company_id
                )
            ){
                $this->setHTMLTitle("Payment #{$this->payment_data['id']}");
            }
            else {
                $this->setFollow_Error403();
            }
        }
        else {
            $this->setFollow_Error404();
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add('ID',  $this->payment_data['id']);


        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $id = $this->payment_data['id'];

        $menu->add('Payment Info',         "/account/webmasters/payments/payment/?id={$id}",            'fa-dollar');

        /*********************************
         * breadcrumbs
         */

        $this->addBreadcrumbElement(
            'Pays History',
            "/account/webmasters/payments/history",
            'icon-list'
        );

        $this->addBreadcrumbElement(
            "Payment #{$this->payment_data['id']}",
            "/account/webmasters/payments/payment/?id=" . $id
        );

        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    function indexAction(){
        echo new View_Page_ComingSoon();
    }

}