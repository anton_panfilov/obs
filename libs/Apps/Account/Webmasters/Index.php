<?php

class Apps_Account_Webmasters_Index extends Controller_Abstract {
    /**************************************************************************************/

    function postLoad(){
        $menu = new View_Panel_Menu();

        $menu->add('Webmasters List',    '/account/webmasters/',       'fa-list');

        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            $menu->add('Leads Channels',    '/account/webmasters/channels/',       'fa-paper-plane');
            $menu->add('Payment',     '/account/webmasters/payments/',       'fa-credit-card');
        }

        $this->addToSidebar(
            $menu->render()
        );
    }

    /**************************************************************************************/

    /**
     * Список постингов
     */
    function indexAction(){
        $this->setTitle('Webmasters List');

        $form = new Form();
        $form->setMethodGET();

        $statuses = (new Webmaster_Status())->getArrayToSelect('All');

        $form->addElement_MarketsSelect("market", "Market", "All");

        $form->addElementRadio('status', 'Webmaster Status', $statuses)
            ->setRenderTypeBigValue()
            ->setRequired(false)
            ->setValue('');

        $form->addSeparator('Webmaster');

        $form->addElement_Webmaster('webmaster', 'Name / ID')
            ->setRequired(false);

        $form->addSeparator('User');

        $form->addElementText('email', 'Email')
            ->setRequired(false);

        $form->addElementText('username', 'Login')
            ->setRequired(false);

        if(UserAccess::isAdmin()){
            $form->addSeparator();

            $form->addElement_WebmasterAgent('agent')
                ->setRequired(false);
        }

        $form->isValidNotErrors();

        ////////////////////////////////
        $select = Db::processing()->select(
            "webmaster_accounts",
            [
                "id",
                "name",
                "name",
                "create_date",
            ]
        )
            ->order('id desc');

        if($form->getValue('webmaster')) $select->where("`id`=?", $form->getValue('webmaster'));

        if($form->getValue('email')){
            $select->where(
                "`id`=?",
                Db::site()->fetchOne("SELECT company_id FROM users WHERE email=?", $form->getValue('email'))
            );
        }

        if($form->getValue('username')){
            $select->where(
                "`id`=?",
                Db::site()->fetchOne("SELECT company_id FROM users WHERE login=?", $form->getValue('username'))
            );
        }

        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            /*
            if($form->getValue('agent')){
                $select->where("`agent`=?", $form->getValue('agent'));
            }
            */
        }
        else if(UserAccess::isAgentWebmaster()){
            //$select->where("`agent`=?", Users::getCurrent()->id);
        }
        else {
            $select->where("1=0");
        }

        $webmasters = null;

        if($form->getValue('market')){
            if($form->getValue('status')){
                $webmasters = Db::processing()->fetchCol(
                    "SELECT id FROM `webmaster_markets` WHERE `market`=? AND `status`=?",
                    $form->getValue('market'), $form->getValue('status')
                );
            }
            else {
                $webmasters = Db::processing()->fetchCol(
                    "SELECT id FROM `webmaster_markets` WHERE `market`=?",
                    $form->getValue('market')
                );
            }
        }
        else {
            if($form->getValue('status')){
                $webmasters = Db::processing()->fetchCol(
                    "SELECT id FROM `webmaster_markets` WHERE `status`=?",
                    $form->getValue('status')
                );
            }
        }

        if(!is_null($webmasters)){
            if(count($webmasters)){
                $select->where("`id` in (" . implode(",", $webmasters) . ")");
            }
            else {
                $select->where("1=0");
            }
        }

        // if($form->getValue('status')) $select->where("`status`=?", $form->getValue('status'));

        ////////////////////////////////
        // table
        $table = new Table($select);

        // id	name	active	agent

        $table->addField_Text('id', 'ID')
            ->setLink("/account/webmasters/webmaster/?id={id}")
            ->setTdCSS('width', '1px');

        /*
        $table->addField_SetArray('status', 'Status', new Webmaster_Status());
        */

        $table->addField_Text('name', 'Company Name');

        /*
        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            $table->addField_UserLogin('agent', 'Agent');
        }
        */

        $table->addField_Date("create_date", 'Create');


        $table->startGroup("Markets");

        $table->addField_WebmasterMarketsFlagsAll("id", "All")
            ->setTextAlignCenter();

        $table->addField_WebmasterMarketsFlagsActive("id", "Active")
            ->setTextAlignCenter();

        $table->endGroup();


        echo $form->render() . $table->render();
    }

    /*******************************************************************************************/

    function filterDatetimeAPI(){
        $config_worktime = isset($_POST['config_worktime']) ?
            $_POST['config_worktime'] :
            (isset($_GET['config_worktime']) ?
                $_GET['config_worktime'] :
                ""
            );
        return [
            'config_worktime'           => $config_worktime,
            'timeline'                  => Posting_Filters_Element_Calendar::renderTimeline($config_worktime,""),
            'description_worktime'      => Posting_Filters_Element_Calendar::renderWorktimeDescription($config_worktime),
        ];
    }
}