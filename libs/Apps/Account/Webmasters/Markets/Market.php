<?php

class Apps_Account_Markets_Market extends Controller_Abstract {
    /**
     * @var User
     */
    protected $user;

    /**
     * @var User_WebmasterAgent
     */
    protected $webmasterAgent;

    /**************************************************************************************/

    function preLoad(){
        // проверка по ID
        if(isset($_GET['id'])){
            $this->user = User::getObject($_GET['id']);
        }

        if($this->user->id){
            // проверка на права доступа
            if(
                UserAccess::isAdmin() ||
                (
                    UserAccess::isAgentWebmaster() &&
                    $this->user->role == User_Role::WM
                )
                ||
                (
                    UserAccess::isAgentWebmaster() &&
                    $this->user->role == User_Role::WM &&
                    $this->user->company_id > 0 &&
                    Webmaster_Object::getObject($this->user->company_id)->id &&
                    Webmaster_Object::getObject($this->user->company_id)->getAgentID() == Users::getCurrent()->id
                )
            ){
                $this->setHTMLTitle("User: {$this->user->login}");
            }
            else {
                $this->setFollow_Error403();
            }
        }
        else {
            $this->setFollow_Error404();
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add('Login',  $this->user->login);
        $preview->add('Name',   $this->user->getFullName());
        $preview->add('Role',   (new User_Role())->getLabel($this->user->role));
        $preview->add('Status', (new User_Status())->setInLine()->render($this->user->status));
        $preview->add('Create', (new Decorator_Date())->render($this->user->create_date));

        if($this->user->role == User_Role::WM && Webmaster_Object::getObject($this->user->company_id)->id){
            $preview->add(
                'Company',
                "<a " .
                    "href='/account/webmasters/webmaster/?id=" . Webmaster_Object::getObject($this->user->company_id)->id . "' " .
                ">" .
                    Webmaster_Object::getObject($this->user->company_id)->name .
                "</a>"
            );
        }

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $id = $this->user->id;

        $menu->add('User Info',         "/account/users/user/?id={$id}",            'fa-user');
        $menu->add('User Status',       "/account/users/user/status?id={$id}",      'fa-check-circle-o');
        $menu->add('Change Password',   "/account/users/user/password?id={$id}",    'fa-lock');

        if($this->user->role == User_Role::AGN_WM){
            $menu->add('Webmaster Agent',   "/account/users/user/webmaster-agent?id={$id}",    'fa-group');
        }

        if(UserAccess::isAdmin()){
            $menu->add('Source',   "/account/users/user/source?id={$id}",         'fa-code');
        }


        /*********************************
         * breadcrumbs
         */

        if(UserAccess::isAdmin()){
            $this->addBreadcrumbElement(
                'Users List',
                "/account/users/",
                'icon-list',
                [
                    [
                        (new User_Role())->getLabel($this->user->role),
                        "/account/users/?role=" . $this->user->role
                    ],
                    [
                        (new User_Status())->getLabel($this->user->status),
                        "/account/users/?status=" . $this->user->status
                    ],
                ]
            );

            $this->addBreadcrumbElement(
                $this->user->login,
                "/account/users/user/?id=" . $this->user->id
            );
        }

        $this->addToSidebar("<img style='margin:10px; border-radius: 5% !important; height: 203px' src='" .
            $this->user->getAvatarLink(203) .
        "'>");
        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    function indexAction(){
        $this->setPageTitle("User Settings");

        $form = new Form();

        if(UserAccess::isAdmin()){
            $form->addButton('Save');

            $form->addElementStatic("Status", (new User_Status())->setInLine()->render($this->user->status));
            $form->addElementStatic("Create", (new Decorator_Date())->render($this->user->create_date));

            if($this->user->role == User_Role::WM && Webmaster_Object::getObject($this->user->company_id)->id){
                $form->addElementStatic("Role",
                    (new User_Role())->getLabel($this->user->role) .
                    ": <a " .
                    "href='/account/webmasters/webmaster/?id=" . Webmaster_Object::getObject($this->user->company_id)->id . "' " .
                    ">" .
                    Webmaster_Object::getObject($this->user->company_id)->name .
                    "</a>"
                );
            }
            else {
                $form->addElementStatic("Role",   (new User_Role())->getLabel($this->user->role));
            }

            $form->addSeparator();
            $form->addElementStatic("Login", "<b>{$this->user->login}</b>");

            $form->addElementText_FirstAndLastName('first_name', 'last_name');
            $form->getElement('first_name')->setValue($this->user->first_name);
            $form->getElement('last_name')->setValue($this->user->last_name);

            $form->addElementText_Email('email', 'Email')
                ->setValue($this->user->email);

            $phone = $form->addElementText_Phone("phone", "Phone")
                ->setValue($this->user->phone);

            if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                $phone->setRequired(false);
            }

            if($form->isValidAndPost()){
                $form->addSuccessMessage();

                $this->user->first_name = $form->getValue('first_name');
                $this->user->last_name  = $form->getValue('last_name');
                $this->user->email      = $form->getValue('email');
                $this->user->phone      = $form->getValue('phone');

                $this->user->update(['first_name', 'last_name', 'email', 'phone']);
            }
        }
        else {
            $form->addButton('Save');

            $form->addElementStatic("Role",  (new User_Role())->getLabel($this->user->role));

            $form->addElementStatic("Login", "<b>{$this->user->login}</b>");

            $form->addElementText_FirstAndLastName('first_name', 'last_name');
            $form->getElement('first_name')->setValue($this->user->first_name);
            $form->getElement('last_name')->setValue($this->user->last_name);

            $form->addElementStatic("Email", "{$this->user->email}")
                ->setComment("For security you can not change email and phone");

            $form->addElementStatic("Phone", "{$this->user->phone}");

            if($form->isValidAndPost()){
                $form->addSuccessMessage();

                $this->user->first_name = $form->getValue('first_name');
                $this->user->last_name  = $form->getValue('last_name');

                $this->user->update(['first_name', 'last_name']);
            }
        }

        echo $form->render();
    }

    /********************************************************************************************/

    function statusAction_set(){
        $form = new Form();

        if(
            UserAccess::isAdmin() &&
            in_array($this->user->status, [User_Status::ACTIVE, User_Status::LOCK])
        ){
            $form->addElementRadio('status', 'Status', [
                User_Status::ACTIVE => 'Active',
                User_Status::LOCK   => 'Lock',
            ])
                ->setValue($this->user->status);

            $form->addElementTextarea('reason', 'Reason');

            if($form->isValidAndPost()){
                if($form->getValue('status') != $this->user->status){
                    $form->addSuccessMessage('Saved');

                    $this->user->status = $form->getValue('status');
                    $this->user->update(['status']);

                    if($this->user->status == User_Status::LOCK){
                        $this->user->lockTrigger();
                    }

                    User_Log::changeStatus(
                        $this->user->id,
                        $this->user->status,
                        $form->getValue('reason')
                    );

                    $form->setValue('reason', '');
                }
            }
        }
        else {
            $form->disableButtons();
            $form->addElementStatic('Status', (new User_Status())->setInLine()->render($this->user->status));
        }

        return $form;
    }

    function statusAction_log(){
        $select = Db::site()->select('users_change_status_log')
            ->where("`user`=?", $this->user->id)
            ->order('id desc');

        $table = new Table($select);

        // id	datetime	user

        $table->addField_SetArray('status', 'status', new User_Status());
        $table->addField_Date('datetime', 'Date');

        $table->startGroup('Changer');
        $table->addField_UserLogin('admin', 'User');
        $table->addField_IP('ip', 'IP');
        $table->addField_Text('reason', 'Reason');
        $table->endGroup();

        return $table->render();
    }

    function statusAction(){
        $this->setPageTitle("User Status");

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("set", 'Settings');
        $tabs->addTab("log", 'Log');

        $method = 'statusAction_' . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $tabs;
    }

    /********************************************************************************************/

    function passwordAction(){
        $this->setPageTitle("Change Password");

        if(UserAccess::isAdmin()){
            $form = new Form();
            $form->addButton('Change Password');

            $form->addElementRadio("password_type", "", [
                'random'    => 'Random Password',
                'input'     => 'Input Password',
            ])
                ->setValue('random');

            $form->addElementPasswordNew_Ligths('password', '');

            $randomPassLen = 16;
            $random = (isset($_POST['randomPassword']) && strlen($_POST['randomPassword']) == $randomPassLen) ?
                $_POST['randomPassword'] :
                String_Hash::randomString($randomPassLen);

            $form->addElementHidden("randomPassword", $random);
            $form->addElementStatic("",
                "
                    <p><a href='https://" . Conf::main()->domain_account . "/'>https://" . Conf::main()->domain_account . "/</a></p>
                    <p>Login: <b>{$this->user->login}</b></p>
                    <p>Password: <b>{$random}</b></p>
                ",
                'randomText'
            );

            $form->addConditionalFileds_Array('password_type', 'input',  ['password', 'password_confirm']);
            $form->addConditionalFileds_Array('password_type', 'random', 'randomText');

            if($form->isValidAndPost()){
                $form->addSuccessMessage('Password Changed Complete');

                if($form->getValue('password_type') == 'input'){
                    $this->user->setPassword(
                        $form->getValue('password'),
                        true
                    );
                }
                else {
                    $this->user->setPassword(
                        $random,
                        true
                    );
                }
            }

            echo $form->render();
        }
        else {
            $link = "https://" . Conf::main()->domain_account . "/password-recovery";

            echo "
                <p>For security purposes you can not change the password,
                but the " . $this->user->getFullName() . " can change own password by clicking on the link:</p>
                <p><a style='font-size: 120%' href='{$link}'>{$link}</a></p>
            ";
        }

    }

    /*************************************************************************************/

    function webmasterAgent_public(){
        $form = new Form();
        $form->addButton('Save');

        $form->addElementText_FirstAndLastName('first_name', 'last_name')
            ->setRenderTypeBigValue();
        $form->getElement('first_name')->setValue($this->user->first_name);
        $form->getElement('last_name')->setValue($this->user->last_name);


        $form->addSeparator('Contacts Information');

        $form->addElementText('phone', 'Phone')
            ->setValue($this->webmasterAgent->phone)
            ->setRequired(false)
            ->setRenderTypeBigValue();

        $form->addElementText('phone_mobile', 'Mobile Phone')
            ->setValue($this->webmasterAgent->phone_mobile)
            ->setRequired(false)
            ->setRenderTypeBigValue();

        $form->addElementText_Email('email', 'Email')
            ->setValue($this->webmasterAgent->email)
            ->setRequired(false)
            ->setRenderTypeBigValue();


        $form->addElementText('skype', 'Skype')
            ->setValue($this->webmasterAgent->skype)
            ->setRenderTypeBigValue()
            ->setRequired(false);

        if($form->isValidAndPost()){
            $form->addSuccessMessage("<i class='icon-save margin-right-10'></i> Saved");

            $this->webmasterAgent->getUserObject()->first_name  = $form->getValue('first_name');
            $this->webmasterAgent->getUserObject()->last_name   = $form->getValue('last_name');
            $this->webmasterAgent->getUserObject()->update(['first_name', 'last_name']);

            $this->webmasterAgent->phone        = $form->getValue('phone');
            $this->webmasterAgent->phone_mobile = $form->getValue('phone_mobile');
            $this->webmasterAgent->skype        = $form->getValue('skype');
            $this->webmasterAgent->email        = $form->getValue('email');
            $this->webmasterAgent->update(['phone', 'phone_mobile', 'skype', 'email']);
        }

        return
            '<div class="row-fluid">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
                    ' . $form->render() . '
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                    ' . (new View_User_WebmasterAgent($this->webmasterAgent->id))->render() . '
                </div>
            </div><br clear=both>';
    }

    function webmasterAgent_distribution(){
        ////////////////////////////////////////////////////
        $tab = new Tabs_Markets();
        $market = $tab->getActiveTab();


        ////////////////////////////////////////////////////
        $select = Db::site()->select(
            "`users`",
            ['id', 'status', 'email', 'login', 'first_name', 'last_name']
        )
            ->where("role=?", User_Role::AGN_WM)
            ->where("status=?", User_Status::ACTIVE);

        ////////////////////////////////////////////////////
        $table = new Table($select->fetchAll());
        $table->setPagingDisabled();

        $table->addField_Avatar('email', '', 'avatar');
        $table->addField_SetArray('status', 'Status', new User_Status());
        $table->addField_Text('login', 'Login')->setLink(
            Http_Url::convert(['id' => '{id}'])
        );
        $table->addField_Pattern('{first_name} {last_name}', 'Name');

        $table->startGroup("Markets");
        foreach(Lead_Market::getMarkets() as $el){
            $table->addField_Function(function($context, $value, $params){

                $link = Http_Url::convert([
                    'id'     => $context['id'],
                    'market' => $params['market'],
                ]);

                $style = "display: block; padding: 5px; ";

                if(isset(User_WebmasterAgents::getMarketData($params['market'])[$context['id']])) {
                    $text = "<i style='color: #51a351;' class='fa fa-check'></i>";

                    $text .= User_WebmasterAgents::getMarketData($params['market'])[$context['id']]['show_in_sign_up'] ?
                        "<i style='color: #51a351;' class='fa fa-check'></i>" :
                        "<i style='color: #DDD;' class='fa fa-minus'></i>";

                    $text .= User_WebmasterAgents::getMarketData($params['market'])[$context['id']]['random_distribution'] ?
                        "<i style='color: #51a351;' class='fa fa-check'></i>" :
                        "<i style='color: #DDD;' class='fa fa-minus'></i>";
                }
                else {
                    $text = "<i style='color: #DDD;' class='fa fa-minus'></i>";
                }

                if($params['market'] == $params['currentMarket'] && $context['id'] == $params['currentAgent']){
                    $style.= "background: #F9E688; ";
                }

                return "<a href='{$link}' style='{$style}'>{$text}</a>";

            }, "<i class='flag flag-{$el['short']}'></i> {$el['label']}")
                ->setParam('market', $el['code'])
                ->setParam('currentMarket', $market)
                ->setParam('currentAgent', $this->user->id)
                ->setTdCSS('padding', '0')
                ->setTextAlignCenter()
                ->setWidth("130px");

        }




        ////////////////////////////////////////////////////
        if($this->user->isActive()) {
            $form = new Form();
            $form->addButton('Save');
            $form->addSeparator($this->user->getFullName(1) . " - " . Lead_Market::getLabelText($market));
            $form->addElementRadio("active", "Active", [
                '1' => 'Yes',
                '0' => 'No',
            ])->setValue($this->webmasterAgent->isActiveMarket($market) ? 1 : 0);
            $form->addElementCheckbox('show_in_sign_up', 'Show in Sing Up Form')->setValue($this->webmasterAgent->isShowInSignUp($market) ? 1 : 0);
            $form->addElementText_Num('random_distribution', 'Priority in Random Distribution', 0, 200)->setValue($this->webmasterAgent->getRandomDistribution($market));
            $form->addConditionalFileds_Array("active", 1, [
                'show_in_sign_up',
                'random_distribution'
            ]);
            if($form->isValidAndPost()) {
                if($form->getValue("active")) {
                    User_WebmasterAgents::updateMarketData_active($this->webmasterAgent->id, $market, (int)$form->getValue('show_in_sign_up'), (int)$form->getValue('random_distribution'));
                }
                else {
                    User_WebmasterAgents::updateMarketData_delete($this->webmasterAgent->id, $market);
                }
            }
            $marketData = User_WebmasterAgents::getMarketData($market);
            if(count($marketData)) {
                Cache_PartialList_UserLogin::load(array_keys($marketData));
                $total = 0;
                foreach($marketData as $v) {
                    $total += $v['random_distribution'];
                }
                $tbl = "<table>";
                foreach($marketData as $v) {
                    $tbl .= "<tr>
                    <td style='padding-right: 15px'>
                        <a href='" . Http_Url::convert(['id' => $v['agent']]) . "'>" . Cache_PartialList_UserLogin::get($v['agent']) . "</a>
                    </td>
                    <td style='padding-right: 15px; color:#AAA; font-size:10px'>{$v['random_distribution']}</td>
                    <td>" . round((100 * $v['random_distribution']) / $total, 1) . " %</td>
                </tr>";
                }
                $tbl .= "</table>";
                $form->getElement('random_distribution')->setComment($tbl);
            }
            $show_all = User_WebmasterAgents::getListForSignUp($market);
            if(count($show_all)) {
                $tbl = "<div style='clear: both'><table>";
                $i = 1;
                foreach($show_all as $v) {
                    $tbl .= "<tr>
                    <td style='padding-right: 5px'>" . ($i++) . ".</td>
                    <td style='padding-right: 15px'>{$v['name']}</td>
                    <td style='padding-right: 15px'>
                        <a style='font-size:10px' href='" . Http_Url::convert(['id' => $v['id']]) . "'>
                            {$v['login']}
                        </a>
                    </td>
                </tr>";
                }
                $tbl .= "</table></div>";
                $form->getElement('show_in_sign_up')->setComment($tbl);
            }
            else {
                $form->getElement('show_in_sign_up')->setComment('No agents for sing up form');
            }
            $tab->setText($form->render());
        }
        else {
            $tab->setText(
                Copyright_Concept::div(
                    "Что бы приступить к настройкам, <a href='/account/users/user/status?id={$this->user->id}'>активируйте агента</a>",
                    "Anton"
                )
            );
        }

        return
            $table->render() .
            new View_Block_WebmasterAgentTableDescription .
            "<br>" .
            $tab->render();
    }

    function webmasterAgentAction(){
        // возможность показывать эту страницу

        $this->webmasterAgent = User_WebmasterAgent::getObject($this->user->id);

        if(!$this->webmasterAgent->id){
            $this->setFollow_Error404();
            return;
        }

        /***********************/

        $this->setPageTitle("Webmaster Agent");

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab('public', 'Public Information');
        $tabs->addTab('distribution', 'Markets and Settings');

        $method = 'webmasterAgent_' . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon("Method `{$method}` not found"));
        }

        echo $tabs->render();
    }

    function sourceAction(){
        echo String_Highlight::render(
            String_Highlight::jsonPrettyPrint($this->user->getParams()),
            0
        );
    }
}