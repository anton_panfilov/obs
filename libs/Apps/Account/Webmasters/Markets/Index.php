<?php

class Apps_Account_Webmasters_Markets_Index extends Controller_Abstract {
    /**************************************************************************************/

    function postLoad(){

        $menu = new View_Panel_Menu();

        $menu->add('Add Market',    '/account/webmasters/markets/add',     'fa-plus');
        $menu->add('Markets List',  '/account/webmasters/markets/',        'fa-globe');

        $this->addToSidebar(
            $menu->render()
        );

    }

    /**************************************************************************************/

    /**
     * Список маркетов
     */
    function indexAction(){
        $this->setTitle("Markets");


    }

    /*******************************************************************************************/





}