<?php

class Apps_Account_Webmasters_Webmaster extends Controller_Abstract {
    /**
     * @var Webmaster_Object
     */
    protected $webmaster;

    /**************************************************************************************/

    function preLoad(){
        // проверка по ID
        if(isset($_GET['id'])){
            $this->webmaster = Webmaster_Object::getObject($_GET['id']);
        }

        // Существование страницы и права доступа + название на умолчанию
        if($this->webmaster->id){
            if(
                UserAccess::isAdmin()  ||
                UserAccess::isAgentBuyer() ||
                (UserAccess::isAgentWebmaster() /*&& $this->webmaster->getAgentID() == Users::getCurrent()->id*/)
            ){
                $this->setHTMLTitle("Webmaster: {$this->webmaster->name}");
            }
            else {
                $this->setFollow_Error403();
            }
        }
        else {
            $this->setFollow_Error404();
        }

    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();


        $preview->add('ID',         $this->webmaster->id);
        $preview->add('Name',       $this->webmaster->name);


        foreach(Webmaster_Markets::getMarketsData($this->webmaster->id) as $market){
            $data = (new Webmaster_Status())->setInLine()->render(
                $market['status']
            ) . " ";


            if(UserAccess::isAdmin()){
                $data.= "<a href='/account/users/user/webmaster-agent?id={$market['agent']}'>" .
                    Cache_PartialList_UserLogin::get($market['agent']) .
                "</a>";
            }
            else if(UserAccess::isAgentBuyer()){
                $data.= Cache_PartialList_UserLogin::get($market['agent']);
            }

            $preview->add(
                Lead_Market::renderFlag($market['market']),
                $data
            );
        }

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $id = $this->webmaster->id;

        if($this->webmaster->isVerify()){
            $menu->add("Profile",   "/account/webmasters/webmaster/?id={$id}",          'fa-file-text-o');

            if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
                $menu->add("Users",         "/account/webmasters/webmaster/users?id={$id}",     'fa-group');

                if(UserAccess::isAdmin()){
                    $menu->add("Prices",         "/account/webmasters/webmaster/prices?id={$id}",     'fa-dollar');
                }

                $menu->addGroup("Logs", "fa-hdd-o")
                    ->add("Postbacks",         "/account/webmasters/webmaster/log-postbacks?id={$id}");

                $p = $menu->addGroup("Payment",  'fa-credit-card');

                $p->add('Payment Settings',      "/account/webmasters/webmaster/payments-settings?id={$id}");
                $p->add('Billing Info History',   "/account/webmasters/webmaster/billing-info-history?id={$id}");
                $p->add('Pays History',           "/account/webmasters/webmaster/pays-history?id={$id}");
            }
        }
        else {
            $menu->add("Verification",   "/account/webmasters/webmaster/?id={$id}",          'fa-file');

            if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
                $menu->add("Users",         "/account/webmasters/webmaster/users?id={$id}",     'fa-group');
            }
        }

        /*********************************
         * breadcrumbs
         */
        $this->addBreadcrumbElement(
            'Webmasters List',
            "/account/webmasters/",
            'icon-list'
        );

        $this->addBreadcrumbElement(
            $this->webmaster->name,
            "/account/webmasters/webmaster/?id={$id}"
        );


        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    protected function index_main(){
        $form = new Form();

        $form->addButton('Save');

        $form->addElementStatic('ID',    "<b>" . $this->webmaster->id . "</b>");
        $form->addElementStatic('Name',  "<b>" . $this->webmaster->name . "</b>");
        $form->addElementStatic('Create',       (new Decorator_Date())->render($this->webmaster->create_date));
        /*
        $form->addElementText('name', 'Webmaster Name')
            ->setValue($this->webmaster->name)
            ->addValidator(new Form_Validator_UniqueInDatabase(
                "select `id` from `webmaster_accounts` where `name`=? and id!='{$this->webmaster->id}' limit 1",
                Db::processing(),
                "Someone already has that webmaster name"
            ));
        */


        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            foreach($this->webmaster->getMarkets() as $market){
                $form->addSeparator(Lead_Market::getLabelText($market));

                /////////////

                if(UserAccess::isAdmin()) {
                    $agents = ['' => '...'];

                    $temp = User_WebmasterAgents::getActiveAgentsForMarket($market);
                    if(count($temp)) {
                        Cache_PartialList_UserLogin::load($temp);
                        foreach($temp as $el) {
                            $agents[$el] = Cache_PartialList_UserLogin::get($el);
                        }
                    }

                    $form->addElementSelect('agent_' . $market, "Agent", $agents)->setValue($this->webmaster->getAgentID($market));
                }
                else {
                    $form->addElementStatic("Agent", Cache_PartialList_UserName::get($this->webmaster->getAgentID($market)));
                }

                /////////////

                if(
                    UserAccess::isAdmin() ||
                    (
                        UserAccess::isAgentWebmaster() &&
                        $this->webmaster->getAgentID($market) == Users::getCurrent()->id
                    )
                ){
                    $form->addElementSelect('status_' . $market, 'Status', $this->webmaster->getNextStatuses($market))
                        ->setRenderTypeBigValue()
                        ->setValue($this->webmaster->getStatus($market));

                    $form->addElementTextarea("status_reason_" . $market, 'Reason for change')
                        ->setRenderTypeBigValue();

                    $form->addConditionalFileds_NotArray(
                        'status_' . $market,
                        $this->webmaster->getStatus($market),
                        ['status_reason_' . $market]
                    );
                }
                else {
                    $form->addElementStatic("Status", (new Webmaster_Status())->setInLine()->render($this->webmaster->getStatus($market)));
                }

                /////////////


                if(!isset($agents[$this->webmaster->getAgentID($market)])){
                    $agents[$this->webmaster->getAgentID($market)] =
                        Cache_PartialList_UserLogin::get($this->webmaster->getAgentID($market)) . " (not active)";
                }

            }
        }
        else {

        }
        /*
        else if(UserAccess::isAgentBuyer()){
            $form->addSeparator("Account Representative");
            $form->addElementStatic(
                "Agent",
                Cache_PartialList_UserLogin::get($this->webmaster->getAgentID())
            );
        }
        */


        ////////////////////////////
        // $form->addSeparator("Webmaster Status");


        /*
        $form->addElementRadio('active', '')
            ->setOptions([
                '1' => 'Active',
                '0' => 'Paused',
            ])
            ->setValue((string)(int)$this->webmaster->isActive())
            ->setRenderTypeBigValue();

        $form->addElementTextarea("active_reason", 'Reason')
            ->setRequired($this->webmaster->isActive() ? true : false)
            ->setRenderTypeBigValue();

        $form->addElementRadio('status', 'Status', $this->webmaster->getNextStatuses())
            ->setRenderTypeBigValue()
            ->setValue($this->webmaster->getStatus());

        $form->addElementTextarea("status_reason", 'Reason')
            ->setRenderTypeBigValue();

        $form->addConditionalFileds_NotArray('status', $this->webmaster->getStatus(), ['status_reason']);
        */

        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            foreach($this->webmaster->getMarkets() as $market){
                if(
                    UserAccess::isAdmin() ||
                    (
                        UserAccess::isAgentWebmaster() &&
                        $this->webmaster->getAgentID($market) == Users::getCurrent()->id
                    )
                ){
                    if($this->webmaster->getStatus($market) != $form->getValue("status_" . $market)){
                        $this->webmaster->setStatus(
                            $market,
                            $form->getValue('status_' . $market),
                            $form->getValue('status_reason_' . $market)
                        );

                        $form->getElement('status_reason_' . $market)
                            ->setValue('');

                        // переделать записимость показывания комментария
                        $form->getElement("status_" . $market)
                            ->setOptions($this->webmaster->getNextStatuses($market))
                            ->setValue($this->webmaster->getStatus($market));

                        $form->removeConditional(
                            "status_" . $market
                        );

                        $form->addConditionalFileds_NotArray(
                            'status_' . $market,
                            $this->webmaster->getStatus($market),
                            ['status_reason_' . $market]
                        );
                    }
                }

                if(UserAccess::isAdmin()){
                    if($this->webmaster->getAgentID($market) != $form->getValue("agent_" . $market)){
                        $this->webmaster->setAgent(
                            $market,
                            $form->getValue('agent_' . $market),
                            "Manual change"
                        );
                    }
                }
            }
            // Изменение статуса
            /*
            if($this->webmaster->getStatus() != $form->getValue('status')){
                try {
                    $this->webmaster->setStatus(
                        $form->getValue('status'),
                        $form->getValue('status_reason')
                    );

                    $form->getElement('status_reason')
                        ->setValue('');

                    // переделать записимость показывания комментария
                    $form->getElement("status")
                        ->setOptions($this->webmaster->getNextStatuses())
                        ->setValue($this->webmaster->getStatus());

                    $form->removeConditional("status");
                    $form->addConditionalFileds_NotArray('status', $this->webmaster->getStatus(), ['status_reason']);
                }
                catch(Exception $e){}
            }
            */

            /*
            if(UserAccess::isAdmin()){
                if($this->webmaster->getAgentID() != $form->getValue('agent')){
                    $this->webmaster->setAgent($form->getValue('agent'));
                }
            }
            */

        }

        return "
            <div class='col-xs-12 col-sm-12 col-md-12 col-lg-7'>" . $form->render() . "</div>
            <div class='col-xs-12 col-sm-12 col-md-12 col-lg-1'></div>
            <div class='col-xs-12 col-sm-12 col-md-12 col-lg-4'>
                <h2>Webmaster Links</h2>
                <h5>Reports</h5>
                <p><a href='/account/reports/webmaster/?webmaster={$this->webmaster->id}'>Daily Summary</a></p>
                <p><a href='/account/reports/webmaster/leads?webmaster={$this->webmaster->id}'>Leads Details</a></p>
                <p><a href='/account/reports/webmaster/leads-errors?webmaster={$this->webmaster->id}'>Errors</a></p>
            </div>
            <br clear='both'>
        ";
    }

    protected function index_company(){
        return $this->webmaster->renderDetailsForm();
    }

    protected function index_agents_log(){
        $select = Db::site()->select("webmaster_change_agent_log")
            ->where("webmaster=?", $this->webmaster->id)
            ->order('id desc');

        $table = new Table($select);

        $table->addField_MarketFlag('market');
        $table->addField_Date('datetime', 'Date');
        $table->addField_UserLogin("agent", "Agent");
        $table->addField_Text("reason", "Reason");
        $table->addField_UserLogin("user", "Changer");
        $table->addField_IP("ip", "Changer IP");

        return $table->render();
    }

    protected function index_statuses_log(){
        $select = Db::site()->select("webmaster_change_status_log")
            ->where("webmaster=?", $this->webmaster->id)
            ->order('id desc');

        $table = new Table($select);

        $table->addField_MarketFlag('market');
        $table->addField_Date('datetime', 'Date');
        $table->addField_SetArray("status", "Status", new Webmaster_Status());
        $table->addField_Text("reason", "Reason");
        $table->addField_UserLogin("user", "Changer");
        $table->addField_IP("ip", "Changer IP");


        return $table->render();
    }

    function indexAction(){
        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            if($this->webmaster->isVerify()){
                $this->setPageTitle("Webmaster Profile");
                $this->setHTMLTitle("{$this->webmaster->name}: profile");

                $tabs = new Tabs_StaticTabs();

                $tabs->addTab('main',    'Main');
                $tabs->addTab('company', 'Company Profile');

                if(UserAccess::isAdmin()){
                    $tabs->addTab('agents_log', 'Agents Log');
                    $tabs->addTab('statuses_log', 'Statuses Log');
                }

                $method = "index_" . $tabs->getActiveTab();
                if(method_exists($this, $method)){
                    $tabs->setText(
                        $this->$method()
                    );
                }
                else {
                    $tabs->setText(
                        new View_Page_ComingSoon()
                    );
                }

                echo $tabs->render();
            }
            else {
                $this->setTitle("Webmaster Verification");

                $userID = Db::site()->fetchOne(
                    "SELECT id FROM users WHERE `company_id`=? AND `status`=? and `role`=?",
                    $this->webmaster->id, User_Status::VERIFY, User_Role::WM
                );

                if(!$userID || !User::getObject($userID)->id){
                    throw new Exception("Not Found user for webmaster: {$this->webmaster->id}");
                }

                $verificationData = Db::site()->fetchRow(
                    "SELECT * FROM `users_activation` WHERE `id`=?", $userID
                );

                if(!$verificationData){
                    throw new Exception("Invalid verification data for user: {$userID}");
                }

                // Value::export($verificationData);

                $form = new Form();

                $form->addSeparator('Contact Information');

                $form->addElementStatic('Name', User::getObject($userID)->getFullName());

                $form->addElementStatic(
                    'Email',
                    "<a href='mailto:" . User::getObject($userID)->email . "'>" . User::getObject($userID)->email . "</a>"
                );

                if($verificationData['step'] == 1){
                    $form->addElementStatic('Phone', User::getObject($userID)->phone);

                    $form->addButton("Skip Email Confirmation");

                    if($form->isValidAndPost()){
                        Db::site()->update("users_activation", [
                            'step' => 2
                        ], "id=?", $verificationData['id']);

                        header('location:' . Http_Url::convert([]));
                    }
                }
                else if($verificationData['step'] == 2){
                    $form->addElementText_Phone('phone', 'Phone')
                        ->setValue(User::getObject($userID)->phone);

                    $form->addButton("Save Phone and Skip Phone Confirmation");

                    if($form->isValidAndPost()){
                        Db::site()->update("users_activation", [
                            'step' => 3
                        ], "id=?", $verificationData['id']);

                        User::getObject($userID)->phone = $form->getValue("phone");
                        User::getObject($userID)->update(['phone']);

                        header('location:' . Http_Url::convert([]));
                    }
                }
                else if($verificationData['step'] == 3){

                    $form = Webmaster_Object::getObject(User::getObject($userID)->company_id)->renderDetailsForm();

                    if($form->getIsValidCache()){
                        Webmaster_Object::getObject(User::getObject($userID)->company_id)
                            ->verificationComplete(false, "User: " . Users::getCurrent()->id);

                        // активация пользователя, удаление сессии активации и перенаправление в панель
                        User::getObject($userID)->verificationComplete(false, "Agent activated this account");
                        Db::site()->delete("users_activation", "id=?", $userID);

                        header('location:' . Http_Url::convert([]));
                    }

                    $form->setDescription(
                        "<center><h2>Suggest customer login and himself complete activation or fill it for him</h2></center>"
                    );
                    $form->addButton("Save and Activate");

                    $form->addSeparator('Contact Information');

                    $form->addElementStatic('Name', User::getObject($userID)->getFullName());

                    $form->addElementStatic(
                        'Email',
                        "<a href='mailto:" . User::getObject($userID)->email . "'>" . User::getObject($userID)->email . "</a>"
                    );

                    $form->addElementStatic('Phone', User::getObject($userID)->phone);

                }

                echo (new Ui_Steps())
                    ->addStep('Email Confirmation')
                    ->addStep('Phone Confirmation')
                    ->addStep('Company Profile')
                    ->setCurrentStep($verificationData['step'])
                    ->render();

                echo "<div style='clear: both; padding-top: 40px'>";

                echo $form;
            }
        }
        else {
            $this->setTitle('Webmaster Profile');

            $form = new Form();
            $form->disableButtons();

            $form->addElementStatic("ID",       $this->webmaster->id);
            $form->addElementStatic("Name",     $this->webmaster->name);

            echo $form;
        }
    }

    /*************************************************************************************/

    function usersAction(){
        $this->setTitle("Webmaster Users");

        $select = Db::site()->select(
            "users",
            ['id', 'create_date', 'status', 'role', 'login', 'email', 'phone', 'company_id', 'first_name', 'last_name']
        )
            ->where("company_id=?", $this->webmaster->id);

        $table = new Table($select);
        $table->setPagingAuto(100);

        $table->addField_Text('id', 'ID')->setLink(
            "/account/users/user/?id={id}"
        );
        $table->addField_Avatar('email', '', 'avatar');
        $table->addField_SetArray('role', 'Role', (new User_Role()));
        $table->addField_SetArray('status', 'Status', new User_Status());
        $table->addField_Text('login', 'Login')->setLink(
            "/account/users/user/?id={id}"
        );

        $table->addField_Text('email', 'Email');
        $table->addField_Text('phone', 'Phone');
        $table->addField_Pattern('{first_name} {last_name}', 'Name');
        $table->addField_Date('create_date', 'Create');


        echo $table->render();
    }

    /*************************************************************************************/

    protected function prices_set(){
        $form = new Form();
        $form->addButton("Save");

        $form->addElementRadio("type", "Price Settings", [
            'default'       => 'Default',
            'individual'    => 'Individual Settings',
        ])
            ->setValue($this->webmaster->getPrices()->webmaster ? "individual" : "default");


        /////////////////////////////////////////////////
        $form->addSeparator("Prices Settings");
        /////////////////////////////////////////////////

        $form->addElementStatic(
            "Web Leads",
            Webmaster_Prices::getPrices(0)->rate_leads . " <small style='color:#999'>%</small>",
            'default_rate_leads'
        );

        /////////////////

        $form->addElementText_Num('rate_leads', 'Web Leads', 0, 99)
            ->setValue($this->webmaster->getPrices()->rate_leads)
            ->setWidth('50px')
            ->setTextPostInput("<span style='font-size:24px; padding-left: 10px; color:#CCC'>%</span>");


        ///////////////////////////////////////////////

        $form->addConditionalFileds_Array(
            "type",
            "default",
            [
                "default_rate_leads",
            ]
        );

        $form->addConditionalFileds_Array(
            "type",
            "individual",
            [
                "rate_leads",
            ]
        );

        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            if($form->getValue("type") == 'default'){
                Webmaster_Prices::deletePrices($this->webmaster->id);
            }
            else {
                // установить новые правила
                Webmaster_Prices::setPrices($this->webmaster->id, new Webmaster_Prices([
                    'rate_leads'            => $form->getValue('rate_leads'),
                ]));
            }
        }

        return $form->render();
    }

    protected function prices_log(){
        $select = Db::site()->select("`webmaster_prices_changes_log`")
            ->where("`webmaster`=?", $this->webmaster->id)
            ->order("id desc");

        $table = new Table($select);

        // id		webmaster		ip

        $table->addField_Date("create", "Create");

        $table->addField_SetArray("action", "Action")
            ->addValue_White("insert", "Insert")
            ->addValue_White("update", "Update")
            ->addValue_White("delete", "Delete");

        $table->addField_Text("details", "Details")

            ->setTdCSS('width', '50%')
            ->setTdCSS("padding", '0px')
            ->addDecoratorFunction(
                function($value){
                    $res = '';

                    if(strlen($value)){
                        $value = Json::decode($value);

                        $res = "
                            <table style='width: 100%'>
                                <tr>
                                    <td style='border: 0; width: 1px; color: #999'><small>Web Leads:</small></td>
                                    <td style='border: 0; padding-right: 20px'>{$value['rate_leads']} <small>%</small></td>
                                </tr>
                            </table>
                        ";
                    }

                    return $res;
                }
            );

        $table->addField_UserLogin("user", "User");

        $table->addField_IP("ip", "IP");

        return $table->render();
    }

    public function pricesAction(){
        $this->setTitle("Prices");

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab('set', 'Settings');
        $tabs->addTab('log', 'Log');

        $method = "prices_" . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText(
                $this->$method()
            );
        }
        else {
            $tabs->setText(
                new View_Page_ComingSoon()
            );
        }

        echo $tabs->render();
    }

    /*************************************************************************************/

    function paymentsSettingsAction(){
        $this->setTitle("Payment Settings");

        echo Webmaster_Payments_Settings::renderForm($this->webmaster->id);
        return;

        $tabs = new Tabs_Markets();
        $market = $tabs->getActiveTab();

        $form = new Form();

        $settings = Webmaster_Payments_Object::getObject($this->webmaster->id, $market);

        if(UserAccess::isAdmin()){
            $form->addElementText_Num('hold', 'Hold', 0, 200)
                ->setValue($settings->hold);

            $form->addElementRadio('fee', 'Fee')
                ->setOptions([
                    '1' => 'Yes',
                    '0' => 'No',
                ])
                ->setLabelWidth("50px")
                ->setValue($settings->fee);

            $form->addElementSelect('period', 'Period')
                ->setOptions((new Webmaster_Payments_Period())->getArrayToSelect())
                ->setValue($settings->period);

            $form->addElementStatic(
                'Billing Info',
                "<div style='font-size:120%; padding-bottom: 10px'>
                    <a href='/account/webmasters/webmaster/billing-info?id={$this->webmaster->id}'>" .
                        (new Webmaster_Payments_Type())->getLabel($settings->type) .
                    "</a>
                </div>" .
                $settings->renderDetails()
            );

            if($form->isValidAndPost()){
                if(
                    $form->getValue('fee') != $settings->fee ||
                    $form->getValue('hold') != $settings->hold ||
                    $form->getValue('period') != $settings->period
                ){
                    $form->addSuccessMessage();

                    $settings->fee = $form->getValue('fee');
                    $settings->hold = $form->getValue('hold');
                    $settings->period = $form->getValue('period');
                    $settings->update(['hold', 'period']);
                }
            }
        }
        else {
            $form->disableButtons();
            $form->addElementStatic('Hold', "{$settings->hold} <small>days</small>");
            if(!$settings->fee){
                $form->addElementStatic('Fee', "No");
            }
            $form->addElementStatic('Period', (new Webmaster_Payments_Period())->getLabel($settings->period));
            $form->addElementStatic(
                'Billing Info',
                "<b>" . (new Webmaster_Payments_Type())->getLabel($settings->type) . "</b>" . $settings->renderDetails()
            );
        }


        $tabs->setText($form->render());

        echo $tabs->render();
    }

    /*
    function billingInfoAction(){
        $this->setTitle("Set Billing Info");

        //echo Webmaster_Payments_Settings::renderForm($this->webmaster->id);
    }
    */

    function billingInfoHistoryAction(){
        $this->setTitle("Billing Info History");

        echo Webmaster_Payments_Settings::renderHistory($this->webmaster->id);
    }


    function paysHistoryAction(){
        $this->setTitle("Pays History");

        echo Webmaster_Payments_Settings::renderPaysHistory($this->webmaster->id);
    }

    /*************************************************************************************/


}