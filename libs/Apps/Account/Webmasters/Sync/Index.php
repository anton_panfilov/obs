<?php

class Apps_Account_Webmasters_Sync_Index extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('account/sync');

        // Хлебные крошки
        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            $this->addBreadcrumbElement('Webmasters',   "/account/webmasters/");
        }

        // if($this->_action != 'index'){
            $this->addBreadcrumbElement('Synchronization',     "/account/webmasters/sync/");
        //}
    }

    function postLoad(){
        $menu = new View_Panel_Menu();

        $menu->add('Postbacks',  '/account/webmasters/sync/postback',    'fa-retweet');
        $menu->add('API',        '/account/webmasters/sync/api',         'fa-rss');

        $this->addToSidebar(
            $menu->render()
        );
    }

    function indexAction(){
        echo (new View_Page_Sync_Main())->render();
    }

    function postbackAction(){
        $this->setTitle('Realtime Postbacks');
        echo View_Page_Sync_Main::getDescriptionPostbacks() . "<br>";

        $webmaster = null;
        if(UserAccess::isWebmaster()){
            $webmaster = Users::getCurrent()->company_id;
        }
        else if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            $formWM = new Form();
            $formWM->setMethodGET();
            $formWM->disableButtons();

            $formWM->addElement_Webmaster("webmaster");
            $formWM->addMultiElement(['webmaster'], 'Webmaster')
                ->setTemplate(
                    "<table style='width: 100%; max-width: 400px'><tr>
                        <td style='padding: 5px'>{webmaster}</td>
                        <td style='padding: 2px 5px 5px 5px; width: 1%'><input type='submit' class='btn btn-default' value='" .
                            Translate::t("Show Settings") .
                        "'></td>
                    </tr></table>"
                )
                //->setRenderTypeBigValue()
                ->setRenderTypeBigValue();

            $formWM->isValidNotErrors();
            echo $formWM;

            $webmaster = $formWM->getValue('webmaster');

            if($webmaster){
                echo "<br><h3>Postbacks settings for <b>" . Cache_PartialList_WebmasterName::get($webmaster) . "</b>:</h3>";
            }
        }

        if($webmaster){
            Site::addJSCode("
                jQuery('#link_new').focus(function(){
                    jQuery('#link_online_sale_doc').hide('fast');
                    jQuery('#link_offline_sale_doc').hide('fast');
                    jQuery('#link_return_doc').hide('fast');

                    jQuery('#link_new_doc').show('fast');
                 });


                jQuery('#link_online_sale').focus(function(){
                    jQuery('#link_new_doc').hide('fast');
                    jQuery('#link_offline_sale_doc').hide('fast');
                    jQuery('#link_return_doc').hide('fast');

                    jQuery('#link_online_sale_doc').show('fast');
                });

                jQuery('#link_offline_sale').focus(function(){
                    jQuery('#link_new_doc').hide('fast');
                    jQuery('#link_online_sale_doc').hide('fast');
                    jQuery('#link_return_doc').hide('fast');

                    jQuery('#link_offline_sale_doc').show('fast');
                });

                jQuery('#link_return').focus(function(){
                    jQuery('#link_new_doc').hide('fast');
                    jQuery('#link_online_sale_doc').hide('fast');
                    jQuery('#link_offline_sale_doc').hide('fast');

                    jQuery('#link_return_doc').show('fast');
                });
            ");

            $settings = Webmaster_Tracking_Postback_Settings::getObject($webmaster);
            if(!$settings->id){
                $settings->id = $webmaster;
                $settings->insert();
            }

            $form = new Form();
            $form->addButton("Save");

            //////////////////////////////////////////
            $form->addSeparator(
                ""
            );
            $form->addElementRadio("is_link_new", "Новый лид", [
                '0' => 'Off',
                '1' => 'On',
            ])
                ->setLabelWidth("70px")
                ->setRequired(false)
                ->setRenderTypeBigValue()
                ->setComment("
                    <p>Получен новый лид, особенно полезно для тек кто посылает трафик</p>
                ")
                ->setValue(strlen($settings->link_new) ? '1' : '0');

            $form->addElementText_Url("link_new", "", ['lead_id'])
                ->setRenderTypeBigValue()
                ->setValue($settings->link_new)
                ->setComment("
                    <div id='link_new_doc' style='display: none'>
                        <table class='table'>
                            <tr><td colspan='3' style='padding-top: 20px; border: 0; color: #111'><b>Required macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>LeadID в нашей системе. Пример: 1234567.12345</td>
                            </tr>

                            <tr><td colspan='3' style='padding-top: 30px; border: 0; color: #111'><b>Optional macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{click_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Ваш ClickID, который вы передавали посылая нам лиды или трафик.</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_date}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Дата и время создания лида. Формат YYYY-MM-DD HH:MM:SS</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{webmaster}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID вебмастра. Может быть полезно если вы принимаете в одно место данные от разным вебматров</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{product}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID продукта</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel_type}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>Тип канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{domain}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Домен с которого пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{page}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Страница с которой пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{region}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID Региона по базе ФИАС</td>
                            </tr>
                        </table>
                    </div>
                ");

            $form->addConditionalFileds_Array('is_link_new', '1', 'link_new');




            //////////////////////////////////////////
            $form->addSeparator();
            $form->addElementRadio("is_link_online_sale", "CPL", [
                '0' => 'Off',
                '1' => 'On',
            ])
                ->setLabelWidth("70px")
                ->setRequired(false)
                ->setRenderTypeBigValue()
                ->setComment("
                    <p>Оплата по CPL модели. Сразу в момент получения лида.
                    <i class='txt-color-red'>Будьте внимательны если у вас API каналы, вы уже видели эти цены!</i>
                    </p>
                ")
                ->setValue(strlen($settings->link_online_sale) ? '1' : '0');

            $form->addElementText_Url("link_online_sale", "", ['lead_id', 'price_add'])
                ->setRenderTypeBigValue()
                ->setValue($settings->link_online_sale)
                ->setComment("
                    <div id='link_online_sale_doc' style='display: none'>
                        <table class='table'>
                            <tr><td colspan='3' style='padding-top: 20px; border: 0; color: #111'><b>Required macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>LeadID в нашей системе. Пример: 1234567.12345</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{price_add}</b></td>
                                <td style='width: 1%; color: #008000'>(decimal)</td>
                                <td>Сумма, на которую изменилась цена лида</td>
                            </tr>

                            <tr><td colspan='3' style='padding-top: 30px; border: 0; color: #111'><b>Optional macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{price_finish}</b></td>
                                <td style='width: 1%; color: #008000'>(decimal)</td>
                                <td>Конечная цена лида, после изменения</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{click_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Ваш ClickID, который вы передавали посылая нам лиды или трафик.</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_date}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Дата и время создания лида. Формат YYYY-MM-DD HH:MM:SS</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{webmaster}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID вебмастра. Может быть полезно если вы принимаете в одно место данные от разным вебматров</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{product}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID продукта</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel_type}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>Тип канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{domain}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Домен с которого пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{page}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Страница с которой пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{region}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID Региона по базе ФИАС</td>
                            </tr>
                        </table>
                    </div>
                ");

            $form->addConditionalFileds_Array('is_link_online_sale', '1', 'link_online_sale');



            //////////////////////////////////////////
            $form->addSeparator();
            $form->addElementRadio("is_link_offline_sale", "CPA", [
                '0' => 'Off',
                '1' => 'On',
            ])
                ->setLabelWidth("70px")
                ->setRequired(false)
                ->setRenderTypeBigValue()
                ->setComment("
                    <p>Оплата по CPA модели. При совершении клиентом какого то действия, например он взял кредит или страховку</p>
                ")
                ->setValue(strlen($settings->link_offline_sale) ? '1' : '0');

            $form->addElementText_Url("link_offline_sale", "", ['lead_id', 'price_add'])
                ->setRenderTypeBigValue()
                ->setValue($settings->link_offline_sale)
                ->setComment("
                    <div id='link_offline_sale_doc' style='display: none'>
                        <table class='table'>
                            <tr><td colspan='3' style='padding-top: 20px; border: 0; color: #111'><b>Required macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>LeadID в нашей системе. Пример: 1234567.12345</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{price_add}</b></td>
                                <td style='width: 1%; color: #008000'>(decimal)</td>
                                <td>Сумма, на которую изменилась цена лида</td>
                            </tr>

                            <tr><td colspan='3' style='padding-top: 30px; border: 0; color: #111'><b>Optional macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{price_finish}</b></td>
                                <td style='width: 1%; color: #008000'>(decimal)</td>
                                <td>Конечная цена лида, после изменения</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{click_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Ваш ClickID, который вы передавали посылая нам лиды или трафик.</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_date}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Дата и время создания лида. Формат YYYY-MM-DD HH:MM:SS</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{webmaster}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID вебмастра. Может быть полезно если вы принимаете в одно место данные от разным вебматров</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{product}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID продукта</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel_type}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>Тип канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{domain}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Домен с которого пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{page}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Страница с которой пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{region}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID Региона по базе ФИАС</td>
                            </tr>
                        </table>
                    </div>
                ");

            $form->addConditionalFileds_Array('is_link_offline_sale', '1', 'link_offline_sale');



            //////////////////////////////////////////
            $form->addSeparator();
            $form->addElementRadio("is_link_return", "Возврат лида", [
                '0' => 'Off',
                '1' => 'On',
            ])
                ->setLabelWidth("70px")
                ->setRequired(false)
                ->setRenderTypeBigValue()
                ->setComment("<p></p>")
                ->setValue(strlen($settings->link_return) ? '1' : '0');

            $form->addElementText_Url("link_return", "", ['lead_id', 'price_add'])
                ->setRenderTypeBigValue()
                ->setValue($settings->link_return)
                ->setComment("
                    <div id='link_return_doc' style='display: none'>
                        <table class='table'>
                            <tr><td colspan='3' style='padding-top: 20px; border: 0; color: #111'><b>Required macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>LeadID в нашей системе. Пример: 1234567.12345</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{price_add}</b></td>
                                <td style='width: 1%; color: #008000'>(decimal)</td>
                                <td>Сумма, на которую изменилась цена лида</td>
                            </tr>

                            <tr><td colspan='3' style='padding-top: 30px; border: 0; color: #111'><b>Optional macroses:</b></td></tr>
                            <tr>
                                <td style='width: 1%'><b>{price_finish}</b></td>
                                <td style='width: 1%; color: #008000'>(decimal)</td>
                                <td>Конечная цена лида, после изменения</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{click_id}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Ваш ClickID, который вы передавали посылая нам лиды или трафик.</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{lead_date}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Дата и время создания лида. Формат YYYY-MM-DD HH:MM:SS</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{webmaster}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID вебмастра. Может быть полезно если вы принимаете в одно место данные от разным вебматров</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{product}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID продукта</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{channel_type}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>Тип канала</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{domain}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Домен с которого пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{page}</b></td>
                                <td style='width: 1%; color: #008000'>(string)</td>
                                <td>Страница с которой пришел лид</td>
                            </tr>
                            <tr>
                                <td style='width: 1%'><b>{region}</b></td>
                                <td style='width: 1%; color: #008000'>(int)</td>
                                <td>ID Региона по базе ФИАС</td>
                            </tr>
                        </table>
                    </div>
                ");

            $form->addConditionalFileds_Array('is_link_return', '1', 'link_return');



            ////////////////////////////////////////////////////////////////
            if($form->isValidAndPost()){
                $settings->link_new             = $form->getValue('is_link_new')            ? $form->getValue('link_new')           : null;
                $settings->link_online_sale     = $form->getValue('is_link_online_sale')    ? $form->getValue('link_online_sale')   : null;
                $settings->link_offline_sale    = $form->getValue('is_link_offline_sale')   ? $form->getValue('link_offline_sale')  : null;
                $settings->link_return          = $form->getValue('is_link_return')         ? $form->getValue('link_return')        : null;

                $settings->update(['link_new', 'link_online_sale', 'link_offline_sale', 'link_return']);
            }

            echo $form;
        }
    }

    function apiAction(){
        $this->setTitle('Synchronization by API');
        echo new View_Page_ComingSoon();
    }


}