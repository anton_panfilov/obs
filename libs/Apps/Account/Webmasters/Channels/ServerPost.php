<?php

class Apps_Account_Webmasters_Channels_ServerPost extends Controller_Abstract {
    /**
     * @var Webmaster_ServerPostChannel_Object
     */
    protected $channel;

    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/channels');

        // проверка по ID
        if(isset($_GET['id'])){
            $this->channel = Webmaster_ServerPostChannel_Object::getObject($_GET['id']);
        }

        if($this->channel->id){
            // проверка на права доступа
            if(
                UserAccess::isAdmin() ||
                (
                    UserAccess::isAgentWebmaster() &&
                    Webmaster_Object::getObject($this->channel->webmaster)->id &&
                    Webmaster_Object::getObject($this->channel->webmaster)->getAgentID() == Users::getCurrent()->id
                )
                ||
                (
                    UserAccess::isWebmaster() && $this->channel->webmaster == Users::getCurrent()->company_id
                )
            ){
                $this->setHTMLTitle("Server Post Channel: {$this->channel->label} ({$this->channel->id})");
            }
            else {
                $this->setFollow_Error403();
            }
        }
        else {
            $this->setFollow_Error404();
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add(
            'Status',
            (new Webmaster_ServerPostChannel_Status())
                ->setInLine()
                ->render($this->channel->getStatus())
        );
        $preview->add('ID',         $this->channel->id);
        $preview->add('Label',      $this->channel->label);
        $preview->add('Product',    Cache_PartialList_ProductLabel::get($this->channel->product));

        if(!UserAccess::isWebmaster()){
            $preview->add(
                'Webmaster',
                "<a href='/account/webmasters/webmaster/?id={$this->channel->webmaster}'>" .
                    Cache_PartialList_WebmasterName::get($this->channel->webmaster) .
                "</a>"
            );
        }

        if(UserAccess::isAdmin()){
            $preview->add(
                'Agent',
                "<a href='/account/users/user/webmaster-agent?id=" .
                    Webmaster_Object::getObject($this->channel->webmaster)->getAgentID() .
                "'>" .
                    Cache_PartialList_UserLogin::get(Webmaster_Object::getObject($this->channel->webmaster)->getAgentID()) .
                "</a>"
            );
        }

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $id = $this->channel->id;

        $menu->add('Channel Profile',   "/account/webmasters/channels/server-post/?id={$id}",              'fa-file-text-o');
        $menu->add('Documentation',     "/account/webmasters/channels/server-post/documentation?id={$id}", 'fa-file');
        $menu->add('Test Leads',        "/account/webmasters/channels/server-post/test-leads?id={$id}",    'fa-database');

        /*********************************
         * breadcrumbs
         */

        $this->addBreadcrumbElement('Channels',             "/account/webmasters/channels/", 'icon-road');

        if(UserAccess::isWebmaster()){
            $this->addBreadcrumbElement(
                'Server Post Channels',
                "/account/webmasters/channels/server-post-channels",
                null,
                [
                    [
                        Cache_PartialList_ProductLabel::get($this->channel->product),
                        "/account/webmasters/channels/server-post-channels?product=" . $this->channel->product
                    ],
                ]
            );
        }
        else {
            $this->addBreadcrumbElement(
                'Server Post Channels',
                "/account/webmasters/channels/server-post-channels",
                null,
                [
                    [
                        Webmaster_Object::getObject($this->channel->webmaster)->name,
                        "/account/webmasters/channels/server-post-channels?webmaster=" . $this->channel->webmaster
                    ],
                    [
                        Cache_PartialList_ProductLabel::get($this->channel->product),
                        "/account/webmasters/channels/server-post-channels?product=" . $this->channel->product
                    ],
                ]
            );
        }

        $this->addBreadcrumbElement(
            "{$this->channel->label}",
            "/account/webmasters/channels/server-post/?id=" . $this->channel->id
        );


        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    function indexAction(){
        $this->setPageTitle("Server Post Channel Profile");

        $form = new Form();
        $form->addButton('Save');

        $form->addElementStatic('Product', '<b>' . Cache_PartialList_ProductLabel::get($this->channel->product) . '</b>');

        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            $form->addElementRadio('status', 'Status', $this->channel->getNextStatuses())
                ->setRenderTypeBigValue()
                ->setValue($this->channel->getStatus());

            $form->addElementTextarea("status_reason", 'Reason')
                ->setRenderTypeBigValue();

            $form->addConditionalFileds_NotArray('status', $this->channel->getStatus(), ['status_reason']);

        }
        else {
            $form->addElementStatic(
                'Status',
                (new Webmaster_ServerPostChannel_Status())
                    ->setInLine()
                    ->render($this->channel->getStatus()) .
                (
                    $this->channel->getStatus() == Webmaster_ServerPostChannel_Status::ACTIVE ?
                    "" : " - " . Translate::t("Contact your <a href='/account/'>account representative</a> to activate this channel")
                )
            );
        }

        if(UserAccess::isAdmin()){
            $form->addElementRadio("full_response", "Full Response", [
                '0' => 'No',
                '1' => 'Yes',
            ])
                ->setComment("Дополнительная информация при ответе. Нужна для более сложной интеграции, например с t3leads.com")
                ->setValue($this->channel->full_response);
        }


        $form->addElementText('label', 'Label')
            ->addValidator(new Form_Validator_Length(1, 32))
            ->addValidator(
                new Form_Validator_UniqueInDatabase(
                    "SELECT id FROM `webmaster_channels_server_post` ".
                    "WHERE webmaster='{$this->channel->webmaster}' AND `id`!='{$this->channel->id}' AND label=?",
                    Db::processing(),
                    "You already use this name"
                )
            )
            ->setValue($this->channel->label);


        $form->addSeparator('Public access to information about this channel');
        $form->addElementStatic('',
            "
                <p>
                    <a href='/server-post-documentation/?key=" . $this->channel->getApiKey() . "'>
                        https://" . Conf::main()->domain_account . "/server-post-documentation/?key=" . $this->channel->getApiKey() . "
                    </a>
                </p>
                <p style='color:#777; font-size:90%'>
                    Anyone with this link has access to the information about this channel
                </p>
            "
        )
            ->setRenderTypeFullLine();

        if($form->isValidAndPost()){
            $update = [];

            if($this->channel->label != $form->getValue('label')){
                $this->channel->label = $form->getValue('label');
                $update[] = 'label';
            }

            // Если в форме есть это поле и оно не равно тому что в настройках
            if($form->getValue('full_response') !== null && $form->getValue('full_response') != $this->channel->full_response){
                $this->channel->full_response = $form->getValue('full_response');
                $update[] = 'full_response';
            }

            if(count($update)){
                $this->channel->update($update);
            }

            if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
                if($this->channel->getStatus() != $form->getValue('status')){
                    $this->channel->setStatus(
                        $form->getValue('status'),
                        $form->getValue('status_reason')
                    );

                    $form->getElement('status_reason')
                        ->setValue('');

                    // переделать записимость показывания комментария
                    $form->getElement("status")
                        ->setOptions($this->channel->getNextStatuses())
                        ->setValue($this->channel->getStatus());

                    $form->removeConditional("status");
                    $form->addConditionalFileds_NotArray('status', $this->channel->getStatus(), ['status_reason']);
                }
            }
        }

        echo $form->render();
    }

    function documentationAction(){
        $this->setPageTitle("API Documentation for channel `{$this->channel->label}`");

        echo "
            <div class='hidden-print' style='text-align: right; '>
                <a
                    href='/server-post-documentation/?key=" . $this->channel->getApiKey() . "'
                    class='btn btn-primary'
                >Get Public Link for this  Documentation</a>
            </div>
        ";

        echo $this->channel->renderDocumentation();
    }

    function testLeadsAction(){
        $this->setPageTitle("Test Leads");

        $select = Db::logs()->select("lead_data_head", ['id', 'status', 'create_date', 'region'])
            ->where("`channel`=?", $this->channel->id)
            ->where("`is_test`='1'")
            ->order("id desc");

        $table = new Table($select);
        $table->setPagingAuto(10, '_pageLeads');

        $table->addField_SetArray('status', 'Status', new Lead_Status());
        $table->addField_Date('create_date', 'Date');
        $table->addField_LeadID('id', 'Lead ID');
        $table->addField_LeadClickID('id');
        $table->addField_Region('region', 'Region');
        $table->addField_LeadEmail('id');
        $table->addField_LeadMobilePhone('id');

        echo "<br><h4 style='background: #DDD; padding: 10px'>Last Test Leads</h4><br>";
        echo $table->render();

        $errors = Db::logs()->fetchAll(
            "SELECT `create`,`description`, ip, runtime, `request`, `response` FROM `lead_getting_error` " .
            "WHERE channel=? and is_test=1 ORDER BY id DESC LIMIT 100",
            $this->channel->id
        );

        $table = new Table($errors);
        $table->setPagingAuto(10, '_pageErrors');

        //

        $table->addField_Date('create', 'Date')
            ->setWidthMin();
        $table->addField_Text('description', 'Description');
        $table->setDetailsField(function($error){
            $form = new Form();

            $form->addElementStatic('Create',
                (new Decorator_Date())->setDateFormatFixed('M d, Y, H:i:s')->render($error['create']));
            $form->addElementStatic('IP',               inet_ntop($error['ip']));
            $form->addElementStatic('Runtime',          round($error['runtime'], 4) . " <small>sec</small>");

            $form->addSeparator('Request');
            $form->addElementStatic(
                '',
                String_Highlight::render(
                    String_Highlight::jsonPrettyPrint(
                        $error['request']
                    ), 0
                )
            )->setRenderTypeFullLine();

            $form->addSeparator('Response');
            $form->addElementStatic(
                '',
                String_Highlight::render(
                    String_Highlight::jsonPrettyPrint(
                        $error['response']
                    ), 0
                )
            )->setRenderTypeFullLine();

            return $form->render();
        });


        echo "<br><br><h4 style='background: #DDD; padding: 10px'>Last Errors</h4><br>";
        echo $table->render();

    }
}