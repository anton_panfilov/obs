<?php

class Apps_Account_Webmasters_Channels_Index extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('account/channels');

        // Хлебные крошки
        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            $this->addBreadcrumbElement('Webmasters',   "/account/webmasters/");
        }

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('All Channels',     "/account/webmasters/channels/");
        }
    }

    function postLoad(){
        $menu = new View_Panel_Menu();

        $menu->add('Landing Pages',   '/account/webmasters/channels/public-websites',    'fa-globe');
        $menu->add('Banners',           '/account/webmasters/channels/banners',            'fa-picture-o');
        $menu->add('JavaScript Forms',  '/account/webmasters/channels/javascript-forms',   'fa-list-alt');
        $menu->add('Thank You Pages',  '/account/webmasters/channels/tnx',   'fa-list-alt');
        $menu->addGroup('Server Post API',  'fa-briefcase')
            ->add("Channels List",      '/account/webmasters/channels/server-post-channels')
            ->add("Create New Channel", '/account/webmasters/channels/server-post-create');
        $menu->add('Offers',  '/account/webmasters/channels/offers',   'fa-list-alt');

        $this->addToSidebar(
            $menu->render()
        );
    }

    function indexAction(){
        $this->setTitle('Lead Channels');
        echo (new View_Page_Channels_Main())->render();
    }

    function publicWebsitesAction(){
        $this->setTitle('Landing Pages');

        if(isset($_GET['id']) && $_GET['id'] > 0){
            if(Webmaster_Channel_PublicFeed_Object::getObject($_GET['id'])->id){
                // Если ты админ или вебмастер агент или фид активный то показать
                if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || Webmaster_Channel_PublicFeed_Object::getObject($_GET['id'])->isActive()){
                    // добавление названия фида в хлебные крошки
                    $this->setTitle(Webmaster_Channel_PublicFeed_Object::getObject($_GET['id'])->title);
                    $this->addBreadcrumbElement('Landing Pages',      "/account/webmasters/channels/public-websites");

                    // отрисовка страницы фида
                    echo View_Page_Channels_PublicFeeds::renderOneFeed(Webmaster_Channel_PublicFeed_Object::getObject($_GET['id']));
                    return;
                }
                else {
                    $this->setFollow_Error403();
                }
            }
            else {
                $this->setFollow_Error404();
            }
        }

        echo View_Page_Channels_PublicFeeds::renderList();

    }

    function bannersAction(){
        $this->setTitle('Banners');

        if(isset($_GET['id']) && $_GET['id'] > 0){
            if(Webmaster_Channel_Banner_Object::getObject($_GET['id'])->id){
                // Если ты админ или вебмастер агент или фид активный то показать
                if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || Webmaster_Channel_Banner_Object::getObject($_GET['id'])->isActive()){
                    // добавление названия фида в хлебные крошки
                    $this->setTitle(Translate::t("Banner") . " - " . Webmaster_Channel_Banner_Object::getObject($_GET['id'])->title);
                    $this->addBreadcrumbElement('Banners',      "/account/webmasters/channels/banners");

                    // отрисовка страницы фида
                    echo View_Page_Channels_Banners::renderOneFeed(Webmaster_Channel_Banner_Object::getObject($_GET['id']));
                    return;
                }
                else {
                    $this->setFollow_Error403();
                }
            }
            else {
                $this->setFollow_Error404();
            }
        }

        echo View_Page_Channels_Banners::renderList();

    }

    function offersAction() {
        $this->setTitle('Offers');

        if(isset($_GET['id']) && $_GET['id'] > 0) {
            $offer = Buyer_Offers_Object::getObject($_GET['id']);
            if ($offer->id) {
                $this->setTitle($offer->title);
                $this->addBreadcrumbElement('Offers',      "/account/webmasters/channels/offers");
                $wmid = null;
                if (UserAccess::isWebmaster()) {
                    $wmid = Users::getCurrent()->company_id;
                }
                echo View_Page_Channels_Offers::renderView($offer, $wmid);
            }
        } else {
            // list
            echo View_Page_Channels_Offers::renderList();
        }
    }

    function javascriptFormsAction(){
        $this->setTitle('JavaScript Forms');

        if(isset($_GET['id']) && $_GET['id'] > 0){
            if(FormServer_Api::isTemplate($_GET['id'])){
                // добавление названия фида в хлебные крошки
                $this->setTitle(FormServer_Api::getTemplate($_GET['id'])['label']);
                $this->addBreadcrumbElement('JavaScript Forms',     "/account/webmasters/channels/javascript-forms");

                // отрисовка страницы фида
                echo View_Page_Channels_JSForm::renderOneFeed((int)$_GET['id']);
                return;

            }
            else {
                $this->setFollow_Error404();
            }
        }
        echo View_Page_Channels_JSForm::renderList();
    }

    function serverPostCreateAction(){
        $this->setTitle('Create Server Post Channel');
        $this->addBreadcrumbElement('Server Post Channels', "/account/webmasters/channels/server-post-channels");

        $form = new Form();
        $form->addButton('Create');

        if(!UserAccess::isWebmaster()){
            $form->addElement_Webmaster('webmaster');

            $wmTemp = (isset($_POST['webmaster']) ? (int)$_POST['webmaster'] : 0);
        }
        else {
            $wmTemp = (int)Users::getCurrent()->company_id;
        }

        $form->addElementText('label', 'Channel Name')
            ->addValidator(new Form_Validator_Length(1, 32))
            ->addValidator(new Form_Validator_UniqueInDatabase(
                "select id from webmaster_channels_server_post where webmaster='{$wmTemp}' and label=?",
                Db::processing(),
                "You already use this name"
            ));

        $form->addElement_MarketsSelect("market", "Market", "...");

        $marketsCodes = [];
        foreach(Lead_Market::getMarkets() as $el){
            $form->addElement_ProductForMarket($el['code'], "product_{$el['code']}", "...");
            $form->addConditionalFileds_Array("market", $el['code'], ["product_{$el['code']}"]);
            $marketsCodes[] = $el['code'];
        }

        if($form->isValidAndPost()){
            $channel = new Webmaster_ServerPostChannel_Object();

            $channel->webmaster = UserAccess::isWebmaster() ?
                Users::getCurrent()->company_id : $form->getValue('webmaster');

            $channel->label     = $form->getValue("label");
            $channel->product   = $form->getValue('product_' . $form->getValue('market'));
            $channel->market    = $form->getValue('market');

            $channel->insert();

            header('location: /account/webmasters/channels/server-post/?id=' . $channel->id);
        }

        echo $form->render();
    }

    function serverPostChannelsAction(){
        if(UserAccess::isWebmaster()){
            $count = Db::processing()->fetchOne(
                "select count(*) from webmaster_channels_server_post where webmaster=?",
                Users::getCurrent()->company_id
            );

            if($count == 0){
                header("location: /account/webmasters/channels/server-post-create");
                return;
            }
        }

        $this->setTitle('Server Post Channels');

        $form = new Form();
        $form->setMethodGET();

        if(!UserAccess::isWebmaster()){
            $form->addElement_Webmaster('webmaster');
        }

        $form->addElement_MarketsSelect("market", "Market", "All");

        $marketsCodes = [];
        foreach(Lead_Market::getMarkets() as $el){
            $form->addElement_ProductForMarket($el['code'], "product_{$el['code']}", "All");
            $form->addConditionalFileds_Array("market", $el['code'], ["product_{$el['code']}"]);
            $marketsCodes[] = $el['code'];
        }

        // $form->addElement_Product('product', 'All');
        $form->addElementSelect('status', 'Channel Status', (new Webmaster_ServerPostChannel_Status())->getArrayToSelect('All'));


        $form->isValidNotErrors();


        $select = Db::processing()->select("webmaster_channels_server_post")
            ->order('id desc');

        if(UserAccess::isWebmaster()){
            $select->where("webmaster=?", Users::getCurrent()->company_id);
        }
        else {
            if($form->getValue('webmaster')){
                $select->where("webmaster=?", $form->getValue('webmaster'));
            }

            // агоентам тольок их вебмастров
            if(UserAccess::isAgentWebmaster()){
                $webmasters = [];
                //$webmasters = User_WebmasterAgents::getWebmastersIDS(Users::getCurrent()->id);

                if(count($webmasters))  $select->where("`webmaster` in (" . implode(",", $webmasters) . ")");
                else                    $select->where("1=0");
            }
        }

        if($form->getValue('status'))   $select->where("status=?", $form->getValue('status'));
        if($form->getValue('market')){
            $select->where("market=?", $form->getValue('market'));

            if($form->getValue('product_' . $form->getValue('market'))){
                $select->where("product=?", $form->getValue('product_' . $form->getValue('market')));
            }
        }

        $table = new Table($select);

        // id	key	create	status	label	webmaster	market	product	test_good	test_error	full_response

        $table->addField_MarketFlag('market');

        $table->addField_Text('id', 'ID')
            ->setWidthMin()
            ->setLink('/account/webmasters/channels/server-post/?id={id}');

        $table->addField_SetArray(
            'status',
            'Status',
            new Webmaster_ServerPostChannel_Status()
        );

        $table->addField_Text('label', 'Label')
            ->setLink('/account/webmasters/channels/server-post/?id={id}');

        $table->addField_ProductLabel('product', 'Product')
            ->addDecoratorFunction(function($v, $c){
                return strtoupper(Lead_Market::getShortCode($c['market'])) . " - " . $v;
            });;

        ///////////////////////////////////
        if(!UserAccess::isWebmaster()){
            $table->addField_WebmasterName('webmaster', 'Webmaster');
        }

        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            $table->addField_WebmasterAgentLogin('webmaster');
        }

        ///////////////////////////////////
        $table->addField_Pattern(
            "<a href='/server-post-documentation/?key={id}-{webmaster}-{product}-{key}'><i class='fa fa-file-text'></i></a>",
            "Documentation"
        )
            ->setTextAlignCenter()
            ->setWidthMin();

        ///////////////////////////////////
        $table->startGroup('Test Leads');

        $table->addField_Date('test_good', 'Good')
            ->setLink('/account/webmasters/channels/server-post/test-leads?id={id}')
            ->setTextAlignCenter();

        $table->addField_Date('test_error', 'Error')
            ->setLink('/account/webmasters/channels/server-post/test-leads?id={id}')
            ->setTextAlignCenter();

        $table->endGroup();


        ///////////////////////////////////
        echo $form->render() . $table->render();
    }

    public function tnxAction(){
        $this->setTitle("Thank You Pages");

        $select = Db::processing()->select("leads_products", ['id', 'label', 'market']);

        //$data = Db::processing()->fetchAll('select leads_products.id, market, label as label, url as url
        //                                   from leads_products left join tnx_rules on leads_products.id=tnx_rules.id');
        // $data = $select->fetch_all(MYSQL_ASSOC);

        $table = new Table($select);

        $table->addField_MarketFlag("market");

        $table->addField_ProductLabel('id', 'Product')
            ->setLink("tnx-edit?id={id}");

        $table->addField_Cache(new Cache_PartialList_TnxURL(), "id", "URL", 'url')
            ->addDecoratorFunction(function($v){
                if(strlen($v)){
                    return "<a href='{$v}'>{$v}</a>";
                }

                return "<small class='txt-color-blueLight'>Not set</small>";
            });

        echo $table->render();

    }

    public function tnxEditAction(){
        if(isset($_GET['id'])){
            $product_label = Cache_PartialList_ProductLabel::get($_GET['id']);
            $rule = Tnx_Rule::getByProductId($_GET['id']);

            if(!$rule){
                $rule = new Tnx_Rule();
                $rule->url = '';
                $rule->product_id = $_GET['id'];
                $rule->type = 'IN_PLACE';
            }

            $form = new Form('Edit Thank You Page Action');
            $form->setMethodPOST();

            $form->addElementStatic('Product', $product_label);
            $form->addElementText_Url('url', 'URL', $macroses = [],  $allow_self_domain = true);
            $form->addElementSelect('type', 'Type', Tnx_Rule::getPossibleTypes());
            $form->setValue('url', $rule->url);
            $form->setValue('type', $rule->type);
            if($form->isValidAndPost()){
                $rule->url = $form->getValue('url');
                $rule->type = $form->getValue('type');
                if($rule->id){
                    $rule->update();
                }else{
                    $rule->insert();
                }
            }

            echo $form->render();
        }

    }
}