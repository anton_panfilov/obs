<?php

class Apps_Account_Orders_Order extends Controller_Abstract {
    /**
     * @var Order_Item
     */
    protected $order;

    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/orders');


        // проверка по ID
        if(isset($_GET['id'])){
            $this->order = Order_Item::getObject($_GET['id']);
        }

        if($this->order->id){
            $this->setTitle(Translate::t("title_orderDefault", ['id' => $this->order->id]));
        }
        else {
            $this->setFollow_Error404();
        }
    }

    function postLoad(){
        $id = $this->order->id;

        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add('preview_id',      $this->order->id);
        $preview->add('preview_status',  (new Order_Status())->setInLine()->render($this->order->status));

        if(isset($this->order->client_data['name']) && mb_strlen($this->order->client_data['name'])){
            $preview->add(
                'preview_name',
                "<a href='/account/orders/order/client?id={$id}'>" . $this->order->client_data['name'] . "</a>"
            );
        }

        if(isset($this->order->client_data['mobile_phone']) && mb_strlen($this->order->client_data['mobile_phone'])){
            $preview->add('preview_mobile_phone', $this->order->client_data['mobile_phone']);
        }

        if(isset($this->order->delivery_type)){
            $preview->add('preview_delivery_type', (new Order_DeliveryType())->setInLine()->render($this->order->delivery_type));
        }

        $preview->add('preview_count',   number_format($this->order->count) . " <small>" . Translate::t("count_short") . "</small>");
        $preview->add('preview_price',   number_format($this->order->price) . " <small>" . Translate::t("cur_RUR") . "</small>");

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();



        $menu->add('leftMenu_orderMain',         "/account/orders/order/?id={$id}",         'fa-file-text');
        $menu->add('leftMenu_orderClient',       "/account/orders/order/client?id={$id}",   'fa-user');
        $menu->add('leftMenu_orderGoods',        "/account/orders/order/goods?id={$id}",    'fa-shopping-cart');
        $menu->add('leftMenu_orderDelivery',     "/account/orders/order/delivery?id={$id}", 'fa-truck');

        /*********************************
         * breadcrumbs
         */

        $this->addBreadcrumbElement(
            'breadcrumb_ordersList',
            "/account/orders/",
            'icon-list'
        );

        $this->addBreadcrumbElement(
            Translate::t("breadcrumb_order", ['id' => $this->order->id]),
            "/account/orders/order/?id=" . $this->order->id
        );

        ///////////////////////////////////

        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    function indexAction(){
        $this->setTitle("title_main");

        echo $this->order->renderFormMain();
    }

    function clientAction(){
        $this->setTitle("title_client");

        echo $this->order->renderFormClient();
    }

    function goodsAction(){
        $this->setTitle("title_goods");

        echo $this->order->renderFormGoods();
    }

    function deliveryAction(){
        $this->setTitle("title_delivery");

        echo $this->order->renderFormDelivery();
    }
}