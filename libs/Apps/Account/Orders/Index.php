<?php

class Apps_Account_Orders_Index extends Controller_Abstract {
    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/orders');

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('breadcrumb_orders', "/account/orders/");
        }
    }

    function postLoad(){
        $menu = new View_Panel_Menu();

        if(UserAccess::isAdmin() || UserAccess::isClientManager()){
            $menu->add('leftMenu_createOrder', '/account/orders/create', 'fa-plus');
        }

        $menu->add('leftMenu_ordersList', '/account/orders/', 'fa-list');

        $this->addToSidebar(
            $menu->render()
        );
    }

    /**************************************************************************************/

    /**
     * Список постингов
     */
    function indexAction(){
        $this->setTitle('title_ordersList');

        $form = new Form();
        $form->setMethodGET();

        $form->addElementDateRange("date")
            ->setValue([
                'from' => date("Y-m-d", strtotime("now - 1year")),
                'till' => date("Y-m-d"),
            ])
            ->setButtonsMonths();

        $manager = $form->addElement_ClientManager("manager");

        if(UserAccess::isClientManager()){
            $manager->setValue(Users::getCurrent()->id);
        }


        $form->isValidNotErrors();

        ////////////////////////////////
        $select = Db::site()->select(
            "orders_list"
        )
            ->where(
                "`date` between ? and ?",
                $form->getValue("date")['from'] . " 00:00:00",
                $form->getValue("date")['till'] . " 23:59:59"
            )
            ->order("id desc");

        // if($form->getValue('status'))   $select->where("`status`=?", $form->getValue('status'));

        $tabs = new Tabs_StaticTabs();
        $tabs->setValueName("viewType");
        if(isset($_GET['viewType'])) {
            $form->addElementHidden("viewType", $_GET['viewType']);
        }

        $tabs->addTab("list", "tab_list");

        $method = "indexAction_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs->setText($this->$method($form, $select));
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $form->render() . $tabs->render();
    }

    function indexAction_list($form, $select){
        $table = new Table($select);

        // id	status	key	create	date	manager	client_id	client_data	client_comment	manager_comment	delivery_type	delivery_data

        $table->addField_Text("id", "tbl_id")
            ->setLink("/account/orders/order/?id={id}");

        $table->addField_SetArray("status", "tbl_status", new Order_Status);
        $table->addField_UserLogin("manager", "tbl_manager");
        $table->addField_Date("date", "tbl_date");
        $table->addField_Text("client_data", "tbl_client_data")->addDecoratorFunction(function($v){
            $v = Json::decode($v);

            $els = ['name', 'mobile_phone'];
            $r = [];
            foreach($els as $el){
                if(isset($v[$el]) && strlen($v[$el])){
                    $r[] = $v[$el];
                }
            }

            return count($r) ? implode(", ", $r) : "-";
        });

        $table->addField_SetArray("delivery_type", "tbl_delivery_type", new Order_DeliveryType);

        $table->addField_Text("count", "tbl_count")
            ->setLink("/account/orders/order/goods?id={id}")
            ->setTextAlignCenter();

        $table->addField_Text("price", "tbl_price")
            ->setTextBold()
            ->setTextAlignCenter()
            ->addDecoratorFunction(function($v){
                return round($v + 0, 2);
            });

        return $table->render();
    }

    /*******************************************************************************************/

    public function createAction(){
        $this->setTitle('title_createOrder');
        echo (new Order_Item())->renderFormClient();
    }
}