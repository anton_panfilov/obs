<?php

class Apps_Account_Reports_Webmaster extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('account/reports');
    }

    function postLoad(){
        $menu = new View_Panel_Menu();

        $add = "";
        if(UserAccess::isWebmaster()){
            $add = "?webmaster=" . Users::getCurrent()->company_id;
        }

        $el = $menu->addGroup('Leads Main Reports', 'fa-bar-chart-o');
        $el->add('Daily Summary',    '/account/reports/webmaster/'                  . $add);
        $el->add('Hourly Summary',   '/account/reports/webmaster/summary-hourly'    . $add);
        $el->add('Leads Details',    '/account/reports/webmaster/leads'             . $add);
        $el->add('Calls Details',    '/account/reports/webmaster/calls'             . $add);
        $el->add('Cost per Actions', '/account/reports/webmaster/cpa'               . $add);
        $el->add('Leads Returns',    '/account/reports/webmaster/returns'           . $add);
        $el->add('Errors',           '/account/reports/webmaster/leads-errors'      . $add);

        /**************/

        $menu->addGroup('Performance', 'fa-list-ol')
            ->add('Top List', "/account/reports/webmaster/top-list"     . $add)
            ->add('Dynamic', "/account/reports/webmaster/dynamic"       . $add);

        if(T3Leads_V2::isShow()){
            $menu->addGroup('T3<small class="txt-color-blueLight">.v2</small> Main Reports', 'fa-list')
                ->add('Daily Summary',     "/account/reports/webmaster/v2-summary"     . $add)
                ->add('Leads Details',     "/account/reports/webmaster/v2-leads"       . $add);
        }


        /**************/

        $this->addToSidebar(
            $menu->render()
        );


        /*********************************
         * breadcrumbs
         */

        $this->addBreadcrumbElement(
            'Reports',
            UserAccess::isAdmin() ? "/account/reports/" : "",
            ' icon-hdd'
        );

        $this->addBreadcrumbElement(
            "Webmaster Reports"
        );
    }

    /**************************************************************/

    function getMainReportForm(){
        $form = new Form();
        $form->setMethodGET();

        // кнокпи
        $buttons = [];

        $buttons['index']           = ['action' => '',                  'label' => 'Daily Summary'];
        if($this->_action == 'summaryHourly'){
            $buttons['summaryHourly']   = ['action' => 'summary-hourly',    'label' => 'Hourly Summary'];
        }
        $buttons['leads']           = ['action' => 'leads',             'label' => 'Leads Details'];
        $buttons['calls']           = ['action' => 'calls',             'label' => 'Calls Details'];
        $buttons['cpa']             = ['action' => 'cpa',               'label' => 'Cost per Actions'];
        $buttons['returns']         = ['action' => 'returns',           'label' => 'Leads Returns'];
        $buttons['leadsErrors']     = ['action' => 'leads-errors',      'label' => 'Errors'];

        foreach($buttons as $id => $btn){
            $form->addButton(
                $btn['label'],
                $id,
                ($this->_action == $id)
            )
                ->setAttrib('name', '')
                ->setScript(
                    "jQuery(this).closest('form').attr('action', '/account/reports/webmaster/{$btn['action']}');"
                );
        }

        $form->addElementDateRange('date')
            ->setButtonsMonths();

        $form->addElement_ProductForMarket(Db::getDefaultMarket(), 'product',   'All...')
            ->getAttribs()->setCSS("width", '200px');

        $form->addElement_Region('region',   'Region')
            ->getAttribs()->setCSS("width", '200px');

        if(UserAccess::isAdmin()){
            $form->addElement_WebmasterAgent('agent', 'Agent')
                ->setRequired(false);
        }

        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
            $form->addElement_Webmaster('webmaster');
        }

        $form->addElementSelect(
            'channel_type',
            'Channel Type',
            (new Lead_ChannelType())->getArrayToSelect('All')
        )
            ->getAttribs()->setCSS("width", '250px');

        $form->addElement_WebmasterDomain('domain')
            ->setRequired(false)
            ->setWidth("300px");

        return $form;
    }

    function getMainReportForm_modify(Form $form){
        try{
            if($form->getElement('product') && $form->getElement('region')){
                $form->addMultiElement(['product', 'region'], 'Product', 'region_and_product')
                    ->setTemplate([
                        '{product}', "<span style='padding: 0 20px 0 117px'>" . Translate::t("Region") . "</span>", '{region}'
                    ])
                    ->setRenderTypeBigValue();
            }
        }
        catch(Exception $e){}

        try{
            if($form->getElement('agent') && $form->getElement('webmaster')){
                $form->addMultiElement(['agent', 'webmaster'], 'Agent', 'agent_and_webmaster')
                    ->setTemplate([
                        '{agent}', "<span style='padding: 0 20px 0 50px'>" . Translate::t("Webmaster") . "</span>", '{webmaster}'
                    ])
                    ->setRenderTypeBigValue();

                $form->getElement('agent')->setWidth("235px");
                $form->getElement('webmaster')->setWidth("300px");
            }
        }
        catch(Exception $e){}

        try{
            if($form->getElement('channel_type') && $form->getElement('domain')){
                $form->addMultiElement(['channel_type', 'domain'], 'Channel Type', 'channel_type_and_domain')
                    ->setTemplate([
                        '{channel_type}', "<span style='padding: 0 20px 0 54px'>" . Translate::t("Domain") . "</span>", '{domain}'
                    ])
                    ->setRenderTypeBigValue();
            }
        }
        catch(Exception $e){
            try{
                if($form->getElement('domain')){
                    $form->addMultiElement(['domain'], 'Domain');
                }
            }
            catch(Exception $e){}
        }

        try{
            if($form->getElement('status')){
                $form->addMultiElement(['status'], 'Lead Status', 'lead_status_only')
                    ->setTemplate([
                        '{status}'
                    ])
                    ->setRenderTypeBigValue();
            }
        }
        catch(Exception $e){
        }


        return $form;
    }

    function setTableFields(Table $table, $total, Form $form){
        $table->startGroup('Leads');

        $table->addField_Text('leads_all', "All")
            ->setDescription("Total number of leads generated, including test leads.")
            ->setTextAlignCenter();

        if($total['leads_test']){
            $table->addField_Text('leads_test', 'Test')
                ->setDescription(
                    "Number of test leads sent. If the lead has the word “test” in it,
                    it will be sent as a test lead."
                )
                ->setTextAlignCenter();
        }

        if(!UserAccess::isWebmaster()){
            if($total['leads_fraud']){
                $table->addField_Text('leads_fraud', 'Fraud')
                    ->setDescription(
                        "These leads were not sold because the system
                        determined that they are fraudulent or unnecessary."
                    )
                    ->setTextAlignCenter();
            }
        }

        $table->addField_Text('leads_sold', 'Sold')
            ->setDescription("Number of leads sold.")
            ->setTextAlignCenter();

        if($total['leads_return']){
            $table->addField_Text('leads_return', 'Return')
                ->setDescription("Number of leads return.")
                ->setTextAlignCenter();
        }

        $table->addField_Text('leads_errors', 'Error')
            ->setDescription("Leads with invalid information, leads from inactive accounts or channels, etc.")
            ->setTextAlignCenter();

        if($total['leads_fatal_errors']){

            $table->addField_Text('leads_fatal_errors', 'Fatal Error')
                ->setDescription("Number of leads declined.")
                ->setTextAlignCenter();
        }

        // $table->addField_Text('convert',            'Convert')      ->setTextAlignCenter();

        $table->endGroup();


        if($total['leads_pending_all'] > 0){
            $table->startGroup('Cost per Actions');

            $table->addField_Text('leads_pending_all', 'All')
                ->setDescription("Number of leads pending.")
                ->setTextAlignCenter();

            if($total['leads_pending_sold'] > 0){
                $table->addField_Text('leads_pending_sold', 'Sold')
                    ->setTextAlignCenter();
            }

            if($total['leads_pending_reject'] > 0){
                $table->addField_Text('leads_pending_reject', 'Reject')
                    ->setTextAlignCenter();
            }

            if($total['leads_pending_wait'] > 0){
                $table->addField_Text('leads_pending_wait', 'Wait')
                    ->setTextAlignCenter();
            }

            $table->endGroup();
        }

        if(UserAccess::isAdmin()){
            $table->startGroup('Distribution');

            $table->addField_Text('distribution_wm', 'WM')
                ->setWidthMin()
                ->setTextAlignCenter();

            if($total['distribution_pool'] > 0){
                $table->addField_Text('distribution_pool', 'POOL')
                    ->setWidthMin()
                    ->setTextAlignCenter();
            }

            if($total['distribution_costs'] > 0){
                $table->addField_Text('distribution_costs', 'COSTS')
                    ->setWidthMin()
                    ->setTextAlignCenter();
            }

            $table->addField_Text('distribution_ern', 'ERN')
                ->setWidthMin()
                ->setTextAlignCenter();

            $table->endGroup();

            $table->startGroup('xPL');

            $table->addField_Text('epl', 'EPL')
                ->setDescription("Webmaster Earnings per Lead")
                ->setTextAlignCenter()
                ->setTextColor('#333');

            $table->addField_Text('ipl',                'IPL')
                ->setDescription("Total Income per Lead")
                ->setTextAlignCenter()
                ->setTextColor('#333');

            $table->endGroup();
        }

        ////////////////////////////////////////////
        if(
            UserAccess::isWebmaster() ||
            UserAccess::isAgentWebmaster()
        ){
            $table->startGroup('Earnings');

            if(!(UserAccess::isAdmin())){
                $table->addField_Text('epl', 'EPL')
                    ->setDescription("Total revenue, including revenue from advertisements, earned from each lead.")
                    ->setTextAlignCenter()
                    ->setTextColor('#777');
            }

            $table->addField_Text('leads_money_wm', UserAccess::isWebmaster() ? 'Earnings' : 'Webmaster Earnings')
                ->setDescription("Revenue earned from leads.")
                ->setTextAlignCenter()
                ->setTextColor(
                    (UserAccess::isAdmin()) ? '#b94a48' : '#0A0'
                );

            $table->endGroup();
        }


        if(UserAccess::isAgentBuyer()){
            $table->startGroup('Income');

            $table->addField_Text('ipl', 'IPL')
                ->setDescription("Income per lead")
                ->setTextAlignCenter()
                ->setTextColor('#777');

            $table->addField_Text('leads_money_ttl', 'Income')
                ->setTextAlignCenter()
                ->setTextColor('#172');

            $table->endGroup();
        }

        if((UserAccess::isAdmin())){

            $table->startGroup('Earnings');

            $table->addField_Text('leads_money_wm',   'WM')
                ->setTextAlignCenter()
                ->setTextColor('#b94a48');

            if($total['leads_money_pool'] > 0){
                $table->addField_Text('leads_money_pool', 'POOL')
                    ->setTextAlignCenter()
                    ->setTextColor('#AAA')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }

            if($total['leads_money_costs'] > 0){
                $table->addField_Text('leads_money_costs', 'COSTS')
                    ->setTextAlignCenter()
                    ->setTextColor('#AAA')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }

            $table->addField_Text('leads_money_ern',  'ERN')
                ->setTextAlignCenter()
                ->setTextColor('#0A0')
                ->addDecoratorFunction(function($v){
                    return round($v, 2);
                });

            $table->addField_Text('leads_money_ttl',  'TTL')
                ->setTextAlignCenter()
                ->setTextColor('#333');

            $table->endGroup();
        }

        return $table;
    }

    /*****************************************/

    function indexAction(){
        $this->setTitle('Leads Daily Summary');

        $tabs = new Tabs_Markets();
        $tabs->setShowTimezone();
        Db::setDefaultMarket($tabs->getActiveTab());
        ob_start();

        Site::addCSS('reports/main-summary.css');

        $form = $this->getMainReportForm();
        $form = $this->getMainReportForm_modify($form);

        $form->isValidNotErrors();
        echo $form->render();

        /****************************/

        $all = Report_Webmaster_Summary::getSummaryDays(
            $form->getValue('date')['from'],
            $form->getValue('date')['till'],
            $form->getValue('product'),
            $form->getValue('webmaster'),
            $form->getValue('channel_type'),
            $form->getValue('channel'),
            $form->getValue('region'),
            $form->getValue('domain'),
            $form->getValue('agent')
        );

        /****************************/

        $pData = [];
        if(count($all['data'])){
            foreach($all['data'] as $el){
                $add = [
                    'flortTime'             => ((strtotime($el['date'] . " UTC"))*1000),
                    'leads_money_wm'        => isset($el['leads_money_wm']) ? $el['leads_money_wm'] : 0,
                    'epl'                   => round(isset($el['epl']) ? $el['epl'] : 0, 2),
                ];

                if(UserAccess::isAdmin()){
                    $add['total_ern'] = $el['leads_money_ern'];
                    $add['total_ttl'] = $el['leads_money_ttl'];
                    $add['ipl'] = round($el['ipl'], 2);
                }

                if(UserAccess::isAgentBuyer()){
                    $add['ipl'] = round($el['ipl'], 2);
                }

                $pData[] = $add;
            }
        }

        Site::addJS('/static/js/plugins/jquery.cookie.min.js');

        ?>
        <!--[if IE]>
            <script language="javascript" type="text/javascript" src="/static/plugins/flot/excanvas.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.js"></script>
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.selection.js"></script>
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.time.js"></script>
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.stack.js"></script>
        <script language="javascript" type="text/javascript" src="/static/js/reports/webmaster-summary.js"></script>
        <script>
            jQuery(function(){
                document.summaryReport = new reportSummary('<?=Users::getCurrent()->role?>');
                document.summaryReport.setData(<?=Json::encode($pData)?>);
                document.summaryReport.setPeriodType('days');
                document.summaryReport.renderPlot('summaryReport_Plot');

                jQuery(window).resize(function(){
                    document.summaryReport.renderPlot_Create();
                });
            });
        </script>
        <div id='summaryReport_Plot_pre'>
            <div id='summaryReport_Plot_Selecter'></div>
            <div id='summaryReport_Plot'></div>
            <center><div id='summaryReport_Plot_Overview'></div></center>
        </div>
        <?

        /****************************/

        $blank_style = 'color:#CCC';

        $data = $all['data'];

        $decorator = new Decorator_Date();
        $decorator->markWeekends(1);

        foreach($data as $k => $el){

            $data[$k]['leads_return'] = $el['leads_return'] ?
                $el['leads_return'] :
                "<span style='{$blank_style}'>{$el['leads_return']}</span>";

            if($el['leads_all']){
                $data[$k]['leads_all'] = "<a href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => null,
                    ], '/account/reports/webmaster/leads') . "'>{$el['leads_all']}</a>";
            }
            else {
                $data[$k]['leads_all'] = "<span style='{$blank_style}'>{$el['leads_all']}</span>";
            }

            if($el['leads_test']){
                $data[$k]['leads_test'] = "<a style='color:#A00' href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => 'test',
                    ], '/account/reports/webmaster/leads') . "'>{$el['leads_test']}</a>";
            }
            else {
                $data[$k]['leads_test'] = "<span style='{$blank_style}'>{$el['leads_test']}</span>";
            }

            if($el['leads_fraud']){
                $data[$k]['leads_fraud'] = "<a style='color:#A00' href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => 'fraud',
                    ], '/account/reports/webmaster/leads') . "'>{$el['leads_fraud']}</a>";
            }
            else {
                $data[$k]['leads_fraud'] = "<span style='{$blank_style}'>{$el['leads_fraud']}</span>";
            }

            if($el['leads_sold']){
                $data[$k]['leads_sold'] = "<a href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => Lead_Status::SOLD,
                    ], '/account/reports/webmaster/leads') . "'>{$el['leads_sold']}</a>";
            }
            else {
                $data[$k]['leads_sold'] = "<span style='{$blank_style}'>{$el['leads_sold']}</span>";
            }

            if($el['leads_pending_all']){
                $data[$k]['leads_pending_all'] = "<a href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => null,
                    ], '/account/reports/webmaster/cpa') . "'>{$el['leads_pending_all']}</a>";
            }
            else {
                $data[$k]['leads_pending_all'] = "<span style='{$blank_style}'>{$el['leads_pending_all']}</span>";
            }

            if($el['leads_pending_sold']){
                $data[$k]['leads_pending_sold'] = "<a href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => Lead_Pending_Status::SOLD,
                    ], '/account/reports/webmaster/cpa') . "'>{$el['leads_pending_sold']}</a>";
            }
            else {
                $data[$k]['leads_pending_sold'] = "<span style='{$blank_style}'>{$el['leads_pending_sold']}</span>";
            }

            if($el['leads_pending_reject']){
                $data[$k]['leads_pending_reject'] = "<a href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => Lead_Pending_Status::REJECT,
                    ], '/account/reports/webmaster/cpa') . "'>{$el['leads_pending_reject']}</a>";
            }
            else {
                $data[$k]['leads_pending_reject'] = "<span style='{$blank_style}'>{$el['leads_pending_reject']}</span>";
            }

            if($el['leads_pending_wait']){
                $data[$k]['leads_pending_wait'] = "<a href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'status' => Lead_Pending_Status::WAIT,
                    ], '/account/reports/webmaster/cpa') . "'>{$el['leads_pending_wait']}</a>";
            }
            else {
                $data[$k]['leads_pending_wait'] = "<span style='{$blank_style}'>{$el['leads_pending_wait']}</span>";
            }

            //////////////////////////////////////////

            if($el['leads_errors']){
                $data[$k]['leads_errors'] = "<a href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'errorStatus' => Lead_Status::DATA_ERROR,
                    ], '/account/reports/webmaster/leads-errors') . "'>{$el['leads_errors']}</a>";
            }
            else {
                $data[$k]['leads_errors'] = "<span style='{$blank_style}'>{$el['leads_errors']}</span>";
            }

            if($el['leads_fatal_errors']){
                $data[$k]['leads_fatal_errors'] = "<a style='font-weight: bold' href='" . Http_Url::convert([
                        'date' => [
                            'from' => $el['date'],
                            'till' => $el['date'],
                        ],
                        'errorStatus' => Lead_Status::FATAL_ERROR,
                    ], '/account/reports/webmaster/leads-errors') . "'>{$el['leads_fatal_errors']}</a>";
            }
            else {
                $data[$k]['leads_fatal_errors'] = "<span style='{$blank_style}'>{$el['leads_fatal_errors']}</span>";
            }

            $data[$k]['convert']  = is_null($el['convert'])   ? "<span style='{$blank_style}'>-</span>" : "{$el['convert']} %";

            if(isset($el['epl'])){
                $data[$k]['epl']      = is_null($el['epl'])       ? "<span style='{$blank_style}'>-</span>" : $el['epl'];
            }

            if(isset($el['ipl'])){
                $data[$k]['ipl']      = is_null($el['ipl'])       ? "<span style='{$blank_style}'>-</span>" : $el['ipl'];
            }

            $data[$k]['date'] = "<a style='text-decoration:none' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $el['date'],
                        'till' => $el['date'],
                    ],
                    'status' => null,
                ], '/account/reports/webmaster/summary-hourly') . "'>" . $decorator->render($el['date']) . "</a>";


            if(isset($el['distribution_wm'])){
                $data[$k]['distribution_wm']  = is_null($el['distribution_wm'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_wm']}</small>";
            }

            if(isset($el['distribution_wm'])){
                $data[$k]['distribution_pool']  = is_null($el['distribution_pool'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_pool']}</small>";
            }

            if(isset($el['distribution_wm'])){
                $data[$k]['distribution_costs']  = is_null($el['distribution_costs'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_costs']}</small>";
            }

            if(isset($el['distribution_wm'])){
                $data[$k]['distribution_ern']  = is_null($el['distribution_ern'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_ern']}</small>";
            }

        }

        /****************************/

        $table = new Table();
        $table->setPagingDisabled();
        $table->setData($data);

        $table->addField_Text('date',               'Date');
        $table = $this->setTableFields($table, $all['total'], $form);

        // суммарные данные
        if($all['total']['leads_all']){
            $all['total']['leads_all'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'status' => null,
                ], '/account/reports/webmaster/leads') . "'>{$all['total']['leads_all']}</a>";
        }

        if($all['total']['leads_sold']){
            $all['total']['leads_sold'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'status' => Lead_Status::SOLD,
                ], '/account/reports/webmaster/leads') . "'>{$all['total']['leads_sold']}</a>";
        }

        if($all['total']['leads_pending_all']){
            $all['total']['leads_pending_all'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'status' => Lead_Status::PENDING,
                ], '/account/reports/webmaster/leads') . "'>{$all['total']['leads_pending_all']}</a>";
        }

        if($all['total']['leads_errors']){
            $all['total']['leads_errors'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'errorStatus' => Lead_Status::DATA_ERROR,
                ], '/account/reports/webmaster/leads-errors') . "'>{$all['total']['leads_errors']}</a>";
        }

        if($all['total']['leads_fatal_errors']){
            $all['total']['leads_fatal_errors'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'errorStatus' => Lead_Status::FATAL_ERROR,
                ], '/account/reports/webmaster/leads-errors') . "'>{$all['total']['leads_fatal_errors']}</a>";
        }

        if(isset($all['total']['distribution_wm'])){
            $all['total']['distribution_wm']  = is_null($all['total']['distribution_wm'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_wm']}</small>";
        }

        if(isset($all['total']['distribution_pool'])){
            $all['total']['distribution_pool']  = is_null($all['total']['distribution_pool'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_pool']}</small>";
        }

        if(isset($all['total']['distribution_costs'])){
            $all['total']['distribution_costs']  = is_null($all['total']['distribution_costs'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_costs']}</small>";
        }

        if(isset($all['total']['distribution_ern'])){
            $all['total']['distribution_ern']  = is_null($all['total']['distribution_ern'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_ern']}</small>";
        }


        $table->addFooter($all['total']);

        echo $table->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');


        echo $tabs->setText(ob_get_clean())->render();
    }

    function summaryHourlyAction(){
        $this->setTitle('Leads Hourly Summary');

        $tabs = new Tabs_Markets();
        $tabs->setShowTimezone();
        Db::setDefaultMarket($tabs->getActiveTab());
        ob_start();

        Site::addCSS('reports/main-summary.css');

        $form = $this->getMainReportForm();
        $form->getElement('date')
            ->setValue([
                'from' => date('Y-m-d', strtotime('-1Day')),
                'till' => date('Y-m-d'),
            ]);
        $form = $this->getMainReportForm_modify($form);

        $form->isValidNotErrors();
        echo $form->render();

        /****************************/

        $all = Report_Webmaster_Summary::getSummaryHourly(
            $form->getValue('date')['from'],
            $form->getValue('date')['till'],
            $form->getValue('product'),
            $form->getValue('webmaster'),
            $form->getValue('channel_type'),
            $form->getValue('channel'),
            $form->getValue('region'),
            $form->getValue('domain'),
            $form->getValue('agent')
        );

        /****************************/

        $pData = [];
        if(count($all['data'])){
            foreach($all['data'] as $el){
                $add = [
                    'flortTime'             => ((strtotime($el['date'] . " UTC"))*1000),
                    'leads_money_wm'        => isset($el['leads_money_wm']) ? $el['leads_money_wm'] : 0,
                    'epl'                   => round(isset($el['epl']) ? $el['epl'] : 0, 2),
                ];

                if(UserAccess::isAdmin()){
                    $add['total_ern'] = $el['leads_money_ern'];
                    $add['total_ttl'] = $el['leads_money_ttl'];
                    $add['ipl'] = round($el['ipl'], 2);
                }

                if(UserAccess::isAgentBuyer()){
                    $add['ipl'] = round($el['ipl'], 2);
                }

                $pData[] = $add;
            }
        }

        Site::addJS('/static/js/plugins/jquery.cookie.min.js');

        ?>
        <!--[if IE]>
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/excanvas.js"></script>
        <![endif]-->
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.js"></script>
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.selection.js"></script>
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.time.js"></script>
        <script language="javascript" type="text/javascript" src="/static/plugins/flot/jquery.flot.stack.js"></script>
        <script language="javascript" type="text/javascript" src="/static/js/reports/webmaster-summary.js"></script>
        <script>
            jQuery(function(){
                document.summaryReport = new reportSummary('<?=Users::getCurrent()->role?>');
                document.summaryReport.setData(<?=Json::encode($pData)?>);
                document.summaryReport.setPeriodType('hours');
                document.summaryReport.renderPlot('summaryReport_Plot');

                jQuery(window).resize(function(){
                    document.summaryReport.renderPlot_Create();
                });
            });
        </script>
        <div id='summaryReport_Plot_pre'>
            <div id='summaryReport_Plot_Selecter'></div>
            <div id='summaryReport_Plot'></div>
            <center><div id='summaryReport_Plot_Overview'></div></center>
        </div>
        <?

        /****************************/

        $blank_style = 'color:#CCC';

        $data = $all['data'];

        $decorator = new Decorator_Date();
        $decorator->showTime(false);

        foreach($data as $k => $el){
            $data[$k]['date'] = "<span style='color:#666'>" . $decorator->render($el['date']) . ",</span> " .
                substr($el['date'], 11, 2) . "<span style='color:#CCC'>:00</span>";



            $data[$k]['leads_all'] = $el['leads_all'] ?
                $el['leads_all'] :
                "<span style='{$blank_style}'>{$el['leads_all']}</span>";

            $data[$k]['leads_test'] = $el['leads_test'] ?
                "<span style='color:#A00'>{$el['leads_test']}</span>" :
                "<span style='{$blank_style}'>{$el['leads_test']}</span>";

            $data[$k]['leads_fraud'] = $el['leads_fraud'] ?
                "<span style='color:#A00'>{$el['leads_fraud']}</span>" :
                "<span style='{$blank_style}'>{$el['leads_fraud']}</span>";

            $data[$k]['leads_sold'] = $el['leads_sold'] ?
                $el['leads_sold'] :
                "<span style='{$blank_style}'>{$el['leads_sold']}</span>";

            $data[$k]['leads_return'] = $el['leads_return'] ?
                $el['leads_return'] :
                "<span style='{$blank_style}'>{$el['leads_return']}</span>";

            $data[$k]['leads_errors'] = $el['leads_errors'] ?
                $el['leads_errors'] :
                "<span style='{$blank_style}'>{$el['leads_errors']}</span>";

            $data[$k]['leads_fatal_errors'] = $el['leads_fatal_errors'] ?
                $el['leads_fatal_errors'] :
                "<span style='{$blank_style}'>{$el['leads_fatal_errors']}</span>";

            $data[$k]['convert']  = is_null($el['convert'])   ? "<span style='{$blank_style}'>-</span>" : "{$el['convert']} %";

            if(isset($el['epl'])){
                $data[$k]['epl'] = is_null($el['epl']) ? "<span style='{$blank_style}'>-</span>" : $el['epl'];
            }

            if(isset($el['ipl'])){
                $data[$k]['ipl'] = is_null($el['ipl']) ? "<span style='{$blank_style}'>-</span>" : $el['ipl'];
            }


            if(isset($el['distribution_wm'])){
                $data[$k]['distribution_wm']  = is_null($el['distribution_wm'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_wm']}</small>";
            }

            if(isset($el['distribution_pool'])){
                $data[$k]['distribution_pool']  = is_null($el['distribution_pool'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_pool']}</small>";
            }

            if(isset($el['distribution_costs'])){
                $data[$k]['distribution_costs']  = is_null($el['distribution_costs'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_costs']}</small>";
            }

            if(isset($el['distribution_ern'])){
                $data[$k]['distribution_ern']  = is_null($el['distribution_ern'])   ?
                    "<span style='{$blank_style}'>-</span>" :
                    "<small style='color:#999'>{$el['distribution_ern']}</small>";
            }
        }

        /****************************/

        $table = new Table();
        $table->setPagingDisabled();
        $table->setData($data);

        $table->addField_Text('date', 'Date');
        $table = $this->setTableFields($table, $all['total'], $form);

        // суммарные данные
        if($all['total']['leads_all']){
            $all['total']['leads_all'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'status' => null,
                ], '/account/reports/webmaster/leads') . "'>{$all['total']['leads_all']}</a>";
        }

        if($all['total']['leads_sold']){
            $all['total']['leads_sold'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'status' => Lead_Status::SOLD,
                ], '/account/reports/webmaster/leads') . "'>{$all['total']['leads_sold']}</a>";
        }

        if($all['total']['leads_errors']){
            $all['total']['leads_errors'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'errorStatus' => Lead_Status::DATA_ERROR,
                ], '/account/reports/webmaster/leads-errors') . "'>{$all['total']['leads_errors']}</a>";
        }

        if($all['total']['leads_fatal_errors']){
            $all['total']['leads_fatal_errors'] = "<a style='color:#000' href='" . Http_Url::convert([
                    'date' => [
                        'from' => $form->getValue('date')['from'],
                        'till' => $form->getValue('date')['till'],
                    ],
                    'errorStatus' => Lead_Status::FATAL_ERROR,
                ], '/account/reports/webmaster/leads-errors') . "'>{$all['total']['leads_fatal_errors']}</a>";
        }

        if(isset($all['total']['distribution_wm'])){
            $all['total']['distribution_wm']  = is_null($all['total']['distribution_wm'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_wm']}</small>";
        }


        if(isset($all['total']['distribution_pool'])){
            $all['total']['distribution_pool']  = is_null($all['total']['distribution_pool'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_pool']}</small>";
        }

        if(isset($all['total']['distribution_costs'])){
            $all['total']['distribution_costs']  = is_null($all['total']['distribution_costs'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_costs']}</small>";
        }

        if(isset($all['total']['distribution_ern'])){
            $all['total']['distribution_ern']  = is_null($all['total']['distribution_ern'])   ?
                "" :
                "<small style='color:#999'>{$all['total']['distribution_ern']}</small>";
        }

        $table->addFooter($all['total']);

        echo $table->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');

        echo $tabs->setText(ob_get_clean())->render();
    }

    function leadsAction(){
        $this->setPageTitle('Leads Details');

        $tabs = new Tabs_Markets();
        $tabs->setShowTimezone();
        Db::setDefaultMarket($tabs->getActiveTab());
        ob_start();

        $form = $this->getMainReportForm();

        $statuses = [
            ''                          => 'All',
            Lead_Status::SOLD    => 'Sold',
            'test'                      => 'Test',
        ];

        if(!UserAccess::isWebmaster()){
            $statuses['fraud'] = 'Fraud';
        }

        $form->addElementSelect('status', 'Lead Status', $statuses)
            ->setRenderTypeBigValue()
            ->setRequired(0);

        $form = $this->getMainReportForm_modify($form);

        $form->isValidNotErrors();

        echo $form->render();

        if($form->getValue('status') == Lead_Status::SOLD){
            $field = 'leads_sold';
        }
        else if($form->getValue('status') == 'test'){
            $field = 'leads_test';
        }
        else if($form->getValue('status') == 'fraud'){
            $field = 'leads_fraud';
        }
        else {
            $field = 'leads_all';
        }

        $total = Report_Webmaster_Summary::getTotal(
            $form->getValue('date')['from'],
            $form->getValue('date')['till'],
            $form->getValue('product'),
            $form->getValue('webmaster'),
            $form->getValue('channel_type'),
            $form->getValue('channel'),
            $form->getValue('region'),
            $form->getValue('domain'),
            $form->getValue('agent')
        );


        $all_count = $total[$field];
        $paging = new Table_Paging();
        $paging->calculate($all_count);

        $all = [];
        if($all_count > 0){
            $select = Db::logs()
                ->select(
                    "lead_data_head", [
                        'id',
                        'status',
                        'num',
                        'webmaster',
                        'agent',
                        'channel_type',
                        'channel',
                        'create_date',
                        'region',
                        'webmaster',
                        'product',
                        'page',
                        'is_test',
                        'is_fraud',
                        'money_wm',
                        'money_pool',
                        'money_costs',
                        'money_ern',
                        'money_ttl',
                    ]
                )
                ->where(
                    '`create_date` between ? and ?',
                    $form->getValue('date')['from'],
                    $form->getValue('date')['till'] . " 23:59:59"
                )
                ->order('id desc')
                ->limit(
                    $paging->getLimitCount() + ($paging->isLastPage() ? 1000 : 0), // если это последняя страница, то получаем на 1000 лидов больше
                    $paging->getLimitOffset()
                );

            // тип лидов
            if($form->getValue('status') == Lead_Status::SOLD){
                $select->where('`status` in (' . Lead_Status::SOLD . ', ' . Lead_Status::RETURNED . ')');
            }
            else if($form->getValue('status') == 'test'){
                $select->where('`is_test`=1');
            }
            else if($form->getValue('status') == 'fraud'){
                $select->where('`is_fraud`=1');
            }

            // просмотр по правам
            $webmaster  = UserAccess::getWebmasterAccess($form->getValue('webmaster'));
            $agent      = UserAccess::getWebmasterAgentAccess($form->getValue('agent'));

            if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
            if($agent)          $select->where("`agent`=?",         $agent);

            // другие фильтры
            if(strlen($form->getValue('product')))      $select->where('product=?',         (int)$form->getValue('product'));
            if(strlen($form->getValue('channel_type'))) $select->where('channel_type=?',    (int)$form->getValue('channel_type'));
            if(strlen($form->getValue('channel')))      $select->where('channel=?',         (int)$form->getValue('channel'));
            if(strlen($form->getValue('region')))       $select->where('region=?',          (int)$form->getValue('region'));
            if(strlen($form->getValue('domain')))       $select->where('domain=?',          (int)$form->getValue('domain'));

            $all = $select->fetchAll();
        }

        // настройка таблицы
        $table = new Table();
        $table->setPagingObject($paging);

        $is_flags = false;
        $show_costs = false;

        if(count($all)){
            $channels   = [];
            $pages      = [];

            foreach($all as $k => $v){
                $all[$k]['flags'] = '';

                if($v['is_test']){
                    $all[$k]['flags'].= "<span class='label label-default'>Test</span> ";
                    $is_flags = true;
                }

                if(!UserAccess::isWebmaster()){
                    if($v['is_fraud']){
                        $all[$k]['flags'].= "<span class='label label-danger'>Fraud</span> ";
                        $is_flags = true;
                    }
                }

                if($v['channel_type'] == Lead_ChannelType::SERVER_POST){
                    $channels[] = $v['channel'];
                }
                else {
                    $pages[] = $v['page'];
                }

            }

            $table->setData($all);

            if(count($channels))    Cache_PartialList_WebmasterChannel::load($channels);
            if(count($pages))       Cache_PartialList_WebmasterDomainPage::load($pages);

            $table->setTrAttrib("lead_public_id", function($context){
                return $context['num'] . "." . $context['webmaster'];
            });

            $table->setTrAttrib("class", 'lead_tr');


            //////////////////////////////////////////////////////

            if($is_flags){
                $table->addField_Text('flags', 'Flags')
                    ->setTdCSS('white-space', 'nowrap')
                    ->setTdCSS('width', '1px')
                    ->setTdCSS("font-size", "11px");
            }

            $table->addField_SetArray('status', 'Status', new Lead_Status())
                ->setTdCSS('font-size', '11px')
                ->setLink("/account/leads/lead/log-buyer?id={id}&market=" . Db::getDefaultMarket());

            $table->addField_LeadID('id', 'ID');

            if(UserAccess::isAdmin()){
                $table->addField_LeadEmail('id', 'Email')
                    ->setTextColor('#777')
                    ->setTdCSS('font-size', '11px');
            }

            $table->addField_Date('create_date',    'Date')
                ->setTdCSS('font-size', '11px');

            $table->addField_ProductLabel('product', 'Product')
                ->setTdCSS('font-size', '11px');

            $table->addField_Region('region', 'Region')
                ->setTdCSS('font-size', '11px')
                ->setTextAlignCenter();

            if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                $table->addField_WebmasterName('webmaster',      'Webmaster')
                    ->setTdCSS('font-size', '11px');
            }

            if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
                $table->addField_UserLogin('agent',      'Agent')
                    ->setTdCSS('font-size', '11px');
            }

            if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
                $table->addField_Cache(new Cache_PartialList_LeadSalesRedirects(), 'id', "Sales", 'sales')
                    ->setTdCSS('font-size', '11px');
            }


            //////////////////////////////////////
            $table->startGroup("Channel");

            $table->addField_SetArray('channel_type', 'Type', new Lead_ChannelType())
                ->setTdCSS('font-size', '11px');

            $table->addField_Pattern('', 'Name')
                ->setTdCSS('font-size', '11px')
                ->addDecoratorFunction(function($value, $context){
                    if($context['channel_type'] == Lead_ChannelType::SERVER_POST){
                        return "<a href='/account/webmasters/channels/server-post/?id={$context['channel']}'>" .
                        Cache_PartialList_WebmasterChannel::get($context['channel']) .
                        "</a>";
                    }
                    else {
                        return (new Decorator_LinkColored())->render(
                            Cache_PartialList_WebmasterDomainPage::get($context['page'])
                        );
                    }
                });

            $table->endGroup();

            //////////////////////////////////////
            if(
                UserAccess::isWebmaster() ||
                UserAccess::isAgentWebmaster()
            ){
                $table->addField_Text('money_wm', 'Earnings')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }

            if(UserAccess::isAgentBuyer()){
                $table->addField_Text('money_ttl', 'Income')
                    ->setTextAlignCenter()
                    ->setTextBold()
                    ->setTextColor('#333')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }


            if(UserAccess::isAdmin()){

                $table->startGroup('Earnings');

                $table->addField_Text('money_wm',       'WM')
                    ->setTextAlignCenter()
                    ->setTextColor('#A00')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_pool',      'POOL')
                    ->setTextAlignCenter()
                    ->setTextColor('#AAA')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });


                if($show_costs){
                    $table->addField_Text('money_costs',      'COST')
                        ->setTextAlignCenter()
                        ->setTextColor('#AAA')
                        ->addDecoratorFunction(function($v){
                            return round($v, 2);
                        });
                }

                $table->addField_Text('money_ern',      'ERN')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_ttl',      'TTL')
                    ->setTextAlignCenter()
                    ->setTextColor('#333')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->endGroup();
            }

            $tempPage = [
                'leads'             => 0,

                'thank_money_wm'    => 0,
                'thank_money_ern'   => 0,
                'thank_money_ttl'   => 0,

                'money_wm'          => 0,
                'money_pool'        => 0,
                'money_costs'       => 0,
                'money_ern'         => 0,
                'money_ttl'         => 0,
            ];

            foreach($all as $el){
                $tempPage['leads']++;

                $tempPage['money_wm']           += $el['money_wm'];
                $tempPage['money_pool']         += $el['money_pool'];
                $tempPage['money_costs']        += $el['money_costs'];
                $tempPage['money_ern']          += $el['money_ern'];
                $tempPage['money_ttl']          += $el['money_ttl'];
            }

            $table->addFooter([
                'status'       => '<span style="position: absolute; font-weight: normal; color: #999; padding: 6px 0 0 10px">
                    <b style="color: #000">' . $tempPage['leads'] . '</b> leads ' .
                    ($paging->getPagesCount() > 1 ? 'per page' : '') .
                    '
                </span>',
                'money_wm'          => round($tempPage['money_wm'],         2),
                'money_pool'        => round($tempPage['money_pool'],       2),
                'money_costs'       => round($tempPage['money_costs'],      2),
                'money_ern'         => round($tempPage['money_ern'],        2),
                'money_ttl'         => round($tempPage['money_ttl'],        2),
            ]);

            if($paging->getPagesCount() > 1){
                $totalLeads = $total[$field];

                // если кеш еще не догнал лиды, то может быть рассинхрон, убираем его
                if($paging->isLastPage()){
                    $totalLeads = $tempPage['leads'] + ($paging->getPagesCount() - 1) * $paging->getPageLen();
                }

                $table->addFooter([
                    'status' => '<span style="position: absolute; font-weight: normal; color: #999; padding: 6px 0 0 10px">
                        <b style="color: #000">' . $totalLeads . '</b> leads total
                    </span>',

                    'money_wm'          => round(isset($total['leads_money_wm']) ? $total['leads_money_wm'] : 0,         2),
                    'money_pool'        => round(isset($total['leads_money_pool']) ? $total['leads_money_pool'] : 0,     2),
                    'money_costs'       => round(isset($total['leads_money_costs']) ? $total['leads_money_costs'] : 0,   2),
                    'money_ern'         => round(isset($total['leads_money_ern']) ? $total['leads_money_ern'] : 0,       2),
                    'money_ttl'         => round(isset($total['leads_money_ttl']) ? $total['leads_money_ttl'] : 0,       2),
                ]);
            }
        }
        else {
            $table->setMessagesNotData("<center><br><br><h1>Leads not found</h1></center>");
        }

        echo $table->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');

        echo $tabs->setText(ob_get_clean())->render();
    }

    function callsAction(){
        $this->setPageTitle('Calls Details');

        $tabs = new Tabs_Markets();
        $tabs->setShowTimezone();
        Db::setDefaultMarket($tabs->getActiveTab());
        ob_start();

        $form = $this->getMainReportForm();
        $form->removeElement('channel_type');
        $form->removeElement('domain');

        $statuses = [
            ''          => ['All',      'call_all',         null],
            'wait'      => ['Wait',     'call_wait',        [Lead_Call_Status::NEW_LEAD, Lead_Call_Status::NEW_CALL]],
            'exit'      => ['Exit',     'call_exit',        Lead_Call_Status::ERR_END_CALL],
            'error'     => ['Error',    'call_data_err',    Lead_Call_Status::ERR_DATA],
            'sold'      => ['Sold',     'call_sold',        Lead_Call_Status::SALE],
            'reject'    => ['Reject',   'call_reject',      Lead_Call_Status::REJECT],
        ];

        $statusesSelect = [];
        foreach($statuses as $k => $v){
            $statusesSelect[$k] = $v[0];
        }

        $form->addElementSelect('status', 'Lead Status', $statusesSelect)
            ->setRenderTypeBigValue()
            ->setRequired(0);

        $form = $this->getMainReportForm_modify($form);

        $form->isValidNotErrors();

        echo $form->render();

        $field = $statuses[$form->getValue('status')][1];

        $total = Report_Webmaster_Summary::getTotal(
            $form->getValue('date')['from'],
            $form->getValue('date')['till'],
            $form->getValue('product'),
            $form->getValue('webmaster'),
            $form->getValue('channel_type'),
            $form->getValue('channel'),
            $form->getValue('region'),
            $form->getValue('domain'),
            $form->getValue('agent')
        );


        $all_count = $total[$field];
        $paging = new Table_Paging();
        $paging->calculate($all_count);

        $all = [];
        if($all_count > 0){
            $select = Db::logs()
                ->select(
                    "lead_calls"
                )
                ->where(
                    '`create` between ? and ?',
                    $form->getValue('date')['from'],
                    $form->getValue('date')['till'] . " 23:59:59"
                )
                ->order('id desc')
                ->limit(
                    $paging->getLimitCount() + ($paging->isLastPage() ? 1000 : 0), // если это последняя страница, то получаем на 1000 лидов больше
                    $paging->getLimitOffset()
                );

            // тип лидов
            if(strlen($form->getValue('status'))){
                if(is_array($statuses[$form->getValue('status')][2])){
                    $select->where('`status` in (' . implode(",", $statuses[$form->getValue('status')][2]) . ')');
                }
                else if(is_numeric($statuses[$form->getValue('status')][2])){
                    $select->where('`status`=?', $statuses[$form->getValue('status')][2]);
                }
            }

            // просмотр по правам
            $webmaster  = UserAccess::getWebmasterAccess($form->getValue('webmaster'));
            $agent      = UserAccess::getWebmasterAgentAccess($form->getValue('agent'));

            if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
            if($agent)          $select->where("`agent`=?",         $agent);

            // другие фильтры
            if(strlen($form->getValue('product')))      $select->where('product=?',         (int)$form->getValue('product'));
            if(strlen($form->getValue('channel')))      $select->where('dialed_phone=?',    (int)$form->getValue('channel'));
            if(strlen($form->getValue('region')))       $select->where('region=?',          (int)$form->getValue('region'));

            $all = $select->fetchAll();
        }

        // настройка таблицы
        $table = new Table();
        $table->setPagingObject($paging);


        if(count($all)){

            $leads = [];

            foreach($all as $el){
                if($el['lead']) {
                    $leads[] = $el['lead'];
                }
            }

            if(count($leads)){
                $leadsIndex = Db::logs()->fetchUnique(
                    "select id, `money_wm`, `money_pool`, `money_ern`, `money_ttl` from lead_data_head where id in (" .
                    implode(",", $leads) .
                    ")"
                );
            }
            else {
                $leadsIndex = [];
            }

            foreach($all as $k => $v){
                $all[$k]['money_wm']    = isset($leadsIndex[$v['lead']]['money_wm'])    ? $leadsIndex[$v['lead']]['money_wm']    : 0;
                $all[$k]['money_pool']  = isset($leadsIndex[$v['lead']]['money_pool'])  ? $leadsIndex[$v['lead']]['money_pool']  : 0;
                $all[$k]['money_ern']   = isset($leadsIndex[$v['lead']]['money_ern'])   ? $leadsIndex[$v['lead']]['money_ern']   : 0;
                $all[$k]['money_ttl']   = isset($leadsIndex[$v['lead']]['money_ttl'])   ? $leadsIndex[$v['lead']]['money_ttl']   : 0;
            }


            $table->setData($all);


            $table->setTrAttrib("call_id", function($context){
                return $context['id'];
            });

            $table->setTrAttrib("class", 'lead_tr');


            //////////////////////////////////////////////////////

            //

            if(!UserAccess::isWebmaster()) {
                $table->addField_Text("id", "ID")
                    ->setLink("/account/leads/call/?id={id}");
            }

            $table->addField_SetArray("status", "Status", new Lead_Call_Status());
            $table->addField_Date("create", "Create");
            $table->addField_LeadID("lead", "Lead");
            $table->addField_ProductLabel("product", "Product");

            if(!UserAccess::isWebmaster()) {
                $table->addField_WebmasterName("webmaster", "Webmaster");
            }

            if(UserAccess::isAdmin()) {
                $table->addField_UserLogin("agent", "Agent");
            }

            $table->addField_StateFIPS("region", "State");

            $table->addField_Text("dialed_phone", "Channel (dialed phone)")
                ->addDecoratorFunction(function($v){
                    return sprintf("%s %s %s",
                        substr($v, 0, 3),
                        substr($v, 3, 3),
                        substr($v, 6, 4)
                    );
                });

            if(!UserAccess::isWebmaster()) {
                $table->addField_Text("client_phone", "Client Phone")
                    ->addDecoratorFunction(function($v){
                        return sprintf("%s %s %s",
                            substr($v, 0, 3),
                            substr($v, 3, 3),
                            substr($v, 6, 4)
                        );
                    });;
            }

            //////////////////////////////////////
            if(
                UserAccess::isWebmaster() ||
                UserAccess::isAgentWebmaster()
            ){
                $table->addField_Text('money_wm', 'Earnings')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }

            if(UserAccess::isAgentBuyer()){
                $table->addField_Text('money_ttl', 'Income')
                    ->setTextAlignCenter()
                    ->setTextBold()
                    ->setTextColor('#333')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }


            if(UserAccess::isAdmin()){

                $table->startGroup('Earnings');

                $table->addField_Text('money_wm',       'WM')
                    ->setTextAlignCenter()
                    ->setTextColor('#A00')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_pool',      'POOL')
                    ->setTextAlignCenter()
                    ->setTextColor('#AAA')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_ern',      'ERN')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_ttl',      'TTL')
                    ->setTextAlignCenter()
                    ->setTextColor('#333')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->endGroup();
            }

            $tempPage = [
                'calls'             => 0,

                'money_wm'          => 0,
                'money_pool'        => 0,
                'money_ern'         => 0,
                'money_ttl'         => 0,
            ];

            foreach($all as $el){
                $tempPage['calls']++;

                $tempPage['money_wm']           += $el['money_wm'];
                $tempPage['money_pool']         += $el['money_pool'];
                $tempPage['money_ern']          += $el['money_ern'];
                $tempPage['money_ttl']          += $el['money_ttl'];
            }

            $table->addFooter([
                'status'       => '<span style="position: absolute; font-weight: normal; color: #999; padding: 6px 0 0 10px">
                    <b style="color: #000">' . $tempPage['calls'] . '</b> calls ' .
                    ($paging->getPagesCount() > 1 ? 'per page' : '') .
                    '
                </span>',
                'money_wm'          => round($tempPage['money_wm'],         2),
                'money_pool'        => round($tempPage['money_pool'],       2),
                'money_ern'         => round($tempPage['money_ern'],        2),
                'money_ttl'         => round($tempPage['money_ttl'],        2),
            ]);

            if($paging->getPagesCount() > 1){
                $totalLeads = $total[$field];

                // если кеш еще не догнал лиды, то может быть рассинхрон, убираем его
                if($paging->isLastPage()){
                    $totalLeads = $tempPage['leads'] + ($paging->getPagesCount() - 1) * $paging->getPageLen();
                }

                $table->addFooter([
                    'status' => '<span style="position: absolute; font-weight: normal; color: #999; padding: 6px 0 0 10px">
                        <b style="color: #000">' . $totalLeads . '</b> leads total
                    </span>',

                    'money_wm'          => round(isset($total['leads_money_wm']) ? $total['leads_money_wm'] : 0,         2),
                    'money_pool'        => round(isset($total['leads_money_pool']) ? $total['leads_money_pool'] : 0,     2),
                    'money_costs'       => round(isset($total['leads_money_costs']) ? $total['leads_money_costs'] : 0,   2),
                    'money_ern'         => round(isset($total['leads_money_ern']) ? $total['leads_money_ern'] : 0,       2),
                    'money_ttl'         => round(isset($total['leads_money_ttl']) ? $total['leads_money_ttl'] : 0,       2),
                ]);
            }
        }
        else {
            $table->setMessagesNotData("<center><br><br><h1>Calls not found</h1></center>");
        }

        echo $table->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');

        echo $tabs->setText(ob_get_clean())->render();
    }

    function cpaAction(){
        $this->setPageTitle('Cost per Actions');

        $tabs = new Tabs_Markets();
        $tabs->setShowTimezone();
        Db::setDefaultMarket($tabs->getActiveTab());
        ob_start();

        $form = $this->getMainReportForm();

        $form->addElementSelect('status', 'Status', (new Lead_Pending_Status())->getArrayToSelect("All"))
            ->setRenderTypeBigValue()
            ->setRequired(0);

        $form = $this->getMainReportForm_modify($form);

        $form->isValidNotErrors();

        echo $form->render();

        $total = Report_Webmaster_Summary::getTotal(
            $form->getValue('date')['from'],
            $form->getValue('date')['till'],
            $form->getValue('product'),
            $form->getValue('webmaster'),
            $form->getValue('channel_type'),
            $form->getValue('channel'),
            $form->getValue('region'),
            $form->getValue('domain'),
            $form->getValue('agent')
        );

        if($form->getValue('status') == Lead_Pending_Status::SOLD){
            $all_count = $total['leads_pending_sold'];
        }
        else if($form->getValue('status') == Lead_Pending_Status::REJECT){
            $all_count = $total['leads_pending_reject'];
        }
        else if($form->getValue('status') == Lead_Pending_Status::WAIT){
            $all_count = $total['leads_pending_all'] - $total['leads_pending_sold'] - $total['leads_pending_reject'];
        }
        else {
            $all_count = $total['leads_pending_all'];
        }

        $paging = new Table_Paging();
        $paging->calculate($all_count);

        $all = [];
        if($all_count > 0){
            $select = Db::logs()
                ->select("lead_pending")
                ->where(
                    '`create` between ? and ?',
                    $form->getValue('date')['from'],
                    $form->getValue('date')['till'] . " 23:59:59"
                )
                ->order('id desc')
                ->limit(
                    $paging->getLimitCount() + ($paging->isLastPage() ? 1000 : 0), // если это последняя страница, то получаем на 1000 лидов больше
                    $paging->getLimitOffset()
                );

            // просмотр по правам
            $webmaster  = UserAccess::getWebmasterAccess($form->getValue('webmaster'));
            $agent      = UserAccess::getWebmasterAgentAccess($form->getValue('agent'));

            if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
            if($agent)          $select->where("`agent`=?",         $agent);
            if($agent)          $select->where("`agent`=?",         $agent);

            // другие фильтры
            if(strlen($form->getValue('product')))      $select->where('`product`=?',         (int)$form->getValue('product'));
            if(strlen($form->getValue('channel_type'))) $select->where('`channel_type`=?',    (int)$form->getValue('channel_type'));
            if(strlen($form->getValue('channel')))      $select->where('`channel`=?',         (int)$form->getValue('channel'));
            if(strlen($form->getValue('region')))       $select->where('`region`=?',          (int)$form->getValue('region'));
            if(strlen($form->getValue('domain')))       $select->where('`domain`=?',          (int)$form->getValue('domain'));
            if(strlen($form->getValue('status')))       $select->where('`status`=?',          (int)$form->getValue('status'));

            $all = $select->fetchAll();
        }

        // настройка таблицы
        $table = new Table();
        $table->setPagingObject($paging);

        if(count($all)){
            $channels   = [];
            $pages      = [];

            foreach($all as $v){
                if($v['channel_type'] == Lead_ChannelType::SERVER_POST){
                    $channels[] = $v['channel'];
                }
                else {
                    $pages[] = $v['page'];
                }

            }

            $table->setData($all);


            if(count($channels))    Cache_PartialList_WebmasterChannel::load($channels);
            if(count($pages))       Cache_PartialList_WebmasterDomainPage::load($pages);

            $table->setTrAttrib("class", 'lead_tr');


            // id	create	status	our_id	lead	product	region	domain	webmaster	agent	channel_type	channel	buyer	posting	value

            $table->addField_SetArray('status', 'Status', new Lead_Pending_Status())
                ->setLink("/account/leads/lead/log-buyer?id={lead}&market=" . Db::getDefaultMarket())
                ->setTdCSS('font-size', '11px');

            $table->addField_LeadID('lead');

            if(UserAccess::isAdmin()){
                $table->addField_LeadEmail('id', 'Email')
                    ->setTextColor('#777')
                    ->setTdCSS('font-size', '11px');
            }

            $table->addField_Date('create',    'Date')
                ->setTdCSS('font-size', '11px');

            $table->addField_ProductLabel('product', 'Product')
                ->setTdCSS('font-size', '11px');

            $table->addField_Region('region', 'Region')
                ->setTdCSS('font-size', '11px')
                ->setTextAlignCenter();

            //////////////////////////////////////////////////////

            if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                $table->addField_WebmasterName('webmaster',      'Webmaster')
                    ->setTdCSS('font-size', '11px');
            }

            if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
                $table->addField_UserLogin('agent',      'Agent')
                    ->setTdCSS('font-size', '11px');
            }

            if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
                $table->addField_Cache(new Cache_PartialList_LeadSalesRedirects(), 'id', "Sales", 'sales')
                    ->setTdCSS('font-size', '11px');
            }

            //////////////////////////////////////
            $table->startGroup("Channel");

            $table->addField_SetArray('channel_type', 'Type', new Lead_ChannelType())
                ->setTdCSS('font-size', '11px');

            $table->addField_Pattern('', 'Name')
                ->setTdCSS('font-size', '11px')
                ->addDecoratorFunction(function($value, $context){
                    if($context['channel_type'] == Lead_ChannelType::SERVER_POST){
                        return "<a href='/account/webmasters/channels/server-post/?id={$context['channel']}'>" .
                        Cache_PartialList_WebmasterChannel::get($context['channel']) .
                        "</a>";
                    }
                    else {
                        return (new Decorator_LinkColored())->render(
                            Cache_PartialList_WebmasterDomainPage::get($context['page'])
                        );
                    }
                });

            $table->endGroup();


            //////////////////////////////////////
            if(
                UserAccess::isWebmaster() ||
                UserAccess::isAgentWebmaster()
            ){
                $table->addField_Text('money_wm', 'Earnings')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }

            if(UserAccess::isAgentBuyer()){
                $table->addField_Text('money_ttl', 'Income')
                    ->setTextAlignCenter()
                    ->setTextBold()
                    ->setTextColor('#333')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });
            }


            if(UserAccess::isAdmin()){

                $table->startGroup('Earnings');

                $table->addField_Text('money_wm',       'WM')
                    ->setTextAlignCenter()
                    ->setTextColor('#A00')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_pool',      'POOL')
                    ->setTextAlignCenter()
                    ->setTextColor('#AAA')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_ern',      'ERN')
                    ->setTextAlignCenter()
                    ->setTextColor('#0A0')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->addField_Text('money_ttl',      'TTL')
                    ->setTextAlignCenter()
                    ->setTextColor('#333')
                    ->addDecoratorFunction(function($v){
                        return round($v, 2);
                    });

                $table->endGroup();
            }

            $tempPage = [
                'leads'             => 0,
                'money_wm'          => 0,
                'money_pool'        => 0,
                'money_ern'         => 0,
                'money_ttl'         => 0,
            ];

            foreach($all as $el){
                $tempPage['leads']++;

                $tempPage['money_wm']           += $el['money_wm'];
                $tempPage['money_pool']         += $el['money_pool'];
                $tempPage['money_ern']          += $el['money_ern'];
                $tempPage['money_ttl']          += $el['money_ttl'];
            }

            $table->addFooter([
                'status'       => '<span style="position: absolute; font-weight: normal; color: #999; padding: 6px 0 0 10px">
                    <b style="color: #000">' . $tempPage['leads'] . '</b> leads ' .
                    ($paging->getPagesCount() > 1 ? 'per page' : '') .
                    '
                </span>',
                'money_wm'          => round($tempPage['money_wm'],         2),
                'money_pool'        => round($tempPage['money_pool'],       2),
                'money_ern'         => round($tempPage['money_ern'],        2),
                'money_ttl'         => round($tempPage['money_ttl'],        2),
            ]);

            if($paging->getPagesCount() > 1){
                if($form->getValue('status') == Lead_Pending_Status::SOLD){
                    $totalLeads = $total['leads_pending_sold'];
                }
                else if($form->getValue('status') == Lead_Pending_Status::REJECT){
                    $totalLeads = $total['leads_pending_reject'];
                }
                else if($form->getValue('status') == Lead_Pending_Status::WAIT){
                    $totalLeads = $total['leads_pending_all'] - $total['leads_pending_sold'] - $total['leads_pending_reject'];
                }
                else {
                    $totalLeads = $total['leads_pending_all'];
                }

                // если кеш еще не догнал лиды, то может быть рассинхрон, убираем его
                if($paging->isLastPage()){
                    $totalLeads = $tempPage['leads'] + ($paging->getPagesCount() - 1) * $paging->getPageLen();
                }

                $table->addFooter([
                    'status' => '<span style="position: absolute; font-weight: normal; color: #999; padding: 6px 0 0 10px">
                        <b style="color: #000">' . $totalLeads . '</b> leads total
                    </span>',

                    'money_wm'          => round(isset($total['pending_money_wm'])   ? $total['pending_money_wm']   : 0, 2),
                    'money_pool'        => round(isset($total['pending_money_pool']) ? $total['pending_money_pool'] : 0, 2),
                    'money_ern'         => round(isset($total['pending_money_ern'])  ? $total['pending_money_ern']  : 0, 2),
                    'money_ttl'         => round(isset($total['pending_money_ttl'])  ? $total['pending_money_ttl']  : 0, 2),
                ]);
            }
        }
        else {
            $table->setMessagesNotData("<center><br><br><h1>Leads not found</h1></center>");
        }

        echo $table->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');

        echo $tabs->setText(ob_get_clean())->render();
    }

    function returnsAction(){
        $this->setPageTitle('Leads Returns');

        $tabs = new Tabs_Markets();
        $tabs->setShowTimezone();
        Db::setDefaultMarket($tabs->getActiveTab());
        ob_start();

        $form = $this->getMainReportForm();
        $form->removeElement('channel_type');
        $form = $this->getMainReportForm_modify($form);
        $form->removeElement('showEarningsType');

        $form->isValidNotErrors();

        echo $form->render();

        ///////////////////////////////////
        $select = Db::logs()->select("lead_return_webmaster")
            ->where("`lead_date` BETWEEN ? AND ?", $form->getValue('date')['from'], $form->getValue('date')['till'] . " 23:59:59")
            ->order("id desc");

        if($form->getValue('product'))   $select->where('product=?',    (int)$form->getValue('product'));

        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
            if($form->getValue('webmaster'))  $select->where("`webmaster`=?", $form->getValue('webmaster'));
        }
        else if(UserAccess::isWebmaster()){
            $select->where("`webmaster`=?", Users::getCurrent()->company_id);
        }
        else {
            $select->where("1=0");
        }

        // права агента
        if(UserAccess::isAdmin()){
            if(strlen($form->getValue('agent'))) $select->where('agent=?', (int)$form->getValue('agent'));
        }
        else if(UserAccess::isAgentWebmaster()){
            $select->where('agent=?', Users::getCurrent()->id);
        }

        if(strlen($form->getValue('channel')))  $select->where('channel=?',         (int)$form->getValue('channel'));
        if(strlen($form->getValue('region')))   $select->where('region=?',          (int)$form->getValue('region'));
        if(strlen($form->getValue('domain')))   $select->where('domain=?',          (int)$form->getValue('domain'));

        try {
            $all = $select->fetchAll();
        }
        catch(Exception $e){
            $all = [];
        }

        ///////////////////
        $table = new Table($all);
        $table->setPagingAuto(100);
        $table->setMessagesNotData("<center><br><br><h1>Returns not found</h1></center>");

        $totalPrice = 0;

        if(count($all)){
            foreach($all as $el){
                $totalPrice+= $el['price'];
            }
        }

        $totalPrice = round($totalPrice, 2);

        $table->addField_Text(          'reason',       'Reason')
            ->setTdCSS('font-size', '11px')
            ->addDecoratorFunction(function($value){
                $limit = 64;

                if(strlen($value) > 64){
                    return "<span class='more'><div><div>{$value}</div></div>" . substr($value, 0, strripos(substr($value, 0, $limit), " ")) . "..." . "</span>";
                }

                return $value;
            });

        $table->addField_Date("lead_date", "Lead Date");
        $table->addField_Date("create", "Return Create");

        $table->addField_Function(function($context){
            return (new Decorator_AfterSeconds())->render(strtotime($context['create']) - strtotime($context['lead_date']));
        }, "After");

        $table->addField_LeadID("lead_id", "Lead ID");

        $table->addField_ProductLabel("product", "Product");

        if(!UserAccess::isWebmaster()){
            $table->addField_WebmasterName("webmaster", "Webmaster");
        }

        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            $table->addField_UserLogin('agent',      'Agent')
                ->setTdCSS('font-size', '11px');
        }


        $table->addField_Text("price", "Return Price")
            ->setTextAlignCenter()
            ->setTextColor("#A00")
            ->addDecoratorFunction(function($value){
                return $value + 0;
            });


        $table->addFooter([
            'lead_date' => '<span style="position: absolute; padding-left: 10px">Total in all pages:</span>',
            'price'     => "<b>{$totalPrice}</b>",
        ]);

        echo $table->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');

        echo $tabs->setText(ob_get_clean())->render();
    }

    function leadsErrorsAction(){
        $this->setPageTitle('Leads Errors');

        $tabs = new Tabs_Markets();
        $tabs->setShowTimezone();
        Db::setDefaultMarket($tabs->getActiveTab());
        ob_start();

        $form = $this->getMainReportForm();
        $form->removeElement('product');
        $form->removeElement('region');
        $form->removeElement('region_and_product');
        $form->removeElement('channel');
        $form->removeElement('channel_type');
        $form->removeElement('domain');

        $form = $this->getMainReportForm_modify($form);
        $form->removeElement('showEarningsType');

        $form->addElementRadio('errorStatus', 'Type of Errors', [
            Lead_Status::DATA_ERROR => 'Errors in the data',
            Lead_Status::FATAL_ERROR => 'Fatal errors',
        ])->setValue(Lead_Status::DATA_ERROR);

        $form->isValidNotErrors();
        echo $form->render();

        if($form->getValue('errorStatus') == Lead_Status::DATA_ERROR){
            if(isset($_GET['error-id']) && strlen($_GET['error-id'])){
                $id = (int)$_GET['error-id'];

                try {
                    $select = Db::logs()->select('lead_getting_error')->where('id=?', $id);

                    // просмотр по правам
                    $webmaster  = UserAccess::getWebmasterAccess($form->getValue('webmaster'));
                    $agent      = UserAccess::getWebmasterAgentAccess($form->getValue('agent'));

                    if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
                    if($agent)          $select->where("`agent`=?",         $agent);

                    $error = $select->fetchRow();

                    if($error === false){
                        throw new Exception();
                    }
                }
                catch(Exception $e){
                    $this->setFollow_Error404();
                    return;
                }

                $tabs = new Tabs_StaticTabs();
                $tabs->setValueName('error-id');

                $tabs->addTab('', 'Errors List');
                $tabs->addTab("{$id}", "Error # {$id}");

                $form = new Form();
                $form->disableButtons();

                if(UserAccess::isWebmaster()){
                    $form->addElementStatic('Create',
                        (new Decorator_Date())->setDateFormatFixed('M d, Y, H:i:s')->render($error['create']));

                    $form->addElementStatic(
                        'Channel Type',
                        (new Lead_ChannelType())->setInLine()->render($error['channel_type'])
                    );

                    $form->addElementStatic(
                        'Channel',
                        $error['channel_type'] == Lead_ChannelType::SERVER_POST ?
                            "<a href='/account/webmasters/channels/server-post/?id={$error['channel']}'>" .
                                Cache_PartialList_WebmasterChannel::get($error['channel']) .
                            "</a>" :
                            "?"
                    );

                    $form->addElementStatic('IP',               inet_ntop($error['ip']));
                    $form->addElementStatic('Runtime',          round($error['runtime'], 4) . " <small>sec</small>");
                    $form->addElementStatic('Error Details',
                        /*"<pre>" .*/ htmlspecialchars($error['description']) /*. "</pre>"*/
                    );

                    $form->addSeparator('Response');
                    $form->addElementStatic(
                        '',
                        String_Highlight::render(
                            String_Highlight::jsonPrettyPrint(
                                $error['response']
                            ), 0
                        )
                    )->setRenderTypeFullLine();
                }
                else {

                    $form->addElementStatic('Create',
                        (new Decorator_Date())->setDateFormatFixed('M d, Y, H:i:s')->render($error['create']));

                    $form->addElementStatic(
                        'Webmaster',
                        "<a href='/account/webmasters/webmaster/?id={$error['webmaster']}'>" .
                        Cache_PartialList_WebmasterName::get($error['webmaster']) .
                        "</a>"
                    );

                    $form->addElementStatic(
                        'Channel Type',
                        (new Lead_ChannelType())->setInLine()->render($error['channel_type'])
                    );

                    $form->addElementStatic(
                        'Channel',
                        $error['channel_type'] == Lead_ChannelType::SERVER_POST ?
                            "<a href='/account/webmasters/channels/server-post/?id={$error['channel']}'>" .
                                Cache_PartialList_WebmasterChannel::get($error['channel']) .
                            "</a>" :
                        "?"
                    );

                    $form->addElementStatic('IP',               inet_ntop($error['ip']));

                    $form->addSeparator('Server Performance');
                    $form->addElementStatic('Runtime',          $error['runtime']);
                    $form->addElementStatic('Memory Usage',     $error['memory_peak']);
                    $form->addElementStatic('CPU Usage',        $error['usage_cpu']);
                    $form->addElementStatic('Database Usage',   $error['usage_db']);

                    $form->addSeparator('Error Details');
                    $form->addElementStatic('',
                        "<pre>" . htmlspecialchars($error['description']) . "</pre>"
                    )->setRenderTypeFullLine();

                    $form->addSeparator('Request');
                    $form->addElementStatic(
                        '',
                        String_Highlight::render(
                            String_Highlight::jsonPrettyPrint(
                                $error['request']
                            ), 0
                        )
                    )->setRenderTypeFullLine();

                    $form->addSeparator('Response');
                    $form->addElementStatic(
                        '',
                        String_Highlight::render(
                            String_Highlight::jsonPrettyPrint(
                                $error['response']
                            ), 0
                        )
                    )->setRenderTypeFullLine();

                    $form->addSeparator('Server');
                    $form->addElementStatic(
                        '',
                        String_Highlight::render(
                            String_Highlight::jsonPrettyPrint(
                                $error['server']
                            ), 0
                        )
                    )->setRenderTypeFullLine();
                }

                $tabs->setText($form->render());
                echo $tabs->render();
            }
            else {
                /*
                 * 1. определить сколько данных
                 * 2. определить в каких таблицах находяться данные
                 */

                $filed = "leads_errors";
                $all_count =  Report_Webmaster_Summary::getTotal(
                    $form->getValue('date')['from'],
                    $form->getValue('date')['till'],
                    null,
                    $form->getValue('webmaster')
                )[$filed];


                $paging = new Table_Paging();
                $paging->calculate($all_count);

                $all = [];
                if($all_count > 0){
                    $select = Db::logs()
                        ->select(
                            "lead_getting_error",
                            [
                                'id',
                                'create',
                                'webmaster',
                                'ip',
                                'description',
                                'runtime',
                                'memory_peak',
                                'channel_type',
                                'channel',
                                'is_test',
                                'period' => new Db_Expr("DATE_FORMAT(`create`, '%Y%m')"),
                            ]
                        )
                        ->where(
                            '`create` between ? and ?',
                            $form->getValue('date')['from'],
                            $form->getValue('date')['till'] . " 23:59:59"
                        )
                        ->order('id desc')
                        ->limit(
                            $paging->getLimitCount() + ($paging->isLastPage() ? 1000 : 0), // если это последняя страница, то получаем на 1000 лидов больше
                            $paging->getLimitOffset()
                        );

                    // просмотр по правам
                    $webmaster  = UserAccess::getWebmasterAccess($form->getValue('webmaster'));
                    $agent      = UserAccess::getWebmasterAgentAccess($form->getValue('agent'));

                    if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
                    if($agent)          $select->where("`agent`=?",         $agent);

                    $all = $select->fetchAll();
                }

                $is_flags = false;
                foreach($all as $k => $v){
                    $all[$k]['flags'] = '';

                    if($v['is_test']){
                        $all[$k]['flags'].= "<span class='label label-default'>Test</span> ";
                        $is_flags = true;
                    }
                }

                // настрйока таблицы
                $table = new Table();
                $table->setMessagesNotData("
                    <br><br>
                    <center>
                        <h1>
                            <i class='icon-ok' style='color:#35aa47; margin-right: 20px'></i>
                            Was not errors
                        </h1>
                    </center>
                ");
                $table->setTitle('<h3>Errors in the data</h3>');
                $table->setPagingObject($paging);
                $table->setData($all);

                if($is_flags){
                    $table->addField_Text('flags', 'Flags')
                        ->setTdCSS('white-space', 'nowrap')
                        ->setTdCSS('width', '1px')
                        ->setTdCSS("font-size", "11px");
                }

                $table->startGroup("Channel");

                if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                    $table->addField_WebmasterName('webmaster');
                }

                $table->addField_SetArray('channel_type', 'Type', new Lead_ChannelType())
                    ->setTdCSS('font-size', '11px');

                $table->addField_Pattern('', 'Name')
                    ->setTdCSS('font-size', '11px')
                    ->addDecoratorFunction(function($value, $context){
                        if($context['channel_type'] == Lead_ChannelType::SERVER_POST){
                            return "<a href='/account/webmasters/channels/server-post/?id={$context['channel']}'>" .
                            Cache_PartialList_WebmasterChannel::get($context['channel']) .
                            "</a>";
                        }
                        else {
                            return "-";
                        }
                    });

                $table->endGroup();

                $table->addField_Date('create', 'Date');

                $table->addField_Text('description', 'Details')
                    ->addDecoratorFunction(function($value, $all){
                        return
                            "<b>{$all['id']}.{$all['period']} - </b> " .
                            substr($value, 0, 86) .
                            (strlen($value) > 86 ? " ..." : "");
                    })
                    ->setLink(Http_Url::convert([
                        'error-id' => '{id}',
                    ]));

                if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
                    $table->addField_WebmasterAgentLogin('webmaster');
                }

                $table->addField_IP('ip');

                $table->addField_Text('runtime', 'Runtime')->addDecoratorFunction(function($value){
                    return number_format($value, 4);
                });

                if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                    $table->addField_Text('memory_peak', 'Memory (Mb)')->addDecoratorFunction(function($value){
                        return number_format($value / 1048576, 2);
                    });
                }

                echo $table;
            }

        }
        else if($form->getValue('errorStatus') == Lead_Status::FATAL_ERROR){
            if(
                isset($_GET['fatal-error-id']) && strlen($_GET['fatal-error-id']) &&
                (
                    UserAccess::isAdmin() ||
                    UserAccess::isAgentWebmaster() ||
                    UserAccess::isAgentBuyer()
                )
            ){
                $id = (int)$_GET['fatal-error-id'];

                try {
                    $select = Db::logs()->select('lead_getting_fatal_error')->where('id=?', $id);

                    // просмотр по правам
                    $webmaster  = UserAccess::getWebmasterAccess($form->getValue('webmaster'));
                    $agent      = UserAccess::getWebmasterAgentAccess($form->getValue('agent'));

                    if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
                    if($agent)          $select->where("`agent`=?",         $agent);

                    $error = $select->fetchRow();

                    if($error === false){
                        throw new Exception();
                    }
                }
                catch(Exception $e){
                    $this->setFollow_Error404();
                    return;
                }

                $tabs = new Tabs_StaticTabs();
                $tabs->setValueName('fatal-error-id');

                $tabs->addTab('', 'Errors List');
                $tabs->addTab("{$id}", "Error # {$id}");

                $form = new Form();
                $form->disableButtons();

                $form->addElementStatic('Create',
                    (new Decorator_Date())->setDateFormatFixed('M d, Y, H:i:s')->render($error['create']));

                $form->addElementStatic('Webmaster ID',     $error['webmaster']); // todo
                $form->addElementStatic('Lead ID',          $error['lead']); // todo
                $form->addElementStatic('IP',               inet_ntop($error['ip']));

                $form->addSeparator('Server Performance');
                $form->addElementStatic('Runtime',          $error['runtime']);
                $form->addElementStatic('Memory Usage',     $error['memory_peak']);
                $form->addElementStatic('CPU Usage',        $error['usage_cpu']);
                $form->addElementStatic('Database Usage',   $error['usage_db']);

                $form->addSeparator('Error Details');
                $form->addElementStatic('',
                    "<pre>" . htmlspecialchars($error['error']) . "</pre>"
                )->setRenderTypeFullLine();

                $form->addSeparator('Request');
                $form->addElementStatic('',
                    "<pre>" . htmlspecialchars($error['request']) . "</pre>"
                )->setRenderTypeFullLine();

                $form->addSeparator('Response');
                $form->addElementStatic('',
                    "<pre>" . htmlspecialchars($error['response']) . "</pre>"
                )->setRenderTypeFullLine();

                $form->addSeparator('Server');
                $form->addElementStatic('',
                    "<pre>" . htmlspecialchars($error['server']) . "</pre>"
                )->setRenderTypeFullLine();

                $form->addSeparator('Buffered Content');
                $form->addElementStatic('',
                    "<pre>" . htmlspecialchars($error['content']) . "</pre>"
                )->setRenderTypeFullLine();

                $tabs->setText($form->render());
                echo $tabs->render();
            }
            else {
                /*
                 * 1. определить сколько данных
                 * 2. определить в каких таблицах находяться данные
                 */

                $filed = "leads_fatal_errors";
                $all_count =  Report_Webmaster_Summary::getTotal(
                    $form->getValue('date')['from'],
                    $form->getValue('date')['till'],
                    null,
                    $form->getValue('webmaster')
                )[$filed];

                $paging = new Table_Paging();
                $paging->calculate($all_count);

                $all = [];
                if($all_count > 0){
                    $select = Db::logs()
                        ->select(
                            "lead_getting_fatal_error",
                            [
                                'id',
                                'create',
                                'webmaster',
                                'lead',
                                'ip',
                                'error',
                                'runtime',
                                'memory_peak',
                                'period' => new Db_Expr("DATE_FORMAT(`create`, '%Y%m')"),
                            ]
                        )
                        ->where(
                            '`create` between ? and ?',
                            $form->getValue('date')['from'],
                            $form->getValue('date')['till'] . " 23:59:59"
                        )
                        ->order('id desc')
                        ->limit(
                            $paging->getLimitCount() + ($paging->isLastPage() ? 1000 : 0), // если это последняя страница, то получаем на 1000 лидов больше
                            $paging->getLimitOffset()
                        );

                    // просмотр по правам
                    $webmaster  = UserAccess::getWebmasterAccess($form->getValue('webmaster'));
                    $agent      = UserAccess::getWebmasterAgentAccess($form->getValue('agent'));

                    if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
                    if($agent)          $select->where("`agent`=?",         $agent);

                    $all = $select->fetchAll();
                }

                // настрйока таблицы
                $table = new Table();
                $table->setTitle('<h3>Fatal Errors</h3>');
                $table->setPagingObject($paging);
                $table->setData($all);

                $table->addField_Date('create', 'Date');

                if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                    $table->addField_Text('error', 'Details')
                        ->addDecoratorFunction(function($value, $all){
                            return
                                "<b>{$all['id']} - </b> " .
                                substr($value, 0, 86) .
                                (strlen($value) > 86 ? " ..." : "");
                        })
                        ->setLink(Http_Url::convert([
                            'fatal-error-id' => '{id}',
                        ]));
                }

                if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                    $table->addField_WebmasterName('webmaster');
                }

                if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
                    $table->addField_WebmasterAgentLogin('webmaster');
                }

                $table->addField_LeadID('lead');
                $table->addField_IP('ip');

                $table->addField_Text('runtime', 'Runtime')->addDecoratorFunction(function($value){
                    return number_format($value, 4);
                });

                if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                    $table->addField_Text('memory_peak', 'Memory (Mb)')->addDecoratorFunction(function($value){
                        return number_format($value / 1048576, 2);
                    });
                }

                echo $table->render();

                /*
                $counts = Report_Webmaster_Summary::getFatalErrorsDailyCounts(
                    $form->getValue('date')['from'],
                    $form->getValue('date')['till'],
                    $form->getValue('webmaster')
                );

                $all_count = 0;
                if(count($counts)){
                    foreach($counts as $period => $count){
                        $all_count+= $count;
                    }
                }

                if($all_count){
                    $paging = new Table_Paging();
                    $paging->calculate($all_count);

                    $lim1 = $paging->getLimitOffset();
                    $lim2 = $paging->getLimitOffset() + $paging->getLimitCount();

                    // получить таблицы из которых идет выборки
                    $periods = [];
                    $current = 0;
                    $old_counts = 0;
                    $i = 0;
                    foreach($counts as $period => $count){
                        $current_next = $current + $count;

                        if(
                            ($lim1 >= $current && $lim1 < $current_next) ||
                            ($lim2 >  $current && $lim2 <= $current_next)
                        ){
                            $periods[$period] = [
                                'offset' => max($current, $lim1) - $old_counts,
                                'count' => min($current_next, $lim2) - max($current, $lim1)
                            ];

                            // для последней станицы, лимит увеличиваеться до 1000, что бы кончик не исчезал из за не индексации количесва
                            if($paging->isLastPage() && $i == (count($counts) - 1)){
                                $periods[$period]['count'] = 1000;
                            }
                        }

                        $old_counts+= $count;
                        $i++;
                        $current = $current_next;
                    }

                    // получение данных
                    $all = [];
                    foreach($periods as $period => $limits){
                        $select = Db::logs()
                            ->select(
                                "lead_getting_fatal_error",
                                [
                                    'id',
                                    'create',
                                    'webmaster',
                                    'lead',
                                    'ip',
                                    'error',
                                    'runtime',
                                    'memory_peak',
                                    'period' => new Db_Expr("DATE_FORMAT(`create`, '%Y%m')"),
                                ]
                            )
                            ->where(
                                '`create` between ? and ?',
                                $form->getValue('date')['from'],
                                $form->getValue('date')['till'] . " 23:59:59"
                            )
                            ->order('id desc')
                            ->limit(
                                $limits['count'],
                                $limits['offset']
                            );

                        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                            if($form->getValue('webmaster'))  $select->where("`webmaster`=?", $form->getValue('webmaster'));
                        }
                        else if(UserAccess::isWebmaster()){
                            $select->where("`webmaster`=?", Users::getCurrent()->company_id);
                        }
                        else {
                            $select->where("1=0");
                        }

                        // права агента
                        if(UserAccess::isAdmin()){
                            if(strlen($form->getValue('agent'))) $select->where('agent=?', (int)$form->getValue('agent'));
                        }
                        else if(UserAccess::isAgentWebmaster()){
                            $select->where('agent=?', Users::getCurrent()->id);
                        }

                        $all = array_merge($all , $select->fetchAll());
                    }

                    // настрйока таблицы
                    $table = new Table();
                    $table->setTitle('<h3>Fatal Errors</h3>');
                    $table->setPagingObject($paging);
                    $table->setData($all);

                    $table->addField_Date('create', 'Date');

                    if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                        $table->addField_Text('error', 'Details')
                            ->addDecoratorFunction(function($value, $all){
                                return
                                    "<b>{$all['id']}.{$all['period']} - </b> " .
                                    substr($value, 0, 86) .
                                    (strlen($value) > 86 ? " ..." : "");
                            })
                            ->setLink(Http_Url::convert([
                                'fatal-error-id' => '{id}.{period}',
                            ]));
                    }

                    if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                        $table->addField_WebmasterName('webmaster');
                    }

                    if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
                        $table->addField_WebmasterAgentLogin('webmaster');
                    }

                    $table->addField_LeadID('lead');
                    $table->addField_IP('ip');

                    $table->addField_Text('runtime', 'Runtime')->addDecoratorFunction(function($value){
                        return number_format($value, 4);
                    });

                    if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
                        $table->addField_Text('memory_peak', 'Memory (Mb)')->addDecoratorFunction(function($value){
                            return number_format($value / 1048576, 2);
                        });
                    }

                    echo $table->render();

                }
                else {
                    echo "
                        <br><br>
                        <center>
                            <h1>
                                <i class='icon-ok' style='color:#35aa47; margin-right: 20px'></i>
                                Hooray! Was not fatal errors
                            </h1>
                        </center>
                    ";
                }
                */
            }
        }

        Webmaster_Url::convertUrlForWebmasters('webmaster');

        echo $tabs->setText(ob_get_clean())->render();
    }

    /********************************************************************************************/

    protected function getFormForPerformanceReport($excludeElements = []){
        if(is_string($excludeElements)) $excludeElements = [$excludeElements];
        if(!is_array($excludeElements)) $excludeElements = [];

        $form = new Form();
        $form->setMethodGET();

        $form->addElementDateRange("date")
            ->setValueDates('-7 Days', '-1 Day')
            ->setButtonsMonths();

        $form->addSeparator();

        if(!in_array('agent', $excludeElements)){
            if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
                $el = $form->addElement_WebmasterAgent('agent', 'Agent')
                    ->setWidth('250px')
                    ->setRequired(false);

                if(UserAccess::isAgentWebmaster()){
                    $el->setValue(Users::getCurrent()->id);
                }
            }
        }

        if(!in_array('webmaster', $excludeElements)){
            if(!UserAccess::isWebmaster()){
                $form->addElement_Webmaster('webmaster')
                    ->setRequired(false)
                    ->setWidth('250px');
            }
        }

        if(!in_array('channel_type', $excludeElements)){
            $form->addElementSelect(
                'channel_type',
                'Channel Type',
                (new Lead_ChannelType())->getArrayToSelect('All...')
            )
                ->setRequired(false)
                ->setWidth('264px');
        }

        $form->addSeparator();

        if(!in_array('domain', $excludeElements)){
            $form->addElement_WebmasterDomain('domain', 'Domain')
                ->setRequired(false)
                ->setWidth('350px');
        }

        if(!in_array('product', $excludeElements)){
            $form->addElement_ProductForMarket(Db::getDefaultMarket(), 'product',   'All...')
                ->setRequired(false)
                ->setWidth('264px');
        }

        if(!in_array('region', $excludeElements)){
            $form->addElement_Region('region',   'Region')
                ->setRequired(false)
                ->setWidth('250px');
        }

        return $form;
    }

    /***************************************************/

    protected function addPerformanceButtons(Table $table, Form $form, $type){
        //////////////////////////////////////////////////
        // дополнительные кнопки, для деления трафика

        if(UserAccess::isAdmin()){
            if($type != 'agent' && !strlen($form->getValue("agent"))){
                $table->addField_Button("Agent")
                    ->setLink(Http_Url::convert(
                        [
                            $type => "{" . $type . "}",
                            'tab' => "agent",
                        ]
                    ));
            }
        }

        if(!UserAccess::isWebmaster()){
            if($type != 'webmaster' && !strlen($form->getValue("webmaster"))){
                $table->addField_Button("WM")
                    ->setLink(Http_Url::convert(
                        [
                            $type => "{" . $type . "}",
                            'tab' => "webmaster",
                        ]
                    ));
            }
        }

        if($type != 'channel_type' && !strlen($form->getValue("channel_type"))){
            $table->addField_Button("ChType")
                ->setLink(Http_Url::convert(
                    [
                        $type => "{" . $type . "}",
                        'tab' => "channel_type",
                    ]
                ));
        }

        /*
        if($type != 'channel' && !strlen($form->getValue("channel"))){
            $table->addField_Button("Ch")
                ->setLink(Http_Url::convert(
                    [
                        $type => "{" . $type . "}",
                            'tab' => "channel",
                    ]
                ));
        }
        */

        if($type != 'domain' && !strlen($form->getValue("domain"))){
            $table->addField_Button("Domain")
                ->setLink(Http_Url::convert(
                    [
                        $type => "{" . $type . "}",
                        'tab' => "domain",
                    ]
                ));
        }

        if($type != 'product' && !strlen($form->getValue("product"))){
            $table->addField_Button("Prod")
                ->setLink(Http_Url::convert(
                    [
                        $type => "{" . $type . "}",
                        'tab' => "product",
                    ]
                ));
        }

        if($type != 'region' && !strlen($form->getValue("region"))){
            $table->addField_Button("Region")
                ->setLink(Http_Url::convert(
                    [
                        $type => "{" . $type . "}",
                        'tab' => "region",
                    ]
                ));
        }

        return $table;
    }

    /********************************************************************************************/

    public function topListAction(){
        $tabsMarket = new Tabs_Markets();
        $tabsMarket->setShowTimezone();
        Db::setDefaultMarket($tabsMarket->getActiveTab());

        $tabs = new Tabs_StaticTabs();

        if(UserAccess::isAdmin()){
            $tabs->addTab('agent', 'Webmaster Agents');
        }

        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            $tabs->addTab('webmaster', 'Webmasters');
        }

        $tabs->addTab('channel_type', 'Channel Types');
        $tabs->addTab('domain', 'Domains');
        $tabs->addTab('product', 'Products');
        $tabs->addTab('region', 'Regions');


        $type = $tabs->getActiveTab();

        $this->setTitle("Top List - " . $tabs->getActiveTabLabel());

        $form = $this->getFormForPerformanceReport($type);
        $form->addElementHidden('tab', $type);

        $form->addSeparator();
        $form->addElementRadio('order', "Order", [
            'money'         => 'Earnings',
            'leads_sold'    => 'Sales',
            'epl'           => 'EPL',
            'leads_all'     => 'Leads Count',
        ])
            ->setValue('money')
            ->setRenderTypeBigValue();

        $form->isValidNotErrors();

        /************************************************************/

        $select = Db::reports()->select("webmaster_summary_days", [
            $type,
            'leads_all'     => 'sum(leads_all)',
            'leads_return'  => 'sum(leads_return)',
            'leads_sold'    => 'sum(leads_sold)',
            'epl'           => '((sum(leads_money_wm)) / sum(leads_all))',
            'money'         => '(sum(leads_money_wm))',
        ])
            ->where("leads_all > 0")
            ->where("`date` between ? and ?", $form->getValue("date")['from'], $form->getValue("date")['till'])
            ->group($type)
            ->order($form->getValue('order') . ' desc');

        /***************************/


        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster() || UserAccess::isAgentBuyer()){
            if($form->getValue('webmaster'))  $select->where("`webmaster`=?", $form->getValue('webmaster'));
        }
        else if(UserAccess::isWebmaster()){
            $select->where("`webmaster`=?", Users::getCurrent()->company_id);
        }
        else {
            $select->where("1=0");
        }

        if($form->getValue('product'))   $select->where('product=?',    (int)$form->getValue('product'));

        // права агента
        if(UserAccess::isAdmin()){
            if($form->getValue('agent')) $select->where('agent=?', (int)$form->getValue('agent'));
        }
        else if(UserAccess::isAgentWebmaster()){
            $select->where('agent=?', Users::getCurrent()->id);
        }

        if(UserAccess::isWebmaster()){
            $select->where("`webmaster` =?", Users::getCurrent()->company_id);
        }

        if($form->getValue('channel_type'))     $select->where('channel_type=?',    (int)$form->getValue('channel_type'));
        if($form->getValue('region'))           $select->where('region=?',          (int)$form->getValue('region'));
        if($form->getValue('domain'))           $select->where('domain=?',          (int)$form->getValue('domain'));

        /***************************/

        $table = new Table($select->fetchAll());
        $table->setPagingDisabled();

        if($type == 'agent'){
            $table->addField_UserLogin('agent');
        }
        else if($type == 'webmaster'){
            $table->addField_WebmasterName("webmaster");
            $table->addField_WebmasterAgentLogin("webmaster");
        }
        else if($type == 'channel'){
            $table->addField_WebmasterChannel("channel");
        }
        else if($type == 'channel_type'){
            $table->addField_SetArray("channel_type", "Channel Type", new Lead_ChannelType());
        }
        else if($type == 'domain'){
            $table->addField_WebmasterDomain("domain");
        }
        else if($type == 'product'){
            $table->addField_ProductLabel("product", "Product");
        }
        else if($type == 'region'){
            $table->addField_Region("region", "Region");
        }
        else {
            $table->addField_Text($type, $type);
        }

        $table->addField_Text("leads_all",  "All Leads")
            ->setLink(
                Http_Url::convert(
                    [
                        'date[from]'    => $form->getValue('date')['from'],
                        'date[till]'    => $form->getValue('date')['till'],
                        $type           => "{" . $type . "}",
                        'status'        => null,
                    ],
                    "/account/reports/webmaster/leads"
                ))
            ->setTextAlignCenter();

        $table->addField_Text("leads_return", "Returns")
            ->setLink(
                Http_Url::convert(
                    [
                        'date[from]'    => $form->getValue('date')['from'],
                        'date[till]'    => $form->getValue('date')['till'],
                        $type           => "{" . $type . "}",
                    ],
                    "/account/reports/webmaster/returns"
                ))
            ->setTextAlignCenter();

        $table->addField_Text("leads_sold", "Sales")
            ->setLink(
                Http_Url::convert(
                    [
                        'date[from]'    => $form->getValue('date')['from'],
                        'date[till]'    => $form->getValue('date')['till'],
                        $type           => "{" . $type . "}",
                        'status'        => Lead_Status::SOLD,
                    ],
                    "/account/reports/webmaster/leads"
                ))
            ->setTextAlignCenter();

        $table->addField_Function(
            function($c){
                return round(($c['leads_sold']*100) / $c['leads_all']) . " <small>%</small>";
            },
            "Convert"
        )
            ->setTextAlignCenter();

        $table->addField_Function(
            function($c){
                return round($c['money'] / $c['leads_all'], 2);
            },
            "EPL"
        )
            ->setDescription('Earnings per Lead')
            ->setTextAlignCenter();

        $table->addField_Function(
            function($c){
                return ($c['leads_sold'] > 0) ?
                    round($c['money'] / $c['leads_sold'], 2) :
                    "-";
            },
            "EPS"
        )
            ->setDescription('Earnings per Sale')
            ->setTextAlignCenter();

        $table->addField_Text("money",      "Earnings")
            ->setTextBold()
            ->setTextAlignCenter();

        // добавление кнопок перехода
        $table = $this->addPerformanceButtons($table, $form, $type);

        echo
            $tabsMarket->render() .
            $this->renderTimezone() .
            $tabs
                ->setText(
                    $form->render() .
                    $table->render()
                )
                ->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');
    }

    /*****************************************************************************************************/

    protected function dynamicGetTop($type, Form $form, $from, $till, $values = null, $sum = 'earnings'){
        $sumQuery = 'SUM(`leads_money_wm`)';

        if($sum == 'epl'){

            $sumQuery = '((sum(leads_money_wm)) / sum(leads_all))';
        }
        else if($sum == 'leads_all'){
            $sumQuery = 'SUM(`leads_all`)';
        }

        $select = Db::reports()->select("webmaster_summary_days", [
            $type,
            'value' => $sumQuery
        ])
            ->where("`date` BETWEEN ? AND ?", $from, $till)
            ->having("`value`>0")
            ->group($type)
            ->order("value desc")
            ->limit(50);

        // права вебмастера
        if(UserAccess::isWebmaster()){
            $select->where('webmaster=?', Users::getCurrent()->company_id);
        }

        // права агента
        if(UserAccess::isAdmin()){
            if($form->getValue('agent')) $select->where('agent=?', (int)$form->getValue('agent'));
        }
        else if(UserAccess::isAgentWebmaster()){
            $select->where('agent=?', Users::getCurrent()->id);
        }

        if($form->getValue('webmaster'))        $select->where('webmaster=?',       (int)$form->getValue('webmaster'));
        if($form->getValue('channel_type'))     $select->where('channel_type=?',    (int)$form->getValue('channel_type'));
        if(strlen($form->getValue('channel')))  $select->where('channel=?',         (int)$form->getValue('channel'));
        if($form->getValue('region'))           $select->where('region=?',          (int)$form->getValue('region'));
        if($form->getValue('domain'))           $select->where('domain=?',          (int)$form->getValue('domain'));
        if($form->getValue('product'))          $select->where("product=?",         (int)$form->getValue('product'));

        if(is_array($values)){
            if(count($values)){
                $select->where("`{$type}` in (" . implode(",", $values) . ")");
            }
            else {
                $select->where("1=0");
            }
        }

        return $select->fetchPairs();

        Webmaster_Url::convertUrlForWebmasters('webmaster');
    }

    public function dynamicAction(){
        $tabsMarket = new Tabs_Markets();
        $tabsMarket->setShowTimezone();
        Db::setDefaultMarket($tabsMarket->getActiveTab());

        $tabs = new Tabs_StaticTabs();

        if(UserAccess::isAdmin()){
            $tabs->addTab('agent', 'Webmaster Agents');
        }

        if(UserAccess::isAdmin() || UserAccess::isAgentWebmaster()){
            $tabs->addTab('webmaster', 'Webmasters');
        }

        $tabs->addTab('channel_type', 'Channel Types');
        $tabs->addTab('domain', 'Domains');
        $tabs->addTab('product', 'Products');
        $tabs->addTab('region', 'Regions');


        $type = $tabs->getActiveTab();

        $this->setTitle("Dynamic - " . $tabs->getActiveTabLabel());

        $form = $this->getFormForPerformanceReport($type);
        $form->addElementHidden('tab', $type);

        $form->addSeparator();
        $form->addElementRadio('dynamic', "Dynamic", [
            'earnings'    => 'Earnings',
            'epl'           => 'EPL',
            'leads_all'     => 'Leads Count',
        ])
            ->setValue('earnings')
            ->setRenderTypeBigValue();

        $form->isValidNotErrors();

        $dynamic = $form->getValue('dynamic');

        $datetime1 = new DateTime($form->getValue('date')['from']);
        $datetime2 = new DateTime($form->getValue('date')['till']);
        $days = (int)$datetime1->diff($datetime2)->format("%a") + 1;

        $k = $days % 7;
        $delta = 0;
        if($k != 0){
            $delta = 7 - $k;
        }

        $w1_from_mk = strtotime($form->getValue('date')['from']);
        $w1_till_mk = strtotime($form->getValue('date')['till']);
        $w2_from_mk = strtotime($form->getValue('date')['from'] . " -" . ($days+$delta) . "Days");
        $w2_till_mk = strtotime($form->getValue('date')['from'] . " -" . (1+$delta) . "Days");


        $week1_from = date('Y-m-d', $w1_from_mk);
        $week1_till = date('Y-m-d', $w1_till_mk);
        $week2_from = date('Y-m-d', $w2_from_mk);
        $week2_till = date('Y-m-d', $w2_till_mk);

        $values = array_unique(
            array_merge(
                array_keys(self::dynamicGetTop($type, $form, $week1_from, $week1_till, null, $dynamic)),
                array_keys(self::dynamicGetTop($type, $form, $week2_from, $week2_till, null, $dynamic))
            )
        );

        $week1 = self::dynamicGetTop($type, $form, $week1_from, $week1_till, $values, $dynamic);
        $week2 = self::dynamicGetTop($type, $form, $week2_from, $week2_till, $values, $dynamic);

        $result = [];
        if(count($week1)){
            $i = 0;
            foreach($week1 as $id => $value){
                $i++;

                $result[$id] = [
                    $type               => $id,
                    'week1_value'       => round($value, 2),
                    'week2_value'       => 0,
                    'week1_position'    => $i,
                    'week2_position'    => null,
                ];
            }
        }



        if(count($week2)){
            $i = 0;
            foreach($week2 as $id => $value){
                $i++;

                if(!isset($result[$id])){
                    $result[$id] = [
                        $type              => $id,
                        'week1_value'       => 0,
                        'week2_value'       => round($value, 2),
                        'week1_position'    => null,
                        'week2_position'    => $i,
                    ];
                }
                else {
                    $result[$id]['week2_value']     = round($value, 2);
                    $result[$id]['week2_position']  = $i;
                }
            }
        }

        $total = [
            'week1_value' => 0,
            'week2_value' => 0,
        ];

        if(count($result)){
            foreach($result as $el){
                $total['week1_value'] = round((float)$total['week1_value'] + (float)$el['week1_value'], 2);
                $total['week2_value'] = round((float)$total['week2_value'] + (float)$el['week2_value'], 2);
            }
        }

        $total['values_delta'] = round($total['week1_value'] - $total['week2_value'], 2);

        if($total['values_delta'] > 0){
            $total['values_delta'] = "<span style='color:#3cc051'>{$total['values_delta']}</span>";
        }
        else if($total['values_delta'] < 0){
            $total['values_delta'] = "<span style='color:#ed4e2a'>{$total['values_delta']}</span>";
        }
        else {
            $total['values_delta'] = "<span style='color:#BBB'>{$total['values_delta']}</span>";
        }

        $table = new Table($result);

        $table->setPagingDisabled();

        $table->addField_Pattern("", "")
            ->setWidthMin()
            ->addDecoratorFunction(function($value, $context){
                if(is_numeric($context['week1_position']) && is_numeric($context['week2_position'])){
                    if($context['week1_position'] < $context['week2_position']){
                        return "<b style='color:#3cc051'>+" .
                        ($context['week2_position'] - $context['week1_position']) .
                        "</b>";
                    }
                    else if($context['week1_position'] > $context['week2_position']){
                        return "<b style='color:#ed4e2a'>" .
                        ($context['week2_position'] - $context['week1_position']) .
                        "</b>";
                    }
                    else {
                        return "<b style='color:#BBB'>+0</b>";
                    }
                }
                else if(is_numeric($context['week1_position'])){
                    return "<b style='color:#3cc051; font-size: 10px'>new</i></b>";
                }
                else if(is_numeric($context['week2_position'])){
                    return "<b style='color:#ed4e2a; font-size: 10px'>down</i></b>";
                }
            });



        if($type == 'agent'){
            $table->addField_UserLogin('agent');
        }
        else if($type == 'webmaster'){
            $table->addField_WebmasterName("webmaster");
            $table->addField_WebmasterAgentLogin("webmaster");
        }
        else if($type == 'channel'){
            $table->addField_WebmasterChannel("channel");
        }
        else if($type == 'channel_type'){
            $table->addField_SetArray("channel_type", "Channel Type", new Lead_ChannelType());
        }
        else if($type == 'domain'){
            $table->addField_WebmasterDomain("domain");
        }
        else if($type == 'product'){
            $table->addField_ProductLabel("product", "Product");
        }
        else if($type == 'region'){
            $table->addField_Region("region", "Region");
        }
        else {
            $table->addField_Text($type, $type);
        }

        //$table->startGroup("Webmaster Earnings");

        $table->addField_Text(
            "week1_value",
            "{$days} days: <span style='font-weight:100; font-size: 12px'>" .
            date('M d', $w1_from_mk) . " &nbsp;-&nbsp; " . date('M d', $w1_till_mk) .
            "</span>"
        )
            ->setTextAlignCenter();

        $table->addField_Text(
            "week2_value",
            "Previous: <span style='font-weight:100; font-size: 12px'>" .
            date('M d', $w2_from_mk) . " &nbsp;-&nbsp; " . date('M d', $w2_till_mk) .
            "</span>"
        )
            ->setTextAlignCenter();

        $table->addField_Pattern("", "", "values_delta")
            ->setWidthMin()
            ->addDecoratorFunction(function($value, $context){
                if($context['week1_value'] > $context['week2_value']){
                    return "<span style='color:#3cc051'>+" .
                    ($context['week1_value'] - $context['week2_value']) .
                    "</span>";
                }
                else if($context['week1_value'] < $context['week2_value']){
                    return "<span style='color:#ed4e2a'>" .
                    ($context['week1_value'] - $context['week2_value']) .
                    "</span>";
                }
                else {
                    return "<span style='color:#BBB'>+0</span>";
                }

            });

        $table->addFooter($total);

        // добавление кнопок перехода
        $table = $this->addPerformanceButtons($table, $form, $type);

        echo
            $tabsMarket->render() .
            $this->renderTimezone() .
            $tabs
                ->setText(
                    $form->render() .
                    $table->render()
                )
                ->render();

        Webmaster_Url::convertUrlForWebmasters('webmaster');
    }

    protected function renderTimezone(){
        ob_start();
        ?>
            <div style="float: right; font-family: Arial, 'sans-serif'; letter-spacing: 0; position: relative; top:-15px; height: 10px; padding-right: 11px">
                <span style="font-size: 10px;" class="txt-color-blueLight">timezone:</span>
                <span style="font-size: 12px;" class="txt-color-blueDark">
                    <?=Lead_Market::getTimezone(Db::getDefaultMarket())?>
                </span>
            </div>
        <?
        return ob_get_clean();
    }


    /***************************************************************************************/

}