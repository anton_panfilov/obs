<?php

class Apps_Account_Servers extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('account/servers');

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('Servers', "/account/servers/");
        }
    }

    function postLoad(){
        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $menu->add('leftMenu_gitUpdate',    "/account/servers/",            'fa-user');

        if(Boot::isDev()){
            $menu->add('leftMenu_updateLocalCron', "/account/servers/update-local-cron", 'fa-clock-o');
        }

        $this->addToSidebar($menu->render());
    }

    /************************************************************/

    function indexAction(){
        $this->setPageTitle("title_gitUpdate");

        $key = '9mn8I76cDFv32HFzb8';
        // show=json1

        $servers = Conf::servers()->update_list;

        $data = [];
        $js = "";
        $updateArray = [];

        if(is_array($servers) && count($servers)) {
            foreach($servers as $k => $server) {
                $updateArray["s{$k}"] = 0;
                $host                 = parse_url($server['git']);

                $data[] = [
                    'id'      => $k,
                    'host'    => $host['host'],
                    'link'    => $server['git'],
                    'details' => "<div id='update_details_{$k}'>...</div>",
                ];

                $js .= "document.updateServer{$k}();\r\n";

                Site::addJSCode("
                    jQuery.getJSON(
                        '{$server['ip']}?show=jsonp&jsonp=?',
                        function(data){
                            jQuery('#update_ip_{$k}').html(data.ip);
                        }
                    );

                    document.updateServer{$k} = function(){
                        if(document.updateAllServersNow['s{$k}'] == 0){
                            console.log('------------------------------------------');
                            console.log('Start - {$k}');

                            document.updateAllServersNow['s{$k}'] = 1;

                            jQuery('#update_git_{$k}').html(\"<img src='/static/img/load/16x10.gif'>\");
                            jQuery('#update_cron_{$k}').html('');
                            jQuery('#update_runtime_{$k}').html('');

                            jQuery.getJSON(
                                '{$server['git']}?key={$key}&show=jsonp&jsonp=?',
                                function(data){
                                    console.log('------------------------------------------');
                                    console.log('Finish - {$k}');
                                    console.log(data);

                                    // git
                                    var git_status = \"<i class='fa fa-warning txt-color-red'></i>\";

                                    if(data.git_status == 'ok')         git_status = \"<i class='fa fa-check-circle txt-color-green'></i>\";
                                    else if(data.git_status == 'error') git_status = \"<i class='fa fa-times-circle txt-color-yellow'></i>\";

                                    jQuery('#update_git_{$k}').html(git_status);


                                    // cron
                                    var cron_status = \"<i class='fa fa-warning txt-color-red'></i>\";

                                    if(data.cron_status == 'ok')         cron_status = \"<i class='fa fa-check-circle txt-color-green'></i>\";
                                    else if(data.cron_status == 'error') cron_status = \"<i class='fa fa-times-circle txt-color-yellow'></i>\";
                                    else if(data.cron_status == 'blank') cron_status = \"<i class='fa fa-circle-thin txt-color-blueLight'></i>\";

                                    jQuery('#update_cron_{$k}').html(cron_status);


                                    // details
                                    jQuery('#update_runtime_{$k}').html((Math.round(data.runtime * 100) / 100) + ' <small>sec</small>');
                                    jQuery('#update_details_{$k}').html(data.text);

                                    document.updateAllServersNow['s{$k}'] = 0;
                                }
                            );
                        }
                    }
                ");
            }
        }

        Site::addJSCode("
            document.updateAllServersNow = " . Json::encode($updateArray) . ";
            document.updateAllServers = function(){
                {$js}
            }
        ");

        $table = new Table($data);
        $table->setPagingDisabled();
        $table->setDetailsField('details');
        $table->addButton("Update All", "update_all")
            ->setScript("document.updateAllServers();");

        $table->addField_Pattern("<div id='update_git_{id}'></div>", "Git")
            ->setTextAlignCenter()
            ->setWidthMin();

        $table->addField_Pattern("<div id='update_cron_{id}'></div>", "Cron")
            ->setTextAlignCenter()
            ->setWidthMin();

        $table->addField_Pattern("<div id='update_runtime_{id}'></div>", "Runtime")
            ->setTextAlignCenter()
            ->setWidthMin();

        $table->addField_Pattern("<div id='update_ip_{id}'><img src='/static/img/load/16x10.gif'></div>", "IP")
            ->setDescription("IP при запросах с этого сервера")
            ->setWidth("100px");

        $table->addField_Text("host", "Host");

        $table->addField_Button("Update")
            ->setScript("document.updateServer{id}()");

        echo $table->render();
    }


    function updateLocalCronAction(){
        $this->setTitle("title_updateLocalCron");

        Site::addJSCode("
            document.resizeIframe = function(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        ");

        ?>
        <iframe
            id="cronUpdateIframe"
            width="100%"
            frameborder="0"
            src="<?=Http_Url::getCurrentScheme()?>://<?=Conf::main()->domain_account?>/api/update-local-cron"
            onload='document.resizeIframe(this)'
            ></iframe>
        <?
    }

}