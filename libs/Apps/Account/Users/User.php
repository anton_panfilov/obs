<?php

class Apps_Account_Users_User extends Controller_Abstract {
    /**
     * @var User
     */
    protected $user;

    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/users');


        // проверка по ID
        if(isset($_GET['id'])){
            $this->user = User::getObject($_GET['id']);
        }

        if($this->user->id){
            // проверка на права доступа
            if(
                UserAccess::isAdmin()
            ){
                $this->setHTMLTitle(Translate::t("title_userDefault", ['login' => $this->user->login]));
            }
            else {
                $this->setFollow_Error403();
            }
        }
        else {
            $this->setFollow_Error404();
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add('preview_username',  $this->user->login);
        $preview->add('preview_name',   $this->user->getFullName());
        $preview->add('preview_role',   (new User_Role())->getLabel($this->user->role));
        $preview->add('preview_status', (new User_Status())->setInLine()->render($this->user->status));
        $preview->add('preview_create', (new Decorator_Date())->render($this->user->create_date));

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $id = $this->user->id;

        $menu->add('leftMenu_userInfo',             "/account/users/user/?id={$id}",            'fa-user');
        $menu->add('leftMenu_userContacts',         "/account/users/user/contacts?id={$id}",    'fa-phone');
        $menu->add('leftMenu_userStatus',           "/account/users/user/status?id={$id}",      'fa-check-circle-o');
        $menu->add('leftMenu_userChangePassword',   "/account/users/user/password?id={$id}",    'fa-lock');

        if(UserAccess::isAdmin()){
            $menu->add('leftMenu_userSource',   "/account/users/user/source?id={$id}",         'fa-code');
        }


        /*********************************
         * breadcrumbs
         */
            $add = [];

            if(UserAccess::isAdmin()){
                $add[] = [
                    (new User_Role())->getLabel($this->user->role),
                    "/account/users/?role=" . $this->user->role
                ];
            }

            $add[] = [
                (new User_Status())->getLabel($this->user->status),
                "/account/users/?status=" . $this->user->status
            ];


            $this->addBreadcrumbElement(
                'breadcrumb_usersList',
                "/account/users/",
                'icon-list',
                $add
            );

            $this->addBreadcrumbElement(
                $this->user->login,
                "/account/users/user/?id=" . $this->user->id
            );

        $this->addToSidebar("<img style='margin:10px; border-radius: 5% !important; height: 203px' src='" .
            $this->user->getAvatarLink(203) .
        "'>");
        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    function indexAction(){
        $this->setPageTitle("title_userSettings");

        $form = new Form();

        if(UserAccess::isAdmin()){
            $form->addButton('Save');

            $form->addElementStatic("form_status", (new User_Status())->setInLine()->render($this->user->status));
            $form->addElementStatic("form_create", (new Decorator_Date())->render($this->user->create_date));
            $form->addElementStatic("form_role",   (new User_Role())->getLabel($this->user->role));


            $form->addSeparator();
            $form->addElementStatic("form_login", "<b>{$this->user->login}</b>");

            $form->addElementText('name', 'form_name')
                ->setValue($this->user->name);;

            $form->addElementText_Email('email', 'form_email')
                ->setValue($this->user->email);

            /*
            $form->addElementText_Phone("phone", "form_phone")
                ->setRequired(false)
                ->setValue($this->user->phone);
            */

            if($form->isValidAndPost()){
                $form->addSuccessMessage();

                $this->user->name   = $form->getValue('name');
                $this->user->email  = $form->getValue('email');
                //$this->user->phone  = $form->getValue('phone');

                $this->user->update(['name', 'email'/*, 'phone'*/]);
            }
        }
        else {
            $form->addButton('btn_save');

            $form->addElementStatic("form_role",  (new User_Role())->getLabel($this->user->role));

            $form->addElementStatic("form_login", "<b>{$this->user->login}</b>");

            $form->addElementText('name', 'form_name')
                ->setValue($this->user->name);

            $form->addElementStatic("form_email", "{$this->user->email}")
                ->setComment("For security you can not change email and phone");

            //$form->addElementStatic("form_phone", "{$this->user->phone}");

            if($form->isValidAndPost()){
                $form->addSuccessMessage();

                $this->user->name = $form->getValue('name');

                $this->user->update(['name']);
            }
        }

        echo $form->render();
    }

    /********************************************************************************************/

    protected function contactsAction_main(){
        return $this->user->renderContactsForm();
    }

    protected function contactsAction_log(){
        $select = Db::site()->select("users_contacts_log")
            ->where("user=?", $this->user->id)
            ->order('id desc');

        $table = new Table($select);

        $table->addField_Text('contacts', 'tbl_contacts')->addDecoratorFunction(function($v){
            return String_Highlight::render(
                String_Highlight::jsonPrettyPrint($v),
                false
            );
        });

        $table->addField_Date('create', 'tbl_logCreate')->setWidthMin();
        $table->addField_UserLogin('changer', 'tbl_changer')->setWidthMin();

        if(UserAccess::isAdmin()){
            $table->addField_IP('ip', 'tbl_ip')->setWidthMin();
        }

        return $table->render();
    }

    function contactsAction(){
        $this->setPageTitle("title_userContacts");

        $tabs = new Tabs_StaticTabs();
        $tabs->addTab('main', 'tab_user_contacts');
        $tabs->addTab('log',  'tab_user_contacts_log');

        $method = 'contactsAction_' . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $tabs->render();
    }

    /********************************************************************************************/

    function statusAction_set(){
        $form = new Form();

        if(
            UserAccess::isAdmin() &&
            in_array($this->user->status, [User_Status::ACTIVE, User_Status::LOCK])
        ){
            $form->addElementRadio('status', 'Status', [
                User_Status::ACTIVE => 'Active',
                User_Status::LOCK   => 'Lock',
            ])
                ->setValue($this->user->status);

            $form->addElementTextarea('reason', 'Reason');

            if($form->isValidAndPost()){
                if($form->getValue('status') != $this->user->status){
                    $form->addSuccessMessage('Saved');

                    $this->user->status = $form->getValue('status');
                    $this->user->update(['status']);

                    if($this->user->status == User_Status::LOCK){
                        $this->user->lockTrigger();
                    }

                    User_Log::changeStatus(
                        $this->user->id,
                        $this->user->status,
                        $form->getValue('reason')
                    );

                    $form->setValue('reason', '');
                }
            }
        }
        else {
            $form->disableButtons();
            $form->addElementStatic('Status', (new User_Status())->setInLine()->render($this->user->status));
        }

        return $form;
    }

    function statusAction_log(){
        $select = Db::site()->select('users_change_status_log')
            ->where("`user`=?", $this->user->id)
            ->order('id desc');

        $table = new Table($select);

        // id	datetime	user

        $table->addField_SetArray('status', 'status', new User_Status());
        $table->addField_Date('datetime', 'Date');

        $table->startGroup('Changer');
        $table->addField_UserLogin('admin', 'User');
        $table->addField_IP('ip', 'IP');
        $table->addField_Text('reason', 'Reason');
        $table->endGroup();

        return $table->render();
    }

    function statusAction(){
        $this->setPageTitle("User Status");

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("set", 'Settings');
        $tabs->addTab("log", 'Log');

        $method = 'statusAction_' . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $tabs;
    }

    /********************************************************************************************/

    function passwordAction(){
        $this->setPageTitle("Change Password");

        // админ не может явно поменять пароль другому админу
        if(UserAccess::isAdmin() && $this->user->role != User_Role::ADMIN) {
            $form = new Form();
            $form->addButton('Change Password');

            $form->addElementRadio("password_type", "", [
                'random'    => 'Random Password',
                'input'     => 'Input Password',
            ])
                ->setValue('random');

            $form->addElementPasswordNew_Ligths('password', '');

            $randomPassLen = 16;
            $random = (isset($_POST['randomPassword']) && strlen($_POST['randomPassword']) == $randomPassLen) ?
                $_POST['randomPassword'] :
                String_Hash::randomString($randomPassLen);

            $form->addElementHidden("randomPassword", $random);
            $form->addElementStatic("",
                "
                    <p><a href='https://" . Conf::main()->domain_account . "/'>https://" . Conf::main()->domain_account . "/</a></p>
                    <p>Login: <b>{$this->user->login}</b></p>
                    <p>Password: <b>{$random}</b></p>
                ",
                'randomText'
            );

            $form->addConditionalFileds_Array('password_type', 'input',  ['password', 'password_confirm']);
            $form->addConditionalFileds_Array('password_type', 'random', 'randomText');

            if($form->isValidAndPost()){
                $form->addSuccessMessage('Password Changed Complete');

                $this->user->setPassword(
                    $form->getValue('password_type') == 'input' ?
                        $form->getValue('password') :
                        $random,
                    true, // save
                    true  // temp pass
                );
            }

            echo $form->render();
        }
        else {
            $link = "https://" . Conf::main()->domain_account . "/password-recovery";

            echo "
                <p>For security purposes you can not change the password,
                but the " . $this->user->getFullName() . " can change own password by clicking on the link:</p>
                <p><a style='font-size: 120%' href='{$link}'>{$link}</a></p>
            ";
        }

    }

    ////////////////////////////////////////////////////////////////////////////////

    function sourceAction(){
        echo String_Highlight::render(
            String_Highlight::jsonPrettyPrint($this->user->getParams()),
            0
        );
    }
}