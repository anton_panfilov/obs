<?php

class Apps_Account_Users_Index extends Controller_Abstract {
    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/users');

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('breadcrumb_users', "/account/users/");
        }
    }

    function postLoad(){
        $menu = new View_Panel_Menu();

        if(UserAccess::isAdmin()){
            $menu->addGroup('leftMenu_createUser',      'fa-user')
                ->add('leftMenu_createAdmin',           '/account/users/create-admin')
                ->add('leftMenu_createClientManager',   '/account/users/create-client-manager');
        }

        $menu->add('leftMenu_usersList',            '/account/users/',       'fa-list');

        $this->addToSidebar(
            $menu->render()
        );
    }

    /**************************************************************************************/

    /**
     * Список постингов
     */
    function indexAction(){
        $this->setTitle('title_usersList');

        $form = new Form();
        $form->setMethodGET();

        $form->addElementSelect('role', 'form_role', (new User_Role)->getArrayToSelect('form_role_all'));

        $form->addElementSelect('status', 'form_status', (new User_Status)->getArrayToSelect('form_status_all'))
            ->setValue(User_Status::ACTIVE);

        $form->addElementText('search', 'form_searchString')
            ->setRequired(false);

        $form->isValidNotErrors();

        ////////////////////////////////
        $select = Db::site()->select(
            "users",
            ['id', 'create_date', 'status', 'role', 'login', 'email', 'company_id', 'name']
        )
            ->order("id desc");

        if($form->getValue('status'))   $select->where("`status`=?", $form->getValue('status'));

        if(UserAccess::isAdmin()){
            if($form->getValue('role'))     $select->where("`role`=?", $form->getValue('role'));
        }
        else {
            $select->where("1=0");
        }

        if(strlen($form->getValue('search'))){
            $search = explode(" ", $form->getValue('search'));

            $emails = [];
            $names = [];
            foreach($search as $el){
                $el = trim($el);
                if(strlen($el)){
                    if(strpos($el, '@') !== false){
                        $emails[] = $el;
                    }
                    else {
                        $names[] = Db::site()->escape($el);
                    }
                }
            }

            if(count($emails)){
                $select->where_in('`email`=?', $emails);
            }

            if(count($names)){
                $values = implode(",", $names);
                $select->where("(`login` in ({$values}) or `first_name` in ({$values}) or `last_name` in ({$values}))");
            }
        }

        $tabs = new Tabs_StaticTabs();
        $tabs->setValueName("viewType");
        if(isset($_GET['viewType'])) {
            $form->addElementHidden("viewType", $_GET['viewType']);
        }

        $tabs->addTab("list", "tab_list");

        if($form->getValue('role') == User_Role::CLIENT_MANAGER){
            $tabs->addTab("cm_cards", "tab_cards");
        }

        $method = "indexAction_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs->setText($this->$method($form, $select));
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $form->render() . $tabs->render();
    }

    function indexAction_cm_cards(Form $form, Db_Statment_Select $select){
        $res = "";
        $all = $select->fetchAll();

        foreach($all as $el){
            $res.=
                "<div style='margin-bottom: 10px; padding-bottom: 10px; border-bottom: #EEE solid 1px; width: 500px; float: left;'>" .
                    (new View_User_WebmasterAgent(
                        $el['id']
                    ))->setLink("/account/users/user/webmaster-agent?id={$el['id']}") .
                "<br clear='both'></div>";
        }

        return $res . "<br clear='both'>";
    }

    function indexAction_list($form, $select){
        ////////////////////////////////
        // table
        $table = new Table($select);
        $table->setPagingAuto(100);

        $table->addField_Text('id', 'tbl_id')->setLink(
            "/account/users/user/?id={id}"
        );
        $table->addField_Avatar('email', '', 'avatar');
        $table->addField_SetArray('role', 'tbl_role', (new User_Role()));
        $table->addField_SetArray('status', 'tbl_status', new User_Status());
        $table->addField_Text('login', 'tbl_login')->setLink(
            "/account/users/user/?id={id}"
        );

        $table->addField_Text('email', 'tbl_email');
        $table->addField_Text('name', 'tbl_name');
        $table->addField_Date('create_date', 'tbl_create');

        // текст под таблицу
        $addText = "";


        return $table->render() . $addText;
    }

    /*******************************************************************************************/

    function getCreateUserForm(){
        $form = new Form();
        $form->addButton("btn_createUser", 'create');

        Site::addJSCode("
            document.setLogiFromName = function(){
                var first = jQuery('#first_name').val();
                var last = jQuery('#last_name').val();

                if(first.length && last.length){
                    jQuery('#login').val(
                        first.toLowerCase() + '.' + last.toLowerCase()
                    );
                    jQuery('#login').blur();
                }
            };

            jQuery('#first_name').blur(function(){
                document.setLogiFromName();
            });

            jQuery('#last_name').blur(function(){
                document.setLogiFromName();
            });

            jQuery('#login').keyup(function(){
                jQuery('#login_str_doc').html(jQuery('#login').val());
            });

            jQuery('#login').change(function(){
                jQuery('#login_str_doc').html(jQuery('#login').val());
            });

            jQuery('#login').blur(function(){
                jQuery('#login_str_doc').html(jQuery('#login').val());
            });

        ");

        $form->addElementText_FirstAndLastName('first_name', 'last_name');
        $form->addElementText_Email('email', 'form_email');

        $form->addSeparator();
        $form->addElementText_NewLogin('login');

        $form->addElementRadio("password_type", "", [
            'random'    => 'form_password_randomPassword',
            'input'     => 'form_password_inputPassword',
        ])
            ->setLabelWidth('100%')
            ->setValue('random')
            ->setRenderTypeBigValue();

        $form->addElementPasswordNew_Ligths('password', '');

        $randomPassLen = 16;
        $random = (isset($_POST['randomPassword']) && strlen($_POST['randomPassword']) == $randomPassLen) ?
            $_POST['randomPassword'] :
            String_Hash::randomString($randomPassLen);

        $form->addElementHidden("randomPassword", $random);
        $form->addElementStatic("",
            "
                <p><a href='https://" . Conf::main()->domain_account . "/'>https://" . Conf::main()->domain_account . "/</a></p>
                <p>" . Translate::t("form_randomPassword_login") . ": <b id='login_str_doc'></b></p>
                <p>" . Translate::t("form_randomPassword_password") . ": <b>{$random}</b></p>
            ",
            'randomText'
        );

        $form->addConditionalFileds_Array('password_type', 'input',  ['password', 'password_confirm']);
        $form->addConditionalFileds_Array('password_type', 'random', 'randomText');

        $form->addSeparator();
        $form->addElementCheckbox('sendCredentialsToEmail', 'form_sendCredentialsToEmail')
            ->setValue('1');

        return $form;
    }

    function createAdminAction(){
        if(!UserAccess::isAdmin()){
            $this->setFollow_Error403();
            return;
        }

        $this->setTitle('title_createAdmin');
        $form = $this->getCreateUserForm();
        $form->addButton("btn_createAdmin", 'create');

        if($form->isValidAndPost()){
            Db::site()->transactionStart();

            try {
                $password = ($form->getValue('password_type') == 'input') ?
                    $form->getValue('password') :
                    (
                        isset($_POST['randomPassword']) ?
                            $_POST['randomPassword'] :
                            String_Hash::randomString(16)
                    );

                $role = User_Role::ADMIN;

                $user = new User();
                $user->createNew(
                    $form->getValue('login'),
                    $password,
                    $role,
                    $form->getValue('email'),
                    $form->getValue('first_name') . " " . $form->getValue('last_name'),
                    null,
                    User_Status::ACTIVE,
                    null,
                    null,
                    true
                );

                if($form->getValue('sendCredentialsToEmail')){
                    Email_Templates::createUser(
                        $user->email,
                        $user->name,
                        $user->login,
                        $password,
                        $role
                    );
                }

                header('location: /account/users/user/?id=' . $user->id);

                Db::site()->transactionCommit();
            }
            catch(Exception $e){
                Db::site()->transactionRollback();
                throw $e;
            }
        }

        echo $form->render();
    }

    /*******************************************/

    function createClientManagerAction(){
        if(!UserAccess::isAdmin()){
            $this->setFollow_Error403();
            return;
        }

        $this->setTitle('title_createClientManager');

        $form = $this->getCreateUserForm();
        $form->addButton("btn_createClientManager", 'create');

        if($form->isValidAndPost()){
            Db::site()->transactionStart();

            try {
                $password = ($form->getValue('password_type') == 'input') ?
                    $form->getValue('password') :
                    (
                    isset($_POST['randomPassword']) ?
                        $_POST['randomPassword'] :
                        String_Hash::randomString(16)
                    );

                $role = User_Role::CLIENT_MANAGER;

                $user = new User();
                $user->createNew(
                    $form->getValue('login'),
                    $password,
                    $role,
                    $form->getValue('email'),
                    $form->getValue('first_name') . " " . $form->getValue('last_name'),
                    null,
                    User_Status::ACTIVE,
                    null,
                    null,
                    true
                );

                Db::site()->insert("users_client_managers", [
                    'id' => $user->id,
                ]);

                if($form->getValue('sendCredentialsToEmail')){
                    Email_Templates::createUser(
                        $user->email,
                        $user->name,
                        $user->login,
                        $password,
                        $role
                    );
                }

                header('location: /account/users/user/?id=' . $user->id);

                Db::site()->transactionCommit();
            }
            catch(Exception $e){
                Db::site()->transactionRollback();
                throw $e;
            }
        }

        echo $form->render();
    }

    /*******************************************************************************************/

    /**
     * Helper for sending credentials
     *
     * @param $email
     * @param $name
     * @param $login
     * @param $pass
     * @return mixed|string
     */
    public function sendCredentials($email, $name, $login, $pass) {


        $link = 'https://' . Conf::main()->domain_account;
        $mail = new Email_Sender();
        $mail->credentials($email, $name, $login, $pass, $link);

        return $mail->send();
    }
}