<?php

class Apps_Account_Profile extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('account/profile');

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('breadcrumb_profile', "/account/profile/");
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add('preview_username',   Users::getCurrent()->login);
        $preview->add('preview_name',       Users::getCurrent()->getFullName());

        if(!UserAccess::isClient()) {
            $preview->add('preview_role', (new User_Role())->getLabel(Users::getCurrent()->role));
        }

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $menu->add('leftMenu_profile',          "/account/profile/",                        'fa-user');
        $menu->add('leftMenu_vk',               "/account/profile/vk",                      'fa-vk');
        $menu->add('leftMenu_contacts',         "/account/profile/contacts",                'fa-phone');
        $menu->add('leftMenu_changePassword',   "/account/profile/password-change",         'fa-lock');

        $this->addToSidebar("<img style='margin:10px; border-radius: 5% !important; height: 203px' src='" .
            Users::getCurrent()->getAvatarLink(203) .
        "'>");

        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /************************************************************/

    function indexAction(){
        $this->setPageTitle("title_profile");

        $form = new Form();
        $form->addButton('save');

        $form->addElementStatic("form_username", Users::getCurrent()->login);
        $form->addElementStatic("form_email",    Users::getCurrent()->email);
        $form->addElementStatic("form_password", "***************  <a href='/account/profile/password-change'>" . Translate::t("form_password_change") . "</a>");

        $form->addElementText('name', 'form_name')
            ->setValue(Users::getCurrent()->name);


        $form->addElementStatic(
            'form_picture',
            "<img
                style='border-radius: 50% !important; margin-right: 7px'
                src='" . Users::getCurrent()->getAvatarLink(28) . "'
            > " .
            "<a href='https://gravatar.com/' target='_blank'>" . Translate::t("form_picture_changer") . "</a>"
        );

        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            Users::getCurrent()->name = $form->getValue('name');
            Users::getCurrent()->update(['name']);
        }

        echo $form;
    }

    function vk_settings(){
        $api = new Vk_AUploader();

        if(isset($_GET['vkOAuth'])){
            $api->login();
            return;
        }

        $form = new Form();

        // buttons
        if(UserAccess::isAdmin()){
            $form->addButton("btn_login", "login");

            if($api->getUserID()){
                $form->addButton("btn_logout", "logout", false);
            }
        }

        /////////////////////////////////////////////////////
        $form->addSeparator("form_sep_vk_current_user");

        if(isset($api->getProfile()['photo_50'])){
            $form->addElementStatic(
                "",
                "<img src='" . $api->getProfile()['photo_50'] . "' />"
            );
        }

        if(isset($api->getProfile()['first_name'], $api->getProfile()['last_name'])){
            $form->addElementStatic(
                "form_vk_current_user_name",
                $api->getProfile()['first_name'] . " " . $api->getProfile()['last_name']
            );
        }

        $form->addElementStatic(
            "form_vk_current_user",
            $api->getUserID() ?
                $api->getUserID() :
                Translate::t("form_vk_current_user_null")
        );

        if(UserAccess::isAdmin() && $form->isValidAndPost()){
            if($form->getButtonClicked() == 'logout'){
                $api->logout();
                header("location: " . Http_Url::getCurrentURL());
            }
            else if($form->getButtonClicked() == 'login'){
                $api->login();
            }
        }

        return $form->render();
    }

    function vkAction(){
        $this->setPageTitle("title_vk");

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("settings",   "tab_vk_settings");

        if(UserAccess::isAdmin()){
            $tabs->addTab("logs",       "tab_vk_logs");
        }

        $method = "vk_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $tabs->render();
    }

    function contactsAction(){
        $this->setPageTitle("title_contacts");

        echo Users::getCurrent()->renderContactsForm();
    }

    function passwordChangeAction(){
        $this->setPageTitle("title_changePassword");

        $form = new Form();
        $form->addButton('Save');

        $form->addElementPasswordNew('password', '');
        $form->getElement('password')->setLabel('New Password');
        $form->addElementCaptcha('new_password')
            ->setOneCheckSession(true);


        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            Users::getCurrent()->setPassword($form->getValue('password'));
            Users::setLoginCookie(Users::getCurrent());
        }

        echo $form;
    }
}