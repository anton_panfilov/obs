<?php

class Apps_Account_Catalog_Index extends Controller_Abstract {
    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/catalog');

        if($this->_action != 'index'){
            $this->addBreadcrumbElement('breadcrumb_catalog', "/account/catalog/");
        }
    }

    function postLoad(){
        $menu = new View_Panel_Menu();

        if(UserAccess::isAdmin() || UserAccess::isClientManager()){
            $menu->add('leftMenu_create', '/account/catalog/create', 'fa-plus');
        }

        $menu->add('leftMenu_search_by_id', '/account/catalog/search', 'fa-search');
        $menu->add('leftMenu_all_list', '/account/catalog/', 'fa-th-large');

        /*
        if(UserAccess::isClientManager()){
            $counts = Db::site()->fetchPairs(
                "select `status`, count(*) from catalog_remains where `manager`=? group by `status`",
                Users::getCurrent()->id
            );
        }
        else {
        */
        $counts         = Db::site()->fetchPairs("select `status`, count(*) from catalog_remains group by `status`");
        $countsInStock  = Db::site()->fetchPairs("select IF(`order`=0, 0, 1) as `order`, count(*)  from catalog_remains where `status`=30 group by IF(`order`=0, 0, 1)");
        //}

        $lists = $menu->addGroup("leftMenu_lists", 'fa-list');
        foreach((new Catalog_RemainsStatus)->getArrayToSelect() as $k => $v){
            if($k == Catalog_RemainsStatus::IN_STOCK){
                // свободны
                $v = "<span style='font-size: 90%'>{$v}</span>";
                $count = (isset($countsInStock[0]) ? $countsInStock[0] : 0);
                $v.=
                    " <span style='font-size: 70%' class='badge " . ($count ? "bg-color-green" : "") . " pull-right inbox-badge'>{$count}</span>";

                $lists->add($v, '/account/catalog/remains-list?is_order=0&status=' . $k, null, true);

                // заняты
                $v = "<span style='font-size: 90%'>" . Translate::t("leftMenu_label_in_stock_to_order") . "</span>";
                $count = (isset($countsInStock[1]) ? $countsInStock[1] : 0);
                $v.=
                    " <span style='font-size: 70%' class='badge " . ($count ? "bg-color-green" : "") . " pull-right inbox-badge'>{$count}</span>";

                $lists->add($v, '/account/catalog/remains-list?is_order=1&status=' . $k, null, true);
            }
            else {
                $v = "<span style='font-size: 90%'>{$v}</span>";

                if($k != Catalog_RemainsStatus::SOLD){
                    $count = (isset($counts[$k]) ? $counts[$k] : 0);

                    $v.=
                        " <span style='font-size: 70%' class='badge " . ($count ? "bg-color-green" : "") . " pull-right inbox-badge'>{$count}</span>";
                }

                $lists->add($v, '/account/catalog/remains-list?status=' . $k, null, true);
            }
        }


        //$menu->add('leftMenu_vk_wall', '/account/catalog/vk-wall', 'fa-vk');

        $this->addToSidebar(
            $menu->render()
        );
    }

    /**************************************************************************************/

    /**
     * Список постингов
     */
    function indexAction(){
        $this->setTitle('title_list');

        $form = new Form();
        $form->setMethodGET();
        $form->addButton('btn_show');

        $form->addElement_CatalogUser("user", 'form_user');

        $form->addElementRadio("status", "form_status", [
            'active'    => 'form_status_active',
            'to_order'  => 'form_status_to_order',
            'in_stock'  => 'form_status_in_stock',
            'remains'   => 'form_status_remains',
            'archive'   => 'form_status_archive',
            ''          => 'form_status_all',
        ])
            ->setLabelWidth("100px")
            ->setValue('active')
            ->setRenderTypeBigValue();

        //////////////////////////
        $form->addSeparator('form_sep_specifications');
        // выбор типа
        $form->addElementSelect_CatalogType("type", "form_type", '...')
            ->setRenderTypeBigValue()
            ->setRequired(false);

        // добавление опций
        if(count(Catalog_Types::getFullParams())) {
            foreach(Catalog_Types::getFullParams() as $el) {
                $form->addElementSelect(
                    'option_' . $el,
                    Catalog_Options::getTypeName($el),
                    Catalog_Options::getTypeList($el, "...")
                )
                    ->setRequired(false)
                    ->setRenderTypeBigValue();
            }
        }

        // установка зависимостей опций от типа
        foreach(Catalog_Types::getAllTypesForConditions() as $el){
            array_walk($el['options'], function(&$v){
                $v = "option_{$v}";
            });
            $form->addConditionalFileds_Array("type", $el['ids'], $el['options']);
        }

        $form->isValidNotErrors();

        ////////////////////////////////
        $select = Db::site()->select(
            "catalog_items"
        )
            ->order("id desc");


        if($form->getValue('type')){
            $select->where(
                "`type` in (" . implode(",", Catalog_Types::getAllRelationsTypes($form->getValue('type'))) . ")"
            );
        }

        if(count(Catalog_Types::getFullParams())) {
            $only = null;

            foreach(Catalog_Types::getFullParams() as $option) {
                if(strlen($form->getValue('option_' . $option))) {
                    $ids = Db::site()->fetchCol(
                        "SELECT item FROM `catalog_options` WHERE `option`=? AND `value`=?",
                        $option,
                        $form->getValue('option_' . $option)
                    );

                    if(is_null($only)) {
                        $only = $ids;
                    }
                    else {
                        $only = array_intersect($only, $ids);
                    }
                }
            }

            if(is_array($only)){
                if(count($only)){
                    $select->where("id in (" . implode(",", $only) . ")");
                }
                else {
                    $select->where("1=0");
                }
            }
        }

        if($form->getValue('status') == 'active') {
            $select->where("(`remains_not_sale`>0 or `to_order`=1)");
        }
        else if($form->getValue('status') == 'to_order'){
            $select->where("`to_order`=1");
        }
        else if($form->getValue('status') == 'in_stock'){
            $select->where("`stock`=1");
        }
        else if($form->getValue('status') == 'remains'){
            $select->where("`remains_not_sale`>0");
        }
        else if($form->getValue('status') == 'archive'){
            $select->where("(`remains_not_sale`=0 and to_order=0)");
        }

        if($form->getValue('user')){
            $select->where("`user`=?", $form->getValue('user'));
        }

        $tabs = new Tabs_StaticTabs();
        $tabs->setValueName("viewType");
        if(isset($_GET['viewType'])) {
            $form->addElementHidden("viewType", $_GET['viewType']);
        }

        $tabs->addTab("list", "tab_list");

        $method = "indexAction_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs->setText($this->$method($form, $select));
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $form->render() . $tabs->render();
    }

    /*********************************************/

    function indexAction_list($form, $select){
        $table = new Table($select);
        $table->setPagingAuto(20);
        $table->disabledStriped();
        $table->disabledHover();

        // id	long_id	create	user	title	link	options	description	price_purchase	price_purchase_currency	price
        $table->addField_Function(function($c) {
            $item = Catalog_Item::getObject($c['id']);

            $imageLink = $item->getImageLink();

            if($imageLink){
                return "<a href='/account/catalog/item/photos?id={$c['long_id']}'><img style='max-width: 100%' src='{$imageLink}'></a><br><br>";
            }
            else{
                return "<br><center><h4>" . Translate::t("no_image") . "</h4></center><br><br>";
            }
        })
            ->setWidth("160px");

        $table->addField_Function(function($c){
            $tbl = new View_Table_Simple();

            $tbl->add("tbl_long_id", "<span>{$c['long_id']}</span>");
            $tbl->add(
                "tbl_price_purchase",
                "<span>" . number_format($c['price_purchase'] + 0) . "</span> " .
                "<small class='txt-color-blueLight'>{$c['price_purchase_currency']}</small>"
            );

            if($c['link']) {
                $url = parse_url($c['link']);

                $tbl->add(
                    "tbl_link",
                    "<a class='txt-color-greenDark' target='blank' href='{$c['link']}'>" .
                    (isset($url['host']) ? $url['host'] : substr($c['link'], 0, 32) . "...") .
                    "</a>"
                );
            }

            if($c['type']){
                $tbl->add("tbl_full_type", Catalog_Types::getTypeInfo($c['type'])['full_name']);
            }

            if(count(Catalog_Types::getTypeInfo($c['type'], 0)['full_params_real'])) {
                foreach(Catalog_Types::getTypeInfo($c['type'], 0)['full_params_real'] as $el) {
                    $str = Catalog_Options::renderString(
                        $c['id'],
                        $el,
                        $el == Catalog_Options::AGE ? "<br>" : ", "
                    );

                    if(strlen($str)){
                        $tbl->add(
                            Catalog_Options::getTypeName($el),
                            "<div style='white-space: normal'>{$str}</div>"
                        );
                    }
                }
            }

            $options = Catalog_Options::getOptions($c['id']);

            if(count($options)){
                foreach($options as $el){

                }
            }

            $flags = [];
            if($c['to_order']){
                $flags[] = "<a href='/account/catalog/item/?id={$c['long_id']}'><i class='label label-info'>" . Translate::t("flag_order") . "</i></a>";
            }

            if($c['stock']){
                $flags[] = "<a href='/account/catalog/item/remains?id={$c['long_id']}'><i class='label label-success'>" . Translate::t("flag_stock") . "</i></a>";
            }
            else if($c['remains_not_sale']){
                $flags[] = "<a href='/account/catalog/item/remains?id={$c['long_id']}'><i class='label label-default'>" . Translate::t("flag_remains_not_sale") . "</i></a>";
            }

            return
                "<h4>
                    <a href='/account/catalog/item/?id={$c['long_id']}'>" .
                    String_Filter::cutByWord($c['title'], 50) .
                    "</a>
                    <a style='font-size:10px; margin-left:12px' href='/account/catalog/item/?id={$c['long_id']}&tab=edit'>
                        <i class='fa fa-pencil'></i>
                    </a>
                </h4>" . (
                count($flags) ?
                    "<div style='margin: 5px 0 0 0'>" . implode(" ", $flags) . "</div>" :
                    ""
                ) . "<div style='float:right; font-size:90%; color:#777; '>" .
                Cache_PartialList_UserName::get($c['user']) .
                "&nbsp; <small class='txt-color-blueLight'>" .
                (new Decorator_Date())
                    ->showTime(false)
                    ->showCurrentYear(true)
                    ->render($c['create']) .
                "</small>"
                . "</div><br>" . $tbl->render() . "<br>";
        });

        // $table->addField_UserLogin("user", "tbl_user");

        $table->addField_Text("price", "tbl_price")
            ->addDecoratorFunction(function($v){
                if($v != 0) {
                    return number_format($v) . " <small class='txt-color-blueLight'>" . Translate::t("RUR") . "</small>";
                }
                else {
                    return "<span class='txt-color-blueLight'>-</span>";
                }
            })
            ->setTextAlignCenter();

        return $table->render();
    }

    /**************************************************************************************/

    function searchAction(){
        $this->setTitle("title_search");

        $form = new Form();
        $form->addButton("btn_search", null);

        $form->addElementText("n", "form_search_item_number");

        $notFullMatchTable = "";

        if($form->isValidAndPost()){
            if(UserAccess::isAdmin() && substr($form->getValue('n'), 0, 1) == '#'){
                $n = (int)(new Filter_NumbersOnly())->filter($form->getValue('n'));

                if(Catalog_Item::getObject($n)->id){
                    header(
                        "location: /account/catalog/item/?id=" .
                        Catalog_Item::getObject($n)->long_id
                    );
                }
                else {
                    $form->addErrorMessage(Translate::t("search_not_found", $n));
                }
            }
            else {
                $n = (int)(new Filter_NumbersOnly())->filter($form->getValue('n'));

                if(Catalog_Item::getObject(IdEncryption::decodeForInt($n))->id){
                    header(
                        "location: /account/catalog/item/?id=" .
                        Catalog_Item::getObject(IdEncryption::decodeForInt($n))->long_id
                    );
                }
                else {
                    $form->addErrorMessage(Translate::t("search_not_found_full_match", $n));

                    $select = Db::site()->select("catalog_items")
                        ->where("long_id like '%{$n}%'");

                    $table = new Table($select);

                    $table->addField_Function(
                        function($c){
                            $link = Catalog_Item::getObject($c['id'])->getImageLink();
                            if(strlen($link)){
                                return "<a href='/account/catalog/item/photos?id={$c['long_id']}'><img style='max-height:28px' src='{$link}'></a>";
                            }
                        })
                        ->setWidthMin()
                        ->getTdAttribs()->setCSS('padding', 0);

                    $table->addField_Function(function($c, $t, $p){
                        return "<a href='/account/catalog/item/photos?id={$c['long_id']}'>" .
                            str_replace($p['search'], "<b>{$p['search']}</b>", $c['long_id']) .
                        "</a>";
                    }, "tbl_id")
                        ->setParam("search", $n)
                        ->setWidthMin();

                    $table->addField_Text("title", "tbl_title")
                        ->addDecoratorFunction(function($v){
                            return String_Filter::cutByWord($v, 43);
                        });

                    $notFullMatchTable = "<br><br><h3>" . Translate::t("not_full_match_table_header") . "</h3>" . $table->render();
                }
            }


        }

        echo $form->render() . $notFullMatchTable;
    }

    function remainsListAction(){
        if(isset($_GET['status']) && (new Catalog_RemainsStatus())->isValid($_GET['status'])){
            $this->setTitle(Translate::t('title_remains_list') . ": " . (new Catalog_RemainsStatus())->getLabel($_GET['status']));

            $ui = new View_List_Remains();
            $ui->setStatus((int)$_GET['status']);


            echo $ui->render();
        }
        else {
            $this->setFollow_Error404();
        }
    }

    /*******************************************************************************************/

    public function createAction(){
        $this->setTitle('title_create');

        $tabs = new Tabs_StaticTabs();
        $tabs->setValueName("createType");

        $tabs->addTab("link", "tab_createLink");
        $tabs->addTab("manual", "tab_createManual");

        $method = "createAction_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $tabs->render();
    }

    function createAction_link(){
        $form = new Form();
        $form->addButton('btn_link_continue');

        $sites = Catalog_Parser_Abstract::getSites();
        foreach($sites as $k => $v){
            $sites[$k] = "<a href='http://{$v}' target='_blank'>{$v}</a>";
        }

        $form->addElementTextarea("link", "form_link")
            ->setAutoHeight()
            ->setComment(
                "<div style='background: #FFF; padding: 10px'>" .
                    "<img style='width:80px; float: left; margin: 10px 10px 10px 0'  src='/static/img/robo.jpg'>" .
                    "<h5>" . Translate::t("parsers_sites") . "</h5>" .
                    "<ul style='margin-left:90px'><li>" . implode("</li><li>", $sites) . "</li></ul>" .
                    "<div style='text-align:center'>" . Translate::t("parsers_sites_desc") . "</div>" .
                "</div>"
            );

        /*
        $form->addElementRadio("add_to_vk_toOrder_album", "form_add_to_vk_toOrder_album", [
            '1' => 'form_add_to_vk_toOrder_album_yes',
            '0' => 'form_add_to_vk_toOrder_album_no',
        ])
            ->setRequired(false)
            ->setRenderTypeBigValue()
            ->setValue('1');
        */

        if($form->isValidAndPost()){
            $object = Catalog_Parser_Abstract::getObject($form->getValue('link'));

            if($object instanceof Catalog_Parser_Abstract){
                if(
                    strlen($object->title) &&
                    $object->price > 0 &&
                    (new Currency_List)->isValid($object->price_currency)
                ){
                    $item = new Catalog_Item();

                    $item->title       = $object->title;
                    $item->link        = $object->link;
                    $item->description = $object->description;

                    $price = $object->price * 2;
                    if($object->price_currency == Currency_List::USD){
                        $price = $price * 70;
                    }
                    $item->price                    = ceil($price / 100) * 100;

                    $item->price_purchase           = $object->price;
                    $item->price_purchase_currency  = $object->price_currency;


                    // создание нового заказа
                    $item->insert();

                    if(count($object->photos)){
                        foreach($object->photos as $photo){
                            $filename = "/tmp/uploadfile_" . time() . rand(1000,9000);
                            file_put_contents($filename, file_get_contents($photo));

                            Catalog_Photo::create($item->id, $filename);
                        }
                    }

                    header('location: /account/catalog/item/?id=' . $item->long_id . "&tab=edit");

                }
                else {
                    $form->getElement("link")->addError(
                        Translate::t('error_bad_parsing', [
                            'title'             => $object->title,
                            'price'             => $object->price,
                            'price_currency'    => $object->price_currency,
                        ])
                    );
                }
            }
            else {
                $form->getElement("link")->addError('error_parser_not_found');
            }
        }

        return $form->render();
    }

    function createAction_manual(){
        return (new Catalog_Item())->renderForm();
    }

    ///////////////////////////////////////////

    /*
    function vkWall_add(){
        $form = new Form();
        $form->addButton("btn_vk_wall_add");

        $form->addElementTextarea_VKComment("comment", "form_comment");

        Site::addJSCode("
            document.formAddItem = function(txtToAdd){
                var caretPos = document.getElementById('items').selectionStart;
                var textAreaTxt = jQuery('#items').val();

                if(textAreaTxt.length != 0){
                    if(caretPos == 0){
                        txtToAdd = txtToAdd + ',';
                    }
                    else {
                        txtToAdd = ',' + txtToAdd
                    }
                }

                jQuery('#items')
                .val(
                    textAreaTxt.substring(0, caretPos) +
                    txtToAdd +
                    textAreaTxt.substring(caretPos)
                )
                .focus()
                .selectRange(caretPos + txtToAdd.length);
            }
        ");

        $comment = "";
        $all = [];
        $tmp = Db::site()->fetchCol(
            "SELECT id FROM `catalog_items` WHERE `vk_id_order_group` NOT LIKE '' AND `vk_id_order_page` NOT LIKE '' ORDER BY id DESC LIMIT 30"
        );
        foreach($tmp as $el){
            $img = Catalog_Item::getObject($el)->getImageLink();
            if($img){
                $all[] =
                    "<a onclick='document.formAddItem(\"" .
                        Catalog_Item::getObject($el)->long_id .
                    "\")'><img style='max-height: 70px; margin:3px' src='{$img}'></a>";
            }
        }
        if(count($all)){
            $comment = implode("", $all);
        }

        $form->addElementTextarea("items", "form_items")
            ->setRenderTypeBigValue()->setComment($comment);




        if($form->isValidAndPost()){
            $obj = new Vk_AUploader();

            $temp = $form->getValue('items');
            $temp = explode(",", $temp);

            $allPage = [];
            $allPageIDS = [];
            $allGroup = [];
            $allGroupIDS = [];

            foreach($temp as $el){
                $el = IdEncryption::decodeForInt((int)$el);

                if($el > 0 && Catalog_Item::getObject($el)->id){
                    if(strlen(Catalog_Item::getObject($el)->vk_id_order_page)){
                        $allPage[]= Catalog_Item::getObject($el)->vk_id_order_page;
                        $allPageIDS[]= Catalog_Item::getObject($el)->long_id;
                    }

                    if(strlen(Catalog_Item::getObject($el)->vk_id_order_group)){
                        $allGroup[]= Catalog_Item::getObject($el)->vk_id_order_group;
                        $allGroupIDS[]= Catalog_Item::getObject($el)->long_id;
                    }
                }

            }

            if(count($allPage) && count($allGroup)) {
                $resPage = [];
                $resGroup = [];

                if(count($allPage)) {
                    $resPage = $obj->api('wall.post', [
                        'message'     => Vk_Smiles::filter($form->getValue('comment')),
                        'attachments' => count($allPage) ? implode(",", $allPage) : "",
                    ]);
                }

                if(count($allGroup)) {
                    $resGroup = $obj->api('wall.post', [
                        'owner_id'    => "-" . Conf::vk()->group_id,
                        'message'     => Vk_Smiles::filter($form->getValue('comment')),
                        'attachments' => count($allGroup) ? implode(",", $allGroup) : "",
                    ]);
                }

                // https://vk.com/id313872150?w=wall313872150_6%2Fall

                $pageID = isset($resPage['response']['post_id'])   ? $resPage['response']['post_id'] : 0;
                $groupID = isset($resGroup['response']['post_id']) ? $resGroup['response']['post_id'] : 0;

                if($pageID || $groupID){
                    Db::site()->insert("catalog_vk_walls", [
                        'user'       => Users::getCurrent()->id,
                        'message'    => $form->getValue('comment'),
                        'post_group' => $groupID,
                        'post_page'  => $pageID,
                        'pageItems'  => count($allPageIDS)  ? implode(",",$allPageIDS) : "",
                        'groupItems' => count($allGroupIDS) ? implode(",",$allGroupIDS) : "",
                    ]);

                    $pid = Db::site()->lastInsertId();
                    $insert = [];
                    foreach(array_unique(array_merge($allGroupIDS, $allPageIDS)) as $el){
                        $insert[] = [
                            'pid'  => $pid,
                            'item' => $el,
                        ];
                    }
                    Db::site()->insertMulti("catalog_vk_walls_items", $insert);

                    if($pageID){
                        $link = "https://vk.com/id" . Conf::vk()->user_id . "?w=wall" . Conf::vk()->user_id . "_{$pageID}";
                        $form->addSuccessMessage("<a target='_blank' href='{$link}'>{$link}</a>");
                    }

                    if($groupID){
                        $link = "https://vk.com/club" . Conf::vk()->group_id . "?w=wall-" . Conf::vk()->group_id . "_{$groupID}";
                        $form->addSuccessMessage("<a target='_blank' href='{$link}'>");
                    }

                    $form->getElement('items')->setValue('');
                    $form->getElement('comment')->setValue('');
                }

                if(isset($resGroup['error']['redirect_uri'])){
                    $form->addErrorMessage("Error: " . $resGroup['error']['redirect_uri']);
                }

                if(isset($resPage['error']['redirect_uri'])){
                    $form->addErrorMessage("Error: " . $resPage['error']['redirect_uri']);
                }
            }
            else {
                $form->getElement('items')->addError('count_items_0');
            }
        }

        return $form->render();
    }

    function vkWall_list(){
        $form = new Form();

        $form->addElement_CatalogUser('user', 'form_user');

        $form->isValidNotErrors();

        $select = Db::site()->select("catalog_vk_walls")
            ->order('id desc');

        $table = new Table($select);
        // id	create	user	message	post_group	post_page	pageItems	groupItems

        $table->addField_Function(function($c){
            $all = array_unique(array_merge(
                strlen($c['pageItems']) ? explode(",", $c['pageItems']) : [],
                strlen($c['groupItems']) ? explode(",", $c['groupItems']) : []
            ));

            if(count($all)){
                $result = [];
                foreach($all as $el){
                    $result[] = "<a href='/account/catalog/item?id={$el}'>
                        <img style='max-height: 50px;' src='" . Catalog_Item::getObject(IdEncryption::decodeForInt($el))->getImageLink(150) . "'>
                    </a>";
                }
                return implode(" ", $result);
            }
        }, "tbl_items")
            ->setWidthMin();

        $table->addField_Text("message", "tbl_message")
            ->setTdCSS('white-space', 'normal')
            ->addDecoratorFunction(function($v){
                return Vk_Smiles::render($v);
            });

        $table->addField_Date('create', 'table_create');
        $table->addField_UserLogin('user', 'table_user');

        return $form->render() . $table->render();
    }

    function vkWallAction(){
        $this->setTitle('title_vk_wall');


        $tabs = new Tabs_StaticTabs();
        $tabs->setValueName("viewType");

        $tabs->addTab("add", "tab_vk_wall_add");
        $tabs->addTab("list", "tab_vk_wall_list");

        $method = "vkWall_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $tabs->render();
    }
    */

    ////////////////////////////////////////////////////////////////////////////////////////

    function getItemInfoAPI(){

        $item = Catalog_Item::getObject(isset($_GET['item']) ? $_GET['item'] : 0);

        return [
            'id'    => $item->id,
            'price' => $item->price,
            'photo' => $item->getImageLink(150),
        ];
    }
}