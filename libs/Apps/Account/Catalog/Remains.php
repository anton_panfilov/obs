<?php

class Apps_Account_Catalog_Remains extends Controller_Abstract {
    /**
     * @var Catalog_Remains
     */
    protected $remains;

    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/catalog');

        // проверка по ID
        if(isset($_GET['id']) && $_GET['id'] > 0){
            $this->remains = Catalog_Remains::getObject($_GET['id']);
        }

        if($this->remains->id){
            $this->setTitle(Translate::t("title_remainsDefault", ['id' => $this->remains->id]));
        }
        else {
            $this->setFollow_Error404();
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add('preview_id',      $this->remains->id);

        $preview->add(
            'preview_item',
            "<a href='/account/catalog/item/?id=" . $this->remains->getItem()->long_id . "'>" .
                $this->remains->getItem()->long_id .
            "</a>"
        );

        if($this->remains->order){
            $preview->add(
                'preview_order',
                "<a href='/account/orders/order/?id=" . $this->remains->order . "'>" .
                    $this->remains->order .
                "</a>"
            );
        }

        $preview->add('preview_status', (new Catalog_RemainsStatus())->setInLine()->render($this->remains->status));
        $preview->add('preview_count', $this->remains->count);
        $preview->add('preview_price', String_Filter::price($this->remains->price) . " <small class='txt-color-blueLight'>RUR</small>");

        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $id = $this->remains->id;

        $menu->add('leftMenu_itemForm',         "/account/catalog/remains/?id={$id}",           'fa-file-text');


        /*********************************
         * breadcrumbs
         */

        $this->addBreadcrumbElement(
            Translate::t('breadcrumb_remains_list') . ": " . (new Catalog_RemainsStatus())->getLabel($this->remains->status),
            "/account/catalog/remains-list?status=" . $this->remains->status,
            'icon-list'
        );

        $this->addBreadcrumbElement(
            Translate::t("breadcrumb_catalogItem", [
                'id' => $this->remains->getItem()->long_id,
                'title' => $this->remains->getItem()->getShortTitle()]
            ),
            "/account/catalog/item/?id=" . $this->remains->getItem()->long_id
        );

        ///////////////////////////////////

        if($this->remains->getImageLink(200)) {
            $this->addToSidebar(
                "<img style='margin:10px; border-radius: 5% !important; width: 200px' src='" .
                    $this->remains->getImageLink(200) .
                "'>"
            );
        }

        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    protected function index_main(){
        return $this->remains->renderMainPage();
    }

    protected function index_edit(){
        return $this->remains->renderForm();
    }

    function indexAction(){
        $this->setTitle(Translate::t("title_remainsMain", ['id' => $this->remains->id]));

        /**
         * 1. Обзорная страница
         *   - действия
         *      - продать
         *      - отложить
         *      - добавить в заказ
         *   - общее описание
         *   - история работы с товаром (что с ним происходило)
         *
         * 2. Редактирование
         *
         */

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("main", "tab_remainsMain");
        $tabs->addTab("edit", "tab_remainsEdit");

        $method = "index_" . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        echo $tabs->render();
    }

}