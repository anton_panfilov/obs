<?php

class Apps_Account_Catalog_Item extends Controller_Abstract {
    /**
     * @var Catalog_Item
     */
    protected $item;

    /**************************************************************************************/

    function preLoad(){
        Translate::addDictionary('account/catalog');

        // проверка по ID
        if(isset($_GET['id'])){
            $this->item = Catalog_Item::getObject(IdEncryption::decodeForInt($_GET['id']));
        }

        if($this->item->id){
            $this->setTitle(Translate::t("title_itemDefault", ['id' => $this->item->long_id]));
        }
        else {
            $this->setFollow_Error404();
        }
    }

    function postLoad(){
        /*********************************
         * left preview
         */
        $preview = new View_Panel_Preview();

        $preview->add(
            'preview_id',
            $this->item->long_id .
            (UserAccess::isAdmin() ? " <small style='margin-left: 10px; color: #AAA'>{$this->item->id}</small>" : "")

        );

        if(strlen($this->item->link)) {
            $preview->add('preview_buyer', "<a target='_blank' href='{$this->item->link}'>" . $this->item->getLinkDomain() . "</a>");
        }

        /*
        if(strlen($this->item->vk_id_order_group) || strlen($this->item->vk_id_order_page)){
            $all = [];

            if(strlen($this->item->vk_id_order_group)){
                $all[] = "<a target='_blank' href='https://vk.com/{$this->item->vk_id_order_group}'>" . Translate::t("vk_group") . "</a>";
            }

            if(strlen($this->item->vk_id_order_page)){
                $all[] = "<a target='_blank' href='https://vk.com/{$this->item->vk_id_order_page}'>" . Translate::t("vk_page") . "</a>";
            }

            $preview->add('preview_vk_to_order', implode(", ", $all));
        }
        */

        // $preview->add('preview_price_purchase',     number_format($this->item->price_purchase) . " <small class='txt-color-blueLight'>" . $this->item->price_purchase_currency . "</small>");
        $preview->add('preview_price',  String_Filter::price($this->item->price)  . " <small class='txt-color-blueLight'>RUR</small>");


        /*
        $preview->add(
            'preview_stock_or_order',
            $this->item->isStock() ?
                "<a href='/account/catalog/item/?id={$this->item->long_id}'><i class='label label-success'>" .
                    Translate::t("flag_stock") .
                "</i></a>" :
                "<i class='label label-default'>" .
                    Translate::t("flag_order") .
                "</i>"
        );
        */



        /*********************************
         * left menu
         */
        $menu = new View_Panel_Menu();

        $id = $this->item->long_id;

        $menu->add('leftMenu_itemForm',         "/account/catalog/item/?id={$id}",           'fa-file-text');
        $menu->add('leftMenu_photos',           "/account/catalog/item/photos?id={$id}",     'fa-file-image-o');
        $menu->add('leftMenu_vk_photos',        "/account/catalog/item/vk-photos?id={$id}",  'fa-vk');
        $menu->add('leftMenu_remains',          "/account/catalog/item/remains?id={$id}",    'fa-archive');


        $other = $menu->addGroup('leftMenu_other', 'fa-navicon');

        if(UserAccess::isAdmin()){
            $other->add('leftMenu_sync',             "/account/catalog/item/sync?id={$id}");
        }

        $other->add('leftMenu_source',           "/account/catalog/item/source?id={$id}");

        /*********************************
         * breadcrumbs
         */

        $this->addBreadcrumbElement(
            'breadcrumb_catalog',
            "/account/catalog/",
            'icon-list'
        );

        $this->addBreadcrumbElement(
            Translate::t("breadcrumb_catalogItem", [
                'id' => $this->item->long_id,
                'title' => $this->item->getShortTitle()]
            ),
            "/account/catalog/item/?id=" . $this->item->long_id
        );

        ///////////////////////////////////

        $img = $this->item->getImageLink("200");
        if($img) {
            $this->addToSidebar("<img style='margin:10px; border-radius: 5% !important; width: 200px' src='{$img}'>");
        }

        $this->addToSidebar(
            "<div style='margin: " . ($img ? "0" : "10px") . " 0 10px 10px; text-align:center'>" .
                $this->item->renderFlags() .
            "</div>"
        );
        $this->addToSidebar($preview->render());
        $this->addToSidebar($menu->render());
    }

    /**************************************************************************************/

    function indexAction(){
        $this->setTitle(Translate::t("title_itemForm", ['id' => $this->item->long_id, 'title' => $this->item->getShortTitle()]));

        echo $this->item->renderForm();
    }

    function photosAction(){
        $this->setTitle("title_photos");

        echo $this->item->renderFormPhotos();
    }

    function vkPhotosAction(){
        $this->setTitle("title_vk_photos");

        $select = $this->item->getVkPosts();

        $table = new Table($select);

        // id	create	photo	item	item_type	vk_owner	vk_album	vk_photo	caption


        $table->addField_Text("photo", "")
            ->addDecoratorFunction(function($v){
                return "<img style='max-height:150px' src='" . Catalog_Photo::getObject($v)->getLink(150) . "'>";
            })
            ->setWidthMin()
            ->setLink("https://vk.com/photo{vk_owner}_{vk_photo}")
            ->getTdAttribs()->setCSS('padding', 0);

        $table->addField_Function(function($c){
            $link = "https://vk.com/photo{$c['vk_owner']}_{$c['vk_photo']}";

            return
                "
                <div style='padding: 5px 10px 5px 10px; font-size: 80%'>
                    <div style='font-size: 120%; margin-bottom: 3px'>
                        <a href='{$link}' target='_blank'>{$link}</a>
                    </div>
                    <div style='white-space: normal; padding:10px'>
                        " . Vk_Smiles::render($c['caption']) . "
                    </div>
                </div>
                ";
        }, "tbl_vk_link_and_caption");

        $table->addField_SetArray("item_type", "tbl_item_type", new Catalog_Vk_Type);

        echo $table->render();
    }

    //////////////////////////////////////////

    protected function remainsAction_list(){
        $select = Db::site()->select("catalog_remains")
            ->where("`item`=?", $this->item->id)
            ->order(['status', 'id desc']);

        $table = new Table($select);


        $table->addField_Text("photo", "")
            ->addDecoratorFunction(function($v){
                if($v){
                    return "<img style='max-height:28px' src='" . Catalog_Photo::getObject($v)->getLink(150) . "'>";
                }
            })
            ->setLink(Http_Url::convert(['tab' => 'edit', 'edit_id' => '{id}']))
            ->getTdAttribs()->setCSS('padding', 0);

        $table->addField_Text("id", "tbl_id")
            ->setLink(Http_Url::convert(['tab' => 'edit', 'edit_id' => '{id}']));

        $table->addField_Date("create", "tbl_addDate");
        $table->addField_SetArray("status", "tbl_status", new Catalog_RemainsStatus());
        $table->addField_Text("count", "tbl_count")->setTextAlignCenter();
        $table->addField_Text("price_purchase", "tbl_price_purchase")->setTextAlignCenter();
        $table->addField_Text("price", "tbl_price")->setTextAlignCenter();

        if(count(Catalog_Types::getTypeInfo($this->item->type, 0)['remains_params'])){
            foreach(Catalog_Types::getTypeInfo($this->item->type, 0)['remains_params'] as $el){
                $table->addField_Function(
                    function($c, $t, $p){
                        if(!is_null(Catalog_Options::getRemainsOption($c['id'], $p['option']))){
                            return Catalog_Options::getTypeValueLabel(
                                $p['option'],
                                Catalog_Options::getRemainsOption($c['id'], $p['option'])
                            );
                        }
                    },
                    Catalog_Options::getTypeName($el)
                )
                ->setParam("option", $el);
            }
        }

        $table->addField_Text("comment", "tbl_comment")
            ->addDecoratorFunction(function($v){
                return String_Filter::cutByWord($v, 43);
            });

        return $table->render();
    }

    protected function remainsAction_add(){
        $r = new Catalog_Remains();

        $form = $r->renderForm($this->item->id);

        if($form->getIsValidCache()){
            header("location: /account/catalog/item/remains?id={$this->item->long_id}&tab=list");
        }

        return $form;
    }

    protected function remainsAction_edit(){
        if(isset($_GET['edit_id']) && Catalog_Remains::getObject($_GET['edit_id'])->id){
            $form = Catalog_Remains::getObject($_GET['edit_id'])->renderForm();

            if($form->getIsValidCache()){
                header("location: /account/catalog/item/remains?id={$this->item->long_id}&tab=list");
            }

            return
                "<div style='text-align: right; font-size: 120%; padding-bottom: 10px'>
                    <a href='/account/catalog/remains?id=" . Catalog_Remains::getObject($_GET['edit_id'])->id . "'>" .
                        Translate::t("go_to_remains_page_link") .
                    "</a>
                </div>" .
                $form;
        }
    }

    public function syncAction(){
        $this->setTitle('title_sync');

        $object = Catalog_Parser_Abstract::getObject($this->item->link);

        $form = new Form();
        $form->addButton("btn_update_photos", "update_photos");

        /////////////////////
        $oldPhotos = "";
        $allCurrentPhotos = Db::site()->fetchAll(
            "SELECT id FROM `catalog_photos` WHERE item=?",
            $this->item->id
        );
        if(count($allCurrentPhotos)){
            foreach($allCurrentPhotos as $el){
                $oldPhotos.= "<img style='max-wight:100px; max-height: 100px; margin: 2px;' src='" . Catalog_Photo::getObject($el['id'])->getLink(200) . "'>";
            }
        }


        /////////////////////
        $newPhotos = "";
        if(is_array($object->photos) && count($object->photos)){
            foreach($object->photos as $el){
                $newPhotos.= "<img style='max-wight:100px; max-height: 100px; margin: 2px;' src='" . $el . "'>";
            }
        }

        $form->addSeparator("form_sep_photos");
        $form->addElementStatic("form_old_photos", $oldPhotos)->setRenderTypeBigValue();
        $form->addElementStatic("form_new_photos", $newPhotos)->setRenderTypeBigValue();

        $form->addSeparator("form_sep_price");
        $form->addElementStatic("form_old_price", ($this->item->price_purchase + 0) . " <small>{$this->item->price_purchase_currency}</small>");
        $form->addElementStatic("form_new_price", ($object->price + 0) . " <small>{$object->price_currency}</small>");

        if($form->isValidAndPost()){
            if($form->getButtonClicked() == 'update_photos'){
                if(count($allCurrentPhotos)){
                    foreach($allCurrentPhotos as $el){
                        Catalog_Photo::getObject($el['id'])->delete();
                    }
                }

                if(count($object->photos)){
                    foreach($object->photos as $photo){
                        $filename = "/tmp/uploadfile_" . time() . rand(1000,9000);
                        file_put_contents($filename, file_get_contents($photo));

                        Catalog_Photo::create($this->item->id, $filename);
                    }
                }

                header("location:" . Http_Url::getCurrentURL());
            }
        }

        echo $form;
    }

    public function remainsAction(){
        $this->setTitle('title_remains');

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("list", "tab_list");
        $tabs->addTab("add",  "tab_add");

        if(
            isset($_GET['tab'], $_GET['edit_id']) &&
            $_GET['tab'] == 'edit' &&
            $_GET['edit_id'] > 0 &&
            Catalog_Remains::getObject($_GET['edit_id'])->id
        ){
            $tabs->addTab("edit",  "tab_edit");
        }

        $method = "remainsAction_" . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else{
            $tabs->setText(
                new View_Page_ComingSoon()
            );
        }

        echo $tabs->render();

        /*
        $form  = $this->remainsAction_add();
        $table = $this->remainsAction_list();
        ?>
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-bottom: 70px">
                <h4 class="h_border"><?=Translate::t("catalogRemainsTableTitle")?></h4>
                <?=$table?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="margin-bottom: 70px">
                <h4 class="h_border"><?=Translate::t("catalogRemainsFormTitle")?></h4>
                <?=$form?>
            </div>
        </div>
        <?
        */
    }

    public function sourceAction(){
        $this->setTitle('title_source');

        echo "<h2>" . Translate::t("h_object") . "</h2>";
        echo String_Highlight::render(
            String_Highlight::jsonPrettyPrint(
                $this->item->getParams()
            ), false
        );

        echo "<h2>" . Translate::t("h_options") . "</h2>";
        echo String_Highlight::render(
            String_Highlight::jsonPrettyPrint(
                $this->item->getOptions()
            ), false
        );

        echo "<h2>" . Translate::t("h_stock") . "</h2>";
        echo String_Highlight::render(
            String_Highlight::jsonPrettyPrint(
                $this->item->getStock()
            ), false
        );
    }

}