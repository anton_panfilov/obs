<?php

class Apps_Account_Index extends Controller_Abstract {
    function preLoad() {
        /*
        $webmasterId = Users::getCurrent()->company_id;
        $userMarkets = Lead_Market::getMarkets();
        foreach ($userMarkets as $code => $marketItem) {
            $short = $marketItem['short'];
            foreach ($this->check as $docName) {
                $fullName = $docName . '_' . $short;
                $documents = new Documents("user/doc/status", [
                    'id' => $webmasterId,
                    'type' => 'webmaster',
                    'docs' => [
                        [
                            'name' => $fullName
                        ]
                    ]
                ]);
                $result = $documents->send();

                $showStatuses = [
                    'disagree',
                    'viewed',
                    'new'
                ];
                if (!empty($result->status) && in_array($result->status, $showStatuses)) {
                    $documents = new Documents('snippet/agreement', [
                        'id' => $webmasterId,
                        'type' => 'webmaster',
                        'docs' => [
                            [
                                'name' => $fullName
                            ]
                        ],
                        'agree_url' => '//' . $_SERVER['HTTP_HOST']  . '/account/',
                        'disagree_url'=> '//' . $_SERVER['HTTP_HOST'] . '/account/'
                    ]);
                    echo '<div style="height: 300px; overflow: auto; border: 3px #d43f3a solid; margin-bottom: 20px">' . $documents->renderScript() . '</div>';
                    break 2;
                }
            }
        }
        */

    }

    function indexAction(){
        Translate::addDictionary("account/index");
        echo (new View_Menu_Top)->renderIndex();
        echo (new View_Page_AccountIndex())->render();
    }

    function testAction(){
        $select = Db::site()->select("_test", ["test", "test2"])
            //->distinct(1)
        ;

        $table = new Table($select);

        echo $table->render();
    }

    function checkNotAgreeDocumentsAPI(){
        $result = [
            'count'  => 0,
            'docs'   => [],
        ];

        if(UserAccess::isWebmaster()){
            $notAgree = Webmaster_Documents::apiGetNotAgreeDocuments(Users::getCurrent()->company_id);


            if(count($notAgree)){
                $result['count'] = count($notAgree);

                foreach($notAgree as $el){
                    $result['docs'][$el] = Webmaster_Documents::translate($el);
                }
            }


            /*

            // for test - not active 2 documents

            $result['count']  = 2;
            $result['docs'] = [
                'affiliate_agreement_ru'        => Webmaster_Documents::translate("affiliate_agreement_ru"),
                'affiliate_terms_conditions_us' => Webmaster_Documents::translate("affiliate_terms_conditions_us"),
            ];

            //*/
        }

        return $result;
    }
}