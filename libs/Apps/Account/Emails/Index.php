<?php

class Apps_Account_Emails_Index extends Controller_Abstract {
    /**************************************************************************************/

    function postLoad(){
        $menu = new View_Panel_Menu();

        if(UserAccess::isAdmin()){
            $menu->add('Create Template', '/account/emails/create', 'icon-envelope');
        }

        $menu->add('Templates List',    '/account/emails/',       'icon-list');

        $this->addToSidebar(
            $menu->render()
        );
    }

    /**************************************************************************************/

    /**
     * Список постингов
     */
    function indexAction(){
        $this->setTitle('Templates List');

        $form = new Form();
        $form->setMethodGET();

        $form->addElement_BuyerAgent('agent')
            ->setValue(
                UserAccess::isAgentBuyer() ?
                    Users::getCurrent()->id :
                    ""
            );

        $form->isValidNotErrors();

        ////////////////////////////////
        $select = Db::processing()->select(
            "buyers_accounts",
            ["id", "name", "active", "balance", "credit_limit", "company_name", "agent"]
        );

        if($form->getValue('agent')) $select->where("`agent`=?", $form->getValue('agent'));

        ////////////////////////////////
        // table
        $table = new Table($select);

        // id	name	active	balance	credit_limit	company_name	agent


        $table->addField_SetArray("active", "Status", new Buyer_Status());

        $table->addField_Text("id", "ID")
            ->setLink("/account/buyers/buyer/?id={id}");

        $table->addField_Text("name", "Name")
            ->setLink("/account/buyers/buyer/?id={id}");

        $table->addField_Text("company_name", "Company Name");

        $table->addField_Text('balance', 'Balance')->addDecoratorFunction(function($value, $content){
            $value = $content['balance'] + $content['credit_limit'];
            $color = ($value > 50) ? "#10a062" : "#ed4e2a";

            return "<a style='text-decoration:none; color:{$color}' href='/account/buyers/buyer/balance?id={$content['id']}'>" .
                number_format($value, 2) .
            "</a>";
        });

        $table->addField_Text('credit_limit', 'Credit')->addDecoratorFunction(function($value){
            if($value > 0){
                return number_format($value, 2);
            }

            return "-";
        });

        $table->addField_UserLogin("agent", 'Agent');


        echo $form->render() . $table->render();
    }

    /*******************************************************************************************/


    function createAction(){

    }

    /*******************************************************************************************/

    function groupSalesSetGroupLabelAPI(){
        $result = new Result_OneReason();

        $id     = isset($_GET['id']) ? (int)$_GET['id'] : 0;
        $label  = isset($_GET['label']) ? htmlspecialchars(trim($_GET['label'])) : '';

        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            try {
                Db::processing()->update("buyers_channels_sales_limit_groups", [
                    'label' => $label,
                ], 'id=?', $id);
            }
            catch(Exception $e){
                $result->setError(1, 'This label is use');
            }
        }
        else if(UserAccess::isBuyer()){
            try {
                Db::processing()->update("buyers_channels_sales_limit_groups", [
                    'label' => $label,
                ], 'id=? and buyer=?', $id, Users::getCurrent()->company_id);
            }
            catch(Exception $e){
                $result->setError(1, 'This label is use');
            }
        }

        return $result->getArray();
    }

    function groupSalesCreateGroupAPI(){
        $result = [
            'create_id' => 0,
            'reason'    => '',
        ];

        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            if(!isset($_GET['buyer']) || $_GET['buyer'] < 1){
                $result['reason'] = 'Buyer is require';
                return $result;
            }

            $buyer = (int)$_GET['buyer'];

            if(!Buyer_Object::getObject($buyer)->id){
                $result['reason'] = 'Invalid Buyer ID';
                return $result;
            }

            Db::processing()->insert("buyers_channels_sales_limit_groups", [
                'buyer'             => Buyer_Object::getObject($buyer)->id,
                'label'             => '',
                'data_ui'           => '',
                'data_processing'   => '[]',
            ]);

            $result['create_id'] = Db::processing()->lastInsertId();
        }
        else if(UserAccess::isBuyer()){
            // todo
            $result['reason'] = 'Coming Soon';
        }
        else {
            $result['reason'] = 'Access Deny';
        }

        return $result;
    }

    function groupSalesDeleteGroupAPI(){
        $result = new Result_OneReason();

        $id = isset($_GET['id']) ? (int)$_GET['id'] : 0;

        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer()){
            Db::processing()->delete(
                "buyers_channels_sales_limit_groups",
                'id=?', $id
            );

            Db::processing()->delete(
                "buyers_channels_sales_limit_relations",
                '`group`=?', $id
            );
        }
        else if(UserAccess::isBuyer()){
            $result->setError(1, 'Coming Soon'); // todo
        }

        return $result->getArray();
    }

    function groupSalesTogglePostingAPI(){
        $result = new Result_OneReason();

        if(UserAccess::isAdmin() || UserAccess::isAgentBuyer() || UserAccess::isBuyer()){
            $group      = isset($_GET['group']) ? (int)$_GET['group'] : 0;
            $posting    = isset($_GET['posting']) ? (int)$_GET['posting'] : 0;
            $action     = isset($_GET['action']) ? (bool)$_GET['action'] : true;

            if(!$group)             return $result->setError(1, '`group` is required')->getArray();
            if(!$posting)           return $result->setError(2, '`posting` is required')->getArray();

            $gB = Db::processing()->fetchOne("SELECT `buyer` FROM `buyers_channels_sales_limit_groups` WHERE `id`=?", $group);
            $pObj = Posting_Object::getObject($posting);

            // права доступа для баера
            if(UserAccess::isBuyer() && Users::getCurrent()->company_id != $gB){
                return $result->setError(1, 'Access Deny')->getArray();
            }

            if(!$pObj->id)          return $result->setError(3, "Posting `{$posting}` not found")->getArray();
            if($gB === false)       return $result->setError(4, "Group `{$group}` not found")->getArray();
            if($gB != $pObj->buyer) return $result->setError(5, 'Group and posting does not belong to the same buyer')->getArray();



            if($action){
                try {
                    Db::processing()->insert('buyers_channels_sales_limit_relations', [
                        'group'     => $group,
                        'posting'   => $posting,
                    ]);
                }
                catch(Exception $e){
                    if($e->getCode() != '1062'){
                        return $result->setError(3, $e->getMessage())->getArray();
                    }
                }
            }
            else {
                Db::processing()->delete(
                    'buyers_channels_sales_limit_relations',
                    '`group`=? and `posting`=?',
                    $group,
                    $posting
                );
            }
        }
        else {
            return $result->setError(1, 'Access Deny')->getArray();
        }

        return $result->getArray();
    }

}