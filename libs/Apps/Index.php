<?php

class Apps_Index extends Controller_Abstract {

    function preLoad(){
        Translate::addDictionary('public/index');
        $this->_layout = new View_Layout_MainLayout();
        // $this->_layout;
    }

    /*************************************************************************************************/

    function indexAction(){
        $this->setHTMLTitle('title_index');
        $this->_layout->setTopBlock(new View_Layout_Block_Sale);

        echo new View_Page_ComingSoon();
    }

    function signInAction(){
        $this->setTitle('title_login');

        echo Users::renderLoginForm();
    }

    /*************************************************************************************************/

    function passwordRecoveryAction(){
        $this->setTitle('title_passwordRecovery');
        $this->addBreadcrumbElement("bc_login", "/sign-in");

        $form = new Form();

        $form->setDescription("passwordRecovery_description");

        $form->addButton("btn_continue");
        $form->addButton("btn_backToLogin", "login", false)
            ->setLink("/sign-in");

        $form->addElementText("value", "form_usernameOrEmail");

        //$form->addSeparator("Prove you’re not a robot");
        $form->addElementCaptcha('recovery')
            ->setRenderTypeFullLine();

        if($form->isValidAndPost()){
            $value = $form->getValue('value');
            $field = strpos($form->getValue('value'), '@') !== false ? "email" : "login";

            $valid = true;

            Http_Url::convert(['newGetValue' => '...']);

            if($field == 'email'){
                $validatior = new Form_Validator_Email();
                if(!$validatior->isValid($value)){
                    $form->getElement('value')->addError('Invalid Email');
                    $valid = false;
                }
            }

            if($valid){
                $user = Db::site()->fetchRow(
                    "SELECT id, `name`, `email` FROM `users` WHERE `{$field}`=? limit 1",
                    $value
                );

                if($user['id']){
                    $key = String_Hash::randomString(128);

                    $insert = [
                        "key"           => $key,
                        "ip_request"    => inet_pton($_SERVER['REMOTE_ADDR']),
                        "email"         => $user['email'],
                    ];

                    // если валидируеться определенный login, записать это и на следуюзем шаге будет только он
                    // иначе будет все логины по email
                    if($field == 'login'){
                        $insert['user'] = $user['id'];
                    }

                    Db::site()->insert("password_recovery_sessions", $insert);

                    $link = Http_Url::getCurrentScheme() . "://" .
                        (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : Conf::main()->domain_account)
                        . "/enter-new-password?key=" . urlencode(Db::site()->lastInsertId() . "-" . $key);

                    try {
                        Email_Templates::passwordRecovery($user['email'], $user['name'], $link);

                        header("location: /password-recovery-end?value=" . urlencode($value));
                    }
                    catch(Exception $e){
                        $form->addErrorMessage("Sorry, an error while sending email, please try later");
                    }
                }
                else {
                    $form->addErrorMessage("Sorry, account not found");

                    // что бы не поняли что нет такого аккаунта
                    // header("location: /password-recovery-end?value=" . urlencode($value));
                }
            }
        }

        echo $form->render();
    }

    function passwordRecoveryEndAction(){
        $this->setTitle('title_passwordRecoveryEnd');
        $this->addBreadcrumbElement("bc_login", "/sign-in");

        $value = isset($_GET['value']) ? htmlspecialchars($_GET['value']) : "";
        $field = strpos($value, '@') !== false ? "email" : "login";

        $form = new Form();
        $form->disableButtons();

        $form->addSuccessMessage("passwordRecovery_message_checkEmail");

        if(
            ($field == 'login' && preg_match("/^[a-z0-9\\.]{4,32}$/i", $value)) ||
            ($field == 'email' && (new Validator_Email())->isValid($value))
        ){
            $form->addElementStatic(
                (($field == 'login') ? 'form_username' : 'form_email'),
                $value
            );
        }

        echo $form->render();
    }

    function enterNewPasswordAction(){

        $this->setTitle('title_enterNewPassword');

        /*****************************************/

        if(!isset($_GET['key'])) $this->setFollow_Error404();

        $a = explode("-", $_GET['key']);

        if(count($a) != 2) $this->setFollow_Error404();

        $data = Db::site()->fetchRow(
            "SELECT id, `user`, `email` FROM `password_recovery_sessions` WHERE `id`=? AND `key`=? AND `create`>? AND `finish`=0",
            [
                (int)$a[0],
                $a[1],
                date('Y-m-d H:i:s', time() - 3600),
            ]
        );

        if($data === false){
            $this->setFollow_Error404();
        }

        /*****************************************/

        $form = new Form();
        $form->addButton('btn_SaveAndSignIn');

        // формирование списка пользователей, для которых доступно восстанвление
        $usersSelect = Db::site()->select("users", ['id, login'])
            ->where("email=?", $data['email'])
            ->order("login");

        if($data['user'] > 0) $usersSelect->where("id=?", $data['user']);

        $users = $usersSelect->fetchPairs();
        if(count($users) > 1){
            $users = ['' => '...'] + $users;
        }

        $form->addElementSelect('user', 'form_username')
            ->setOptions($users);

        $form->addElementPasswordNew('password', "form_newAccountPassword");

        if($form->isValidAndPost()){
            Db::site()->update("password_recovery_sessions", [
                "finish"       => 1,
                "change_ts"    => date('Y-m-d H:i:s'),
                "change_ip"    => inet_pton($_SERVER['REMOTE_ADDR']),
            ], 'id=?', $data['id']);

            User::getObject($form->getValue('user'))->setPassword(
                $form->getValue('password')
            );

            Users::login(
                User::getObject($form->getValue('user'))->login,
                $form->getValue('password')
            );

            Users::setLoginCookie(
                User::getObject($form->getValue('user'))
            );

            header('location: /account/');
        }


        echo $form;
    }

    /******************************************************************************************************************/

    function createAccountAction(){
        $this->setTitle('title_createAccount');
        $this->addBreadcrumbElement("bc_login", "/sign-in");

        echo new View_Page_ComingSoon();
    }

    /*************************************************************************************************/

    function loginAPI(){
        Translate::addDictionary('user/login');

        $result = [
            'status'    => 1,
            'reason'    => '',
            'errors'    => []
        ];

        $login      = isset($_GET['username']) ? trim($_GET['username']) : "";
        $password   = isset($_GET['password']) ? trim($_GET['password']) : "";

        if(!strlen($login)){
            $result['status'] = 0;
            $result['errors']['username'] = 'loginError_usernameRequired';
        }

        if(!strlen($password)){
            $result['status'] = 0;
            $result['errors']['password'] = 'loginError_passwordRequired';
        }

        if($result['status']){
            $res = Users::login($login, $password);

            if($res->is()){
                $result['status'] = 1;

                Users::setLoginCookie(
                    Users::getCurrent(),
                    (isset($_GET['persistent']) && $_GET['persistent'])
                );
            }
            else {
                $result['status'] = 0;
                $result['reason'] = $res->getErrorString();
                $result['errors']['username'] = '';
                $result['errors']['password'] = '';
            }
        }

        return $result;
    }

    /**
     * API для получение информации по текущему логину в системе
     *
     * @return array
     */
    function getMyInfoAPI(){
        return [
            'id'    => Users::getCurrent()->id,
            'name'  => Users::getCurrent()->getFullName(),
            'email' => Users::getCurrent()->email,
            'role'  => Users::getCurrent()->role,
        ];
    }

    function checkNewLoginAPI(){
        $login = isset($_GET['login']) ? trim($_GET['login']) : "";

        $validator = new Form_Validator_NewLogin();

        if($validator->isValid($login)){
            return [
                'valid'     => 1,
            ];
        }

        return [
            'valid'     => 0,
            'reason'    => isset($validator->getErrors()[0]) ? $validator->getErrors()[0] : Translate::t("unknownError", "user/login"),
        ];
    }

    /*************************************************************************************************/

    function logoutAction(){
        $this->layoutDisabled();
        Users::logout();
        header('location: /');
    }

    /*************************************************************************************************/
}