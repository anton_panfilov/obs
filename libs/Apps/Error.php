<?php

class Apps_Error extends Controller_Abstract {
    function __construct(){
        $this->_layout = false;
    }

    function error404Action(){
        echo new View_Page_Error_Error404();
    }

    function error403Action(){
        echo new View_Page_Error_Error403();
    }

    function error500Action(){
        echo new View_Page_Error_Error500();
    }
}