<?php

class Apps_Activation_Index extends Controller_Abstract {
    protected $userActivationInfo;

    function isLoginAccess(){
        return (
            isset($_GET['eKey']) &&
            isset($_GET['id']) &&
            is_string($_GET['eKey']) &&
            is_numeric($_GET['id']) &&
            User::getObject($_GET['id'])->isVerify() && // аккаунт проверяеться
            Db::site()->fetchRow(
                "select id from users_activation where id=? and emailCode=?",
                $_GET['id'], $_GET['eKey']
            )
        );
    }

    function preLoad(){
        Translate::addDictionary('activation');

        if($this->_action !== 'login'){
            if(Users::getCurrent()->isActive()){
                header('location: /account/');
                return;
            }
            else if(Users::getCurrent()->isVerify()){
                $this->userActivationInfo = Db::site()->fetchRow(
                    "select * from users_activation where id=?",
                    Users::getCurrent()->id
                );

                if($this->userActivationInfo !== false){
                    /**
                     * - не логин
                     * - текущий пользователь еще акивируеться
                     * - и есть данные для его активации
                     */
                    return;
                }
            }
            else if($this->isLoginAccess()){
                /**
                 * - не в логине
                 * - даннные для активации позволяют продолжить активацию
                 */
                $this->setFollow("/activation/login");
                return;
            }
        }
        else if($this->_action == 'login' && $this->isLoginAccess()){
            /**
             * - в логине
             * - даннные для активации позволяют продолжить активацию
             */
            return;
        }

        $this->setFollow_Error404();
    }

    function loginAction(){
        $this->setTitle('Account Activation');

        $user = User::getObject($_GET['id']);

        $form = new Form();
        $form->addButton("Continue");

        $form->addElementStatic(
            "<img style='border-radius: 50% !important; height: 80px' src='" .
                $user->getAvatarLink(80) .
            "'>",
            "
                <h3 style=' margin:0; padding: 0'>" . $user->getFullName() . "</h3>
                <p>" . $user->login . "</p>
            "
        );

        $form->addElementPassword('password', 'Password')
            ->setWidth(250);

        $form->addElementCheckbox("remember", "Stay signed in")
            ->setValue(1);

        if($form->isValidAndPost()){
            $res = Users::login($user->login, $form->getValue('password'));

            if($res->is()){
                Users::setLoginCookie(Users::getCurrent(), 1);
                $this->setFollow('/activation/');
            }
            else {
                if($res->getErrorCode() == Result_Login::ERROR_INVALID_CREDENTIALS){
                    $form->getElement('password')->addError("Invalid password");
                }
                else {
                    $form->getElement('password')->addError($res->getErrorString());
                }
            }
        }

        echo $form->render();
    }

    /**********************************************************************************************************/

    protected function stepComplete(){
        if($this->userActivationInfo['step'] == 3){
            $documentsType = null;
            if(UserAccess::isWebmaster()){
                // активация вебмастра
                Webmaster_Object::getObject(Users::getCurrent()->company_id)->verificationComplete();
                $documentsType = 'webmaster';
            }
            else if(UserAccess::isBuyer()){
                // активация баера
                // todo
                $documentsType = 'buyer';
            }

            // активация пользователя, удаление сессии активации и перенаправление в панель
            Users::getCurrent()->verificationComplete(true, "Standart Verification");
            Db::site()->delete("users_activation", "id=?", Users::getCurrent()->id);
            header('location: /account/');
        }
        else {
            Db::site()->update("users_activation", [
                'step' => $this->userActivationInfo['step'] + 1
            ], 'id=?', $this->userActivationInfo['id']);

            header('location: /activation/');
        }
    }

    /****************************************************************************************************
     * Верификация email
     *
     * @return string
     */
    protected function tab1(){
        if(
            isset($_GET['eKey']) &&
            is_string($_GET['eKey']) &&
            $_GET['eKey'] == $this->userActivationInfo['emailCode']
        ){
            $this->stepComplete();
        }

        return "
            <p style='font-size:120%'>" .
                Translate::t(
                    "Dear {name}",
                    [
                        'name' => Users::getCurrent()->first_name
                    ]
                ) . ",</p>
            <br>
            <p style='font-size:120%'>" .
                Translate::t(
                    "We sent an email to " .
                    "<a style='text-decoration: underline'>{email}</a> " .
                    "Please follow the link in this email to continue the activation process.",
                    [
                        'email' => Users::getCurrent()->email
                    ]
                ) .
            "</p>";
    }

    /****************************************************************************************************
     * Верификация mobile phone
     *
     * @return string
     */
    protected function tab2(){
        if(!strlen($this->userActivationInfo['phoneCode']) || !strlen($this->userActivationInfo['phoneType'])){
            $form = new Form();
            $form->addButton('Continue');
            $form->setDescription("
                <p>
                    Please confirm that this phone number is correct and then click on the link below.
                </p>
                <p>
                    You will receive a code to validate your phone number by text message or phone call based on your preference.
                </p>
            ");

            $form->addElementText_Phone('phone', 'Phone Number')
                ->setValue(Users::getCurrent()->phone);

            $form->addElementRadio('type', 'Type', [
                'sms' => "
                    SMS (Recommended)
                    <small style='padding-left: 20px; color: #942a25; white-space: nowrap'>
                        Please make sure phone number can receive text message
                    </small>
                ",
                'call' => "Call",
            ])
                ->setRenderTypeBigValue()
                ->setValue('sms')
                ->getLabelAttribs()
                    ->setCSS('width', '99%');

            $form->addElementSelect('language', 'Language', User_Phone_Verification::getCallLanguages())
                ->setValue(
                    (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) == 'ru') ?
                        "ru-RU" : "en-US"
                );

            $form->addConditionalFileds_Array('type', 'call', 'language');

            if($form->isValidAndPost()){
                if(Users::getCurrent()->phone != $form->getValue('phone')){
                    Users::getCurrent()->phone = $form->getValue('phone');
                    Users::getCurrent()->update(['phone']);
                }

                if($form->getValue('type') == 'sms'){
                    $code = User_Phone_Verification::createVerification_SMS(
                        Users::getCurrent()->phone,
                        $this->userActivationInfo['phoneCode']
                    );
                }
                else {
                    $code = User_Phone_Verification::createVerification_Call(
                        Users::getCurrent()->phone,
                        $form->getValue('language'),
                        $this->userActivationInfo['phoneCode']
                    );
                }

                Db::site()->update("users_activation", [
                    'phoneCode'     => $code,
                    'phoneType'     => $form->getValue('type'),
                    'phoneAttempts' => new Db_Expr("`phoneAttempts`+1"),
                ], 'id=?', $this->userActivationInfo['id']);

                header('location: /activation/');
                die;
            }

            return  $form->render();
        }
        else {
            if(
                isset($_GET['phone_confirmation_try_again']) &&
                is_string($_GET['phone_confirmation_try_again']) &&
                $_GET['phone_confirmation_try_again'] == '1'
            ){
                Db::site()->update("users_activation", [
                    'phoneType' => null,
                ], 'id=?', $this->userActivationInfo['id']);

                header('location: /activation/');
                die;
            }


            $form = new Form();
            $form->addButton('Continue');

            if($this->userActivationInfo['phoneType'] == 'sms'){
                $form->setDescription(Translate::t("
                    <p style='font-size:110%'>
                        SMS with the code sent to phone number <b>{phone}</b>
                    </p>
                ", ['phone' => Users::getCurrent()->phone]));
            }
            else {
                $form->setDescription(Translate::t("
                    <p style='font-size:110%'>
                        Now calling number <b>{phone}</b> and dictating code
                    </p>
                ", ['phone' => Users::getCurrent()->phone]));
            }

            $form->addElementText('code', "<span style='font-size:150%'>Please enter code</span>")
                ->setWidth('100')
                ->setMask("/^[0-9]*\$/")
                ->setMaxLength(5)
                ->getAttribs()
                    ->setCSS('font-size', '24px')
                    ->setCSS('height', '30px');

            if($this->userActivationInfo['phoneErrors'] >= 5){
                $form->addElementCaptcha('ver_phone');
            }

            if(isset($_POST['code'])){
                if($form->isValidAndPost()){
                    if($form->getValue('code') == $this->userActivationInfo['phoneCode']){
                        $this->stepComplete();
                    }
                    else {
                        $form->getElement('code')->addError(
                            "Sorry this is not the correct Code <small style='color:#999'> - total of " .
                                ($this->userActivationInfo['phoneErrors'] + 1) .
                            " error(s)</small>"
                        );

                        Db::site()->update("users_activation", [
                            'phoneErrors' => new Db_Expr("`phoneErrors`+1"),
                        ], 'id=?', $this->userActivationInfo['id']);
                    }
                }
            }

            return  $form->render() . "<br><p style='color:#777'>" . Translate::t("
                If you have not received a code and would like to request a phone call, please
                <a style='text-decoration: underline' href='/activation/?phone_confirmation_try_again=1'>return to the previous step</a>
                and resubmit your information.
            ") . "</p>";
        }
    }

    /****************************************************************************************************
     * Информация о компании
     *
     * @return string
     */
    protected function tab3(){
        if(UserAccess::isWebmaster()){
            $form = Webmaster_Object::getObject(Users::getCurrent()->company_id)
                ->renderDetailsForm();
        }
        else if(UserAccess::isBuyer()){
            $form = Buyer_Object::getObject(Users::getCurrent()->company_id)
                ->renderDetailsForm();
        }

        /** @noinspection PhpUndefinedVariableInspection */
        echo $form->render();

        if($form->getIsValidCache()){
            $this->stepComplete();
        }
    }

    /**********************************************************************************************************/

    function indexAction(){
        ?>
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7" style="margin-bottom:70px">
                <h3 class='h_border'><?=Translate::t("Account Activation")?></h3>
                <br>
                <?

                echo (new Ui_Steps())
                    ->addStep('Email Confirmation')
                    ->addStep('Phone Confirmation')
                    ->addStep('Company Profile')
                    ->setCurrentStep($this->userActivationInfo['step'])
                    ->render();

                echo "<div style='clear: both; padding-top: 70px'>";

                $method = "tab" . $this->userActivationInfo['step'];
                if(method_exists($this, $method)){
                    echo $this->$method();
                }
                else {
                    echo "method Apps_Activation_Index::{$method} not found";
                }

                echo "</div>";

                ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5" style="margin-bottom: 70px">
                <?=Webmaster_Object::getObject(Users::getCurrent()->company_id)->renderMyAgents()?>
            </div>
        </div>
        <?
    }
}