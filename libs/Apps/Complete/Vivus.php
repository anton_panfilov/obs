<?php

class Apps_Complete_Vivus extends Controller_Abstract {

    const VIVUS_TABLE = "buyers_complete_page_vivus";
    const VIVUS_URI = 'https://www.vivus.ru/client/update-lead';
    const SUCCESS = 1;
    const FAILED = 0;
    const ERROR = 2;

    /**
     * @var Lead_Object
     */
    protected $lead;

    /**
     * @var array
     */
    protected $client;

    function preLoad() {
        $this->_layout = new View_Layout_Complete_Vivus();

        if (!in_array($this->_action, ['error404'])) {
            if (isset($_GET['client'])) {

                $client = explode(".", $_GET['client']);

                if (
                        count($client) == 2 && is_numeric($client[0])
                ) {

                    $client = Db::processing()->fetchRow(
                            "select * from " . self::VIVUS_TABLE . " where `id`=? and `code`=?", $client
                    );

                    if (
                            $client !== false &&
                            Lead_Object::getLeadByID($client['lead'])->id
                    ) {

                        $this->client = $client;
                        $this->client['form'] = Json::decode($this->client['form']);
                        $this->lead = Lead_Object::getLeadByID($client['lead']);
                    }
                }
            }

            if (!$this->lead) {
                $this->_follow = "/complete/vivus/error404";
            } else if ($this->_action != 'thankyou' && $client['status'] != self::SUCCESS) {
                $this->_follow = "/complete/vivus/thankyou";
            } else if ($this->_action != 'phone' && isset($_GET['phone']) && $_GET['phone'] == 'edit') {
                $this->_follow = "/complete/vivus/phone";
            }
        }
    }

    function postLoad() {

    }

    /*     * ********************************************************* */

    protected function getPhone() {
        if (strlen($this->client['phone'])) {
            return $this->client['phone'];
        }
        return $this->lead->getBody()->_get_mobile_phone();
    }

    function phoneFormat($value) {
        return sprintf("+7 (%s) %s-%s-%s", substr($value, 0, 3), substr($value, 3, 3), substr($value, 6, 2), substr($value, 8, 2)
        );
    }

    function error404Action() {
        $this->setTitle("Страница не найдена");
    }

    function phoneAction() {
        $this->setTitle("Изменение телефонного номера");

        $form = new Form();
        $form->addButton("Сохранить");

        $form->addElementText_PhoneMobileRus("phone", "Телефонный номер")
                ->setValue($this->getPhone());

        if ($form->isValidAndPost()) {
            Db::processing()->update(self::VIVUS_TABLE, [
                'phone' => (new Filter_NumbersOnly())->filter($form->getValue('phone')),
                    ], "id=?", $this->client['id']);

            header("location:" . Http_Url::convert(['phone' => null]));
        }

        echo $form->render();
    }

    function indexAction() {
        $this->setHTMLTitle("Vivus : Последний шаг");
        $this->setPageTitle(
                $this->lead->getBody()->_get_first_name() . " " . $this->lead->getBody()->_get_middle_name() .
                "<span style='font-size:85%; color:#83a441'>, ваш займ одобрен в сумме {$_GET['amount']} рублей</span>" .
                "<span style='font-size:80%;'>, остался последний шаг:</span>"
        );

        $form = new Form();
        $form->addButton("Я согласен с условиями договора. Отправьте мне деньги");

        ////////////////////////////

        $form->addSeparator("Куда отправить деньги?");
        $form->addElementRadio("type", "<span style='position: relative; top:-8px'>Типа платежа</span>", [
                    'contact' => '<span style="font-size: 140%; font-weight: 800;">Наличными через систему Контакт</span>',
                    'wire' => '<span style="font-size: 140%;  font-weight: 800;">Перечисление на банковский счет</span>',
                ])
                ->setLabelWidth("326px")
                ->setRenderTypeBigValue();


        ////////////////////////////

        $form->addElementStatic("", "
            <ul style='font-size: 140%; line-height: 200%'>
            <li>
                После того как вы примите Условия договора, вы получите СМС с кодом
                на <nobr>ваш номер <b>" . sprintf(
                        "(%s) %s - %s", substr($this->getPhone(), 0, 3), substr($this->getPhone(), 3, 3), substr($this->getPhone(), 6, 4)
                ) . "</b>
                </nobr>
            </li>
            <li>
                Отправляйтесь с этим кодом и паспортом в любое отделение системы Контакт и получите денги.
            </li>
            </ul><br>
            <iframe
                src='https://www.contact-sys.com/where/point/FNCV/1'
                frameborder='0'
                style='width:100%; height:666px'
                scrolling='no'
                ></iframe>
        ", 'contact_desc')->setRenderTypeFullLine();

        ////////////////////////////

        $form->addElementStatic("", "
            <ul style='font-size: 140%; line-height: 200%'>
                <li>Заполните ваши банковские реквизиты и согласитесь с условиями договора;</li>
                <li>Дождитесь звонка на ваш номер телефона: <nobr><b>" . sprintf(
                        "(%s) %s - %s", substr($this->getPhone(), 0, 3), substr($this->getPhone(), 3, 3), substr($this->getPhone(), 6, 4)
                ) . "</b>
                </nobr></li>
            </ul>
        ", 'wire_desc')->setRenderTypeFullLine();

        Site::addJSCode("
            document.lastBik = '';
            document.changeBik = function(){
                var bik = jQuery('#bik').val();
                if(bik.length == 9){
                    if(document.lastBik != bik){
                        document.lastBik = bik;

                        jQuery('#ks').val('...');
                        jQuery('#bank_name_text').html('...');
                        jQuery('#bik_error').hide();

                        jQuery.getJSON(
                            'https://json-service.com/api/banks/bik-info?bik=' + bik + '&jsonp=?',
                            function(data){
                                // only last bik
                                if(data.request.bik == document.lastBik){
                                    if(data.response.bik == ''){
                                        jQuery('#ks').val('-');
                                        jQuery('#bank_name_text').html('-');
                                        jQuery('#bik_error').show();
                                    }
                                    else {
                                        jQuery('#ks').val(data.response.ks);
                                        jQuery('#bank_name_text').html(
                                            '<b>' + data.response.name + '</b> ' + data.response.city + ', ' + data.response.address
                                        );
                                    }
                                }
                            }
                        )
                    }
                }
                else {
                    document.lastBik = bik;
                    jQuery('#ks').val('-');
                    jQuery('#bank_name_text').html('-');
                }
            }
            jQuery('#bik').click(function(){document.changeBik()});
            jQuery('#bik').keyup(function(){document.changeBik()});
            jQuery('#bik').change(function(){document.changeBik()});
            document.changeBik();

        ");

        $form->addElementText("bik", "БИК")
                ->setMask('num')
                ->setComment("
                    <span style='font-size: 120%'>
                    (банковский идентификационный код)
                    Этот код есть у каждого отделения банка, он указан в реквизитах/документах на карту или сберкнижку,
                    также его можно уточнить в самом отделении банка, позвонив по горячей линии банка
                    (номер указывается на обратной стороне карты);<br>
                    состоит из 9 цифр, начинается на 04.
                    </span>
                ")
                ->addFilter(new Form_Filter_NumbersOnly())
                ->addValidator(new Form_Validator_Length(9, 9))
                ->addValidator(new Form_Validator_BankBik())
                ->setTextPostInput("<div style='float: right; color: #ac2925; display: none' id='bik_error'>Банк не найден</div>")
                ->setValue("04")
                ->getAttribs()
                ->setCSS("width", "auto")
                ->setAttrib('size', '12')
                ->setAttrib('maxlength', '9');



        $form->addElementText("ks", "Кор. счет")
                ->setMask('num')
                ->addValidator(new Form_Validator_Length(20, 20))
                ->getAttribs()
                ->setCSS("width", "auto")
                ->setAttrib('size', '25')
                ->setAttrib('maxlength', '20')
                ->setAttrib('readonly', 'readonly');



        //$form->addElementStatic('Кор. счет', '<div id="ks_text" name="ks_text" style="padding-top: 9px">...</div>', "ks")->setRenderTypeBigValue();
        $form->addElementStatic('Банк', '<div id="bank_name_text" style="padding-top: 9px">...</div>', "bank_name")->setRenderTypeBigValue();

        $form->addElementText("rs", "Расчетный счет")
                ->setMask('num')
                ->addValidator(new Form_Validator_Length(20, 20))
                ->setComment("
                <span style='font-size: 120%'>
                    (лицевой счет получателя), состоит из 20 цифр, начинается на 40817, 4230
                </span>
            ")
                ->getAttribs()
                ->setCSS("width", "auto")
                ->setAttrib('size', '25')
                ->setAttrib('maxlength', '20');

        ////////////////////////////

        $form->addConditionalFileds_Array('type', 'contact', ['contact_desc']);
        $form->addConditionalFileds_Array('type', 'wire', ['wire_desc', 'bik', 'rs', 'ks', 'bank_name']);

        ////////////////////////////

        $form->addSeparator("Договор");
        $form->addElementStatic(
                        "", "
                <ul style='font-size: 140%'>
                    <li>
                        <a
                            href='https://www.vivus.ru/ru/agreement-terms-and-conditions'
                            target='_blank'
                            onclick=\"window.open('https://www.vivus.ru/ru/agreement-terms-and-conditions','TermsAndConditions','width=800,height=600,toolbar=0,scrollbars=1'); return false;\"
                        >
                            Общие условия договора потребительского микрозайма
                        </a>
                    </li>
                    <li>
                        <a
                            href='https://www.vivus.ru/ru/agreement-general-terms'
                            target='_blank'
                            onclick=\"window.open('https://www.vivus.ru/ru/agreement-general-terms','GeneralTerms','width=800,height=600,toolbar=0,scrollbars=1'); return false;\"
                        >
                            Правила предоставления микрозаймов закрытого акционерного общества «4финанс»
                        </a>
                    </li>
                </ul>
            ")
                ->setRenderTypeFullLine();

        if ($form->isValidAndPost()) {
            $this->setFollow("/complete/vivus/thankyou");
            Db::processing()->update(self::VIVUS_TABLE, [
                'status' => self::SUCCESS,
                'form' => Json::encode($form->getValues()),
                    ], "id=?", $this->client['id']);
        }

        echo $form->render();
    }

    function thankyouAction() {

        if ($this->saveFormAttributes($this->client['form'])) {
            $this->pushToVivus($this->client['form']);
        }

        if (isset($this->client['form']['type']) && $this->client['form']['type'] == 'contact') {
            echo '
                <h3>Заём одобрен!</h3>
                <br>
                <p>Получите денежные средства в любом отделении Contact</p>
                <div style="padding-top:30px">
                    <p><b>Для получения займа наличными, Вам необходимо:</b></p>
                    <ul>
                        <li style="text-align:left"><a href="https://www.contact-sys.com/where/point/FNCV/1" rel="nofollow" target="_blank">Выбрать отделение Contact</a></li>
                        <li style="text-align:left">Подойти в выбранное отделение с паспортом и номером перевода, полученным в СМС</li>
                    </ul>
                </div>
                <p style="padding-top:40px">
                    Карту с адресами отделений Вы сможете найти <a href="https://www.contact-sys.com/where/point/FNCV/1" rel="nofollow" target="_blank">здесь</a>
                </p>
            ';
        } else if (isset($this->client['form']['type']) && $this->client['form']['type'] == 'wire') {
            echo '
                <h3>Спасибо за заявку</h3>
                <br>
                <p>' . $this->lead->getBody()->_get_first_name() . ' ' . $this->lead->getBody()->_get_middle_name() .
            ', дождитесь звонка на ваш контакнтый номер телефона: ' . $this->lead->getBody()->_get_mobile_phone() . '</p>
            ';
        } else {
            // неизвестные данные
            $this->_follow = "/complete/vivus/error404";
        }
    }

    function saveFormAttributes($attrs) {

        if ($attrs) {

            $attrs['timestamp'] = time();

            //saving Form data from the Second step
            Db::processing()->update(self::VIVUS_TABLE, ['form' => Json::encode($attrs), 'phone' => $this->getPhone()], "id=?", $this->client['id']);

            return true;
        }

        return false;
    }

    function pushToVivus($attrs) {

        //prepare CURL data to be sent to Vivus
        $data = [];
        $data['mobilePhone'] = $this->phoneFormat($this->getPhone());
        $data['clientNumber'] = Db::processing()->fetchOne("SELECT client_id FROM `" . self::VIVUS_TABLE . "` WHERE `id`=?", $this->client['id']);

        switch ($attrs['type']) {
            case 'contact': $data['offlineCash'] = 'true';
                break;
            case 'wire': $data['offlineCash'] = 'false';
                $data['bicCode'] = $attrs['bik'];
                $data['correspondentAccountNumber'] = $attrs['ks'];
                $data['accountNumber'] = $attrs['rs'];
                break;
        }
        //sending it
        $response = Json::decode($this->sendRequest(self::VIVUS_URI, $data));
    }

    function sendRequest($uri, $data) {

        $qString = '';

        //array to a query string
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $qString .= $key . '=' . urlencode($value) . '&';
            }
            $qString = rtrim($qString, '&');
        }

        //adding timestamp for log fields json
        $data['timestamp'] = time();


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $qString);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/x-www-form-urlencoded; charset=UTF-8"]);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        try {
            $Response = curl_exec($ch);
            $Request = curl_getinfo($ch, CURLINFO_HEADER_OUT) . $qString;
            Db::processing()->update(self::VIVUS_TABLE, ['vivus_log' => Json::encode($data)], "id=?", $this->client['id']);
            $RunTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        } catch (Exception $e) {

        }

        // анализ отправки
        if (curl_errno($ch)) {
            // Ошибка
            $curl_info = curl_getinfo($ch);
            Db::processing()->update(self::VIVUS_TABLE, ['vivus_status' => self::ERROR], "id=?", $this->client['id']);
        } else {
            // Отправка завершена успешно
            curl_close($ch);

            //check if Response Good or Not
            if (Json::decode($Response)['validationErrors']) {
                Db::processing()->update(self::VIVUS_TABLE, ['vivus_status' => self::FAILED], "id=?", $this->client['id']);
            } else {
                Db::processing()->update(self::VIVUS_TABLE, ['vivus_status' => self::SUCCESS], "id=?", $this->client['id']);
            }
        }

        //return response status for futher actions
        return $Response;
    }

}
