<?php

class Apps_TempPassword_Index extends Controller_Abstract {
    function preLoad(){
        Translate::addDictionary('public/temp-pass');

        if(Users::getCurrent()->id){
            if(Users::getCurrent()->isPasswordTemp()){
                return;
            }
            else {
                header('location: /account/');
                return;
            }
        }

        $this->setFollow_Error404();
    }

    function indexAction(){
        $this->setTitle('title_tempPassSetNewPassword');

        $form = new Form();
        $form->setDescription('tempPass_descr');
        $form->addButton("btn_saveAndContinue");

        $form->addElementPasswordNew('password');

        if($form->isValidAndPost()){
            if(Users::getCurrent()->isValidPassword($form->getValue('password'))){
                $form->addErrorMessage("tempPass_error_matchPass");
            }
            else {
                Users::getCurrent()->setPassword($form->getValue('password'));
                Users::login(Users::getCurrent()->login, $form->getValue('password'));
                Users::setLoginCookie(Users::getCurrent(), 1);
                $this->setFollow('/activation/');
            }
        }

        echo $form->render();
    }
}