<?php

class Value {
    static public function export($var){
        echo "\n<pre>" . var_export($var, 1) . "</pre>\n";
    }

    static public function is(&$var, $default = null){
        return isset($var) ? $var : $default;
    }

    /******************************************************************/

    /**
     * @var array
     */
    static protected $global = [];

    /**
     * @var array
     */
    static protected $register = [];

    /******************************************************************/

    static protected function getName($name){
        return substr(trim($name), 0, 255);
    }

    /******************************************************************/

    static public function setGlobal($name, $value){
        self::$global[self::getName($name)] = $value;
    }

    static public function getGlobal($name, $default = null){
        $name = self::getName($name);
        return isset(self::$global[$name]) ? self::$global[$name] : $default;
    }

    /******************************************************************/

    static public function setRegister($name, $value){
        if(is_null($value)){
            unset(self::$register[$name]);
            Db::site()->delete("variables_register_blob", "`name`=?", $name);
            return;
        }

        if(is_array($value)){
            $value = Json::encode($value);
        }

        $value = (string)$value;

        $name = self::getName($name);
        if(!isset(self::$register[$name]) || self::$register[$name] != $value){
            try{
                Db::site()->insert("variables_register_blob", [
                    'name'  => $name,
                    'value' => $value,
                ]);
            }
            catch (Exception $e){
                Db::site()->update("variables_register_blob", [
                    'value' => $value,
                ], "`name`=?", $name);
            }

            self::$register[$name] = $value;
        }
    }

    static public function getRegister($name, $default = null){
        $name = self::getName($name);

        if(!isset(self::$register[$name])){
            $val = Db::site()->fetchOne(
                "select `value` from variables_register_blob where `name`=?",
                $name
            );

            self::$register[$name] = ($val === false) ? $default : $val;
        }

        return self::$register[$name];
    }
}