<?php

class Tabs_StaticTabs {
    protected $title;

    protected $float_left = 1;

    protected $tabsText = "";

    /**
     * Имена гет переменных, которые должны быть отчищены при переходе между табами
     *
     * @var array
     */
    protected $clearValuesNames = [];

    /**
     * Имя Get переменной, из которой получется название таба, при условии что
     * функция getActionFromRequest() не переопределена на другую логику
     *
     * @var string
     */
    protected $actionValueName = 'tab';

    /**
     * Массив табов, а также разделяющих их декораторов
     *
     * @var array
     */
    protected $tabs = [];

    /**
     * Имя таба по умолчанию. Эти правила работают только если имя таба не переданно в запросе
     *
     * null - имя по умолчанию не определенно, будет выбран первый
     * false - не надо выбирать активный таб
     * string - имя таба по умолчанию
     *
     * @var string|null|bool
     */
    protected $default = null;

    /**
     * Активный таб, если null то определяеться из запроса
     */
    protected $activeTab = null;

    /**
     * Текст отображаемый под табами
     * Не обязательно изпользовать этот тип вывода данных, можно просто сделать Echo после рендера табов
     *
     * @var mixed
     */
    protected $text;

    protected $is_hide_print = false;

    /**
     * Получить из запроса имя активного таба.
     * Если запрос не передает это имя вернеться null
     *
     * @return string|null
     */
    protected function getActiveTabFromRequest(){
        return isset($_GET[$this->actionValueName]) ? $_GET[$this->actionValueName] : null;
    }

    /**
     * Спрятать при печати
     *
     * @return Table
     */
    public function setPrintHide(){
        $this->is_hide_print = true;
        return $this;
    }

    public function setFloatRight(){
        $this->float_left = false;
    }

    /**
     * Установить название гет переменной, которая будет влиять на табы
     *
     * @param $name
     * @return $this
     */
    public function setValueName($name){
        $this->actionValueName = $name;
        return $this;
    }

    /**
     * @param $dafault
     * @return $this
     */
    public function setDefaultTab($dafault){
        $this->default = (string)$dafault;
        return $this;
    }

    public function setDefaultFirstTab(){
        $this->default = null;
        return $this;
    }

    public function setDefaultCloseAllTabs(){
        $this->default = false;
        return $this;
    }

    /**
     * Добавить гет переменные которые будут отчищаться при переключении между табами
     *
     * @param $values
     * @return $this
     */
    public function addClearValuesNames($values){
        if(!is_array($values)) $values = [$values];
        if(count($values)){
            foreach($values as $el){
                $this->clearValuesNames[] = (string)$el;
            }
        }
        return $this;
    }

    /**
     * Установить гет переменные которые будут отчищаться при переключении между табами
     *
     * @param $values
     * @return $this
     */
    public function setClearValuesNames($values){
        $this->clearValuesNames = [];
        return $this->addClearValuesNames($values);
    }

    /**
     * Отрисовка одного таба
     * Можно переопределять для изменения метода передачи имени таба
     *
     * @param Tabs_Tab $tab
     *
     * @return string
     */
    protected function renderTab(Tabs_Tab $tab){
        $attr = new Html_Attribs();
        $get = [
            $this->actionValueName => $tab->getName()
        ];

        if(!$this->float_left) {
            $attr->setCSS('float', 'right');
        }

        if(count($this->clearValuesNames)){
            foreach($this->clearValuesNames as $el){
                $get[$el] = null;
            }
        }

        $attr->setAttrib("href", Http_Url::convert($get));

        if($tab->getName() == $this->getActiveTab()){
            $attr->addAttribPre("class", "activeTab ");
        }

        return "<a " . $attr->render() . ">" . Translate::t($tab->getLabel()) . "</a>";
    }


    /**
     * Уставновить текст, который пойдет после табов
     *
     * @param $text
     */
    public function setTabsPostText($text, $link = null){
        $this->tabsText = "<a href='{$link}' style='float: right'>{$text}</a>";
    }

    /**
     * Определение активного таба
     *
     * Срабатывает только 1 раз, при повторном срабатывании берет данные из кеша,
     * поэтому нельзы вызывать эту функцию до полной настройки табов
     *
     * Эта функция срабатывает автоматически при рендере
     *
     * return string Имя активного таба
     */
    public function getActiveTab(){
        if(is_null($this->activeTab)){
            if(is_string($this->default) && isset($this->tabs[$this->default])){
                $this->activeTab = $this->default;
            }

            if(count($this->tabs)){
                $requestActiveTab = $this->getActiveTabFromRequest();
                foreach($this->tabs as $t){
                    if($t instanceof Tabs_Tab){
                        if($requestActiveTab == $t->getName()){
                            $this->activeTab = $t->getName();
                            break;
                        }

                        if(is_null($this->activeTab)) $this->activeTab = $t->getName();
                    }
                }
            }
            else {
                $this->activeTab = false;
            }
        }

        return $this->activeTab;
    }

    /**
     * Получение параметров активногор таба или null Если нет такого таба
     *
     * @return mixed
     */
    public function getActiveTabParams(){
        if(isset($this->tabs[$this->getActiveTab()]) && $this->tabs[$this->getActiveTab()] instanceof Tabs_Tab){
            return $this->tabs[$this->getActiveTab()]->getParams();
        }
        return null;
    }

    /**
     * Получение параметров активногор таба или null Если нет такого таба
     *
     * @return mixed
     */
    public function getActiveTabLabel(){
        if(isset($this->tabs[$this->getActiveTab()]) && $this->tabs[$this->getActiveTab()] instanceof Tabs_Tab){
            return $this->tabs[$this->getActiveTab()]->getLabel();
        }
        return null;
    }

    /**
     * Добавить новый таб
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Tabs_Tab
     * @throws \Exception
     */
    public function addTab($name, $label = null, $params = null){
        if(!isset($this->tabs[$name])){
            $this->tabs[$name] = new Tabs_Tab($name, $label, $params);
            return $this->tabs[$name];
        }

        throw new \Exception("Tab with name `{$name}` already exists");
    }


    /**
     * Установить текст, отображаемый под табами
     *
     * @param $text
     *
     * @return $this
     */
    public function setText($text){
        $this->text = $text;
        return $this;
    }

    /**
     * Отрисовка табов
     *
     * @return string
     */
    public function render(){
        Site::addCSS('tabs.css');

        $r = '';

        if(count($this->tabs)){
            // todo для табов слева
            if($this->title){
                $r.= "<div style='float: left;'>{$this->title}</div>";
            }

            $r.= "<div class='ap_tabs'>";
            $r.= ($this->float_left ? "" : $this->tabsText);

            $tabs = $this->tabs;
            if(!$this->float_left){
                $tabs = array_reverse($tabs);
            }

            foreach($tabs as $tab){
                $r.= ($tab instanceof Tabs_Tab) ? $this->renderTab($tab) : (string)$tab;
            }

            $r.= ($this->float_left ? $this->tabsText : "") . "</div>";
        }

        if(strlen($this->text)){
            $r.= "<div class='ap_tabs_content'>" . $this->getText() . "</div>";
        }
        else {
            $r.= "<div class='ap_tabs_separator'></div>";
        }

        if($this->is_hide_print){
            $r = "<div class='hidden-print'>{$r}</div>";
        }

        return $r;
    }



    protected function getText(){
        return $this->text;
    }


    /**
     * При использовании объекта как строки будет вызванна отрисовка табов через функцию render()
     *
     * @return string
     */
    public function __toString(){
        return (string)$this->render();
    }
}