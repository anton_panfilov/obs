<?php

class Tabs_Tab {
    protected $name;
    protected $label;
    protected $params;

    public function __construct($name, $label = null, $params = null){
        if(is_null($label)) $label = $name;

        $this->name = $name;
        $this->label = $label;
        $this->params = $params;
    }

    public function getName(){
        return $this->name;
    }

    public function getLabel(){
        return $this->label;
    }

    public function getParams(){
        return $this->params;
    }
}