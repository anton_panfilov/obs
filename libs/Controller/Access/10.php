<?php

// Admin

return [
    '/account/index',

    '/account/profile/*',
    '/account/catalog/*',
    '/account/orders/*',
];