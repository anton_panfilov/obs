<?php

class Controller_Boot {
    /**
     * @var Controller_Abstract
     */
    static protected $controller;

    /**
     * Загрузка сайта по запросу
     *
     * @param null $request
     * @return mixed
     */
    static public function run($request = null){
        header("Content-Type:text/html; charset=utf-8");

        if(is_null($request)){
            $request = $_SERVER['REQUEST_URI'];
        }

        $temp =  explode("?", $request, 2);
        $path = $temp[0];
        $get = isset($temp[1]) ? $temp[1] : '';

        /******************************************
         * вырезание индексов - 301 редирект
         */
        if(substr($path, -6) == '/index')               $path = substr($path, 0, strlen($path) - 5);
        if(str_replace('index/', '', $path) != $path)   $path = str_replace('index/', '', $path);

        if($temp[0] != $path){
            header("Location: {$path}" . (strlen($get) ? "?{$get}" : ""), true, 301);
        }

        return self::renderController($path);
    }

    /**
     * Рекурсивная функция загрузки контролеров, которые могу ссылаться на другие контроллеры
     *
     * @param $path
     * @return string
     */
    static protected function renderController($path){
        try {
            if(is_string($path)){
                /******************************************
                 * Преобразование path в массив
                 */
                if($path[strlen($path) - 1] == "/"){
                    $path.= "index";
                }

                $path = explode("/", ltrim($path, "/"));

                // добавить index action, если в адресации получился только 1 элемент
                if(count($path) == 1) $path = array('index', $path[0]);

                foreach($path as $el){
                    $name = mb_strtolower(trim($el, "-"));
                    if(!preg_match('/^[a-z0-9-]{1,64}$/', $el) || $el != $name){
                        $path = array('error', 'error404');
                    }
                }
            }

            /******************************************
             * поиск контролера
             */
            $folder = Boot::$dir_libs . "/Apps";
            $class = "Apps";
            $controllerName = null;
            $actionName = null;
            $not_filter_i = 0;

            $type = 0; // 1 - Action, 2 - api

            for($i = 0; $i < count($path); $i++){
                if(is_dir($temp = $folder . '/' . self::getClassOrMethodName($path[$i]))){
                    // если есть папка, переходим в неё
                    $folder = $temp;
                    $class.= "_" . self::getClassOrMethodName($path[$i]);
                    $not_filter_i = $i+1;
                }
                else if(class_exists($temp = $class . '_' . self::getClassOrMethodName($path[$i]))){
                    // echo "$temp";
                    // если есть контролер, идем в него
                    $methods = get_class_methods($temp);

                    if($i+1 == count($path)){
                        $method = 'index';
                    }
                    else {
                        $method = implode(
                            '_',
                            self::getClassOrMethodName(array_slice($path, $i+1), true)
                        );
                    }

                    $methodAction   = $method . "Action";
                    $methodAPI      = $method . "API";

                    $isMethodAction = in_array($methodAction, $methods);
                    $isMethodAPI = in_array($methodAPI, $methods);

                    if($isMethodAction && $isMethodAPI){
                        throw new Exception(
                            "Duplicate routes for action and api: `{$methodAction}` and `{$methodAPI}` in class `{$temp}`"
                        );
                    }
                    else if($isMethodAction) {
                        $actionName     = $methodAction;
                        $controllerName = $temp;
                        $type           = 1;
                    }
                    else if($isMethodAPI) {
                        $actionName     = $methodAPI;
                        $controllerName = $temp;
                        $type           = 2;
                    }

                    break;
                }
                else if($i == count($path) - 1 && class_exists($temp = $class . '_Index')){
                    // если контролера нет, ищем в индексном
                    $methods = get_class_methods($temp);

                    $method = implode(
                        '_',
                        self::getClassOrMethodName(array_slice($path, $not_filter_i), true)
                    );

                    $methodAction   = $method . "Action";
                    $methodAPI      = $method . "API";

                    $isMethodAction = in_array($methodAction, $methods);
                    $isMethodAPI = in_array($methodAPI, $methods);

                    if($isMethodAction && $isMethodAPI){
                        throw new Exception(
                            "Duplicate routes for action and api: `{$methodAction}` and `{$methodAPI}` in class `{$temp}`"
                        );
                    }
                    else if($isMethodAction) {
                        $actionName     = $methodAction;
                        $controllerName = $temp;
                        $type           = 1;
                    }
                    else if($isMethodAPI) {
                        $actionName     = $methodAPI;
                        $controllerName = $temp;
                        $type           = 2;
                    }
                }
                else {
                    return self::renderController("/error/error404");
                }
            }

            // todo: проверка прав доступа!


            if(Users::getCurrent()->id && Users::getCurrent()->isVerify() && $path[0] == 'account'){
                header('location: /activation/');
                die;
            }
            else if(Users::getCurrent()->id && Users::getCurrent()->isPasswordTemp() && $path[0] == 'account'){
                header('location: /temp-password/');
                die;
            }
            /*
            else if($path[0] == 'activation' && !Users::getCurrent()->isVerify()){
                // return self::renderController("/error/error404");
            }
            */
            else if($path[0] == 'account'){
                $is = false;
                $isAccessPath = '';

                $file = __DIR__ . "/Access/" . Users::getCurrent()->role . ".php";
                if(is_file($file)){
                    $accessArray = include($file);
                    if(!is_array($accessArray)) $accessArray = [];

                }
                else {
                    $accessArray = [];
                }

                if(in_array("/" . implode("/", $path), $accessArray)){
                    $is = true;
                }
                else {
                    for($i = 0; $i < count($path) - 1; $i++){
                        $isAccessPath.= "/{$path[$i]}";

                        if(in_array($isAccessPath . "/*", $accessArray)){
                            $is = true;
                            break;
                        }
                    }
                }

                if(!$is){
                    return self::renderController("/error/error403");
                }
            }

            // отрезать контент, который уходил в поток до этого (перередирект с другого контролера) и стартануть новый буфер
            ob_end_clean();
            ob_start();

            // загрузка контролера
            if($controllerName){
                self::$controller = new $controllerName($method);

                if(self::$controller instanceof Controller_Abstract){
                    // предзагрузка (поиск объектов, проверка прав по этим объектам и т.п.)
                    self::$controller->preLoad();
                    if(self::$controller->getFollow()) return self::renderController(self::$controller->getFollow());

                    // метод экшена
                    $result = self::$controller->$actionName();
                    if(self::$controller->getFollow()) return self::renderController(self::$controller->getFollow());

                    // пост загрузка (меню, превью и т.п.)
                    self::$controller->postLoad();
                    if(self::$controller->getFollow()) return self::renderController(self::$controller->getFollow());

                    if($type == 1){
                        return self::$controller->renderAction(ob_get_clean());
                    }
                    else {
                        return self::$controller->renderAPI($result);
                    }
                }
            }

            return self::renderController("/error/error404");
        }
        catch(Exception $e){
            View_Layout_Block_Debug::setException($e);
            System_Warnings::add($e->getMessage());
            return self::renderController("/error/error500");
        }
    }

    /**************************************************************************************/

    /**
     * Преобразует имя полученное из URL в имя, которое можно использовать в названиях классов и методов
     * Если передать массив, то будет преобразован каждый его элемент в отдельности
     */
    static protected function getClassOrMethodName($urlName, $lowerFirst = false){
        if(is_array($urlName)){
            if(count($urlName)){
                foreach($urlName as $k => $v){
                    $urlName[$k] = self::getClassOrMethodName($v, $lowerFirst);
                }
            }
            return $urlName;
        }

        $urlName = str_replace("/", "_", $urlName);

        $urlName = explode("-", $urlName);
        foreach($urlName as $k => $v){
            $urlName[$k] = ucwords($v);
        }
        $urlName = implode("", $urlName);
        if($lowerFirst) $urlName[0] = mb_strtolower($urlName[0]);

        return $urlName;
    }

    /**
     * Получить параметр из контроллера
     *
     * @param $name
     * @param $default
     * @return null
     */
    static public function controllerParamGet($name, $default = null){
        if(self::$controller instanceof Controller_Abstract){
            return self::$controller->paramGet($name, $default);
        }
        return $default;
    }

    /**
     * @param $name
     * @return null
     */
    static public function controllerParamIs($name){
        if(self::$controller instanceof Controller_Abstract){
            return self::$controller->paramIs($name);
        }
        return false;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    static public function controllerParamSet($name, $value){
        if(self::$controller instanceof Controller_Abstract){
            return self::$controller->paramSet($name, $value);
        }
    }
}