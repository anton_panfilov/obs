<?php

abstract class Controller_Abstract {
    /**
     * @var string
     */
    protected $_action;

    /**
     * @var null|boot|View_Layout_Abstract
     */
    protected $_layout;

    /**
     * Перенаправление на другой адрес, но без перезагрузки страницы
     *
     * @var string
     */
    protected $_follow;

    /**
     * Параметры, котоыре можно получить функцией
     *
     *
     * @var array
     */
    protected $_params = [];

    /****************************************************************/

    public function __construct($action){
        $this->_action = $action;

        $path = explode("?", $_SERVER['REQUEST_URI'], 2)[0];
        if(!in_array($path, ['/', '/index', '/account/', '/account/index', '/account/index/'])){
            if(substr($path, 0, 9) == '/account/') {
                $this->addBreadcrumbElement(Translate::t("breadcrumb_account"), "/account/", "fa fa-home");
            }
            else {
                $this->addBreadcrumbElement(Translate::t("breadcrumb_main"), "/", "fa fa-home");
            }
        }
    }

    /**
     * Отрабатывает, когда уже определено, что такая стрнаница точно есть и на её отображение хватает прав, но до работы функции экшена
     * Тут можно загружать общие объекты и переадресовываться на 404 если объект не найден или на его просмотр не хватет прав
     */
    public function preLoad(){}

    /**
     * Отрабатывает после успешной работы функции экшена, если она никуда не переадресовывает и не выходит с ошибкой
     */
    public function postLoad(){
        // настройка меню и превтю блоков
    }

    /****************************************************************/

    /**
     * Не рендерить лейаут, только контент из контролера
     */
    public function layoutDisabled(){
        $this->_layout = false;
    }

    /****************************************************************/

    /**
     * @param null|string $message
     */
    public function setComingSoon($message = null){
        echo (new View_Page_ComingSoon())
            ->setMessage($message)
            ->render();
    }

    /****************************************************************/

    /**
     * Получить ссылку на страницу, на которую идет внутреннее перенаправление без редиректа
     *
     * @return string|array
     */
    public function getFollow(){
        return $this->_follow;
    }

    /**
     * Установка скрытой переадресации между контролерами
     *
     * @param $follow
     */
    public function setFollow($follow){
        $this->_follow = $follow;
    }

    /**
     *
     */
    public function setFollow_Error404(){
        $this->setFollow(['error', 'error404']);
    }

    /**
     *
     */
    public function setFollow_Error403(){
        $this->setFollow(['error', 'error403']);
    }

    /****************************************************************/

    /**
     * Установить параметр контролера, который в дальнейшем можно будет получить так: Controller_Boot::controllerParamGet('NAME');
     *
     * @param $name
     * @param $value
     * @return $this
     */
    public function paramSet($name, $value){
        $this->_params[$name] = $value;
        return $this;
    }

    /**
     * @param $name
     * @param null $default
     * @return null
     */
    public function paramGet($name, $default = null){
        return isset($this->_params[$name]) ? $this->_params[$name] : $default;
    }

    /**
     * @param $name
     * @return bool
     */
    public function paramIs($name){
        return isset($this->_params[$name]) ? true : false;
    }

    /**
     * @param $string
     * @return $this
     */
    public function addToSidebar($string){
        $this->paramSet('sidebar', (string)$this->paramGet('sidebar') . (string)$string);
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title){
        $this->setHTMLTitle($title);
        $this->setPageTitle($title);

        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setHTMLTitle($title){
        $this->paramSet('html_title', Translate::t($title));
        return $this;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setPageTitle($title){
        $this->paramSet('page_title', Translate::t($title));
        return $this;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setPageDescription($text){
        $this->paramSet('page_description', Translate::t($text));
        return $this;
    }

    /**
     * Добавление элемента в хлебные крошки
     *
     * @param $label
     * @param null $link
     * @param null $icon
     * @param array $sub
     * @return $this
     */
    public function addBreadcrumbElement($label, $link = null, $icon = null, $sub = []){
        $label = Translate::t($label);

        if($this->paramIs('breadcrumb'))    $breadcrumb = $this->paramGet('breadcrumb');
        else                                $breadcrumb = [];

        if($link){
            $attr = new Html_Attribs();
            $attr->setAttrib('href', $link);
            $el = "<a " . $attr->render() . ">{$label}</a>";
        }
        else {
            $el = "<i>{$label}</i>";
        }



        if(is_array($sub) && count($sub)){
            $subArr = [];
            foreach($sub as $sel){
                if(isset($sel[0], $sel[1])){

                    $attr = new Html_Attribs();
                    if($link) $attr->setAttrib('href', $sel[1]);

                    $subArr[] = "<a " . $attr->render() . ">{$sel[0]}</a>";
                }
            }

            if(count($subArr)){
                $el.= "<span class='sub-breadcrumbs'>(" . implode(", &nbsp;", $subArr) . ")</span>";
            }
        }

        if($icon) $el = "<i class='{$icon}'></i> " . $el;

        $breadcrumb[] = $el;
        $this->paramSet('breadcrumb', $breadcrumb);

        return $this;
    }

    /****************************************************************/

    /**
     * @param $content
     * @return mixed
     */
    public function renderAction($content){
        $layout = $this->_layout;

        // если не уставнлен, загрузить по умолчанию
        if(is_null($layout)){
            $layout = new View_Layout_MainLayout();
        }

        // если не то что нужно, то отрендерить только контент
        if(!($layout instanceof View_Layout_Abstract)){
            return $content;
        }

        $layout->setContent($content);
        return $layout->render();
    }

    /**
     * @param $result
     * @return string
     */
    public function renderAPI($result){
        if(isset($_GET['jsonp']) && is_string($_GET['jsonp'])){
            header("content-type: application/javascript");
            return "{$_GET['jsonp']}(" . Json::encode($result) . ")";
        }
        else {
            header('content-type: application/json');
            return Json::encode($result);
        }
    }
}