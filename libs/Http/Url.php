<?php

class Http_Url {

    /**
     * Получение ссылки с теим же GET переменными что и текущая, дополненные переменными из $addGetValies
     * А также можно поменять сам path, передав ег ов переменную $path
     *
     * @param mixed $replaceGetValues
     * @param mixed $path
     * @param bool $getFromQuery получить или не получить значения из текущего запроса
     *                          если получить, то они дополняются переменными из $replaceGetValues
     *                          иначе используются только $replaceGetValues
     *
     * @return string
     */
    static public function convert($replaceGetValues, $path = null, $getFromQuery = true){

        ///////////////////////////////////////////
        // разбитие доп строки

        $pathArray = strlen($path) ? explode("?", $path, 2) : [];


        ///////////////////////////////////////////
        // составление строки

        $get = $replaceGetValues;
        if(!is_array($get)){
            // если изменения переданны не массивом, считаеться что это GET строка
            parse_str((string)$get, $get);
        }

        if($getFromQuery){
            if(strlen($path)) {
                if(count($pathArray) == 2){
                    $query_string = $pathArray[1];
                }
                else {
                    $query_string = "";
                }
            }
            else {
                $query_string = $_SERVER['QUERY_STRING'];
            }

            if(strlen($query_string)){
                parse_str($query_string, $query_string);
                $get = array_replace_recursive($query_string, $get);
            }
        }

        if(count($get)){
            $get = http_build_query($get);

            if(strlen($get)){
                $get = "?" . str_replace(
                        ["%7B", "%7D", "%5B", "%5D"],
                        ["{",   "}",   "[",   "]"],
                        $get
                    );
            }
        }
        else {
            $get = "";
        }

        ///////////////////////////////////////////
        // составление PATH

        if(strlen($path)){
            $path = $pathArray[0];
        }
        else {
            $path = $_SERVER['DOCUMENT_URI'];
        }

        ///////////////////////////////////////////

        return $path . $get;
    }

    static public function getCurrentURL(){
        return ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? "https" : "http") .
            "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    }

    static public function getCurrentScheme(){
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? "https" : "http";
    }
}