<?php

class Http_SearchEngineParse {
    const GOOGLE_ORGANIC    = 'Google.Organic';
    const GOOGLE_ADWORDS    = 'Google.AdWords';
    const GOOGLE_MAPS       = 'Google.Maps';
    const GOOGLE_MAIL       = 'Google.Mail';
    const GOOGLE_OTHER      = 'Google.Other';

    const YAHOO_ORGANIC     = 'Yahoo.Organic';
    const YAHOO_MAIL        = 'Yahoo.Mail';
    const YAHOO_ANSWERS     = 'Yahoo.Answers';
    const YAHOO_BLOGS       = 'Yahoo.Blogs';
    const YAHOO_OTHER       = 'Yahoo.Other';

    const BING_ORGANIC      = 'Bing.Organic';
    const BING_OTHER        = 'Bing.Other';

    const AOL_ORGANIC       = 'AOL.Organic';
    const AOL_MAIL          = 'AOL.Mail';
    const AOL_OTHER         = 'AOL.Other';

    const ASK_ORGANIC       = 'Ask.Organic';
    const ASK_ANSWERS       = 'Ask.Answers';
    const ASK_OTHER         = 'Ask.Other';

    const YANDEX_ORGANIC    = 'Yandex.Organic';
    const YANDEX_OTHER      = 'Yandex.Other';

    // ...

    public $searchEngine;
    public $keyword;



    /**
     * Парсинг входящего URL на:
     * 1. название поиской системы
     * 2. словосочетание, по которому осуществялся поиск
     *
     * Если система нее
     *
     * @param $source_url
     */
    public function __construct($source_url){
        if(strlen($source_url)){
            $url = parse_url($source_url);
            if(isset($url['host'])){
                $domain         = mb_strtolower($url['host']);
                $domainArray    = explode(".", $domain);

                $path = isset($url['path']) ? $url['path'] : "/";
                $pathArray = explode("/", substr($path, 1));

                // убираем окончание домена
                $zone = $domainArray[count($domainArray) - 1];
                unset($domainArray[count($domainArray) - 1]);
                if(in_array($domainArray[count($domainArray) - 1], ['co', 'com'])){
                    $zone = $domainArray[count($domainArray) - 1] . ".{$zone}";
                    unset($domainArray[count($domainArray) - 1]);
                }

                if(count($domainArray)){
                    $domainArray = array_reverse($domainArray);

                    $getValues = [];
                    if(isset($url['query']) && strlen($url['query'])){
                        parse_str($url['query'], $getValues);
                    }

                    /**
                     * $domain       :  "maps.google.com"
                     * $domainArray  :  ['google', 'maps']
                     * $zone         :  "com" or "co.uk"
                     *
                     * $path        :   "/maps/places" or "/"
                     * $pathArray   :   ['maps', 'places'] or ['']
                     *
                     * $getValues    :  [ .. get values .. ]
                     */

                    /**************************************************************/


                    /************   GOOGLE   ************/

                    if($domain == 'www.googleadservices.com'){
                        $this->searchEngine = self::GOOGLE_ADWORDS;
                        $this->keyword      = $this->getQueryStringValue(['q'], $getValues);
                    }
                    else if($domainArray[0] == 'google'){
                        if(isset($domainArray[1]) && $domainArray[1] == 'maps'){
                            $this->searchEngine = self::GOOGLE_MAPS;
                            if($pathArray[0] == 'local_url'){
                                $this->keyword      = $this->getQueryStringValue(['dq', 'q'], $getValues);
                            }
                            else {
                                $this->keyword      = $this->getQueryStringValue(['q'], $getValues);
                            }
                        }
                        else if(in_array($path, ['/search', '/url', '/m', '/m/search', '/custom'])){
                            $this->searchEngine = self::GOOGLE_ORGANIC;
                            $this->keyword      = $this->getQueryStringValue(['q'], $getValues);
                        }
                        else if($path == '/aclk'){
                            $this->searchEngine = self::GOOGLE_ADWORDS;
                            $this->keyword      = $this->getQueryStringValue(['q'], $getValues);
                        }
                        else if($pathArray[0] == 'mail'){
                            $this->searchEngine = self::GOOGLE_MAIL;
                        }
                        else {
                            $this->searchEngine = self::GOOGLE_OTHER;
                            $this->keyword      = $this->getQueryStringValue(['q'], $getValues);
                        }
                    }


                    /************   YAHOO   ************/

                    else if(
                        isset($domainArray[1]) &&
                        $domainArray[1] == 'mail' &&
                        $domainArray[0] == 'yahoo' &&
                        in_array($zone, ['com', 'net'])
                    ){
                        $this->searchEngine = self::YAHOO_MAIL;
                    }
                    else if(
                        isset($domainArray[1]) &&
                        $domainArray[1] == 'shine' &&
                        $domainArray[0] == 'yahoo' &&
                        in_array($zone, ['com'])
                    ){
                        $this->searchEngine = self::YAHOO_BLOGS;
                    }
                    else if(
                        isset($domainArray[1]) &&
                        $domainArray[1] == 'answers' &&
                        $domainArray[0] == 'yahoo' &&
                        in_array($zone, ['com'])
                    ){
                        $this->searchEngine = self::YAHOO_ANSWERS;
                    }
                    else if(
                        $domainArray[0] == 'yahoo' &&
                        $zone == 'com' &&
                        in_array($path, ['/search', '/mobile/s'])
                    ){
                        $this->searchEngine = self::YAHOO_ORGANIC;
                        $this->keyword      = $this->getQueryStringValue(['p'], $getValues);
                    }
                    else if(
                        $domainArray[0] == 'yahoo' &&
                        $zone == 'com'
                    ){
                        $this->searchEngine = self::YAHOO_OTHER;
                    }


                    /************   BING   ************/

                    else if(
                        $domainArray[0] == 'bing' &&
                        $zone == 'com'
                    ){
                        if(in_array($pathArray[0], ['search', 'url'])){
                            $this->searchEngine = self::BING_ORGANIC;
                            $this->keyword      = $this->getQueryStringValue(['q'], $getValues);
                        }
                        else {
                            $this->searchEngine = self::BING_ORGANIC;
                            $this->keyword      = $this->getQueryStringValue(['q'], $getValues);
                        }
                    }


                    /************   AOL   ************/

                    else if($domainArray[0] == 'aol'){
                        if(
                            (isset($domainArray[1]) && $domainArray[1] == 'm' && $pathArray[0] == 'search') ||
                            (isset($pathArray[1]) && $pathArray[0] == 'aol' && $pathArray[1] == 'search')
                        ){
                            $this->searchEngine = self::AOL_ORGANIC;
                            $this->keyword = $this->getQueryStringValue(['q', 'query'], $getValues);
                        }
                        else if(
                            (isset($domainArray[1]) && $domainArray[1] == 'm' && $pathArray[0] == 'mail') ||
                            (isset($domainArray[1]) && $domainArray[1] == 'mail')
                        ){
                            $this->searchEngine = self::AOL_MAIL;
                        }
                        else {
                            $this->searchEngine = self::AOL_OTHER;
                            $this->keyword = $this->getQueryStringValue(['q', 'query'], $getValues);
                        }

                    }


                    /************   Ask   ************/

                    else if($domainArray[0] == 'ask' && $zone == 'com'){
                        if(in_array($pathArray[0], ['web'])){
                            $this->searchEngine = self::ASK_ORGANIC;
                            $this->keyword = $this->getQueryStringValue(['q'], $getValues);
                        }
                        else if(in_array($pathArray[0], ['web', 'search', 'mywebsearch'])){
                            $this->searchEngine = self::ASK_ORGANIC;
                            $this->keyword = $this->getQueryStringValue(['searchfor', 'searchFor'], $getValues);
                        }
                        else if(in_array($pathArray[0], ['answers'])){
                            $this->searchEngine = self::ASK_ORGANIC;
                            $this->keyword = isset($pathArray[2]) ? str_replace('-', ' ', $pathArray[2]) : null;
                        }
                        else {
                            $this->searchEngine = self::ASK_OTHER;
                        }
                    }

                    /************   Yandex   ************/

                    else if($domainArray[0] == 'yandex' && $zone == 'ru'){
                        $this->searchEngine = self::YANDEX_ORGANIC;
                        $this->keyword = $this->getQueryStringValue(['text'], $getValues);
                    }
                }
            }
        }

        $this->keyword = mb_strtolower(substr($this->keyword, 0, 255));

        // изменение кодировки
        $c = mb_detect_encoding($this->keyword, array('UTF-8', 'Windows-1251'));
        if($c != 'UTF-8'){
            $this->keyword = iconv($c, 'UTF-8', $this->keyword);
        }
    }

    protected function getQueryStringValue($variables, $getValues){
        if(!is_array($variables)) $variables = [$variables];
        if(count($variables)){
            foreach($variables as $val){
                if(isset($getValues[$val])) return $getValues[$val];
            }
        }
        return null;
    }
}