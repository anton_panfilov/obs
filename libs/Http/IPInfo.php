<?php

class Http_IPInfo {
    public $ip;

    public $country = '';
    public $region = '';
    public $city = '';
    public $postal_code = '';

    public function __construct($ip){
        $this->ip = (string)$ip;
        unset($ip);

        // GEO
        if(function_exists('geoip_record_by_name')){
            $details = @geoip_record_by_name($this->ip);

            if(
                $details &&
                isset(
                    $details['country_code'],
                    $details['region'],
                    $details['city'],
                    $details['postal_code']
                )
            ){
                $this->country      = (string)$details['country_code'];
                $this->region       = (string)$details['region'];
                $this->city         = (string)$details['city'];
                $this->postal_code  = (string)$details['postal_code'];
            }
            else {
                $this->country = (string)@geoip_country_code_by_name($this->ip);
            }
        }
    }
}