<?php
/**
 * Examples of usage
 */

/**
 * Plural forms
 */
echo "<p>" . \Translate::t('cucumber|cucumbers', 1);
// or
echo "<p>" . \Translate::t('cucumber|cucumbers', array(
    'n' => 1
));
//output:
// cucumber

echo "<p>" . \Translate::t('cucumber|cucumbers', 2);
// or
echo "<p>" . \Translate::t('cucumber|cucumbers', array(
 'n' => 2
));
//output:
//cucumbers

/**
 * Skip translate and return ''
 */

//returns '' in current locale
return array(
    "Username" => false,
);
echo "<p>" . Translate::t('Username');
//output:
// ''

//returns original word
return array(
    "Username" => "",
);
echo "<p>" . Translate::t('Username');
//output:
// Username

/**
 * Named parameters
 */
echo "<p>" . Translate::t('This alias {alias}', array(
 'alias' => 'works!'
));
// output:
// This alias works!

/**
 * Combine aliases and plural forms
 *
 * if local array has such key => value pair for ru locale
 * array(
 *     "Username {name} {name2} {n} | Usernames {name} {name2} {n}" => "{n} Пользователь {name} {name2}|{n} Пользователи {name} {name2}",
 * );
 * then
 */
echo "<p>" . \Translate::t('Username {name} {name2} {n} | Usernames {name} {name2} {n}', array(
 'name'  => 'Vasi',
 'name2' => 'Pupkini',
 'n'     => 2
));
// or
echo "<p>" .  \Translate::t('Username {name} {name2} {n} | Usernames {name} {name2} {n}', 2, array(
 'name'  => 'Vasi',
 'name2' => 'Pupkini'
));
// output:
// 2 Пользователи Vasi Pupkini

echo "<p>" . \Translate::t('Username {name} {name2} {n} | Usernames {name} {name2} {n}', 1, array(
 'name'  => 'Vasya',
 'name2' => 'Pupkin'
));
// output:
// 1 Пользователь Vasya Pupkin

/**
 * "{n} cucumber|{n} cucumbers" => "{n} огурец|{n} огурца|{n} огурцов|{n} огурца",
 */
echo "<p>" . Translate::t('{n} cucumber|{n} cucumbers', 62);
// 62 огурца

echo "<p>" . Translate::t('{n} cucumber|{n} cucumbers', 1.5);
// 1.5 огурца

echo "<p>" . Translate::t('{n} cucumber|{n} cucumbers', 1);
// 1 огурец

echo "<p>" . Translate::t('{n} cucumber|{n} cucumbers', 7);
// 7 огурцов


/**
 * with custom dictionary
 * /langs/old/ru.php
 * "{n} cucumber|{n} cucumbers" => "{n} огурецъ|{n} огурцас|{n} огурцовс|{n} огурцас",
 */
echo '<p>' . Translate::t("{n} cucumber|{n} cucumbers", 1);
// 1 огурец
echo '<p>' . Translate::t("{n} cucumber|{n} cucumbers", 'old', 1);
// 1 огурецъ