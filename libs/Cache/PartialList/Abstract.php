<?php

abstract class Cache_PartialList_Abstract {
    static protected $cache = array();

    static protected $instance = array();

    static public function getInstance(){
        if(!isset(static::$instance[get_called_class()])){
            static::$instance[get_called_class()] = new static();
        }

        return static::$instance[get_called_class()];
    }

    /**
     * @param array $ids
     * @return array
     */
    static protected function select($ids){}

    /**
     * Получнеие значения, для ключа, по которому не получается получить значение
     *
     * @param $id
     * @return mixed
     */
    static protected function getNotFoundValue($id){
        return null;
    }

    /**
     * Загрузка кеша по переданным ключам
     *
     * Функция проверяет ключи, для которых еще не был загружен кеш и передает их в абстрактную функцию select
     * Обрабатывает ответ от функции select и записывает эти значения в кеш
     *
     * Если при получении данных функцией select для какого то ключа не будет полученно значение,
     * Оно будет полученно из ключа функией: getNotFoundValue($id)
     *
     * @param array $ids Массив ключей, для которых загружается кеш
     */
    static public function load($ids){
        if(is_array($ids) && count($ids)){
            $idsNew = array();
            foreach($ids as $id){
                if(!isset(static::$cache[get_called_class()][$id])) $idsNew[] = $id;
            }

            if(count($idsNew)){
                $idsNew = array_unique($idsNew);

                $all = static::select($idsNew);

                foreach($idsNew as $id){
                    if(isset($all[$id])){
                        static::$cache[get_called_class()][$id] = $all[$id];
                    }
                    else {
                        static::$cache[get_called_class()][$id] = static::getNotFoundValue($id);
                    }
                }
            }
        }
    }

    /**
     * Получить значение для ключа, если в кеше нет ключа, значение будет загруженно только для него
     *
     * @param $id
     * @return mixed
     */
    static public function get($id){
        if(!isset(static::$cache[get_called_class()][$id])){
            static::load(array($id));
        }
        return static::$cache[get_called_class()][$id];
    }
}