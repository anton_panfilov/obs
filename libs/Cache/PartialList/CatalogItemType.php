<?php

class Cache_PartialList_CatalogItemType extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::site()->fetchPairs(
            "select `id`, `type` from `catalog_items` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}