<?php

class Cache_PartialList_Timezone extends Cache_PartialList_Abstract {
    static public function get($id){
        return Date_Timezones::getTimezoneLabel($id);
    }
}