<?php

class Cache_PartialList_CatalogItemLongID extends Cache_PartialList_Abstract {
    static protected function select($ids){
        return Db::site()->fetchPairs(
            "select `id`, `long_id` from `catalog_items` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}