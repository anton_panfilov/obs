<?php

class Cache_PartialList_UserLogin extends Cache_PartialList_Abstract {
    static protected function select($ids){
        // лечение проюлемы с пользователем №0
        $n = array_search(0, $ids);
        if($n !== false) unset($ids[$n]);
        if(!count($ids)) return [];

        return Db::site()->fetchPairs(
            "select `id`, `login` from `users` where `id` in ('" . implode("','", $ids) . "')"
        );
    }
}