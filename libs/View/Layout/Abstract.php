<?php

abstract class View_Layout_Abstract extends View_Abstract {
    protected $content;

    public function setContent($text){
        $this->content = $text;
    }
}