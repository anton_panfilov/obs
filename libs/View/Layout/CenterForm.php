<?php

class View_Layout_CenterForm extends View_Layout_Abstract {
    protected function renderCSS(){
        $data = "";
        $css = Controller_Boot::controllerParamGet('SiteCSSArray', []);
        if(count($css)){
            foreach($css as $file){
                if(is_string($file)){
                    $data.= "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . htmlspecialchars($file) . "\" />\r\n";
                }
            }
        }
        return $data;
    }

    protected function renderJS(){
        $data = "";
        $css = Controller_Boot::controllerParamGet('SiteJSArray', []);
        if(count($css)){
            foreach($css as $file){
                if(is_string($file)){
                    $data.= "<script src=\"" . htmlspecialchars($file) . "\" type=\"text/javascript\"></script>\r\n";
                }
            }
        }
        return $data;
    }

    public function render(){
        ob_start();
       ?>
<!DOCTYPE html>
<html lang="en-us" id="lock-page" style="url(/static/smart/img/mybg.png) #fff !important;">
<head>
    <meta charset="utf-8">
    <title><?=Controller_Boot::controllerParamGet('html_title', Conf::main()->title)?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/smartadmin-skins.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="/static/css/site.css">

    <?=$this->renderCSS()?>

    <!-- page related CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="/static/smart/css/lockscreen.min.css">

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="/static/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/static/img/favicon.ico" type="image/x-icon">

    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- #APP SCREEN / ICONS -->
    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="/static/smart/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/static/smart/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/static/smart/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/static/smart/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="/static/smart/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="/static/smart/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="/static/smart/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

    <?=$this->renderJS()?>
</head>

<body style="background: url(/static/smart/img/mybg.png) #fff;">
<div style="width: 100%; text-align: right; padding: 5px">
    <?=Translate::renderLanguageSelectBtn()?>
</div>
<div id="main" role="main">
    <div class="lockscreen animated flipInY">

        <div class="logo" style="padding-bottom: 5px">
            <? if(Controller_Boot::controllerParamIs('page_title') && strlen(Controller_Boot::controllerParamGet('page_title'))){ ?>
                <div style="color: #777; font-size: 16px; float:right; padding-top: 30px;"> <?=Controller_Boot::controllerParamGet('page_title')?></div>
            <? } ?>
            <span id="logo" style="font-size: 18px; line-height: 24px; white-space: nowrap; text-align: center; color:#333">
                <span style="font-size: 20px; color: #487182; padding-right: 5px"><i class="fa fa-bank"></i></span>
                <?=Conf::main()->title?>
            </span>
        </div>
        <div style="border: #BBB solid 1px">
            <?=$this->content?>
        </div>
        <p class="font-xs margin-top-5" style="text-align: right; padding: 10px">
            <?=Conf::main()->title?> &copy; <?=date('Y')?>
        </p>
    </div>
</div>

<script src="/static/smart/js/plugin/pace/pace.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script> if (!window.jQuery) { document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');} </script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script> if (!window.jQuery.ui) { document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>
<script src="/static/smart/js/bootstrap/bootstrap.min.js"></script>
<script src="/static/smart/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<script src="/static/smart/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<script src="/static/smart/js/app.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //pageSetUp();
        <?= Controller_Boot::controllerParamGet('SiteAddJSCode') ?>
    })
</script>
</body>
</html>
        <?
        return ob_get_clean();
    }
}