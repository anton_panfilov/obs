<?php

class View_Layout_MainLayout extends View_Layout_Abstract {
    protected $topBlock;

    protected function renderCSS(){
        $data = "";
        $css = Controller_Boot::controllerParamGet('SiteCSSArray', []);
        if(count($css)){
            foreach($css as $file){
                if(is_string($file)){
                    $data.= "<link type=\"text/css\" rel=\"stylesheet\" href=\"" . htmlspecialchars($file) . "\" />\r\n";
                }
            }
        }
        return $data;
    }

    protected function renderJS(){
        $data = "";
        $css = Controller_Boot::controllerParamGet('SiteJSArray', []);
        if(count($css)){
            foreach($css as $file){
                if(is_string($file)){
                    $data.= "<script src=\"" . htmlspecialchars($file) . "\" type=\"text/javascript\"></script>\r\n";
                }
            }
        }
        return $data;
    }

    public function setTopBlock($content){
        $this->topBlock = $content;
        return $this;
    }

    public function render(){
        ob_start();
       ?>
<!DOCTYPE html>
<html lang="en-us" style="background-image: url(<?=Site::staticLink('/img/bg/tileable_wood_texture.png')?>);" >
<head>
    <meta charset="utf-8">
    <title><?=Controller_Boot::controllerParamGet('html_title', Conf::main()->title)?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" media="screen" href="<?=Site::staticLink('/smart/css/bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=Site::staticLink('/smart/css/font-awesome.min.css')?>">

    <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=Site::staticLink('/smart/css/smartadmin-production.min.css')?>">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=Site::staticLink('/smart/css/smartadmin-skins.min.css')?>">

    <link rel="stylesheet" type="text/css" href="<?=Site::staticLink('/css/site.css')?>">

    <?=$this->renderCSS()?>

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="<?=Site::staticLink('/img/favicon.ico" type="image/x-icon')?>">
    <link rel="icon" href="<?=Site::staticLink('/img/favicon.ico" type="image/x-icon')?>">

    <!-- #GOOGLE FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- #APP SCREEN / ICONS -->
    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="<?=Site::staticLink('/smart/img/splash/sptouch-icon-iphone.png')?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=Site::staticLink('/smart/img/splash/touch-icon-ipad.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=Site::staticLink('/smart/img/splash/touch-icon-iphone-retina.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=Site::staticLink('/smart/img/splash/touch-icon-ipad-retina.png')?>">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="<?=Site::staticLink('/smart/img/splash/ipad-landscape.png')?>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="<?=Site::staticLink('/smart/img/splash/ipad-portrait.png')?>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="<?=Site::staticLink('/smart/img/splash/iphone.png')?>" media="screen and (max-device-width: 320px)">

    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script>
        if (!window.jQuery) {
            document.write('<script src="<?=Site::staticLink('/smart/js/libs/jquery-2.0.2.min.js')?>"><\/script>');
        }
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
        if (!window.jQuery.ui) {
            document.write('<script src="<?=Site::staticLink('/smart/js/libs/jquery-ui-1.10.3.min.js')?>"><\/script>');
        }
    </script>

    <?if(
        date("m-d") == "04-01" &&
        (
            !isset($_COOKIE['first_apr_rotate']) ||
            ($_COOKIE['first_apr_rotate'] < 5 && rand(1, 20) == 1)
        )
    ){
        setcookie(
            "first_apr_rotate",
            max(
                1,
                (int)(isset($_COOKIE['first_apr_rotate']) ? $_COOKIE['first_apr_rotate'] : 0) + 1
            ),
            time() + 604800
        );

        ?>
        <link rel="stylesheet" type="text/css" href="<?=Site::staticLink('/css/first_apr.css')?>">
        <script src="<?=Site::staticLink('/js/first_apr.js')?>"></script>
        <script>
            jQuery(function(){
                jQuery("body,html").animate({
                    "scrollTop": jQuery("body").height()
                }, 0);

                setTimeout(function(){
                    document.first_apr_exit();
                }, 2000);
            });
        </script>
    <?}?>
</head>
<body class="smart-style-3 <?if(!Controller_Boot::controllerParamIs('sidebar')){?>menu-on-top<?}?> container fixed-header">
<header id="header" class="hidden-print">
    <div id="logo-group">
        <span id="logo" style="text-align: center; position: relative; top: -8px">
            <a
                style="font-size: 20px; line-height: 24px; white-space: nowrap; color:#EEE; text-decoration: none"
                href="/"
            >
                <!--<img src="<?=Site::staticLink('/img/logo/70.png')?>" style="width: 120px" alt="Online Baby Shop">-->
                <div style="font-family: 'Open Sans',Arial,Helvetica,Sans-Serif; font-weight: 300; line-height: 20px;font-size: 20px; margin-top: 3px">
                    Online<span style="color:#E8949C; padding: 0 2px 0 2px">Baby</span>Shop
                    <div style="font-size: 11px; line-height:12px; margin-top: 3px; color: #CCC">Одежда для любимых деток</div>
                </div>

            </a>
        </span>
    </div>
    <div class="pull-right" style="background: #2C3742">
        <?if(Controller_Boot::controllerParamIs('sidebar')){?>
            <div id="hide-menu" class="btn-header pull-right">
                <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
            </div>
        <?}?>

        <?if(false){?>
            <a class="btn btn-order" style="margin-top: 8px;" href="/shoping-cart/">
                <i class="fa fa-shopping-cart"></i> <?=Translate::t("btnCheckoutMyOrder")?>: 2
            </a>
        <?}?>

        <?if(Users::getCurrent()->id){?>
            <div class="btn-header pull-right">
                <span> <a href="/logout" title="Logout"><i class="fa fa-sign-out"></i></a> </span>
            </div>

            <div class="pull-right hidden-mobile hidden-tablet hidden-md hidden-sm">
                <a href="/account/profile/" class="btn btn-labeled btn-primary" style="margin-top: 8px">
                    <span class="btn-label"><i class="glyphicon glyphicon-user"></i></span>
                    <?
                        echo Users::getCurrent()->login;
                    ?>
                </a>
            </div>
        <?}else{?>
            <a href="/sign-in" class="btn btn-primary" style="margin-top: 8px">
                <?=Translate::t('btnLogin')?>
            </a>
        <?}?>


        <?=Site::renderRightMenuBlock()?>
    </div>
    <?=(new View_Menu_Top())->render()?>


</header>
<?= new View_Layout_Block_Sidebar() ?>

<!-- MAIN PANEL -->
<div id="main" role="main">
    <?
        if(!is_null($this->topBlock)){
            echo $this->topBlock;
        }
    ?>
    <?=new View_Layout_Block_Breadcrumb()?>
    <div id="content">
        <?=new View_Layout_Block_Header()?>
        <?=$this->content?>

    </div>
</div>
<div class="page-footer hidden-print">
    <div class="row">
        <div class="col-xs-12 col-sm-12 text-center ">
            <div style="float: right">
                <div class="lang_bottom">
                    <? /*Translate::renderLanguageSelectBtn('dropup') */ ?>
                </div>
            </div>
            <?if(Boot::isShowErrors()){?>
                <div style="float: right; padding: 0 10px 0 10px">
                    <div class="txt-color-white inline-block">
                        <a style="color: #DDD;cursor:pointer;" onclick="jQuery('#footerDebugBlock').toggle();"><?=sprintf("%0.4f sec", Boot::getRuntime())?></a>
                    </div>
                </div>
            <?}?>
            <span class="txt-color-white"><?=new View_Layout_Block_Sign()?></span>
        </div>

    </div>
    <div id="footerDebugBlock" style="display: none">
        <?= new View_Layout_Block_Footer()?>
    </div>
</div>

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?=Site::staticLink('/smart/js/plugin/pace/pace.min.js')?>"></script>

<?=$this->renderJS()?>

<script src="<?=Site::staticLink('/smart/js/bootstrap/bootstrap.min.js')?>"></script>
<script src="<?=Site::staticLink('/smart/js/app.min.js')?>"></script>
<script src="<?=Site::staticLink('/js/top-menu.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
        <?= Controller_Boot::controllerParamGet('SiteAddJSCode') ?>
    })
</script>
</body>
</html>
        <?
        return ob_get_clean();
    }
}