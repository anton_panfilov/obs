<?php

class View_Layout_Block_Sale extends View_Abstract {
    public function render(){
        ob_start();

        Site::addJSCode("
            jQuery('.carousel.slide').carousel({
				interval : 4000,
				cycle : true
			});
        ");

        ?>
        <div id="myCarousel" class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                <li data-target="#myCarousel" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <a href="/"><img src="<?=Site::staticLink("/img/sale/1.jpg")?>" alt=""></a>
                </div>
                <div class="item ">
                    <a href="/"><img src="<?=Site::staticLink("/img/sale/2.jpg")?>" alt=""></a>
                </div>
                <div class="item">
                    <a href="/"><img src="<?=Site::staticLink("/img/sale/3.jpg")?>" alt=""></a>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
        </div>
        <?

        return ob_get_clean();
    }
}