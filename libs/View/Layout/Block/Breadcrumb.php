<?php

class View_Layout_Block_Breadcrumb extends View_Abstract {
    public function render(){
        ob_start();
        if(
            Controller_Boot::controllerParamIs('breadcrumb') &&
            is_array(Controller_Boot::controllerParamGet('breadcrumb')) &&
            count(Controller_Boot::controllerParamGet('breadcrumb'))
        ){
            ?>
            <div id="ribbon" style="background: #fbfbfb; border-bottom: #F5F5F5 solid 1px; " class="hidden-print">
                <ol class="breadcrumb" style="padding: 10px 10px 7px 10px !important;">
                    <li><?=implode("</li><li>", Controller_Boot::controllerParamGet('breadcrumb'))?></li>
                </ol>
            </div>
            <?
        }

        return ob_get_clean();
    }
}