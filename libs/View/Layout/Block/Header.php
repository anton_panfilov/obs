<?php

class View_Layout_Block_Header extends View_Abstract {
    public function render(){
        ob_start();
        ?>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">

            <? if(Controller_Boot::controllerParamIs('page_title') && strlen(Controller_Boot::controllerParamGet('page_title'))){ ?>
                <h1 class="txt-color-blueDark" style="margin-bottom: 20px"><?=Controller_Boot::controllerParamGet('page_title')?></h1>
            <? } ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 text-align-right">
                <?=Site::renderRightTitleBlock()?>
            </div>
        </div>

        <?/*
            <? if(Controller_Boot::controllerParamIs('page_description') && strlen(Controller_Boot::controllerParamGet('page_description'))){ ?>
            <p style="color:#666;">
                <?=Controller_Boot::controllerParamGet('page_description')?>
            </p>
            <? } ?>
        </div>
        <h1 class="visible-print"><?=Controller_Boot::controllerParamGet('page_title')?></h1>
        <? if(
            Controller_Boot::controllerParamIs('breadcrumb') &&
            is_array(Controller_Boot::controllerParamGet('breadcrumb')) &&
            count(Controller_Boot::controllerParamGet('breadcrumb'))
        ){ ?>
            <ul class="breadcrumb hidden-print">
                <?
                $all = Controller_Boot::controllerParamGet('breadcrumb');
                $i = 0;
                foreach($all as $el){

                    echo "<li> {$el} ";
                    if($i < count($all) - 1){
                        echo ' <i class="icon-angle-right"></i> ';
                    }
                    echo "</li>";

                    $i++;
                }
                ?>
            </ul>
        <? } ?>
    </div>
</div>
    <?*/
        return ob_get_clean();
    }
}