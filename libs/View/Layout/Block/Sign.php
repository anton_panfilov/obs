<?php

class View_Layout_Block_Sign extends View_Abstract {
    public function render(){
        return 
            "<div style='color: #DDD; font-size: 12px; margin-top: 2px; font-family: \"Open Sans\",Arial,Helvetica,Sans-Serif;'>
                Online<span style='color: #E8949C; padding: 0 1px 0 1px'>Baby</span>Shop 
                <span style='color:#BBB;;'>© " . date("Y") . "</span>
            </div>";
    }
}