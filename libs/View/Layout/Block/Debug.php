<?php

class View_Layout_Block_Debug extends View_Abstract {
    /**
     * @var Exception
     */
    static protected $exception;

    static public function setException(Exception $exception){
        self::$exception = $exception;
    }

    public function render($marginTop = '100px'){
        ob_start();

        // Debug Block
        if(Boot::isShowErrors()){
            ?>
            <link href="/static/css/debug.css" rel="stylesheet" type="text/css"/>
            <div id='debug_block' class="hidden-print debug_block " style="margin-top: <?=$marginTop?>;">
                <h1>Debug Information</h1>
                <div>
                <table class="main_info">
                    <tr>
                        <td>Runtime:</td>
                        <td><?=sprintf("%0.4f sec", Boot::getRuntime())?></td>
                    </tr>
                    <tr>
                        <td>Memory Peak Usage:</td>
                        <td>
                            <?=sprintf("%.02f Kb", memory_get_peak_usage() / 1024)?>
                            <span style="color: #999; font-size: 0.9em">/ Now: <?=sprintf("%.02f Kb", memory_get_usage() / 1024)?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>CPU Runtime ~</td>
                        <td><?=Boot::getCPUUsage() ?></td>
                    </tr>
                    <tr>
                        <td>Database Runtime:</td>
                        <td><?=Boot::getDatabaseUsage() ?></td>
                    </tr>
                </table>


                <? if(count($connections = Db::getProfilerSummaryInfo())){ ?>
            <h2>
                <a style="cursor: pointer; color: #444" onclick="jQuery('#debug_db_profiler').toggle()">
                    Databases Profiler
                </a>
            </h2>
            <div id='debug_db_profiler' style="display: block">
            <table class='db_info' style="width: auto">
                <tr>
                    <td><strong>Database</strong></td>
                    <td><strong>Engine/Driver</strong></td>
                    <td><strong>Querys</strong></td>
                    <td colspan="2"><strong>Runtime</strong></td>
                    <td colspan="2"><strong>Long Query</strong></td>
                </tr>
                <? foreach($connections as $el) { ?>
                    <tr>
                        <td style="white-space: nowrap"><?= $el['database_name'] ?></td>
                        <td style="white-space: nowrap"><?= $el['database_engine_driver'] ?></td>
                        <td><?= $el['querys_count'] ?></td>
                        <td><?= sprintf("%.4f", $el['runtime']) ?></td>
                        <td><?= sprintf("%.2f", ($el['runtime'] / Boot::getRuntime()) * 100) ?>%</td>
                        <td><?= sprintf("%.4f", $el['lond_query_time']) ?></td>
                        <td style="width: 100%"><pre><?
                                $query_string = $el['lond_query'];

                                if(strlen($query_string) > 100) {
                                    $query_string = "<a style='cursor:pointer' onclick=\"jQuery(this).find('.short').toggle(); jQuery(this).find('.full').toggle();\">" . "<span class='short'>" . substr($query_string, 0, 100) . "...</span>" . "<span class='full' style='display:none'>{$query_string}</span>" . "</a>";
                                }

                                echo $query_string;
                                ?></pre>
                        </td>
                    </tr>
                <?
                } ?>
            </table>


            <h2>Queries Log</h2>

            <pre style="color:#444;"><?
                echo "1) Время выполнения запроса" . "\r\n" . "2) Время от завершения предыдущего, до начала текущего запроса" . "\r\n" . "3) Время от начала выполнения скрипта, до начала выполнения запроса" . "\r\n" . "4) Текст запроса" . "\r\n" . "\r\n" . "<sup>1,2,3</sup> если в числе нет точки, значит это стотысячные доли секунды" . "\r\n\r\n";
                ?></pre>

                <? foreach($connections as $el) { ?>
                        <div style="width: 100%; overflow: auto; white-space:nowrap; margin-top:10px; ">
                            <pre style="white-space: pre; "><?
                                echo "<div style='font-size: 11px; margin-bottom: 7px;'><strong>Database `{$el['database_name']}`";
                                // добавление информации о рынке
                                echo "</strong>:</div>";
                                echo Db::get($el['id'], null)->getProfiler()->getAllQueriesText(true);
                                echo "\r\n";
                                ?></pre>
                        </div><br><br><?
                    }
                    echo "</div>";
                }

                if(self::$exception instanceof \Exception){
                    echo "<h2>Exception Trace</h2>";

                    echo "<strong>" .
                        self::$exception->getMessage() .
                        " (" . self::$exception->getCode() . ")" . "</strong><br><br><pre>" .
                        self::$exception->getTraceAsString() .
                        "</pre><br>";

                    // \Value::export(self::$exception->getTrace());
                }

                if(count(Boot::getPhpErrors())){
                    echo "<h2>PHP Errors</h2>";

                    foreach(Boot::getPhpErrors() as $error){
                        echo "<strong>{$error['type']}</strong>: {$error['errmsg']}: <span style='color:#888;'>{$error['file']}</span> ({$error['line']})";
                        echo "<br>";
                    }
                }

                ?>
                    <h2><a style="cursor: pointer; color: #444" onclick="jQuery('#debug_querys_environment').toggle()">
                            Environment
                    </a></h2>
                    <div id='debug_querys_environment' style="display: block">
                <?

                $error['context']['_GET']       = $_GET;
                $error['context']['_POST']      = $_POST;
                $error['context']['_FILES']     = $_FILES;
                $error['context']['_COOKIE']    = $_COOKIE;
                $error['context']['_SERVER']    = $_SERVER;

                foreach($error['context'] as $k => $v){
                    echo "<pre style='margin:3px 0 7px 20px'><strong>\${$k}</strong> = ";

                    if(is_array($v) && !count($v)){
                        echo "array()";
                    }
                    /*
                    else if(is_array($v) && count($v)){
                        echo "<a
                            onclick='jQuery(this).next().toggle()'
                            style='color:#007; cursor:pointer; text-decoration:underline; font-weight: bold'
                            >" . (is_array($v) ? "array" : "object") . "(...)</a>" .
                            "<div style='display:none'>" . var_export($v, 1) . "<div>";
                    }
                    else if(is_object($v)){
                        echo "<a
                            onclick='jQuery(this).next().toggle()'
                            style='color:#007; cursor:pointer; text-decoration:underline; font-weight: bold'
                            >object</a>" .
                            "<div style='display:none'>" . var_export($v, 1) . "<div>";
                    }
                    */
                    else {
                        echo var_export($v, 1);
                    }

                    echo "</pre>";
                }

                echo "</div>";

            ?></div></div><?
        }

        return ob_get_clean();
    }
}