<?php

class View_Page_AccountIndex extends View_Abstract {
    public function render(){
        if(UserAccess::isAdmin()){
            return (new View_Page_AccountIndex_Admin())->render();
        }

        if(UserAccess::isClientManager()){
            return (new View_Page_AccountIndex_ClientManager())->render();
        }

        if(UserAccess::isClient()){
            return (new View_Page_AccountIndex_Client())->render();
        }

        /**************************/

        return new View_Page_ComingSoon();
    }
}