<?php

class View_Page_ComingSoon extends View_Abstract {
    protected $message;

    public function __construct($message = null){
        $this->setMessage($message);
    }

    public function setMessage($message){
        $this->message = $message;
        return $this;
    }

    public function render(){
        return "<br><br><center><h1><i class='fa fa-clock-o margin-right-5'></i> " .
            Translate::t("label_ComingSoon") .
        "</h1><br><br><p>{$this->message}</p></center>";
    }
}