<?php

class View_Table_Simple extends View_Abstract {
    protected $showLabels = true;

    protected $elements = [];

    /**
     * @var Html_Attribs
     */
    protected $attr_td_left;

    public function __construct(){
        $this->attr_td_left = new Html_Attribs();
    }

    /**
     * @return Html_Attribs
     */
    public function getAttrTdLeft(){
        return $this->attr_td_left;
    }

    public function add($label, $text){
        $this->elements[] = [
            'label' => $label,
            'text'  => $text,
        ];
        return $this;
    }

    /**
     * Не показывать названия
     *
     * @param bool $flag
     *
     * @return $this
     */
    public function setShowLabels($flag = true){
        $this->showLabels = (bool)$flag;
        return $this;
    }

    public function render(){
        if(!count($this->elements)) return "";

        Site::addCSS("table/simple.css");

        if(count($this->elements)){
            ob_start();

            echo "<table class='tbl_simple'><tbody>";
            foreach($this->elements as $el){
                echo "<tr>";

                if($this->showLabels){
                    echo "<td " . $this->attr_td_left->render() . ">" . Translate::t($el['label']) . "</td>";
                }

                echo "<td>{$el['text']}</td>";
                echo "</tr>";
            }
            echo "</tbody></table>";

            return ob_get_clean();
        }

        return '';
    }
}