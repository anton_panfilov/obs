<?php


/**
 * Список товаров
 *
 * Class View_List_Remains
 */

class View_List_Remains extends View_Abstract {

    /**
     * Значения, которые не будет показываться в форме, а будут зафиксированны
     *
     * @var array
     */
    protected $fixValues = [];


    /**
     * @param $type
     *
     * @return $this
     */
    public function setStatus($type){
        $this->fixValues['status'] = $type;
        return $this;
    }

    public function render(){
        Translate::addDictionary("catalog/form");

        $form = new Form();
        $form->setMethodGET();
        $form->addButton('btn_show', null);

        if(count($this->fixValues)){
            foreach($this->fixValues as $k => $v){
                $form->addElementHidden($k, $v);
            }
        }

        if(!isset($this->fixValues['manager'])){
            $form->addElement_CatalogUser("manager", 'form_manager')
                ->setRequired(false);
        }

        if(!isset($this->fixValues['status'])){
            $form->addElementSelect("status", "form_status", (new Catalog_RemainsStatus())->getArrayToSelect("form_status_all"))
                ->setRequired(false);;
        }

        $form->addElementRadio("is_order", "form_is_order", [
            "" => "form_is_order_all",
            "1" => "form_is_order_yes",
            "0" => "form_is_order_no",
        ])->setRenderTypeBigValue();

        //////////////////////////
        $form->addSeparator('form_sep_specifications_remains');

        // выбор типа
        $form->addElementSelect_CatalogType("type", "form_type", '...')
            ->setRenderTypeBigValue()
            ->setRequired(false);

        // добавление опций
        if(count(Catalog_Types::getFullParams())) {
            foreach(Catalog_Types::getFullParams() as $el) {
                $form->addElementSelect(
                    'o' . $el,
                    Catalog_Options::getTypeName($el),
                    Catalog_Options::getTypeList($el, "...")
                )
                    ->setRequired(false)
                    ->setRenderTypeBigValue();
            }
        }

        // установка зависимостей опций от типа
        foreach(Catalog_Types::getAllTypesForConditions() as $el){
            array_walk($el['options'], function(&$v){
                $v = "o{$v}";
            });
            $form->addConditionalFileds_Array("type", $el['ids'], $el['options']);
        }

        $form->isValidNotErrors();

        $values = $form->getValues();
        if(count($this->fixValues)){
            foreach($this->fixValues as $k => $v){
                $values[$k] = $v;
            }
        }

        ////////////////////////////////
        $select = Db::site()->select(
            "catalog_remains"
        )
            ->order("`status`, id desc");


        if(isset($values['type']) && $values['type'] > 0){

            ////////////////////////////////////////////
            // фильтрация по items

            $onlyItems = Db::site()->fetchCol("select id from catalog_items where `type` in (" .
                implode(", ", Catalog_Types::getAllRelationsTypes($values['type'])) .
            ")");

            if(count(Catalog_Types::getTypeInfo($values['type'])['full_params_real'])) {
                foreach(Catalog_Types::getTypeInfo($values['type'])['full_params_real'] as $option) {
                    if(
                        (
                            !count(Catalog_Types::getTypeInfo($values['type'])['remains_params']) ||
                            !in_array($option, Catalog_Types::getTypeInfo($values['type'])['remains_params'])
                        ) &&
                        strlen($form->getValue('o' . $option))
                    ) {
                        $ids = Db::site()->fetchCol(
                            "SELECT item FROM `catalog_options` WHERE `option`=? AND `value`=?",
                            $option,
                            $form->getValue('o' . $option)
                        );

                        $onlyItems = array_intersect($onlyItems, $ids);
                    }
                }
            }

            if(is_array($onlyItems)){
                if(count($onlyItems)){
                    $select->where("`item` in (" . implode(",", $onlyItems) . ")");
                }
                else {
                    $select->where("1=0");
                }
            }

            ////////////////////////////////////////////
            // фильтрация по опциям remains

            if(count(Catalog_Types::getTypeInfo($values['type'])['remains_params'])) {
                $only = null;
                foreach(Catalog_Types::getTypeInfo($values['type'])['remains_params'] as $option) {
                    if(strlen($form->getValue('o' . $option))) {
                        $ids = Db::site()->fetchCol(
                            "SELECT remains FROM `catalog_remains_options` WHERE `option`=? AND `value`=?",
                            $option,
                            $form->getValue('o' . $option)
                        );

                        if(is_null($only)) {
                            $only = $ids;
                        }
                        else {
                            $only = array_intersect($only, $ids);
                        }
                    }
                }

                if(is_array($only)){
                    if(count($only)){
                        $select->where("id in (" . implode(",", $only) . ")");
                    }
                    else {
                        $select->where("1=0");
                    }
                }
            }

        }

        if(isset($values['manager']) && $values['manager'] > 0){
            $select->where("`manager`=?", $values['manager']);
        }

        if(isset($values['status']) && $values['status'] > 0){
            $select->where("`status`=?", $values['status']);
        }

        if(isset($values['is_order']) && strlen($values['is_order'])){
            if($values['is_order'] == '1'){
                $select->where("`order`>0");
            }
            else {
                $select->where("`order`=0");
            }
        }

        ////////////////////////////////////////////////////

        $table = new Table($select);

        // id	create	manager	status	item	photo	order	count	price_purchase	price_purchase_currency	currency_rate	price	comment

        $table->addField_Text("photo", "")
            ->setTextAlignCenter()
            ->addDecoratorFunction(function($v){
                if($v){
                    return "<img style='max-height:50px' src='" . Catalog_Photo::getObject($v)->getLink(150) . "'>";
                }
            })
            ->setLink("/account/catalog/remains/?id={id}")
            ->getTdAttribs()->setCSS('padding', 0);

        if(!isset($values['status']) || !$values['status']){
            $table->addField_SetArray("status", "tbl_status", new Catalog_RemainsStatus())
                ->setTextAlignCenter();
        }

        $table->addField_Text("id", "tbl_id")
            ->setLink("/account/catalog/remains?id={id}");

        $table->addField_Date("create", "tbl_addDate")
            ->setTextAlignCenter()
            ->getDecoratorDate()->showTime(false);

        if(!isset($values['is_order']) || in_array($values['is_order'], ["", "1"])){
            $table->addField_Text("order", "tbl_in_order")
                ->setLink("/account/orders/order/?id={order}")
                ->setTextAlignCenter();
        }

        if(!isset($values['item']) || !$values['item']){
            $table->addField_CatalogItemLongID("item", "tbl_in_catalog")
                ->setTextAlignCenter();
        }

        if(!isset($values['manager']) || !$values['manager']){
            $table->addField_UserLogin('manager', 'form_manager')
                ->setTextAlignCenter();
        }

        $table->addField_Text("count", "tbl_count")
            ->setTextAlignCenter();

        /*
        $table->addField_Text("price_purchase", "tbl_price_purchase")
            ->setTextAlignCenter();
        */

        $table->addField_Text("price", "tbl_price")
            ->addDecoratorFunction(function($v){
                return String_Filter::price($v);
            })
            ->setTextAlignCenter();


        $table->startGroup("tbl_group_spec");
        $table->addField_CatalogType("item", "tbl_type")->setTextAlignCenter();

        if(count(Catalog_Types::getTypeInfo($form->getValue('type'), 0)['remains_params'])){
            foreach(Catalog_Types::getTypeInfo($form->getValue('type'), 0)['remains_params'] as $el){
                $table->addField_Function(
                    function($c, $t, $p){
                        if(!is_null(Catalog_Options::getRemainsOption($c['id'], $p['option']))){
                            return Catalog_Options::getTypeValueLabel(
                                $p['option'],
                                Catalog_Options::getRemainsOption($c['id'], $p['option'])
                            );
                        }
                    },
                    Catalog_Options::getTypeName($el)
                )
                    ->setParam("option", $el)
                    ->setTextAlignCenter();
            }
        }
        $table->endGroup();

        /*
        $table->addField_Text("comment", "tbl_comment")
            ->addDecoratorFunction(function($v){
                return String_Filter::cutByWord($v, 43);
            });
        */

        return $form->render() . $table->render();
    }
}