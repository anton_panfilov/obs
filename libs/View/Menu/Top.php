<?php

class View_Menu_Top extends View_Abstract {
    protected $content;

    public function setContent($text){
        $this->content = $text;
    }

    /**
     * @return null|View_Menu_Top_Abstract
     */
    protected function getObject(){
        if(Users::getCurrent()->isVerify()){
            //return (new View_Menu_Top_UserActivation());
        }
        else {
            if(UserAccess::isAdmin())               return (new View_Menu_Top_Admin());
            else if(UserAccess::isClient())         return (new View_Menu_Top_Client());
            else if(UserAccess::isClientManager())  return (new View_Menu_Top_ClientManager());
        }

        return null;
    }

    public function render(){
        ob_start();
        ?>
        <div class="hor-menu hidden-mobile hidden-tablet">
            <div class="navbar-inner" style="background: #2C3742">
                <?
                $object = $this->getObject();
                if($object instanceof View_Menu_Top_Abstract){
                    echo $object->render();
                }
                ?>
            </div>
        </div>
        <? return ob_get_clean();
    }

    public function renderLeft(){
        ob_start();
        ?>
        <div class="hor-menu hidden-desktop visible-mobile visible-tablet" style="width: 100%">
            <?
            $object = $this->getObject();
            if($object instanceof View_Menu_Top_Abstract){
                echo $object->renderLeft();
            }
            ?>
        </div>
        <? return ob_get_clean();
    }

    public function renderIndex(){
        ob_start();
        ?>
        <div class="hor-menu hidden-desktop visible-mobile visible-tablet" style="width: 100%; border: #AAA solid 1px; margin-bottom: 20px">
            <?
            $object = $this->getObject();
            if($object instanceof View_Menu_Top_Abstract){
                echo $object->renderIndex();
            }
            ?>
        </div>
        <? return ob_get_clean();
    }
}