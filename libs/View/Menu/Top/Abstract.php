<?php

class View_Menu_Top_Abstract extends View_Abstract {
    protected $left = [];

    protected function t($key){
        return Translate::t($key, 'topmenu');
    }

    public function renderIndex(){
        $r = "";

        if(count($this->left)){
            $activeLink = null;

            foreach($this->left as $el){
                if($el[1] == substr($_SERVER['REQUEST_URI'], 0, strlen($el[1])) && strlen($el[1]) > strlen($activeLink)){
                    $activeLink = $el[1];
                }
            }

            foreach($this->left as $k => $el){
                $attr = new Html_Attribs();

                if($el[1] == $activeLink){
                    $attr->setAttrib('class', 'active');
                }

                $this->left[$k]['attr'] = $attr;
            }

            $r.= "<nav><ul>";
            foreach($this->left as $el){
                if($el[1] != "/account/"){
                    $r.= "<li " . $el['attr']->render() . "><a href='" . htmlspecialchars($el[1]) . "'>" .
                        $el[0] .
                        "</a>" .
                        "</li>";
                }
            }

            $r.= "</ul></nav>";
        }

        return $r;
    }

    public function renderLeft(){
        $r = "";

        if(count($this->left)){
            $activeLink = null;

            foreach($this->left as $el){
                if($el[1] == substr($_SERVER['REQUEST_URI'], 0, strlen($el[1])) && strlen($el[1]) > strlen($activeLink)){
                    $activeLink = $el[1];
                }
            }

            foreach($this->left as $k => $el){
                $attr = new Html_Attribs();

                if($el[1] == $activeLink){
                    $attr->setAttrib('class', 'active');
                }

                $this->left[$k]['attr'] = $attr;
            }

            $r.= "<nav><ul class='nav topmenuLeft'>";
            foreach($this->left as $el){
                $r.= "<li " . $el['attr']->render() . "><a href='" . htmlspecialchars($el[1]) . "'>" .
                    $el[0] .
                    "</a>" .
                    "</li>";
            }

            $r.= "</ul></nav>";
        }

        return $r;
    }

    public function render(){
        $r = "";

        if(count($this->left)){
            $activeLink = null;

            foreach($this->left as $el){
                if($el[1] == substr($_SERVER['REQUEST_URI'], 0, strlen($el[1])) && strlen($el[1]) > strlen($activeLink)){
                    $activeLink = $el[1];
                }
            }

            foreach($this->left as $k => $el){
                $attr = new Html_Attribs();

                if($el[1] == $activeLink){
                    $attr->setAttrib('class', 'activeTopMenuElement');
                }

                $this->left[$k]['attr'] = $attr;
            }

            $r.= "<ul class='nav topmenu'>";
            foreach($this->left as $el){
                $r.= "<li " . $el['attr']->render() . "><a href='" . htmlspecialchars($el[1]) . "'>" .
                        $el[0] .
                    "</a>" .
                    (isset($el[2]) ? "<div class='topmenuDropDownBlock hidden-mobile hidden-tablet'><div>{$el[2]}</div>" : "") .
                "</li>";
            }

            $r.= "</ul>";
        }

        return $r;
    }
}