<?php

class View_Menu_Top_Admin extends View_Menu_Top_Abstract {
    public function __construct(){
        $this->left[] = [$this->t('plans'),        '/account/'];
        $this->left[] = [$this->t('profile'),     '/account/profile/',
            "
                <table><tr>
                    <td>
                        <h5>" . $this->t('profileUser') . "</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/profile/'>"                . $this->t('profileUserProfile')    . "</a></div>
                            <div><a href='/account/profile/contacts'>"        . $this->t("profileContacts")       . "</a></div>
                            <div><a href='/account/profile/password-change'>" . $this->t('profileChangePassword') . "</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];

        $catalogRemainsLists = "";
        foreach((new Catalog_RemainsStatus)->getArrayToSelect() as $k => $v){
            if($k == Catalog_RemainsStatus::IN_STOCK){
                $catalogRemainsLists.= "<div><a href='/account/catalog/remains-list?is_order=0&status={$k}'>{$v}</a></div>";
                $catalogRemainsLists.= "<div><a href='/account/catalog/remains-list?is_order=1&status={$k}'>" . $this->t("catalogRemainsInStockToOrder") . "</a></div>";
            }
            else {
                $catalogRemainsLists.= "<div><a href='/account/catalog/remains-list?status={$k}'>{$v}</a></div>";
            }
        }

        $this->left[] = [$this->t('catalog'),       '/account/catalog/',
            "
                <table><tr>
                    <td>
                        <div style='font-size: 16px'>
                            <div><a href='/account/catalog/create'><i class='fa fa-plus-circle' style='padding-right: 5px'></i>" . $this->t('catalogCreate') . "</a></div>
                        </div>

                        <div style='padding: 10px 0 0 10px; font-size:14px'>
                            <div>
                                <a href='/account/catalog/search'>" . $this->t('catalogSearchByID')   . "</a>
                                <a href='/account/catalog/'>" . $this->t('catalogList')   . "</a>
                            </div>
                        </div>

                        <h5>" . $this->t('catalog_gr_lists') . "</h5>
                        <div style='padding-left: 10px'>
                            {$catalogRemainsLists}
                            <!--<div><a href='/account/catalog/'>"       . $this->t('catalogListInStock')   . "</a></div>-->
                        </div>
                    </td>
                    <td>
                        <h5>" . $this->t('catalog_gr_posts') . "</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/catalog/vk-wall'>"       . $this->t('catalogVKWall')   . "</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];

        $this->left[] = [$this->t('orders'),       '/account/orders/',
            "
                <table><tr>
                    <td>
                        <div style='font-size: 16px'>
                            <div><a href='/account/orders/create'><i class='fa fa-plus-circle' style='padding-right: 5px'></i>" . $this->t('ordersCreate') . "</a></div>
                        </div>

                        <h5>" . $this->t('orders_gr_lists') . "</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/orders/'>"       . $this->t('ordersMainList')   . "</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];

        $this->left[] = [$this->t('users'),       '/account/users/',
                         "
                <table><tr>
                    <td>
                        <h5>" . $this->t('users_gr_byRoles') . "</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/users/?role=" . User_Role::ADMIN          . "'>" . $this->t('usersListAdmin')         . "</a></div>
                            <div><a href='/account/users/?role=" . User_Role::CLIENT_MANAGER . "'>" . $this->t('usersListClientManager') . "</a></div>
                            <div><a href='/account/users/?role=" . User_Role::CLIENT         . "'>" . $this->t('usersListClient')        . "</a></div>
                        </div>
                    </td>
                    <td>
                        <h5>" . $this->t('users_gr_createUser') . "</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/users/create-admin'>"          . $this->t('usersCreateAdmin')         . "</a></div>
                            <div><a href='/account/users/create-client-manager'>" . $this->t('usersCreateClientManager') . "</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];

        $this->left[] = [$this->t('servers'),       '/account/servers/',
                         "
                <table><tr>
                    <td>
                        <h5>" . $this->t('servers_gr_tools') . "</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/servers/'>" . $this->t('serversGitUpdate') . "</a></div>
                            " . (Boot::isDev() ?
                                    "<div><a href='/account/servers/update-local-cron'>" . $this->t('serversUpdateLocalCron') . "</a></div>" :
                                    ""
                            ) . "
                        </div>
                    </td>
                </tr></table>
            "
        ];
    }
}