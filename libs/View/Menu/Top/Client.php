<?php

class View_Menu_Top_Client extends View_Menu_Top_Abstract {
    public function __construct(){
        // $this->left[] = [$this->t('main'),        '/account/'];
        $this->left[] = [$this->t('profile'),     '/account/profile/',
            "
                <table><tr>
                    <td>
                        <h5>" . $this->t('profileUser') . "</h5>
                        <div style='padding-left: 10px'>
                            <div><a href='/account/profile/'>" . $this->t('profileUserProfile') . "</a></div>
                            <div><a href='/account/profile/contacts'>" . $this->t("profileContacts") . "</a></div>
                            <div><a href='/account/profile/password-change'>" . $this->t('profileChangePassword') . "</a></div>
                        </div>
                    </td>
                </tr></table>
            "
        ];
    }
}