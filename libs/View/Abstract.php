<?php

abstract class View_Abstract {
    abstract public function render();

    public function __toString(){
        return (string)$this->render();
    }
}