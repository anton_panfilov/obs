<?php

class View_Panel_MenuGroup {
    CONST TYPE_SEPARATOR = 1;

    protected $label;
    protected $icon;

    protected $elements = [];

    protected $cache_is_active;

    public function __construct($label, $icon = 'icon-file'){
        $this->label = Translate::t($label);
        $this->icon = $icon;
    }

    public function add($label, $url, $icon = null, $get = false){
        $this->cache_is_active = null;

        $this->elements[] = [
            'label' => Translate::t($label),
            'url'   => $url,
            'icon'  => $icon,
            'get'   => (bool)$get,
        ];

        return $this;
    }

    protected function isActiveElement($el){

        if(isset($el['url'])){
            if(explode('?', $el['url'], 2)[0] == $_SERVER['DOCUMENT_URI']){
                if($el['get']){
                    $get = (explode('?', $el['url'], 2)[1]);
                    if(strlen($get)){
                        $output = [];
                        parse_str($get, $output);

                        foreach($output as $k => $v){
                            if(!isset($_GET[$k]) || $_GET[$k] != $v){
                                return false;
                            }
                        }

                        return true;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return true;
                }
            }
        }

        return false;

        return (
            isset($el['url']) &&
            (
                (!$el['get'] && explode('?', $el['url'], 2)[0] == $_SERVER['DOCUMENT_URI']) ||
                ($el['get'] && $el['url'] == $_SERVER['DOCUMENT_URI'] . "?" . $_SERVER['QUERY_STRING'])
            )
        );

    }

    public function isActive(){
        if(is_null($this->cache_is_active)){
            $this->cache_is_active = false;
            if(count($this->elements)){
                foreach($this->elements as $el){
                    if($this->isActiveElement($el)){
                        $this->cache_is_active = true;
                        break;
                    }
                }
            }
        }
        return $this->cache_is_active;
    }

    public function render(){
        ob_start();

        if(count($this->elements)){
            echo "<a style='cursor: pointer'>";
            echo "<i class='fa fa-lg fa-fw {$this->icon}'></i>";
            echo "<span class='title'>{$this->label}</span>";



            echo "</a>";

            echo "<ul class='sub-menu'>\r\n";
            foreach($this->elements as $el){
                echo "<li" . ($this->isActiveElement($el) ? " class='active'" : "") . ">" .
                    "<a href='" . htmlspecialchars($el['url']) . "'>" .
                        ($el['icon'] ? "<i class='{$el['icon']}'></i> " : "") .
                        "{$el['label']}" .
                    "</a>" .
                "</li>\r\n";
            }
            echo "</ul>";
        }

        return ob_get_clean();
    }
}