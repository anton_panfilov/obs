<?php

class View_Panel_Menu extends View_Abstract {
    CONST TYPE_SEPARATOR = 1;

    CONST OPTION_LABEL      = 1;
    CONST OPTION_URL        = 2;
    CONST OPTION_ICON       = 3;
    CONST OPTION_SUB_ICON   = 4; // текст маленькой иконки над основной иконкой

    protected $elements = [];

    public function add($label, $url, $icon = 'icon-file', $options = []){
        if(!is_array($options)){
            $options = [];
        }

        $options[self::OPTION_LABEL] = Translate::t($label);
        $options[self::OPTION_URL] = $url;
        $options[self::OPTION_ICON] = $icon;

        $this->elements[] = $options;
        return $this;
    }

    /**
     * @param $label
     * @param string $icon
     * @return View_Panel_MenuGroup
     */
    public function addGroup($label, $icon = 'icon-file'){
        $this->elements[] = new View_Panel_MenuGroup($label, $icon);
        return end($this->elements);
    }


    public function render(){
        ob_start();

        echo '<nav><ul>';

        /*
        ?>
        <li>
            <a href="index.html" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
        </li>
        <li>
            <a href="inbox.html"><i class="fa fa-lg fa-fw fa-inbox"></i> <span class="menu-item-parent">Inbox</span><span class="badge pull-right inbox-badge">14</span></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">UI Elements</span></a>
            <ul>
                <li>
                    <a href="general-elements.html">General Elements</a>
                </li>
                <li>
                    <a href="buttons.html">Buttons</a>
                </li>
                <li>
                    <a href="grid.html">Grid</a>
                </li>
                <li>
                    <a href="treeview.html">Tree View</a>
                </li>
                <li>
                    <a href="nestable-list.html">Nestable Lists</a>
                </li>
                <li>
                    <a href="jqui.html">JQuery UI</a>
                </li>
                <li>
                    <a href="typography.html">Typography</a>
                </li>
            </ul>
        </li>
        <?
        */

        if(count($this->elements)){
            $blocks = [];
            foreach($this->elements as $el){
                if(is_array($el)){
                    if(isset($el['type']) && $el['type'] == self::TYPE_SEPARATOR){
                        // todo: separator
                    }
                    else {
                        // если ссылка без GET параметров, равно текущему запросу без параметров
                        $active = explode('?', $el[self::OPTION_URL], 2)[0] == $_SERVER['DOCUMENT_URI'];

                        $subicon = isset($el[self::OPTION_SUB_ICON]) ? $el[self::OPTION_SUB_ICON] : "";

                        $blocks[] = [
                            'active'    => $active,
                            'text'      => "<a href='" . htmlspecialchars($el[self::OPTION_URL]) . "'> " .
                                "<i class='fa fa-lg fa-fw {$el[self::OPTION_ICON]}'>{$subicon}</i> " .
                                "<span  class='menu-item-parent'>{$el[self::OPTION_LABEL]}</span>" .
                            "</a>"
                        ];
                    }
                }
                else if($el instanceof View_Panel_MenuGroup){
                    $blocks[] = [
                        'active' => $el->isActive(),
                        'text'   => $el->render(),
                    ];
                }
            }

            if(count($blocks)){
                for($i = 0; $i < count($blocks); $i++){
                    echo "<li";
                    if(
                        $i == 0 ||
                        $i == count($blocks) - 1 ||
                        (isset($blocks[$i]['active']) && $blocks[$i]['active'])
                    ){
                        echo " class='";

                        if(isset($blocks[$i]['active']) && $blocks[$i]['active']){
                            echo "active ";
                        }

                        echo "'";
                    }
                    echo ">{$blocks[$i]['text']}</li>";
                }
            }
        }

        echo '</ul></nav>';

        return ob_get_clean();
    }
}