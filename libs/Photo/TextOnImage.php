<?php

class Photo_TextOnImage {
    // Качество jpg по-умолчанияю
    public   $jpegQuality = 100;

    // Каталог шрифтов
    public   $ttfFontDir;

    protected $ttfFont      = false;
    protected $ttfFontSize  = false;

    protected $hImage       = false;
    protected $hColor       = false;

    protected $shadowColor  = null;
    protected $shadowSize   = 1;

    public function __construct($imagePath) {
        if (!is_file($imagePath) || !list(,,$type) = @getimagesize($imagePath)) return false;

        switch ($type) {
            case 1:  $this->hImage = @imagecreatefromgif($imagePath);  break;
            case 2:  $this->hImage = @imagecreatefromjpeg($imagePath);  break;
            case 3:  $this->hImage = @imagecreatefrompng($imagePath);  break;
            default: $this->hImage = false;
        }

        $this->ttfFontDir = Boot::$dir_libs . "/Fonts/ttf";
    }

    public function __destruct() {
        if ($this->hImage){
            imagedestroy($this->hImage);
        }
    }

    public function createThumbnail($size){

        $x = imagesx($this->hImage);
        $y = imagesy($this->hImage);

        if($x != $size){
            $nx = $size;
            $ny = $y * $size / $x;

            $temp = imagecreatetruecolor($nx, $ny);
            imagecopyresampled($temp, $this->hImage, 0, 0, 0, 0, $nx, $ny, $x, $y);

            $this->hImage = $temp;
        }
    }

    public function getWidth(){
        return imagesx($this->hImage);
    }

    public function getHeight(){
        return imagesy($this->hImage);
    }

    /**
     * Устанавливает шрифт
     *
     */
    public function setFont($font, $size = 14, $color = false, $alpha = false) {
        if (!is_file($font) && !is_file($font = $this->ttfFontDir.'/'.$font))
            return false;

        $this->ttfFont     = $font;
        $this->ttfFontSize   = $size;

        if ($color)  $this->setColor($color, $alpha);
    }


    /**
     * Пишет текст
     *
     */
    public function writeText ($x, $y, $text, $angle = 0) {
        if (!$this->ttfFont || !$this->hImage || !$this->hColor) return false;

        if($x < 0){
            $bbox = imagettfbbox($this->ttfFontSize, $angle, $this->ttfFont, $text);
            $x = imagesx($this->hImage) + $x - $bbox[2];
        }

        if($y < 0){
            $y = imagesy($this->hImage) + $y;
        }
        else {
            $y = $y + $this->ttfFontSize;
        }

        if($this->shadowSize){
            if(!$this->shadowColor){
                $shadowColor = imagecolorallocate($this->hImage, 255, 255, 255);
            }
            else {
                $shadowColor = $this->shadowColor;
            }

            imagettftext(
                $this->hImage,
                $this->ttfFontSize,
                $angle,
                $x+1,
                $y,
                $shadowColor,
                $this->ttfFont,
                $text
            );


            imagettftext(
                $this->hImage,
                $this->ttfFontSize,
                $angle,
                $x,
                $y+1,
                $shadowColor,
                $this->ttfFont,
                $text
            );

            imagettftext(
                $this->hImage,
                $this->ttfFontSize,
                $angle,
                $x-1,
                $y,
                $shadowColor,
                $this->ttfFont,
                $text
            );

            imagettftext(
                $this->hImage,
                $this->ttfFontSize,
                $angle,
                $x,
                $y-1,
                $shadowColor,
                $this->ttfFont,
                $text
            );

        }

        imagettftext(
            $this->hImage,
            $this->ttfFontSize,
            $angle,
            $x,
            $y,
            $this->hColor,
            $this->ttfFont,
            $text
        );
    }

    /**
     * Форматирует текст (согласно текущему установленному шрифту),
     * что бы он не вылезал за рамки ($bWidth, $bHeight)
     * Убирает слишком длинные слова
     */
    public function textFormat($bWidth, $bHeight, $text) {
        // Если в строке есть длинные слова, разбиваем их на более короткие
        // Разбиваем текст по строкам

        $strings   = explode("\n",
            preg_replace('!([^\s]{24})[^\s]!su', '\\1 ',
                str_replace(array("\r", "\t"),array("\n", ' '), $text)));

        $textOut   = array(0 => '');
        $i = 0;

        foreach ($strings as $str) {
            // Уничтожаем совокупности пробелов, разбиваем по словам
            $words = array_filter(explode(' ', $str));

            foreach ($words as $word) {
                // Какие параметры у текста в строке?
                $sizes = imagettfbbox($this->ttfFontSize, 0, $this->ttfFont, $textOut[$i].$word.' ');

                // Если размер линии превышает заданный, принудительно
                // перескакиваем на следующую строку
                // Иначе пишем на этой же строке
                if ($sizes[2] > $bWidth) $textOut[++$i] = $word.' '; else $textOut[$i].= $word.' ';

                // Если вышли за границы текста по вертикали, то заканчиваем
                if ($i*$this->ttfFontSize >= $bHeight) break(2);
            }

            // "Естественный" переход на новую строку
            $textOut[++$i] = ''; if ($i*$this->ttfFontSize >= $bHeight) break;
        }

        return implode ("\n", $textOut);
    }

    /**
     * Устанваливет цвет вида #34dc12
     *
     */
    public function setColor($color, $alpha = false) {
        if (!$this->hImage) return false;

        list($r, $g, $b) = array_map('hexdec', str_split(ltrim($color, '#'), 2));

        return $alpha === false ?
            $this->hColor = imagecolorallocate($this->hImage, min(255, $r+1), min(255,$g+1), min(255,$b+1)) :
            $this->hColor = imagecolorallocatealpha($this->hImage, min(255, $r+1), min(255,$g+1), min(255,$b+1), $alpha);
    }


    public function setShadow($color, $alpha = false){
        if (!$this->hImage) return false;

        list($r, $g, $b) = array_map('hexdec', str_split(ltrim($color, '#'), 2));


        return $alpha === false ?
            $this->shadowColor = imagecolorallocate($this->hImage, min(255, $r+1), min(255,$g+1), min(255,$b+1)) :
            $this->shadowColor = imagecolorallocatealpha($this->hImage, min(255, $r+1), min(255,$g+1), min(255,$b+1), $alpha);
    }

    /**
     * Выводит картинку в файл. Тип вывода определяется из расширения.
     *
     */
    public function output ($target) {
        $ext = strtolower(substr($target, strrpos($target, ".") + 1));

        switch ($ext) {
            case "gif":
                imagegif ($this->hImage, $target);
                break;

            case "jpg" :
            case "jpeg":
                imagejpeg($this->hImage, $target, $this->jpegQuality);
                break;

            case "png":
                imagepng($this->hImage, $target);
                break;

            default:
                imagepng($this->hImage, $target);
        }

        return true;
    }
}