<?php

class Conf {

    static protected $configs = [];

    /************************************************************************************************/

    /**
     * @param      $name
     * @param      $className
     * @param null $version
     *
     * @return mixed
     * @throws Exception
     */
    static protected function getConfigArray($name, $className, $version = null){
        $version_key = (int)$version;

        if(!isset(self::$configs[$name][$version_key])){
            $class_name = null;

            if(is_file($file = Boot::$dir_conf . "/{$name}.php")){
                /** @noinspection PhpIncludeInspection */
                $confArray = include $file;
                if(!is_null($version)){
                    if(isset($confArray[$version])) {
                        $confArray = $confArray[$version];
                    }
                    else {
                        throw new Exception('invalid config version: ' . $version);
                    }
                }

                // Подгрузка данных из локального конфига
                if(is_file($file = Boot::$dir_conf . "/local/{$name}.php")){
                    /** @noinspection PhpIncludeInspection */
                    $confArrayAdd = include $file;

                    if(!is_null($version)){
                        if(isset($confArrayAdd[$version])) {
                            $confArrayAdd = $confArrayAdd[$version];
                        }
                        else {
                            throw new Exception('invalid config version: ' . $version);
                        }
                    }

                    $confArray = array_merge($confArray, $confArrayAdd);
                }

                self::$configs[$name][$version_key] = new $className($confArray);
            }
            else {
                throw new Exception("config `{$name}` not found");
            }
        }

        return self::$configs[$name][$version_key];
    }

    /************************************************************************************************/

    /**
     * @return Conf_Main
     */
    static public function main() {
        return self::getConfigArray('main', 'Conf_Main');
    }

    /**************/

    /**
     * @return Conf_Mysql
     */
    static public function mysql_site() {
        return self::getConfigArray('mysql_site', 'Conf_Mysql');
    }

    /************************************************************************************************/

    static public function pgsql_test() {
        return self::getConfigArray('pgsql_test', 'Conf_Db');
    }
    /************************************************************************************************/

    /**
     * @return Conf_Twilio
     */
    static public function twilio() {
        return self::getConfigArray('twilio', 'Conf_Twilio');
    }


    /**
     * @return Conf_Vk
     */
    static public function vk() {
        return self::getConfigArray('vk', 'Conf_Vk');
    }

    /**
     * @return Conf_Servers
     */
    static public function servers() {
        return self::getConfigArray('servers', 'Conf_Servers');
    }


    /**
     * @return Conf_Security
     */
    static public function security() {
        return self::getConfigArray('security', 'Conf_Security');
    }
}