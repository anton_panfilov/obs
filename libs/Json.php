<?php

class Json {
    /**
     * Decodes a JSON string
     *
     * @param string $json
     * @return array
     */
    static public function decode($json){
        return json_decode($json, true);
    }

    /**
     * Returns the JSON representation of a value
     *
     * @param array $array
     * @return string JSON
     */
    static public function encode(array $array){
        if(count($array)){
            /**
             * Тип объекта:
             * 1 - массив
             * 2 - объект
             */
            $type = 1;
            $i = 0;

            foreach($array as $k => $v){
                if(!is_numeric($k) || $k != $i){
                    $type = 2; // Если ключ не последователен от 0, то тип меняется на объект
                    break;
                }
                $i++;
            }

            $res = ($type == 1) ? "[" : "{";
            $addsep = false;

            foreach($array as $k => $v){
                if(is_array($v)){
                    $v = self::encode($v);
                }
                else if(is_bool($v)){
                    $v = $v ? "true" : "false";
                }
                else if(is_null($v)){
                    $v = "null";
                }
                else if(is_int($v) || is_float($v)){
                    // без изменений
                }
                else if($v instanceof Json_Expr){
                    $v = (string)$v;
                }
                else if($v instanceof \Closure){
                    //
                    $v = "\"" . str_replace("\"", "\\\"", (string)$v()) . "\"";
                }
                else {
                    $v = "\"" . self::escape((string)$v) . "\"";
                }

                // добавление разделителя
                if($addsep) $res.= ",";
                $addsep = true;

                if($type == 1){
                    $res.= $v;
                }
                else {
                    $res.= "\"" . str_replace("\"", "\\\"", $k) . "\"" . ':' . $v;
                }

            }

            return $res . (($type == 1) ? "]" : "}");
        }
        else {
            return "[]";
        }
    }

    static protected function escape($value){
        return str_replace(
            ["\\",   "\r",  "\n",  "\"",  "/"],
            ["\\\\", "\\r", "\\n", "\\\"", "\\/"],
            $value
        );
    }
}