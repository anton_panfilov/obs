<?php

class Users {
    const COOKIE_USER_SESSION_NAME = 'user_login';

    const LOGIN_ERROR_INVALID       = 1;
    const LOGIN_ERROR_NOT_VERIFY    = 2;
    const LOGIN_ERROR_LOCK          = 3;

    static protected $load = false;

    static protected $current;
    static protected $last;

    static protected function load(){


        if(!self::$load){
            self::$load = true;

            if(isset($_COOKIE[self::COOKIE_USER_SESSION_NAME])){
                $a = explode("-", $_COOKIE[self::COOKIE_USER_SESSION_NAME]);
                if(count($a) == 5){
                    // cookies hashes
                    list($id, $h1, $h2, $h3, $h4) = $a;

                    $user = User::getObject(IdEncryption::decodeForInt($id));
                    if($user->id && ($user->isActive() || $user->isVerify())){
                        // эталонные хеши
                        list($h1R, $h2R, $h3R, $h4R) = $user->getLoginHashes();

                        if($h1 == $h1R && $h2 == $h2R && $h3 == $h3R && $h4 == $h4R){
                            // вход
                            self::$current = $user->id;
                            self::$last    = $user->id;
                        }
                        else if(($h1 == $h1R && $h2 != $h2R) || ($h3 == $h3R && $h4 != $h4R)){
                            // потенциальная попытка взлома, смена user->login_key
                            self::$current = 0;
                            self::$last    = 0;

                            self::deleteLoginCookie();
                        }
                        else if($h1 == $h1R && $h2 == $h2R){
                            // помним логин, ну не пускаем
                            self::$current = 0;
                            self::$last    = $user->id;
                        }
                    }
                }
            }
        }
    }

    /**
     * Получить объект пользователя, из под которого просматриваеться система
     *
     * @return User
     */
    static public function getCurrent(){
        self::load();
        return User::getObject(self::$current);
    }

    /**
     * Получить объект пользователя, которому не достаточно прав для того что бы залогиниться, но он был в системе
     *
     * @return User
     */
    static public function getLastLoginUser(){
        self::load();
        return User::getObject(self::$last);
    }

    /**
     * Залогиниться в сситеме
     * Сессия логирования при этому не иницилизируеться
     *
     * @param $login
     * @param $password
     * @return Result_Login
     */
    static public function login($login, $password){
        Translate::addDictionary("user/login");

        $result = new Result_Login();
        $result->setError($result::ERROR_INVALID_CREDENTIALS, 'loginError_invalidCredentials');

        /***************************
         * Защита от подбора
         */

        $loginHash  = sprintf("%u", crc32(trim(mb_strtolower($login))));;
        $ipBin      = inet_pton($_SERVER['REMOTE_ADDR']);

        // Если недавно были ошибки, с этого IP или
        $lastErrors = Db::site()->fetchOne(
            "SELECT count(*) FROM `users_login_errors` WHERE `create`>? AND (`login_hash`=? OR `ip`=?)",
            date('Y-m-d H:i:s', time() - 120), $loginHash, $ipBin
        );

        if($lastErrors > 5){
            return $result->setError(
                $result::ERROR_MANY_ERRORS,
                'loginError_manyErrors'
            );
        }

        /**************************/

        $id = Db::site()->fetchOne("select id from users where `login`=?", $login);

        if($id){
            $user = User::getObject($id);
            if($user->id){
                if($user->isValidPassword($password)){
                    self::$load     = true;
                    self::$current  = $user->id;
                    self::$last     = $user->id;

                    if($user->status == User_Status::ACTIVE || $user->status == User_Status::VERIFY){
                        return $result->setSuccess();
                    }
                    else if($user->status == User_Status::VERIFY){
                        $result->setError($result::ERROR_ACCOUNT_VERIFY, 'loginError_accountNotActivated');
                    }
                    else if($user->status == User_Status::LOCK) {
                        $result->setError($result::ERROR_ACCOUNT_LOCK, 'loginError_accountBlocked');
                    }
                    else {
                        $result->setError($result::ERROR_UNKNOWN, 'loginError_unknownError');
                    }
                }

            }
        }

        Db::site()->insert('users_login_errors', [
            'login_hash'    => $loginHash,
            'ip'            => inet_pton($_SERVER['REMOTE_ADDR']),
        ]);

        return $result;
    }

    /**************************************************************/

    /**
     * Установить сессию для данного пользователя
     *
     * @param User $user
     * @param bool $persistent
     */
    static public function setLoginCookie(User $user, $persistent = true){
        setcookie(
            self::COOKIE_USER_SESSION_NAME,
            IdEncryption::encodeToInt($user->id) . "-" . implode("-", $user->getLoginHashes()),
            ($persistent ? 4294967295 : null), // max или до конца сессии
            "/"
        );
    }

    /**
     * Удалить сессию - выйти
     */
    static public function deleteLoginCookie(){
        setcookie(self::COOKIE_USER_SESSION_NAME, "", 1, "/");
    }

    /**
     * Полностью разлогиниться
     */
    static public function logout(){
        self::$current  = 0;
        self::$last     = 0;
        self::$load     = true;

        unset($_COOKIE[self::COOKIE_USER_SESSION_NAME]);

        self::deleteLoginCookie();
    }

    static public function renderLoginForm(){
        Translate::addDictionary("user/login");

        $form = new Form();
        $form->addButton("btn_signIn");
        $form->addButton("btn_createNewAccount", "create", false)
            ->setLink("/create-account");

        $form->addElementText("user", "form_username")
            ->addValidator(new Form_Validator_Regexp('/^[a-z0-9\.]{4,32}$/i', 'form_error_username'));

        $form->addElementPassword("password", "form_password")
            ->setComment('<a href="/password-recovery">' . Translate::t('forgotPasswordLink') . '</a>');

        if($form->isValidAndPost()){
            $result = Users::login(
                $form->getValue('user'),
                $form->getValue('password')
            );

            if($result->is()){
                Users::setLoginCookie(Users::getCurrent());
                header('location: /account/');
            }
            else {
                $form->addErrorMessage($result->getErrorString());
            }
        }

        return $form->render();
    }


    /**
     * @param       $role
     * @param       $company_id
     * @param       $email
     * @param array $data
     *
     * @return User_Invite
     * @throws Exception
     */
    static public function createInvite($role, $company_id, $email, $data = []){
        if(!(new User_Role())->isValid($role)){
            throw new Exception("Invalid Role");
        }

        if(!(new Validator_Email())->isValid($email)){
            throw new Exception("Invalid Email");
        }

        if(!is_array($data)){
            $data = [];
        }
        $data['email'] = $email;

        $invite = new User_Invite();
        $invite->role       = $role;
        $invite->company_id = (int)$company_id;
        $invite->data       = $data;
        $invite->email      = $email;

        $invite->insert();

        return $invite;
    }
}