<?php

/**
 * Class Filter_NumbersOnly
 *
 *  Оставляет в строке только цифры, нули впереди не убирает
 *
 */

class Filter_NumbersOnly extends Filter_Abstract {
    public function filter($value, $context = []){
        return preg_replace("/[^\\d]/", "", $value);
    }   
}