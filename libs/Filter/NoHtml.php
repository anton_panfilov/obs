<?php

class Filter_NoHtml extends Filter_Abstract {
    public function filter($value, $context = []){
        return str_replace(['<', '>'], ['&lt;', '&gt;'], $value);
    }   
}