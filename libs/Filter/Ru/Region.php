<?php

class Filter_Ru_Region extends Filter_Abstract {
    public function filter($value, $context = []){
        return sprintf("%02d", (int)$value);
    }   
}