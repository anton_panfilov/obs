<?php

class Filter_Ru_PhoneRepair extends Filter_Abstract {
    protected $extension = false;

    /**
     * @param $flag
     * @return $this
     */
    public function setExtension($flag){
        $this->extension = (bool)$flag;
        return $this;
    }

    public function filter($value, $context = []){
        $ext = "";
        if($this->extension){
            $a = explode(",", $value, 2);
            $value = $a[0];

            if(isset($a[1])){
                $ext = (new Filter_NumbersOnly())->filter($a[1]);
            }
        }

        $value = (new Filter_NumbersOnly())->filter($value, $context);

        if(strlen($value) == 11 && in_array(substr($value, 0, 1), ['7', '8'])){
            $value = substr($value, 1);
        }

        if($this->extension){
            // если длинна телефона больше 10 и добавочный не задан, перевести все что больше 10 в добавочный
            if(!strlen($ext) && strlen($value) > 10){
                $value = substr($value, 0, 10);
                $ext   = substr($value, 10);
            }

            if(strlen($ext)){
                $value.= ",{$ext}";
            }
        }

        return $value;
    }
}
