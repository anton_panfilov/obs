<?php

class Filter_Ru_DocumentsAlphaNumeric extends Filter_Abstract {
    public function filter($value, $context = []){
        $value = trim($value);
        $value = urldecode(urldecode(urldecode(urldecode($value))));

        // привести к большим буквам
        $value = mb_strtoupper($value);

        if(mb_strlen($value)){
            // поменять заменяемые английские буквы на русские
            $replace = [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'АВС Е  Н  К М ОР   Т   ХУ ',
            ];

            $new = "";
            for($i = 0; $i < mb_strlen($value); $i++){
                $s = $value[$i];
                $pos = mb_strpos($replace[0], $s);

                if($pos !== false){
                    $s = mb_substr($replace[1], $pos, 1);
                }
                $new.= $s;
            }
            $value = $new;
        }

        return (new Filter_RegExpSymbolOnly("/[0-9АВСЕНКМОРТХУ]/ui"))->filter($value);
    }
}