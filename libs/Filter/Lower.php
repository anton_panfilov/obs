<?php

class Filter_Lower extends Filter_Abstract {
    public function filter($value, $context = []){
        return mb_strtolower($value);
    }   
}