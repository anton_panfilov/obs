<?php

class Filter_PrintFormat extends Filter_Abstract {
    protected $format;

    public function __construct($format){
        $this->format = $format;
    }

    public function filter($value, $context = []){
        return sprintf($this->format, $value);
    }   
}