<?php

/**
 * Class Filter_BoolInt
 *
 * Преобразование значения bool в различных написаниях true - false, yes - no, y -n, t - f ... к 1 и 0
 *
 */

class Filter_BoolInt extends Filter_Abstract {
    public function filter($value, $context = []){
        $valueLower = mb_strtolower($value);

        if(in_array($valueLower, ["yes", "true", "on", "y", "t"])){
            $value = "1";
        }
        else if(in_array($valueLower, ["no", "false", "off", "n", "f"])){
            $value = "0";
        }

        return $value;
    }   
}