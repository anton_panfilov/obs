<?php

abstract class Filter_Abstract {
    abstract function filter($value, $context = []);
}