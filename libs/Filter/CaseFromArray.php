<?php

/**
 * Class Filter_CaseFromArray
 *
 * Приведение строки к такому же регистру как и подходящее значение из списка
 *
 */

class Filter_CaseFromArray extends Filter_Abstract {

    protected $array = [];

    public function __construct($array){
        if(is_array($array)){
            $this->array = $array;
        }
        else {
            throw new Exception("Invalid Value - only array");
        }
    }

    public function filter($value, $context = []){
        if(count($this->array)){
            $vLower = mb_strtolower($value);

            foreach($this->array as $v){
                if(mb_strtolower($v) == $vLower){
                    $value = $v;
                    break;
                }
            }
        }
        return $value;
    }   
}