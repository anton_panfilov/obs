<?php

class Filter_Trim extends Filter_Abstract {
    public function filter($value, $context = []){
        return trim($value);
    }   
}