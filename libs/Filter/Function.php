<?php

class Filter_Function extends Filter_Abstract {
    protected $function;

    public function __construct(Closure $function){
        $this->function = $function;
    }

    public function filter($value, $context = []){
        $function = $this->function;
        return $function($value, $context);
    }   
}