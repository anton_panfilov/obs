<?php

class Filter_OurID extends Filter_Abstract {
    public function filter($value, $context = []){
        return String_Filter::only(
            mb_substr(trim($value), 0, 32),
            String_Hash::symbolsAlphaNumeric . ".:-/,|",
            false,
            '?'
        );
    }   
}