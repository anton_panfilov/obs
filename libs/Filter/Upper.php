<?php

class Filter_Upper extends Filter_Abstract {
    public function filter($value, $context = []){
        return strtoupper($value);
    }   
}