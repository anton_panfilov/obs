<?php

class Filter_Us_SSNRepair extends Filter_Abstract {
    public function filter($value, $context = []){
        $value = (new Filter_NumbersOnly())->filter($value);

        if($value > 0){
            $value = sprintf("%09d", $value);
        }

        return $value;
    }
}
