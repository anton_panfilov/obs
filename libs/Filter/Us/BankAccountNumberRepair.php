<?php

/**
 * Class Filter_Us_BankAccountNumberRepair
 *
 * Исправление банковского аккаунта если приходит 3-4 цифры, то дополнять до 5 ведущими нулями
 *
 */

class Filter_Us_BankAccountNumberRepair extends Filter_Abstract {
    public function filter($value, $context = []){
        $value = (new Filter_NumbersOnly())->filter($value);

        if(strlen($value) >= 3 && strlen($value) > 5){
            $value = sprintf("%05d", $value);
        }

        return $value;
    }
}
