<?php

class Filter_Us_NameRepair extends Filter_Abstract {
    public function filter($value, $context = []){
        $value = trim($value);
        $value = urldecode(urldecode(urldecode(urldecode($value))));
        $value = str_replace(",", " ", $value);

        if(!preg_match('/^([a-z]|\ |\-|\\\'|\.)*$/i', $value, $matches)){
            $new = "";
            for($i = 0; $i < strlen($value); $i++){
                if(preg_match('/^([a-z]|\ |\-|\\\'|\.)*$/i', $value[$i], $matches)){
                    $new.= $value[$i];
                }
            }
            $value = $new;
        }

        $value = str_replace(["    ", "   ", "  "], [" ", " ", " "], $value);
        $value = trim($value, " '.-\n\r\t\0\x0B");

        return $value;
    }
}
