<?php

/**
 * Class Filter_RegExpSymbolOnly
 *
 *  Фильтр оставлет в строке только символы, которые подходят под регулярное выражение
 */

class Filter_RegExpSymbolOnly extends Filter_Abstract {
    protected  $regexp;

    public function __construct($regexp){
        $this->regexp = $regexp;
    }

    public function filter($value, $context = []){
        $new = "";


        for($i = 0; $i < mb_strlen($value); $i++){
            $symb = mb_substr($value, $i, 1);
            if(preg_match($this->regexp, $symb, $matches)){
                $new.= $symb;
            }
        }
        return $new;
    }   
}