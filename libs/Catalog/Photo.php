<?php

class Catalog_Photo extends Db_Model {
    static protected $_table = 'catalog_photos';
    static protected $_structure = [

        // не обновляються при update, если явно не указанны
        ['create'],
        ['item'],
        ['key'],
        ['size'],
        ['width'],
        ['height'],
        ['type'],
        ['path'],
        ['md5'],
    ];

    protected function getDatabase() {
        return Db::site();
    }

    /*********************************/

    public $create;
    public $item;
    public $key;
    public $size;
    public $width;
    public $height;
    public $type;
    public $path;
    public $md5;

    ////////////////////////////////////////

    static protected $typesArray = [
        'image/png',
        'image/jpeg',
    ];

    static public function getTypesArray(){
        return self::$typesArray;
    }

    ////////////////////////////////////////

    static public function create($catalogItemID, $filename, $unlinkFile = true){

        $result = false;

        if(strlen($filename)){
            if(is_file($filename)){
                $md5 = md5_file($filename);

                if(!Db::site()->fetchOne("select id from catalog_photos where `item`=? and `md5`=?", $catalogItemID, $md5)) {

                    $fileType = mime_content_type($filename);

                    if(in_array($fileType, static::getTypesArray())) {
                        list($w, $h) = getimagesize($filename);

                        $obj         = new Catalog_Photo();
                        $obj->item   = (int)$catalogItemID;
                        $obj->md5    = $md5;
                        $obj->type   = $fileType;
                        $obj->size   = filesize($filename);
                        $obj->width  = $w;
                        $obj->height = $h;

                        $obj->insert();

                        ////////////////////////////////////////////

                        if(copy($filename, $obj->path)) {
                            $result = true;

                            $obj->createThumbnail(800);
                            $obj->createThumbnail(400);
                            $obj->createThumbnail(200);
                            $obj->createThumbnail(150);

                            // основная фотка
                            if(!Catalog_Item::getObject($obj->item)->main_photo){
                                Catalog_Item::getObject($obj->item)->main_photo = $obj->id;
                                Catalog_Item::getObject($obj->item)->update("main_photo");
                            }
                        }
                        else {
                            $obj->delete();
                        }
                    }
                    else {
                        // $form->addErrorMessage('invalid file type: ' . $fileType);
                    }
                }
                else {
                    // duplicate file
                }

                if($unlinkFile){
                    unlink($filename);
                }
            }
            else {
                // $form->addErrorMessage('invalid filename: ' . $filename);
            }
        }

        return $result;
    }

    public function insert(){
        $this->create = date("Y-m-d H:i:s");
        $this->key    = String_Hash::randomString(16);

        parent::insert();

        $this->path = Boot::$dir_root . "/web/static/uploads/catalog-{$this->id}-{$this->key}";
        $this->update("path");
    }

    public function delete(){
        if($this->id) {
            // отписание главной фотки
            if(Catalog_Item::getObject($this->item)->main_photo == $this->id){
                Catalog_Item::getObject($this->item)->main_photo = 0;
                Catalog_Item::getObject($this->item)->update("main_photo");
            }

            unlink($this->path);
            FileSystem::deleteDir($this->path . "_th");
            parent::delete();
        }
    }

    public function getPath($size = 800){
        return $this->path . "_th/" . $size;
    }

    public function createThumbnail($size, $new = false){
        if(!is_dir($this->path . "_th")) {
            mkdir($this->path . "_th");
        }

        if($new || !is_file($this->path . "/" . $size)) {
            // добавление надписей
            $ttfImg = new Photo_TextOnImage($this->path);

            $ttfImg->createThumbnail($size);

            if($size > 300) {
                $ttfImg->setFont('CaviarDreams.ttf', 10, "#FFFFFF");
                $ttfImg->setShadow("#888888");
                $ttfImg->writeText(-2, 3, "vk.com/online_baby_shop");
                //$ttfImg->writeText(-2, 28, "online-baby-shop.ru");
            }


            $ttfImg->setFont('CaviarDreams.ttf', $size > 300 ? 12 : 9, "#FFFFFF");
            $ttfImg->setShadow("#555555");
            $ttfImg->writeText(-2, -3, "No. " . Catalog_Item::getObject($this->item)->long_id);

            $ttfImg->output($this->path . "_th/" . $size);
        }
    }

    public function getLink($size = 800){
        return "/static/uploads/catalog-{$this->id}-{$this->key}_th/{$size}";
    }
}