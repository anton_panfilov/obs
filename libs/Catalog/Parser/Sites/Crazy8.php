<?php

class Catalog_Parser_Sites_Crazy8 extends Catalog_Parser_Abstract {
    static public function getSites(){
        return [
            'www.crazy8.com'
        ];
    }

    public function parse(){
        $page = $this->send($this->link);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // title

        $this->title = $page->bodyParserBetween2Strings('<title>', " at Crazy 8</title>");

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // description

        $this->description;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // price

        $this->price = str_replace(['&nbsp;', ' '], ['', ''], $page->bodyParserBetween2Strings('<meta property="og:price:amount" content="', '" />'));
        $this->price_currency = Currency_List::USD;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // photos

        $photo = $page->bodyParserBetween2Strings('<meta name="fbimage" property="og:image" content="', '" />');

        if(strlen($photo)){
            $this->photos = [$photo];
        }
    }
}