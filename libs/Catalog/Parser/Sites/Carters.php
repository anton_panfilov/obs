<?php

class Catalog_Parser_Sites_Carters extends Catalog_Parser_Abstract {
    static public function getSites(){
        return [
            'www.carters.com'
        ];
    }

    public function parse(){
        $page = $this->send($this->link);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // title

        $this->title = $page->bodyParserBetween2Strings("\"product_name\": [\n    \"", "\"");


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // description

        $this->description;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // price

        $this->price = str_replace(['&nbsp;', ' '], ['', ''], $page->bodyParserBetween2Strings('"product_price": [' . "\n" . '    "', '"'));
        $this->price_currency = Currency_List::USD;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // photos

        $photo = $page->bodyParserBetween2Strings(
            '<img itemprop="image" class="primary-image" src="',
            '?sw=350&amp;sh=350"'
        );

        if(strlen($photo)){
            $this->photos = [$photo];
        }
    }
}