<?php

class Catalog_Parser_Sites_GapOldNavy extends Catalog_Parser_Abstract {
    static public function getSites(){
        return [
            //'oldnavy.gap.com'
        ];
    }

    public function parse(){
        $page = $this->send($this->link);

        $pageScript = $this->send(
            "http://oldnavy.gap.com/browse/productData.do?pid=" . $page->bodyParserBetween2Strings('productPage.loadProductData("', '"')
        );

        Value::export(htmlspecialchars($page->body));

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // title

        $this->title = trim($page->bodyParserBetween2Strings('<title>', "|"));


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // description

        $this->description;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // price

        $this->price = str_replace(['&nbsp;', ' '], ['', ''], $pageScript->bodyParserBetween2Strings(
            '<span class="priceDisplay">$', '</span>'
        ));
        $this->price_currency = Currency_List::USD;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // photos

        $photo = $pageScript->bodyParserBetween2Strings(
            ",'Z': '",
            "'"
        );

        if(strlen($photo)){
            $this->photos = [$photo];
        }
    }
}









