<?php

class Catalog_Parser_Sites_Next extends Catalog_Parser_Abstract {
    static public function getSites(){
        return [
            'rus.nextdirect.com'
        ];
    }

    public function parse(){
        $page = $this->send($this->link);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // title

        $this->title = $page->bodyParserBetween2Strings('<td id="itemName"><h1>', " <!--");


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // description

        $this->description;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // price

        $this->price = str_replace(['&nbsp;', ' '], ['', ''], $page->bodyParserBetween2Strings(
            '<span class="price_RUB">', ' руб.</span>',
            $page->bodyParserBetween2Strings('<td id="PPRitemPrice">', '</tr>')
        ));
        $this->price_currency = Currency_List::RUR;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // photos

        $photo = $page->bodyParserBetween2Strings(
            '<ul><li class="selected" style="overflow: hidden;"><a rel="',
            '"'
        );

        if(strlen($photo)){
            $this->photos = [$photo];
        }
    }
}