<?php

class Catalog_Parser_Sites_Amazon extends Catalog_Parser_Abstract {
    static public function getSites(){
        return [
            'www.amazon.com'
        ];
    }

    public function parse(){
        $page = $this->send($this->link);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // title

        $this->title = $page->bodyParserBetween2Strings('<span id="productTitle" class="a-size-large">', '</span>');


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // description

        $this->description;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // price

        // <span id="priceblock_saleprice" class="a-size-medium a-color-price">$7.99</span>
        // <span id="priceblock_ourprice" class="a-size-medium a-color-price">$13.97 - $18.99</span>

        $price = $page->bodyParserBetween2Strings('<span id="priceblock_saleprice" class="a-size-medium a-color-price">', '</span>');

        if($price == ''){
            $price = $page->bodyParserBetween2Strings('<span id="priceblock_ourprice" class="a-size-medium a-color-price">', '</span>');
        }

        $price = str_replace(['&nbsp;', ' ', '$'], ['', '', ''], $price);

        if(strlen($price)) {
            $price = explode("-", $price);
            if(count($price) == 1) {
                $this->price = $price[0];
            }
            else {
                $this->price = round(($price[0] + $price[1]) / 2, 2);
            }

            $this->price_currency = Currency_List::USD;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // photos

        $photo = $page->bodyParserBetween2Strings(
            '" data-old-hires="',
            '"'
        );

        if(strlen($photo)){
            $this->photos = [$photo];
        }
    }
}