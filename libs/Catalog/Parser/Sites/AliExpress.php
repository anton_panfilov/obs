<?php

class Catalog_Parser_Sites_AliExpress extends Catalog_Parser_Abstract {
    static public function getSites(){
        return [
            'ru.aliexpress.com'
        ];
    }

    public function parse(){
        $page = $this->send($this->link);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // title

        $this->title = $page->bodyParserBetween2Strings('<h1 class="product-name" itemprop="name">', '</h1>');


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // description

        $this->description;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // price

        $price = str_replace(['&nbsp;', ' '], ['', ''], $page->bodyParserBetween2Strings(' itemprop="price">', '</span>'));

        if($price == ''){
            $price1 = str_replace(['&nbsp;', ' '], ['', ''], $page->bodyParserBetween2Strings('<span itemprop="lowPrice">', '</span>'));
            $price2 = str_replace(['&nbsp;', ' '], ['', ''], $page->bodyParserBetween2Strings('<span itemprop="highPrice">', '</span>'));

            if($price1 != 0 && $price2 != 0){
                $price = max($price1, $price2);
            }
        }

        $this->price = round($price, 2);

        $this->price_currency = Currency_List::RUR;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // photos

        $this->photos = $page->bodyParserBetween2StringsArray(
            'src="',
            '_50x50.jpg"',
            $page->bodyParserBetween2Strings(
                '<ul class="image-nav util-clearfix">', '</ul>'
            )
        );

        if(!count($this->photos)){
            $photo = $page->bodyParserBetween2Strings(
                'src="',
                '_350x350.jpg"',
                $page->bodyParserBetween2Strings('<div class="ui-image-viewer-thumb-wrap" data-role="thumbWrap">', '</div>')
            );

            if(strlen($photo)){
                $this->photos = [$photo];
            }
        }

    }
}