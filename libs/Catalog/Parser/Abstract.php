<?php

abstract class Catalog_Parser_Abstract {

    static protected $classes;
    static protected $sites;

    static public function getClasses(){
        if(is_null(self::$classes)) {
            self::$classes = [];

            foreach(FileSystem::rglob("*", __DIR__ . "/Sites") as $el) {
                if($el != 'Abstract.php') {
                    $class = "Catalog_Parser_Sites_" . substr($el, 0, -4);

                    if(class_exists($class)) {
                        self::$classes[] = $class;
                    }
                }
            }
        }

        return self::$classes;
    }

    static public function getSites(){
        if(is_null(self::$sites)) {
            self::$sites = [];

            if(count(self::getClasses())) {
                foreach(self::getClasses() as $class) {
                    $add = $class::getSites();

                    if(is_array($add) && count($add)){
                        foreach($add as $site){
                            self::$sites[] = $site;
                        }
                    }
                }
            }
        }
        return self::$sites;
    }

    /**
     * @param $link
     *
     * @return Catalog_Parser_Abstract
     */
    static public function getObject($link){
        $url = parse_url($link);

        if(isset($url['host'])){
            $runClass = null;

            if(count(self::getClasses())) {
                foreach(self::getClasses() as $class) {
                    $add = $class::getSites();

                    if(is_array($add) && count($add)){
                        foreach($add as $site){
                            if($url['host'] == $site) {
                                $runClass = $class;
                                break;
                            }
                        }
                    }
                }
            }

            if($runClass){
                $obj = (new $runClass($link));
                $obj->parse();

                return $obj;
            }

            return null;
        }
    }

    /////////////////////////////////////////////

    /////////////////////////////////////////////

    public $link;

    public $title;
    public $price;
    public $price_currency;
    public $description;
    public $photos;

    public function __construct($link){
        $this->link = $link;
    }

    abstract public function parse();

    //////////////////////////////////////////////

    /**
     * Используемый при подключениях User-Agent
     *
     * @var string
     */
    public $userAgent = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)';

    /**
     * Кукисы этой сессии
     * Они автоматически сохраняются при использовании функций работы с сетью.
     *
     * @var array
     */
    public $cookies = array();

    /**
     * Последняя посещенная страница
     *
     * @var string
     */
    public $referer = '';

    /**
     * Лог отправки
     *
     * @var array
     */
    public $sendLog = array();

    /**
     * Преобразование переменной в Post строку.
     *
     * Если в качаестве переменной передать массив, он преобразуется в POST строку
     * В другом случае, строка останется без исключений
     *
     * @param mixed $value
     * @param string $parent - для внутеннего использования при формировании POST строки
     *
     * @return string
     */
    private function valueToUrl($value, $parent = null){
        if(is_array($value) && count($value)){
            $r = "";
            foreach($value as $k => $v){
                if(strlen($r))$r.= "&";
                if(is_array($v)){
                    if(is_null($parent))    $parent = urlencode($k);
                    else                    $parent.= "[" . urlencode($k) . "]";

                    $r.= $this->valueToUrl($v, $parent);
                }
                else {
                    if(is_null($parent))    $r.= urlencode($k) . "=" . urlencode($v);
                    else                    $r.= "{$parent}[" . urlencode($k) . "]=" . urlencode($v);
                }
            }
            return $r;
        }
        else {
            return $value;
        }
    }

    protected function setCookiesFromHeader($header){
        $h = explode("\n", $header);
        if(count($h) > 1){
            $nowSetCookie = false;
            for($i = 1; $i < count($h); $i++){
                $el = trim($h[$i]);
                if(strtolower(substr($el, 0, 11)) == strtolower("Set-Cookie:")){
                    $nowSetCookie = true;
                    $run = trim(substr($el, 11));
                }
                else {
                    // Если предидущая строка была Set-Cookie
                    if($nowSetCookie){
                        $pos2Dots = @strpos(":", $el);
                        $posRavno = @strpos("=", $el);

                        if($posRavno && ($pos2Dots === false || $posRavno < $pos2Dots)){
                            $run = $el;
                        }
                        else {
                            $nowSetCookie = false;
                        }
                    }
                }

                if($nowSetCookie){
                    // Если у котого то что то не работает с этим вариантом пишите мне я поправлю. Антон
                    // Я на 99% уверен что тут не надо делать перевор параметров через ;, т.к. по спецификации
                    // через точку с запятой передаются параметры, типа имя жихни переменной, её область видимости и т.д.
                    // Именно поэтому я буру только первый элемент.
                    $data = explode(";", $run);

                    // Имя и значение
                    list($name, $value) = explode("=", $data[0], 2);
                    $this->cookies[$name] = $value;
                }
            }
        }
    }

    /**
     * Отправка данных
     *
     * @param mixed $url
     * @param array $post
     * @param array $get
     * @return Catalog_Parser_Response
     */
    protected function send($url, $post = null, $get = null, $connectName = null){
        // преобразование данных.
        $data = $this->valueToUrl($post);

        if ($get){
            $path = Http_Url::convert($get, $url);
        }

        $head = array(
            'Expect:',
        );
        // настрйока отправки
        $ch = curl_init();

        // для отпавки методом POST.
        curl_setopt(    $ch,    CURLOPT_URL,               $url                            );
        if ($post){
            curl_setopt(    $ch,    CURLOPT_POST,          0                               );
            curl_setopt(    $ch,    CURLOPT_POSTFIELDS,    $data                           );
        }

        curl_setopt(    $ch,    CURLOPT_USERAGENT,         $this->userAgent                );

        curl_setopt(    $ch,    CURLOPT_HTTPHEADER,        $head                           );
        curl_setopt(    $ch,    CURLOPT_FAILONERROR,       1                               );
        curl_setopt(    $ch,    CURLOPT_HEADER,            1                               );
        curl_setopt(    $ch,    CURLOPT_RETURNTRANSFER,    1                               );
        curl_setopt(    $ch,    CURLOPT_SSL_VERIFYPEER,    false                           );
        curl_setopt(    $ch,    CURLOPT_SSL_VERIFYHOST,    false                           );
        curl_setopt(    $ch,    CURLOPT_TIMEOUT,           120                             );
        curl_setopt(    $ch,    CURLINFO_HEADER_OUT,       true                            );
        curl_setopt(    $ch,    CURLOPT_HTTP_VERSION,      CURL_HTTP_VERSION_1_1           );
        curl_setopt(    $ch,    CURLOPT_ENCODING,          'gzip'                          );
        curl_setopt(    $ch,    CURLOPT_FOLLOWLOCATION,    1          );

        if($this->referer){
            curl_setopt(    $ch,    CURLOPT_REFERER,           $this->referer              );
        }

        if(is_array($this->cookies) && count($this->cookies)){
            $cookies = "";;
            foreach($this->cookies as $name => $opt){
                if(strlen($cookies))$cookies.= "; ";
                $cookies.= "{$name}={$opt}";
            }

            curl_setopt(    $ch,    CURLOPT_COOKIE,      $cookies);
        }

        // отправка данных, получение ответа, отделение header от body($return)
        $output         = curl_exec($ch);
        $header_size    = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $headers        = substr($output, 0, $header_size - 4);


        $return         = substr($output, $header_size);

        $request_header = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        $request_header = substr($request_header, 0, strlen($request_header)-4);

        $this->setCookiesFromHeader($headers);

        $result = new Catalog_Parser_Response();

        $result->requestURL = $url;
        //$result->requestURL_PathOnly = $path;

        $result->requestHeader = $request_header;
        $result->requestData = $data;


        // анализ отправки
        if(curl_errno($ch)){
            $result->success            = false;
            $result->reason             = curl_error($ch);
            $result->curl_error_number  = curl_errno($ch);
        }
        else {
            // Отправка завершена успешно
            curl_close($ch);

            $this->referer = $url;

            $result->success = true;
            $result->header  = $headers;
            $result->body    = $return;
        }

        $this->sendLog[] = $result->getLogArray();
        return $result;
    }
}