<?php

class Catalog_Parser_Response {
    /**
    * Флаг успешного выполнения запроса
    * 
    * @var bool
    */
    public $success = false;
    
    /**
    * Текст ошибки
    * 
    * @var string
    */
    public $reason;
    
    /**
    * Номер ошибки, который отдает CURL
    * 
    * @var int
    */
    public $curl_error_number = 0;
    
    /**
    * Строка заголовков ответа
    * 
    * @var string
    */
    public $header = '';
    
    /**
    * Строка тела ответа
    * 
    * @var string
    */
    public $body = '';
    
    
    public $requestURL = '';
    public $requestURL_PathOnly = '';
    
    public $requestHeader = '';
    public $requestData = '';
    
    /////////////////////// Header ///////////////////////////////////////////////////// 
    protected $headerArray;
    
    /**
    * Разобрать заголовки ответа
    * 
    */
    protected function headerParse(){
        if(is_null($this->headerArray)){
            $this->headerArray = array();
            
            if($this->success){
                $headerArr = explode("\n", $this->header);
                if(is_array($headerArr) && count($headerArr)){
                    foreach($headerArr as $el){
                        $el = trim($el);
                        if(strlen($el)){
                            $lineArr = explode(":", $el, 2);
                            if(is_array($lineArr) && count($lineArr) == 2){
                                $name = trim(strtolower($lineArr[0]));
                                if(!isset($this->headerArray[$name]))$this->headerArray[$name] = array();
                                $this->headerArray[$name][] = trim($lineArr[1]);   
                            }
                        }        
                    }   
                }
            }
        }   
        
        return $this->headerArray;
    }

    /**
     * Получить переменную из header
     *
     * @param     $name
     * @param int $num
     *
     * @return null
     */
    public function header($name, $num = 0){
        $this->headerParse(); 
        $name = trim(strtolower($name));
        
          
        
        if(isset($this->headerArray[$name][$num])){
            return $this->headerArray[$name][$num];
        }  
        return null;
    }
    
    /**
    * Возвращает только path из http заголовка location
    * 
    * Если там присутсвовал домен, он обрежется.
    * Если этого заголовка нет, функция вернет NULL
    * 
    * @return string|null HTTP.Header.location.path() 
    */
    public function getLocationPath(){
        $path = null;
        $url = $this->header("location");
        
        if(strlen($url)){     
            if(substr($url, 0, 7) == 'http://' || substr($url, 0, 8) == 'https://'){
                $path = strstr(substr($url, 8), "/");
                if($path === false)$path = "/";   
            } 
            else {
                $path = $url;  
            }        
        }
        
        return $path;
    }

    public function getValFromResponseBody($name){

        $pattern = "<input[^<>]*name=[^<>]*".$name."[^<>]*.*?value=[\"'](.*?)[\"'].*?>";
        preg_match($pattern,$this->body,$matches);
        if (isset($matches['1'])){
            return $matches['1'];
        }else{
            return false;
        }
    }
    
    
    /////////////////////// Body /////////////////////////////////////////////////////

    public function bodyParserBetween2Strings($startString, $endString = null, $str = null){
        if(is_null($str))$str = $this->body;

        // Поиск, есть ли входящая подстрока
        $pos = mb_stripos($str, $startString);
        if($pos === false) return null;

        // Обрезать текст до первой подстроки, включаяя её саму
        $str = mb_substr($str, $pos + mb_strlen($startString));

        if(is_null($endString)){
            $pos = mb_strlen($str);
        }
        else {
            // Проверить, есть ли в тсавшеся тексте вторая строка
            $pos = mb_stripos($str, $endString);
            if($pos === false){
                return null;
            }
        }

        // Если есть то обрезать текст до неё
        return mb_substr($str, 0, $pos);
    }

    public function bodyParserBetween2StringsArray($startString, $endString, $str = null){
        if(is_null($str))$str = $this->body;

        $result = [];

        do{
            $pos = mb_stripos($str, $startString);
            if($pos === false){
                break;
            }

            // Обрезать текст до первой подстроки, включаяя её саму
            $str = mb_substr($str, $pos + mb_strlen($startString));


            // Проверить, есть ли в тсавшеся тексте вторая строка
            $pos = mb_stripos($str, $endString);
            if($pos === false){
                break;
            }

            $result[] = mb_substr($str, 0, $pos);
            $str = mb_substr($str, $pos + mb_strlen($endString));

        }
        while(true);

        return $result;
    }
    
    public function getLogArray(){
        return array(
            'success' => $this->success,
            'reason' => $this->reason,
            'curl_ernum' => $this->curl_error_number,
            'request' => array(
                'head' => $this->requestHeader,
                'body' => $this->requestData,
            ),
            'response' => array(
                'head' => $this->header,
                'body' => $this->body,
            )
        );
    }
    
        
    public function getHiddenValues()
    {
        // Parsing hidden inputs from the whole returned source 
        $h = explode("input", $this->body);

        $hidd = array();
        foreach($h as $el)
        {
            if(preg_match('/type="hidden"/i',$el))
            {
                $hidd[$this->bodyParserBetween2Strings('name="','"',$el)]=$this->bodyParserBetween2Strings('value="','"',$el);
            }
        }
        return $hidd;
    }   
}