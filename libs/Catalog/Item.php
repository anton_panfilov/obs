<?php

class Catalog_Item extends Db_Model {
    static protected $_table = 'catalog_items';
    static protected $_structure = [

        // не обновляються при update, если явно не указанны
        ['type'],
        ['create'],
        ['long_id'],
        ['main_photo'],
        ['user'],
        ['title'],
        ['link'],
        ['description'],
        ['price_purchase'],
        ['price_purchase_currency'],
        ['price'],

        ['stock'],
        ['to_order'],
        ['remains_not_sale'],
    ];

    protected function getDatabase(){
        return Db::site();
    }

    /*********************************/

    public $type = Catalog_Types::TYPE_UNKNOWN_ID;
    public $create;
    public $long_id;
    public $main_photo = 0;
    public $user;
    public $title;
    public $link;
    public $description;
    public $price_purchase;
    public $price_purchase_currency;
    public $price;

    protected $stock   = 0; // cache
    protected $to_order = 1;
    protected $remains_not_sale = 0; // cache

    /*************************************************************************/



    /*************************************************************************/

    public function insert(){
        $this->create = date('Y-m-d H:i:s');
        $this->user = Users::getCurrent()->id;

        parent::insert();

        $this->long_id = IdEncryption::encodeToInt($this->id);
        $this->update(['long_id']);
    }

    /**
     * Активность товара
     *
     * @return bool
     */
    public function isActive(){
        return ($this->to_order || $this->remains_not_sale);
    }

    /**
     * Есть ли в наличие
     *
     * @return int
     */
    public function isStock(){
        return $this->stock;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $type
     * @param $values
     *
     * @return bool
     */
    public function setOptions($type, $values){
        return Catalog_Options::setOptions($this->id, $type, $values);
    }

    /**
     * @return bool
     */
    public function deleteNotActualOptions(){
        return Catalog_Options::deleteNotActualOptions($this->id);
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getOptions($type = null){
        return Catalog_Options::getOptions($this->id, $type);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return array
     */
    public function getStock(){
        return Catalog_Options::getOptions($this->id);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected function renderForm_decoratorPhotos($content){
        if($this->id){
            $all = Db::site()->fetchAll(
                "SELECT id, `key` FROM `catalog_photos` WHERE item=?",
                $this->id
            );

            if(count($all) > 1) {
                ob_start();
                ?>
                <div class="row-fluid">
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" style="margin-bottom: 70px">
                        <?= $content ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <?

                        if(count($all)) {
                            foreach($all as $el) {
                                ?>
                                <div id="photo_<?= $el['id'] ?>" style="margin-bottom: 20px; text-align: center">
                                    <div>
                                        <a href="/account/catalog/item/photos?id=<?=$this->long_id?>#photo_<?=$el['id']?>">
                                            <img src="/static/uploads/catalog-<?= $el['id'] ?>-<?= $el['key'] ?>_th/150">
                                        </a>
                                    </div>
                                </div>
                            <?
                            }
                        }
                        ?>
                    </div>
                </div>
                <br clear="both">
                <?
                return ob_get_clean();
            }
        }

        return $content;
    }

    protected function renderForm_main(){
        $form = new Form();
        $form->addButton("save");

        $form->addElementRadio("to_order", "form_to_order", [
            '1' => 'form_to_order_yes',
            '0' => 'form_to_order_no',
        ])
            ->setLabelWidth("100px")
            ->setValue($this->to_order)
            ->setRenderTypeBigValue();

        $form->addElementStatic("form_stock",               (new Ui_Bool)->setInLine()->render($this->stock));

        $form->addElementStatic("form_remains_not_sale",    (new Ui_Bool)->setInLine()->render((int)(bool)$this->remains_not_sale))
            ->setComment("form_remains_not_sale_comment")
            ->setRenderTypeBigValue();

        if($form->isValidAndPost()){
            if($this->to_order != $form->getValue("to_order")){
                $this->to_order = $form->getValue("to_order");
                $this->update("to_order");
            }
        }

        $formSpec = new Form();
        $formSpec->disableButtons();

        if($this->remains_not_sale){
            $formSpec->addSeparator("form_sep_remains_not_sale");

            $select = Db::site()->select("catalog_remains")
                ->where("`item`=?", $this->id)
                ->where("`status`!=?", Catalog_RemainsStatus::SOLD)
                ->order(['status', 'id desc']);

            $table = new Table($select);
            $table->setPagingDisabled();

            $table->addField_Text("photo", "")
                ->addDecoratorFunction(function($v){
                    if($v){
                        return "<img style='max-height:28px' src='" . Catalog_Photo::getObject($v)->getLink(150) . "'>";
                    }
                })
                ->setLink("/account/catalog/remains/?id={id}")
                ->getTdAttribs()->setCSS('padding', 0);

            $table->addField_Text("id", "tbl_id")
                ->setLink("/account/catalog/remains/?id={id}");

            $table->addField_Date("create", "tbl_addDate");
            $table->addField_SetArray("status", "tbl_status", new Catalog_RemainsStatus());
            $table->addField_Text("count", "tbl_count")->setTextAlignCenter();
            $table->addField_Text("price", "tbl_price")
                ->addDecoratorFunction(function($v){
                    return String_Filter::price($v) . " <small class='txt-color-blueLight'>" . Translate::t("RUR") . "</small>";
                })
                ->setTextAlignCenter();

            $formSpec->addElementStatic("", $table->render())->setRenderTypeFullLine();
        }

        $vkPosts = $this->getVkPosts();
        if(count($vkPosts)){
            $formSpec->addSeparator("form_sep_vk_posts");

            $table = new Table($vkPosts);
            $table->setPagingDisabled();


            $table->addField_Text("photo", "")
                ->addDecoratorFunction(function($v){
                    return "<img style='max-height:28px' src='" . Catalog_Photo::getObject($v)->getLink(150) . "'>";
                })
                ->setLink("https://vk.com/photo{vk_owner}_{vk_photo}", true)
                ->setWidthMin()
                ->getTdAttribs()->setCSS('padding', 0);

            $table->addField_Function(function($c){
                $link = "https://vk.com/photo{$c['vk_owner']}_{$c['vk_photo']}";
                return "<a href='{$link}' target='_blank'>{$link}</a>";
            }, "tbl_vk_link_and_caption");

            $table->addField_SetArray("item_type", "tbl_item_type", new Catalog_Vk_Type);

            $formSpec->addElementStatic("", $table->render())->setRenderTypeFullLine();
        }

        $formSpec->addSeparator("form_sep_specifications");
        $formSpec->addElementStatic("form_type",    Catalog_Types::getTypeInfo($this->type, false)['full_name'])
            ->setRenderTypeBigValue();

        if(count(Catalog_Options::getOptions($this->id))){
            foreach(Catalog_Options::getOptions($this->id) as $k => $v){
                $formSpec->addElementStatic(
                    Catalog_Options::getTypeName($k),
                    Catalog_Options::renderString($this->id, $k)
                )
                    ->setRenderTypeBigValue();
            }
        }


        return $this->renderForm_decoratorPhotos($form->render() . "<br>" . $formSpec->render());
    }

    protected function renderForm_edit(){
        $form = new Form();

        if($this->id){
            $form->addButton('btn_save');
        }
        else {
            $form->setMarkRequiredFields(true);
            $form->addButton('btn_continue');
        }

        //////////////////////////////////////////////////////////////////////////////

        /*
            title
            link
            gender
            age
            options
            description
            price_purchase
            price_purchase_currency
            price
        */

        $form->addElementTextarea('title', 'form_title')
            ->setAutoHeight()
            ->setRenderTypeBigValue();

        $form->addElementTextarea('link', 'form_link')
            ->setAutoHeight()
            ->setRequired(false)
            ->setRenderTypeBigValue()
            ->setComment(
                (new Ui_Button("btn_open_buyer_link"))
                    ->setScript('window.open(jQuery("#link").val(), "_blank")')
                    ->render()
            );

        $form->addElementTextarea("description", "form_description")
            ->setRequired(false)
            ->setRenderTypeBigValue();

        ////////////////////////////////////////////////////////
        $form->addSeparator('form_sep_main_photo');

        $form->addElementImagePicker(
            "main_photo", "form_main_photo",
            ["0" => "/static/img/nophoto.png"] + Catalog_Item::getObject($this->id)->getImagesLinks()
        )->setRenderTypeFullLine();

        ////////////////////////////////////////////////////////
        $form->addSeparator('form_sep_price');

        $form->addElementText_Price('price_purchase', 'form_price_purchase');

        $form->addElementSelect('price_purchase_currency', 'form_price_purchase_currency', (new Currency_List())->getArrayToSelect());

        $form->addMultiElement(['price_purchase', 'price_purchase_currency'], 'form_price_purchase_all');

        $form->addElementText('price', 'form_price')
            ->setRequired(false);


        ////////////////////////////////////////////////////////
        $form->addSeparator('form_sep_specifications');


        // выбор типа
        $form->addElementSelect_CatalogType("type", "form_type", '...')
            ->setRenderTypeBigValue()
            ->setRequired(false);


        if($this->id){
            // добавление опций
            if(count(Catalog_Types::getFullParams())) {
                foreach(Catalog_Types::getFullParams() as $el) {
                    $form->addElementCheckboxes(
                        'option_' . $el,
                        Catalog_Options::getTypeName($el),
                        Catalog_Options::getTypeListForCheckboxes($el)
                    )
                        ->setLabelWidth('200px')
                        ->setRequired(false)
                        ->setRenderTypeBigValue()
                        ->setValue($this->getOptions($el));
                }
            }

            // установка зависимостей опций от типа
            foreach(Catalog_Types::getAllTypesForConditions() as $el){
                array_walk($el['options'], function(&$v){
                    $v = "option_{$v}";
                });
                $form->addConditionalFileds_Array("type", $el['ids'], $el['options']);
            }

        }

        if($this->id){
            $form->setValues([
                'type'                    => $this->type,
                'title'                   => $this->title,
                'link'                    => $this->link,
                'description'             => $this->description,
                'main_photo'              => $this->main_photo,
                'price_purchase_currency' => $this->price_purchase_currency,
                'price_purchase'          => $this->price_purchase,
                'price'                   => $this->price,
            ]);
        }

        if($form->isValidAndPost()){
            $this->type        = $form->getValue('type');
            $this->title       = $form->getValue('title');
            $this->link        = $form->getValue('link');
            $this->description = $form->getValue('description');
            $this->main_photo  = $form->getValue('main_photo');

            $this->price                    = $form->getValue('price');
            $this->price_purchase           = $form->getValue('price_purchase');
            $this->price_purchase_currency  = $form->getValue('price_purchase_currency');

            if($this->id){
                $form->addSuccessMessage();

                $this->update(['type', 'title', 'link', 'description', 'main_photo', 'price', 'price_purchase', 'price_purchase_currency']);

                // сохранение опций

                // удаление не актуальных опций
                $this->deleteNotActualOptions();

                if($this->type){
                    // сохранение настроенных опций
                    if(count(Catalog_Types::getTypeInfo($this->type)['full_params_real'])) {
                        foreach(Catalog_Types::getTypeInfo($this->type)['full_params_real'] as $el) {
                            $this->setOptions(
                                $el, $form->getValue('option_' . $el)
                            );
                        }
                    }

                    // сохранение фиксированных опций
                    if(count(Catalog_Types::getTypeInfo($this->type)['fix_options'])){
                        foreach(Catalog_Types::getTypeInfo($this->type)['fix_options'] as $k => $v) {
                            $this->setOptions($k, $v);
                        }
                    }
                }
            }
            else {
                // создание нового заказа
                $this->insert();

                header('location: /account/catalog/item/?id=' . $this->long_id);
            }

        }

        return $form->render();
    }

    public function renderFormPhotos(){
        Translate::addDictionary('catalog/form', 20);

        // удаление фотографии
        if(isset($_GET['deletePhoto']) && $_GET['deletePhoto'] > 0){
            $obj = Catalog_Photo::getObject((int)$_GET['deletePhoto']);
            if($obj->id){
                $obj->delete();
            }

            return null;
        }

        $form = new Form();
        $form->addButton('btn_upload_add');

        $form->addElementRadio('upload_type', 'form_upload_type', [
            'upload' => 'upload_type_upload',
            'link'   => 'upload_type_link',
        ])
            ->setRenderTypeFullLine()
            ->setValue('upload');

        $form->addElementFile('upload_photo', '')
            ->setRenderTypeFullLine();

        $form->addElementTextarea('upload_link', 'form_upload_link')
            ->addValidator(new Form_Validator_Url())
            ->setPlaceholder('form_upload_link_placeholder')
            ->setAutoHeight()
            ->setRenderTypeFullLine();

        $form->addConditionalFileds_Array('upload_type', 'upload', 'upload_photo');
        $form->addConditionalFileds_Array('upload_type', 'link',   'upload_link');


        if(isset($_POST['upload_type']) && $form->isValidAndPost()){
            $typesArray = Catalog_Photo::getTypesArray();

            $filename = null;

            if($form->getValue('upload_type') == 'link'){
                $filename = "/tmp/uploadfile_" . time() . rand(1000,9000);
                file_put_contents($filename, file_get_contents($form->getValue('upload_link')));
            }
            else {
                if(in_array($_FILES['upload_photo']['type'], $typesArray)) {
                    $filename = $_FILES['upload_photo']['tmp_name'];
                }
                else {
                    $form->addErrorMessage('Invalid file type: ' . $_FILES['upload_photo']['type']);
                }
            }

            if(!Catalog_Photo::create($this->id, $filename)){
                $form->addErrorMessage("Invalid file upload");
            }
        }

        $formMainPhoto = new Form();
        $formMainPhoto->addElementImagePicker(
            "main_photo", "form_main_photo",
            ["0" => "/static/img/nophoto.png"] + Catalog_Item::getObject($this->id)->getImagesLinks()
        )
            ->setValue($this->main_photo)
            ->setRenderTypeFullLine();


        if(isset($_POST['main_photo']) && $formMainPhoto->isValidAndPost()){
            $this->main_photo = $formMainPhoto->getValue("main_photo");
            $this->update("main_photo");
        }

        $all = Db::site()->fetchAll(
            "SELECT * FROM `catalog_photos` WHERE item=?",
            $this->id
        );

        if(!count($all)){
            $form->setTitle("h_addPhoto");
            return $form->render();
        }

        ob_start();
        ?>
        <div class="row-fluid">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" style="margin-bottom: 30px">
                <h4 class='h_border'><?=Translate::t("h_addPhoto")?></h4>
                <?=$form->render()?>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" style="margin-bottom: 30px">
                <h4 class='h_border'><?=Translate::t("h_mainPhoto")?></h4>
                <?=$formMainPhoto->render()?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 70px">
                <h4 class='h_border'><?=Translate::t("h_photos")?></h4>
                <?

                Site::addJSCode("
                    document.delete_photo = function(id){
                        jQuery('#photo_' + id).hide('fast');
                        jQuery.getJSON(
                            '" . Http_Url::convert([]) . "',
                            {
                                'deletePhoto': id
                            }
                        );
                    }
                ");

                if(count($all)){
                    foreach($all as $el){
                        ?>
                            <div id="photo_<?=$el['id']?>" style="margin-bottom: 20px;">
                                <a name="photo_<?=$el['id']?>"><br>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px; background: #FFF; border: #EEE solid 1px; border-bottom: 0;">
                                    <center>
                                        <img style="max-width: 400px; width: 100%" src="/static/uploads/catalog-<?=$el['id']?>-<?=$el['key']?>_th/400">
                                    </center>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 10px; background: #F9F9F9; border: #EEE solid 1px; ">
                                    <?
                                    $tbl = new View_Table_Simple();

                                    $tbl->add("tbl_create", (new Decorator_Date())->render($el['create']));
                                    $tbl->add("tbl_size",   "{$el['width']} x {$el['height']} <small class='txt-color-blueLight'>(" .
                                            round($el['size'] / 1024) . " " . Translate::t("tbl_size_kb") .
                                        ")</small>"
                                    );
                                    $tbl->add("tbl_type",   $el['type']);

                                    /*
                                    $tbl->add(
                                        "",
                                        (new Ui_Button("btn_delete_photo"))
                                            ->setScript("document.delete_photo('{$el['id']}')")
                                            ->setConfirm("confirm_delete_photo")
                                    );
                                    */

                                    echo $tbl->render();
                                    ?>
                                </div>
                                    <br clear="both">
                            </div>
                        <?
                    }
                }
                else {
                    echo "<br><br><center><h2>" . Translate::t("mess_noPhotos") . "</h2></center>";
                }

                ?>
            </div>

        </div>
        <br clear="both">
        <?
        return ob_get_clean();

    }


    public function renderForm(){
        Translate::addDictionary('catalog/form', 20);

        if(!$this->id){
            return $this->renderForm_edit();
        }

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab('main', 'tab_main');
        $tabs->addTab('edit', 'tab_edit');

        $method = 'renderForm_' . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $content = $this->$method();
        }
        else {
            $content = new View_Page_ComingSoon();
        }

        $tabs->setText($content);

        return $tabs->render();
    }

    ///////////////////////////////////

    public function getImagesLinks($size = 150){
        // todo optimize

        $result = [];
        $photos = Db::site()->fetchAll("SELECT id, `key` FROM `catalog_photos` where `item`=? ORDER BY id", $this->id);

        if(count($photos)){
            foreach($photos as $photo){
                $result[$photo['id']] = "/static/uploads/catalog-{$photo['id']}-{$photo['key']}_th/{$size}";
            }
        }

        return $result;
    }

    public function getImageLink($size = 150){
        if($this->main_photo && Catalog_Photo::getObject($this->main_photo)->id){
            return Catalog_Photo::getObject($this->main_photo)->getLink($size);
        }

        return null;
    }

    public function getImagePath($size = 800){
        if($this->main_photo && Catalog_Photo::getObject($this->main_photo)->id){
            return Catalog_Photo::getObject($this->main_photo)->getPath($size);
        }

        return null;
    }

    /**
     * Получить название не длинее $len символов
     *
     * @param int $len
     *
     * @return string
     */
    public function getShortTitle($len = 40){
        return mb_strlen($this->title) > $len ? (mb_substr($this->title, 0, $len - 3) . "...") : $this->title;
    }

    /**
     * Получить домен продавца
     *
     * @return null|string
     */
    public function getLinkDomain(){
        $url = parse_url($this->link);
        return isset($url['host']) ? $url['host'] : null;
    }

    /**************************************************************************************/

    /**
     * Обновление кеша товаров в наличие
     */
    public function updateCacheInStock(){
        $is = (int)(bool)Db::site()->fetchOne(
            "select `id` from `catalog_remains` where `status`=? and `count`>0 and `item`=? limit 1",
            Catalog_RemainsStatus::IN_STOCK,
            $this->id
        );

        if($is != $this->stock){
            $this->stock = $is;
            $this->update("stock");
        }
    }

    /**
     * Обновление кеша товаров в наличие
     */
    public function updateCacheInRemainsNotSale(){
        $cnt = (int)Db::site()->fetchOne(
            "select sum(`count`) from `catalog_remains` where `status`!=? and `count`>0 and `item`=? limit 1",
            Catalog_RemainsStatus::SOLD,
            $this->id
        );

        if($cnt != $this->remains_not_sale){
            $this->remains_not_sale = $cnt;
            $this->update("remains_not_sale");
        }
    }

    public function renderCard(){
        $tbl = new View_Table_Simple();

        $tbl->add("tbl_long_id", "<span>{$this->long_id}</span>");
        $tbl->add(
            "tbl_price_purchase",
            "<span>" . number_format($this->price_purchase + 0) . "</span> " .
            "<small class='txt-color-blueLight'>{$this->price_purchase_currency}</small>"
        );

        if($this->link) {
            $url = parse_url($this->link);

            $tbl->add(
                "tbl_link",
                "<a class='txt-color-greenDark' target='blank' href='{$this->link}'>" .
                (isset($url['host']) ? $url['host'] : substr($this->link, 0, 32) . "...") .
                "</a>"
            );
        }

        if(count(Catalog_Options::getTypes())) {
            foreach(Catalog_Options::getTypes() as $el) {
                $str = Catalog_Options::renderString($this->id, $el);

                if(strlen($str)){
                    $tbl->add(
                        Catalog_Options::getTypeName($el),
                        $str
                    );
                }
            }
        }

        $options = Catalog_Options::getOptions($this->id);

        if(count($options)){
            foreach($options as $el){

            }
        }

        return "<h4><a href='/account/catalog/item/?id={$this->long_id}'>" .
            String_Filter::cutByWord($this->title, 50) .
            "</a></h4>" . (
            strlen($this->renderFlags()) ?
                "<div style='margin: 5px 0 0 0'>" . $this->renderFlags() . "</div>" :
                ""
            ) . "<div style='float:right; font-size:90%; color:#777; '>" .
            Cache_PartialList_UserName::get($this->user) .
            "&nbsp; <small class='txt-color-blueLight'>" .
            (new Decorator_Date())
                ->showTime(false)
                ->showCurrentYear(true)
                ->render($this->create) .
            "</small>"
            . "</div><br>" . $tbl->render();
    }

    public function renderFlags(){
        $flags = [];

        if($this->to_order){
            $flags[] = "<a href='/account/catalog/item/?id={$this->long_id}'><i class='label label-info'>" . Translate::t("flag_order") . "</i></a>";
        }

        if($this->stock){
            $flags[] = "<a href='/account/catalog/item/remains?id={$this->long_id}'><i class='label label-success'>" . Translate::t("flag_stock") . "</i></a>";
        }
        else if($this->remains_not_sale){
            $flags[] = "<a href='/account/catalog/item/remains?id={$this->long_id}'><i class='label label-default'>" . Translate::t("flag_remains_not_sale") . "</i></a>";
        }

        return count($flags) ? implode(" ", $flags) : null;
    }

    public function renderImage(){
        $item = Catalog_Item::getObject($this->id);

        $imageLink = $item->getImageLink();

        if($imageLink){
            return "<a href='/account/catalog/photos?id={$this->long_id}'><img style='max-width: 100%' src='{$imageLink}'></a>";
        }
        else{
            return "<br><center><h4>" . Translate::t("no_image") . "</h4></center><br>";
        }
    }

    public function getVkPosts(){
        $photos = Db::site()->fetchCol("select id from catalog_photos where item=?", $this->id);
        $addWhere = count($photos) ? " or photo in (" . implode(", ", $photos) . ")" : "";

        return Db::site()->fetchAll("select * from vk_photos where item=? {$addWhere}", $this->id);
    }
}