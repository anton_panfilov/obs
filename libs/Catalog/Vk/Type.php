<?php



class Catalog_Vk_Type extends Ui_Set {
    const TO_ORDER = 1; // на заказ
    const IS_STOCK = 2; // в наличии

    public function __construct(){
        parent::__construct('catalog/vk_sync_status');

        $this->addValue_White(self::TO_ORDER, 'vk_sync_status_to_order');
        $this->addValue_White(self::IS_STOCK, 'vk_sync_status_in_stock');
    }
}