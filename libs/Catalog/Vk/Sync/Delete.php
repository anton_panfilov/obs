<?php

class Catalog_Vk_Sync_Delete extends Catalog_Vk_Sync_Abstract {
    protected $vk_photo;

    public function __construct($vk_photo, $owner_id, $album_id){
        $this->vk_photo = $vk_photo;
        $this->owner_id = $owner_id;
        $this->album_id = $album_id;
    }

    public function run(){
        $res = (new Vk_AUploader())->api('photos.delete', [
            'owner_id' => $this->owner_id,
            'photo_id' => $this->vk_photo,
        ]);

        if(isset($res['response']) && $res['response'] == 1){
            Db::site()->delete(
                "vk_photos",
                "vk_owner=? and vk_photo=?",
                $this->owner_id, $this->vk_photo
            );

            return true;
        }

        return false;
    }
}