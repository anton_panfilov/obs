<?php

class Catalog_Vk_Sync_Insert extends Catalog_Vk_Sync_Abstract {
    protected $photoID;
    protected $caption;
    protected $item = 0;
    protected $itemType = 0;

    public function __construct($photoID, $caption, $owner_id, $album_id, $item = 0, $itemType = 0){
        $this->photoID  = $photoID;
        $this->caption  = $caption;
        $this->owner_id = $owner_id;
        $this->album_id = $album_id;
        $this->item     = $item;
        $this->itemType = $itemType;
    }

    public function run(){
        if(Catalog_Photo::getObject($this->photoID)->id){
            $path = Catalog_Photo::getObject($this->photoID)->getPath(800);
            if(!is_file($path)){
                $path = Catalog_Photo::getObject($this->photoID)->getPath(400);
            }

            if(is_file($path)){
                $obj = new Vk_AUploader();

                $r = [
                    'album_id' => $this->album_id,
                ];

                if($this->owner_id < 0){
                    $r['group_id'] = -$this->owner_id;
                }

                $r1 = $obj->api('photos.getUploadServer', $r);


                if(isset($r1['response']['upload_url'])){
                    $temp = $obj->uploadFile(
                        $r1['response']['upload_url'],
                        $path
                    );

                    if(isset($temp['server'], $temp['photos_list'], $temp['hash'])){
                        $r = [
                            'album_id'      => $this->album_id,
                            'server'        => $temp['server'],
                            'photos_list'   => $temp['photos_list'],
                            'hash'          => $temp['hash'],
                            'caption'       => $this->caption,
                        ];

                        if($this->owner_id < 0){
                            $r['group_id'] = -$this->owner_id;
                        }

                        $res = $obj->api('photos.save', $r);

                        if(isset($res['response']['0']['id'])){
                            $vk_photo = $res['response']['0']['id'];

                            if($vk_photo){
                                Db::site()->insert("vk_photos", [
                                    'photo'     => Catalog_Photo::getObject($this->photoID)->id,
                                    'item_type' => (int)$this->itemType,
                                    'item'      => (int)$this->item,
                                    'vk_owner'  => $this->owner_id,
                                    'vk_album'  => $this->album_id,
                                    'vk_photo'  => $vk_photo,
                                    'caption'   => $this->caption,
                                ]);
                            }

                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}