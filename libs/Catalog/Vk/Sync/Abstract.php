<?php

abstract class Catalog_Vk_Sync_Abstract {
    protected $owner_id;
    protected $album_id;

    /**
     * Провести запись
     *
     * @return bool
     */
    abstract public function run();

    public function getKey(){
        return $this->owner_id . "_" . $this->album_id;
    }
}