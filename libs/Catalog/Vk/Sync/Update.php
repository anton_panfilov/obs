<?php

class Catalog_Vk_Sync_Update extends Catalog_Vk_Sync_Abstract {
    protected $vk_photo;
    protected $caption;

    public function __construct($vk_photo, $caption, $owner_id, $album_id){
        $this->vk_photo = $vk_photo;
        $this->caption  = $caption;
        $this->owner_id = $owner_id;
        $this->album_id = $album_id;
    }

    public function run(){
        $res = (new Vk_AUploader())->api('photos.edit', [
            'owner_id' => $this->owner_id,
            "photo_id" => $this->vk_photo,
            "caption"  => $this->caption,
            // "captcha_sid" => "461001332677",
            // "captcha_key" => "hhqv",
        ]);

        if(isset($res['response']) && $res['response'] == 1){
            Db::site()->update(
                "vk_photos",
                [
                    "caption" => $this->caption,
                ],
                "vk_owner=? and vk_photo=?",
                $this->owner_id, $this->vk_photo
            );

            return true;
        }
        // die;

        return false;
    }
}