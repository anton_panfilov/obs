<?php


class Catalog_Vk_Sync {

    /******************************************************************************************************************/

    /**
     * В наличии - Обувь
     *
     * @return array
     */
    static public function in_stock_shoes(){
        return self::in_stock_abstract(
            [7],  // типы товаров - одежда и обувь
            [3, 5],     // параметры, информацию о которых выводить в комментарии - возраст и цвет
            [],         // фильтры по опциям элемента каталога - не заданны

            // фильтры по опциям физического товара
            [],

            // альбомы
            [
                224130334 => -46466262,
                224385691 => 17871826,
            ]
        );
    }

    /**
     * В наличии - Малыши до 2-х лет
     *
     * @return array
     */
    static public function in_stock_kids_up_to_2_years(){
        return self::in_stock_abstract(
            [1000],  // типы товаров - одежда
            [3, 5],     // параметры, информацию о которых выводить в комментарии - возраст и цвет
            [],         // фильтры по опциям элемента каталога - не заданны

            // фильтры по опциям физического товара
            [
                3 => [100, 103, 106, 109, 112, 118] // до 2-х лет
            ],

            // альбомы
            [
                220646541 => -46466262,
                220797282 => 17871826,
            ]
        );
    }

    /**
     * В наличии - Мальчики от 2-х лет
     *
     * @return array
     */
    static public function in_stock_boys(){
        return self::in_stock_abstract(
            [1000], // типы товаров - одежда
            [3, 5], // параметры, информацию о которых выводить в комментарии - возраст и цвет

            // фильтры по опциям элемента каталога
            [
                2 => 1, // мальчики
            ],

            // фильтры по опциям физического товара
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            // альбомы
            [
                220646558 => -46466262,
                220797336 => 17871826,
            ]
        );
    }

    /**
     * В наличии - Девочки от 2-х лет
     *
     * @return array
     */
    static public function in_stock_girls(){
        return self::in_stock_abstract(
            [1000], // типы товаров - одежда
            [3, 5], // параметры, информацию о которых выводить в комментарии - возраст и цвет

            // фильтры по опциям элемента каталога
            [
                2 => 2, // девочки
            ],

            // фильтры по опциям физического товара
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            // альбомы
            [
                220646573 => -46466262,
                220797349 => 17871826,
            ]
        );
    }

    //////////////////////////////////////////

    /**
     * Вся одежда и обувь в наличии
     *
     * @return array
     */

    static public function order_children_clothes_and_shoes(){
        return self::order_abstract(
            [1000, 7],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 5, [6, ['219']]],

            // не фильтровать по опциям
            [],

            [
                221839906 => 17871826,
            ]
        );
    }


    /*
    Обувь для малышей до 2 лет      -46466262_221710455 17871826_222909295
    Одежда для малышей до 2 лет     -46466262_221708828 17871826_222909322
    Обувь для детей от 2 лет        -46466262_221710545 17871826_222909279
    Платья                          -46466262_221708577 17871826_222909345
    Пижамы и постельное белье       -46466262_221708525 17871826_222909359
    Комплекты                       -46466262_221708370 17871826_222909373
    Толстовки, рубашки, футболки    -46466262_221708279 17871826_222909398
    Джинсы и штаны                  -46466262_221708191 17871826_222909418
    Верхняя одежда, шапки, шарфы    -46466262_221708098 17871826_222909436
    */


    /*
    Обувь для малышей до 2 лет      -46466262_221710455 17871826_222909295
    Обувь для детей от 2 лет        -46466262_221710545 17871826_222909279
    */

    /**
     * Одежда для малышей до 2 лет
     *
     * @return array
     */
    static public function order_baby_dress(){
        return self::order_abstract(
            [1000],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 4, 5, [6, ['219']]],

            // фильтры по опциям
            [
                3 => [100, 103, 106, 109, 112, 118] // до 2-х лет
            ],

            [
                221708828 => -46466262,
                222909322 => 17871826,
            ]
        );
    }

    /**
     * Платья
     *
     * @return array
     */
    static public function order_clothes(){
        return self::order_abstract(
            [1],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 4, 5, [6, ['219']]],

            // фильтры по опциям
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            [
                221708577 => -46466262,
                222909345 => 17871826,
            ]
        );
    }

    /**
     * Пижамы и постельное белье
     *
     * @return array
     */
    static public function order_pyjamas(){
        return self::order_abstract(
            [8],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 4, 5, [6, ['219']]],

            // фильтры по опциям
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            [
                221708525 => -46466262,
                222909359 => 17871826,
            ]
        );
    }

    /**
     * Пижамы и постельное белье
     *
     * @return array
     */
    static public function order_postelnoe(){
        return self::order_abstract(
            [11],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [],

            // фильтры по опциям
            [],

            [
                223950279 => -46466262,
                224199816 => 17871826,
            ]
        );
    }

    /**
     * Комплекты
     *
     * @return array
     */
    static public function order_kits(){
        return self::order_abstract(
            [6],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 4, 5, [6, ['219']]],

            // фильтры по опциям
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            [
                221708370 => -46466262,
                222909373 => 17871826,
            ]
        );
    }

    /**
     * Джинсы и штаны
     *
     * @return array
     */
    static public function order_pants(){
        return self::order_abstract(
            [5],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 4, 5, [6, ['219']]],

            // фильтры по опциям
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            [
                221708191 => -46466262,
                222909418 => 17871826,
            ]
        );
    }

    /**
     * Толстовки, рубашки, футболки
     *
     * @return array
     */
    static public function order_shirts(){
        return self::order_abstract(
            [2, 3, 4],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 4, 5, [6, ['219']]],

            // фильтры по опциям
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            [
                221708279 => -46466262,
                222909398 => 17871826,
            ]
        );
    }

    /**
     * Верхняя одежда, шапки, шарфы
     *
     * @return array
     */
    static public function order_winter(){
        return self::order_abstract(
            [10, 901, 902],

            // параметры, информацию о которых выводить в комментарии - возраст и цвет
            // а производителья показывать только если это США
            [3, 4, 5, [6, ['219']]],

            // фильтры по опциям
            [
                3 => [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214] // от 2-х лет
            ],

            [
                221708098 => -46466262,
                222909436 => 17871826,
            ]
        );
    }

    /******************************************************************************************************************/

    static protected function in_stock_abstract($types, $remainOptions, $whereItemOptions, $whereRemainsOptions, $albums){
        $types = Catalog_Types::getAllRelationsTypes($types);

        $inStock = [];

        $addJoin = "";
        $addWhere = "";

        $whereAddN = 0;

        if(count($whereItemOptions)){
            foreach($whereItemOptions as $k => $v){
                $whereAddN++;
                $tableAlias = "aliasOptions{$whereAddN}";

                $k = (int)$k;

                if(!is_array($v)){
                    $v = [$v];
                }

                foreach($v as $vk => $vv){
                    $v[$vk] = (int)$vv;
                }

                if($k > 0 && count($v)){
                    $addJoin.= "\r\n" . "LEFT JOIN `catalog_options` as `{$tableAlias}` on `catalog_remains`.`item` = `{$tableAlias}`.`item`";
                    $addWhere.= "\r\n" . "AND `{$tableAlias}`.`option` = {$k}";
                    $addWhere.= "\r\n" . "AND `{$tableAlias}`.`value` in (" .  implode(",", $v) . ")";
                }
            }
        }

        if(count($whereRemainsOptions)){
            foreach($whereRemainsOptions as $k => $v){
                $whereAddN++;
                $tableAlias = "aliasOptions{$whereAddN}";

                $k = (int)$k;

                if(!is_array($v)){
                    $v = [$v];
                }

                foreach($v as $vk => $vv){
                    $v[$vk] = (int)$vv;
                }

                if($k > 0 && count($v)){
                    $addJoin.= "\r\n" . "LEFT JOIN `catalog_remains_options` as `{$tableAlias}` on `catalog_remains`.`id` = `{$tableAlias}`.`remains`";
                    $addWhere.= "\r\n" . "AND `{$tableAlias}`.`option` = {$k}";
                    $addWhere.= "\r\n" . "AND `{$tableAlias}`.`value` in (" .  implode(",", $v) . ")";
                }
            }
        }

        $temp = Db::site()->fetchUnique(
            "select" . "
                DISTINCT
                catalog_remains.id,
                catalog_photos.id as photo,
                catalog_items.id as item
            from
                catalog_remains
                LEFT JOIN catalog_items on catalog_remains.item = catalog_items.id
                LEFT JOIN catalog_photos on catalog_remains.photo = catalog_photos.id
                {$addJoin}
            where
                catalog_remains.`status` = 30
                AND catalog_remains.`order` = 0
                AND catalog_remains.`photo` > 0
                AND catalog_items.`type` in (" . implode(",", $types) . ")
                {$addWhere}
            "
        );

        if(count($temp)){
            // составление сгрупированного по фотографии списка
            foreach($temp as $remains => $tempEl){
                $photo = $tempEl['photo'];
                $item = $tempEl['item'];

                if(!isset($inStock[$photo])){
                    $inStock[$photo] = [
                        'item' => $item,
                        'remains' => [
                            $remains
                        ],
                        'caption' => '',
                    ];
                }
                else {
                    $inStock[$photo]['remains'][] = $remains;
                }
            }

            // составление комментария
            foreach($inStock as $photo => $data){
                $captions = [];

                foreach($data['remains'] as $remainsID){
                    $captionByOneRemains = [];

                    // добавление опций
                    foreach($remainOptions as $remainOption){
                        $optionValue = Catalog_Options::getRemainsOption($remainsID, $remainOption);

                        if(!is_null($optionValue)){
                            $captionByOneRemains[] =
                                Catalog_Options::getTypeName($remainOption) . ": " .
                                Catalog_Options::getTypeValueLabel($remainOption, $optionValue);
                        }
                    }

                    // добавление комментария
                    if(mb_strlen(Catalog_Remains::getObject($remainsID)->comment)){
                        $captionByOneRemains[] = Catalog_Remains::getObject($remainsID)->comment;
                    }

                    // добавление цены
                    if(Catalog_Remains::getObject($remainsID)->price){
                        $captionByOneRemains[] = "Цена: " . String_Filter::price(Catalog_Remains::getObject($remainsID)->price) . " руб.";
                    }

                    if(count($captionByOneRemains)){
                        $captions[] = implode("\r\n", $captionByOneRemains);
                    }
                }

                if(count($captions)){
                    $inStock[$photo]['caption'] =
                        "В НАЛИЧИИ!\r\n\r\n----------------------------------------\r\n\r\n" .
                        implode("\r\n\r\n-----------------\r\n\r\n", $captions);
                }
            }
        }

        $result = [
            'insert' => [],
            'update' => [],
            'delete' => [],
        ];

        if(count($albums)){
            foreach($albums as $albumID => $albumOwner){
                $photos = Db::site()->fetchUnique(
                    "select photo, vk_photo, caption from vk_photos where vk_owner=? and vk_album=?",
                    $albumOwner, $albumID
                );

                if(count($inStock)){
                    foreach($inStock as $photo => $data){
                        $caption = $data['caption'];

                        if(isset($photos[$photo])){
                            if($caption != $photos[$photo]['caption']){
                                // update
                                $result['update'][] = new Catalog_Vk_Sync_Update(
                                    $photos[$photo]['vk_photo'],
                                    $caption,
                                    $albumOwner,
                                    $albumID
                                );
                            }
                        }
                        else {
                            // insert
                            $result['insert'][] = new Catalog_Vk_Sync_Insert(
                                $photo,
                                $caption,
                                $albumOwner,
                                $albumID,
                                $data['item'],
                                Catalog_Vk_Type::IS_STOCK
                            );
                        }
                    }
                }

                if(count($photos)){
                    foreach($photos as $item => $el){
                        if(!isset($inStock[$item])){
                            // delete
                            $result['delete'][] = new Catalog_Vk_Sync_Delete(
                                $el['vk_photo'],
                                $albumOwner,
                                $albumID
                            );
                        }
                    }
                }
            }
        }

        return $result;
    }


    /******************************************************************************************************************/

    /**
     * @param $types
     * @param $options
     * @param $whereItemOptions
     * @param $albums
     *
     * @return array
     */
    static protected function order_abstract($types, $options, $whereItemOptions, $albums){
        $types = Catalog_Types::getAllRelationsTypes($types);

        $toOrder = [];

        $addJoin = "";
        $addWhere = "";

        $whereAddN = 0;

        if(count($whereItemOptions)){
            foreach($whereItemOptions as $k => $v){
                $whereAddN++;
                $tableAlias = "aliasOptions{$whereAddN}";

                $k = (int)$k;

                if(!is_array($v)){
                    $v = [$v];
                }

                foreach($v as $vk => $vv){
                    $v[$vk] = (int)$vv;
                }

                if($k > 0 && count($v)){
                    $addJoin.= "\r\n" . "LEFT JOIN `catalog_options` as `{$tableAlias}` on `catalog_items`.`id` = `{$tableAlias}`.`item`";
                    $addWhere.= "\r\n" . "AND `{$tableAlias}`.`option` = {$k}";
                    $addWhere.= "\r\n" . "AND `{$tableAlias}`.`value` in (" .  implode(",", $v) . ")";
                }
            }
        }


        $temp = Db::site()->fetchPairs(
            "select" . "
                catalog_items.id,
                catalog_photos.id as photo
            from
                catalog_items
                LEFT JOIN catalog_photos on catalog_items.main_photo = catalog_photos.id
                {$addJoin}
            where
                catalog_items.to_order = 1
                AND catalog_photos.id > 0
                AND catalog_items.`type` in (" . implode(",", $types) . ")
                {$addWhere}

            "
        );

        if(count($temp)){
            // составление сгрупированного по фотографии списка
            foreach($temp as $item => $photo){
                $captions = [];

                // добавление опций
                foreach($options as $option){
                    $filter = null;

                    if(is_array($option)){
                        $filter = $option[1];
                        $option = $option[0];
                    }

                    $optionValue = Catalog_Options::getOptions($item, $option);

                    if(is_array($optionValue) && count($optionValue)){
                        $valueTemp = [];
                        foreach($optionValue as $ovt){
                            if(is_null($filter) || in_array($ovt, $filter)){
                                $valueTemp[] = Catalog_Options::getTypeValueLabel($option, $ovt);
                            }
                        }

                        if(count($valueTemp) > 0){
                            if(count($valueTemp) > 3 || (count($valueTemp) > 1 && $option == 3)){
                                $captions[] =
                                    Catalog_Options::getTypeName($option) . ":\r\n  " .
                                    implode(";\r\n  ", $valueTemp) . ".\r\n";
                            }
                            else {
                                $captions[] =
                                    Catalog_Options::getTypeName($option) . ": " .
                                    implode(", ", $valueTemp) . "\r\n";
                            }
                        }
                    }
                }


                // добавление комментария

                if(mb_strlen(Catalog_Item::getObject($item)->description)){
                    $captionByOneRemains[] = Catalog_Item::getObject($item)->description;
                }


                // добавление цены
                if(Catalog_Item::getObject($item)->price){
                    $captions[] = "Цена: " . String_Filter::price(Catalog_Item::getObject($item)->price) . " руб.";
                }

                $toOrder[$photo] = [
                    'item' => $item,
                    'caption' =>
                        "НА ЗАКАЗ\r\nОплата при получении!\r\n\r\n----------------------------------------\r\n\r\n" .
                        (
                            count($captions) ?
                            implode("\r\n", $captions) :
                            ""
                        )
                ];
            }
        }

        $result = [
            'insert' => [],
            'update' => [],
            'delete' => [],
        ];

        if(count($albums)){
            foreach($albums as $albumID => $albumOwner){
                $photos = Db::site()->fetchUnique(
                    "select photo, vk_photo, caption from vk_photos where vk_owner=? and vk_album=?",
                    $albumOwner, $albumID
                );

                if(count($toOrder)){
                    foreach($toOrder as $photo => $data){
                        $caption = $data['caption'];

                        if(isset($photos[$photo])){
                            if($caption != $photos[$photo]['caption']){
                                // update
                                $result['update'][] = new Catalog_Vk_Sync_Update(
                                    $photos[$photo]['vk_photo'],
                                    $caption,
                                    $albumOwner,
                                    $albumID
                                );
                            }
                        }
                        else {
                            // insert
                            $result['insert'][] = new Catalog_Vk_Sync_Insert(
                                $photo,
                                $caption,
                                $albumOwner,
                                $albumID,
                                $data['item'],
                                Catalog_Vk_Type::TO_ORDER
                            );
                        }
                    }
                }

                if(count($photos)){
                    foreach($photos as $item => $el){
                        if(!isset($toOrder[$item])){
                            // delete
                            $result['delete'][] = new Catalog_Vk_Sync_Delete(
                                $el['vk_photo'],
                                $albumOwner,
                                $albumID
                            );
                        }
                    }
                }
            }
        }

        return $result;
    }

    /******************************************************************************************************************/

    static public function run($limit = 1, $methods = null){
        if(!is_array($methods)){
            $methods = [
                'in_stock_shoes',
                "in_stock_kids_up_to_2_years",
                "in_stock_boys",
                "in_stock_girls",

                "order_children_clothes_and_shoes",

                "order_baby_dress",
                "order_clothes",
                "order_pyjamas",
                "order_kits",
                "order_pants",
                "order_shirts",
                "order_winter",

                "order_postelnoe",

            ];
        }

        $sum = [];
        $types = ['insert', 'delete', 'update'];

        foreach($types as $type){
            $sum[$type] = [];
        }

        foreach($methods as $method){
            $result = Catalog_Vk_Sync::$method();

            foreach($types as $type){
                if(isset($result[$type]) && is_array($result[$type]) && count($result[$type])){
                    foreach($result[$type] as $el){
                        if($el instanceof Catalog_Vk_Sync_Abstract){
                            $sum[$type][] = $el;
                        }
                    }
                }
            }
        }

        return self::sync($sum['insert'], $sum['delete'], $sum['update'], $limit);
    }

    /**
     * Синхронизировать записи
     *
     * @param array $insert
     * @param array $delete
     * @param array $update
     * @param int   $limit
     *
     * @return array
     */
    static protected function sync($insert = [], $delete = [], $update = [], $limit = 1){
        $limit = max(1, (int)$limit);

        $run    = 0;
        $total  = 0;
        $finish = 0;
        $counts = [];

        $countTemplate = [
            'insert' => [
                'total'  => 0,
                'finish' => 0,
            ],
            'delete' => [
                'total'  => 0,
                'finish' => 0,
            ],
            'update' => [
                'total'  => 0,
                'finish' => 0,
            ],
        ];

        if(count($insert)){
            foreach($insert as $el){
                if($el instanceof Catalog_Vk_Sync_Insert){
                    if(!isset($counts[$el->getKey()])){
                        $counts[$el->getKey()] = $countTemplate;
                    }

                    $counts[$el->getKey()]['insert']['total']++;
                    $total++;

                    if($run < $limit){
                        $run++;
                        if($el->run()){
                            $counts[$el->getKey()]['insert']['finish']++;
                            $finish++;
                        }
                    }
                }
            }
        }

        if(count($delete)){
            foreach($delete as $el){
                if($el instanceof Catalog_Vk_Sync_Delete){
                    if(!isset($counts[$el->getKey()])){
                        $counts[$el->getKey()] = $countTemplate;
                    }

                    $counts[$el->getKey()]['delete']['total']++;
                    $total++;

                    if($run < $limit){
                        $run++;
                        if($el->run()){
                            $counts[$el->getKey()]['delete']['finish']++;
                            $finish++;
                        }
                    }
                }
            }
        }

        if(count($update)){
            foreach($update as $el){
                if($el instanceof Catalog_Vk_Sync_Update){
                    if(!isset($counts[$el->getKey()])){
                        $counts[$el->getKey()] = $countTemplate;
                    }

                    $counts[$el->getKey()]['update']['total']++;
                    $total++;

                    if($run < $limit){
                        $run++;
                        if($el->run()){
                            $counts[$el->getKey()]['update']['finish']++;
                            $finish++;
                        }
                    }
                }
            }
        }


        return [
            'limit'  => $limit,
            'run'    => $run,
            'total'  => $total,
            'finish' => $finish,
            'counts' => $counts
        ];
    }
}