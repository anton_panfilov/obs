<?php

class Catalog_Types {
    const TYPE_UNKNOWN_ID = 0;

    static public function translate($key){
        return Translate::t($key, "catalog/types");
    }

    static protected function getTypes(){
        return [
            '1000' => [
                'name'           => 'kids_dress',  // детская одежда
                'full_params'    => [
                    Catalog_Options::GENDER,
                    Catalog_Options::COLOR,
                    Catalog_Options::AGE,
                    Catalog_Options::MATERIAL,
                    Catalog_Options::COUNTRY,
                    Catalog_Options::TAG,
                ],
                'remains_params' => [Catalog_Options::COLOR, Catalog_Options::AGE],
                'sub' => [
                    '1' => [
                        "name"          => "clothes",  // Платье
                        "fix_options"    => [
                            Catalog_Options::GENDER => Catalog_Option_2::FEMALE,
                        ]
                    ],
                    '2' => "jacket",     // Толстовки и свитера
                    '3' => "shirt",      // Рубашка
                    '4' => "t_shirt",    // Футболка
                    '5' => [
                        "name" => "pants", // Штаны
                        "sub"  => [
                            '501' => 'jeans',               // джинсы
                            '502' => 'sports_trousers',     // спортивные штаны
                            '503' => 'leggings_and_tights', // лосины и колготки
                        ],
                    ],
                    '6' => [
                        "name" => "dress_kits", // Комплекты
                        "sub"  => [
                            '601' => 'dress_family_kits', // семейные комплекты
                        ],
                    ],
                    '8' => "pyjamas",           // Пижамы
                    '9' => [
                        "name" => "asccessories",      // Аксессуары
                        "sub" => [
                            "901" => "asccessories_shapki",     // шарфы и шапки
                            "902" => "asccessories_perchatki",  // перчатки
                            "903" => "asccessories_remni",      // ремни и подтяжки
                        ],
                    ],
                    '10' => [
                        "name" => "outerwear",  // Верхняя одежда
                        "sub" => [
                            "101" => "outerwear_jacket", // куртки
                            "102" => "outerwear_coat",   // пальто
                            "103" => "outerwear_vests",  // жилетки
                        ],
                    ]
                ]
            ],
            '7' => [ // детская обувь
                'name'           => 'kids_shoes',
                'full_params'    => [
                    Catalog_Options::GENDER,
                    Catalog_Options::COLOR,
                    Catalog_Options::COUNTRY,
                    Catalog_Options::TAG,
                ],
                'remains_params' => [Catalog_Options::COLOR],
            ],
            '11' => [ // постельное белье
                'name'           => 'bed_linen',
                'full_params'    => [
                    Catalog_Options::COLOR,
                    Catalog_Options::COUNTRY,
                    Catalog_Options::TAG,
                ],
                'remains_params' => [Catalog_Options::COLOR],
            ]
        ];
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static protected function searchTypeInfo_getName($info, $not_name = 'not_name'){
        return self::translate(
            is_string($info) ?
                (strlen($info) ? $info : $not_name) :
                (isset($info['name']) && is_string($info['name']) && strlen($info['name']) ? $info['name'] : $not_name)
        );
    }

    static protected function searchTypeInfo_mergeEnv($env, $info, $currentID = null){
        $merges = [
            "full_params",
            "remains_params"
        ];
        foreach($merges as $merge){
            if(isset($info[$merge]) && is_array($info[$merge]) && count($info[$merge])){
                foreach($info[$merge] as $el){
                    if(!in_array($el, $env[$merge])){
                        $env[$merge][] = $el;
                    }
                }
            }
        }

        if(isset($info['fix_options']) && is_array($info['fix_options']) && count($info['fix_options'])){
            foreach($info['fix_options'] as $elK => $ekV){
                if(!is_null($ekV)){
                    $env['fix_options'][$elK] = $ekV;
                }
                else {
                    unset($env['fix_options'][$elK]);
                }
            }
        }

        // вычисление full_params с учетом fix_options
        $env['full_params_real'] = $env['full_params'];
        if(count($env['fix_options'])){
            foreach($env['fix_options'] as $k => $v){
                $searchID = array_search($k, $env['full_params_real']);
                if($searchID !== false){
                    unset($env['full_params_real'][$searchID]);
                }
            }
            $env['full_params_real'] = array_values($env['full_params_real']);
        }

        // если передан текущий id то он будет добавлен в parents
        if($currentID){
            $env['parents'][$currentID] = self::searchTypeInfo_getName($info);
        }

        return $env;
    }

    static public function getTypeInfo($id, $exception = true){
        if(isset(self::getTypesSimpleList()[$id])){
            return self::getTypesSimpleList()[$id];
        }

        if($exception){
            throw new Exception("Type not found");
        }

        return [
            'name'              => self::translate("unknown"),
            'full_name'         => self::translate("unknown"),
            'parents'           => [],
            'full_params'       => [],
            'full_params_real'  => [],
            'remains_params'    => [],
            'fix_options'       => [],
        ];
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static protected function getTypesSimpleList_addTree($tree, $env = [
        'parents'           => [],
        'full_params'       => [],
        'full_params_real'  => [],
        'remains_params'    => [],
        'fix_options'       => [],
    ]){
        $result = [];

        if(is_array($tree) && count($tree)){
            foreach($tree as $id => $info){
                $newEnv = self::searchTypeInfo_mergeEnv($env, $info);

                $result[$id] = [
                    'name'              => self::searchTypeInfo_getName($info),
                    'full_name'         => self::getFullName($newEnv['parents'], self::searchTypeInfo_getName($info)),
                    'parents'           => $newEnv['parents'],
                    'full_params'       => $newEnv['full_params'],
                    'full_params_real'  => $newEnv['full_params_real'],
                    'remains_params'    => $newEnv['remains_params'],
                    'fix_options'       => $newEnv['fix_options'],
                ];


                if(is_array($info) && isset($info['sub']) && is_array($info['sub']) && count($info['sub'])){
                    $add = self::getTypesSimpleList_addTree($info['sub'], self::searchTypeInfo_mergeEnv($env, $info, $id));
                    if(is_array($add) && count($add)){
                        foreach($add as $k => $v){
                            $result[$k] = $v;
                        }
                    }
                }
            }
        }

        return $result;
    }

    static protected $cache_simple_list;

    static public function getTypesSimpleList(){
        if(!isset(self::$cache_simple_list)){
            self::$cache_simple_list = self::getTypesSimpleList_addTree(self::getTypes());
        }
        return self::$cache_simple_list;
    }

    //////////////////////////////////////////////////////////////////////

    /*
     * Вариант с предсимволами
     *
    static protected function getTypesListForSelect_addTree($tree, $pre = 0){
        $result = [];

        if(is_array($tree) && count($tree)){
            foreach($tree as $id => $info){
                $result[$id] = ($pre ? str_repeat("_", $pre) . " " : "") . self::searchTypeInfo_getName($info);


                if(is_array($info) && isset($info['sub']) && is_array($info['sub']) && count($info['sub'])){
                    $add = self::getTypesListForSelect_addTree($info['sub'], $pre+1);
                    if(is_array($add) && count($add)){
                        foreach($add as $k => $v){
                            $result[$k] = $v;
                        }
                    }
                }
            }
        }

        return $result;
    }
    */

    /**
     * Поулчить полное имя в формате "parent1 → parent1 → name"
     *
     * @param $parents
     * @param $name
     *
     * @return string
     */
    static protected function getFullName($parents, $name){
        return (count($parents) ? implode(" → ", $parents) . " → " : "") . $name;
    }

    static protected function getTypesListForSelect_addTree($tree, $parents = []){
        $result = [];

        if(is_array($tree) && count($tree)){
            foreach($tree as $id => $info){
                $result[$id] = self::getFullName($parents, self::searchTypeInfo_getName($info));

                if(is_array($info) && isset($info['sub']) && is_array($info['sub']) && count($info['sub'])){
                    $add = self::getTypesListForSelect_addTree($info['sub'], array_merge($parents, [self::searchTypeInfo_getName($info)]));
                    if(is_array($add) && count($add)){
                        foreach($add as $k => $v){
                            $result[$k] = $v;
                        }
                    }
                }
            }
        }

        return $result;
    }

    static protected $cache_list_for_select;

    /**
     * Получить список типов для select box
     *
     * @return array
     */
    static public function getTypesListForSelect(){
        if(!isset(self::$cache_list_for_select)){
            self::$cache_list_for_select = self::getTypesListForSelect_addTree(self::getTypes());
        }
        return self::$cache_list_for_select;
    }

    /////////////////////////////////////////////////////////////////////

    static protected function getOptionsLists_read($tree){
        $result = [
            'full_params'    => [],
            'remains_params' => [],
        ];

        if(is_array($tree) && count($tree)){
            foreach($tree as $id => $info){
                foreach(array_keys($result) as $key){
                    if(isset($info[$key]) && is_array($info[$key]) && count($info[$key])){
                        $result[$key] = array_unique(array_merge($result[$key], $info[$key]));
                    }
                }

                if(is_array($info) && isset($info['sub']) && is_array($info['sub']) && count($info['sub'])){
                    $info = self::getOptionsLists_read($info['sub']);
                    foreach(array_keys($result) as $key){
                        if(isset($info[$key]) && is_array($info[$key]) && count($info[$key])){
                            $result[$key] = array_unique(array_merge($result[$key], $info[$key]));
                        }
                    }
                }
            }
        }

        return $result;
    }

    static protected $cache_options;

    static public  function getOptionsLists(){
        if(is_null(self::$cache_options)){
            self::$cache_options = self::getOptionsLists_read(self::getTypes());
        }

        return self::$cache_options;
    }

    static public function getFullParams(){
        return self::getOptionsLists()['full_params'];
    }


    /**
     * Получить список conditions для формы, когда под типом выбираються характерисики
     *
     * @return array
     */
    static public function getAllTypesForConditions(){
        $all = self::getTypesSimpleList();

        foreach($all as $k => $v){
            sort($all[$k]['full_params_real']);
            $all[$k] = $all[$k]['full_params_real'];
        }

        $res = [];
        foreach($all as $k => $v){
            $key = Json::encode($v);
            if(!isset($res[$key])){
                $res[$key] = [
                    'ids' => [$k],
                    'options' => $v,
                ];
            }
            else {
                $res[$key]['ids'][] = $k;
            }
        }

        return array_values($res);
    }

    /////////////////////////////////////////////////////////////////////////////////////

    static protected $cache_allRelationsTypes = [];

    /**
     * Получить тип м все подтипы
     *
     * @param $type
     *
     * @return array
     */
    static public function getAllRelationsTypes($type){
        if(is_array($type)){
            $res = [];

            if(count($type)){
                foreach($type as $el){
                    $add = self::getAllRelationsTypes($el);
                    if(count($add)){
                        foreach($add as $v){
                            $res[] = $v;
                        }
                    }
                }
            }

            return array_unique($res);
        }

        $type = (int)$type;

        if(!isset(self::$cache_allRelationsTypes[$type])){
            self::$cache_allRelationsTypes[$type] = [$type];

            foreach(self::getTypesSimpleList() as $k => $v){
                if(isset($v['parents'][$type])){
                    self::$cache_allRelationsTypes[$type][] = $k;
                }
            }
        }

        return self::$cache_allRelationsTypes[$type];
    }
}
