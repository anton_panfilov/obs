<?php

class Catalog_RemainsStatus extends Ui_Set {
    const CREATING          = 1;   // добавлен в новый заказ
    const ORDERING          = 10;  // ожидаеться заказ
    const WAIT              = 20;  // ожидаеться поставка
    //const WAIT_TO_ORDER     = 21;  // ожидаеться поставка под заказ
    const IN_STOCK          = 30;  // в наличии (свободно)
    // const IN_STOCK_TO_ORDER = 31;  // в наличии (под заказ)
    // const IN_STOCK_HOLD     = 32;  // в наличии (отложенно)
    const DELIVERY          = 40;  // доставка
    const WAIT_PAY          = 50;  // ожидание оплаты
    const SOLD              = 60;  // денги полученны
    const REJECT            = 70;  // отметить как отклоненный - добавленный по ошибке


    public function __construct(){
        parent::__construct('catalog/remains_status');

        $this->addValue_Yellow(  self::CREATING,            'remains_CREATING');
        $this->addValue_Yellow(  self::ORDERING,            'remains_ORDERING');

        $this->addValue_Yellow(  self::WAIT,                'remains_WAIT');
        //$this->addValue_Yellow(  self::WAIT_TO_ORDER,       'remains_WAIT_TO_ORDER');

        $this->addValue_White(   self::IN_STOCK,            'remains_IN_STOCK');
        //$this->addValue_White(   self::IN_STOCK_TO_ORDER,   'remains_IN_STOCK_TO_ORDER');
        //$this->addValue_White(   self::IN_STOCK_HOLD,       'remains_IN_STOCK_HOLD');

        $this->addValue_Yellow(  self::DELIVERY,            'remains_DELIVERY');

        $this->addValue_Yellow(  self::WAIT_PAY,            'remains_WAIT_PAY');

        $this->addValue_Green(   self::SOLD,                'remains_SOLD');
    }
}