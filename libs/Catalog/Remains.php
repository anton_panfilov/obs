<?php


class Catalog_Remains extends Db_Model {
    static protected $_table = 'catalog_remains';
    static protected $_structure = [

        // не обновляються при update, если явно не указанны
        ['create'],
        ['manager'],
        ['status'],
        ['item'],
        ['photo'],
        ['order'],
        ['count'],
        ['price_purchase'],
        ['price_purchase_currency'],
        ['currency_rate'],
        ['price'],
        ['comment'],

    ];

    protected function getDatabase(){
        return Db::site();
    }

    /*********************************/

    public $create;
    public $manager;
    public $status;
    public $item;
    public $photo;
    public $order;
    public $count;
    public $price_purchase;
    public $price_purchase_currency;
    public $currency_rate;
    public $price;
    public $comment;

    /*************************************************************************/

    /**
     * @param      $option
     * @param null $default
     *
     * @return null
     */
    public function getParam($option, $default = null){
        return Catalog_Options::getRemainsOption($this->id, $option, $default);
    }

    /**
     * @param $option
     * @param $value
     *
     * @return $this
     */
    public function setParam($option, $value){
        Catalog_Options::setRemainsOption($this->id, $option, $value);
        return $this;
    }

    /*************************************************************************/

    public function insert(){
        $this->create = date('Y-m-d H:i:s');

        // если не установлени менеджер и текущий пользователь менеджер - выбрать его
        if(!$this->manager && UserAccess::isClientManager()){
            $this->manager = Users::getCurrent()->id;
        }

        $this->manager = (int)$this->manager;
        $this->order   = (int)$this->order;
        $this->item    = (int)$this->item;

        parent::insert();

        $this->getItem()->updateCacheInStock(); // обновление кеша в наличии
        $this->getItem()->updateCacheInRemainsNotSale(); // обновление кеша в наличии
    }

    public function update($fields){
        parent::update($fields);
        $this->getItem()->updateCacheInStock(); // обновление кеша в наличии
        $this->getItem()->updateCacheInRemainsNotSale(); // обновление кеша в наличии
    }

    /*************************************************************************/

    /**
     * @return Catalog_Item
     */
    public function getItem(){
        return Catalog_Item::getObject($this->item);
    }

    public function getOptionsList(){
        try {
            return Catalog_Types::getTypeInfo($this->getItem()->type)['remains_params'];
        }
        catch(Exception $e){
            return [];
        }
    }

    /**
     * @param null $item
     *
     * @return Form
     */
    public function renderForm($item = null){
        Translate::addDictionary("catalog/form");

        $form = new Form();

        if($this->id){
            $form->addButton("btn_save");
        }
        else {
            $form->addButton("btn_add");
        }

        if(!$this->id && !($item || !Catalog_Item::getObject($item)->id)){
            // если эта форма на изменение или не задан item id, показать выбор товара
            $form->addElement_CatalogItem("item");
        }

        if($this->item > 0 || $item > 0){
            $photosItem = $this->item ? (int)$this->item : (int)$item;

            $photos = ["0" => "/static/img/nophoto.png"] + Catalog_Item::getObject($photosItem)->getImagesLinks();

            if(count($photos)){
                $form->addElementImagePicker("photo", "form_photo", $photos);
            }
        }

        $form->addElementSelect("status", "form_status", (new Catalog_RemainsStatus)->getArrayToSelect("..."))
            ->setRenderTypeBigValue();

        $form->addElementText_Num("count", "form_count", 1, 9999);

        $form->addElementText_Price('price_purchase', 'form_price_purchase');

        $form->addElementSelect('price_purchase_currency', 'form_price_purchase_currency', (new Currency_List())->getArrayToSelect());

        $form->addMultiElement(['price_purchase', 'price_purchase_currency'], 'form_price_purchase_all');

        $form->addElementText_Price('price', 'form_price')
            ->setRequired(false);

        $optionsList = [];
        if($this->id){
            $optionsList = $this->getOptionsList();
        }
        else if($item){
            try {
                $optionsList = Catalog_Types::getTypeInfo(Catalog_Item::getObject($item)->type)['remains_params'];
            }
            catch(Exception $e){}
        }

        if(count($optionsList)){
            foreach($optionsList as $el){
                $form->addElementRadio(
                    'o' . $el,
                    Catalog_Options::getTypeName($el),
                    Catalog_Options::getTypeListForCheckboxes($el, "select_unknown")
                )
                    ->setRequired(false)
                    ->setRenderTypeBigValue();
            }
        }

        $form->addElementTextarea("comment", "form_comment")
            ->setRequired(false);

        if($this->id){
            $form->setValues([
                'photo'                     => $this->photo,
                'status'                    => $this->status,
                'count'                     => $this->count,
                'price_purchase'            => $this->price_purchase,
                'price_purchase_currency'   => $this->price_purchase_currency,
                'price'                     => $this->price,
                'comment'                   => $this->comment,
            ]);

            if(count($optionsList)){
                foreach($optionsList as $el){
                    $form->setValue('o' . $el, $this->getParam($el));
                }
            }
        }
        else if($item && Catalog_Item::getObject($item)->id) {
            $form->setValues([
                'price_purchase'            => Catalog_Item::getObject($item)->price_purchase,
                'price_purchase_currency'   => Catalog_Item::getObject($item)->price_purchase_currency,
                'price'                     => Catalog_Item::getObject($item)->price,
            ]);
        }

        if($form->isValidAndPost()){
            if(!$this->id){
                $this->item = $item ? $item : $form->getValue('item');
            }

            $this->photo                    = $form->getValue('photo');
            $this->status                   = $form->getValue('status');
            $this->count                    = $form->getValue('count');
            $this->price_purchase           = $form->getValue('price_purchase');
            $this->price_purchase_currency  = $form->getValue('price_purchase_currency');
            $this->price                    = $form->getValue('price');
            $this->comment                  = $form->getValue('comment');

            if($this->id){
                $this->update([
                    "photo",
                    "status",
                    "count",
                    "price_purchase",
                    "price_purchase_currency",
                    "price",
                    "comment",
                ]);
            }
            else {
                $this->insert();
            }

            if(count($optionsList)){
                foreach($optionsList as $el){
                    $this->setParam($el, $form->getValue('o' . $el));
                }
            }
        }

        return $form;
    }

    public function renderMainPage(){
        if(!$this->id){
            throw new Exception("Not render main page for not init remains");
        }

        Translate::addDictionary("catalog/form");

        $form = new Form();

        // добавлен в новый заказ
        if($this->status == Catalog_RemainsStatus::CREATING){
            $form->setTitle(
                (new Catalog_RemainsStatus)->getLabel(Catalog_RemainsStatus::CREATING),
                "type_creating_description"
            );
        }

        // ожидаеться заказ
        else if($this->status == Catalog_RemainsStatus::ORDERING){
            $form->addSeparator(
                (new Catalog_RemainsStatus)->getLabel(Catalog_RemainsStatus::ORDERING),
                "type_ordering_description"
            );
        }

        // доставка
        else if($this->status == Catalog_RemainsStatus::DELIVERY){
            $form->addSeparator(
                (new Catalog_RemainsStatus)->getLabel(Catalog_RemainsStatus::DELIVERY),
                "type_delivery_description"
            );
        }

        // в наличии (свободно)
        else if(
            $this->status == Catalog_RemainsStatus::IN_STOCK &&
            !$this->order
        ){
            $form->setMarkRequiredFields(true);

            $form->setTitle("type_in_stock_free_label");
            $form->setDescription("type_in_stock_free_description");

            $form->addElementRadio("action", "form_action", [
                'new_order' => 'form_action_new_order',
                'to_order'  => 'form_action_to_order',
                'reject'    => 'form_action_reject',
            ])
                ->setLabelWidth("100%")
                ->setRenderTypeBigValue();


            $form->addElement_OrdersOnlyCreatings("order");

            $form->addConditionalFileds_Array(
                "action",
                "to_order",
                "order"
            );

            $form->addElementText_Num("count", "form_count", 1, $this->count)
                ->setValue("1")
                ->setRenderTypeBigValue()
                ->setComment(Translate::t("form_count_description", $this->count));

            $form->addElementText_Price("price", "form_price")
                ->setValue($this->price)
                ->setRenderTypeBigValue();

            $form->addConditionalFileds_Array(
                "action",
                ["new_order", "to_order"],
                ["count", "price"]
            );

            //////////////////////////

            $form->addSeparator();

            $form->addElementText('client_name', 'form_client_name')
                ->setRenderTypeBigValue();


            $form->addElementText_PhoneMobileRus('client_mobile_phone', 'form_client_mobile_phone')
                ->setPlaceholder('form_client_mobile_phone')
                ->setRequired(false);

            $form->addElementText('client_email', 'form_client_email')
                ->setPlaceholder('form_client_email')
                ->setRequired(false);

            $form->addMultiElement(
                ["client_mobile_phone", "client_email"],
                "form_client_mobile_phone_and_email",
                "client_mobile_phone_and_email"
            )
                ->setRenderTypeBigValue();

            $form->addElementText('client_vk', 'form_client_vk')
                ->setRenderTypeBigValue()
                ->setRequired(false);

            $form->addElementTextarea('client_other_contacts', 'form_client_other_contacts')
                ->setAutoHeight()
                ->setRequired(false)
                ->setRenderTypeBigValue();

            $form->addSeparator();

            $form->addElementTextarea('manager_comment', 'form_manager_comment')
                ->setAutoHeight()
                ->setRenderTypeBigValue()
                ->setRequired(false);

            $form->addConditionalFileds_Array(
                "action",
                "new_order",
                [
                    "client_name",
                    "client_mobile_phone_and_email",
                    "client_email",
                    "client_mobile_phone",
                    "client_vk",
                    "client_other_contacts",
                    "manager_comment"
                ]
            );

            if($form->isValidAndPost()){
                if($form->getValue("action") == 'new_order'){
                    $order = new Order_Item();

                    $client_fields = [
                        'name'              => 'client_name',
                        'vk'                => 'client_vk',
                        'mobile_phone'      => 'client_mobile_phone',
                        'email'             => 'client_email',
                        'other_contacts'    => 'client_other_contacts',
                    ];

                    $client_data = [];
                    foreach($client_fields as $k => $v){
                        $client_data[$k] = $form->getValue($v);
                    }

                    $order->client_data      = $client_data;
                    $order->manager_comment  = $form->getValue('manager_comment');

                    $order->insert();

                    if($form->getValue("count") < $this->count){
                        $this->count = $this->count - $form->getValue("count");
                        $this->update("count");

                        $new = clone $this;
                        $new->id    = null;
                        $new->order = $order->id;
                        $new->count = $form->getValue("count");
                        $new->insert();

                        header("location: " . Http_Url::convert(["id" => $new->id]));
                    }
                    else {
                        $this->order = $order->id;
                        $this->update("order");

                        header("location: " . Http_Url::getCurrentURL());
                    }

                    $order->updateCachePrice();

                    header("location: /account/orders/order/?id=" . $order->id);
                }
                else if($form->getValue("action") == 'to_order'){
                    if($form->getValue("count") < $this->count){
                        $this->count = $this->count - $form->getValue("count");
                        $this->update("count");

                        $new = clone $this;
                        $new->id    = null;
                        $new->order = $form->getValue("order");
                        $new->count = $form->getValue("count");
                        $new->insert();

                        header("location: " . Http_Url::convert(["id" => $new->id]));
                    }
                    else {
                        $this->order = $form->getValue("order");
                        $this->update("order");

                        header("location: " . Http_Url::getCurrentURL());
                    }

                    Order_Item::getObject($form->getValue("order"))->updateCachePrice();
                }
                else if($form->getValue("action") == 'reject'){
                    $this->status = Catalog_RemainsStatus::REJECT;
                    $this->update("status");

                    header("location: " . Http_Url::getCurrentURL());
                }
            }

        }

        // в наличии (входит в заказ)
        else if(
            $this->status == Catalog_RemainsStatus::IN_STOCK &&
            $this->order
        ){
            $form->setTitle("type_in_stock_order_label");
            $form->setDescription("type_in_stock_order_description");

            $form->addSeparator("form_sep_order");
            $form->addElementStatic(
                "form_inside_order",
                "<a href='/account/orders/order/?id={$this->order}'>" . $this->order . "</a>" .
                "<small style='padding-left:10px'>(" . Translate::t("click_for_go_to_order") . ")</small>"
            );

            $form->addElementStatic("form_order_count", Order_Item::getObject($this->order)->count);
            $form->addElementStatic("form_order_price", String_Filter::price(Order_Item::getObject($this->order)->price));


            $form->addSeparator("form_sep_order_client");
            $form->addElementStatic(
                "form_inside_order_name",
                Order_Item::getObject($this->order)->getName("?") .
                "<a style='padding-left:10px' href='/account/orders/order/client?id={$this->order}'><i class='fa fa-pencil'></i></a> "
            );

            if(strlen(Order_Item::getObject($this->order)->getPhone())){
                $form->addElementStatic(
                    "form_inside_order_phone",
                    Order_Item::getObject($this->order)->getPhone()
                );
            }

            if(strlen(Order_Item::getObject($this->order)->getVkProfile())){
                $form->addElementStatic(
                    "form_inside_order_vk",
                    Order_Item::getObject($this->order)->getVkProfile(true)

                );
            }

            ////////////////////////////
            $form->addSeparator();
            $form->addElementRadio("action", "form_action", [
                'out_order' => 'form_action_out_order',
            ])
                ->setLabelWidth("100%")
                ->setRenderTypeBigValue();

            if($form->isValidAndPost()){
                if($form->getValue("action") == 'out_order'){
                    // отвязать товар от заказа
                    $order = $this->order;
                    $this->order = 0;
                    $this->update("order");

                    // переиндексирование товаров
                    Order_Item::getObject($order)->updateCachePrice();

                    // если в товаре больше не осталось товаров
                    if(Order_Item::getObject($order)->count == 0){
                        // удалить заказ
                        Order_Item::getObject($order)->delete();
                    }

                    header("location: " . Http_Url::getCurrentURL());
                }
            }
        }

        // денги полученны
        else if($this->status == Catalog_RemainsStatus::SOLD){
            $form->addSeparator(
                (new Catalog_RemainsStatus)->getLabel(Catalog_RemainsStatus::SOLD),
                "type_creating_description"
            );
        }

        // ожидаеться поставка
        else if($this->status == Catalog_RemainsStatus::WAIT){
            $form->addSeparator(
                (new Catalog_RemainsStatus)->getLabel(Catalog_RemainsStatus::WAIT),
                "type_creating_description"
            );
        }

        // ожидание оплаты
        else if($this->status == Catalog_RemainsStatus::WAIT_PAY){
            $form->addSeparator(
                (new Catalog_RemainsStatus)->getLabel(Catalog_RemainsStatus::WAIT_PAY),
                "type_creating_description"
            );
        }

        return $form;


    }

    public function getImageLink($size = 150){
        if($this->photo){
            $photo = Catalog_Photo::getObject($this->photo);

            if($photo->id && $photo->getLink($size)){
                return $photo->getLink($size);
            }
        }

        return null;
    }
}