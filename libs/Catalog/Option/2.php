<?php

class Catalog_Option_2 extends Catalog_Option_Abstract {
    const MALE   = 1;
    const FEMALE = 2;

    // пол
    static protected $options = [
        1, // мальчики
        2, // девочки
    ];
}