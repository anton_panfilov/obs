<?php

class Catalog_Option_5 extends Catalog_Option_Abstract {
    // цвет
    // http://www.mathsisfun.com/hexadecimal-decimal-colors.html

    public function decoratorLabelForCheckboxes($id, $label){
        if($id === ""){
            // не определенно
            return "<span style='width:14px; height: 12px; margin: 0 5px 0 5px; background: #FFF; border: #FFF solid 1px; display: inline-block; text-align: center'>?</span>" . $label;
        }

        if($id === 1){
            // черно белый
            return "<span style='width:7px; height: 12px; margin: 0 0 0 5px; background: #000; display: inline-block'></span>" .
            "<span style='width:7px; height: 12px; margin: 0 5px 0 0; background: #FFF; border: #DDD solid 1px; border-left:0; display: inline-block'></span>" . $label;
        }

        if($id === 2){
            // многоцветный
            return
                "<span style='width:2px; height: 12px; margin: 0 0 0 5px; background: #ff4500; display: inline-block'></span>" .
                "<span style='width:2px; height: 12px; margin: 0 0 0 0; background: #ffa500; display: inline-block'></span>" .
                "<span style='width:2px; height: 12px; margin: 0 0 0 0; background: #ffff00; display: inline-block'></span>" .
                "<span style='width:2px; height: 12px; margin: 0 0 0 0; background: #228b22; display: inline-block'></span>" .
                "<span style='width:2px; height: 12px; margin: 0 0 0 0; background: #87cefa; display: inline-block'></span>" .
                "<span style='width:2px; height: 12px; margin: 0 0 0 0; background: #1e90ff; display: inline-block'></span>" .
                "<span style='width:2px; height: 12px; margin: 0 5px 0 0; background: #da70d6; display: inline-block'></span>" .
                $label;
        }

        if($id == 16777215){
            // белый
            return "<span style='width:14px; height: 12px; margin: 0 5px 0 5px; background: #FFF; border: #DDD solid 1px; display: inline-block'></span>" . $label;
        }

        $color = dechex($id);
        if($id == 0){
            $color = "000";
        }
        return "<span style='width:14px; height: 12px; margin: 0 5px 0 5px; background: #{$color}; display: inline-block'></span>" . $label;
    }

    static protected $options = [

        0,          // черный
        13882323,   // серый
        16777215,   // белый
        1,          // черно-белый
        16729344,   // красный
        16753920,   // оранжевый
        16761035,   // розовый
        16768494,   // светло-розовый

        2003199,    // синий
        8900346,    // голубой
        4251856,    // бирюзовый
        14315734,   // фиолетовый


        16776960,   // желтый
        2263842,    // зеленый
        10506797,   // коричневый
        16772045,   // бежевый
        2,          // многоцвет
    ];
}