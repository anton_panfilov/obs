<?php

class Catalog_Option_1 extends Catalog_Option_Abstract {
    // тип
    static protected $options = [
        1, // платье
        2, // кофта
        3, // рубашка
        4, // футболка
        5, // штаны
        6, // комплект
        7, // обувь
    ];
}