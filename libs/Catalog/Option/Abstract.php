<?php

abstract class Catalog_Option_Abstract {
    static protected $listCache = [];
    static protected $type;
    static protected $options;

    /**
     * @return int
     */
    static protected function getID(){
        return (int)substr(get_called_class(), 15);
    }


    public function decoratorLabelForCheckboxes($id, $label){
        return $label;
    }

    /**
     * @return string
     */
    static public function getName(){
        return Catalog_Options::translate(static::getID());
    }

    /**
     * @param null $first
     *
     * @return array
     */
    static public function getList($first = null, $forCheckboxes = false){
        if(!isset(self::$listCache[static::getID()])) {
            self::$listCache[static::getID()] = [];

            if(count(static::$options)) {
                foreach(static::$options as $el) {
                    self::$listCache[static::getID()][$el] = Catalog_Options::translate(static::getID() . "." . (int)$el);
                }
            }
        }

        $result = self::$listCache[static::getID()];

        if(strlen($first)){
            $result = ['' => Catalog_Options::translate($first)] + $result;
        }

        if($forCheckboxes && count($result)){
            foreach($result as $k => $v){
                $result[$k] = Catalog_Options::getObject(static::getID())->decoratorLabelForCheckboxes($k, $v);
            }
        }

        return $result;
    }
}