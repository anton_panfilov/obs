<?php

class Catalog_Option_4 extends Catalog_Option_Abstract {
    // материал
    static protected $options = [
        1, // хлопок
        2, // полиэстер
        4, // нейлон
        6, // Шерсть
        7, // Лён
        8, // Кашемир
        9, // Джинс
        10, // Флис
    ];
}