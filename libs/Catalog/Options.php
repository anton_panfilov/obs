<?php

class Catalog_Options {

    const TYPE       = 1;
    const GENDER     = 2;
    const AGE        = 3;
    const MATERIAL   = 4;
    const COLOR      = 5;
    const COUNTRY    = 6;
    const TAG        = 7;

    //////////////////

    static public function translate($key){
        return Translate::t($key, 'catalog/option');
    }

    //////////////////

    static protected $types = [
        self::TYPE,
        self::GENDER,
        self::AGE,
        self::MATERIAL,
        self::COLOR,
    ];

    static protected $typesStock = [
        self::AGE,
        self::COLOR,
    ];

    static public function getTypes(){
        return self::$types;
    }


    static public function getTypesForStock(){
        return self::$typesStock;
    }

    /**
     * @param $type
     *
     * @return Catalog_Option_Abstract
     * @throws Exception
     */
    static public function getObject($type){
        $class = "Catalog_Option_" . (int)$type;


        if(class_exists($class)){
            return new $class;
        }

        throw new Exception("Invalid catalog type `{$type}`");
    }

    /**
     * @param      $type
     * @param null $first
     *
     * @return array
     * @throws Exception
     */
    static public function getTypeList($type, $first = null){
        return self::getObject($type)->getList($first);
    }

    /**
     * Поулчить строковое значение по ids типр и значения
     *
     * @param      $type
     * @param      $option
     * @param null $default
     *
     * @return null
     */
    static public function getValue($type, $option, $default = null){
        $option = (int)$option;
        $all = self::getTypeList((int)$type);

        return isset($all[$option]) ? $all[$option] : $default;
    }

    /**
     * @param      $type
     * @param null $first
     *
     * @return array
     * @throws Exception
     */
    static public function getTypeListForCheckboxes($type, $first = null){
        return self::getObject($type)->getList($first, true);
    }

    /**
     * Получить label по id типа + id элемента в этому типе
     *
     * @param        $type
     * @param        $elementID
     * @param string $default
     *
     * @return string
     */
    static public function getTypeValueLabel($type, $elementID, $default = 'unknown'){
        return isset(self::getTypeList($type)[(int)$elementID]) ?
            self::getTypeList($type)[(int)$elementID] :
            $default;
    }

    /**
     * @param $type
     *
     * @return string
     * @throws Exception
     */
    static public function getTypeName($type){
        return self::getObject($type)->getName();
    }

    //////////////////

    static protected $options = [];

    /**
     * Получить опции
     *
     * @param $item
     * @param $type
     *
     * @return array
     */
    static public function getOptions($item, $type = null){
        $item = (int)$item;
        $type = is_null($type) ? null : (int)$type;

        if(!isset(self::$options[$item])){
            // load
            self::$options[$item] = [];

            $all = Db::site()->fetchAll("SELECT `option`, `value` FROM `catalog_options` WHERE `item`=?", $item);


            if(count($all)){
                foreach($all as $el){
                    if(!isset(self::$options[$item][$el['option']])){
                        self::$options[$item][$el['option']] = [];
                    }
                    self::$options[$item][$el['option']][] = $el['value'];
                }
            }
        }

        //Value::export($type);
        //Value::export(self::$options[$item]);

        if(is_null($type)){
            return self::$options[$item];
        }

        return isset(self::$options[$item][$type]) ? self::$options[$item][$type] : [];
    }

    /**
     * @param $item
     * @param $type
     * @param $options
     *
     * @return bool
     */
    static public function setOptions($item, $type, $options){
        $item = (int)$item;
        $type = (int)$type;

        if(!is_array($options)){
            if(is_numeric($options)) {
                $options = [$options];
            }
            else {
                $options = [];
            }
        }

        $new = [];

        if(count($options)){
            foreach($options as $el){
                $el = (string)(int)$el;
                $new[] = $el;
            }
            array_unique($new);
        }

        $current = self::getOptions($item, $type);

        sort($new);
        sort($current);

        $newString     = count($new)     ? implode(",", $new)     : "";
        $currentString = count($current) ? implode(",", $current) : "";

        if($newString != $currentString){
            unset(self::$options[$item][$type]);
            Db::site()->delete("catalog_options", "`item`=? and `option`=?", $item, $type);

            if(count($new)){
                self::$options[$item][$type] = $new;

                $insert = [];
                foreach($new as $el){
                    $insert[] = [
                        'item'   => $item,
                        'option' => $type,
                        'value'  => $el,
                    ];
                }

                Db::site()->insertMulti('catalog_options', $insert);
            }

            return true;
        }

        return false;
    }

    /**
     * Удаление лишних опций
     *
     * @param $item
     *
     * @return bool
     */
    static public function deleteNotActualOptions($item){
        $del = [];
        $actual = Catalog_Types::getTypeInfo(Catalog_Item::getObject($item)->type)['full_params'];
        $allSaves = array_keys(self::getOptions($item));

        if(count($allSaves)){
            foreach($allSaves as $el) {
                if(!in_array($el, $actual)){
                    $del[] = $el;
                    unset(self::$options[$item][$el]);
                }
            }

        }

        if(count($del)){
            Db::site()->delete(
                "catalog_options",
                "`item`=? and `option` in (" .
                    implode(", ", $del) .
                ")",
                Catalog_Item::getObject($item)->id
            );
        }

        return false;
    }

    //////////////////////////////////////////

    static public function getRenderArray($item, $type){
        $options = Catalog_Options::getOptions($item, $type);

        if(count($options)){
            $all = [];

            $list = self::getTypeList($type);

            foreach($options as $el){
                if(isset($list[$el])){
                    $all[] = $list[$el];
                }
            }

            if(count($all)){
                return $all;
            }

        }

        return [];
    }


    /**
     * Отрисовать строку определенную строку параметров для элемента каталога
     *
     * @param        $item - id элемента каталога
     * @param        $type - id типа параметров
     *
     * @param string $separator
     *
     * @return null|string
     */
    static public function renderString($item, $type, $separator = "<br>"){
        $options = self::getRenderArray($item, $type);
        if(count($options)){
            return implode($separator, $options);
        }


        return null;
    }

    //////////////////////////////////////////////////////////////////////////////////////////

    static protected $remainsOptions = [];

    /**
     * Поулчить все параметры
     *
     * @param $remains_id
     *
     * @return mixed
     */
    static public function getRemainsOptions($remains_id){
        $remains_id = (int)$remains_id;

        if(!isset(self::$remainsOptions[$remains_id])){
            self::$remainsOptions[$remains_id] = Db::site()->fetchPairs(
                "select `option`, `value` from `catalog_remains_options` where `remains`=?", $remains_id
            );
        }

        return self::$remainsOptions[$remains_id];
    }

    /**
     * Полуить один параметр
     *
     * @param      $remains_id
     * @param      $option
     * @param null $default
     *
     * @return null
     */
    static public function getRemainsOption($remains_id, $option, $default = null){
        self::getRemainsOptions($remains_id);

        return isset(self::$remainsOptions[$remains_id][$option]) ? self::$remainsOptions[$remains_id][$option] : $default;
    }

    /**
     * Сохранить один параметр
     *
     * @param $remains_id
     * @param $option
     * @param $value
     */
    static public function setRemainsOption($remains_id, $option, $value){
        self::getRemainsOptions($remains_id);

        $remains_id = (int)$remains_id;
        $option     = (int)$option;
        $value      = (int)$value;

        Db::site()->delete("catalog_remains_options", "`remains`=? and `option`=?", $remains_id, $option);
        unset(self::$remainsOptions[$remains_id][$option]);


        if(
            $value &&
            in_array($option, self::getTypesForStock()) &&
            Catalog_Remains::getObject($remains_id)->id
        ){
            // todo проверить value

            Db::site()->insert("catalog_remains_options", [
                "remains"   => $remains_id,
                "item"      => Catalog_Remains::getObject($remains_id)->item,
                "option"    => $option,
                "value"     => $value,
            ]);

            self::$remainsOptions[$remains_id][$option] = $value;
        }
    }
}