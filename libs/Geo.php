<?php

class Geo {

    static protected $states = array(
        'AL'=>"Alabama",                'AK'=>"Alaska",         'AZ'=>"Arizona",        'AR'=>"Arkansas",
        'CA'=>"California",             'CO'=>"Colorado",       'CT'=>"Connecticut",    'DE'=>"Delaware",
        'DC'=>"District Of Columbia",   'FL'=>"Florida",        'GA'=>"Georgia",        'HI'=>"Hawaii",
        'ID'=>"Idaho",                  'IL'=>"Illinois",       'IN'=>"Indiana",        'IA'=>"Iowa",
        'KS'=>"Kansas",                 'KY'=>"Kentucky",       'LA'=>"Louisiana",      'ME'=>"Maine",
        'MD'=>"Maryland",               'MA'=>"Massachusetts",  'MI'=>"Michigan",       'MN'=>"Minnesota",
        'MS'=>"Mississippi",            'MO'=>"Missouri",       'MT'=>"Montana",        'NE'=>"Nebraska",
        'NV'=>"Nevada",                 'NH'=>"New Hampshire",  'NJ'=>"New Jersey",     'NM'=>"New Mexico",
        'NY'=>"New York",               'NC'=>"North Carolina", 'ND'=>"North Dakota",   'OH'=>"Ohio",
        'OK'=>"Oklahoma",               'OR'=>"Oregon",         'PA'=>"Pennsylvania",   'RI'=>"Rhode Island",
        'SC'=>"South Carolina",         'SD'=>"South Dakota",   'TN'=>"Tennessee",      'TX'=>"Texas",
        'UT'=>"Utah",                   'VT'=>"Vermont",        'VA'=>"Virginia",       'WA'=>"Washington",
        'WV'=>"West Virginia",          'WI'=>"Wisconsin",      'WY'=>"Wyoming",
    );

    static protected $states_fips = [
        'AK' => 2,  'AL' => 1,  'AR' => 5,  'AS' => 60, 'AZ' => 4,
        'CA' => 6,  'CO' => 8,  'CT' => 9,  'DC' => 11, 'DE' => 10,
        'FL' => 12, 'FM' => 64, 'GA' => 13, 'GU' => 66, 'HI' => 15,
        'IA' => 19, 'ID' => 16, 'IL' => 17, 'IN' => 18, 'KS' => 20,
        'KY' => 21, 'LA' => 22, 'MA' => 25, 'MD' => 24, 'ME' => 23,
        'MH' => 68, 'MI' => 26, 'MN' => 27, 'MO' => 29, 'MP' => 69,
        'MS' => 28, 'MT' => 30, 'NC' => 37, 'ND' => 38, 'NE' => 31,
        'NH' => 33, 'NJ' => 34, 'NM' => 35, 'NV' => 32, 'NY' => 36,
        'OH' => 39, 'OK' => 40, 'OR' => 41, 'PA' => 42, 'PR' => 72,
        'PW' => 70, 'RI' => 44, 'SC' => 45, 'SD' => 46, 'TN' => 47,
        'TX' => 48, 'UT' => 49, 'VA' => 51, 'VI' => 78, 'VT' => 50,
        'WA' => 53, 'WI' => 55, 'WV' => 54, 'WY' => 56,
    ];

    static protected $states_timezones = [
        'est' => ['ME','NH','MA','RI','CT','NY','VT','PA','NJ','DE','MD','MI','OH','IN','WV','VA','NC','SC','GA','FL'],
        'cst' => ['ND','MN','WI','SD','NE','IA','IL','KS','MO','OK','AR','TN','TX','LA','MS','AL'],
        'mst' => ['MT','ID','WY','UT','CO','NM','AZ'],
        'pst' => ['WA','OR','CA','NV'],
    ];


    /********************************************************************************************/


    /**
     * Получить массив штатов разбитых по таймзонам
     *
     * @return array
     */
    static public function getTimeZoneStatesRelationsArray(){
        return self::$states_timezones;
    }

    /**
     * Получить таймзону по 2-х буквенному коду штата
     * Если будет указан не существующий штат, таймзона будет получена по умолчанию
     *
     * @param mixed $state
     * @param mixed $default
     * @return int|mixed|string
     */
    static public function getTimeZone_FromState($state, $default = 'pst'){
        if(strlen($state) == 2){
            $state = strtoupper($state);
            $array = self::getTimeZoneStatesRelationsArray();

            foreach($array as $tz => $ar){
                foreach($ar as $st){
                    if($state == $st) return $tz;
                }
            }
        }

        return $default;
    }



    /********************************************************************************************/

    static protected $statesByZIP = [];
    static protected $statesAndCityByZIP = [];

    /**
     * Получить код штата по ZIP
     *
     * @param $zip
     * @return mixed
     */
    static public function getStateByZIP($zip){
        $zip = (int)(string)$zip;
        if(!isset(self::$statesByZIP[$zip])){
            self::$statesByZIP[$zip] = Db::processing()->fetchOne("SELECT State FROM `geo_us_zips` WHERE `ZipCode`=?", $zip);
            if(!strlen(self::$statesByZIP[$zip])){
                self::$statesByZIP[$zip] = null;
            }
        }
        return self::$statesByZIP[$zip];
    }

    /**
     * Получить код штата и город по ZIP
     *
     * @param $zip
     * @return false or [state, city]
     */
    static public function getStateAndCityByZIP($zip){
        $zip = (int)(string)$zip;
        if(!isset(self::$statesAndCityByZIP[$zip])){
            self::$statesAndCityByZIP[$zip] = Db::processing()->fetchRow(
                "SELECT `State` as `state`, `City` as `city` FROM `geo_us_zips` WHERE `ZipCode`=?", $zip
            );
        }
        return self::$statesAndCityByZIP[$zip];
    }

    /**
     * Получить FIPS номер штата по 2-х буквенному коду
     *
     * @param $stateCode
     * @return int|null
     */
    static public function getStateFIPS($stateCode){
        if(is_numeric($stateCode)) return (int)$stateCode;

        return isset(self::$states_fips[$stateCode]) ? self::$states_fips[$stateCode] : null;
    }

    /**
     * Получить 2-х буквенный код штата по FIPS номеру
     *
     * @param $stateFIPS
     * @return mixed|null
     */
    static public function getStateCode($stateFIPS){
        $fips = array_search((int)$stateFIPS, self::$states_fips);
        return ($fips !== false) ? $fips : null;
    }

    /**
     * Массив штатов - 2-х буквенный код => название
     *
     * @return array
     */
    static public function getStatesList(){
        return self::$states;
    }

    /**
     * Массив 2-х буквенных кодов штатов
     *
     * @return array
     */
    static public function getStatesCodesList(){
        return array_keys(self::$states);
    }

    /**
     * Получить название штата по 2-х буквенному коду
     *
     * @param $name
     * @return mixed
     */
    static public function getStateTitle($name){
        $intName = (int)$name;
        if($intName > 0 && in_array($intName, self::$states_fips)){
            $name = array_search($intName, self::$states_fips);
        }

        return isset(self::$states[$name]) ? self::$states[$name] : $name;
    }

    /********************************************************************************************/

    static protected $regions = [
        "22" => "Алтайский край",
        "28" => "Амурская область",
        "29" => "Архангельская область",
        "30" => "Астраханская область",
        "31" => "Белгородская область",
        "32" => "Брянская область",
        "33" => "Владимирская область",
        "34" => "Волгоградская область",
        "35" => "Вологодская область",
        "36" => "Воронежская область",
        "79" => "Еврейская АО",
        "75" => "Забайкальский край",
        "37" => "Ивановская область",
        "38" => "Иркутская область",
        "07" => "Кабардино-Балкарская Республика",
        "39" => "Калининградская область",
        "40" => "Калужская область",
        "41" => "Камчатский край",
        "09" => "Карачаево-Черкесская Республика",
        "42" => "Кемеровская область",
        "43" => "Кировская область",
        "44" => "Костромская область",
        "23" => "Краснодарский край",
        "24" => "Красноярский край",
        "45" => "Курганская область",
        "46" => "Курская область",
        "47" => "Ленинградская область",
        "48" => "Липецкая область",
        "49" => "Магаданская область",
        "77" => "Москва",
        "50" => "Московская область",
        "51" => "Мурманская область",
        "83" => "Ненецкий АО",
        "52" => "Нижегородская область",
        "53" => "Новгородская область",
        "54" => "Новосибирская область",
        "55" => "Омская область",
        "56" => "Оренбургская область",
        "57" => "Орловская область",
        "58" => "Пензенская область",
        "59" => "Пермский край",
        "25" => "Приморский край",
        "60" => "Псковская область",
        "01" => "Республика Адыгея",
        "04" => "Республика Алтай",
        "02" => "Республика Башкортостан",
        "03" => "Республика Бурятия",
        "05" => "Республика Дагестан",
        "06" => "Республика Ингушетия",
        "08" => "Республика Калмыкия",
        "10" => "Республика Карелия",
        "11" => "Республика Коми",
        "81" => "Республика Крым",
        "12" => "Республика Марий Эл",
        "13" => "Республика Мордовия",
        "14" => "Республика Саха /Якутия/",
        "15" => "Республика Северная Осетия - Алания",
        "16" => "Республика Татарстан",
        "17" => "Республика Тыва",
        "19" => "Республика Хакасия",
        "61" => "Ростовская область",
        "62" => "Рязанская область",
        "63" => "Самарская область",
        "78" => "Санкт-Петербург",
        "64" => "Саратовская область",
        "65" => "Сахалинская область",
        "66" => "Свердловская область",
        "82" => "Севастополь", // !
        "67" => "Смоленская область",
        "26" => "Ставропольский край",
        "68" => "Тамбовская область",
        "69" => "Тверская область",
        "70" => "Томская область",
        "71" => "Тульская область",
        "72" => "Тюменская область",
        "18" => "Удмуртская Республика",
        "73" => "Ульяновская область",
        "27" => "Хабаровский край",
        "86" => "Ханты-Мансийский АО",
        "74" => "Челябинская область",
        "20" => "Чеченская Республика",
        "21" => "Чувашская Республика",
        "87" => "Чукотский АО",
        "89" => "Ямало-Ненецкий АО",
        "76" => "Ярославская область",
    ];

    /**
     * Получить список регионов РФ
     *
     * @return array
     */
    static public function getRegions(){
        return self::$regions;
    }

    /**
     * Получить название региона по коду
     *
     * @param $code
     * @param string $default
     * @return string
     */
    static public function getRegionTitle($code, $default = 'Unknown'){
        $code = sprintf("%02d", (int)(string)$code);
        return isset(self::$regions[$code]) ? self::$regions[$code] : $default;
    }

    /**
     * Получить список кодов регионов РФ
     *
     * @return array
     */
    static public function getRegionsCodes(){
        return array_keys(self::$regions);
    }


    /********************************************************************************************/


    static protected $cache_counties = null;

    /**
     * Получить всю информацию о странах из таблицы site.geo_countries
     *
     * @return array|null
     */
    static public function getCountriesAllData(){
        if(is_null(self::$cache_counties)){
            self::$cache_counties = Db::site()->fetchAll(
                "SELECT * FROM `geo_countries` order by `title_eng`"
            );
        }

        return self::$cache_counties;
    }

    /**
     * Получить код страны по IP
     *
     * @param null $ip
     * @return string
     */
    static public function getCountryCode($ip = null){
        if(is_null($ip)) $ip = $_SERVER['REMOTE_ADDR'];
        $code = '';
        if(function_exists('geoip_country_code_by_name')){
            $code = @geoip_country_code_by_name($ip);
        }
        return $code;          
    }

    /**
     * Получить список стран для select box
     *
     * @return array
     */
    static public function getCountriesList(){
        $result = [];

        $countries = self::getCountriesAllData();
        if(count($countries)){
            foreach($countries as $country) {
                if($country['title_eng'] == $country['title_lang']) {
                    $result[$country['code_2']] = $country['title_eng'];
                }
                else {
                    $result[$country['code_2']] = $country['title_eng'] . " ({$country['title_lang']})";
                }
            }
        }

        return $result;
    }

    /**
     * Получить телефонны код страны у пользователя с текущим ip
     *
     * @return string
     */
    static public function getCurrentCountryPhoneCode(){
        if(
            ($code = @geoip_country_code_by_name($_SERVER['REMOTE_ADDR'])) &&
            ($code = Db::site()->fetchOne("SELECT `phone_code` FROM `geo_countries` WHERE `code_2`=?", $code))
        ){
            return "+{$code}.";
        }
        return "";
    }

    /********************************************************************************************/

}