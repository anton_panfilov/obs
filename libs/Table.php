<?php

class Table {
    static protected $autoIdNum = 1;

    protected $id;

    /**
     * массив кнопок над таблицей
     *
     * @var Ui_Button[]
     */
    protected $buttons = array();

    /**
     * Заголовок таблицы
     *
     * @var mixed
     */
    protected $title;

    /**
     * Логотип таблицы
     *
     * @var string|null ссылка на картинку
     */
    protected $logo;

    /**
     * Текст, выводимый, если нет данных
     *
     * @var mixed
     */
    protected $messageNotData = 'No Data';

    protected $hover = true;

    protected $striped = true;

    ///////////////////////////////////////////////////

    protected $header_groups = [];

    protected $header_group_current = null;

    /**
     * Данные
     *
     * @var array
     */
    protected $data = array();

    /**
     * Настройка сток суммарных данных
     *
     * @var mixed
     */
    protected $footer = array();

    /**
     * Насторойка полей
     *
     * @var Table_Field_Abstract[]
     */
    protected $fields = array();

    protected $details_field = null;

    protected $is_all_nowrap = true;

    protected $is_hide_print = false;

    ///////////////////////////////////////////////////

    const PAGING_DISABLED   = 0; // пейджинг отключен
    const PAGING_AUTO       = 1; // пейджинг делается автоматически на основе полученных данных
    const PAGING_MANUAL     = 2; // принимается заранее настроенный объект пейджинга


    /**
     * Использование пейджинга
     *
     * @var int
     */
    protected $is_paging = null;

    protected $paging_show_one_page_counts = true;

    /**
     * Использование пейджинга
     *
     * @var Table_Paging|int (для хранения pageLen при PAGING_AUTO)
     */
    protected $paging = null;

    protected $pagingGetValueName = '_page';

    ///////////////////////////////////////////////////

    /**
     * Показывать ли заголовки полей
     *
     * @var bool
     */
    protected $head_show = true;

    /**
     * Атрибуты таблицы
     *
     * @var Html_Attribs
     */
    protected $table_attribs;

    /**
     * Атрибуты строки с данными
     *
     * @var Html_Attribs
     */
    protected $tr_attribs;

    /**
     * Массив сорторовок по умолчанию
     *
     * @var array
     */
    protected $sort_default = array();

    /***********************************************************************************************************/

    public function __construct($data = null){
        // \AP::site()->addCSS('table/main.css');
        // todo: добавить стили

        $this->id = 'tbl' . self::$autoIdNum++;

        $this->setData($data);
        $this->table_attribs = new Html_Attribs();
        $this->tr_attribs = new Html_Attribs();
        $this->setPagingAuto(100);
    }

    /************************************************************************************************************
    *  Header groups
    ****************************************************/

    public function startGroup($label){
        if($this->header_group_current){
            $this->endGroup();
        }

        $this->header_group_current = count($this->header_groups) + 1;
        $this->header_groups[$this->header_group_current] = [
            'label'     => Translate::t($label),
            'counts'    => 0,
        ];

        return $this;
    }

    public function endGroup(){
        $this->header_group_current = null;

        return $this;
    }

    /************************************************************************************************************
    *  Content and vaiew settings
    ****************************************************/

    /**
     * Установка названия таблицы
     *
     * @param mixed $title
     * @return Table
     */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * Установить логотип таблицы
     *
     * @param mixed $logo
     * @return self
     */
    public function setLogo($logo){
        $this->logo = $logo;
        return $this;
    }

    /**
     * Установить текст, который показывается если нет данных
     *
     * @param mixed $text
     * @return Table
     */
    public function setMessagesNotData($text){
        $this->messageNotData = $text;
        return $this;
    }

    /**
     * Прятять шапку табоицы
     *
     * @return Table
     */
    public function setHeadHide(){
        $this->head_show = false;
        return $this;
    }

    /**
     * Спрятать при печати
     *
     * @return Table
     */
    public function setPrintHide(){
        $this->is_hide_print = true;
        return $this;
    }

    /**
     * Прятять шапку таблицы (по умолчанию)
     *
     * @return Table
     */
    public function setHeadShow(){
        $this->head_show = true;
        return $this;
    }

    public function setAllWhiteSpaceNowrap($flag){
        $this->is_all_nowrap = (bool)$flag;
        return $this;
    }

    /************************************************************************************************************
     *  Сортировки
     ****************************************************/

    /**
     * @param $fieldIndex
     * @return self
     */
    public function addSortDefaultDESC($fieldIndex){
        $this->sort_default[] = array($fieldIndex, 1);
        return $this;
    }

    /**
     * @param $fieldIndex
     * @return self
     */
    public function addSortDefaultASC($fieldIndex){
        $this->sort_default[] = array($fieldIndex, 0);
        return $this;
    }

    /************************************************************************************************************
    *  Кнопки надтабличные (например: ссылка на добаление новой записи)
    ****************************************************/

    /**
     * Создать новую кнопку и получить её объект
     *
     * @param string $name
     * @param string $label
     *
     * @return Ui_Button
     */
    public function addButton($label, $name = null){
        if(is_null($name))  return $this->buttons[]      = new Ui_Button($label);
        else                return $this->buttons[$name] = new Ui_Button($label, $name);
    }

    /**
     * Получить объект кнопки или Exception если такой кнопки не существует
     *
     * @param string $name Имя кнопки
     *
     * @throws \Exception  Если кнопки с данным именем не существует
     * @return Ui_Button
     */
    public function getButton($name){
        if(!isset($this->buttons[$name])){
            throw new \Exception("Button '{$name}' not found");
        }
        return $this->buttons[$name];
    }

    /**
     * Получить массив кнопок
     *
     * @return array
     */
    public function getButtons(){
        return $this->buttons;
    }

    /************************************************************************************************************
    *  AJAX
    ****************************************************/

    /**
     * Добавить функцию, вызываемую через AJAX
     * - что бы вызвать функцию, надо передать ей имя через get переменную ap_table_ajax_func
     * - функции никуда не сохраняются, а вызываются в реальном времени, при этом надо учитывать
     *   что добавляться она долна, только если у данного пользоваетля, есть права на её выполнение
     *   или внутри функции проверяются права
     *
     * @param string $name
     * @param \Closure $callable
     */
    /*
    static public function ajaxFunction($name, \Closure $callable){
        if(isset($_GET['ap_table_ajax_func']) && $_GET['ap_table_ajax_func'] == $name){
            $result = $callable($_POST);

            // если результат не объект типа \AP\Result
            if(!($result instanceof \AP\Result)){
                $result = ($result === true || $result === null || (int)$result === 1) ?
                    (new \AP\Result()) :
                    (new \AP\Result())->addError($result);
            }

            die( $result->getJSON() );
        }
    }
    */


    /************************************************************************************************************
    *  Работа с данными
    ****************************************************/

    /**
     * Установить данные
     * Принимает массивы или объекты типа Db_Statment_Select
     *
     * @param array|Db_Statment_Select|null $data
     * @return Table
     */
    public function setData($data){
        if(
            $data instanceof Db_Statment_Select ||
            is_array($data)
        ){
            $this->data = $data;
        }

        return $this;
    }

    /************************************************************************************************************
     *  Paging
     ****************************************************/

    /**
     * Отключение постаничного вывода данных
     *
     * @return Table
     */
    public function setPagingDisabled(){
        $this->is_paging = self::PAGING_DISABLED;
        return $this;
    }

    /**
     * @return Table
     */
    public function setPagingHideOnePageCount(){
        $this->paging_show_one_page_counts = false;
        return $this;
    }

    /**
     * Автоматическое посроение страниц на основе полученных данных
     * Если данные будут представленны в виде массива, то на странице будут показаны только подходящие элементы массива
     * А если будет передан объект запроса, будут к нему будет добавлен limit и из базы данных будут получены только те страницы, которые нужны для тсраницы
     *
     * @param int $pageLen
     * @return Table
     */
    public function setPagingAuto($pageLen = 100, $getValueName = '_page'){
        $this->is_paging            = self::PAGING_AUTO;
        $this->paging               = max(1, (int)$pageLen);

        if($getValueName){
            $this->pagingGetValueName   = $getValueName;
        }

        return $this;
    }

    /**
     * Задать настроенный объект пейджинга
     *
     * @param Table_Paging $pagingObject
     * @return Table
     */
    public function setPagingObject(Table_Paging $pagingObject){
        $this->is_paging    = self::PAGING_MANUAL;
        $this->paging       = $pagingObject;
        return $this;
    }

    /**
     * @return int|Table_Paging
     */
    public function getPaging(){
        return $this->paging;
    }

    /************************************************************************************************************
     *  Атрибуты
     ****************************************************/

    /**
     * Задать атрибут строки (td в table)
     *
     * @param string $name
     * @param string $value
     * @return Table
     */
    public function setTrAttrib($name, $value = null){
        $this->tr_attribs->setAttrib($name, Context::pattern($value));
        return $this;
    }

    /**
     * Задать ID строки (td в table)
     * Используется если необходимо проводить со строками какие то манипуляции на JavaScript
     * Напрмиер: "tr_{id}", при том что в контексте будет уникальная переменная id
     *
     * @param string $id
     * @return Table
     */
    public function setTrId($id){
        return $this->setTrAttrib('id', $id);
    }

    public function setTableAttrib($name, $value = null){
        $this->table_attribs->setAttrib($name, $value);
        return $this;
    }

    /************************************************************************************************************
     *  Поля
     ****************************************************/

    protected $randomIndexesNum = 1;

    /**
     * Получить случайное, уникальное имя для поля, для которого имя не имеет значения
     *
     * @return string
     */
    protected function getRandomFieldIndex(){
        $index = "element" . $this->randomIndexesNum++;

        if(isset($this->fields[$index])){
            $index = $this->getRandomFieldIndex();
        }

        return $index;
    }

    /**
     * Переписываает насйтойку отображения массива на стандартную
     *
     * Присвоить всем полям тектовый тип
     */
    protected function autoFields(){
        $this->fields = array();

        if(count($this->data)){
            $keys = array_keys(reset($this->data));
            if(count($keys)){
                foreach($keys as $v){
                    $this->addField_Text($v);
                }
            }
        }
    }

    /**
     * Добавление нового текстового поля
     *
     * @param Table_Field_Abstract $field
     * @param string|null $index
     *
     * @throws \Exception
     * @return Table_Field_Abstract
     */
    public function addField(Table_Field_Abstract $field, $index = null){
        if(is_null($index)) $index = $field->getName();             // если индекс не передан, использовать имя
        if(!strlen($index)) $index = $this->getRandomFieldIndex();  // Если имени нет, назначить случайный индекс (используется в pattern элементе)

        if(isset($this->fields[$index])){
            throw new \Exception("Сolumn with the name `{$index}` is already defined");
        }

        // устновка двойного меню
        if($this->header_group_current){
            $field->header_group = $this->header_group_current;
            $this->header_groups[$this->header_group_current]['counts']++;
        }

        $this->fields[$index] = $field;

        return $this->fields[$index];
    }

    /*******************************************************************************/

    /**
     * Текстовое поле
     *
     * @param mixed $name
     * @param mixed $title
     * @param mixed $index
     *
     * @return Table_Field_Text
     */
    public function addField_Text($name, $title = null, $index = null){
        return $this->addField(new Table_Field_Text($name, $title), $index);
    }

    public function addField_IP($name, $title = 'IP', $index = null){
        return $this->addField_Text($name, $title, $index)->addDecoratorFunction(function($value){
            return inet_ntop($value);
        });
    }

    public function addField_StateFIPS($name, $title = 'State', $index = null){
        return $this->addField_Text($name, $title, $index)->addDecoratorFunction(function($value){
            $stateCode = Geo::getStateCode($value);

            if($stateCode){
                return "<span title='" . htmlspecialchars(Geo::getStateTitle($stateCode)) . "'>{$stateCode}</span>";
            }
            else {
                return "<span style='color: #AAA'>Unknown</span>";
            }

            return null;
        });
    }

    public function addField_RURegion($name, $title = 'Region', $index = null){
        return $this->addField_Text($name, $title, $index)->addDecoratorFunction(function($value){
            $value = (new Filter_Ru_Region())->filter($value);
            if(isset(Geo::getRegions()[$value])){
                return Geo::getRegions()[$value];
            }
            else {
                return "<span style='color: #AAA'>?</span>";
            }
        });
    }


    public function addField_Region($name, $title = 'Region', $index = null){
        return $this->addField_Text($name, $title, $index)->addDecoratorFunction(function($value){
            return Geo::getStateLabelForMarket($value);
        });
    }

    /**
     * Данные по шаблону из текущего контекста. \AP\Context::pattern($pattern)
     * Доступ к переменным по их имени в фигурных скобках. Пример: "{value1} / {value2}"
     *
     * @param mixed $pattern
     * @param mixed $title
     * @param mixed $index
     * @param mixed $baseName Базовое имя, можнео использовать для autoSum
     *
     * @return Table_Field_Pattern
     */
    public function addField_Pattern($pattern, $title = '', $index = null, $baseName = null){
        return $this->addField(new Table_Field_Pattern($pattern, $title, $baseName), $index);
    }

    /**
     * @param callable $closureFunction
     * @param string $title
     * @param null $index
     * @param null $baseName
     * @return Table_Field_Function
     */
    public function addField_Function(Closure $closureFunction, $title = '', $index = null, $baseName = null){
        return $this->addField(new Table_Field_Function($closureFunction, $title, $baseName), $index);
    }

    /**
     * Поле с датой
     *
     * @param mixed $name
     * @param mixed $title
     * @param mixed $index
     *
     * @return Table_Field_Date
     */
    public function addField_Date($name, $title = null, $index = null){
        return $this->addField(new Table_Field_Date($name, $title), $index);
    }

    /**
     * Поле с кнопкой
     *
     * @param string|null $label - Label у кнопки
     * @param string|null $title - Заголовок столбца
     * @param string|null $index
     *
     * @return Table_Field_Button
     */
    public function addField_Button($label = '', $title = '', $index = null){
        return $this->addField(new Table_Field_Button($label, $title), $index);
    }

    /**
     * Декорированный список
     * К каждому ключу подбирается соответсвующее значение.
     * Ключ проходит предварительную декорацию
     *
     * Пример настрйоки списка 1 (Просто названия):
     * [
     *     1 => 'Active',
     *     2 => 'Paused',
     * ]
     *
     * Пример настрйоки списка 2 (Названия и цвета):
     * [
     *     1 => ['Active', \AP\Ui\Colors::BACKGROUND_GREEN],
     *     2 => ['Paused', \AP\Ui\Colors::BACKGROUND_RED],
     * ]
     *
     * @param string $name
     * @param string $title
     * @param array $set    Насйтрока списка
     * @param string $index
     *
     * @return Table_Field_Set
     */
    public function addField_SetArray($name, $title = null, $set = null, $index = null){
        return $this->addField(
            new Table_Field_Set($name, $title, $set),
            $index
        );
    }

    /**
     * Данные, подгружаемые из кеша
     *
     * @param Cache_PartialList_Abstract $cacheClass
     * @param string $name
     * @param string $title
     * @param string $index
     *
     * @return Table_Field_CachePartialList
     */
    public function addField_Cache($cacheClass, $name, $title = '', $index = null){
        return $this->addField(new Table_Field_CachePartialList($cacheClass, $name, $title), $index);
    }

    /***************************************************************************/

    public function addField_Avatar($emailName, $title = '', $index = null){
        return $this->addField_Text(
            $emailName,
            $title,
            $index
        )
            ->setTdCSS('padding', '1px')
            ->setTdCSS('width', '26px')
            ->addDecoratorFunction(function($value){
                return "<img " .
                "style='border-radius: 50% !important' " .
                "src='https://www.gravatar.com/avatar/" . md5(mb_strtolower(trim($value))) . "?s=26&d=mm'>";
            });
    }

    /**
     * Имя вебмастра
     *
     * @param $name
     * @param $title
     * @param $index
     * @return Table_Field_CachePartialList
     */
    public function addField_UserLogin($name, $title = 'User', $index = null){
        $el = $this->addField_Cache(
            new Cache_PartialList_UserLogin(),
            $name,
            $title,
            $index
        );

        if(UserAccess::isAdmin()){
            $el->setLink("/account/users/user/?id={" . $name . "}");
        }

        return $el;
    }

    /**
     * Длинный id товара в каталоге
     *
     * @param $name
     * @param $title
     * @param $index
     * @return Table_Field_CachePartialList
     */
    public function addField_CatalogItemLongID($name, $title = 'tbl_item', $index = null){
        $el = $this->addField_Cache(
            new Cache_PartialList_CatalogItemLongID(),
            $name,
            $title,
            $index
        )
            ->addDecoratorFunction(function($v){
                return "<a href='/account/catalog/item/?id={$v}'>{$v}</a>";
            });

        return $el;
    }

    public function addField_CatalogType($name, $title = 'tbl_item', $index = 'catalogType'){
        $el = $this->addField_Cache(
            new Cache_PartialList_CatalogItemType(),
            $name,
            $title,
            $index
        )
            ->addDecoratorFunction(function($v){
                if($v){
                    try{
                        return Catalog_Types::getTypeInfo($v)['name'];
                    }
                    catch(Exception $e){}
                }
            });

        return $el;
    }

    /**
     * Миниатюрка
     *
     * @param $emailName
     * @param string $title
     * @param null $index
     * @return Table_Field_Abstract
     */
    public function addField_Thumbnail($emailName, $title = '', $index = null){
        return $this->addField_Text(
            $emailName,
            $title,
            $index
        )
            ->setTdCSS('padding', '1px')
            ->setTdCSS('width', '100px')
            ->addDecoratorFunction(function($value){
                if (empty($value)) {
                    return '';
                } else {
                    return "<img " .
                    "style='max-width:100% !important' " .
                    "src='" . trim($value) . "' alt=''>";
                }
            });
    }

    /**
     * @param $name
     * @param string $title
     * @param null $index
     * @return Table_Field_CachePartialList
     */
    public function addField_Timezone($name, $title = '', $index = null){
        return $this->addField_Cache(
            new Cache_PartialList_Timezone(),
            $name,
            $title,
            $index
        );
    }



    /************************************************************************************************************
     * Footer
     ****************************************************/

    /**
     * Добавить footer в виде массива, ключ - значение
     *
     * @param array $data
     * @return Table
     */
    public function addFooter($data){
        $this->footer[] = $data;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setDetailsField($name){
        $this->details_field = $name;
        return $this;
    }


    /************************************************************************/

    public function disabledHover(){
        $this->hover = false;
    }

    public function disabledStriped(){
        $this->striped = false;
    }

    /************************************************************************************************************
     *  Отрисовка
     ****************************************************/

    protected function renderTitle(){
        $r = '';
        if(strlen((string)$this->logo)){
            $r.= "<img src='" . $this->logo . "' class='ap_table_logo'>";
        }

        if(strlen($this->title)){
            $r.= "<div class='ap_table_title'>" . $this->title . "</div>";
        }
        return $r;
    }

    protected function renderPaging(){
        if($this->paging instanceof Table_Paging){
            return $this->paging->render();
        }
        return "";
    }

    protected function getFooter(){
        if(count($this->fields) && count($this->data)){
            $autoFooter = [];

            foreach($this->fields as $index => $field){
                if($field->isFooterAutoSum()){
                    $autoFooter[$index] = 0;
                    foreach($this->data as $el){
                        $autoFooter[$index]+= isset($el[$field->getName()]) ? $el[$field->getName()] : 0;
                    }
                    $autoFooter[$index] = round($autoFooter[$index], 10);
                    if($autoFooter[$index] == -0) $autoFooter[$index] = 0;
                }
            }

            // Если есть поля с автоматичеким подсчетом суммы, добавить футер с их значениями
            if(count($autoFooter)){
                $this->footer['auto'] = $autoFooter;
            }
        }

        return $this->footer;
    }

    protected function renderButtons(){
        $r = '';
        if(count($this->buttons)){
            $r.= "<div class='ap_table_btns_panel'>";
            foreach($this->buttons as $button){
                $r.= $button->render();
            }
            $r.= "</div>";
        }
        return $r;
    }

    static protected $uniqueInt = 0;

    static public function getUniqueIntToPage(){
        return self::$uniqueInt++;
    }

    /**
     * Получить id таблицы
     *
     * @return string
     */
    public function getTableID(){
        return $this->id;
    }

    public function render(){
        // автопейджинг - предобразование массива или запроса к базе данных
        if($this->is_paging == self::PAGING_AUTO && is_numeric($this->paging) && $this->paging){
            $this->paging = (new Table_Paging())
                ->setPageLen($this->paging)
                ->setGetValueName($this->pagingGetValueName)
                ->setShowOnePageCounts($this->paging_show_one_page_counts);

            $this->data = $this->paging->getLimitData($this->data);
        }

        // Если дата это объект запрооса, получаем из него дату
        if($this->data instanceof Db_Statment_Select){
            $this->data = $this->data->fetchAll();
        }

        // Если отображение не настроено, настроить его по умолчанию
        if(!count($this->fields)) $this->autoFields();

        ob_start();

        echo $this->renderTitle();
        echo $this->renderPaging();
        echo $this->renderButtons();

        // Первичная обработка данных и формирование кешей
        if(count($this->data) && count($this->fields)){
            foreach($this->fields as $el){
                $el->preLoad($this->data);
            }
        }

        // сортировка
        $i = 0;
        $sort = array();
        $is_sort = false;

        foreach($this->fields as $el){
            // настройка сортировок
            if($el->getSort()){
                $is_sort = true;

                $sort[$i] = array(
                    'sorter' => $el->getSort(),
                );
            }
            else {
                $sort[$i] = array(
                    'sorter' => false,
                );
            }

            $i++;
        }

        // Сортировки
        /*
        if($is_sort){
            \AP::site()->addJS("jquery/plugin/tablesorter.js");
            \AP::site()->addCSS("table/tablesorter.css");

            $sort = array(
                'sortMultiSortKey' => 'shiftKey',
                'cancelSelection'   => true,
                'headers'           => $sort,
            );

            // сортировка по умолчанию
            if(count($this->sort_default)){
                $sort_default = array();

                foreach($this->sort_default as $s){
                    $i = 0;
                    foreach($this->fields as $index => $el){
                        if($s[0] == $index){
                            $sort_default[] = array($i, $s[1]);
                            break;
                        }
                        $i++;
                    }
                }

                if(count($sort_default)){
                    $sort['sortList'] = $sort_default;
                }
            }

            \AP::site()->addJSCode("jQuery('.ap_table').tablesorter(" . json_encode($sort) . ");");
        }
        */

        // ренден таблицы
        include dirname(__FILE__) . "/Table/Templates/table_main.phtml";

        // Если данных много, под такблицей будет отображаться еще один пейджинг
        if(count($this->data) > 19){
            echo $this->renderPaging();
        }

        $res = ob_get_clean();

        if($this->is_hide_print){
            $res = "<div class='hidden-print'>{$res}</div>";
        }

        return $res;
    }

    public function __toString(){
        return (string)$this->render();
    }
}
