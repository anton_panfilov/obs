<?php

class Html_Attribs {
    protected $attribs = array();
    protected $css = array();

    /**
     * Задать значения всех атрибутов, одним массивом
     *
     * @param array $attribsArray
     * @return self
     */
    public function setAttribs($attribsArray){
        if(is_array($attribsArray) && count($attribsArray)){
            foreach($attribsArray as $k => $v){
                $this->attribs[mb_strtolower($k)] = $v;
            }
        }

        return $this;
    }

    /**
     * Получить атрибут
     *
     * @param string $name
     * @return string
     */
    public function getAttrib($name){
        $name = mb_strtolower($name);

        // TODO: перенести сюда автогенерируемые значения типа style из массива $this->css
        return isset($this->attribs[$name]) ? $this->attribs[$name] : "";
    }

    /**
     * Задать значение атрибута
     * Или удалить его, если занчение = NULL
     *
     * @param $name
     * @param null $value
     * @return self
     */
    public function setAttrib($name, $value = null){
        $name = mb_strtolower($name);

        if(is_null($value)){
            if(isset($this->attribs[$name])){
                unset($this->attribs[$name]);
            }
        }
        else {
            $this->attribs[$name] = $value;
        }
        return $this;
    }

    /**
     * Устанвоить CSS, все переданные значения добавятся к свойству style во время отрисовки
     *
     * @param $name
     * @param null $value
     * @return self
     */
    public function setCSS($name, $value = null){
        $name = mb_strtolower($name);

        if(is_null($value)){
            if(isset($this->css[$name])){
                unset($this->css[$name]);
            }
        }
        else {
            $this->css[$name] = (string)$value;
        }
        return $this;
    }

    /**
     * Добавить текст в конец атрибута
     *
     * @param $name
     * @param $value
     * @return self
     */
    public function addAttribPost($name, $value){
        $name = mb_strtolower($name);

        if(!isset($this->attribs[$name])) $this->attribs[$name] = "";
        $this->attribs[$name] = $this->attribs[$name] . $value;
        return $this;
    }

    /**
     * Добавить текст в начало атрибута
     *
     * @param string $name
     * @param string $value
     * @return self
     */
    public function addAttribPre($name, $value){
        $name = mb_strtolower($name);

        if(!isset($this->attribs[$name])) $this->attribs[$name] = "";
        $this->attribs[$name] = $value . $this->attribs[$name];
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(){
        return (string)$this->render();
    }

    /**
     * Получить строку свойств для HTML элемента
     *
     * @return string строка параметров для html тега
     */
    public function render(){
        $els = array();

        $attribs = $this->attribs;

        if(count($this->css)){
            if(!isset($attribs['style'])) $attribs['style'] = "";
            $add = "";
            foreach($this->css as $k => $v){
                $add.= "{$k}: {$v};";
            }
            $attribs['style'] = $add . $attribs['style'];
        }

        if(count($attribs)){
            foreach($attribs as $k => $v){
                if($k == 'value')   $els[] = $k.'="' . htmlspecialchars((string)$v) . '"';
                else                $els[] = $k.'="' . str_replace(
                    // пробовал по другому, ну конектно работает только изменение ковычки (") на (&quot); (В JS в том числе)
                    array('"'),
                    array("&quot;"),
                    (string)$v
                ) . '"';
            }
        }

        if(count($els)){
            return " " . implode(" ", $els) . " ";
        }

        return " ";
    }
}