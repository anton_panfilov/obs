<?php

/**
* Проверка присутсвия значения в массиве значений
*/

class Form_Validator_InArray extends Form_Validator_Abstract {
    protected $array;

    public function __construct(array $array, $message = null){
        $this->array = $array;
        if($message){
            $this->_messages[0] = $message;
        }
    }

    protected $_messages = array(
        'Invalid value',
    );

    public function isValid($value){
        if(!in_array($value, $this->array)){
            $this->_error();
            return false;
        }
        return true;
    }
}