<?php

class Form_Validator_UniqueInDatabase extends Form_Validator_Abstract {
    protected $db;
    protected $query;
    
    public function __construct($query, $database = null, $errorMessage = null){
        $this->db    = $database;
        $this->query = $query;
        
        if($errorMessage){
            $this->_messages[0] = $errorMessage;
        }
    } 
    
    protected $_messages = array(
        'Already in use',
    );     
    
    public function isValid($value){                                          
        if($this->db instanceof Db_Connection_Abstract){
            $db = $this->db;
        }
        else {
            $db = Db::site();
        }

        if($db->fetchOne($this->query, $value) !== false){
            $this->_error(0, [
                'value' => $value,
            ]);
            return false;
        }
        return true;
    }   
}