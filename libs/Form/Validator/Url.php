<?php

class Form_Validator_Url extends Form_Validator_Abstract {

    const ERROR_SCHEME              = 0;
    const ERROR_INVALID_HOST        = 1;
    const ERROR_HOST_2LEVEL         = 2;
    const ERROR_HOST_BLOCK          = 3;
    const ERROR_MACROS_NOT_FOUND    = 4;

    protected $required_macroses = [];

    public function __construct($required_macroses = [], $allow_self_domain = false){

        $this->allow_self_domain = $allow_self_domain;

        if(is_string($required_macroses)){
            $required_macroses = [$required_macroses];
        }

        if(is_array($required_macroses) && count($required_macroses)){
            foreach($this->required_macroses as $el){
                if(!(is_string($el) || is_numeric($el)) || !strlen($el)){
                    throw new Exception("Invalid macroses");
                }
            }
            $this->required_macroses = $required_macroses;
        }
    } 
    
    protected $_messages = array(
        self::ERROR_SCHEME              => 'Scheme is require. `http` or `https`',
        self::ERROR_INVALID_HOST        => 'Invalid host',
        self::ERROR_HOST_2LEVEL         => 'You can not use the first level domain',
        self::ERROR_HOST_BLOCK          => 'You can not send postbacks to this domain',
        self::ERROR_MACROS_NOT_FOUND    => 'Macros `{macros}` is required',
    );
    
    public function isValid($value){
        $url = parse_url($value);

        if(!isset($url['scheme']) || !in_array($url['scheme'], ['http', 'https'])){
            $this->_error(self::ERROR_SCHEME);
            return false;
        }

        $validHost = true;

        if(!isset($url['host']) || !(new Form_Validator_Hostname())->isValid($url['host'])){
            $this->_error(self::ERROR_INVALID_HOST);
            return false;
        }

        $host = explode(".", $url['host']);

        if(count($host) == 1){
            $this->_error(self::ERROR_HOST_2LEVEL);
            return false;
        }

        $host2 = $host[count($host) - 2] . "." . $host[count($host) - 1];
        if(
            !$this->allow_self_domain && ($host2 == Conf::main()->domain || $host2 == 't3leads.com')
        ){
            $this->_error(self::ERROR_HOST_BLOCK);
            return false;
        }


        $query_string = isset($url['path']) ? $url['path'] : "/";
        if(isset($url['query'])){
            $query_string.= "?{$url['query']}";
        }
        if(count($this->required_macroses)){
            foreach($this->required_macroses as $el){
                $m = "{" . $el . "}";
                if(strpos($query_string, $m) === false){
                    $this->_error(self::ERROR_MACROS_NOT_FOUND, [
                        'macros' => $m
                    ]);
                    return false;
                }
            }
        }



        /*
        if(!preg_match($this->pattern, $value)){
            $this->_error();
            return false;
        }
        */
        return true;
    }   
}