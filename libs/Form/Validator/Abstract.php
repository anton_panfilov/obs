<?php

abstract class Form_Validator_Abstract {
    protected $_errors = array();
    protected $_messages = array();
    
    /**
    * @var Form
    */
    protected $_form;

    /**
     * @param null $key
     * @param array $params
     * @return bool
     */
    protected function _error($key = null, $params = array()){
        if(count($this->_messages)){
            if(is_null($key)){
                list($key) = array_keys($this->_messages);   
            }
            
            if(isset($this->_messages[$key])){
                $message = Translate::t($this->_messages[$key], $params);
                
                if(is_array($params) && count($params)){
                    foreach($params as $k => $v){
                        $message = str_replace("{" . $k . "}", $v, $message);    
                    }
                }  
                
                $this->_errors[] = $message;
                return false;
            } 
        }
        
        $this->_errors[] = 'Unknown Error: ' . get_called_class();
        return false;
    }
    
    /**
    * Получить причины, по которым значение не прошло валидацию
    * 
    */
    public function getErrors(){
        return $this->_errors;
    }
    
    abstract function isValid($value);
    
    public function setForm($form){
        $this->_form = $form;
        return $this;
    }
}