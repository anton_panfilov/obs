<?php

class Form_Validator_NewLogin extends Form_Validator_Abstract {
    public function __construct(){
        Translate::addDictionary('user/login');
    } 
    
    protected $_messages = array(
        /*
        1 => "Invalid username length, a minimum of 4",
        2 => "Invalid username length, a maximum of 32",
        3 => "Please use only letters (a-z), numbers, and dots",
        4 => "The first character of your username should be a letter (a-z) or number",
        5 => "The last character of your username should be a letter (a-z) or number",
        6 => "Alas, username can't have consecutive dots",
        7 => "This username is already registered",
        */
        1 => "loginError_length4",
        2 => "loginError_length32",
        3 => "loginError_symbs",
        4 => "loginError_firstSymb",
        5 => "loginError_lastSymb",
        6 => "loginError_dots",
        7 => "loginError_alreadyRegistered",
    );     
    
    public function isValid($value){
        $value = trim($value);

        if(strlen($value) < 4)                                      return $this->_error(1);
        if(strlen($value) > 32)                                     return $this->_error(2);
        if(!preg_match("/^[a-z0-9\\.]*$/i", $value))                return $this->_error(3);
        if(!preg_match("/[a-z0-9]/i", $value[0]))                   return $this->_error(4);
        if(!preg_match("/[a-z0-9]/i", $value[strlen($value) - 1]))  return $this->_error(5);
        if(strpos($value, "..") !== false)                          return $this->_error(6);

        if(Db::site()->fetchOne(
            "select `id` from `users` where `login`=? limit 1", $value
        )){
            return $this->_error(7);
        }


        return true;
    }   
}