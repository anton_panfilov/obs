<?php

class Form_Validator_Regexp extends Form_Validator_Abstract {
    protected $pattern;
    
    public function __construct($pattern, $errorMessage = null){
        $this->pattern = $pattern;
        if($errorMessage){
            $this->_messages[0] = $errorMessage;
        }
    } 
    
    protected $_messages = array(
        'Invalid value',
    );     
    
    public function isValid($value){
        if(!preg_match($this->pattern, $value)){
            $this->_error();
            return false;
        }
        return true;
    }   
}