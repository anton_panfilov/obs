<?php

class Form_Validator_BankBik extends Form_Validator_Abstract {
    protected $_messages = array(
        0 => 'БИК не найден',
        1 => 'К сожалению у этого банка не известен кор. счет',
    );
    
    public function isValid($value){
        if(strlen($value) == 9) {
            $result = Json::decode(file_get_contents("http://json-service.com/api/banks/bik-info?bik=" . urlencode($value)));
            if(isset($result['response']['bik'])) {
                if($result['response']['bik'] == '') {
                    return $this->_error(0);
                }
                else if($result['response']['ks'] == ''){
                    return $this->_error(1);
                }
            }
        }

        return true;
    }   
}