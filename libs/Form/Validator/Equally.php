<?php

class Form_Validator_Equally extends Form_Validator_Abstract {
    protected $value;

    public function __construct($value, $message = null){
        $this->value = $value;
        if($message){
            $this->_messages[0] = $message;
        }
    }

    protected $_messages = array(
        'Invalid value',
    );

    public function isValid($value){
        if($value != $this->value){
            $this->_error();
            return false;
        }
        return true;
    }
}