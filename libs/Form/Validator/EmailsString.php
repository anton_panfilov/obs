<?php

/**
* Валидатор по Email очень простой, потому что лучше пропустить лишнее, чем не отфильтровать важное
*/

class Form_Validator_EmailsString extends Form_Validator_Abstract {
    protected $_messages = array(
        'Invalid emails string',
    );

    public function isValid($value){
        $res = Email_Functions::parseEmailString($value);

        if(!count($res)){
            $this->_error();
            return false;
        }

        return true;
    }
}