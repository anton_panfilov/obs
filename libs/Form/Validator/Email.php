<?php

/**
* Валидатор по Email очень простой, потому что лучше пропустить лишнее, чем не отфильтровать важное
*/

class Form_Validator_Email extends Form_Validator_Regexp {
    public function __construct($errorMessage = null){
        // значнеие по умолчанию
        $this->_messages[0] = 'Invalid Email';

        // инизилизация регулярки
        parent::__construct('/^(?:\S+)@[a-z0-9][-a-z0-9]{0,61}(?:\.[a-z]{1,10})*$/i', $errorMessage);
    }
}