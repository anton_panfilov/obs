<?php

class Form_Validator_Phone extends Form_Validator_Abstract {
    protected $_messages = array(
        0 => 'Please enter a valid country code',
        1 => 'Please enter a valid phone number',
    );
    
    public function isValid($value){
        if(strpos($value, ".") === false){
            $this->_error(0);
            return false;
        }
        else if(strlen($value) < 7){
            $this->_error(1);
            return false;
        }
        return true;
    }   
}