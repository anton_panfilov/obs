<?php

class Form_Validator_Capcha extends Form_Validator_Abstract {
    protected $name;
    
    public function __construct($name){
        $this->name = $name;
    } 
    
    protected $_messages = array(
        'Символы введены неправильно, попробуйте еще раз',
    );     
    
    public function isValid($value){
        if(!strlen($value) || !Captcha::isValid($this->name, $value)){
            $this->_error();
            return false;
        }
        return true;
    }   
}