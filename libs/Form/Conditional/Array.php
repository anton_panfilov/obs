<?php

class Form_Conditional_Array extends Form_Conditional_Abstract {
    const VALUES = 0;
    const ELEMENTS = 1;

    protected $settings = [];

    /**
     * Во время первого вызова функции run или получения js конфига
     * должна пройти иницилизация настроек, во время которой:
     * - если в зависимости добавленно мультиполе, то зависимости распространяются на все поля, входщие в него
     * - и т. д.
     * Иницилизация долна проходить один раз, эта переменная показывает проходилоа она или нет.
     *
     * @var bool
     */
    protected $init = false;

    protected function init_settings(){
        if(!$this->init){
            $this->init = true;

            if(count($this->settings)){
                foreach($this->settings as $k => $v){
                    // мультиэлемент, подтягивает за собой все элементы внутри себя
                    $elements_new = [];
                    if(count($v[self::ELEMENTS])){
                        foreach($v[self::ELEMENTS] as $el){
                            $elements_new[] = $el;

                            if($this->form->getElement($el) instanceof Form_Element_MultiElement){
                                foreach($this->form->getElement($el)->getMultiElements() as $mel){
                                    $elements_new[] = $mel;
                                }
                            }
                        }

                        $this->settings[$k][self::ELEMENTS] = array_unique($elements_new);
                    }
                }
            }
        }
    }

    /*************************************************************
     * JavaScript
     */

    /**
     * Добавление JS кода
     */
    protected function getJavaScriptSettings(){
        $settings = $this->settings;

        if(count($settings)){
            foreach($settings as $k1 => $v1){
                if(count($settings[$k1][self::ELEMENTS])){
                    foreach($settings[$k1][self::ELEMENTS] as $k2 => $v2){
                        $settings[$k1][self::ELEMENTS][$k2] = str_replace(".", '\\.', $v2);
                    }
                }
            }
        }



        return [
            'show'      => !$this->form->getElement($this->elementName)->isConditionHide(),
            'settings'  => $settings,
            'get'       => new Json_Expr(
                "function(){return " . $this->form->getElement($this->elementName)->javaScriptFunction_GetValue() . ";}"
            ),
            'run'       => new Json_Expr(
                "function(){document.ap_form_conditionals.runArray(this.settings, this.get(), this.show);}"
             ),
        ];
    }

    /************************************************************/

    /**
     * Получить все поля, которые могут быть скрыты от зависимостей этого поля
     *
     * @return array
     */
    public function getAllConditionalFields(){
        $res = [];

        if(count($this->settings)){
            foreach($this->settings as $el){
                if(is_array($el[self::ELEMENTS]) && count($el[self::ELEMENTS])){
                    $res = array_merge($res, $el[self::ELEMENTS]);
                }
            }

            if(count($res)){
                $res = array_unique($res);
            }
        }

        return $res;
    }

    /**
     * применить зависимости
     */
    public function run(){
        $this->init_settings();

        /**
         * 1. Если это поле не используется или скрыто зависимостями, то все его зависимые поля тоже скрытые!
         * 2. Поиск всех открытых полей
         */
        $element = $this->form->getElement($this->elementName);

        $show = [];

        // Если поле используеся, не скрыто зависимосятми или в нем нет ошибок, то обрабатываются его зависимости
        if($element->getUse() && !$element->isConditionHide() && !$element->isErrors()){
            // само поле активно
            if(count($this->settings)){
                foreach($this->settings as $el){
                    if(in_array($element->getValue(), $el[self::VALUES])){
                        $show = array_merge($show, $el[self::ELEMENTS]);
                    }
                }

                if(count($show)){
                    $show = array_unique($show);

                    // открыть все поля, которые открываются зависимостями
                    foreach($show as $el){
                        $this->form->getElement($el)->setConditionalHide(false);
                    }
                }


            }
        }

        // скрыть поля, которые скрываются
        $all = $this->getAllConditionalFields();
        if(count($all)){
            foreach($all as $el){
                if(!in_array($el, $show)){
                    $this->form->getElement($el)->setConditionalHide(true);
                }
            }
        }


    }

    /************************************************************/

    public function addCondition($values, $elements){
        if(!is_array($values))   $values   = [$values];
        if(!is_array($elements)) $elements = [$elements];

        $this->settings[] = [
            self::VALUES => $values,
            self::ELEMENTS => $elements
        ];
    }

}