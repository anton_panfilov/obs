<?php

abstract class Form_Conditional_Abstract {
    /**
     * @var Form
     */
    protected $form;

    /**
     * @var string
     */
    protected $elementName;

    public function __construct(Form $form, $elementName){
        $this->form = $form;
        $this->elementName = $elementName;
    }

    /**
     * Добавление в форму настроек
     */
    final public function addJavaScriptCode(){
        Site::addJS("form/conditionals.js");
        $this->getJavaScriptSettings();
        
        Site::addJSCode("document.ap_form_conditionals.addConditional('" .
            str_replace(".", '\\.', $this->elementName) .
        "', " .
            Json::encode( $this->getJavaScriptSettings() ) .
        ");");
    }

    /*************************************************/

    /**
     * @return array Массив полей, которые могут быть скрыты зависимостями этого поля
     */
    abstract public function getAllConditionalFields();

    /**
     * Получение настроек для javascript conditional
     */
    abstract protected function getJavaScriptSettings();

    abstract public function run();
}