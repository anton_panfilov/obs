<?php

class Form_Conditional_NotArray extends Form_Conditional_Array {
    /**
     * Добавление JS кода
     */
    protected function getJavaScriptSettings(){
        return [
            'show'      => !$this->form->getElement($this->elementName)->isConditionHide(),
            'settings'  => $this->settings,
            'get'       => new Json_Expr(
                "function(){return " . $this->form->getElement($this->elementName)->javaScriptFunction_GetValue() . ";}"
            ),
            'run'       => new Json_Expr(
                "function(){document.ap_form_conditionals.runNotArray(this.settings, this.get(), this.show);}"
             ),
        ];
    }

    /**
     * применить зависимости
     */
    public function run(){
        $this->init_settings();

        /**
         * 1. Если это поле не используется или скрыто зависимостями, то все его зависимые поля тоже скрытые!
         * 2. Поиск всех открытых полей
         */
        $element = $this->form->getElement($this->elementName);

        $show = [];

        // Если поле используеся, не скрыто зависимосятми или в нем нет ошибок, то обрабатываются его зависимости
        if($element->getUse() && !$element->isConditionHide() && !$element->isErrors()){
            // само поле активно
            if(count($this->settings)){
                foreach($this->settings as $el){
                    if(!in_array($element->getValue(), Arrays::executeClosure($el[self::VALUES]))){
                        $show = array_merge($show, $el[self::ELEMENTS]);
                    }
                }

                if(count($show)){
                    $show = array_unique($show);

                    // открыть все поля, которые открываются зависимостями
                    foreach($show as $el){
                        $this->form->getElement($el)->setConditionalHide(false);
                    }
                }


            }
        }

        // скрыть поля, которые скрываются
        $all = $this->getAllConditionalFields();
        if(count($all)){
            foreach($all as $el){
                if(!in_array($el, $show)){
                    $this->form->getElement($el)->setConditionalHide(true);
                }
            }
        }


    }
}