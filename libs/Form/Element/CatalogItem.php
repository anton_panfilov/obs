<?php

class Form_Element_CatalogItem extends Form_Element_AutoComplite {

    protected $onlyActive = false;

    /**
     * @return $this
     */
    public function setOnlyActive(){
        $this->onlyActive = true;
        return $this;
    }

    protected function filterTitle($title){
        return String_Filter::cutByWord($title, 80);
    }

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::site()->select("catalog_items", ['id', 'long_id', 'title', 'price'])
            ->order("title")
            ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`long_id` rlike ? or `title` rlike ?", [$stringRegexp, $stringRegexp]);
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $el['title'] = $this->filterTitle($el['title']);

                $result[$el['id']] = "{$el['long_id']}: {$el['title']} ({$el['price']})";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        $select = Db::site()->select("catalog_items", ['id'])
            ->where("`id`=?", (int)$value)
            ->limit(1);

        return $select->fetchOne();
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            $select = Db::site()->select("catalog_items", ['id', 'long_id', 'title', 'price'])
                ->where("id=?", (int)$this->getValue());

            if($el = $select->fetchRow()){
                $el['title'] = $this->filterTitle($el['title']);
                return "{$el['long_id']}: {$el['title']} ({$el['price']})";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
