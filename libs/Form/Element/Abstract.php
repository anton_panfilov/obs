<?php

abstract class Form_Element_Abstract {
    protected $hover = true;

    /**
    * Тип отображения в форме
    *
    * @var int
    */
    protected $renderType = Form_Template_Default::ElementRenderType_Default;

    /**
    * Массив ошибок
    *
    * null - нет ошибок
    * array() - есть ошибки, элементы массива это причины ошибок
    *
    * @var mixed
    */
    protected $errors = null;

    /**
     * Показатель того что объект содержит в себе переменные
     *
     * @var bool
     */
    protected $isValue = true;

    /**
     * Рендерить или не рендерить блок с формой стандартным способом
     * (элемент может учавтсввать в multi element или в другом типе рендера,
     * при этом стандартно он рендериться не будет)
     *
     * @var bool
     */
    protected $isRenderStandard = true;

    /**
     * Показатель того что объект:
     * - рендериться;
     * - учавствует в валидации формы;
     * - присутсвует в конечном массиве FORM->getValues().
     *
     * При определенных уловиях некоторые объекты могут не использоваться, при этом они как бы не сужествуют
     * Например капча, которая проверяется 1 раз для 1 заполнения, если ввести её правильно, то до полного завершения формы
     * она больше не будет показываться (существовать)
     *
     * @var bool
     */
    protected $isUse = true;

    /**
     * Спрятан ли элемент по причине зависимости от другого
     *
     * При этом оно:
     *  - не учавствует в валидации
     *  - рендериться скрытым или не активным
     *  - не отображается в массиве Form->getValues()
     *
     * Но при этом оно может быть открыто, если в поле, которое влияет на его скрытость,
     * будет выбранно соответсвуещее этому полю значение.
     *
     * @var bool | null     null  - не зависит от других
     *                      false - зависит, ну надо показать
     *                      true  - зависит и скрыть
     */
    public $isConditionHide = null;

    protected $name;
    protected $label;
    protected $comment;
    protected $value;
    protected $required = true;

    /**
     * @var Form_Filter_Abstract[]
     */
    protected $filters = [];

    /**
     * @var Form_Validator_Abstract[]
     */
    protected $validators = [];

    /**
     * @var Form_Filter_Abstract[]
     */
    protected $renderFilters = [];

    /**
    * Объект формы, в котрой находистя элемент
    * Этого объекта может и не быть, если элемент создан не в форме
    *
    * @var Form
    */
    protected $form;

    /**
    * Имя блока, к которому привязан элемент
    *
    * @var mixed
    */
    protected $block;

    /**
    * Иницилизация объекта
    */
    protected function init(){}


    protected $lateInitComplite = false;
    protected function lateInitRunFunction(){
        if(!$this->lateInitComplite) $this->lateInit();
    }

    /**
     * Поздяння загрузка, происходит перед проверокй переменных или перед отрисовкой
     * Может не работать для единичного использования, т.к. отлов стоит в getUse()
     */
    protected function lateInit(){}

    /**
    * Форма загруженна без ошибок
    */
    public function formValidationComplite(){}

    /**
     * @param string $name
     * @param null|string $label
     */
    public function __construct($name, $label = null){
        $this->name = $name;
        $this->setLabel($label);

        $this->init();
    }

    /*********************************************************************************************************************
    * Фильтры и валидаторы
    */

    /**
    * Установить обязательность поля
    * (По умолчанию поля обязательные)
    *
    * @param bool $required
    * @return self
    */
    public function setRequired($required = true){
        $this->required = (bool)$required;
        return $this;
    }

    public function disabledHover(){
        $this->hover = false;
        return $this;
    }

    /**
     * Добавление фильтра который фильтрует данные при получении
     *
     * @param Form_Filter_Abstract $filter
     * @return self
     */
    public function addFilter(Form_Filter_Abstract $filter){
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * Добавление фильтра, который преобразует данные перед выдачей
     *
     * @param Form_Filter_Abstract $filter
     * @return self
     */
    public function addRenderFilter(Form_Filter_Abstract $filter){
        $this->renderFilters[] = $filter;
        return $this;
    }

    /**
     * Добавление валидатора
     *
     * @param Form_Validator_Abstract $validator
     * @param string|null $index Индекс валидатора, если не передавать, то будет присовин индекс по порядку
     * @param bool $duplicateException При явном присвоении индекса, эта пемеменная влияет на поведение,
     *                                 если валидатор с таким индексом уже существует:
     *                                 true - будет вызванно исклюбчение
     *                                 false - валидатор будет перезаписан
     *
     * @return $this
     * @throws Exception
     */
    public function addValidator(Form_Validator_Abstract $validator, $index = null, $duplicateException = false){
        $validator->setForm($this->form);
        if($index){
            if($duplicateException && isset($this->validators[$index])){
                throw new Exception("Duplicate Validator `{$index}`");
            }

            $this->validators[$index] = $validator;
        }
        else {
            $this->validators[] = $validator;
        }
        return $this;
    }

    /*********************************************************************************************************************
    * Получение и проверка данных
    */

    /**
     * Установить флаг, включения, отключения поля
     *
     * @param bool $flag
     * @return Form_Element_Abstract
     */
    public function setUse($flag){
        $this->isUse = (bool)$flag;
        return $this;
    }

    /**
     * @param bool $flag Показатель, скрыто ли поле, в зависиомсти от значения другого
     *                   При этом оно:
     *                    - не учавствует в валидации
     *                    - рендериться скрытым или не активным
     *                    - не отображается в массиве Form->getValues()
     *
     *                   Но при этом оно может быть открыто, если в поле, которое влияет на его скрытость,
     *                   будет выбранно соответсвуещее этому полю значение.
     * @return self
     */
    public function setConditionalHide($flag = true){
        $this->isConditionHide = (bool)$flag;
        return $this;
    }

    /**
     * Установить флаг, включения, отключения стандатного рендера поля
     *
     * @param bool $flag
     * @return Form_Element_Abstract
     */
    public function setRenderStandart($flag){
        $this->isRenderStandard = (bool)$flag;
        return $this;
    }

    /**
     * @param string|null $message
     * @return $this
     */
    public function addError($message = null){
        if(!is_array($this->errors))    $this->errors = array();
        if(!is_null($message))          $this->errors[] = Translate::t($message);
        return $this;
    }

    /**
     * Получение данных
     *
     * @param array $params
     * @param bool $notErrors
     */
    protected function runGetData($params, $notErrors){
        $this->value = isset($params[$this->name]) ? $params[$this->name] : $this->value;

        if($this->required && !strlen($this->value) && !$notErrors){
            $this->addError($this->getTextIsRequired());
        }
    }

    /**
     * Фильтрация данных
     */
    protected function runFilteredData(){
        if(count($this->filters)){
            foreach($this->filters as $filter){
                $this->value = $filter->filter($this->value);
            }
        }
    }

    /**
     * Проверка поля на ошибки
     *
     * @param bool $notErrors
     * @param bool $firstValue
     * @return boolean
     */
    protected function runValidatedData($notErrors, $firstValue){
        $result = true;

        if(count($this->validators)){
            foreach($this->validators as $validator){
                if(!$validator->isValid($this->value)){
                    if(is_array($validator->getErrors()) && count($validator->getErrors())){
                        foreach($validator->getErrors() as $error){
                            $result = false;
                            $this->addError($error);
                        }
                    }
                    else {
                        $result = false;
                        $this->addError();
                    }
                }
            }
        }

        if($notErrors && !$result){
            $this->errors = null;
            $this->value = $firstValue;
            $result = true;
        }

        if($result){
            $this->formValidationComplite();
        }

        return $result;
    }

    /**
     * @return bool Показывает есть ли в объекте переменные
     */
    public function isValue(){
        return $this->isValue;
    }

    /**
     * @return bool
     */
    public function isHover(){
        return $this->hover;
    }

    /**
     * @return bool Рендериться ли элемент стандартным способом
     */
    public function isRenderStandard(){
        return $this->isRenderStandard;
    }

    /**
     * Проврека элемента на ошибки
     *
     * @param array $params
     * @param bool $notErrors Режим, при котором при нахождении ошибки она не показывается, а показывается значение по умолчанию
     * @return boolean
     */
    public function isValid($params, $notErrors = false){
        if($this->isValue && $this->getUse() && !$this->isConditionHide()){
            /**
            * 1. получение данных
            * 2. фильтры
            * 3. валидаторы
            */
            $firstValue = $this->value;

            $this->runGetData($params, $notErrors);

            // Если значение не обязательное и там только один пробелы, то преобразовать значение в пустую строку
            if(!$this->required && is_string($this->value) && !strlen($temp = trim($this->value))){
                $this->value = $temp;
            }

            if(is_null($this->errors)){
                if(
                    $this->required ||
                    (is_string($this->value) && strlen($this->value)) ||
                    (is_array($this->value) && count($this->value))
                ){
                    // Если у поля нет ошибок и оно обязательное или не пустое
                    $this->runFilteredData();

                    // Валидатор может срабоать не на эту переменную, то гда что делать???
                    return $this->runValidatedData($notErrors, $firstValue);
                }
                else if(!$this->required){
                    // не обязательное поле, с пустым значением
                    return true;
                }
            }

            return false;
        }

        // поле не содержит переменных
        return true;

    }

    /*********************************************************************************************************************
    * Тексты ошибок
    */

    protected function getTextIsRequired(){
        return "This field is required";
    }

    protected function getTextUnknownError(){
        return "Неизвестная ошибка";
    }

    /*********************************************************************************************************************
    * Получение и установка некоторых переменных
    */

    public function getName(){
        return $this->name;
    }

    /**
     * @param $comment
     * @return $this
     */
    public function setComment($comment){
        $this->comment = Translate::t($comment);
        return $this;
    }

    public function getComment(){
        if($this->comment instanceof Closure){
            $func = $this->comment;
            return $func(Context::get($this->name), Context::getArray());
        }

        return $this->comment;
    }

    /**
     * @param $label
     * @return $this
     */
    public function setLabel($label){
        $this->label = Translate::t($label);
        return $this;
    }

    /**
     *
     * @return null|string
     */
    public function getLabel(){
        $label = $this->label;

        if($this->form->getMarkRequiredFields()){
            if($this->required) {
                $label = "<b>{$label}</b> <span class='txt-color-redLight'>*</span>";
            }
            else {
                $label = "<span style='color: #777'>{$label}</span>";
            }
        }

        return $label;
    }

    /**
     * NOTE: Есть классы, которые полностью переопределют этот метод, не вызывая родительский
     *
     * @param $value
     * @return $this
     */
    public function setValue($value){
        $this->value = $value;

        return $this;
    }

    public function getValue(){
        $value = $this->value;

        if(count($this->renderFilters)){
            foreach($this->renderFilters as $filter){
                $value = $filter->filter($value);
            }
        }

        return $value;
    }

    /*********************************************************************************************************************
    * Общедоступные типы отрисовки форм
    */

    /**
    * Стандартная отрисовка:
    *
    * Название  -  Элемент  -  Комментариц
    *              Ошибки
    *
    * @return Form_Element_Abstract
    */
    public function setRenderTypeDefault(){
        $this->renderType = Form_Template_Default::ElementRenderType_Default;
        return $this;
    }

    /**
    * Для длинных элементов
    *
    * Название - Элемент
    *            Ошибки
    *            Комментарий
    * @return Form_Element_Abstract
    */
    public function setRenderTypeBigValue(){
        $this->renderType = Form_Template_Default::ElementRenderType_BigValue;
        return $this;
    }

    /**
    * Для элементов на всю страницу
    *
    * Название
    * Элемент
    * Ошибки
    * Комментарий
    *
    * @return Form_Element_Abstract
    */
    public function setRenderTypeFullLine(){
        $this->renderType = Form_Template_Default::ElementRenderType_FullLine;
        return $this;
    }



    /*********************************************************************************************************************
    * Для отрисовки формы
    */

    /**
    * Возвращает null если нет ошибок
    * Или array с 1 или более текстами описывающими ошибку
    *
    * @return null|array
    */
    public function getErrors(){
        if(is_array($this->errors) && !count($this->errors)){
            return array($this->getTextUnknownError());
        }
        return $this->errors;
    }

    /**
     * return bool Есть ли в поле ошибки
     */
    public function isErrors(){
        return is_null($this->getErrors()) ? false : true;
    }

    /**
    * Получить тип того как надо отрисовывать элемент
    *
    */
    public function getRenderType(){
        return $this->renderType;
    }

    /**
    * Получить имя блока к которому принадлежит элемент
    *
    * @return mixed
    */
    public function getBlock(){
        return $this->block;
    }

    /**
     * Используется ли поле
     *
     * @return bool
     */
    public function getUse(){
        $this->lateInitRunFunction();
        return $this->isUse;
    }

    /**
     * Скрывать ли поле
     *
     * @return bool
     */
    public function isConditionHide(){
        return $this->isConditionHide;
    }

    /*******************************************************************************************************************************
    * Устанвоить форму, в котрой создан объект
    *
    * @param Form $form
    * @param mixed $block
    *
    * @return self
    */
    public function setForm(Form $form, $block = null){
        $this->form = $form;
        $this->block = $block;
        return $this;
    }

    abstract public function render();

    public function __toString(){
        return $this->render();
    }

    /**
     * Получение значения переменной в javascript
     *
     * @return string
     */
    public function javaScriptFunction_GetValue(){
        return "jQuery('#" . str_replace(".", '\\\\.', $this->name) . "').val()";
    }
}