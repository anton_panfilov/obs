<?php

class Form_Element_Select extends Form_Element_Abstract {
    /**
    * @var Html_Attribs
    */
    protected $attribs;

    protected $selectOptionLabelEntities = true;

    protected $options = array();

    protected function init(){
        parent::init();

        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("name", $this->name);
        $this->attribs->setAttrib("id",   $this->name);
    }

    /**
     * @param $width
     * @return $this
     */
    public function setWidth($width){
        if(is_numeric($width)) $width.= "px";

        $this->getAttribs()
            ->setCSS('width', $width);

        return $this;
    }

    /**
     * @param bool $flag
     * @return $this
     */
    public function setSelectOptionLabelEntities($flag = true){
        $this->selectOptionLabelEntities = (bool)$flag;
        return $this;
    }

    /**
     * @return Html_Attribs
     */
    public function getAttribs(){
        return $this->attribs;
    }

    /**
    * Устанвоить список возможных значений
    *
    * @param array $options
    * @param string $firstElementLabel Если не пустая стрка, то добавиться первый элемент с пустям ключем и таком названием
     *
    * @return Form_Element_Select
    */
    public function setOptions(array $options, $firstElementLabel = null){
        $this->options = strlen($firstElementLabel) ?
            array('' => (string)$firstElementLabel) + $options : $options;

        $optionsValidator = [];
        if(count($this->options)){
            foreach($this->options as $k => $v){
                if(!is_null($v)){
                    $optionsValidator[] = $k;
                }
            }
        }

        $this->addValidator(new Form_Validator_InArray($optionsValidator), 'inArray');

        return $this;
    }

    protected function renderOptions(){
        $result = "";
        if(count($this->options)){
            foreach($this->options as $k => $v){
                $attr = new Html_Attribs();
                $attr->setAttrib("value", $k);

                if((string)$k == (string)$this->getValue()){
                    $attr->setAttrib("selected", "selected");
                }

                if($v === null){
                    $result.= "<optgroup label=''></optgroup>";
                }
                else {
                    $v = Translate::t($v);

                    $result.=
                        "<option" . $attr->render() .">" .
                            ($this->selectOptionLabelEntities ? htmlentities($v) : $v) .
                        "</option>";
                }
            }
        }

        return $result;
    }

    public function render(){
        $attr = clone $this->attribs;

        if(is_array($this->errors)){
            $attr->addAttribPost("class", " ap_error_input");
        }

        // conditions - setting condition trigger for element
        if($this->form && $this->form->getConditional($this->name)){
            $attr->addAttribPost("onChange", "document.ap_form_conditionals.run('{$this->name}');");
        }

        return "<select " . $attr->render() . ">" . $this->renderOptions() . "</select>";
    }


    /**
     * Добавление условия, когда при определеннных значениях этого поля, показываються или скрываются другие элементы
     *
     * @param string|array $values    Значения элемента, при которых будут показываться значения
     *                                Можно передать как одно значение (string), так и массив значений (array)
     *                                При передаче массива, элементы будут отображаться при совпадении хотябы с один значением
     * @param string|array $elements  Поля, котоорые будут показываться при определенных выше значениях
     *                                Можно передать как имя одного элемента (string), так и массив имен (array)
     * @return Form_Element_Select
     */
    public function addConditionalFields($values, $elements){
        $this->form->addConditionalFileds_Array($this->name, $values, $elements);
        return $this;
    }
}
