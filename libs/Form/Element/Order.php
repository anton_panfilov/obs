<?php

class Form_Element_Order extends Form_Element_AutoComplite {

    protected $statusMax = 10000;

    /*
    CREATE TABLE `orders_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` smallint(6) DEFAULT NULL,
  `key` char(16) DEFAULT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` date DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `client_data` blob,
  `client_comment` blob,
  `manager_comment` blob,
  `delivery_type` int(11) DEFAULT NULL,
  `delivery_data` blob,
  `price` decimal(12,2) DEFAULT '0.00',
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manager` (`manager`),
  KEY `date` (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

*/

    /**
     * @param $status
     *
     * @return $this
     */
    public function setStatusMax($status){
        $this->statusMax = (int)$status;
        return $this;
    }

    protected $_table = "orders_list";
    protected $_fields = ['id', 'status', 'date', 'client_data', 'price'];

    protected function decorateValue($el){
        $res = [];

        $res['id']      = "{$el['id']}";
        $res['status']  = (new Order_Status())->getLabel($el['status']);
        $res['date']    = (new Decorator_Date())->showCurrentYear()->render($el['date']);
        $res['price']   = String_Filter::price($el['price']);
        $res['client']  = "???";

        $temp = Json::decode($el['client_data']);

        if(isset($temp['name']) && strlen($temp['name'])){
            $res['client'] = $temp['name'];
        }

        if(isset($temp['mobile_phone']) && strlen($temp['mobile_phone'])){
            $res['client'].= " (" . $temp['mobile_phone'] . ")";
        }

        return Translate::t("form_auto_complete_el", "order/form", $res);
    }


    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::site()->select($this->_table, $this->_fields)
            ->where("status < ?", $this->statusMax)
            ->order("id")
            ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`id` rlike ? or `client_data` rlike ?", [$stringRegexp, $stringRegexp]);
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['id']] = $this->decorateValue($el);
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        $select = Db::site()->select($this->_table, ['id'])
            ->where("status < ?", $this->statusMax)
            ->where("`id`=?", (int)$value)
            ->limit(1);

        return $select->fetchOne();
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            $select = Db::site()->select($this->_table, $this->_fields)
                ->where("status < ?", $this->statusMax)
                ->where("id=?", (int)$this->getValue());

            if($el = $select->fetchRow()){
                return $this->decorateValue($el);
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
