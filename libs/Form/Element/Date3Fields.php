<?php

class Form_Element_Date3Fields extends Form_Element_Abstract {
    protected $yearMin;
    protected $yearMax;

    protected $tempDateValues = [];
    protected $errorsIndex = [
        'year'  => false,
        'month' => false,
        'day'   => false,
    ];

    protected function init(){
        parent::init();

        $this->setYearMin("1900");
        $this->setYearMax(date("Y"));
    }

    /*********************************************************************************/

    /**
     * @param $year
     * @return $this
     */
    public function setYearMin($year){
        $this->yearMin = (int)$year;
        return $this;
    }

    /**
     * @param $year
     * @return $this
     */
    public function setYearMax($year){
        $this->yearMax = (int)$year;
        return $this;
    }

    /*********************************************************************************/

    protected function runGetData($params, $notErrors){

        // todo: required and mode notErrors

        $year   = '';
        $month  = '';
        $day    = '';

        /*******************************************/

        if(isset($params["{$this->name}_year"]) && strlen($params["{$this->name}_year"])){
            $year = (int)$params["{$this->name}_year"];

            if($year >= 1 && $year <= 99){
                if($year < 30){
                    $year = "20" . sprintf("%02d", (int)$year);
                }
                else {
                    $year = "19" . sprintf("%02d", (int)$year);
                }
            }

            if($year < $this->yearMin){
                $this->errorsIndex['year'] = true;
                $this->addError("Год не должен быть меньше {$this->yearMin}");
            }
            else if($year > $this->yearMax){
                $this->errorsIndex['year'] = true;
                $this->addError("Год не должен быть больше {$this->yearMax}");
            }
            else {
                $year = sprintf("%04d", $year);
            }
        }
        else {
            $year = '';

            if($this->required){
                $this->errorsIndex['year'] = true;
                $this->addError("Введите год");
            }
        }

        /*******************************************/

        if(isset($params["{$this->name}_month"]) && strlen($params["{$this->name}_month"])){
            $month = (int)$params["{$this->name}_month"];

            if($month < 1 || $month > 12){
                $this->errorsIndex['month'] = true;
                $this->addError("Выбирите месяц");
            }
            else {
                $month = sprintf("%02d", $month);
            }
        }
        else {
            $month = '';

            if($this->required){
                $this->errorsIndex['month'] = true;
                $this->addError("Выбирите месяц");
            }
        }

        /*******************************************/

        if(isset($params["{$this->name}_day"]) && strlen($params["{$this->name}_day"])){
            if(!$this->isErrors()){
                $maxDay = date("t", strtotime("{$year}-{$month}-01"));
                $errText = "В " . Date_Texts::getMonthLabel($month, null, 'ru', 'r') . " {$year} года, {$maxDay} дней";
            }
            else {
                $maxDay = 31;
                $errText = "День не может быть больше 31";
            }

            $day = (int)$params["{$this->name}_day"];

            if($day < 1){
                $this->errorsIndex['day'] = true;
                $this->addError("День не может быть меньше 1");
            }
            else if($day > $maxDay){
                $this->errorsIndex['day'] = true;
                $this->addError($errText);
            }
            else {
                $day = sprintf("%02d", $day);
            }
        }
        else {
            $day = '';

            if($this->required){
                $this->errorsIndex['day'] = true;
                $this->addError("Введите день");
            }
        }

        /*******************************************/

        $this->tempDateValues = [
            'year'  => $year,
            'month' => $month,
            'day'   => $day,
        ];


        if(!$this->isErrors()){
            $this->value = "{$year}-{$month}-{$day}";
        }
    }

    /*********************************************************************************/

    public function render(){

        Site::addJS("plugins/jquery.regex.mask.js");
        Site::addJSCode("jQuery('#{$this->name}_day').regexMask(/^[0-9]*\$/);");
        Site::addJSCode("jQuery('#{$this->name}_year').regexMask(/^[0-9]*\$/);");

        //////////////////

        $attrDay = (new Html_Attribs())
            ->setAttrib("value",
                (isset($this->tempDateValues['day'])) ?
                    ltrim($this->tempDateValues['day'], '0') :
                    (strlen($this->value) ? (int)substr($this->value, 8, 2) . "" : "")
            )
            ->setAttrib("type", "text")
            ->setAttrib("placeholder", "День")
            ->setAttrib("name", $this->name . "_day")
            ->setAttrib("id", $this->name . "_day")
            ->setAttrib("maxLength", "2")
            ->setAttrib("size", "2")
            ->setCSS('width', "40px");

        if($this->errorsIndex['day']){
            $attrDay->addAttribPost("class", " ap_error_input");
        }

        //////////////////

        $attrMonth = (new Html_Attribs())
            ->setAttrib("name", $this->name . "_month")
            ->setAttrib("id", $this->name . "_month");

        if($this->errorsIndex['month']){
            $attrMonth->addAttribPost("class", " ap_error_input");
        }

        $monthOptions = "";
        $months =
            [
                ''          => 'Месяц...',
                'blank_0'   => null,
            ] +
            Date_Texts::getMonthsList('ru', 'r');

        $monthSelect = (isset($this->tempDateValues['month'])) ?
            $this->tempDateValues['month'] :
            (strlen($this->value) ? (int)substr($this->value, 5, 2) : "");

        foreach($months as $k => $v){
            $attr = new Html_Attribs();
            $attr->setAttrib("value", $k);

            if((string)$k == (string)$monthSelect){
                $attr->setAttrib("selected", "selected");
            }

            if($v === null){
                $monthOptions.= "<optgroup label=''></optgroup>";
            }
            else {
                $monthOptions.=
                    "<option" . $attr->render() .">{$v}</option>";
            }
        }

        //////////////////

        $attrYear = (new Html_Attribs())
            ->setAttrib("value",
                (isset($this->tempDateValues['year'])) ?
                    $this->tempDateValues['year'] :
                    (strlen($this->value) ? (int)substr($this->value, 0, 4) : "")
            )
            ->setAttrib("type", "text")
            ->setAttrib("placeholder", "Год")
            ->setAttrib("name", $this->name . "_year")
            ->setAttrib("id", $this->name . "_year")
            ->setAttrib("maxLength", "4")
            ->setAttrib("size", "4")
            ->setCSS('width', "60px");

        if($this->errorsIndex['year']){
            $attrYear->addAttribPost("class", " ap_error_input");
        }

        //////////////////

        return
            "<input  " . $attrDay->render()   . "> " .
            "<select " . $attrMonth->render() . ">{$monthOptions}</select> " .
            "<input  " . $attrYear->render()  . "></input>";
    }
}
