<?php

class Form_Element_Password extends Form_Element_Input {
    protected $render_value = false;

    protected function init(){
        parent::init();

        $this->attribs->setAttrib("type", "password");
    }

    /**
     * Если у элемента есть значение, то выводить его в элементе пароля?
     *
     * @param bool $flag
     * @return $this
     */
    public function setRenderValue($flag = true){
        $this->render_value = (bool)$flag;
        return $this;
    }

    public function render(){
        $attr = clone $this->attribs;

        if($this->render_value && !$this->isErrors()){
            $attr->setAttrib("value", $this->getValue());
        }

        if(is_array($this->errors)){
            $attr->addAttribPost("class", " ap_error_input");
        }

        return (string)$this->textPreInput . "<input " . $attr->render() . ">" . (string)$this->textPostInput;
    }
}
