<?php

class Form_Element_SelectList extends Form_Element_Abstract {
    /**
    * @var Html_Attribs
    */
    protected $attribs;
      
    protected $options = array();  
    
    protected function init(){
        parent::init();
        
        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("name", $this->name . "[]");
        $this->attribs->setAttrib("id",   $this->name);
        $this->attribs->setAttrib("multiple",   "multiple");
        
        $this->value = array();
    }
    
    /**
    * @return Html_Attribs
    */
    public function getAttribs(){
        return $this->attribs;
    } 
    
    /**
    * Установить высоту блока в строчках
    * 
    * @param mixed $rowsCount
    * @return Form_Element_SelectList
    */
    public function setSize($rowsCount){
        $this->getAttribs()->setAttrib('size', (int)$rowsCount);
        return $this;
    }
    
    /**  
    * Если Null то пустота, если что то другое но не массив, то это будет единнственным элементом массива
    * 
    * @param array $values OR $value1, [$value2, $value3, ...] 
    *    
    * @return self
    */
    public function setValue($values){
        if(func_num_args() > 1){
            $values = func_get_args();     
        }
        
        if(is_null($values))   $values = array();
        if(!is_array($values)) $values = array($values);
        
        return parent::setValue($values);
    }
    
    /**
    * Устанвоить список возможных значений
    * 
    * @param array $options
    */
    public function setOptions(array $options){
        $this->options = $options;
    }
         
    protected function runGetData($params){
        if(isset($params[$this->name])){
            $this->value = $params[$this->name];
            
            if(!is_array($this->value)){
                $this->value = array($this->value);
            } 
        } 
        else {
            $this->value = array();  
        }   
        
        if($this->required && !count($this->value)){
            $this->addError($this->getTextIsRequired());
        }        
    }
    
    protected function renderOptions(){
        $result = "";
        if(count($this->options)){ 
            foreach($this->options as $k => $v){
                $attr = new Html_Attribs();
                $attr->setAttrib("value", $k);
                
                if(in_array($k, $this->getValue())){
                    $attr->setAttrib("selected", "selected");    
                }
                
                $result.= "<option" . $attr->render() .">" . htmlentities($v) . "</option>";   
            }
        }
        
        return $result;
    }
    
    public function render(){    
        return "<select " . $this->attribs->render() . ">" . $this->renderOptions() . "</select>";
    }   
}
