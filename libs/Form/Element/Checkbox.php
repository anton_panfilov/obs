<?php

class Form_Element_Checkbox extends Form_Element_Input {
    protected function init(){
        parent::init();

        $this->attribs->setAttrib("type", "checkbox");
    }

    protected function runGetData($params, $notErrors = false){
        // $notErrors - игнорируется

        if(true){
            // для веб формы обязательность проверить не получится
            $this->value = isset($params[$this->name]) ? (int)(bool)$params[$this->name] : 0;
        }
        else {
            // TODO: для сервиса можно проврить ну пока нет разделение на веб формы и сервисы, поэтому оставляем так
            if($this->required && !isset($params[$this->name])){
                $this->addError($this->getTextIsRequired());
                $this->value = null;
            }
            else {
                $this->value = isset($params[$this->name]) ? (int)(bool)$params[$this->name] : 0;
            }
        }

    }

    public function render(){
        if($this->getValue()) $this->attribs->setAttrib('checked', 'checked');
        else                  $this->attribs->setAttrib('checked');

        return "<label class='ap_f_one_label'><input " . $this->attribs->render() . "></label>";
    }
}
