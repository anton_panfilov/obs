<?php

class Form_Element_Text extends Form_Element_TextHtml {

    protected function init(){
        parent::init();
        $this->addFilter(new Form_Filter_NoHtml());
    }
}
