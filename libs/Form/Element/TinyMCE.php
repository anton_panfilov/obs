<?php

class Form_Element_TinyMCE extends Form_Element_Textarea {

    protected function init(){
        parent::init();
        $this->setRenderTypeBigValue();
        
        $this->attribs->setCSS('width', '100%');  
    }
    
    public function render(){   
        Site::addJS("tiny_mce/tiny_mce.js");
        
        $init = array(
            'mode'              =>  "exact",
            'elements'          =>  $this->name,
            'theme'             =>  "advanced",
            'convert_urls'      =>  false,

            'plugins'           =>  implode(",", array(
                "autolink" ,
                'autoresize', 
                "spellchecker",  
                "table", 
                "advimage",
                "advlink",  
                "inlinepopups", 
                "contextmenu",
                "paste",
                "directionality",
                "fullscreen",
                "noneditable",
                "visualchars",
                "nonbreaking",
                "xhtmlxtras",
            )), 
            
            'skin'                  =>  "o2k7",
            'skin_variant'          =>  "silver", 
            
            'theme_advanced_fonts'  =>  implode(";", array(
                "Arial=".             "Arial,Helvetica,sans-serif",
                "Comic Sans=".        "Comic Sans MS,cursive",
                "Courier New=".       "Courier New,Courier,monospace",
                "Tahoma=".            "Tahoma,Geneva,sans-serif",
                "Times New Roman=".   "Times New Roman,Times,serif",
                "Verdana=".           "Verdana,Geneva,sans-serif",
            )), 
            'theme_advanced_font_sizes' => implode(",", array(  
                "1 (9px)=9px",
                "2 (10px)=10px",
                "3 (11px)=11px",
                "4 (12px)=12px",
                "5 (13px)=13px",
                "6 (14px)=14px",
                "7 (16px)=16px",
                "8 (18px)=18px",
                "9 (20px)=20px",
                "10 (22px)=22px",
                "11 (24px)=24px",
                "12 (26px)=26px",
                "13 (28px)=28px",
                "14 (32px)=32px",
                "15 (36px)=36px",
            )),
            
            // Theme options            
            'theme_advanced_buttons1' => implode(',|,', array(
                'undo,redo',
                'bold,italic,underline,strikethrough',
                'justifyleft,justifycenter,justifyright,justifyfull',
                'bullist,numlist',
                'styleselect,formatselect,fontselect,fontsizeselect',
            )),
            'theme_advanced_buttons2' => implode(',|,', array(
                'tablecontrols',
                'visualaid',   
                              
                'link,unlink,anchor,image', 
                'forecolor,backcolor',
                'sub,sup',   
                
                'charmap,nonbreaking',
                'spellchecker',
                'code,fullscreen',
                
            )),
            'theme_advanced_buttons3' => '',
            'theme_advanced_buttons4' => '', 
            
            'theme_advanced_toolbar_location'   => "top", // external
            'theme_advanced_toolbar_align'      => "left",
            'save_enablewhendirty'              => false,
            'content_css'                       => implode(',', array(
                // TODO: возможность подключения своих стилей
                '/ap/css/site/t1/main.css',
                '/ap/css/site/t1/style.css',
                
                '/ap/css/form/element/tinymce.frame.css',  
            )),
            'fullpage_hide_in_source_view'      => true,
            'theme_advanced_resizing'           => true,
            'theme_advanced_resize_horizontal'  => false,  
        );
        
        Site::addJSCode("tinyMCE.init(" . Json::encode($init) . ");");
        
          
        $attr = clone $this->attribs;
        
        
        if(is_array($this->errors)){
            $attr->addAttribPost("class", " ap_error_input");  
            
            return "<div class='ap_error_tinymce_div'>" . 
                "<textarea " . $attr->render() . ">" . htmlspecialchars($this->getValue()) . "</textarea>" . 
            "</div>";
        } 
        
        return "<textarea " . $attr->render() . ">" . htmlspecialchars($this->getValue()) . "</textarea>";
    }   
}
