<?php

abstract class Form_Element_Input extends Form_Element_Abstract {

    protected $textPreInput = '';
    protected $textPostInput = '';

    public function setTextPreInput($text){
        $this->textPreInput = $text;
        return $this;
    }

    public function setTextPostInput($text){
        $this->textPostInput = $text;
        return $this;
    }

    /**
    * @var Html_Attribs
    */
    protected $attribs;

    protected function init(){
        parent::init();

        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("type", "text");
        $this->attribs->setAttrib("name", $this->name);
        $this->attribs->setAttrib("id",   $this->name);
    }

    public function render(){
        $attr = clone $this->attribs;
        $attr->setAttrib("value", $this->getValue());

        if(is_array($this->errors)){
            $attr->addAttribPost("class", " ap_error_input");
        }

        return (string)$this->textPreInput . "<input " . $attr->render() . ">" . (string)$this->textPostInput;
    }

    /**
    * @return Html_Attribs
    */
    public function getAttribs(){
        return $this->attribs;
    }

    /**
     * Установить у элемента HTML свофство, ограниивающее длинну
     * При этом проверка длинны на сервере не производиться.
     * Для работы с длинной строки на сервере используйте модификаторы и валидаторы
     *
     * @param int $len
     * @return $this
     */
    public function setMaxLength($len){
        if($len > 0){
            $this->getAttribs()
                ->setAttrib('maxLength', (int)$len);
        }

        return $this;
    }

    /**
     * @param $len
     * @return $this
     */
    public function setWidthAuto(){
        $this->getAttribs()
            ->setCSS('width', 'auto');

        return $this;
    }

    /**
     * @param $len
     * @return $this
     */
    public function setWidth($width){
        if(is_numeric($width)) $width.= "px";

        $this->getAttribs()
            ->setCSS('width', $width);

        return $this;
    }

    /**
    * Добавить всплывающую подсказку
    *
    * @param mixed $text
    *
    * @return self
    */
    public function setTooltip($text){
        Site::addJS("plugins/tipTip.js");
        Site::addCSS("plugins/tipTip.css");
        Site::addJSCode(
            'jQuery("#' . $this->name . '").tipTip({'.
                'edgeOffset: 3,'.
                'activation: "focus",'.
                'defaultPosition: "right",'.
                'maxWidth: "400px"'.
            '});'
        );

        $this->getAttribs()->addAttribPost("title", Translate::t($text));

        return $this;
    }
}
