<?php

class Form_Element_ClientManager extends Form_Element_AutoComplite {

    protected $onlyActive = false;

    /**
     * @return $this
     */
    public function setOnlyActive(){
        $this->onlyActive = true;
        return $this;
    }

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::site()->select("users", ['id', 'login'])
            ->where("`role`=?", User_Role::CLIENT_MANAGER)
            ->order("id")
            ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`id` rlike ? or `login` rlike ?", [$stringRegexp, $stringRegexp]);
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['id']] = "{$el['id']}: {$el['login']}";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        $select = Db::site()->select("users", ['id'])
            ->where("`id`=?", (int)$value)
            ->where("`role`=?", User_Role::CLIENT_MANAGER)
            ->limit(1);

        return $select->fetchOne();
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            $select = Db::site()->select("users", ['id', 'login'])
                ->where("id=?", (int)$this->getValue())
                ->where("`role`=?", User_Role::CLIENT_MANAGER);

            if($el = $select->fetchRow()){
                return "{$el['id']}: {$el['login']}";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
