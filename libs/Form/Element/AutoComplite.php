<?php

abstract class Form_Element_AutoComplite extends Form_Element_Text {
    protected $autoCompleteParams = [];

    protected function init(){
        parent::init();

        $this->attribs->setAttrib("name", $this->name . "_txt");
        $this->attribs->setAttrib("id",   $this->name . "_txt");

        $this->setAutocomlete(false);

        Site::addCSS("form/element/autocomplite.css");
        Site::addJS("form/element/autocomplite.js");

        // Отдать список по запросу
        if(
            isset($_GET['ap_form_element_autocomplite_name'], $_GET['s']) &&
            $_GET['ap_form_element_autocomplite_name'] == $this->name
        ){
            $params = isset($_GET['p']) ? $_GET['p'] : array();

            ob_get_clean(); // отчиста того что накопилось в буфере до этого
            die(
                Json::encode(
                    $this->getArray($_GET['s'], $params)
                )
            );
        }
    }

    /**
     * Массив параметров, которе будет переданны в javascript класс.
     * Для того что бы передать json функию используйте конструкции:
     *  [
     *      'advertiser' => new \AP\Json\Expr(
     *          "function(){return jQuery('#advertiser').val()}"
     *      ),
     *  ]
     *
     * @param array $params
     *
     * @return Form_Element_AutoComplite
     */
    public function setAutoCompliteParams($params){
        $this->autoCompleteParams = $params;
        return $this;
    }

    /**
     * Абстрактная функция для получения массива значений для поисковой строки и параметров
     *
     * @param string $string
     * @param array $params
     *
     * @return array (key - значение переменной, value - визуальное отображение)
     */
    abstract protected function getArray($string, $params = array());

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     * Значение получается из $this->getValue()
     *
     * @return string
     */
    abstract protected function getValueLabel();

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    abstract protected function isValidHiddenValue($value);

    /**
     * Получения данных и проверка поля на ошибки
     *
     * @param array $params
     * @param bool $notErrors
     *
     * @return bool|void
     */
    public function isValid($params, $notErrors = false){
        /**
         * (А как же валидаторы и фильтры? - не нужны пока)
         * (Обязательность поля)
         *
         *
         * - Сохранить текущее значение (для notErrors)
         *
         * - получить значения (непосредственно значение и то что в поле)
         *
         * 1. Если оба пустые и значение не обязательно или notErrors, выйти с true (при notErrors подставить значение)
         *
         * 2. Если есть скрытое поле, то проверить допустим ли его значение (Если допустимо, выйти c true)
         * 3. Если строка поиска не пустая, поытаться получить для неё лист значений,
         *    если в нем будет только 1 элемент проверить его на доупстимость и если норм выйти с true
         *
         * 4. Если notErrors, выйти с true, подставив значение сохранение в начале
         *
         * 5. false - с ошибкой что значение не верно
         */

        if($this->getUse() && !$this->isConditionHide()){
            $value          = isset($params[$this->name])           ? trim($params[$this->name])            : '';
            $searchString   = isset($params[$this->name . "_txt"])  ? trim($params[$this->name . "_txt"])   : '';

            if($value == '' && $searchString == ''){
                if(!$this->required){
                    // Если поле не обязательно, обнулить его и не показывать ошибку
                    $this->value = '';
                    return true;
                }
                else if($notErrors){
                    // Если в полях нельзя показывать ошибки, то оставить то значение которое есть
                    return true;
                }
                else {
                    // Значение обязательно - ошибка
                    $this->addError($this->getTextIsRequired());
                    return false;
                }
            }

            // проверка на валидность скрытого поля
            if(strlen($value) && $this->isValidHiddenValue($value)){
                $this->value = $value;
                return true;
            }

            // проверка, если search дает только 1 знаение, о принять это значение за искомое
            if(strlen($searchString)){
                // TODO: с учетом дополнительных параметров или нет вот в чем вопрос
                $res = $this->getArray($searchString);
                if(count($res) == 1){
                    list($this->value) = array_keys($res);
                    return true;
                }
            }

            // Если даже при том что значнеие не найденно, нельзя допускать ошибки, то выйти как есть
            if($notErrors){
                return true;
            }

            $this->addError("Invalid Value");
            return false;
        }

        // поле не содержит переменных
        return true;
    }

    public function render(){
        // настройка скрытого значения
        $hidden = new Html_Attribs();
        $hidden->setAttrib("type", "hidden");
        $hidden->setAttrib("name", $this->name);
        $hidden->setAttrib("id",   $this->name);
        $hidden->setAttrib("value", $this->getValue());

        // Если текстовое представление можно получить из переменных окружения, сделать это, иначе получить стандартной функцией
        if(isset($_POST[$this->name . "_txt"]) && strlen($_POST[$this->name . "_txt"])){
            $labelValue = $_POST[$this->name . "_txt"];
        }
        else if(isset($_GET[$this->name . "_txt"]) && strlen($_GET[$this->name . "_txt"])){
            $labelValue = $_GET[$this->name . "_txt"];
        }
        else {
            $labelValue = $this->getValueLabel();
        }

        // Если есть текстовое представление, передать его в JS класс
        if(strlen($labelValue)){
            Site::addJSCode("document.ap_form_element_autocomplite.setDataTxt('{$this->name}', '" .
                str_replace("'", "\\'", $labelValue) .
            "')");
        }

        if(count($this->autoCompleteParams)){
            Site::addJSCode("document.ap_form_element_autocomplite.setParams('{$this->name}', " .
                Json::encode($this->autoCompleteParams) .
            ")");
        }

        // настройка поля поиска значения
        $search = clone $this->attribs;
        $search->setAttrib("value", $labelValue);
        $search->setAttrib("class", "ap_form_autocompite");

        $search->setAttrib("onfocus", "ap_form_element_autocomplite.search('{$this->name}')");
        $search->setAttrib("onkeyup", "ap_form_element_autocomplite.search('{$this->name}')");
        $search->setAttrib("onpaste", "ap_form_element_autocomplite.search('{$this->name}')");

        $search->setAttrib("onblur", "ap_form_element_autocomplite.exit('{$this->name}')");

        if(is_array($this->errors)){
            $search->addAttribPost("class", " ap_error_input");
        }

        return
            "<input " . $search->render() . ">" .
            "<input " . $hidden->render() . ">" .
            "<div id='{$this->name}_dbg'></div>" .
            "<div style='position:relative'>" .
                "<div class='ap_form_autocompite_mainDiv' id='{$this->name}_mainDiv'>" .
                    "<div class='ap_form_autocompite_Close'>" .
                        "<a class='icon-remove-sign' onclick=\"ap_form_element_autocomplite.exitNow('{$this->name}')\"></a>" .
                    "</div>" .
                    "<span class='message' id='{$this->name}_info'>" .
                        "<i id='{$this->name}_img'></i>" .
                        "<span id='{$this->name}_message'>Loading. Please Wait...</span>" .
                    "</span>" .
                    "<div id='{$this->name}_DivContent'></div>" .
                "</div>" .
            "</div>";
    }
}
