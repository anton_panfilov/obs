<?php

class Form_Element_User extends Form_Element_AutoComplite {

    /**
     * Поиск пользователей по id, name or email
     * Отображать его на странице как "ID: Name (Email)"
     *
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::site()->select("users", ['id', 'login', 'email'])
        ->order("id")
        ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`id` rlike ? or `login` rlike ? or `email` rlike ?", [$stringRegexp, $stringRegexp, $stringRegexp]);
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['id']] = "{$el['id']}: {$el['login']} ({$el['email']})";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        return (bool)Db::site()->fetchOne("select id from users where id=?", $value);
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            if($el = Db::site()->fetchRow("select id, login, email from users where id=? limit 1", $this->getValue())){
                return "{$el['id']}: {$el['login']} ({$el['email']})";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
