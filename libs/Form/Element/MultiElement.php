<?php

class Form_Element_MultiElement extends Form_Element_Abstract {
    protected $isValue = false;

    protected $auto_labels = false;

    protected $multi_elements = array();
    protected $multi_renderTemplate = null;

    public function __construct($elements, $label = null, $name = null){
        $this->name = $name;
        $this->setLabel($label);
        $this->multi_elements = $elements;
    }

    /************************************************************************************/

    /**
     * Установить шаблон мультиполя
     *
     * @param null|array|string     $template  шаблон для мультиполя
     *                              null  - элементы будут склеены в одну строку один рядом в с другим
     *                              array - элементы массива склеиваются в td таблицы (вызов рендера элемента через {NAME})
     *                              text  - свободный html темплейт (вызов рендера элемента через {NAME})
     * @return Form_Element_MultiElement
     */
    public function setTemplate($template = null){
        $this->multi_renderTemplate = $template;
        return $this;
    }

    /**
     * @return Form_Element_MultiElement
     */
    public function setAutoLabels(){
        $this->auto_labels = true;
        return $this;
    }

    /************************************************************************************/

    /**
     * Переопределение получения лейбла.
     * Если label = null, он слепливается из lebels входящих элементов
     *
     * @param null $label
     *
     * @return string
     * @throws Exception
     */
    public function getLabel(){
        $label = $this->label;

        if(is_null($label)){
            $l = array();
            foreach($this->multi_elements as $el){
                $l[] = $this->form->getElement($el)->getLabel();
            }
            $label = implode(" / ", $l);
        }

        if($this->form->getMarkRequiredFields()){
            $required = false;

            foreach($this->multi_elements as $el){
                if($this->form->getElement($el)->required){
                    $required = true;
                    break;
                }
            }

            if($required) {
                $label = "<b>{$label}</b> <span class='txt-color-redLight'>*</span>";
            }
            else {
                $label = "<span style='color: #777'>{$label}</span>";
            }
        }

        return $label;
    }

    /**
     * Возвращает массив имен элементов, входящих в мультик
     *
     * @return array
     */
    public function getMultiElements(){
        return $this->multi_elements;
    }

    /**
     * Отрисовка мультиэлемента
     *
     * @return string
     */
    public function render(){
        $t = $this->multi_renderTemplate;

        if(is_null($t) || (is_array($t) && !count($t))){
            $t = "<table class='ap_form_ligths'>";
            if($this->auto_labels){
                $t.= "<tr>";
                foreach($this->multi_elements as $el){
                    $t.= "<td style='padding-top: 5px; line-height: 100%; width:" . floor(100 / count($this->multi_elements)) . "%;'>" . $this->form->getElement($el)->getLabel() . "</td>";
                }
                $t.= "</tr>";
            }
            $t.= "<tr>";
            foreach($this->multi_elements as $el){
                $t.= "<td style='width:" . floor(100 / count($this->multi_elements)) . "%;'>{{$el}}</td>";
            }
            $t.= "</tr>";
            $t.= "</table>";
        }
        else if(is_array($t) && count($t)){
            $renderTemplate_array = $t;

            $t = "<table class='ap_form_ligths' style='width:auto'><tr>";
            foreach($renderTemplate_array as $el){
                $t.= "<td>{$el}</td>";
            }
            $t.= "</tr></table>";
        }

        $r = '';
        $type = 0;
        $name = '';

        for($i = 0; $i < strlen($t); $i++){
            $sybm = substr($t, $i, 1);
            if($type == 0){
                if($sybm == "{"){
                    $type = 1;
                    $name = '';
                }
                else {
                    $r.= $sybm;
                }
            }
            else if($type == 1){
                if($sybm == "}"){
                    $type = 0;
                    $r.= $this->form->getElement($name)->render();
                }
                else {
                    $name.= $sybm;
                }
            }
        }

        return $r;
    }

    /**
    * Возвращает null если нет ошибок
    * Или array с 1 или более текстами описывающими ошибку
    *
    * @return null|array
    */
    public function getErrors(){
        $errors = null;

        if(count($this->multi_elements)){
            foreach($this->multi_elements as $el){
                if(!is_null($this->form->getElement($el)->getErrors())){
                    if(is_null($errors)) $errors = array();

                    if(is_array($this->form->getElement($el)->getErrors()) && count($this->form->getElement($el)->getErrors())){
                        foreach($this->form->getElement($el)->getErrors() as $message){
                            if(strlen($this->form->getElement($el)->getLabel())){
                                $errors[] = $this->form->getElement($el)->getLabel() . ": " . $message;
                            }
                            else {
                                $errors[] = $message;
                            }
                        }
                    }
                }
            }
        }

        return $errors;
    }
}
