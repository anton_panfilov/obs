<?php

class Form_Element_StateCode extends Form_Element_AutoComplite {

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = trim($string);

        // получение данных по запросу
        $select = Db::site()->select("geo_states", ['code', 'title'])
            ->order("code")
            ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`code` rlike ? or `title` rlike ?", [$stringRegexp, $stringRegexp]);
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['code']] = "{$el['code']}: {$el['title']}";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        return (bool)Db::site()->fetchOne("select `code` from geo_states where `code`=?", $value);
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            if($el = Db::site()->fetchRow(
                "select `code`, `title` from geo_states where `code`=? limit 1",
                $this->getValue())
            ){
                return "{$el['code']}: {$el['domain']}";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
