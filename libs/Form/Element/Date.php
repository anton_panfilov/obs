<?php

class Form_Element_Date extends Form_Element_Abstract {
    /**
    * @var Html_Attribs
    */
    protected $attribs;

    protected $dpOptions = [
        'showOtherMonths'   => true,
        'selectOtherMonths' => true,
        'showButtonPanel'   => true,
        'numberOfMonths'    => 1,
        'showAnim'          => 'blind',
        'changeMonth'       => true,
        'changeYear'        => true,
        'dateFormat'        => 'dd.mm.yy',
    ];

    protected $options = array();

    protected function init(){
        parent::init();

        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("type", "text");
        $this->attribs->setAttrib("name", $this->name);
        $this->attribs->setAttrib("id",   $this->name);

        $this->attribs->setCSS('background', 'url(/static/img/16/calendar.gif) no-repeat right #FFF');
        $this->attribs->setCSS('width', '230px');
    }

    /*********************************************************************************/

    /**
     * Установка лимитов для даты
     *
     * @param string $min
     * @param string $max
     *
     * @return Form_Element_Date
     */
    public function setLimit($min = null, $max = null){
        if(strlen($min))$this->dpOptions['minDate'] = $min;
        if(strlen($max))$this->dpOptions['maxDate'] = $max;

        return $this;
    }

    public function getValue(){
        return date('Y-m-d', strtotime(parent::getValue()));
    }

    /*********************************************************************************/

    public function render(){
        Site::addJSCode("jQuery('#{$this->name}').datepicker(" . Json::encode($this->dpOptions) . ");");

        $attr = clone $this->attribs;

        // set value
        $mktime = strtotime($this->getValue());
        if($mktime) $attr->setAttrib("value", date("M d, Y", $mktime));

        if(is_array($this->errors)){
            $attr->addAttribPost("class", " ap_error_input");
        }

        return "<input " . $attr->render() . ">";
    }
}
