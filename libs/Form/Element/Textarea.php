<?php

class Form_Element_Textarea extends Form_Element_Abstract {
    /**
    * @var Html_Attribs
    */
    protected $attribs;     
    
    protected function init(){
        parent::init();
        
        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("name", $this->name);
        $this->attribs->setAttrib("id",   $this->name);
    }

    /**
     * Установить значение, которое будет показываться в поле, если в него ничего не ввести
     *
     * @param string $message
     * @return Form_Element_Textarea
     */
    public function setPlaceholder($message){
        $this->getAttribs()->setAttrib('placeholder', Translate::t($message));
        return $this;
    }
    
    /**
    * @return Html_Attribs
    */
    public function getAttribs(){
        return $this->attribs;
    }

    /**
     * Установить ширину поля
     *
     * @param int $width
     * @return $this
     */
    public function setWidth($width){
        $this->getAttribs()->setCSS('width', (int)$width . "px");
        return $this;
    }

    /**
     * Установить высоту поля
     *
     * @param int $height
     * @return $this
     */
    public function setHeight($height){
        $this->getAttribs()->setCSS('height', (int)$height . "px");
        return $this;
    }

    /**
     * Высота увеличиваеться пропорзионально заполненному тексту
     *
     * @return $this
     */
    public function setAutoHeight(){
        Site::addJS('plugins/jquery.autosize.min.js');
        Site::addJSCode("jQuery('#" . $this->name . "').autosize();");

        return $this;
    }
    
    public function render(){    
        $attr = clone $this->attribs;
         
        if(is_array($this->errors)){
            $attr->addAttribPost("class", " ap_error_input");  
        }
        
        return "<textarea " . $attr->render() . ">" . htmlspecialchars($this->getValue()) . "</textarea>";
    }   
}
