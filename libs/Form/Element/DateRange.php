<?php

class Form_Element_DateRange extends Form_Element_Abstract {
    /**
     * @var Html_Attribs
     */
    protected $attribs;

    protected $dpOptions = [
        'showOtherMonths'   => true,
        'selectOtherMonths' => true,
        'numberOfMonths'    => 3,
        'dateFormat'        => 'M dd, yy',
        'showCurrentAtPos'  => 1,

    ];

    protected $options = array();

    protected $limit = array(
        'min' => null,
        'max' => null,
    );

    protected $buttons = [];

    protected function init(){
        parent::init();

        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("type", "text");
        $this->attribs->setAttrib("name", $this->name);
        $this->attribs->setAttrib("id",   $this->name);

        $this->attribs->setCSS('background', 'url(/static/img/16/calendar.gif) no-repeat right #FFF');
        $this->attribs->setCSS('width', '125px');

        // По умолчанию, лимит полтора года, данные для начала показываются за 7 последних дней, включая сегоджняшиний
        $this->setLimit("-1 Year -6 Months +1 Day", 'Now');
        $this->setValueDates('-27 Days', 'now');
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setButtonsMonths($count = 6){
        for($i = 0; $i < $count; $i++){
            $mkFirstDay = mktime(0, 0, 0, date('m')-$i, 1, date('Y'));
            $mkLastDay = $i == 0 ?
                mktime(0, 0, 0, date('m')-$i, date('d'), date('Y')) :
                mktime(0, 0, 0, date('m')-$i, date('t', $mkFirstDay), date('Y'));

            $this->buttons[] = [
                date('M d, Y',  $mkFirstDay),
                date('M d, Y',  $mkLastDay),
                Translate::t(date('F', $mkFirstDay)),
            ];
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function setButtonsLastDays(){
        $days = [
            [-6,  0, "7 Days"],
            [-13, 0, "14 Days"],
            [-29, 0, "30 Days"],
        ];

        foreach($days as $day){
            $this->buttons[] = [
                date('M d, Y',  strtotime("{$day[0]}Days")),
                date('M d, Y',  strtotime("{$day[1]}Days")),
                $day[2],
            ];
        }

        return $this;
    }

    /****************************************************************************************************/

    /**
     * Установить дату начала
     * Дата принимается в любом формате, который понимает php функкия strtotime:
     *
     * > 2000-01-25
     * > Jan 25, 2000
     * > -1 Month + 1 Day
     * > now
     *
     *   и т.д.
     *
     * @param string $date
     *
     * @return Form_Element_DateRange
     */
    public function setValueDateFrom($date){
        if(strlen(trim($date))){
            $this->value['from'] = Date_Timezones::getDateForTimezone('Y-m-d', strtotime($date));
        }
        else {
            $this->value['from'] = '';
        }
        return $this;
    }

    /**
     * Установить дату конца
     * Дата принимается в любом формате, который понимает php функкия strtotime:
     *
     * > 2000-01-25
     * > Jan 25, 2000
     * > -1 Month + 1 Day
     * > now
     *
     *   и т.д.
     *
     * @param string $date
     *
     * @return Form_Element_DateRange
     */
    public function setValueDateTill($date){
        if(strlen(trim($date))){
            $this->value['till'] = Date_Timezones::getDateForTimezone('Y-m-d', strtotime($date));
        }
        else {
            $this->value['till'] = '';
        }
        return $this;
    }

    /**
     * Установка значений по умолчанию
     * Данные ($from and $till) принимаются в любом формет, который понимает php функкия strtotime:
     *
     * > 2000-01-25
     * > Jan 25, 2000
     * > -1 Month + 1 Day
     * > now
     *
     *   и т.д.
     *
     * @param string $from
     * @param string $till
     *
     * @return Form_Element_DateRange
     */
    public function setValueDates($from, $till){
        return $this
            ->setValueDateFrom($from)
            ->setValueDateTill($till);
    }

    /**
     * Переопределение задания значения
     *
     * @param $value
     * @return $this|Form_Element_DateRange
     */
    public function setValue($value){
        return $this->setValueDates(
            isset($value['from']) ? $value['from'] : "",
            isset($value['till']) ? $value['till'] : ""
        );
    }

    /*********************************************************************************/

    /**
     * Установка лимитов для даты
     *
     * Данные ($min and $max) принимаются в любом формет, который понимает php функкия strtotime:
     *
     * > 2000-01-25
     * > Jan 25, 2000
     * > -1 Month + 1 Day
     * > now
     *
     *   и т.д.
     *
     * @param string $min
     * @param string $max
     *
     * @return Form_Element_Date
     */
    public function setLimit($min = null, $max = null){
        // min
        if(strlen($min)){
            $this->limit['min'] = strtotime($min, mktime(0, 0, 0));
            $this->dpOptions['minDate'] = Date_Timezones::getDateForTimezone("M d, y", $this->limit['min']);
        }
        else {
            $this->limit['min'] = null;
            unset($this->dpOptions['minDate']);
        }

        // max
        if(strlen($max)){
            $this->limit['max'] = strtotime($max, mktime(0, 0, 0));
            $this->dpOptions['maxDate'] = Date_Timezones::getDateForTimezone("M d, y", $this->limit['max']);
        }
        else {
            $this->limit['max'] = null;
            unset($this->dpOptions['maxDate']);
        }

        return $this;
    }

    /*********************************************************************************/

    /**
     * @param array $params
     * @param bool $notErrors
     * @return bool
     */
    public function isValid($params, $notErrors = false){
        $valid = true;

        // from
        $from = "";
        if(isset($params[$this->name]['from']) && strlen(trim($params[$this->name]['from']))){
            $from_mktime = strtotime($params[$this->name]['from']);

            if(
                $from_mktime
                && (!is_null($this->limit['min']) && $from_mktime >= $this->limit['min'])
                && (!is_null($this->limit['max']) && $from_mktime <= $this->limit['max'])
            ){
                // Дата есть и с ней все в порядке
                $from = $params[$this->name]['from'];
            }
            else {
                if($notErrors){
                    // дата с ошибкой, ставим значение по умолчанию
                    $from = $this->value['from'];
                }
                else {
                    $from = $params[$this->name]['from'];

                    $this->addError("The from date invalid");
                    $valid = false;
                }
            }
        }
        else {
            if($this->required){
                if(!$notErrors){
                    $this->addError("The from date required");
                    $valid = false;
                }
                else {
                    $from = $this->value['from'];
                }
            }
            else {
                $from = '';
            }
        }

        // till
        $till = "";
        if(isset($params[$this->name]['till']) && strlen(trim($params[$this->name]['till']))){
            $till_mktime = strtotime($params[$this->name]['till']);

            if(
                $till_mktime
                && (!is_null($this->limit['min']) && $till_mktime >= $this->limit['min'])
                && (!is_null($this->limit['max']) && $till_mktime <= $this->limit['max'])
            ){
                // Дата есть и с ней все в порядке
                $till = $params[$this->name]['till'];
            }
            else {
                if($notErrors){
                    // дата с ошибкой, ставим значение по умолчанию
                    $till = $this->value['till'];
                }
                else {
                    $till = $params[$this->name]['till'];

                    $this->addError("The till date invalid");
                    $valid = false;
                }
            }
        }
        else {
            if($this->required){
                if(!$notErrors){
                    $this->addError("The till date required");
                    $valid = false;
                }
                else {
                    $till = $this->value['till'];
                }
            }
            else {
                $till = '';
            }
        }

        if(strtotime($from) > strtotime($till)){
            $temp = $from;
            $from = $till;
            $till = $temp;
        }

        $this->setValueDates($from, $till);

        return $valid;
    }

    /*********************************************************************************/

    public function render(){
        // Settings Attributs
        $from = clone $this->attribs;
        $till = clone $this->attribs;

        $from->addAttribPost("id",   "_from");
        $from->addAttribPost("name", "[from]");

        $till->addAttribPost("id",   "_till");
        $till->addAttribPost("name", "[till]");

        // Settings Datapicker
        $fromOpt = $this->dpOptions;
        $tillOpt = $this->dpOptions;

        // Limit set
        $fromOpt['maxDate'] = date("M d, y", strtotime($this->value['till']));
        $tillOpt['minDate'] = date("M d, y", strtotime($this->value['from']));

        // Limit functions
        $fromOpt['onClose'] = new Json_Expr("function(selectedDate){
            $('#" . $till->getAttrib('id') . "').datepicker('option', 'minDate', selectedDate);
        }");

        $tillOpt['onClose'] = new Json_Expr("function(selectedDate){" .
            "jQuery('#" . $from->getAttrib('id') . "').datepicker('option', 'maxDate', selectedDate);" .
            "}");

        Site::addJSCode("jQuery('#" . $from->getAttrib('id') . "').datepicker(" . Json::encode($fromOpt) . ");");
        Site::addJSCode("jQuery('#" . $till->getAttrib('id') . "').datepicker(" . Json::encode($tillOpt) . ");");



        // set value
        $mktime_from = strtotime($this->getValue()['from']);
        if($mktime_from) $from->setAttrib("value", date("M d, Y", $mktime_from));

        $mktime_till = strtotime($this->getValue()['till']);
        if($mktime_till) $till->setAttrib("value", date("M d, Y", $mktime_till));

        if(is_array($this->errors)){
            // TODO: подствечиваться долно только поле с ошибкой
            $from->addAttribPost("class", " ap_error_input");
            $till->addAttribPost("class", " ap_error_input");
        }

        $buttons = '';
        if(count($this->buttons)){
            $buttons.= "<span style='margin-left: 20px' class='hidden-print'>";
            foreach($this->buttons as $button){
                $buttons.= "
                    <a
                        style='cursor: pointer; margin-left: 10px'
                        onclick=\"" .
                    "jQuery('#" . $from->getAttrib('id') .  "').attr('value', '{$button[0]}');" .
                    "jQuery('#" . $till->getAttrib('id') .  "').attr('value', '{$button[1]}');" .
                    "\">{$button[2]}</a>";
            }
            $buttons.= "</span>";
        }

        return "<input " . $from->render() . "> <input " . $till->render() . "> {$buttons}";
    }
}
