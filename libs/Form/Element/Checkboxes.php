<?php

class Form_Element_Checkboxes extends Form_Element_Abstract {
    /**
    * @var Html_Attribs
    */
    protected $attribs;

    /**
     * @var Html_Attribs
     */
    protected $attribs_label;

    protected $options = array();  
    
    protected function init(){
        parent::init();
        
        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("name", $this->name . "[]");
        $this->attribs->setAttrib("type", 'checkbox');

        $this->attribs_label = new Html_Attribs();
        
        $this->value = array();
    }

    /**
     * @param $width
     * @return $this
     */
    public function setLabelWidth($width){
        $this->getLabelAttribs()->setCSS('width', $width);
        return $this;
    }
    
    /**
    * Устанвоить список возможных значений
    * 
    * @param array $options
    */
    public function setOptions(array $options){
        $this->options = $options;
    }

    /**
     * @return Html_Attribs
     */
    public function getLabelAttribs(){
        return $this->attribs_label;
    }
    
    protected function runGetData($params, $notErrors){
        $firstValue = $this->value;

        if(isset($params[$this->name])){
            $this->value = $params[$this->name];
            
            if(!is_array($this->value)){
                $this->value = array($this->value);
            } 
        } 
        else {
            $this->value = array();  
        }   



        if($this->required && !count($this->value)){
            if($notErrors){
                $this->value = $firstValue;
            }
            else {
                $this->addError($this->getTextIsRequired());
            }
        } 
    }
    
    /**  
    * Если Null то пустота, если что то другое но не массив, то это будет единнственным элементом массива
    * 
    * @param array $values OR $value1, [$value2, $value3, ...] 
    *    
    * @return self
    */
    public function setValue($values){
        if(func_num_args() > 1){
            $values = func_get_args();     
        }
        
        if(is_null($values))   $values = array();
        if(!is_array($values)) $values = array($values);
        
        return parent::setValue($values);
    }

    
    public function render(){
        Site::addJS("plugins/jquery.checkboxes-1.0.6.min.js");

        $id = "form_checkboxes_group_" . Site::getUniqueInt();
        $result = "<div id='{$id}'>";
        Site::addJSCode("jQuery('#{$id}').checkboxes('range', true);");

        if(count($this->options)){ 
            $i = 0;

            foreach($this->options as $k => $v){
                $this->attribs->setAttrib("id", "{$this->name}_{$i}");
                $this->attribs->setAttrib("value", $k);



                if(in_array($k, $this->getValue())) $this->attribs->setAttrib("checked", "checked");  
                else                                $this->attribs->setAttrib("checked");  
                
                $result.= "<label " . $this->attribs_label->render() . "><input" . $this->attribs->render() . "/>" . Translate::t($v) . "</label>";
                
                $i++; 
            }
        }
        
        return $result . "</div>";
    }   
}
