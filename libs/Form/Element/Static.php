<?php

class Form_Element_Static extends Form_Element_Abstract {
    protected $isValue = false;
    
    protected $text;

    /**
     * @param string $text
     * @return $this
     */
    public function setText($text){
        $this->text = $text;
        return $this;
    }
    
    public function render(){
        if($this->text instanceof Closure){
            $func = $this->text;
            $text = $func(Context::getArray());
        }
        else {
            $text = $this->text;
        }

        return "<div style='padding:2px 0 2px 0;'>{$text}</div>";
    }
}
