<?php

class Form_Element_TextHtml extends Form_Element_Input {

    protected function init(){
        parent::init();

        $this->addFilter(new Form_Filter_Trim());
    }

    /**
     * Установить значение, которое будет показываться в поле, если в него ничего не ввести
     *
     * @param string $message
     * @return Form_Element_Text
     */
    public function setPlaceholder($message){
        $this->getAttribs()->setAttrib('placeholder', Translate::t($message));
        return $this;
    }

    public function setTooltip($message){
        $this->getAttribs()->addAttribPost('class', ' tooltips');
        $this->getAttribs()->setAttrib('data-placement', 'top');
        $this->getAttribs()->setAttrib('data-original-title', $message);
        return $this;
    }
    
    /**
     * Установить/сбросить значение autocomplete
     *
     * @param bool $autocomplete
     * @return Form_Element_Text
     */
    public function setAutocomlete($autocomplete = true){
        $this->getAttribs()->setAttrib("autocomplete", $autocomplete ? "on" : "off");
        return $this;
    }
    
    /**
     * Параметр - это price | ip | num | cтрока с регулярным выражением.
     *
     * @param string $mask
     * @return Form_Element_Text
     */
    public function setMask($mask){
        Site::addJS("plugins/jquery.regex.mask.js");

        $fixedMasks = [
            "price" => "/^[-]?[0-9]*(\\.[0-9]*)?\$/",
            "ip"    => "/^[\\.\\d]+\$/",
            "num"   => "/^[-]?[0-9]*\$/"
        ];

        if(array_key_exists(mb_strtolower($mask), $fixedMasks)){
            $mask = $fixedMasks[$mask];
        }
      
        Site::addJSCode("jQuery('#{$this->name}').regexMask({$mask});");
  
        $this->setAutocomlete(false);
        return $this;
    }
}
