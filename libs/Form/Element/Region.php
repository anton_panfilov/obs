<?php

class Form_Element_Region extends Form_Element_AutoComplite {

    /**
     * @param string $string
     * @param array $params
     *
     * @return array
     */
    protected function getArray($string, $params = array()){
        // обработка входящих переменных
        $string = mb_strtolower(trim($string));

        // получение данных по запросу
        $select = Db::site()->select("geo_fias_regions", ['id', 'name'])
            ->order("name")
            ->limit(15);

        if(strlen($string)){
            // подготовка строки для регулярного выражения
            $stringRegexp = "";
            foreach(str_split($string) as $char){
                $stringRegexp.= "\\" . $char;
            }

            $select->where("`id` rlike ? or LOWER(`name`) rlike ?", [$stringRegexp, $stringRegexp]);
        }

        try {
            $data = $select->fetchAll();
        }
        catch(\Exception $e){
            $data = array();
        }

        // посмотрение выходных данных
        $result = array();

        if(count($data)){
            foreach($data as $el){
                $result[$el['id']] = "{$el['id']}: {$el['name']}";
            }
        }

        return $result;
    }

    /**
     * Проверить можно ли использовать это значение
     *
     * @param $value
     *
     * @return bool
     */
    protected function isValidHiddenValue($value){
        return (bool)Db::site()->fetchOne("select `id` from geo_fias_regions where `id`=?", $value);
    }

    /**
     * Получение текстового представления для значения
     * (Желательно если оно будет равно тесковому представлению, получаемому для списка)
     *
     * @return string
     */
    protected function getValueLabel(){
        if(strlen($this->getValue())){
            if($el = Db::site()->fetchRow(
                "select `id`, `name` from geo_fias_regions where `id`=? limit 1",
                $this->getValue())
            ){
                return "{$el['id']}: {$el['name']}";
            }

            return (string)$this->getValue();
        }

        return "";
    }
}
