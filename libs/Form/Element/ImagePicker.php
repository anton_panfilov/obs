<?php

class Form_Element_ImagePicker extends Form_Element_Select {

    protected function init(){
        parent::init();

        $this->attribs->addAttribPost("class", " image-picker ");

        $this->setRenderTypeBigValue();
    }


    protected function renderOptions(){
        $result = "";
        if(count($this->options)){
            foreach($this->options as $k => $v){
                $attr = new Html_Attribs();
                $attr->setAttrib("value", $k);

                if((string)$k == (string)$this->getValue()){
                    $attr->setAttrib("selected", "selected");
                }

                $attr->setAttrib("data-img-src", $v);

                $v = Translate::t($v);

                $result.=
                    "<option" . $attr->render() .">" .
                        ($this->selectOptionLabelEntities ? htmlentities($v) : $v) .
                    "</option>";

            }
        }

        return $result;
    }

    public function render(){
        Site::addJS("plugins/image-picker.min.js");
        Site::addCSS("plugins/image-picker.css");
        Site::addJSCode("jQuery('#{$this->name}').imagepicker();");

        $attr = clone $this->attribs;

        if(is_array($this->errors)){
            $attr->addAttribPost("class", " ap_error_input");
        }

        // conditions - setting condition trigger for element
        if($this->form && $this->form->getConditional($this->name)){
            $attr->addAttribPost("onChange", "document.ap_form_conditionals.run('{$this->name}');");
        }

        return "<div class='picker form_imagepicker'><select " . $attr->render() . ">" . $this->renderOptions() . "</select></div>";
    }


}
