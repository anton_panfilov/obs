<?php

class Form_Template_Default {
    const ElementRenderType_Default = 1;
    const ElementRenderType_BigValue = 2;
    const ElementRenderType_FullLine = 3;

    /**
    * Отрисовка всей формы
    */
    public function render(Form $form){
        Site::addCSS("form/form.css");
        Site::addJS("form/form.js");

        $result = "<form" . $form->getAttribs() . ">";

        if($form->isPostSafe()){
            $result.= "<input" .
                (new Html_Attribs())
                    ->setAttrib('type',     'hidden')
                    ->setAttrib('name',     '_form_post_safe')
                    ->setAttrib('value',    $form->getSafeHash()) .
                ">";
        }


        if(count($form->getElementsHidden())){
            foreach($form->getElementsHidden() as $k => $v){
                $result.= "<input" .
                    (new Html_Attribs())
                    ->setAttrib('type',     'hidden')
                    ->setAttrib('id',       $k)
                    ->setAttrib('name',     $k)
                    ->setAttrib('value',    $v) .
                ">";
            }
        }

        if(strlen($logo = $form->getLogo())){
            $result.= "<img src='" . $logo . "' class='ap_form_logo'>";
        }

        // Заголовок
        if(strlen($title = $form->getTitle())){
            $result.= "<div class='ap_form_title'>{$title}</div>";
        }

        // Заголовок
        if(strlen($description = $form->getDescription())){
            $result.= "<div class='ap_form_descr'>{$description}</div>";
        }

        // Общие ошибки
        if(count($form->getSuccessMessages())){
            $result.= "<div class='alert alert-success'>";
            foreach($form->getSuccessMessages() as $message){
                $result.= "<div>{$message}</div>";
            }
            $result.= "</div>";
        }

        // Общие ошибки
        if(count($form->getErrorMessages())){
            $result.= "<div class='alert alert-danger fade in'>";
            foreach($form->getErrorMessages() as $message){
                $result.= "<div>{$message}</div>";
            }
            $result.= "</div>";
        }

        $result.= "<table class='ap_form'>";

        if(count($els = $form->getElementsAll())){
            $currentBlock = null;

            Context::start($form->getValues());
            foreach($els as /** @var Form_Element_Abstract */ $el){
                if($el->getUse() && $el->isRenderStandard()){
                    if($currentBlock != $el->getBlock()){
                        if(strlen($currentBlock) && $form->getBlock($currentBlock) instanceof Form_Block){
                            // завершить блок под именем $currentBlock
                            $result.= $this->renderBlockEnd($form->getBlock($currentBlock));
                        }

                        if(strlen($el->getBlock()) && $form->getBlock($el->getBlock()) instanceof Form_Block){
                            // начать блок под именем $el->getBlock()
                            $result.= $this->renderBlockStart($form->getBlock($el->getBlock()));
                        }


                        $currentBlock = $el->getBlock();

                    }

                    $result.= $this->renderElement($el);
                }
            }
            Context::end();
        }

        if(count($form->getButtons())){
            $result.= "<tr class='ap_ftrbtn'><td class='ap_fc1'></td><td colspan='2'>";
            foreach($form->getButtons() as $btn){
                $result.= $btn->render() . " &nbsp; &nbsp; ";
            }
            $result.= "</td></tr>";
        }

        $result.= "</table></form>";

        return $result;
    }

    protected function renderBlockStart(Form_Block $block){
        if(!strlen($block->getTitle()) && !strlen($block->getDescription())){
            return "<tr><td colspan='3' class='ap_fbl_line'></td></tr>";
        }
        else {
            return "<tr class='ap_fbl_start'><td colspan='3'>" .
                (strlen($block->getTitle())       ? "<div class='ap_fbl_title'>" . $block->getTitle() . "</div>" : "") .
                (strlen($block->getDescription()) ? "<div class='ap_fbl_descr'>" . $block->getDescription() . "</div>" : "") .
            "</td></tr>";
        }
    }

    protected function renderBlockEnd(Form_Block $block){
        if($block->getEnded()){
            return "<tr><td colspan='3'></td></tr>";
        }
        return "";
    }

    protected function renderElement(Form_Element_Abstract $element){
        $errors = $element->getErrors();
        $errors_text = '';

        $tr_attr = new Html_Attribs();
        $tr_attr->setAttrib("id", "ap_form_tr_{$element->getName()}");

        // Если поле скрыто в зависимости от других, то скрыть его строку
        if($element->isConditionHide()){
            $tr_attr->setCSS("display", "none");
        }

        if(is_array($errors)){
            $tr_attr->addAttribPost("class", 'ap_error_tr');

            if(count($errors)){
                $errors_text = "<div class='ap_error_node'>";
                if(count($errors) == 1){
                    $errors_text.= current($errors);
                }
                else {
                    $errors_text.= "<ol style='list-style: circle'>";
                    foreach($errors as $error){
                        $errors_text.= "<li style='margin-bottom: 10px; color: #EE9C92'>
                            <span style='color: #DD4B39'>{$error}</span>
                        </li>";
                    }
                    $errors_text.= "</ol>";
                }
                $errors_text.= "</div>";
            }
        }

        if(!$element->isHover()){
            $tr_attr->setCSS("background", "none");
        }

        if($element->getRenderType() == self::ElementRenderType_BigValue){
            return "<tr{$tr_attr->render()}>" .
            "<td class='ap_fc1'>" . $element->getLabel() . "</td>" .
            "<td class='ap_fc2' colspan='2'>" .
                (strlen($element->getLabel()) ? "<div class='respLabel'>" . $element->getLabel() . "</div>" : "") .
                $element->render() .
                $errors_text .
                (strlen($element->getComment()) ? "<div class='ap_fcmnt'>" . $element->getComment() . "</div>" : "") .
            "</td>" .
            "</tr>";
        }
        else if($element->getRenderType() == self::ElementRenderType_FullLine){
            return "<tr{$tr_attr->render()}>" .
            "<td class='ap_fc2' colspan='3'>" .
                (strlen($element->getLabel()) ? "<div class='respLabel'>" . $element->getLabel() . "</div>" : "") .
                $element->render() .
                $errors_text .
                (strlen($element->getComment()) ? "<div class='ap_fcmnt'>" . $element->getComment() . "</div>" : "") .
            "</td>" .
            "</tr>";
        }
        else {
            return "<tr{$tr_attr->render()}>" .
               "<td class='ap_fc1'>" . $element->getLabel() . "</td>" .
               "<td class='ap_fc2'>" .
                    (strlen($element->getLabel()) ? "<div class='respLabel'>" . $element->getLabel() . "</div>" : "") .
                    $element->render() .
                    $errors_text .
                 (strlen($element->getComment()) ? "<div class='ap_fcmnt respComment'>" . $element->getComment() . "</div>" : "") .
                "</td>" .
               "<td class='ap_fc3'>" . $element->getComment() . "</td>" .
            "</tr>";
        }
    }
}