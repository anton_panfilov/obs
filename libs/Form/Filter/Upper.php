<?php

class Form_Filter_Upper extends Form_Filter_Abstract {
    public function filter($value){
        return strtoupper($value);
    }   
}