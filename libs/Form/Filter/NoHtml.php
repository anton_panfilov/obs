<?php

class Form_Filter_NoHtml extends Form_Filter_Abstract {
    public function filter($value){
        return str_replace(['<', '>'], ['&lt;', '&gt;'], $value);
    }   
}