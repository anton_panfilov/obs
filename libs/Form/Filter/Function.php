<?php

class Form_Filter_Function extends Form_Filter_Abstract {
    /**
     * @var callable
     */
    protected $function;

    function __construct(Closure $function){
        $this->function = $function;
    }

    public function filter($value){
        $function = $this->function;
        return $function($value);
    }   
}