<?php

class Form_Block {
    protected $name;
    protected $title;
    protected $description;
    protected $ended = true;
    
    public function __construct($name, $title = null){
        $this->name = $name; 
        $this->setTitle($title);           
    }
    
    public function setNoEnded(){
        $this->ended = false;
        return $this;    
    }
    
    public function setEnded(){
        $this->ended = true;
        return $this;    
    }
    
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }
    
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }
    
    /***********************/
    
    public function getEnded(){
        return $this->ended;
    }
    
    public function getTitle(){
        return $this->title;
    }
    
    public function getDescription(){
        return $this->description;   
    }
}