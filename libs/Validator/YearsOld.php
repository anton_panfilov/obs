<?php

/**
 * Class Validator_Date
 *
 *  Минимальное и максимальное количесво лет в дате с форматом YYYY-MM-DD
 * А также проверка самой даты
 *
 */

class Validator_YearsOld extends Validator_Abstract {

    const FORMAT        = 'format';
    const INVALID_DATE  = 'invalid_date';
    const MIN           = 'min';
    const MAX           = 'max';

    protected $_messages = array(
        self::FORMAT        => 'Invalid date format. Use: YYYY-MM-DD',
        self::INVALID_DATE  => 'Invalid date',
        self::MIN           => 'Minimum age {min} years',
        self::MAX           => 'Maximum age {max} years',
    );

    /*****************************/

    protected $min;
    protected $max;

    public function getSettings(){
        return [
            'min' => $this->min,
            'max' => $this->max,
        ];
    }

    public function __construct($min = null, $max = null, $minMessage = null, $maxMessage = null){
        $this->min = is_null($min) ? null : (int)$min;
        $this->max = is_null($max) ? null : (int)$max;

        if($minMessage){
            $this->_messages[self::MIN] = $minMessage;
        }

        if($maxMessage){
            $this->_messages[self::MAX] = $maxMessage;
        }
    }

    /*****************************/

    public function isValid($value, $context = []){
        if(strlen($value) == 10){
            list($year, $month, $day) = array(substr($value, 0, 4), substr($value, 5, 2), substr($value, 8, 2));
            if(
                $year  === sprintf("%04d", (int)$year) &&
                $month === sprintf("%02d", (int)$month) &&
                $day   === sprintf("%02d", (int)$day)
            ){
                if(checkdate($month, $day, $year)){
                    list($currentYear, $currentMonth, $currentDay) = sscanf(date("Y-m-d"), '%d-%d-%d');
                    $age = $currentYear - $year;
                    if($currentMonth < $month || ($currentMonth == $month && $currentDay < $day)){
                        $age--;
                    }

                    if($this->min !== null && $age < $this->min){
                        $this->_error(self::MIN, array(
                            'min' => $this->min
                        ));
                        return false;
                    }

                    if($this->max !== null && $age > $this->max){
                        $this->_error(self::MAX, array(
                            'max' => $this->max
                        ));
                        return false;
                    }

                    return true;
                }
                else {
                    $this->_error(self::INVALID_DATE);
                    return false;
                }
            }
        }

        $this->_error(self::FORMAT);
        return false;
    }
}