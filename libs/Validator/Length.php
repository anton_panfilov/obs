<?php

/**
 * Class Validator_Length
 *
 * Првоерка длинны строки
 */

class Validator_Length extends Validator_Abstract {

    const MIN = 'min';
    const MAX = 'max';

    protected $_messages = array(
        self::MIN => 'Invalid string length, a minimum of {min}',
        self::MAX => 'Invalid string length, a maximum of {max}',
    );

    /*****************************/

    protected $min;
    protected $max;

    public function getSettings(){
        return [
            'min' => $this->min,
            'max' => $this->max,
        ];
    }

    public function __construct($min = null, $max = null, $minMessage = null, $maxMessage = null){
        $this->min = is_null($min) ? null : (int)$min;
        $this->max = is_null($max) ? null : (int)$max;
       
        if($minMessage){
            $this->_messages[self::MIN] = $minMessage;
        }
        
        if($maxMessage){
            $this->_messages[self::MAX] = $maxMessage;
        }
    }

    /*****************************/
    
    public function isValid($value, $context = []){
        $length = mb_strlen($value, 'utf-8');
        
        if($this->min !== null && $length < $this->min){
            $this->_error(self::MIN, array(
                'min' => $this->min
            ));
            return false;    
        }
        
        if($this->max !== null && $length > $this->max){
            $this->_error(self::MAX, array(
                'max' => $this->max
            ));     
            return false;
        }
        
        return true;
    }   
}