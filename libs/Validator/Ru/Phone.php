<?php

class Validator_Ru_Phone extends Validator_Abstract {
    const DIGITS        = 'digits';
    const LENGTH        = 'length';
    const AREA          = 'area';
    const EXT_DIGITS    = 'ext_digits';
    const EXT_LENGTH    = 'ext_length';

    protected $_messages = array(
        self::DIGITS        => 'Phone number can only digits',
        self::LENGTH        => 'Length phone number must be 10 characters',
        self::AREA          => 'Invalid area code',
        self::EXT_DIGITS    => 'Extension number can only digits',
        self::EXT_LENGTH    => 'Extension number must be 1 - 6 characters',
    );

    protected $extension = false;

    /**
     * @param $flag
     * @return $this
     */
    public function setExtension($flag){
        $this->extension = (bool)$flag;
        return $this;
    }

    /*****************************/

    public function isValid($value, $context = []){
        $ext = "";
        if($this->extension){
            $a = explode(",", $value, 2);
            $value = $a[0];

            if(isset($a[1])){
                $ext = $a[1];
            }
        }

        if(!ctype_digit($value)){
            $this->_error(self::DIGITS);
            return false;
        }

        if(strlen($value) != 10){
            $this->_error(self::LENGTH);
            return false;
        }

        $first = substr($value, 0, 1);
        if(!in_array($first, ['9', '8', '4', '3'])){
            $this->_error(self::AREA);
            return false;
        }

        if(strlen($ext)){
            if(!ctype_digit($ext)){
                $this->_error(self::EXT_DIGITS);
                return false;
            }

            if(strlen($ext) < 1 || strlen($ext) > 6){
                $this->_error(self::EXT_LENGTH);
                return false;
            }
        }

        return true;
    }
}