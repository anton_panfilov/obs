<?php

class Validator_UniqueInDatabase extends Validator_Abstract {

    const USED = 'use';

    protected $_messages = array(
        self::USED => 'Already in use',
    );

    /*****************************/

    /**
     * @var Db_Connection_Abstract
     */
    protected $db;

    protected $query;
    
    public function __construct($query, Db_Connection_Abstract $database, $errorMessage = null){
        $this->db    = $database;
        $this->query = $query;
        
        if($errorMessage){
            $this->_messages[self::USED] = $errorMessage;
        }
    }

    /*****************************/

    public function isValid($value, $context = []){
        if($this->db->fetchOne($this->query, $value) !== false){
            $this->_error(self::USED, [
                'value' => $value,
            ]);
            return false;
        }
        return true;
    }   
}