<?php

/**
 * Class Validator_Email
 *
 * Првоерка email по простой регулярке
 */

class Validator_Email extends Validator_Regexp {
    public function __construct($errorMessage = null){
        // значнеие по умолчанию
        $this->_messages[self::REGEXP] = 'Invalid email';

        // инизилизация регулярки

        parent::__construct(
            '/^' .
            '[-a-z0-9!#$%&\'*+\_`{|}~]+(?:\.[-a-z0-9!#$%&\'*+\_`{|}~]+)*' .
            '@' .
            '([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*([a-z]{1,6})' .
            '$/i',
            $errorMessage
        );

        /*
         * // utf
        parent::__construct(
            '/^' .
            '[-\w!#$%&\'*+`{|}~]+(?:\.[-\w!#$%&\'*+`{|}~]+)*' .
            '@' .
            '([\w]([-\w]{0,61}[\w])?\.)*([\w]{1,10})' .
            '$/iu',
            $errorMessage
        );
        */
    }
}