<?php

/**
 * Class Validator_Date
 *
 *  Дата в формате YYYY-MM-DD
 *
 */

class Validator_Date extends Validator_Abstract {
    const FORMAT        = 'format';
    const INVALID_DATE  = 'invalid_date';

    protected $_messages = array(
        self::FORMAT        => 'Invalid date format. Use: YYYY-MM-DD',
        self::INVALID_DATE  => 'Invalid date',
    );

    /*****************************/

    public function isValid($value, $context = []){
        if(strlen($value) == 10){
            list($year, $month, $day) = array(substr($value, 0, 4), substr($value, 5, 2), substr($value, 8, 2));
            if(
                $year  === sprintf("%04d", (int)$year) &&
                $month === sprintf("%02d", (int)$month) &&
                $day   === sprintf("%02d", (int)$day)
            ){
                if(checkdate($month, $day, $year)){
                    return true;
                }
                else {
                    $this->_error(self::INVALID_DATE);
                    return false;
                }
            }
        }

        $this->_error(self::FORMAT);
        return false;
    }
}