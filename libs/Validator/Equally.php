<?php

/**
 * Class Validator_Equally
 *
 * Проверяеться точное совпадение с определенным значением
 */

class Validator_Equally extends Validator_Abstract {

    const NO_EQUALLY = 'no_qually';

    protected $_messages = array(
        self::NO_EQUALLY => 'Invalid value',
    );

    /*****************************/

    protected $value;

    public function __construct($value, $message = null){
        $this->value = $value;
        if($message){
            $this->_messages[self::NO_EQUALLY] = $message;
        }
    }

    public function isValid($value, $context = []){
        if($value != $this->value){
            $this->_error(self::NO_EQUALLY, ['const' => $this->value]);
            return false;
        }
        return true;
    }
}