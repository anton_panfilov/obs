<?php

/**
 * Class Validator_NumericBetween
 *
 * Проверка числа на минимальное и максимальное значение
 */

class Validator_NumericBetween extends Validator_Abstract {

    const MIN = 'min';
    const MAX = 'max';

    protected $_messages = array(
        self::MIN => 'Invalid numeric value, a minimum of {min}',
        self::MAX => 'Invalid numeric value, a maximum of {max}',
    );

    /*****************************/

    protected $min;
    protected $max;

    public function getSettings(){
        return [
            'min' => $this->min,
            'max' => $this->max,
        ];
    }

    public function __construct($min = null, $max = null, $minMessage = null, $maxMessage = null){
        $this->min = is_null($min) ? null : (int)$min;
        $this->max = is_null($max) ? null : (int)$max;
       
        if($minMessage){
            $this->_messages[self::MIN] = $minMessage;
        }
        
        if($maxMessage){
            $this->_messages[self::MAX] = $maxMessage;
        }
    }

    /*****************************/
    
    public function isValid($value, $context = []){
        if($this->min !== null && $value < $this->min){
            $this->_error(self::MIN, array(
                'min' => $this->min
            )); 
            return false;    
        }
        
        if($this->max !== null && $value > $this->max){
            $this->_error(self::MAX, array(
                'max' => $this->max
            ));     
            return false;
        }
        
        return true;
    }   
}