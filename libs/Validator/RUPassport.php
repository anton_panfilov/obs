<?php

class Validator_RUPassport extends Validator_Abstract {
    const INVALID        = 'bad';
    const NODATA         = 'nodata';

    protected $_messages = array(
        self::INVALID        => 'Passport Expired or Invalid',
        self::NODATA         => 'No Passport Series or Number'
    );

    /*****************************/

    public function isValid($value, $context = []){
        if(!empty($context['series']) && !empty($value)){
            //$passport = (int)$context['series'].$value;
            $passp_series = (int)$context['series'];
            $passp_number = (int)$value;
            if($exists = Db::processing()->fetchOne("SELECT PASSP_NUMBER FROM invalid_passports WHERE PASSP_SERIES = {$passp_series} AND PASSP_NUMBER = {$passp_number}")){
                $this->_error(self::INVALID);
                return false;
            }
        }else{
            $this->_error(self::NODATA);
            return false;
        }
        return true;
    }
}