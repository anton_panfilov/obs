<?php

/**
 * Class Validator_Regexp
 *
 * Првоерка по регулярному выражению
 */

class Validator_Regexp extends Validator_Abstract {

    const REGEXP = 'regexp';

    protected $_messages = array(
        self::REGEXP => 'Invalid value',
    );

    /*****************************/

    protected $pattern;
    
    public function __construct($pattern, $errorMessage = null){
        $this->pattern = $pattern;
        if($errorMessage){
            $this->_messages[self::REGEXP] = $errorMessage;
        }
    }

    /*****************************/

    public function isValid($value, $context = []){
        if(!preg_match($this->pattern, $value)){
            $this->_error(self::REGEXP);
            return false;
        }
        return true;
    }   
}