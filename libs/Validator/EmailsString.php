<?php

/**
 * Class Validator_EmailsString
 *
 * Првоерка строки с перечисленными эл. адресами.
 * формат перечисления достаточно свободный, реализован в Email_Functions::parseEmailString
 */

class Validator_EmailsString extends Validator_Abstract {

    const PARSE_EMAILS = 'regexp';

    protected $_messages = array(
        self::PARSE_EMAILS => 'Invalid emails string',
    );

    /*****************************/

    public function isValid($value, $context = []){
        $res = Email_Functions::parseEmailString($value);

        if(!count($res)){
            $this->_error(self::PARSE_EMAILS);
            return false;
        }
        return true;
    }
}