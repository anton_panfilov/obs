<?php

class Boot {
    const DEBUG_MODE_KEY = 'nub5vcer6v7b8n9m0';

    //////////////////////////////////

    const MODE_REGULAR  = 1;
    const MODE_CRON     = 2;

    static protected $mode = self::MODE_REGULAR;

    //////////////////////////////////

    /**
     * microtime начала работы ядра (можно считать началом работы скрипта)
     *
     * @var float
     */
    static protected $start;

    /**
     * Использованно процессора на начало работы
     *
     * @var float
     */
    static protected $startCPUTime;

    /**
     * Массив PHP ошибок
     *
     * @var array
     */
    static protected $phpErrors = [];

    static public $dir_root;
    static public $dir_api;
    static public $dir_conf;
    static public $dir_libs;

    /************************************/

    static public function setModeCron(){
        self::$mode = self::MODE_CRON;
    }

    static public function isModeCron(){
        return (self::$mode == self::MODE_CRON);
    }

    /************************************/

    /**
     * Загрузка ядра
     *
     * 1. main dirs path
     * 2. include path
     * 3. set auto load
     */
    static public function installCore(){
        self::$start = microtime(1);

        if(function_exists("getrusage")){
            $dat = getrusage();
            if(isset($dat["ru_utime.tv_sec"], $dat["ru_utime.tv_usec"])){
                self::$startCPUTime = $dat["ru_utime.tv_sec"] * 1e6 + $dat["ru_utime.tv_usec"];
            }
        }

        // отображение ошибок
        ini_set('display_errors', self::isShowErrors());
        set_time_limit(600);

        // Реальный IP
        if(
            isset($_SERVER['HTTP_X_REALIP']) &&
            filter_var($_SERVER['HTTP_X_REALIP'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 | FILTER_FLAG_IPV4)
        ){
            $_SERVER['CONNECT_IP']  = $_SERVER['REMOTE_ADDR'];   // сохранение IP внутренней сети в отдельную переменную
            $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_REALIP'];
        }

        // разные настройки PHP
        mb_internal_encoding('UTF-8');

        // задание путей для папок
        self::$dir_root     = realpath(__DIR__ . "/../");
        self::$dir_api      = self::$dir_root . "/api";
        self::$dir_conf     = self::$dir_root . "/conf";
        self::$dir_libs     = self::$dir_root . "/libs";

        // подключение папок для инклуда
        set_include_path(implode(PATH_SEPARATOR, [
            self::$dir_libs,
        ]));

        // ловушка для ошибок
        set_error_handler(['Boot', 'globalErrorHandler']);

        // включение автолоадера
        spl_autoload_register('Boot::autoload');

        // установка таймзоны
        if(Conf::main()->timezone){
            date_default_timezone_set(Conf::main()->timezone);
        }

        // ловушка завершения скрипта
        register_shutdown_function(array('Boot', 'coreShutdownTrigger'));
    }

    /**
     * Количество секунд, которое работал скрипт
     *
     * @return float
     */
    static public function getRuntime(){
        return microtime(1) - self::$start;
    }

    /**
     * Метка начала работы скрипта
     *
     * @return float
     */
    static public function getStartMicrotime(){
        return self::$start;
    }

    /**
     * Использование процессора в процентах от общей работы скрипта
     *
     * @param bool $int
     * @return mixed|null|string
     */
    static public function getCPUUsage($int = false){
        if(function_exists("getrusage")){
            $dat = getrusage();
            if(isset($dat["ru_utime.tv_sec"], $dat["ru_utime.tv_usec"])){
                $time = self::getRunTime() * 1000000;

                // cpu per request
                if($time > 0) {
                    $dat["ru_utime.tv_usec"] = ($dat["ru_utime.tv_sec"]*1e6 + $dat["ru_utime.tv_usec"]) - self::$startCPUTime;

                    return $int ?
                        (
                            min(100, (int)($dat["ru_utime.tv_usec"] / $time) * 100)
                        )
                        :
                        (
                            sprintf("%01.2f",  min(100, ($dat["ru_utime.tv_usec"] / $time) * 100)) . " % / " .
                            sprintf("%0.4f sec", $dat["ru_utime.tv_usec"] / 1000000)
                        );
                }
            }
        }


        return $int ? null : "?";
    }

    /**
     * Время работы базы от общего времени работы скрипта
     *
     * @param bool $int
     * @return null|string
     */
    static public function getDatabaseUsage($int = false){
        if(self::getRuntime() > 0){
            return $int ?
                Db::getDatabaseRuntime() :
                (
                    sprintf("%01.2f",  (Db::getDatabaseRuntime() / self::getRuntime()) * 100) . " % / " .
                    sprintf("%0.4f sec", Db::getDatabaseRuntime())
                ) ;
        }

        return $int ? null : "?";
    }

    /**
     * Показывать ли системные ошибки пользователю
     *
     * @return bool
     */
    static public function isShowErrors(){
        // Включение в файле: /scripts/debug_mode.php

        if(isset($_COOKIE['debug_mode_key']) && $_COOKIE['debug_mode_key'] == self::DEBUG_MODE_KEY){
            return true;
        }
        return false;
    }

    /**
     * Показывать блок с дополнительной информацияе внизу
     *
     * @return bool
     */
    static public function isShowDebugBlock(){
        if(
            self::isShowErrors() &&
            isset($_COOKIE['show_debug_block']) &&
            $_COOKIE['show_debug_block']
        ){
            return true;
        }

        return false;
    }

    /**
     * Получить массив PHP ошибок
     *
     * @return array
     */
    static public function getPhpErrors(){
        return self::$phpErrors;
    }



    /**
     * Загрузка сайта для текущего запроса пользователя.
     * Вызываеться в файле index.php
     */
    static public function installSite(){
        self::installCore();
        register_shutdown_function(array('Boot', 'siteShutdownTrigger'));
        echo Controller_Boot::run();
    }


    static public function siteShutdownTrigger(){
        $last = error_get_last();

        if(
            is_array($last) &&
            isset($last['type']) &&
            in_array($last['type'], array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR))
        ){
            try {

            }
            catch(Exception $e){}

            try {
                echo new View_Page_Error_Error500();
            }
            catch(Exception $e){}
        }
    }

    static public function coreShutdownTrigger(){
        $last = error_get_last();

        if(
            is_array($last) &&
            isset($last['type']) &&
            in_array($last['type'], array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR, E_USER_ERROR, E_RECOVERABLE_ERROR))
        ){
            try {
                // site fatal error log

                Db::site()->insert("fatal_errors", [
                    'runtime'   => Boot::getRuntime(1),
                    'cpu_usage' => Boot::getCPUUsage(1),
                    'errno'     => $last['type'],
                    'message'   => isset($last['message']) ? $last['message'] : "",
                    'file'      => isset($last['file']) ? $last['file'] : "",
                    'line'      => isset($last['line']) ? $last['line'] : "",
                ]);
            }
            catch(Exception $e){}
        }
    }

    /**
     * Функция для автозагрузки классов
     *
     * @param $className
     */
    static protected function autoload($className){
        $className = ltrim($className, '\\');
        $fileName  = '';
        if ($lastNsPos = strripos($className, '\\')) {
            $className = substr($className, $lastNsPos + 1);
            $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, substr($className, 0, $lastNsPos)) . DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

        $include_paths = explode(PATH_SEPARATOR, get_include_path());
        if(count($include_paths)){
            foreach ($include_paths as $path) {
                $include = $path . DIRECTORY_SEPARATOR . $fileName;
                if (is_file($include) && is_readable($include)) {
                    /** @noinspection PhpIncludeInspection */
                    include_once $fileName;
                    break;
                }
            }
        }
    }

    /**
     * Системная функция для учета PHP ошибок
     *
     * @param $errno
     * @param $errmsg
     * @param $file
     * @param $line
     * @param $errcontext
     * @return bool
     */
    static public function  globalErrorHandler($errno, $errmsg, $file, $line, $errcontext){
        if (!(error_reporting() & $errno)) {
            // Этот код ошибки не включен в error_reporting
            return false;
        }

        $type = Base_PhpError::getErrorLabel($errno);

        if(self::isShowErrors()){
            echo "<div style='margin:5px; font-size:140%'>
                <strong>{$type}:</strong>
                <span style='color:#A00'>{$errmsg}</span>:
                <span style='color:#777'>{$file}</span>
                ({$line})
            </div>";
        }

        self::$phpErrors[] = array(
            'type'      => $type,
            'errno'     => $errno,
            'errmsg'    => $errmsg,
            'file'      => $file,
            'line'      => $line,
            'context'   => $errcontext,
        );

        return true;
    }


    static public function newVersionDate(){
        return "2015-04-23 15:50:00";
    }

    static public function isDev(){
        return is_array($h = explode(".", $_SERVER['SERVER_NAME'])) && count($h) > 1 && $h[count($h) - 1] == 'dev';
    }
}