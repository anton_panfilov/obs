<?php

class Encryption {
    const PASS      = 'nG54BS87aw6eb35F6bNeDFsA5v687NRT';
    const IV        = 'n3O4bMDS1F9v9w3E';
    const PHONE_KEY = 76958508529;
    const INT_KEY   = 487593154;

    /***********************************************************/

    static public function encrypt($string){
        return openssl_encrypt($string, 'AES-256-CFB', self::PASS, true, self::IV);
    }

    static public function decrypt($raw){
        return openssl_decrypt($raw, 'AES-256-CFB', self::PASS, true, self::IV);
    }

    /***********************************************************/

    static public function encryptPhoneNumber($number){
        return (int)$number + self::PHONE_KEY;
    }

    static public function decryptPhoneNumber($cryptNumber){
        return (int)$cryptNumber - self::PHONE_KEY;
    }

    /***********************************************************/

    static public function encryptInt($int){
        $int = (string)(int)$int;
        $new = substr($int, 0, 1);

        for($i = 1; $i < strlen($int); $i++){
            $new.= ((int)substr($int, $i, 1) + (int)substr(self::INT_KEY, $i % strlen(self::INT_KEY), 1)) % 10;
        }

        return $new;
    }

    static public function decryptInt($int){
        $int = (string)(int)$int;
        $new = substr($int, 0, 1);

        for($i = 1; $i < strlen($int); $i++){
            $t = ((int)substr($int, $i, 1) - (int)substr(self::INT_KEY, $i % strlen(self::INT_KEY), 1));
            if($t < 0) $t+= 10;
            $new.= $t % 10;
        }

        return $new;
    }
}