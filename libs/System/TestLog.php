<?php

class System_TestLog {
    /**
     * Записать данные в таблицу logs.system_test_log
     * Используеться для различных тестов
     *
     * @param $details
     */
    static public function add($details){
        $filename = '';
        $trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 1);
        if(isset($trace[0]['file'], $trace[0]['line'])){
            $filename = "{$trace[0]['file']}:{$trace[0]['line']}";
        }

        $insert = [
            'filename' => $filename,
            'details'  => print_r($details, 1),
        ];

        Db::site()->insert('system_test_log', $insert);
    }
}