<?php

class System_Warnings {
    static public function add($details){
        try {
            $trace_string = '';
            $traces = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 0);
            if(count($traces)){
                foreach($traces as $trace){
                    if(isset($trace['class'], $trace['function'], $trace['type'])){
                        $trace_string.= "{$trace['class']}{$trace['type']}{$trace['function']}";
                    }
                    else if(isset($trace['function'])){
                        $trace_string.= "{$trace['function']}";
                    }
                    else {
                        $trace_string.= "???";
                    }

                    $trace_string.= " called";

                    if(isset($trace['file'], $trace['line'])){
                        $trace_string.= " at [{$trace['file']}:{$trace['line']}]";
                    }

                    $trace_string.= "\n";
                }
            }

            $insert = [
                'trace'     => $trace_string,
                'env'       => var_export($_SERVER, 1),
                'details'   => $details,
            ];


            Db::site()->insert('system_warnings', $insert);
        }
        catch(Exception $e){

        }
    }
}