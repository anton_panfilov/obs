<?php

/*
Контекст - это статический массив переменных.
Удобено использовать если доступ к переменным нужен из разных участоков не связанного кода.
Конектст пооддерживает вложенность

| start(context 1)
|
|  | start(context 2)
|  |
|  | end(context 2)
|
| end(context 1)


--------------------------------
Пример использования:
-------

function test_values($title, $value1, $value2){
    echo "
        <h2>{$title}</h2>
        Value1 = {$value1} <br>
        Value2 = {$value2} <br><br>
    ";
}

// задачем значения, которые будут получать данные из контекста
$value1 = Context::value('11');
$value2 = Context::pattern('{11} - {city}');

test_values('Before context start', $value1, $value2);

Context::start(array(
    11 => 'Anton',
));

test_values('Point 2', $value1, $value2);

Context::start(array(
    11 => 'John',
    'city' => 'LA',
));

test_values('Point 3', $value1, $value2);

// завершение второго
Context::end();

test_values('Point 4', $value1, $value2);

// завершение первого контектста
Context::end();

test_values('Point 5', $value1, $value2);

*/

class Context {
    static protected $context = array();

    /**
     * Задать контекст
     *
     * @param mixed $context
     */
    static public function start(array $context){
        self::$context[] = $context;
    }

    /**
     * Завершить работу с последним контекстом
     */
    static public function end(){
        unset(self::$context[count(self::$context) - 1]);
        self::$context = array_values(self::$context);
    }

    /**
     * Получить значение из контекста
     *
     * @param string $name
     * @param $default
     * @return null
     */
    static public function get($name, $default = null){
        return isset(self::$context[count(self::$context) - 1][(string)$name]) ?
            self::$context[count(self::$context) - 1][(string)$name] :
            $default;
    }

    /**
     * Получить все значения контекста
     * Если контекст не определен, будет получен пустой массив
     *
     * @return array
     */
    static public function getArray(){
        return @self::$context[count(self::$context) - 1] ?: array();
    }

    /*************************************************************************************
    * Разные типы переменных
    */

    /**
     * Получить объект, который будет получать одно значение из конекста
     *
     * @param string $name
     * @param mixed $default
     * @return Context_Value
     */
    static public function value($name, $default = null){
        return new Context_Value($name, $default);
    }

    /**
     * Получить объект, который будеть патерн на основе контектса
     *
     * @param string $pattern
     * @return Context_Pattern
     */
    static public function pattern($pattern){
        // запрет на паттерн от паттерна
        if($pattern instanceof Context_Abstract) return $pattern;

        return new Context_Pattern($pattern);
    }
}