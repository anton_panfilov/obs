<?php

class Captcha {
    static public $symbols = '23456789abcdeghkmnpqsuvxyz';

    /**********************************************************************************************/

    /**
     * Получить имя, котрое каптча будет использовать для куки, через которую она работает
     * Имя зависит от имени капчи, ну используется crc32 хеш, потому что
     * нельзя делать слишком длинные имена для куков, а имя иногда мождет быть длинным
     *
     * @param string $name
     * @return string
     */
    static public function getCookieName($name){
        return 'CPTH_' . sprintf('%u', crc32($name));
    }

    /**
     * Получить имя, котрое каптча будет использовать для куки, через которую она работает
     * Имя зависит от имени капчи, ну используется crc32 хеш, потому что
     * нельзя делать слишком длинные имена для куков, а имя иногда мождет быть длинным
     *
     * @param string $name
     * @return string
     */
    static public function getCookieSessionName($name){
        return 'CPTH_OC_' . sprintf('%u', crc32($name));
    }

    /**********************************************************************************************/

    /**
     * Получить хеш, основанный на первом ключе (актуальный еще 10 - 20 мин)
     *
     * @param $name
     * @param $value
     * @param null $salt
     * @return string
     */
    static public function getCookieHash1($name, $value, $salt = null){
        return self::getCookieHashAbstract(
            mb_strtolower($name) . trim(mb_strtolower($value)) . Db::site()->fetchOne("select `key1` from `captcha_keys`"),
            $salt
        );
    }

    /**
     * Получить хеш, основанный на втором (актуальный еще 0 - 10 мин)
     *
     * @param $name
     * @param $value
     * @param null $salt
     * @return string
     */
    static public function getCookieHash2($name, $value, $salt = null){
        return self::getCookieHashAbstract(
            mb_strtolower($name) . trim(mb_strtolower($value)) . Db::site()->fetchOne("select `key2` from `captcha_keys`"),
            $salt
        );
    }

    static protected function getCookieHashAbstract($string, $salt = null){
        return crypt(
            $string,
            is_null($salt) ?
                '$1$' . String_Hash::randomString(8) . '$' :
                (string)$salt
        );
    }

    /**********************************************************************************************/


    static public function isValid($name, $value = null){
        $value = trim($value);

        if(strlen($value) && isset($_COOKIE[self::getCookieName($name)])){
            if(
                self::getCookieHash1($name, $value, $_COOKIE[self::getCookieName($name)]) == $_COOKIE[self::getCookieName($name)] ||
                self::getCookieHash2($name, $value, $_COOKIE[self::getCookieName($name)]) == $_COOKIE[self::getCookieName($name)]
            ){
                return true;
            }
        }

        return false;
    }

    /**
     * Отрисовка html блока в котором будет графическая капча
     *
     * @param string $name
     * @param string $backgroundColor
     * @return string
     */
    static function renderCaptchaHtmlBlock($name = 'default', $backgroundColor = 'FFFFFF'){
        try {
            $int = hexdec($backgroundColor);

            $int1 = floor($int / 65536);
            $int2 = floor(($int - $int1*65536) / 256);
            $int3 = $int - $int1*65536 - $int2*256;

            if($int1 >= 0 && $int1 <= 255 && $int2 >= 0 && $int2 <= 255 && $int3 >= 0 && $int3 <= 255){
                $backgroundColor = array($int1, $int2, $int3, $backgroundColor);
            }
            else {
                $backgroundColor = array(255, 255, 255, 'FFFFFF');
            }
        }
        catch(Exception $e){
            $backgroundColor = array(255,  255, 255, 'FFFFFF');
        }

        $img = Http_Url::convert(
                [
                    'name' => $name,
                    'background' => $backgroundColor[3]
                ],
                '//' . $_SERVER['HTTP_HOST'] . '/api/captcha/img.jpg'
            ) . "&" . rand(10000000, 99999999);

        return
        "<table border='0' cellpadding='0' cellspacing='0' class='capche'>" .
        "<tr>" .
        "<td width='150' style='border:0px; padding:0 10px 0 10px; background: url(/static/img/load/89x30.cyrc.gif) center center no-repeat;'>" .
        "<img id='acapche_{$name}' src='{$img}' alt='CAPCHE' height='60' width='150'  style='border-radius: 30px;'/>" .
        "</td>" .
        "<td width='50' style='border:0px;color:#999;font-size:30px;text-align:left;vertical-align:middle;padding:0 10px 0 10px;'>" .
        "&raquo;" .
        "</td>" .
        "<td width='110' style='border:0px;vertical-align:top;padding:15px 10px 0 10px;'>" .
        "<input autocomplete='off' name='{$name}' id='{$name}' type='text' size='7' maxlength='7' style='font-size:24px; height: 35px; width: 120px' />" .
        "</td>" .
        "</tr>" .
        "<tr>" .
        "<td style='text-align:center'>" .
        "<a " .
        "style='cursor:pointer' " .
        "onclick=\"document.getElementById('acapche_{$name}').src='data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';document.getElementById('acapche_{$name}').src='{$img}&'+Math.floor(Math.random()*1000000);\"" .
        ">" . Translate::t("Not readable? Change text") . "</a>" .
        "</td><td colspan='2'></td>" .
        "</tr>" .
        "</table>";
    }

    /**
     * Отрисовать капчу (картинку)
     * 1. Создается новый код для капчи
     * 2. Деалется новая кука, для распознования этого кода
     * 3. Делается каринка
     *
     * @param mixed $name
     * @param mixed $backgroundColor
     */
    static public function renderCaptcha($name = 'default', $backgroundColor = 'FFFFFF'){
        // background
        try {
            $int = hexdec($backgroundColor);

            $int1 = floor($int / 65536);
            $int2 = floor(($int - $int1*65536) / 256);
            $int3 = $int - $int1*65536 - $int2*256;

            if($int1 >= 0 && $int1 <= 255 && $int2 >= 0 && $int2 <= 255 && $int3 >= 0 && $int3 <= 255){
                $backgroundColor = array($int1, $int2, $int3, $backgroundColor);
            }
            else {
                $backgroundColor = array(255, 255, 255, 'FFFFFF');
            }
        }
        catch(\Exception $e){
            $backgroundColor = array(255,  255, 255, 'FFFFFF');
        }

        // создание капчи
        $key = String_Hash::randomString(7, self::$symbols);

        setcookie(
            static::getCookieName($name),
            static::getCookieHash1($name, $key),
            time() + 60 * 10,
            "/",
            $_SERVER['HTTP_HOST']
        );

        // create Image
        $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz"; # do not change without changing font files!

        # folder with fonts
        $fontsdir_absolute = __DIR__ . "/Fonts/png/";

        # CAPTCHA string length
        //$length = mt_rand(5,6); # random 5 or 6
        $length = strlen($key);

        # CAPTCHA image size (you do not need to change it, whis parameters is optimal)
        $width = 150;
        $height = 60;

        # symbol's vertical fluctuation amplitude divided by 2
        $fluctuation_amplitude = 4;

        # increase safety by prevention of spaces between symbols
        $no_spaces = true;

        # show credits
        $show_credits = false; # set to false to remove credits line. Credits adds 12 pixels to image height
        $credits = $_SERVER['HTTP_HOST']; # if empty, HTTP_HOST will be shown

        # CAPTCHA image colors (RGB, 0-255)
        //$foreground_color = array(0, 0, 0);
        $background_color = $backgroundColor;

        $colorSum = $backgroundColor[0] + $backgroundColor[1] + $backgroundColor[2];

        if($colorSum > 600){
            $foreground_color = array(mt_rand(0,150), mt_rand(0,150), mt_rand(0,150));
        }
        else if($colorSum > 384){
            $foreground_color = array(mt_rand(0,100), mt_rand(0,100), mt_rand(0,100));
        }
        else {
            $foreground_color = array(mt_rand(180,255), mt_rand(180,255), mt_rand(180,255));
        }

        # JPEG quality of CAPTCHA image (bigger is better quality, but larger file size)
        $jpeg_quality = 70;

        $fonts=array();
        if ($handle = opendir($fontsdir_absolute)) {
            while (false !== ($file = readdir($handle))) {
                if (preg_match('/\.png$/i', $file)) {
                    $fonts[]= $fontsdir_absolute . $file;
                }
            }
            closedir($handle);
        }

        $alphabet_length = strlen($alphabet);

        while(true){
            $font_file = $fonts[mt_rand(0, count($fonts)-1)];
            $font = imagecreatefrompng($font_file);

            imagealphablending($font, true);

            $fontfile_width     =   imagesx($font);
            $fontfile_height    =   imagesy($font)-1;
            $font_metrics       =   array();
            $symbol             =   0;
            $reading_symbol     =   false;

            // loading font
            for($i=0;$i<$fontfile_width && $symbol<$alphabet_length;$i++){
                $transparent = (imagecolorat($font, $i, 0) >> 24) == 127;

                if(!$reading_symbol && !$transparent){
                    $font_metrics[$alphabet{$symbol}]=array('start'=>$i);
                    $reading_symbol=true;
                    continue;
                }

                if($reading_symbol && $transparent){
                    $font_metrics[$alphabet{$symbol}]['end']=$i;
                    $reading_symbol=false;
                    $symbol++;
                    continue;
                }
            }

            $img=imagecreatetruecolor($width, $height);
            imagealphablending($img, true);
            $white=imagecolorallocate($img, 255, 255, 255);

            imagefilledrectangle($img, 0, 0, $width-1, $height-1, $white);

            // draw text
            $x = 1;
            for($i = 0; $i < $length; $i++){

                $m = $font_metrics[substr($key, $i, 1)];

                $y = mt_rand(
                        -$fluctuation_amplitude,
                        $fluctuation_amplitude
                    ) + ($height - $fontfile_height) / 2 + 2;

                if($no_spaces){
                    $shift = 0;
                    if($i > 0){
                        $shift = 1000;
                        for($sy = 7; $sy < $fontfile_height - 20; $sy += 1){
                            for($sx = $m['start'] - 1; $sx < $m['end']; $sx+=1){
                                $rgb = imagecolorat($font, $sx, $sy);
                                $opacity = $rgb >> 24;

                                if($opacity < 127){
                                    $left = $sx - $m['start'] + $x;
                                    $py = $sy + $y;

                                    if($py > $height) break;

                                    for($px = min($left, $width - 1); $px > $left - 12 && $px >=0 ; $px-=1){
                                        $color = imagecolorat($img, $px, $py) & 0xff;

                                        if($color + $opacity < 190){
                                            if($shift > $left - $px){
                                                $shift = $left - $px;
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                        if($shift == 1000){
                            $shift = mt_rand(4,6);
                        }
                    }
                }
                else{
                    $shift = 1;
                }

                imagecopy(
                    $img,
                    $font,
                    $x - $shift,
                    $y,
                    $m['start'],
                    1,
                    $m['end'] - $m['start'],
                    $fontfile_height
                );

                $x += $m['end'] - $m['start'] - $shift;
            }
            if($x < $width - 10) break; // fit in canvas

        }
        $center = $x / 2;

        // credits. To remove, see configuration file
        $img2        = imagecreatetruecolor($width, $height + ($show_credits ? 12 : 0));
        $background  = imagecolorallocate($img2, $background_color[0], $background_color[1], $background_color[2]);
        $ramka_color = imagecolorallocate($img2, mt_rand(100, 150), mt_rand(100, 150), mt_rand(100, 150));

        imagefilledrectangle($img2, 0, 0,       $width - 1, $height - 1,    $background);
        imagefilledrectangle($img2, 0, $height, $width-1,   $height + 12,   $ramka_color);

        if($show_credits == true){
            imageline($img2, 0,         0,          $width,     0,          $ramka_color);
            imageline($img2, 0,         0,          0,          $height,    $ramka_color);
            imageline($img2, $width-1,  0,          $width-1,   $height,    $ramka_color);
            imageline($img2, 0,         $height-1,  $width,     $height-1,  $ramka_color);
        }

        imagestring(
            $img2,
            2,
            $width / 2 - ImageFontWidth(2) * strlen($credits) / 2,
            $height - 2,
            $credits,
            $background
        );

        // periods
        //$rand1 = mt_rand(750, 1200) / 10000;
        //$rand2 = mt_rand(750, 1200) / 10000;
        //$rand3 = mt_rand(750, 1200) / 10000;
        //$rand4 = mt_rand(750, 1200) / 10000;

        $rand1 = rand(50, 100) / 1000;
        $rand2 = rand(50, 100) / 1000;
        $rand3 = rand(50, 100) / 1000;
        $rand4 = rand(50, 100) / 1000;


        // phases mt_rand(0,30000000)/10000000;
        $rand5 = mt_rand(15000, 31000) / 100000;
        $rand6 = mt_rand(15000, 31000) / 100000;
        $rand7 = mt_rand(15000, 31000) / 100000;
        $rand8 = mt_rand(15000, 31000) / 100000;


        // amplitudes
        $rand9  = rand(200, 400) / 100;
        $rand10 = rand(200, 300) / 100;


        //wave distortion
        for($x = 0; $x < $width; $x++){
            for($y = 0; $y < $height; $y++){

                $sx = $x + (sin($x * $rand1 + $rand5) + sin($y * $rand3 + $rand6)) * $rand9 - $width / 2 + $center + 1;
                $sy = $y + (sin($x * $rand2 + $rand7) + sin($y * $rand4 + $rand8)) * $rand10;

                if($sx < 0 || $sy < 0 || $sx >= $width - 1 || $sy >= $height - 1){
                    continue;
                }
                else{
                    $color      = imagecolorat($img, $sx,       $sy)        & 0xFF;
                    $color_x    = imagecolorat($img, $sx + 1,   $sy)        & 0xFF;
                    $color_y    = imagecolorat($img, $sx,       $sy + 1)    & 0xFF;
                    $color_xy   = imagecolorat($img, $sx + 1,   $sy + 1)    & 0xFF;
                }

                if($color == 255 && $color_x == 255 && $color_y == 255 && $color_xy == 255){
                    continue;
                }
                else if($color == 0 && $color_x == 0 && $color_y == 0 && $color_xy == 0){
                    $newred     =   $foreground_color[0];
                    $newgreen   =   $foreground_color[1];
                    $newblue    =   $foreground_color[2];
                }
                else {
                    $frsx   = $sx - floor($sx);
                    $frsy   = $sy - floor($sy);
                    $frsx1  = 1 - $frsx;
                    $frsy1  = 1 - $frsy;

                    $newcolor = min(1, (
                            $color      * $frsx1    * $frsy1    +
                            $color_x    * $frsx     * $frsy1    +
                            $color_y    * $frsx1    * $frsy     +
                            $color_xy   * $frsx     * $frsy
                        ) / 255);

                    $newcolor0 = 1 - $newcolor;

                    $newred   = $newcolor0 * $foreground_color[0] + $newcolor * $background_color[0];
                    $newgreen = $newcolor0 * $foreground_color[1] + $newcolor * $background_color[1];
                    $newblue  = $newcolor0 * $foreground_color[2] + $newcolor * $background_color[2];
                }

                imagesetpixel($img2, $x, $y, imagecolorallocate($img2, $newred, $newgreen, $newblue));
            }
        }

        header("Content-Type: image/jpeg");

        header("Expires: Mon, 20 Jul 2000 20:00:00 GMT");
        header("Cache-Control: no-cache, must-revalidate, no-store");
        header("Pragma: no-cache");

        imagejpeg($img2, null, $jpeg_quality);
    }

    static public function clearOneCheckSession(){
        Db::site()->delete(
            "captcha_one_check_session",
            "life<? limit 1000",
            date('Y-m-d H:i:s')
        );
    }
}