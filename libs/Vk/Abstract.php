<?php

abstract class Vk_Abstract {
    public $app_name           = 'abstract';

    public $app_id;
    public $app_key;
    public $app_login_scope    = '';
    public $app_login_version  = '5.34';
    public $app_lang           = 'ru';
    public $app_https          = '0';

    ///////////////////////////////////////////////////////////

    public function getToken(){
        return Value::getRegister('vk_login_token_' . $this->app_name);
    }

    public function getUserID(){
        return Value::getRegister('vk_login_user_id_' . $this->app_name);
    }

    public function getProfile(){
        $temp = json_decode(Value::getRegister('vk_login_profile_' . $this->app_name), 1);
        return is_array($temp) ? $temp : [];
    }

    protected function setToken($token){
        Value::setRegister('vk_login_token_' . $this->app_name, $token);
    }

    protected function setUserID($userID){
        Value::setRegister('vk_login_user_id_' . $this->app_name, $userID);
    }

    protected function setProfileInfo($info){
        Value::setRegister('vk_login_profile_' . $this->app_name, $info);
    }

    public function logout(){
        Value::setRegister('vk_login_token_' . $this->app_name, null);
        Value::setRegister('vk_login_user_id_' . $this->app_name, null);
        Value::setRegister('vk_login_profile_' . $this->app_name, null);
    }

    ///////////////////////////////////////////////////////////

    public function loginStandalone(){
        // ssh -D localhost:1080 root@46.36.222.73

        $token_key   = 'vk_login_token_'   . $this->app_name;

        $token   = Value::getRegister($token_key);

        if(strlen($token)){
            // todo проверка что с ним пускает, если не пускает повтор авторизации
            return true;
        }

        if(!strlen($token)) {
            $url = "https://oauth.vk.com/authorize?" . http_build_query([
                    'client_id'        => $this->app_id,
                    'scope'            => $this->app_login_scope,
                    'redirect_uri'     => "https://oauth.vk.com/blank.html",
                    'response_type'    => 'token',
                    'display'          => 'page',
                    'v'                => $this->app_login_version,
                ]);

            header("location: " . $url);
            die;
        }
    }



    public function login(){

        $user_id = $this->getUserID();
        $token   = $this->getToken();


        if(strlen($token) && strlen($user_id)){
            Value::export($this->api("account.getAppPermissions", [
                "12077147" => $user_id,
            ]));

            return true;
        }

        if(!strlen($token)) {
            $redirect = Http_Url::convert(['vkOAuth' => 'yes'], Http_Url::getCurrentURL(), false);

            if(
                isset($_GET['vkOAuth'], $_GET['code']) &&
                $_GET['vkOAuth'] == 'yes' &&
                strlen($_GET['code'])
            ){
                $url = "https://oauth.vk.com/access_token?" . http_build_query([
                    'client_id'     => $this->app_id,
                    'client_secret' => $this->app_key,
                    'code'          => (string)$_GET['code'],
                    'redirect_uri'  => $redirect,
                ]);

                $result = Json::decode(
                    file_get_contents(
                        $url,
                        false,
                        stream_context_create([
                            'http' => [
                                'ignore_errors' => true
                            ]
                        ])
                    )
                );

                if(isset($result['access_token'], $result['user_id'])){
                    $this->setUserID($result['user_id']);
                    $this->setToken($result['access_token']);

                    // сохранить информация о профиле
                    $profile = $this->api(
                        "users.get",
                        [
                            "user_ids" => $this->getUserID(),
                            "fields" => "photo_50,photo_100,photo_200",
                        ]
                    );

                    $this->setProfileInfo(isset($profile['response']['0']) ? $profile['response']['0'] : []);

                    // перенаправить
                    header("location: " . explode("?", Http_Url::getCurrentURL())[0]);

                    return true;
                }
                else {
                    throw new Exception("Invalid VK Login");
                }


            }

            $url = "https://oauth.vk.com/authorize?" . http_build_query([
                'client_id'        => $this->app_id,
                'scope'            => $this->app_login_scope,
                'redirect_uri'     => $redirect,
                'display'          => 'page',
                'response_type'    => 'code',
                'v'                => $this->app_login_version,
                'state'            => "",
            ]);

            header("location: " . $url);
            die;
        }
    }

    public function api($method, $params = []){
        if(!is_array($params)) $params = [];

        $params['lang']         = $this->app_lang;
        $params['v']            = $this->app_login_version;
        $params['https']        = $this->app_https;
        $params['access_token'] = $this->getToken();

        $head = [
            'Expect: ',
        ];

        // настрйока отправки
        $ch = curl_init();

        $url = "https://api.vk.com/method/{$method}";

        curl_setopt($ch, CURLOPT_URL,               $url);
        curl_setopt($ch, CURLOPT_POST,              0);
        curl_setopt($ch, CURLOPT_POSTFIELDS,        $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER,        $head);
        curl_setopt($ch, CURLOPT_FAILONERROR,       0);
        curl_setopt($ch, CURLOPT_HEADER,            0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,    1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,    0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,    0);
        curl_setopt($ch, CURLOPT_TIMEOUT,           10);
        curl_setopt($ch, CURLINFO_HEADER_OUT,       true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION,      CURL_HTTP_VERSION_1_1);

        // отправка данных, получение ответа, отделение header от body($return)
        $start = microtime(1);
        $Response   = curl_exec($ch);

        Db::site()->insert("vk_api_logs", [
            "url"       => $url,
            "request"   => Json::encode($params),
            "response"  => $Response,
            "runtime"   => microtime(1) - $start,
        ]);

        return Json::decode($Response);
    }

    public function uploadFile($url, $filename){
        // настрйока отправки
        $ch = curl_init();

        $head = [
            'Expect: ',
        ];

        $params = array(
            'file1' => new CURLFile($filename, mime_content_type($filename), 'img.jpg')
        );

        curl_setopt($ch, CURLOPT_URL,               $url);
        curl_setopt($ch, CURLOPT_POST,              1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,        $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER,        $head);
        curl_setopt($ch, CURLOPT_FAILONERROR,       0);
        curl_setopt($ch, CURLOPT_HEADER,            0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,    1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,    0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,    0);
        curl_setopt($ch, CURLOPT_TIMEOUT,           10);
        curl_setopt($ch, CURLINFO_HEADER_OUT,       true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION,      CURL_HTTP_VERSION_1_1);

        // отправка данных, получение ответа, отделение header от body($return)
        $Response   = curl_exec($ch);

        //Value::export(curl_getinfo($ch, CURLINFO_HEADER_OUT));

        return Json::decode($Response);
    }
}