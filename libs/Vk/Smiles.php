<?php


class Vk_Smiles {
    // http://myauto26.ru/vkontakte/smileys.php

    static protected $smiles = [
        'heat'      => '10084',
        'heatG'     => '128154',
        'heatB'     => '128153',
        'heatY'     => '128155',
        'smile'     => '9786',
        'truck'     => '128666',
        'girls'     => '128109',
        'boys'      => '128108',
        'balloon'   => '127880',
        'gift'      => '127873',
        'bow'       => '127872',
        'flower1'   => '127800',
        'flower2'   => '127801',
        'flower3'   => '127802',
        'flower4'   => '127799',
        'flower5'   => '127803',
        'flower6'   => '127804',
        'clock'     => '9200',
        'run'       => '127939',
    ];

    static public function getSmiles(){
        return self::$smiles;
    }

    /**
     * Привести условные имена к кода контакта
     *
     * @param $string
     *
     * @return mixed
     */
    static public function filter($string){
        $search = [];
        $replace = [];

        foreach(self::getSmiles() as $k => $v){
            $search[] = "({$k})";
            $replace[] = "&#{$v};";

        }

        return str_replace($search, $replace, $string);
    }

    /**
     * @param $string
     *
     * @return mixed
     */
    static public function render($string){
        return nl2br(
            preg_replace("/\\&\\#([0-9]{4,6})\\;/i", "<img src='/static/img/vk/smiles/$1.png' />", self::filter($string))
        );
    }
}