<?php

class Base_PhpError {
    static public function getErrorLabel($errno){
        switch ($errno) {
            case E_ERROR:               $type = "ERROR";              break;
            case E_WARNING:             $type = "WARNING";            break;
            case E_PARSE:               $type = "PARSE";              break;
            case E_NOTICE:              $type = "NOTICE";             break;
            case E_CORE_ERROR:          $type = "CORE_ERROR";         break;
            case E_CORE_WARNING:        $type = "CORE_WARNING";       break;
            case E_COMPILE_ERROR:       $type = "COMPILE_ERROR";      break;
            case E_COMPILE_WARNING:     $type = "COMPILE_WARNING";    break;
            case E_STRICT:              $type = "STRICT";             break;
            case E_RECOVERABLE_ERROR:   $type = "RECOVERABLE_ERROR";  break;
            case E_DEPRECATED:          $type = "DEPRECATED";         break;
            case E_USER_ERROR:          $type = "USER_ERROR";         break;
            case E_USER_WARNING:        $type = "USER_WARNING";       break;
            case E_USER_NOTICE:         $type = "USER_NOTICE";        break;
            case E_USER_DEPRECATED:     $type = "USER_DEPRECATED";    break;
            case E_ALL:                 $type = "ALL";                break;
            default:                    $type = "UNKNOWN_ERROR";      break;
        }

        return $type;
    }
}