<?php

class Decorator_PhoneColored extends Decorator_Abstract {
    /**
     * @param null|string $value
     * @return string
     */
    public function render($value = null){
        $p = str_split($value);

        $r = "";
        foreach($p as $el){
            if(preg_match("/[a-z]/i", $el)){
                $r.= "<small style='color: #999'>{$el}</small>";
            }
            else if(preg_match("/[\\.\\-\\+\\(\\)]/i", $el)){
                $r.= "<span style='color: #BBB'>{$el}</span>";
            }
            else {
                $r.= $el;
            }
        }

        return $r;
    }
}