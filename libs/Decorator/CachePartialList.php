<?php

class Decorator_CachePartialList extends Decorator_Abstract {
    /**
     * @var Cache_PartialList_Abstract
     */
    protected $cacheClass;

    public function __construct(Cache_PartialList_Abstract $cacheClass){
        $this->setCacheClass($cacheClass);
    }

    /**************************************************************************/

    /**
     * Установить ссылку на кешевый класс, который будет отвечать за загрузку данных
     *
     * @param Cache_PartialList_Abstract $cacheClass
     * @return Decorator_CachePartialList
     */
    public function setCacheClass(Cache_PartialList_Abstract $cacheClass){
        $this->cacheClass = $cacheClass;
        return $this;
    }

    public function getCacheClass(){
        return $this->cacheClass;
    }

    public function load($data){
        $this->cacheClass->load($data);
    }

    public function render($value = null){
        return $this->cacheClass->get($value);
    }
}