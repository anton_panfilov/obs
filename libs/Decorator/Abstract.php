<?php

abstract class Decorator_Abstract {
    abstract public function render($value = null);
    
    public function __toString(){
        return (string)$this->render();
    }
}