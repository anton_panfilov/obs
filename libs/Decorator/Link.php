<?php

class Decorator_Link extends Decorator_Abstract {
    /**
     * @var Html_Attribs
     */
    protected $attr;

    public function __construct($url){
        $this->attr = new Html_Attribs();
        $this->setURL($url);
    }

    /**
    * Установить ссылку
    *
    * @param mixed $url
    * @return Decorator_Link
    */
    public function setURL($url){
        $this->attr->setAttrib("href", Context::pattern($url));
        return $this;
    }

    /**
     * Задать значение атрибута
     * Или удалить его, если занчение = NULL
     *
     * @return Decorator_Link
     */
    public function setAttrib($name, $value = null){
        $this->attr->setAttrib($name, $value);
        return $this;
    }

    /**
     * Добавить к значению ссылку
     *
     * @param string $value
     * @return string
     */
    public function render($value = null){
        return "<a{$this->attr}>{$value}</a>";
    }
}