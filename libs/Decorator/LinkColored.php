<?php

class Decorator_LinkColored extends Decorator_Abstract {
    protected $show_query = false;
    protected $show_http_protocol = false;
    protected $target = null;

    /**
     * @param $target
     * @return $this
     */
    public function setTarget($target){
        $this->target = $target;
        return $this;
    }

    /**
     * @param $flag
     * @return $this
     */
    public function showQuery($flag = true){
        $this->show_query = (bool)$flag;
        return $this;
    }

    /**
     * @param $flag
     * @return $this
     */
    public function showHttpProtocol($flag = true){
        $this->show_http_protocol = (bool)$flag;
        return $this;
    }

    /**
     * Раскрасить линку
     *
     * @param string $value
     * @return string
     */
    public function render($value = null){
        $href = $value;
        $url = parse_url($value);

        if(isset($url['scheme'], $url['host'])){
            $scheme = '';
            if($url['scheme'] == 'https'){
                $scheme = "<span style='color:#006621'>https</span>://";
            }
            else if($url['scheme'] == 'http' && $this->show_http_protocol){
                $scheme = "<span style='color:#999'>http</span>://";
            }
            else if($url['scheme'] != 'http'){
                $scheme = "<span style='color:#e51d18'>{$url['scheme']}</span>://";
            }

            $value = $scheme . "<span style='color:#000'>" . $url['host'] . "</span>";

            if(isset($url['path'])){
                $value.= $url['path'];
            }

            if($this->show_query && isset($url['query'])){
                $value.= "?" . $url['query'];
            }
        }

        $attr = new Html_Attribs();
        $attr->setAttrib("style", "color:#888");
        $attr->setAttrib("href", $href);

        if($this->target){
            $attr->setAttrib("target", $this->target);
        }

        return "<a " . $attr->render() . ">" . $value . "</a>";
    }
}