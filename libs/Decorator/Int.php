<?php

class Decorator_Int extends Decorator_Abstract {
    /**
     * Раскрасить линку
     *
     * @param string $value
     * @return string
     */
    public function render($value = null){
        return (int)$value;
    }
}