<?php

class Decorator_Pattern extends Decorator_Abstract {
    protected $pattern;

    public function __construct($pattern){
        $this->setPattern($pattern);
    }

    /**************************************************************************/

    /**
     * Установить шаблон, который при отрисовке ( Obj->render() )
     * будет использовать окружающий его контекст ( \AP\Context )
     *
     * @param string $pattern
     */
    public function setPattern($pattern){
        $this->pattern = $pattern;
    }

    public function render($value = null){
        return Context::pattern($this->pattern);
    }
}