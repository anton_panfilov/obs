<?php

class Decorator_Date extends Decorator_Abstract {
    protected $showDate         = true;
    protected $showTime         = true;
    protected $showSeconds      = false;
    protected $showSecondsToday = false;
    protected $todayOnlyTime    = true;
    protected $showCurrentYear  = false;
    protected $lastYearNonTime  = true;
    protected $showDetails      = true;
    protected $markWeekends     = false;

    /**
     * Формат даты (как для php функции date()), который перебивает все настройки и выводит даты в этом формате
     *
     * @var null|string
     */
    protected $dateFormatFixed  = null;

    /****************************************************************************/

    /**
     * Задать фиксированный формат даты (формат задается как для php функции date())
     * Если оставить формат пустым, то он будет настраиваться в зависимости от многих параметров
     *
     * @param string|null $format
     * @return Decorator_Date
     */
    public function setDateFormatFixed($format = null){
        $this->dateFormatFixed = $format;
        return $this;
    }

    /**
    * Показывать дату
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function showDate($flag = true){
        $this->showDate = (bool)$flag;
        return $this;
    }

    /**
    * Показывать время
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function showTime($flag = true){
        $this->showTime = (bool)$flag;
        return $this;
    }

    /**
    * Показывать секунды
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function showSeconds($flag = true){
        $this->showSeconds = (bool)$flag;
        return $this;
    }

    /**
    * Показывать секунды сегодня, даже если вобщем они отключены
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function showSecondsToday($flag = true){
        $this->showSecondsToday = (bool)$flag;
        return $this;
    }

    /**
    * Если день сегодняшний, то не показывать дату
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function todayOnlyTime($flag = true){
        $this->todayOnlyTime = (bool)$flag;
        return $this;
    }

    /**
    * Показывать в дате текущий год
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function showCurrentYear($flag = true){
        $this->showCurrentYear = (bool)$flag;
        return $this;
    }

    /**
    * Не показывать время за предыдущий год
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function lastYearNonTime($flag = true){
        $this->lastYearNonTime = (bool)$flag;
        return $this;
    }

    /**
    * Показать детали, если что то может быть скрыто
    *
    * @param bool $flag
    * @return Decorator_Date
    */
    public function showDetails($flag = true){
        $this->showDetails = (bool)$flag;
        return $this;
    }

    /**
     * Выделать выходные
     *
     * @param bool $flag
     * @return $this
     */
    public function markWeekends($flag = true){
        $this->markWeekends = (bool)$flag;
        return $this;
    }

    /****************************************************************************/

    protected function format(){
        return [
            'dt'  => 'date, time',

            'dm'  => Translate::t('M d', 'date/format'),
            'dmY' => Translate::t('M d, Y', 'date/format'),
            'Hi'  => Translate::t('H:i', 'date/format'),
            'His' => Translate::t('H:i:s', 'date/format'),
            'full' => Translate::t('M d, Y, H:i:s', 'date/format'),
        ];
    }

    protected function getDateFormat($cur){
        if($this->showCurrentYear){
            return $this->format()['dmY'];
        }
        else {
            return date("Y") == date("Y", $cur) ? $this->format()['dm'] : $this->format()['dmY'];
        }
    }

    protected function getTimeFormat($cur){
        return ($this->showSeconds || $this->showSecondsToday && date("Y-m-d") == date("Y-m-d", $cur)) ?
            $this->format()['His'] :
            $this->format()['Hi'];
    }

    public function render($value = null){
        $cur = strtotime($value);

        if($cur){
            $format = '';

            if(strlen($this->dateFormatFixed)){
                $format = $this->dateFormatFixed;
            }
            else {
                // Если фиксорованный формат не задан
                if($this->showDate && ($this->showTime && strlen($value) > 10)){
                    // показывается дата и время
                    if($this->todayOnlyTime && date("Y-m-d") == date("Y-m-d", $cur)){
                        $format = $this->getTimeFormat($cur);
                    }
                    else if($this->lastYearNonTime && date("Y") != date("Y", $cur)){
                        $format = $this->getDateFormat($cur);
                    }
                    else {
                        $format = str_replace(
                            ['date', 'time'],
                            [$this->getDateFormat($cur), $this->getTimeFormat($cur)],
                            $this->format()['dt']
                        );
                    }
                }
                else if($this->showDate){
                    // показывать только дату
                    $format = $this->getDateFormat($cur);
                }
                else if($this->showTime && strlen($value) > 10){
                    // показывать только время
                    $format = $this->getTimeFormat($cur);
                }

                // показазать детально при наведении
                if($this->showDetails && strlen($value) > 10){
                    return "<span title='" . date($this->format()['full'], $cur) . "'>" . date($format, $cur) . "</span>";
                }
            }

            $markWeekend = false;
            if($this->markWeekends){
                if(in_array(date('w', $cur), [0, 6])){
                    $markWeekend = true;
                }
            }

            if($markWeekend){
                return "<span title='Weekend' style='color:#999'> " . date($format, $cur) . " <span style='color:#e25342'>*</span></span>";
            }

            return date($format, $cur);
        }

        return $value;
    }
}