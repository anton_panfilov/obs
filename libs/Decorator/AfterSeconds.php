<?php

class Decorator_AfterSeconds extends Decorator_Abstract {

    /**
     * Раскрасить линку
     *
     * @param string $value
     * @return string
     */
    public function render($value = null){
        if(!is_numeric($value)){
            return $this->renderText("-");
        }

        $value = (int)$value;

        $minus = false;
        if($value < 0){
            $value = abs($value);
            $minus = true;
        }

        if($value < 60){
            $return = $value . " " . $this->renderText('sec');
        }
        else if($value < 3600){
            $return = floor($value/60) . " " . $this->renderText('min');

            $temp = ($value%60);
            if($temp > 0){
                $return.= " " . $temp . " " . $this->renderText('sec');
            }
        }
        else if($value < 86400){
            $temp = floor($value/3600);
            $return = $temp . " " . $this->renderText($temp == 1 ? 'hour' : 'hours');

            $temp = (floor($value/60)%60);
            if($temp > 0){
                $return.= " " . $temp . " " . $this->renderText('min');
            }
        }
        else if($value < 2592000){
            $temp = floor($value/86400);
            $return = $temp . " " . $this->renderText($temp == 1 ? 'day' : 'days');
        }
        else if($value < 31536000){
            $temp = floor($value/2592000);
            $return = $temp . " " . $this->renderText($temp == 1 ? 'month' : 'months');
        }
        else {
            $temp = floor($value/31536000);
            $return = $temp . " " . $this->renderText($temp == 1 ? 'year' : 'years');
        }


        return ($minus ? "- " : "") . $return;
    }

    protected function renderText($text){
        return "<small style='color:#AAA'>{$text}</small>";
    }
}