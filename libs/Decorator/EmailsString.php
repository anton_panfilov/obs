<?php

class Decorator_EmailsString extends Decorator_Abstract {
    /**
     * Добавить к значению ссылку
     *
     * @param string $value
     * @return string
     */
    public function render($value = null){
        $data = Email_Functions::parseEmailString($value);

        if(count($data)){
            $result = [];

            $attr = new Html_Attribs();
            $attr->setAttrib('class', 'label label-default tooltips');
            $attr->setAttrib('style', '');
            $attr->setAttrib('rel', 'tooltip');
            $attr->setAttrib('data-placement', 'top');

            foreach($data as $el){
                if(isset($el['name']) && strlen($el['name'])){
                    $attr->setAttrib('data-original-title', "{$el['name']} <{$el['email']}>");
                }
                else {
                    $attr->setAttrib('data-original-title', $el['email']);
                }

                $result[] = "<span " . $attr->render() . ">{$el['email']}</span>";
            }

            return implode(" ", $result);
        }

        return "-";
    }
}