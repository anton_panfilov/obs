<?php

class Arrays {
    /**
     * Рекурсивно выполнить все Closure переменные в массиве
     *
     * @param $array
     * @return array
     */
    static public function executeClosure($array){
        if(is_array($array) && count($array)){
            $new = [];

            foreach($array as $k => $v){
                if(is_array($v)){
                    $new[$k] = self::executeClosure($v);
                }
                else if($v instanceof \Closure){
                    $new[$k] = $v();
                }
                else {
                    $new[$k] = $v;
                }
            }

            return $new;
        }
        return $array;
    }
}