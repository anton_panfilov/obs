<?php

class Translate {
    /**
     * Название обсучаемого словаря, который подключаеться и попалняеться если включен обсучаемый режим Conf::main()->localeLearningMode
     */
    const LEARNING_DICTIONARY = '_learning';
    const JOIN_SEPARATOR = "|+|";

    static protected $useCookie = true;

    /**************************************************************************/

    /**
     * null - последний раз словарь не подключался
     * 1    - словарь из списка
     * 2    - определенный словарь
     *
     * @var null|int
     */
    static protected $lastDictionaryType;

    /**
     * Ключ последнего словаря в кеше объединенных словарей
     *
     * @var null|string
     */
    static protected $lastDictionaryKey;


    /**
     * Кеш объединенных словарей
     *
     * @var array
     */
    static protected $cacheJoinedDictionaries = [];

    /**************************************************************************/

    /**
     * @var array Массив подключенных словарей key - название словаря. value - его приоритет. Чем больше это число, тем выше приоритет
     */
    static protected $dictionaries = [
        'main' => 1
    ];

    /**
     * Кеш переводов по каждому словарю отдельно
     *
     * @var array
     */
    static protected $_localeArray;

    /**
     * Кеш текеущей локали
     *
     * @var string ru, us ...
     */
    static protected $_currentLocale = null;

    /**
     * Кеш локальных множественных настроек
     *
     * @var array
     */
    static protected $_localeSettings = null;


    /************************************************************************/

    /**
     * Включение - отключение использования куков.
     * Используеться для апи и подобного
     *
     * @param $flag
     */
    static public function setUserCookie($flag){
        self::$useCookie = (bool)$flag;
    }

    /***********************************************************************/

    /**
     * Добавление словаря
     * Словари с большим приоритетом перебивают словри с меньшим
     *
     * @param $dictionary
     * @param int $priority
     */
    static public function addDictionary($dictionary, $priority = 10){
        self::$dictionaries[$dictionary] = (int)$priority;
    }

    /**
     * Returns current locale from session or config
     *
     * @return mixed
     */
    static public function getCurrentLocale() {
        /**
         * Приоритет получения:
         * 1. GET переменная lang
         * 2. lang у пользователя, если он загружен
         * 3. cookie lang
         *
         * При любом изменении локация сохраняеться в:
         * 1. юзера (если он загружен и у нео выбранна дугая локация)
         * 2. куку, если там что то другое
         */

        if(!self::$_currentLocale) {
            self::$_currentLocale = false;

            //////////////////////////////////////
            // Получение локали из: Get -> User -> Cookie
            if (isset($_GET['lang']) && strlen($_GET['lang']) == 2) {
                self::$_currentLocale = $_GET['lang'];
            }
            else if(Users::getCurrent()->id) {
                self::$_currentLocale = Users::getCurrent()->lang;
            }
            else if(self::$useCookie && isset($_COOKIE['lang']) && $_COOKIE['lang']) {
                self::$_currentLocale = $_COOKIE['lang'];
            }

            //////////////////////////////////////
            // Валидация локали и выставление по умолчанию если что то пошло не так
            if (!self::$_currentLocale || !isset(Conf::main()->locales[self::$_currentLocale])){
                self::$_currentLocale = Conf::main()->locale_default;
            }

            //////////////////////////////////////
            // сохранение локали в User
            if(Users::getCurrent()->id && Users::getCurrent()->lang != self::$_currentLocale){
                Users::getCurrent()->lang = self::$_currentLocale;
                Users::getCurrent()->update(['lang']);
            }

            //////////////////////////////////////
            // сохранение локали в Cookie
            if(self::$useCookie){
                if(!isset($_COOKIE['lang']) || $_COOKIE['lang'] != self::$_currentLocale){
                    setcookie("lang", self::$_currentLocale, time()+99999999, "/");
                }
            }
        }

        return self::$_currentLocale;
    }

    /**
     * @param null $dictionary
     * @return string
     */
    static protected function getLocaleFileName($dictionary = null, $locale = null){
        if(!isset(Conf::main()->locales[$locale])){
            $locale = self::getCurrentLocale();
        }

        return Boot::$dir_root . "/langs" . "/" . $dictionary . "/" . $locale . ".csv";
    }

    /************************************************************************/

    /**
     * Получение соединенного массива переводов:
     * а) если не передан определенный словарь: по всем подключенным словарям в зависимости от приоритетов
     * б) если передан словарь, то по определенному словарю
     *
     * @param string|null $dictionary
     * @return array
     */
    static protected function _getLocaleArray($dictionary = null) {
        if(count(self::$dictionaries) || !is_null($dictionary)){
            if(!strlen($dictionary)){
                self::$lastDictionaryType = 1; // словарь из всех подключенных

                // выстаивание по приоритетам
                asort(self::$dictionaries, SORT_NUMERIC);

                // формирование последовательности словарей
                $all = array_keys(self::$dictionaries);
            }
            else {
                self::$lastDictionaryType = 2; // выбран определенный словарь
                $all = [$dictionary];
            }

            if(Conf::main()->localeLearningMode){
                // добавление обучаемого словаря на самый низкий приоритет
                $all = array_merge([self::LEARNING_DICTIONARY], $all);
            }

            // ключ в кеше соединенных словарей
            self::$lastDictionaryKey = implode("|+|", $all);

            $locale = self::getCurrentLocale();
            $dictionaries = self::getJoinedDictionary();

            if(!$dictionaries || !isset($dictionaries[$locale][self::$lastDictionaryKey])){
                $dictionaries[$locale][self::$lastDictionaryKey] = [];
                // составление соединенного словаря
                foreach($all as $dictionary){
                    $array = self::_csvToArray($dictionary);
                    if(is_array($array) && count($array)){
                        $dictionaries[$locale][self::$lastDictionaryKey] =
                            $array + $dictionaries[$locale][self::$lastDictionaryKey];
                    }
                }

                // добивание значениями из американского словаря
                if($locale != 'us'){
                    $dictionaries['us'][self::$lastDictionaryKey] = [];
                    // составление соединенного словаря
                    foreach($all as $dictionary){
                        $array = self::_csvToArray($dictionary, 'us');
                        if(is_array($array) && count($array)){
                            $dictionaries['us'][self::$lastDictionaryKey] =
                                $array + $dictionaries['us'][self::$lastDictionaryKey];
                        }
                    }

                    if(count($dictionaries['us'][self::$lastDictionaryKey])){
                        foreach($dictionaries['us'][self::$lastDictionaryKey] as $k => $v){
                            if(!isset($dictionaries[$locale][self::$lastDictionaryKey][$k])){
                                $dictionaries[$locale][self::$lastDictionaryKey][$k] = $v;
                            }
                        };
                    }
                }

                self::setJoinedDictionary($dictionaries);
            }

            return $dictionaries[$locale][self::$lastDictionaryKey];
        }

        return [];
    }

    public static function getJoinedDictionary() {
        if (Conf::main()->localeLearningMode) {
            return self::$cacheJoinedDictionaries;
        }

        $dictionary = \Cacher::get('dictionaries');
        if ($dictionary) {
            self::$cacheJoinedDictionaries = $dictionary;
        }

        return self::$cacheJoinedDictionaries;
    }

    public static function setJoinedDictionary($dictionaries) {
        if (!Conf::main()->localeLearningMode) {
            \Cacher::set('dictionaries', $dictionaries);
        }

        self::$cacheJoinedDictionaries = $dictionaries;
    }

    /**
     * @param $dictionary
     * @param $locale
     * @return array
     */
    private static function _csvToArray($dictionary, $locale = null) {
        if(!isset(self::$_localeArray[$locale][$dictionary])){
            if(!isset(self::$_localeArray[$locale])){
                self::$_localeArray[$locale] = [];
            }

            self::$_localeArray[$locale][$dictionary] = [];

            $file = self::getLocaleFileName($dictionary, $locale);
            if(is_file($file)){
                $handle = @fopen($file, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 4096)) !== false) {
                        self::$_localeArray[$locale][$dictionary][$row[0]] = isset($row[1]) ? $row[1] : "-";
                    }
                    fclose($handle);
                }
                else {
                    System_Warnings::add("Invalid handle: " . $file);
                }
            }
        }

        return self::$_localeArray[$locale][$dictionary];
    }

    static protected function learning($string) {
        if(Conf::main()->localeLearningMode){
            // добавление в кеш словарей
            $locale = self::getCurrentLocale();
            self::$_localeArray[self::LEARNING_DICTIONARY][$string] = "???";            // пословарный кеш
            self::$cacheJoinedDictionaries[$locale][self::$lastDictionaryKey][$string] = "???";  // кеш склеянных словарей

            $localeFile = self::getLocaleFileName(self::LEARNING_DICTIONARY);
            $is_file = is_file($localeFile);

            $handle = fopen($localeFile, "a");
            if ($handle) {
                if(!$is_file){
                    chmod($localeFile, 0666);
                }
                fputcsv($handle, [
                    $string,
                    "???",
                    substr(
                        self::$lastDictionaryKey,
                        strlen(self::LEARNING_DICTIONARY . self::JOIN_SEPARATOR)
                    )
                ]);
                fclose($handle);
            }
            else {
                System_Warnings::add("File: " . $localeFile . ". Permissions deny");
            }

        }
    }

    /******************************************************************************************/

    /**
     * @param $text
     * @param null $mixedParam
     * @param array $params
     * @return string
     */
    public static function t($text, $mixedParam = null, $params = array()) {
        // правила не примерения переводов
        if($text instanceof Translate_Expr){
            return $text;
        }
        else if(trim($text) == ''){
            return '';
        }

        // обработка входящих переменных
        $dictionary = null;
        if (is_array($mixedParam)) {
            $params = $mixedParam;
        }
        else if (is_numeric($mixedParam)) {
            $n = $mixedParam;
            $params['n'] = $n;
        }
        else if (is_string($mixedParam)) {
            $dictionary = $mixedParam;
        }

        if (is_numeric($params)) {
            $n = $params;
            $params = array(
                'n' => $n
            );
        }

        // поиск перевода
        $localeArray = self::_getLocaleArray($dictionary);
        $localizedText = preg_replace('/[\s]+/mu', " ", $text);

        if (isset($localeArray[$localizedText])) {
            if($localeArray[$localizedText] != "???"){
                $localizedText = $localeArray[$localizedText];
            }
        }
        else {
            self::learning($localizedText);
        }

        $result = self::_format(
            self::_pluralEvaluate(
                $localizedText,
                isset($params['n']) ? $params['n'] : null
            ),
            $params
        );

        // Что бы при несколих прохождениях переведа, обучающий режим не писал их все
        if(Conf::main()->localeLearningMode){
            return new Translate_Expr($result);
        }

        return $result;
    }

    /**
     * Replaces aliases {alias} in string
     *
     * @param $params
     * @param $text
     * @return string
     */
    static protected function _format($text, $params) {
        $aliases = array();
        preg_match_all('/{(.*)}/Um', $text, $aliases);
        if ($aliases[1]) {
            $aliases = array_unique($aliases[1]);
            foreach ($aliases as $alias) {
                if (isset($params[$alias])) {
                    $text = str_replace('{' . $alias . '}', $params[$alias], $text);
                }
            }
        }

        return $text;
    }

    /**
     * Check form and returns correct text form
     *
     * @param $text
     * @param $n
     * @return bool
     */
    static protected function _pluralEvaluate($text, $n) {
        $textForms = explode('|', $text);
        if (count($textForms) == 1 || !isset($n)) {
            return $text;
        }

        // try to load plural forms
        $localeSettings = self::_getLocaleSettings();
        $pluralRules = $localeSettings['pluralRules'];

        foreach($pluralRules as $i => $rule) {
            $rule = str_replace('n','$n', $rule);
            if (eval("return $rule;") && isset($textForms[$i])) {
                return $textForms[$i];
            }
        }

        return $text;
    }

    static protected function _getLocaleSettings() {
        if (self::$_localeSettings) {
            return self::$_localeSettings;
        }

        $locale = self::getCurrentLocale();
        $localSettingsFile = realpath(Boot::$dir_root . "/libs/i18n/data/" . $locale . ".php");
        if ($localSettingsFile) {
            self::$_localeSettings = require($localSettingsFile);

            return self::$_localeSettings;
        }

        return null;
    }

    /***************************************************************************/

    /**
     * @param string $divClass
     * @return string
     */
    static public function renderLanguageSelectBtn($divClass = ''){
        ob_start();

        $locales = Conf::main()->locales;

        $currentLocale = self::getCurrentLocale();
        $currentLocaleLabel = isset($locales[$currentLocale]) ? $locales[$currentLocale] : $currentLocale;

        unset($locales[$currentLocale]);

        if(count($locales)){
            ?>
                <div class="btn-group <?=$divClass?>" style="text-align: left">
                    <a class="btn" data-toggle="dropdown" style="width: auto; text-align: left">
                        <i class="flag flag-<?=$currentLocale?>" style="margin-right: 5px"></i> <?=$currentLocaleLabel?>
                    </a>
                    <ul class="dropdown-menu pull-right" style="min-width: inherit">
                        <? foreach($locales as $k => $v){ ?>
                            <li>
                                <a href="<?=Http_Url::convert(["lang" => $k])?>">
                                    <i class="flag flag-<?=$k?>" style="margin-right: 5px"></i> <?=$v?>
                                </a>
                            </li>
                        <?}?>
                    </ul>
                </div>
            <?
        }
        else {
            ?><i class="flag flag-<?=$currentLocale?>" style="margin-right: 5px"></i> <?=$currentLocaleLabel?><?
        }

        return ob_get_clean();
    }
} 