<?php

class Currency_List extends Ui_Set {
    const RUR    = 'RUR';
    const USD    = 'USD';

    public function __construct(){
        parent::__construct('currency/list');

        $this->addValue_White(self::RUR, 'currency_list_RUR');
        $this->addValue_White(self::USD, 'currency_list_USD');
    }
}