<?php

class IdEncryption {
    /**
     * Функция кодирует число в число, длинна конечного числа превышает длинну исходного от (2 + $hashLen) до (1 + $hashLen) цифор
     *
     * CODE = XY____ABC
     *
     * X - на сколько делиться исходне число (2, 9)
     * Y - остаток от деления (0, 9)
     * ___ - частоное от деления
     * ABC - CRC32 последние $hashLen цифры от исходного числа (опционально)
     *
     * @param int $int
     * @param int $hashLen
     * @return string int
     */
    static public function encodeToInt($int, $hashLen = 3){
        $d = rand(1, 9);

        return $d . ($int % $d) . (floor($int / $d)) .
            ($hashLen > 0 ? sprintf("%0{$hashLen}d", substr(crc32($int), -$hashLen, $hashLen)) : "");
    }

    /**
     * Декодировать число, закодированное функцией self::encodeToInt()
     *
     * self::decodeForInt(self::encodeToInt($int)) всегда равно $int
     *
     * @param int|string $code
     * @param int $hashLen
     * @return int
     */
    static public function decodeForInt($code, $hashLen = 3){
        $int = (int)substr($code, 2, strlen($code) - (2 + $hashLen)) * (int)substr($code, 0, 1) + (int)substr($code, 1, 1);

        if(
            $hashLen > 0 &&
            sprintf("%0{$hashLen}d", substr(crc32($int), -$hashLen, $hashLen)) != substr($code, -$hashLen, $hashLen)
        ){
            return 0; // если хеш сумма не совпадает
        }

        return $int;
    }
}


