<?php

abstract class Context_Abstract {
    abstract public function get();

    public function __toString(){
        return (string)$this->get();
    } 
}