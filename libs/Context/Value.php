<?php

class Context_Value extends Context_Abstract {
    protected $name = null;
    protected $default = null;

    public function __construct($name, $default = null){
        $this->setName($name);
        $this->setDefault($default);
    }

    /**
    * Задать название переменной, которая будет получаться из контекста
    *
    * @param string $name
    * @return Context_Value
    */
    public function setName($name){
        $this->name = (string)$name;
        return $this;
    }

    /**
    * Задачть значсение по умолчагию, если при получении переменной в контексте такой не будет
    *
    * @param mixed $default
    * @return Context_Value
    */
    public function setDefault($default = null){
        $this->default = $default;
        return $this;
    }

    /**
    * Получить даннные (переменную) из контекста
    */
    public function get(){
        return Context::get($this->name, $this->default);
    }
}