<?php

class Context_Pattern extends Context_Abstract {
    protected $pattern;
    protected $value;

    public function __construct($pattern){
        $this->setPattern($pattern);
    }

    /**
    * Задать сторку патерна
    *
    * @param mixed $pattern
    * @return Context_Pattern
    */
    public function setPattern($pattern){
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * Задать имя текущей переменной и её значение по умолчанию (Optional)
     * Удобно когда надо примернь оин шаблон для разных элементов
     *
     * @return Context_Pattern
     */
    public function setValue($name, $default = null){
        $this->value = new Value($name, $default);
        return $this;
    }

    /**
     * Получить патерн преобразованный с учетом контекста
     * В паттерне ссылки на переменные контента ставяться в фигурных скобках. Например "{first_name} {last_name}"
     *
     * @return string
     */
    public function get(){
        if($this->pattern instanceof Closure){
            $func = $this->pattern;
            return $func(Context::getArray());
        }
        else {
            preg_match_all("/\{((?:[a-z0-9_])*)\}/i", $this->pattern, $matches);

            if(is_array($matches[1]) && count($matches[1])){
                $search  = array();
                $replace = array();

                foreach($matches[1] as $el){
                    $search[]  = "{{$el}}";
                    $replace[] = Context::get($el);
                }

                return str_replace($search, $replace, $this->pattern);
            }

            return $this->pattern;
        }
    }
}