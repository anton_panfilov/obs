<?php


abstract class Table_Field_Abstract {
    /**
     * Основная для этого поля переменная , которая будет получена из массива
     *
     * @var mixed
     */
    protected $name;

    /**
     * Заголовок поля
     *
     * @var mixed
     */
    protected $title;

    /**
     * Значение поля, если его значение приобразованное в тип string, дает пустую строку
     *
     * @var mixed
     */
    protected $default;

    /**
     * ID группы двойного меню
     *
     * @var null|int
     */
    public $header_group = null;

    /**
     * Удлиннение заголовка на несколько полей
     *
     * @var int
     */
    protected $head_stretch = 0;

    /**
     * Декораторы
     *
     * @var mixed
     */
    protected $decorators = array();

    /**
     * Атрибукты ячеек в колонке (table td)
     *
     * @var Html_Attribs
     */
    protected $td_attr;

    /**
     * Атрибукты ячеек в колонке (table th)
     *
     * @var Html_Attribs
     */
    protected $th_attr;

    /**
     * Показатль того что для этогй колонки надо добавить подсчет суммы
     *
     * @var bool
     */
    protected $footedAutoSum = false;

    /**
     * Возможность сортировки HTML таблицы средствами JS, тип сортировки, по умолчанию отключена (null)
     * Библиотека для сортировки jquery.tablesorter
     *
     * @var string|null
     */
    protected $sort = null;

    protected $description           = null;
    protected $description_help_icon = true;

    /**
     * @var Параметры передаваемые в декораторы
     */
    protected $decoratorParams;

    /**************************************************************************************/

    /**
     * Конструктор колонки
     *
     * При переопределении конструктора, он должен:
     * - или включать конструктор родителя
     * - или вызов init()
     *
     * @param string $name
     * @param string|null $title
     */
    public function __construct($name, $title = null){
        if(is_null($title)) $title = $name;

        $this->name  = (string)$name;
        $this->title = $title;

        $this->init();
    }

    /**
     * Обязательная инизилизация класса
     * Конструктор может быть переопределен, а init сохраняется в наследуемых классах
     */
    protected function init(){
        $this->th_attr  = new Html_Attribs();
        $this->td_attr  = new Html_Attribs();
    }

    /**
     * Предзагрузка перед отрисовкой данных
     * Используется для подготовки данных перед выводом, например массовая загрузка кешей
     */
    public function preLoad($data){}

    /**************************************************************************************/

    public function setDecoratorParam($name, $value){
        $this->decoratorParams[$name] = $value;
        return $this;
    }

    /**************************************************************************************/

    /**
     * Получить тип сортировки
     *
     * @return null|string
     */
    public function getSort(){
        return $this->sort;
    }

    /**
     * @return self
     */
    public function setSort_Off(){
        $this->sort = null;
        return $this;
    }

    /**
     * @return self
     */
    public function setSortText(){
        $this->sort = 'text';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortDigit(){
        $this->sort = 'digit';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortCurrency(){
        $this->sort = 'currency';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortUrl(){
        $this->sort = 'url';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortIsoDate(){
        $this->sort = 'isoDate';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortPercent(){
        $this->sort = 'percent';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortUsLongDate(){
        $this->sort = 'usLongDate';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortShortDate(){
        $this->sort = 'shortDate';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortTime(){
        $this->sort = 'time';
        return $this;
    }

    /**
     * @return self
     */
    public function setSortMetadata(){
        $this->sort = 'metadata';
        return $this;
    }

    /**************************************************************************************/

    /**
     * Получение имени поля
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    public function setDescription($description, $description_help_icon = true){
        $this->description              = $description;
        $this->description_help_icon    = $description_help_icon;
        return $this;
    }

    /**
     * Задать значенние по умолчанию, которое будет использоваться если в контексте не будет значения
     *
     * @param string $default
     * @return Table_Field_Abstract
     */
    public function setDefault($default){
        $this->default = $default;
        return $this;
    }

    /**
     * Установить автоматический посчет суммы по колонке
     *
     * @return Table_Field_Abstract
     */
    public function setFooterAutoSum(){
        $this->footedAutoSum = true;
        return $this;
    }

    /*************************************************************************************
     * Атрибуты заголовка
     */

    /**
     * Удилннение заголовка на несколько полей
     * При этом у нескольоких полей будет один заголовок в шапке
     *
     * @param $length
     * @return $this
     */
    public function setHeadStretch($length){
        $this->head_stretch = (int)$length;
        return $this;
    }

    public function getHeadStretch(){
        return $this->head_stretch;
    }


    /**
     * Задать значение атрибута
     * Или удалить его, если занчение = NULL
     *
     * @param string $name
     * @param string|null $value
     * @return self
     */
    public function setThAttrib($name, $value = null){
        $this->th_attr->setAttrib($name, Context::pattern($value));
        return $this;
    }

    /**
     * Устанвоить CSS, все переданные значения добавятся к свойству style во время отрисовки
     *
     * @param string $name
     * @param string|null $value
     * @return self
     */
    public function setThCSS($name, $value = null){
        $this->th_attr->setCSS($name, Context::pattern($value));
        return $this;
    }

    /*************************************************************************************
     * Атрибуты ячейки
     */

    /**
     * Задать значение атрибута
     * Или удалить его, если занчение = NULL
     *
     * @param string $name
     * @param string|null $value
     * @return self
     */
    public function setTdAttrib($name, $value = null){
        $this->td_attr->setAttrib($name, Context::pattern($value));
        return $this;
    }

    /**
     * Устанвоить CSS, все переданные значения добавятся к свойству style во время отрисовки
     *
     * @param string $name
     * @param string|null $value
     * @return self
     */
    public function setTdCSS($name, $value = null){
        $this->td_attr->setCSS($name, Context::pattern($value));
        return $this;
    }

    /**
     * Установить на колнку минимальную ширину
     *
     * @return Table_Field_Abstract
     */
    public function setWidthMin(){
        return $this
            ->setTdCSS('width', '1px')
            ->setTdCSS('white-space', 'nowrap');
    }

    /**
     * Установить на колнку  ширину
     *
     * @return Table_Field_Abstract
     */
    public function setWidth($width){
        return $this
            ->setTdCSS('width', $width);
    }

    /**
     * Текст в колонках по центру
     *
     * @return Table_Field_Abstract
     */
    public function setTextAlignCenter(){
        return $this
            ->setTdCSS('text-align', 'center')
            ->setThCSS('text-align', 'center');
    }


    /**
     * Размер шрифта
     *
     * @param $size
     * @return Table_Field_Abstract
     */
    public function setTextSize($size){
        return $this
            ->setTdCSS('font-size', $size);
    }

    /**
     * Текст в колонках справа
     *
     * @return Table_Field_Abstract
     */
    public function setTextAlignRight(){
        return $this
            ->setTdCSS('text-align', 'right')
            ->setThCSS('text-align', 'right');
    }

    /**
     * Текст в колонках по центру
     *
     * @return Table_Field_Abstract
     */
    public function setTextBold(){
        return $this
            ->setTdCSS('font-weight', 'bold');
    }

    /**
     * @param $color - Ex: #343433
     * @return Table_Field_Abstract
     */
    public function setTextColor($color){
        return $this->setTdCSS('color', $color);
    }

    /**
     * Добавить текст в конец атрибута
     *
     * @param string $name
     * @param string $value
     *
     * @return Table_Field_Abstract
     */
    public function addTdAttribPost($name, $value){
        $this->td_attr->addAttribPost($name, Context::pattern($value));
        return $this;
    }

    /**
     * Добавить текст в начало атрибута
     *
     * @param $name
     * @param $value
     * @return Table_Field_Abstract
     */
    public function addTdAttribPre($name, $value){
        $this->td_attr->addAttribPre($name, Context::pattern($value));
        return $this;
    }

    /*************************************************************************************
     * Функции настройки поля
     */

    /**
     * Добавление декоратора без проверки типа (не рекомендуется использовать)
     *
     * @param $decorator
     * @param mixed $index - Если ввести имя, то можно будет получать этот декоратор
     *                      по имени (если имя будет дублированное, будет вызванно исключение)
     *                       если оставить как есть (null), декоратор будет сделан на порядковый номер
     *
     * @throws \Exception
     * @return self
     */
    protected function addDecoratorAbstract($decorator, $index = null){
        if(is_null($index)){
            $this->decorators[] = $decorator;
        }
        else {
            if(isset($this->decorators[$index])){
                throw new \Exception("Decorator index `{$index}` already use");
            }
            else {
                $this->decorators[$index] = $decorator;
            }
        }

        return $this;
    }

    /**
     * Добавление декоратора как объекта класса \AP\Decorator\AbstractDecorator
     *
     * @param Decorator_Abstract $decorator
     * @param mixed $index - Если ввести имя, то можно будет получать этот декоратор по имени
     *                       (если имя будет дублированное, будет вызванно исключение)
     *                       если оставить как есть (null), декоратор будет сделан на порядковый номер
     *
     * @return self
     */
    public function addDecoratorObject(Decorator_Abstract $decorator, $index = null){
        return $this->addDecoratorAbstract($decorator, $index);
    }

    /**
     * Добавление декоратора как callable функции
     *
     * @param \Closure $decorator
     * @param mixed $index - Если ввести имя, то можно будет получать этот декоратор по имени
     *                      (если имя будет дублированное, будет вызванно исключение)
     *                       если оставить как есть (null), декоратор будет сделан на порядковый номер
     *
     * @return self
     */
    public function addDecoratorFunction(\Closure $decorator, $index = null){
        return $this->addDecoratorAbstract($decorator, $index);
    }

    /**
     * Получить декоратор
     *
     * @param mixed $name
     * @throws \Exception
     * @return Decorator_Abstract
     */
    public function getDecorator($name){
        if(!isset($this->decorators[$name])){
            throw new \Exception("Decorator `{$name}` not found");
        }

        return $this->decorators[$name];
    }

    /*************************************************************************************
     * Отрисовка
     */

    /**
     * Отрисовка названия поля
     */
    public function renderTitle(){
        if(strlen($this->description)){
            return
                Translate::t($this->title) .
                ($this->description_help_icon ? " <i class='fa fa-question-circle help_icon'></i>" : "") .
                " <div class='table_help_div'>" . Translate::t($this->description) . "</div>";
        }

        return Translate::t($this->title);
    }

    /**
     * Получить строку параметров для td
     *
     * @return string
     */
    public function getTdAttribs(){
        return $this->td_attr;
    }

    /**
     * Получить строку параметров для th
     *
     * @return string
     */
    public function getThAttribs(){
        if(strlen($this->description)){
            $attr = clone $this->th_attr;

            $attr->addAttribPost("class", " table_help_td");

            return $attr;
        }

        return $this->th_attr;
    }

    /**
     * Нужно ли для этогй колонки подсчитать сумму в атоматическом режиме?
     *
     * @return bool
     */
    public function isFooterAutoSum(){
        return $this->footedAutoSum;
    }

    /**
     * Отрисовка непосредтвенно поля:
     * - из контекста берется переменная с именем $this->name (если значения нет в контексте берется значение по умолчанию)
     * - значение проходит через декораторы
     *
     * @return string
     */
    public function render(){
        $value = Context::get($this->name, $this->default);

        if(count($this->decorators)){
            foreach($this->decorators as $decorator){
                if($decorator instanceof Decorator_Abstract){
                    $value = $decorator->render($value);
                }
                else if($decorator instanceof \Closure){
                    $value = $decorator($value, Context::getArray(), $this->name, $this->decoratorParams);
                }
            }
        }

        return $value;
    }

    /**
     * Добавить функцию, вызываемую через AJAX
     * - что бы вызвать функцию, надо передать ей имя через get переменную ap_table_ajax_func
     * - функции никуда не сохраняются, а вызываются в реальном времени, при этом надо учитывать
     *   что добавляться она долна, только если у данного пользоваетля, есть права на её выполнение
     *   или внутри функции проверяются права
     *
     * @param string $name
     * @param \Closure $callable
     *
     * @return static
     */
    public function ajaxFunction($name, \Closure $callable){
        Table::ajaxFunction($name, $callable);
        return $this;
    }

    /**
     * Включить сортировку по полю
     *
     * @return $this
     */
    public function setSorting(){
        $this->th_attr->addAttribPost("class", " sorting");
        return $this;
    }
}