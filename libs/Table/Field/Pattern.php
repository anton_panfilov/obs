<?php

class Table_Field_Pattern extends Table_Field_Text {
    const DECORATOR_NAME_PATTERN = 'pattern';

    public function __construct($pattern, $title, $name = null){
        $this->title = $title;
        $this->name = $name;

        $this->addDecoratorObject(new Decorator_Pattern($pattern), self::DECORATOR_NAME_PATTERN);

        $this->init();
    }
}