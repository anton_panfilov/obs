<?php

class Table_Field_Button extends Table_Field_Abstract {
    /**
     * Объект кнопки
     *
     * @var Ui_Button
     */
    protected $button;


    public function __construct($label = null, $title = null){
        $this->title = $title;

        $this->init();

        // Создание и настрйока кнопки
        $this->button = new Ui_Button();
        $this->button->setCSS("padding", '1px 7px 1px 7px');
        $this->button->setCSS("font-size", '12px');
        $this->button->setCSS("line-height", '19px');
        $this->button->setCSS("margin", '0px');

        if(strlen($label)){
            $this->button->setLabel($label);
        }

        // Настройка поля
        $this->setTdCSS('padding', '2px 4px 2px 4px');
        $this->setTdCSS('width', '1px');


    }

    /**********************************************************************************************/

    /**
     * Стиль: красная кнопка
     *
     * @return $this
     */
    public function setStyleRed(){
        $this->button->setStyleRed();
        return $this;
    }

    /**********************************************************************************************/

    /**
     * Установить url, на который будет перенаправлени клиент при нажатии на кнопку
     * В $link можно использовать переменные контекста. Напрмиер: /account/user/{id};
     *
     * @param mixed $link
     * @return self
     */
    public function setLink($link){
        $this->button->setLink(Context::pattern($link));
        return $this;
    }

    /**
     * Установить JavaScript, который выполниться при нажатии на кнопку
     * В $javaScript можно использовать переменные контекста.
     *
     * @param string $javaScript
     * @return self
     */
    public function setScript($javaScript){
        $this->button->setScript(Context::pattern($javaScript));
        return $this;
    }

    /**
     * @return Ui_Button
     */
    public function getButtonObject(){
        return $this->button;
    }

    /**
     * Устанвоить текст вопроса, который будет подтверждать нажати не кнопку перед выполением скриптиа и(или) редиректа
     * Например: Вы действительно хотите ужалить этого пользователя?
     * В $message можно использовать переменные контекста.
     *
     * @param mixed $message
     * @return self
     */
    public function setConfirm($message){
        $this->button->setConfirm(Context::pattern($message));
        return $this;
    }

    /**********************************************************************************************/

    /**
     * Отрисовка полностью заменяется на кнопку
     *
     * @return string
     */
    public function render(){
        return $this->button->render();
    }

}