<?php

class Table_Field_Text extends Table_Field_Abstract {
    const DECORATOR_NAME_LINK = 'link';

    /**
     * Сделать из переменной ссылку
     *
     * @param mixed $url
     * @param bool $targetBlank
     *
     * @return Table_Field_Text
     */
    public function setLink($url, $targetBlank = false){
        $decorator = new Decorator_Link($url);

        if($targetBlank){
            $decorator->setAttrib("target", "_blank");
        }

        $this->addDecoratorObject($decorator, self::DECORATOR_NAME_LINK);

        return $this;
    }
}