<?php

class Table_Field_Function extends Table_Field_Abstract {
    /**
     * @var Closure
     */
    protected $closureFunction;

    protected $params = [];

    public function __construct(Closure $function, $title, $name = null){
        $this->title = $title;
        $this->name = $name;
        $this->closureFunction = $function;

        $this->init();
    }

    /**
     * Установить параметр, набор которых будет передаваться в функцию 3-её переменной
     *
     * @param $name
     * @param $value
     * @return $this
     */
    public function setParam($name, $value){
        $this->params[$name] = $value;
        return $this;
    }

    public function render(){
        $func = $this->closureFunction;
        return $func(Context::getArray(), Context::get($this->name, $this->default), $this->params);
    }
}