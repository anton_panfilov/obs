<?php

class Table_Field_Date extends Table_Field_Text {
    const DECORATOR_NAME_DATE = 'date';

    protected function init(){
        parent::init();

        // добавление декоратора
        $this->addDecoratorObject(new Decorator_Date(), self::DECORATOR_NAME_DATE);
    }

    /****************************************************************************/

    /**
     * @return Decorator_Date
     */
    public function getDecoratorDate(){
        return $this->getDecorator(self::DECORATOR_NAME_DATE);
    }

    /**
     * Задать фиксированный формат даты (формат задается как для php функции date())
     * Если оставить формат пустым, то он будет настраиваться в зависимости от многих параметров
     *
     * @param string|null $format
     * @return Table_Field_Date
     */
    public function setDateFormatFixed($format = null){
        $this->getDecoratorDate()->setDateFormatFixed($format);
        return $this;
    }

    /**
     * Выделать выходнгые
     *
     * @param bool $flag
     * @return $this
     */
    public function setDateMarkWeekends($flag = true){
        $this->getDecoratorDate()->markWeekends($flag);
        return $this;
    }


    /**
    * Показывать дату
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function showDate($flag = true){
        $this->getDecoratorDate()->showDate($flag);
        return $this;
    }

    /**
    * Показывать время
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function showTime($flag = true){
        $this->getDecoratorDate()->showTime($flag);
        return $this;
    }

    /**
    * Показывать секунды
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function showSeconds($flag = true){
        $this->getDecoratorDate()->showSeconds($flag);
        return $this;
    }

    /**
    * Показывать секунды сегодня, даже если вобщем они отключены
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function showSecondsToday($flag = true){
        $this->getDecoratorDate()->showSecondsToday($flag);
        return $this;
    }

    /**
    * Если день сегодняшний, то не показывать дату
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function todayOnlyTime($flag = true){
        $this->getDecoratorDate()->todayOnlyTime($flag);
        return $this;
    }

    /**
    * Показывать в дате текущий год
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function showCurrentYear($flag = true){
        $this->getDecoratorDate()->showCurrentYear($flag);
        return $this;
    }

    /**
    * Не показывать время за предыдущий год
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function lastYearNonTime($flag = true){
        $this->getDecoratorDate()->lastYearNonTime($flag);
        return $this;
    }

    /**
    * Показать детали, если что то может быть скрыто
    *
    * @param bool $flag
    * @return Table_Field_Date
    */
    public function showDetails($flag = true){
        $this->getDecoratorDate()->showDetails($flag);
        return $this;
    }

    /****************************************************************************/
}