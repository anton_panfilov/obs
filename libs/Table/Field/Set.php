<?php

class Table_Field_Set extends Table_Field_Abstract {
    /**
     * Настйроки списка
     *
     * @var Ui_Set
     */
    protected $set;

    /**
     * @param string $name
     * @param null|string $title
     * @param Ui_Set|array|null $set
     */
    public function __construct($name, $title = null, $set = null){
        parent::__construct($name, $title);

        // Иницилизация объенкта \AP\Ui\Set
        if($set instanceof Ui_Set){
            $this->set = $set;

            $reason = $set->renderComment();
            if(strlen($reason)) {
                $this->setDescription($reason);
            }
        }
        else {
            $this->set = new Ui_Set();

            if(is_array($set)){
                $this->set->setArray($set);
            }
        }

        // Настройка поля
        $this->setTdCSS('padding', '1px');
        $this->setTdCSS('width', '1px');
    }

    /**********************************************************************************************/

    /**
     * Задать массив значений
     * (При этом текущие настрйоки обнуляются)
     *
     * Пример 1 (Просто названия):
     * [
     *     1 => 'Active',
     *     2 => 'Paused',
     * ]
     *
     * Пример 2 (Названия и цвета):
     * [
     *     1 => ['Active', \AP\Ui\Colors::BACKGROUND_GREEN],
     *     2 => ['Paused', \AP\Ui\Colors::BACKGROUND_RED],
     * ]
     *
     * @param array $array
     * @return self
     */
    public function setArray(array $array){
        $this->set->setArray($array);
        return $this;
    }

    /**
     * Добавление значения
     *
     * @param string $key
     * @param string $label
     * @param string $color
     *
     * @return self
     */
    public function addValue($key, $label = null, $color = null){
        $this->set->addValue($key, $label, $color);
        return $this;
    }

    /************************/

    public function addValue_Green($key, $label = null){
        $this->set->addValue_Green($key, $label);
        return $this;
    }

    public function addValue_Red($key, $label = null){
        $this->set->addValue_Red($key, $label);
        return $this;
    }

    public function addValue_Yellow($key, $label = null){
        $this->set->addValue_Yellow($key, $label);
        return $this;
    }

    public function addValue_White($key, $label = null){
        $this->set->addValue_White($key, $label);
        return $this;
    }

    /**********************************************************************************************/

    /**
     * Установить ссылку
     *
     * @param string $url
     * @param bool $targetBlank
     * @return self
     */
    public function setLink($url, $targetBlank = false){
        $this->set->setAttrib("href", Context::pattern($url));

        if($targetBlank){
            $this->set->setAttrib("target", "_blank");
        }

        return $this;
    }

    /**********************************************************************************************/

    /**
     * Отрисовка списка, при этом значение получается с учетом декораторов
     *
     * @return string
     */
    public function render(){
        return $this->set->render(
            parent::render()
        );
    }

}