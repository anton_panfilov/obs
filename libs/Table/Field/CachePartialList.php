<?php

class Table_Field_CachePartialList extends Table_Field_Text {
    const DECORATOR_NAME_CACHE = 'cache';

    public function __construct(Cache_PartialList_Abstract $cacheClass, $name, $title = null){
        parent::__construct($name, $title);

        $this->addDecoratorObject(
            new Decorator_CachePartialList($cacheClass),
            self::DECORATOR_NAME_CACHE
        );
    }

    public function preLoad($data){
        // Value::export($data);
        //Value::export(get_class($this->getDecorator(self::DECORATOR_NAME_CACHE)));

        if(count($data)){
            $res = [];
            foreach($data as $el){
                $res[] = isset($el[$this->getName()]) ? $el[$this->getName()] : null;
            }
            $res = array_unique($res);

            //Value::export($this->getDecorator(self::DECORATOR_NAME_CACHE)->getCacheClass());
            //Value::export($res);

            $this->getDecorator(self::DECORATOR_NAME_CACHE)->load($res);
        }
    }
}