<?php

class Table_Paging {
    /**
    * Название GET переменной, для отслеживания сттаницы
    */
    protected $getValueName = '_page';


    protected $showOnePageCounts = true;

    protected $pagesButtonsCount = 11;

    /**
    * Общее количесво элементов данных
    *
    * @var int
    */
    protected $countElements;

    /**
    * Количество страниц
    *
    * @var int
    */
    protected $pagesCount;

    /**
    * Максимальное количесво элементов на странице
    *
    * @var int
    */
    protected $pageLen = 100;

    /**
    * Номер текущей страницы
    * (Начинается с 1)
    *
    * @var int
    */
    protected $curentPage;


    /**
    * Какой элемент из общего списка будет первым для текущей страницы
    *
    * @var int
    */
    protected $limitOffset;

    /**
    * Сколько элементов будет на странице (вполне возможно что она не будет полностью заполнена)
    *
    * @var int
    */
    protected $limitCount;

    /**************************************************************************************************/

    /**
     * Установить максимальное количесво элементов на странице
     *
     * @param int $len
     * @return Table_Paging
     */
    public function setPageLen($len){
        $this->pageLen = (int)$len;
        return $this;
    }

    /**
     * @param $value
     *
     * @return Table_Paging
     */
    public function setGetValueName($value){
        $this->getValueName = $value;
        return $this;
    }

    /**
     * @param $flag
     *
     * @return Table_Paging
     */
    public function setShowOnePageCounts($flag){
        $this->showOnePageCounts = (bool)$flag;
        return $this;
    }

    /**************************************************************************************************/

    /**
    * Пересчитать данные объекта
    *
    * @param mixed $countElements
    * @param mixed $page
    */
    public function calculate($countElements, $page = null){
        // обработка номера страницы (получение из GET переменной, если не передать вручную)
        if(is_null($page)) $page = isset($_GET[$this->getValueName]) ? $_GET[$this->getValueName] : 1;
        $page = (int)$page;
        if($page < 1) $page = 1;

        $this->countElements = (int)$countElements;
        $this->curentPage = (int)$page;

        $this->pagesCount = ceil($this->countElements / $this->pageLen);

        if($this->curentPage > $this->pagesCount)   $this->curentPage = $this->pagesCount;
        if($this->curentPage < 1)                   $this->curentPage = 1;

        $this->limitOffset = $this->pageLen * ($this->curentPage - 1);

        if($this->curentPage == $this->pagesCount)  $this->limitCount = $this->countElements - ($this->pageLen * ($this->pagesCount - 1));
        else                                        $this->limitCount = $this->pageLen;
    }

    public function getLimitCount(){
        return $this->limitCount;
    }

    public function getLimitOffset(){
        return $this->limitOffset;
    }

    public function getPagesCount(){
        return $this->pagesCount;
    }

    public function getPageLen(){
        return $this->pageLen;
    }

    public function getCountElements(){
        return $this->countElements;
    }

    public function isLastPage(){
        return ($this->pagesCount == $this->curentPage);
    }

    /**
     * Преобразовать данные, оставив только данные определенной тсраницы и настроить объект
     *
     * @param array|Db_Statment_Select $data
     * @throws Exception
     * @return Db_Statment_Select|array
     */
    public function getLimitData($data){
        if($data instanceof Db_Statment_Select){
            /*
                TODO: сейчас работат только для простоых запросов.

                 Дорабоать:
                  1) уже лимиторованных
                  2) с групировкой
                  3) distinct    - готово

            */

            $this->calculate($data->fetchCount());

            $dataNew = clone $data;
            $dataNew->limit($this->limitCount, $this->limitOffset);

            return $dataNew;
        }
        else if(is_array($data)){
            $this->calculate(count($data));
            $result = array();

            if(count($data)){
                // используется foreach, что бы если ключи массива не последовательны, выпорка работала коректно
                $i = 0;
                $break = $this->limitOffset + $this->limitCount;
                foreach($data as $el){
                    if($i == $break){
                        break;
                    }
                    else if($i >= $this->limitOffset){
                        $result[] = $el;
                    }

                    $i++;
                }
            }

            return $result;
        }
        else {
            throw new \Exception("Invalid Data Type");
        }
    }

    /**************************************************************************************************/

    /**
    * Отрисовать текущий пейджинг
    */
    public function render(){
        Translate::addDictionary('table/paging');
        ob_start();
        if($this->pagesCount > 1) {
            ?><p class="muted"><?=Translate::t('paging_Pages')?>: &nbsp;<?

            $page_now = $this->curentPage - 1;

            $start_page = $page_now - floor($this->pagesButtonsCount / 2);
            if($start_page < 0) $start_page = "0";
            $finish_page = $start_page + $this->pagesButtonsCount;
            if($finish_page > $this->pagesCount - 1)$finish_page = $this->pagesCount - 1;

            # ссылка на первую страницу
            if($start_page>0) {
                ?><a class='label txt-color-darken'  style="font-weight: normal" href="<?=Http_Url::convert(array($this->getValueName => null))?>">1</a> ... <?
            }

            # видимый список ссылок на сатрницы
            for($i=$start_page;$i<=$finish_page;$i++) {
                if($i == $page_now) {
                    ?><b class='label label-primary bold'><?=($i + 1)?></b> <?
                }
                else {
                    ?><a class='label txt-color-darken' style="font-weight: normal" href="<?=Http_Url::convert(array(
                        $this->getValueName => ($i == 0) ? null : ($i + 1) // для первой страницы в гет ничего не ставится
                    ))?>"><?=($i + 1)?></a> <?
                }
            }
            # последняя страница
            if($finish_page < $this->pagesCount-1) {
                ?>... <a class='label txt-color-darken' style="font-weight: normal" href="<?=Http_Url::convert(array($this->getValueName => $this->pagesCount))?>"><?
                    echo $this->pagesCount
                ?></a><?
            }

            if($this->pagesCount > $this->pagesButtonsCount){
                ?> &nbsp;<small>(<?=Translate::t('paging_Pages') . ": {$this->pagesCount}; " . Translate::t('paging_AllItems')  . ": {$this->countElements}"?>)</small><?
            }
            else if($this->pagesCount > 1){
                ?> &nbsp;<small>(<?=Translate::t('paging_AllItems') . ": {$this->countElements}"?>)</small><?
            }
            ?></p><?
        }
        else {
            if($this->countElements > 0 && $this->showOnePageCounts){
                ?><p class="muted">
                    <?=Translate::t('paging_OnePage')?>&nbsp;<small>(<?=Translate::t('paging_AllItems') . ": <span class='t3ui_paging_items'>{$this->countElements}</span>"?>)</small>
                </p><?
            }
        }
        $paging = ob_get_clean();

        return  $paging;
    }

    public function __toString(){
        return (string)$this->render();
    }
}