<?php

class Order_Item extends Db_Model {
    static protected $_table = 'orders_list';
    static protected $_structure = [

        // не обновляються при update, если явно не указанны
        ['status'],
        ['key'],
        ['create'],
        ['date'],
        ['manager'],
        ['client_id'],
        ['client_data'],
        ['client_comment'],
        ['manager_comment'],
        ['delivery_type'],
        ['delivery_data'],
        ['price'],
        ['count'],
    ];


    protected function getDatabase(){
        return Db::site();
    }

    /*********************************/

    public $status = Order_Status::CREATE;
    public $key;
    public $create;
    public $date;
    public $manager;
    public $client_id;
    public $client_data = [];
    public $client_comment;
    public $manager_comment;
    public $delivery_type = Order_DeliveryType::UNKNOWN;
    public $delivery_data = [];
    public $price = 0;
    public $count = 0;


    /*************************************************************************/

    /**
     * Иницилизироать массив данных в объект
     *
     * @param array $array
     */
    protected function baseToObject($array){
        parent::baseToObject($array);

        $this->client_data = Json::decode($this->client_data);
        if(!is_array($this->client_data)){
            $this->client_data = [];
        }

        $this->delivery_data = Json::decode($this->delivery_data);
        if(!is_array($this->delivery_data)){
            $this->delivery_data = [];
        }
    }

    /**
     * Формирует из объекта массив для базы данных
     *
     * @param array $only
     * @param bool $update
     * @return array
     */
    protected function objectToBase($only = [], $update = false){
        $result = parent::objectToBase();

        if(isset($result['client_data'])){
            $result['client_data'] = Json::encode(
                is_array($result['client_data']) ? $result['client_data'] : []
            );
        }

        if(isset($result['delivery_data'])){
            $result['delivery_data'] = Json::encode(
                is_array($result['delivery_data']) ? $result['delivery_data'] : []
            );
        }

        return $result;
    }

    /*************************************************************************/

    public function __construct(){
        $this->key    = String_Hash::randomString(16);
        $this->create = date('Y-m-d H:i:s');
        $this->date   = date('Y-m-d');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected function renderFormMain_createOrder(){
        return $this->renderFormGoods();
    }

    public function renderFormMain(){
        Translate::addDictionary('order/form', 20);

        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("createOrder",    "tab_process_createOrder");
        $tabs->addTab("order",          "tab_process_order");
        $tabs->addTab("waitGoods",      "tab_process_waitGoods");
        $tabs->addTab("delivery",       "tab_process_delivery");
        $tabs->addTab("waitPay",        "tab_process_waitPay");

        $method = "renderFormMain_" . $tabs->getActiveTab();

        if(method_exists($this, $method)){
            $tabs = $tabs->render() . $this->$method();
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }



        return $tabs;

        /*
        ?>

        <div style="padding: 5px 30px; background: #c5fbd4; color: #666; border-radius: 10px; margin: 3px; border: #addebb solid 1px; font-size: 80%">
            <h5>Формирование заказа</h5>
            <ul>
                <li>Контактные данные клиента</li>
                <li>Товары</li>
                <li>Доставка</li>
            </ul>
        </div>
        <div style="padding: 20px 30px; background: #FFF; color: #333; border-radius: 10px; margin: 3px; border: #CCC solid 1px">
            <h2>Заказ товаров</h2>
            <ul>
                <li>Список товаров, заказанные, не заказанные, не найдаенные</li>
            </ul>
            <br>
            <?
                $table = new Table([['name' => 'vasya']]);
                echo $table->render();
            ?>
        </div>
        <div style="padding: 5px 30px; background: #F5F5F5; color: #999; border-radius: 10px; margin: 3px; border: #EEE solid 1px; font-size: 80%">
            <h5>Сборка товаров для заказа</h5>
            <ul>
                <li>Что уже получили, что еще в ожидании (треки, дата заказа, орентировачная дата получения)</li>
            </ul>
        </div>
        <div style="padding: 5px 30px; background: #F5F5F5; color: #999; border-radius: 10px; margin: 3px; border: #EEE solid 1px; font-size: 80%">
            <h5>Доставка</h5>
            <ul>
                <li>Если курьер, договориться с клиентом на дату и время доставки, комментрий клиента</li>
                <li>Если почта, статус - отправленно или нет, для отправленных - дата + трекинг</li>
            </ul>
        </div>
        <div style="padding: 5px 30px; background: #F5F5F5; color: #999; border-radius: 10px; margin: 3px; border: #EEE solid 1px; font-size: 80%">
            <h5>Получение платежа</h5>
            <ul>
                <li>Фиксация факта получения денег</li>
            </ul>
        </div>

    <?
        */
    }

    public function renderFormClient(){
        Translate::addDictionary('order/form', 20);

        $form = new Form();

        $client_fields = [
            'name'              => 'client_name',
            'vk'                => 'client_vk',
            'mobile_phone'      => 'client_mobile_phone',
            'email'             => 'client_email',
            'other_contacts'    => 'client_other_contacts',
        ];

        if($this->id){
            $form->addButton('btn_save');
        }
        else {
            $form->setMarkRequiredFields(true);
            $form->addButton('btn_continue');
        }

        //////////////////////////////////////////////////////////////////////////////

        if(!$this->id) {
            $form->addSeparator('form_sep_info');

            $form->addElementDate('date', 'form_date')
                ->setValue(date('Y-m-d'));

            if(UserAccess::isAdmin()){
                $form->addElement_ClientManager('manager', 'form_manager');
            }
            else if(UserAccess::isClientManager()){
                $form->addElementStatic('form_manager', Cache_PartialList_UserLogin::get(Users::getCurrent()->id));
            }
        }

        //////////////////////////////////////////////////////////////////////////////

        $form->addSeparator("form_sep_client");

        $form->addElementText('client_name', 'form_client_name')
            ->setRenderTypeBigValue();

        //////////////////////////

        $form->addElementText_PhoneMobileRus('client_mobile_phone', 'form_client_mobile_phone')
            ->setPlaceholder('form_client_mobile_phone')
            ->setRequired(false);

        $form->addElementText('client_email', 'form_client_email')
            ->setPlaceholder('form_client_email')
            ->setRequired(false);

        $form->addMultiElement(['client_mobile_phone', 'client_email'], 'form_client_mobile_phone_and_email')
            ->setRenderTypeBigValue();

        //////////////////////////

        $form->addElementText('client_vk', 'form_client_vk')
            ->setRenderTypeBigValue()
            ->setRequired(false);

        $form->addElementTextarea('client_other_contacts', 'form_client_other_contacts')
            ->setAutoHeight()
            ->setRequired(false)
            ->setRenderTypeBigValue();

        /*
        $form->addElement_Region('client_region', 'form_client_region')
            ->setRequired(false);

        $form->addElementText('client_city', 'form_client_city')
            ->setRequired(false);

        $form->addMultiElement(['client_region', 'client_city'], 'form_client_region_and_city')
            ->setRenderTypeBigValue();

        $form->addElementText('client_address', 'form_client_address')
            ->setRenderTypeBigValue()
            ->setRequired(false);
        */

        //////////////////////////////////////////////////////////////////////////////

        $form->addSeparator('form_sep_comments');
        $form->addElementTextarea('manager_comment', 'form_manager_comment')
            ->setAutoHeight()
            ->setRenderTypeBigValue()
            ->setRequired(false);

        $form->addElementTextarea('client_comment', 'form_client_comment')
            ->setAutoHeight()
            ->setRenderTypeBigValue()
            ->setRequired(false);


        if($this->id){
            foreach($client_fields as $k => $v){
                $form->setValue($v, isset($this->client_data[$k]) ? $this->client_data[$k] : '');
            }
        }

        if($form->isValidAndPost()){

            $client_data = [];
            foreach($client_fields as $k => $v){
                $client_data[$k] = $form->getValue($v);
            }

            $this->client_data      = $client_data;
            $this->client_comment   = $form->getValue('client_comment');
            $this->manager_comment  = $form->getValue('manager_comment');

            if($this->id){
                $form->addSuccessMessage();
                $this->update(["client_comment", "manager_comment", "client_data"]);
            }
            else {
                // создание нового заказа
                $this->date = $form->getValue('date');

                if(UserAccess::isAdmin()){
                    $this->manager = $form->getValue('manager');
                }
                else if(UserAccess::isClientManager()){
                    $this->manager = Users::getCurrent()->id;
                }

                $this->insert();

                header('location: /account/orders/order/goods?id=' . $this->id);
            }
        }

        return $form->render();
    }

    //////////////////////////////////////////////////////////////////////

    protected function renderFormGoods_form(){
        $tabs = new Tabs_StaticTabs();

        $tabs->addTab("ByOrder", "tab_ByOrder");
        $tabs->addTab("InStock", "tab_InStock");

        $method = "renderFormGoods_form_" . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $tabs->setText($this->$method());
        }
        else {
            $tabs->setText(new View_Page_ComingSoon());
        }

        return "<h3>" . Translate::t("title_add_catalog_item") . "</h3>" . $tabs->render();
    }

    /**
     * Добавление товара из наличия
     *
     * @return string
     */
    protected function renderFormGoods_form_InStock(){
        $form = new Form();

        return $form->render();
    }

    /**
     * Добавление товаров на заказ или редактирлвание уже добавленного товара
     *
     * @param null $id
     *
     * @return Form
     */
    protected function renderFormGoods_form_ByOrder($id = null){

        $formTitle = $id ? Translate::t("title_edit_catalog_item", $id) : "";

        $form = new Form($formTitle);

        $form->setMarkRequiredFields(1);

        if($id){
            $form->addButton("btn_save", "save");
            $form->addButton("btn_back", "back")
                ->setStyleDefault()
                ->setLink(
                    Http_Url::convert(["edit_item_from_order" => null])
                );

            $form->addElementStatic(
                Catalog_Remains::getObject($id)->getItem()->renderImage(),
                Catalog_Remains::getObject($id)->getItem()->renderCard()
            )
                ->setRenderTypeBigValue();
        }
        else {
            $form->addButton("btn_add");

            Site::addJSCode("
                document.item = '';
                document.changeItemPhotos = function(){

                    if(jQuery('#item').val() != document.item){
                        document.item = jQuery('#item').val();

                        if(document.item == ''){
                            jQuery('#item_ptotos').html('');
                        }
                        else {
                            jQuery('#item_ptotos').html('<img style=\"margin:35px\" src=\"/static/img/load/89x30.cyrc.gif\">');

                            jQuery.getJSON(
                                \"/account/catalog/get-item-info\",
                                {
                                    item: document.item
                                },
                                function(data){

                                    if(parseInt(data.id) == parseInt(document.item)){
                                        if(data.photo.length){
                                            jQuery('#item_ptotos').html('<img style=\"max-height: 150px\" src=\"' + data.photo + '\">');
                                        }
                                        else {
                                            jQuery('#item_ptotos').html('');
                                        }

                                        jQuery('#price').val(data.price);
                                    }
                                }
                            );
                        }
                    }
                }

                jQuery('#item_txt').blur(function(){
                    setTimeout(function(){
                        document.changeItemPhotos();
                    }, 200);
                })
                document.changeItemPhotos();
            ");

            $form->addElement_CatalogItem('item', 'form_catalog_item')
                ->setComment("
                    <div style='float:right; width: 200px'>
                        <div style='position: absolute;'>
                            <div style=' overflow: hidden; height: 150px; border: #DDD solid 1px; background: #FFF; min-width: 150px' id='item_ptotos'></div>
                        </div>
                    </div>
                ");
        }


        $form->addElementText_Num('count', 'form_count', 1, 999)
            ->setValue(1);

        $form->addElementText_Price('price', 'form_price')
            ->setWidth("100px");

        $form->addElementTextarea('comment', 'form_comment')
            ->setRequired(false);

        if($id){
            $form->setValues([
                "count"   => Catalog_Remains::getObject($id)->count,
                "price"   => Catalog_Remains::getObject($id)->price,
                "comment" => Catalog_Remains::getObject($id)->comment,
            ]);
        }

        if($form->isValidAndPost()){
            if($id){
                $rem = Catalog_Remains::getObject($id);

                $rem->price     = $form->getValue('price');
                $rem->count     = $form->getValue('count');
                $rem->comment   = $form->getValue('comment');

                $rem->update(["price", "count", "comment"]);


            }
            else {
                $rem = new Catalog_Remains();

                $rem->photo     = Catalog_Item::getObject($form->getValue('item'))->main_photo;
                $rem->status    = Catalog_RemainsStatus::CREATING;
                $rem->order     = $this->id;
                $rem->item      = $form->getValue('item');
                $rem->price     = $form->getValue('price');
                $rem->count     = $form->getValue('count');
                $rem->comment   = $form->getValue('comment');

                $rem->insert();
            }

            $this->updateCachePrice();

            if($id){
                header("location:" . Http_Url::convert(["edit_item_from_order" => null]));
            }
            else {
                $form->setValues([
                    'item'    => null,
                    'price'   => null,
                    'age'     => null,
                    'comment' => null,
                ]);
            }
        }

        return $form;
    }

    public function renderFormGoods(){
        Translate::addDictionary('order/form', 20);

        // удаление
        if(isset($_GET['remove_item_from_order'])){
            if(Catalog_Remains::getObject($_GET['remove_item_from_order'])->id){
                if(Catalog_Remains::getObject($_GET['remove_item_from_order'])->status < Catalog_RemainsStatus::WAIT){
                    Db::site()->delete(
                        "catalog_remains",
                        "`order`=? and `id`=?",
                        $this->id,
                        (int)$_GET['remove_item_from_order']
                    );
                }
                else {
                    Db::site()->update(
                        "catalog_remains",
                        [
                            'order' => null,
                        ],
                        "`order`=? and `id`=?",
                        $this->id,
                        (int)$_GET['remove_item_from_order']
                    );
                }

                header(
                    "location:" . Http_Url::convert([
                        "remove_item_from_order" => null
                    ])
                );

                $this->updateCachePrice();

                return;
            }
        }

        // редактирование
        if(isset($_GET['edit_item_from_order'])){
            if(Catalog_Remains::getObject($_GET['edit_item_from_order'])->id){
                echo $this->renderFormGoods_form_ByOrder(Catalog_Remains::getObject($_GET['edit_item_from_order'])->id);
            }
            return;
        }

        $form = $this->renderFormGoods_form();

        $all = Db::site()->fetchAll("select *, (`count` * `price`) as total from `catalog_remains` where `order`=?", $this->id);
        $table = new Table($all);
        $table->setPagingDisabled();

        $table->addField_Text("id", "tbl_id")
            ->setLink("/account/catalog/remains?id={id}")
            ->setWidthMin();

        //$table->addField_SetArray("status", "tbl_status", new Catalog_RemainsStatus());

        $table->addField_Function(function($c){
            $item = Catalog_Item::getObject($c['item']);

            $imageLink = $item->getImageLink();

            // $link = "/account/catalog/item/?id={$item->long_id}";
            $link = "/account/catalog/remains?id={$c['id']}";

            ob_start();
            ?>
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <?if($imageLink){?>
                        <a href='<?=$link?>'><img style="max-width: 100%" src="<?=$imageLink?>"></a>
                    <?}else{?>
                        <?=Translate::t("no_image")?>
                    <?}?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9" style="margin-bottom: 70px">
                    <h4><a href='<?=$link?>'><?=String_Filter::cutByWord($item->title, 50)?></a></h4>
                    <div style="margin: 10px;;"><?=(new Catalog_RemainsStatus)->setInLine()->render($c['status'])?></div>
                    
                    <?
                    $tbl = new View_Table_Simple();

                    //$tbl->add("tbl_status", (new Catalog_RemainsStatus)->setInLine()->render($c['status']));

                    $tbl->add(
                        "tbl_item_type",
                        Catalog_Types::getTypeInfo(Catalog_Item::getObject($c['item'])->type, false)['full_name']
                    );


                    if(count(Catalog_Options::getRemainsOptions($c['id']))){
                        foreach(Catalog_Options::getRemainsOptions($c['id']) as $k => $v){
                            $tbl->add(
                                Catalog_Options::getTypeName($k),
                                Catalog_Options::getValue($k, $v)
                            );
                        }
                    }

                    /*
                    $tbl->add(
                        "tbl_item_long_id",
                        "<a href='{$link}'>" . $item->long_id . "</a>"
                    );
                    */

                    /*
                    if($item->link) {
                        $url = parse_url($item->link);

                        $tbl->add(
                            "tbl_link",
                            "<a class='txt-color-greenDark' target='blank' href='{$item->link}'>" .
                                (isset($url['host']) ? $url['host'] : substr($item->link, 0, 32) . "...") .
                            "</a>"
                        );
                    }
                    */

                    /*
                    if($c['age']) {

                        $tbl->add(
                            Catalog_Options::getTypeName(Catalog_Options::AGE),
                            Catalog_Options::getTypeValueLabel(Catalog_Options::AGE, $c['age'])
                        );
                    }
                    */

                    if($c['comment'] && mb_strlen($c['comment'])) {
                        $tbl->add("tbl_comment", $c['comment']);
                    }

                    echo $tbl->render();
                    ?>
                </div>
            </div>
            <?
            return ob_get_clean();
        });



        $table->addField_Text("count", "tbl_count")
            ->setTextAlignCenter();

        $table->addField_Text("price", "tbl_price")
            ->addDecoratorFunction(function($v){
                return round($v+0, 2);
            })
            ->setTextAlignCenter();

        $table->addField_Text("total", "tbl_total")
            ->setTextBold()
            ->addDecoratorFunction(function($v){
                return round($v+0, 2);
            })
            ->setTextAlignCenter()
            ->setFooterAutoSum();

        /*
        $table->addField_Button("")
            ->setLink(Http_Url::convert(["edit_item_from_order" => "{id}"]))
            ->getButtonObject()
                ->addAttribPost("class", " fa fa-pencil");
        */
        $table->addField_Button("")
            ->setLink("/account/catalog/remains/?id={id}&tab=edit")
            ->getButtonObject()
                ->addAttribPost("class", " fa fa-pencil");

        $table->addField_Button()
            ->setConfirm("item_delete_confirm")
            ->setLink(Http_Url::convert(["remove_item_from_order" => "{id}"]))
            ->getButtonObject()
                ->addAttribPost("class", " fa fa-trash-o");;



        return $table->render() . $form;
    }

    public function renderFormDelivery(){
        Translate::addDictionary('order/form', 20);

        $form = new Form();

        $form->addElementSelect(
            "delivery_type",
            "form_delivery_type",
            (new Order_DeliveryType())->getArrayToSelect("...")
        )
            ->setRenderTypeBigValue()
            ->setValue($this->delivery_type);

        $form->addElementTextarea("delivery_data_comment", "form_delivery_data_comment")
            ->setAutoHeight();

        if(is_array($this->delivery_data) && count($this->delivery_data)){
            foreach($this->delivery_data as $k => $v){
                try{
                    $name = "delivery_data_" . $k;

                    if($form->getElement($name)){
                        $form->setValue($name, $v);
                    }
                }
                catch(Exception $e){
                    // если в массиве есть эдементы котоые не опрделенны в форме
                }
            }
        }

        if($form->isValidAndPost()){
            $form->addSuccessMessage();

            $this->delivery_type = $form->getValue("delivery_type");
            $this->delivery_data = [];

            foreach($form->getValues() as $k => $v){
                if(substr($k, 0, 14) == 'delivery_data_'){
                    $this->delivery_data[substr($k, 14)] = $v;
                }
            }

            $this->update(["delivery_type", "delivery_data"]);
        }

        return $form->render();
    }

    protected function renderFormContentDecorator($content){
        return $content;
    }

    public function renderForm(){
        $tabs = new Tabs_StaticTabs();

        $tabs->addTab('Client', 'tab_client');

        if($this->id){
            $tabs->addTab('Goods',    'tab_goods');
            $tabs->addTab('Delivery', 'tab_delivery');
        }

        $method = 'renderForm' . $tabs->getActiveTab();
        if(method_exists($this, $method)){
            $content = $this->$method();
        }
        else {
            $content = new View_Page_ComingSoon();
        }

        $tabs->setText($this->renderFormContentDecorator($content));

        return $tabs->render();
    }

    public function updateCachePrice(){
        if($this->id){
            $res = Db::site()->fetchRow(
                "select sum(`price` * `count`) as `price`, sum(`count`) as `count` from `catalog_remains` where `order`=?",
                $this->id
            );

            if($res['price'] != $this->price || $res['count'] != $this->count){
                $this->price = $res['price'];
                $this->count = $res['count'];
                $this->update(["price", "count"]);
            }
        }
    }

    ///////////////////////////////////////////////////

    /**
     * Получить имя клиента
     *
     * @param null $default
     *
     * @return null
     */
    public function getName($default = null){
        if(isset($this->client_data['name']) && mb_strlen($this->client_data['name'])){
            return $this->client_data['name'];
        }

        return $default;
    }

    /**
     * Получить телефон клиента
     *
     * @param null $default
     *
     * @return null
     */
    public function getPhone($default = null){
        if(isset($this->client_data['mobile_phone']) && mb_strlen($this->client_data['mobile_phone'])){
            return $this->client_data['mobile_phone'];
        }

        return $default;
    }

    /**
     * Получить профиль (ссылку на профиль) VK.com
     *
     * @param bool $htmlLink
     * @param null $default
     *
     * @return null|string
     */
    public function getVkProfile($htmlLink = false, $default = null){
        if(isset($this->client_data['vk']) && mb_strlen($this->client_data['vk'])){
            return $htmlLink ?
                "<a target='_blank' href='https://vk.com/{$this->client_data['vk']}'>{$this->client_data['vk']}</a>" :
                $this->client_data['vk'];
        }

        return $default;
    }

    /**
     * Получить email
     *
     * @param null $default
     *
     * @return null
     */
    public function getEmail($default = null){
        if(isset($this->client_data['email']) && mb_strlen($this->client_data['email'])){
            return $this->client_data['email'];
        }

        return $default;
    }
}