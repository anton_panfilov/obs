<?php

class Order_Status extends Ui_Set {
    const CREATE            = 1;  // составление заказа
    const CLIENT_WAITING    = 2;  // ждем клиента
    const VERIFICATION      = 3;  // проверка менеджером

    const ORDERING          = 100; // заказ у поставщиков

    const ASSEMBLY          = 200; // собираем товар

    const DELIVERY          = 300; // доставка

    const COMPLETED         = 400; // завершен

    const RETURNED          = 500; // возврат

    public function __construct(){
        parent::__construct('order/status');

        $this->addValue_Yellow(  self::CREATE,          'order_status_create');
        $this->addValue_Yellow(  self::CLIENT_WAITING,  'order_status_waitingClient');

        $this->addValue_Yellow(  self::ASSEMBLY,        'order_status_assembly');
        $this->addValue_White(   self::DELIVERY,        'order_status_delivery');
        $this->addValue_Green(   self::COMPLETED,       'order_status_completed');
        $this->addValue_Red(     self::RETURNED,        'order_status_returned');
    }
}