<?php

class Order_DeliveryType extends Ui_Set {
    const UNKNOWN   = 0;  // не определен
    const MAIL      = 1;  // почта
    const COURIER   = 2;  // курьер


    public function __construct(){
        parent::__construct('order/delivery_type');

        $this->addValue_Yellow( self::UNKNOWN,     'delivery_type_UNKNOWN' );
        $this->addValue_White(  self::MAIL,        'delivery_type_MAIL' );
        $this->addValue_White(  self::COURIER,     'delivery_type_COURIER' );
    }
}