<?php

class String_Highlight {
    static protected $is_init = false;
    static protected function init(){
        if(!self::$is_init){
            self::$is_init = true;
            Site::addJS("/static/plugins/highlight/highlight.pack.js");
            Site::addCSS("/static/plugins/highlight/styles/github.css");
            Site::addJSCode("hljs.initHighlightingOnLoad();");
        }
    }

    static public function render($code, $htmlspecialchars = true){
        self::init();
        return "<pre><code>" . ($htmlspecialchars ? htmlspecialchars($code) : $code) . "</code></pre>";
    }

    static public function jsonPrettyPrint($jsonStringOrArray){
        if(is_array($jsonStringOrArray)){
            return htmlspecialchars(
                json_encode($jsonStringOrArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE)
            );
        }
        else {
            $jsonStringOrArray = (string)$jsonStringOrArray;

            $result = json_encode(json_decode($jsonStringOrArray), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

            if($result === null || strtolower($result) == 'null'){
                $result = $jsonStringOrArray;
            }

            return htmlspecialchars($result);
        }
    }

    static function xmlFormat($xmlString){
        $dom = @DOMDocument::loadXML($xmlString);
        if($dom){
            $dom->formatOutput = true;
            $out = $dom->saveXML();

            $out = htmlspecialchars($out);
            $out = str_replace(array(" ", "\n"), array("&nbsp;", "<br>"), $out);
            $out = preg_replace(
                '/\&gt\;(([a-zA-Z0-9а-яё]|\ |\-|\#|\+|\&|\(|\)|\!|\@|\%|\^|\*|\-|\=|\,|\.|\?|\_|\:|\;|\/)+)\&lt\;\//ui',
                '&gt;<span style="font-weight: normal; font-size:14px;color:#800;">$1</span>&lt;/',
                $out
            );
            $out = "<div style='color:#999;'>{$out}</div>";
        }
        else {
            $xml = preg_replace("/><([a-zA-Z0-9])/", ">\r\n<$1", $xmlString);

            $patterns = array(
                "/</si",
                "/>/si",
            );

            $replacements = array(
                "&lt;",
                "&gt;",
            );

            $out = preg_replace($patterns, $replacements, $xml);
        }

        return $out;
    }

    static public function prettyPrint($string){
        if(substr($string, 0, 1) == '<'){
            return self::xmlFormat($string);
        }
        else if(substr($string, 0, 1) == "{"){
            return self::jsonPrettyPrint($string);
        }

        $output = [];
        parse_str($string, $output);
        if(count($output) > 4){
            return
                htmlspecialchars($string) .
                "<div style='color:#888; margin-top:30px; border-bottom:#DDD solid 1px; padding: 3px;'>" .
                    "Post String <i class='fa fa-long-arrow-right'></i> JSON" .
                "</div>" .
                "<div style='padding:10px'>" .
                    self::jsonPrettyPrint($output) .
                "</div>";
        }

        // Value::export($output);

        return htmlspecialchars($string);
    }
}