<?php

class String_Macros {
    /**
     * Применить простые макросы: {name} => value
     *
     * @param $string
     * @param array $params
     * @return mixed
     */
    static public function simple($string, $params = []){
        if(is_array($params) && count($params)){
            $search = [];
            $replace = [];

            foreach($params as $k => $v){
                $search[] = "{{$k}}";
                $replace[] = (string)$v;
            }

            $string = str_replace($search, $replace, $string);
        }

        return (string)$string;
    }
}