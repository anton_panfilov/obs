<?php

class String_Hash {

    const symbolsAlphaLower     = "abcdefghijklmnopqrstuvwxyz";
    const symbolsAlphaUpper     = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const symbolsAlpha          = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const symbolsAlphaNumeric   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    const symbolsNumeric        = "0123456789";
    const symbolsSpecial        = "!@#$%^&*()_-+=:;,.[]|/'\"~`";

    /**
    * Возвращает случайною строку, состоящую из сиволов из строки $symbols
    *
    * @param int $len
    * @param string $symbols
    */
    static public function randomString($len, $symbols = self::symbolsAlphaNumeric){
        $h = '';
        $slen = strlen($symbols);
        if($slen){
            for($i = 0; $i < $len; $i++){
                $h.= substr($symbols, rand(0, $slen-1), 1);
            }
        }
        return $h;
    }

    /**
     * Создание хеша Bluefish
     *
     * @param $string
     * @param int $iterations, рекомендуется 4, что бы не нагружать процессор. Или выносить расчеты на отдельный сервер
     * @return string
     */
    static public function createHash($string, $iterations = 4){
        $salt = sprintf('$2a$%02d$%s', $iterations, self::randomString(22, self::symbolsAlpha));
        return crypt($string, $salt);
    }

    /**
     * @param $string
     * @param $hash
     * @return bool
     */
    static public function isValidHash($string, $hash){
        return crypt($string, $hash) == $hash;
    }
}