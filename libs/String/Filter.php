<?php

class String_Filter {
    /**
     * Оставить в строке $string симворля только из заданного набора $symbolsString ($insensitive - требовательность к регистру)
     * Если указать $replace, то все неподходящие символы будут заменяться на эту строку
     *
     * @param $string
     * @param $symbolsString
     * @param bool $insensitive
     * @param string $replace
     * @return string
     */
    static public function only($string, $symbolsString, $insensitive = false, $replace = ''){
        $string         = (string)$string;
        $symbolsString  = (string)$symbolsString;
        $insensitive    = (bool)$insensitive;

        if(strlen($string)){
            $new = '';
            for($i = 0; $i < strlen($string); $i++){
                $symb = substr($string, $i , 1);
                if($insensitive){
                    if(strpos($symbolsString, $symb) !== false){
                        $new.= $symb;
                    }
                    else {
                        $new.= $replace;
                    }
                }
                else {
                    if(stripos($symbolsString, $symb) !== false){
                        $new.= $symb;
                    }
                    else {
                        $new.= $replace;
                    }
                }
            }
            $string = $new;
        }

        return $string;
    }

    /**
     * Обрезает строку, сохраняя целостность слов
     *
     * строка $cutAdd добавлеться в конец обрезанной строки, если строка не обрезаеться $cutAdd в конец не добавляется
     *
     * изменя переменную $toMin можно выбрать 2 варинат, длинна строки будет:
     * 1) меньше или равна заданной длинны (по умолчанию)
     * 2) больше или равна заданной длинне
     *
     * @param        $string
     * @param        $len
     * @param string $cutAdd
     * @param bool   $toMin
     *
     * @return string
     */
    static public function cutByWord($string, $len, $cutAdd = '...', $toMin = true){
        $string = (string)$string;

        if(mb_strlen($string) > $len){
            $cutAdd = (string)$cutAdd;
            $sep = [' ', '\t', '\r', '\n', '-', '.', ',', ':', '!', '?'];

            if($toMin){
                for($i = $len - 3; $i >= 0; $i--){
                    $symb = mb_substr($string, $i, 1);
                    if(in_array($symb, $sep)){
                        return rtrim(mb_substr($string, 0, $i), implode($sep)) . $cutAdd;
                    }
                }

                // если в строке не нашлось разделителей
                return mb_substr($string, 0, $len - 3) . $cutAdd;
            }
            else {
                for($i = $len; $i < mb_strlen($string); $i++){
                    $symb = mb_substr($string, $i, 1);

                    if(in_array($symb, $sep)){
                        return rtrim(mb_substr($string, 0, $i), implode($sep)) . $cutAdd;
                    }
                }
            }
        }

        return $string;
    }

    static public function price($v){
        return number_format(
            $v+0,
            (int)$v == $v ? 0 : 2,
            ".",
            " " // кророткий пробел = &thinsp;
        );
    }
}