<?php

class UserAccess {
    /***************************************************************************/

    /**
     * @return bool
     */
    static public function isAdmin(){
        return Users::getCurrent()->role == User_Role::ADMIN;
    }

    /**************************************************************************/

    /**
     * @return bool
     */
    static public function isClientManager(){
        return Users::getCurrent()->role == User_Role::CLIENT_MANAGER;
    }

    /**
     * @return bool
     */
    static public function isClient(){
        return (Users::getCurrent()->role == User_Role::CLIENT);
    }

}