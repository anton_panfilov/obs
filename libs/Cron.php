<?php

class Cron {
    static protected $id;
    static protected $id_log;

    static protected $shutdown_triggers = [];

    static public function isStart(){
        return (bool)self::$id;
    }

    static public function start($filename){
        // пометить что используеться по крону
        Boot::setModeCron();

        register_shutdown_function(['Cron', 'shutdown']);
        ob_start();

        // процессы
        Db::site()->insert('cron_processes', [
            'filename'  => $filename,
            'pid'       => getmypid()
        ]);
        self::$id = Db::site()->lastInsertId();


        // логи
        Db::site()->insert('cron_log', [
            'filename'  => $filename
        ]);
        self::$id_log = Db::site()->lastInsertId();
    }

    static public function shutdown(){
        Db::site()->delete('cron_processes', 'id=?', self::$id);

        $content = ob_get_clean();

        Db::site()->update('cron_log', [
            'is_finish'         => '1',
            'runtime'           => Boot::getRuntime(),
            'memory_mb'         => memory_get_usage() / 1048576,
            'memory_peak_mb'    => memory_get_usage() / 1048576,
            'use_db'            => Boot::getDatabaseUsage(true),
            'use_cpu'           => Boot::getCPUUsage(true),
            'content'           => $content,
            'last_error'        => Json::encode(error_get_last()),
            'db_profiler'       => '', // todo
        ], 'id=?', self::$id_log);

        if(count(self::$shutdown_triggers)){
            foreach(self::$shutdown_triggers as $func){
                $func();
            }
        }

        echo $content;
    }

    /**
     * Проверка выполняеться ли сейчас файл
     *
     * @param $filename
     * @return bool
     */
    static public function isRun($filename){
        try {
            return (bool)Db::site()->fetchOne("SELECT id FROM cron_processes WHERE filename=?", $filename);
        }
        catch(Exception $e){
            System_Warnings::add('Errors in: Cron::isRun()');
            return true;
        }
    }

    static public function addShutdownTrigger(Closure $function){
        self::$shutdown_triggers[] = $function;
    }
}