<?php

class Conf_Servers extends Conf_Db {
    /**
     * Ex:
     * [
     *     [
     *         'git' => 'https://static.t3leads.com/git-update',
     *         'ip'  => 'https://static.t3leads.com/check_ip',
     *     ],
     *     ...
     * ]
     *
     * @var array
     */
    public $update_list;
}