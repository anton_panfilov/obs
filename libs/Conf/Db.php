<?php


class Conf_Db extends Conf_Abstract {
    public $host;

    public $port;

    public $user;
    public $pass;
    public $base;

    public $engine = 'mysql';
    public $driver = 'pdo';

    const DRIVER_MYSQLI = 'mysqli';
    const DRIVER_PDO = 'pdo';

    const ENGINE_MYSQL = 'mysql';
    const ENGINE_PGSQL = 'pgsql';

    public function __construct($array = null){
        parent::__construct($array);
        if(strpos($this->host,':')!==false){
            $host = explode(':',$this->host);
            $this->host = $host[0];
            if(!isset($array['port'])){
                $this->port = $host[1];
            }
        }

        $this->engine = strtolower($this->engine);

        if(empty($this->port)){
            if($this->engine == self::ENGINE_MYSQL)$this->port = 3306;
            if($this->engine == self::ENGINE_PGSQL)$this->port = 5432;
        }
        if($this->engine == self::ENGINE_PGSQL && $this->host === 'localhost')$this->host = '127.0.0.1';
    }
}
