<?php

class Conf_Mysql extends Conf_Db {
    public $host;
    public $user;
    public $pass;
    public $base;

    public $engine = Conf_Db::ENGINE_MYSQL;
    public $driver = Conf_Db::DRIVER_MYSQLI;
}