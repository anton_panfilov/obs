<?php

class Conf_Vk extends Conf_Db {
    public $user_id;
    public $group_id;

    public $token;

    public $photo_album_to_order_group;
    public $photo_album_to_order_page;

    public $photo_album_in_stock;
}