<?php

class Conf_Main extends Conf_Abstract {

    /********************************************************************/

    public $title = 'ProjectName';

    /********************************************************************/

    /**
     * Массив возможных локализаций
     *
     * @var array
     */
    public $locales = [];

    /**
     * Локализация по умолчанию
     *
     * @var string
     */
    public $locale_default = 'ru';

    /**
     * Режим обучения переводов
     * (не рекомендуется включать на рабочем сервере, что бы не вызывать конфликты)
     *
     * @var bool
     */
    public $localeLearningMode = false;



    /********************************************************************/

    /**
     * Публичный домен с лендингом
     *
     * @var string ex: domain.com
     */
    public $domain;

    /**
     * Домен со статистикой
     *
     * @var string ex: account.domain.com
     */
    public $domain_account;

    /**
     * @var string ex: static.domain.com
     */
    public $domain_static;

    /********************************************************************/

    public $timezone;

    /********************************************************************/

    public $hostname_for_smtp;

    /********************************************************************/
}