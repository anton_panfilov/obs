<?php

abstract class Conf_Abstract {
    public function __construct($array = null){
        if(is_array($array) && count($array)){
            foreach($array as $k => $v){
                $this->$k = $v;
            }
        }
    }
}