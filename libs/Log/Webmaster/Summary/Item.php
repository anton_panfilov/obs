<?php

class Log_Webmaster_Summary_Item {
    // calls
    public $call_all                = 0;
    public $call_exit               = 0;
    public $call_data_err           = 0;
    public $call_lead               = 0;
    public $call_finish             = 0;
    public $call_sold               = 0;
    public $call_reject             = 0;

    // leads
    public $leads_all               = 0;
    public $leads_test              = 0;
    public $leads_fraud             = 0;
    public $leads_sold              = 0;
    public $leads_pending_all       = 0;
    public $leads_pending_sold      = 0;
    public $leads_pending_reject    = 0;
    public $leads_return            = 0;
    public $leads_errors            = 0;
    public $leads_fatal_errors      = 0;

    public $pending_money_wm        = 0;
    public $pending_money_pool      = 0;
    public $pending_money_ern       = 0;
    public $pending_money_ttl       = 0;

    public $leads_money_wm          = 0;
    public $leads_money_pool        = 0;
    public $leads_money_costs       = 0;
    public $leads_money_ern         = 0;
    public $leads_money_ttl         = 0;

    /**
     * Привести переменные к нормальнеому виду
     *
     * @return $this
     */
    public function normalize(){
        // calls
        $this->call_all                 = (int)$this->call_all;
        $this->call_exit                = (int)$this->call_exit;
        $this->call_data_err            = (int)$this->call_data_err;
        $this->call_lead                = (int)$this->call_lead;
        $this->call_finish              = (int)$this->call_finish;
        $this->call_sold                = (int)$this->call_sold;
        $this->call_reject              = (int)$this->call_reject;

        // leads
        $this->leads_all                = (int)$this->leads_all;
        $this->leads_test               = (int)$this->leads_test;
        $this->leads_fraud              = (int)$this->leads_fraud;
        $this->leads_sold               = (int)$this->leads_sold;
        $this->leads_pending_all        = (int)$this->leads_pending_all;
        $this->leads_pending_sold       = (int)$this->leads_pending_sold;
        $this->leads_pending_reject     = (int)$this->leads_pending_reject
        ;
        $this->leads_return             = (int)$this->leads_return;
        $this->leads_errors             = (int)$this->leads_errors;
        $this->leads_fatal_errors       = (int)$this->leads_fatal_errors;

        $this->pending_money_wm         = round((float)$this->pending_money_wm,    2);
        $this->pending_money_pool       = round((float)$this->pending_money_pool,  2);
        $this->pending_money_ern        = round((float)$this->pending_money_ern,   2);
        $this->pending_money_ttl        = round((float)$this->pending_money_ttl,   2);

        $this->leads_money_wm           = round((float)$this->leads_money_wm,    2);
        $this->leads_money_pool         = round((float)$this->leads_money_pool,  2);
        $this->leads_money_costs        = round((float)$this->leads_money_costs, 2);
        $this->leads_money_ern          = round((float)$this->leads_money_ern,   2);
        $this->leads_money_ttl          = round((float)$this->leads_money_ttl,   2);

        return $this;
    }

    /**
     * Сложить значения 2-х объектов и привести их к норлмаьному виду
     *
     * @param Log_Webmaster_Summary_Item $item
     * @return $this
     */
    public function add(Log_Webmaster_Summary_Item $item){
        // calls
        $this->call_all                 = (int)($this->call_all                 + $item->call_all);
        $this->call_exit                = (int)($this->call_exit                + $item->call_exit);
        $this->call_data_err            = (int)($this->call_data_err            + $item->call_data_err);
        $this->call_lead                = (int)($this->call_lead                + $item->call_lead);
        $this->call_finish              = (int)($this->call_finish              + $item->call_finish);
        $this->call_sold                = (int)($this->call_sold                + $item->call_sold);
        $this->call_reject              = (int)($this->call_reject              + $item->call_reject);

        // leads
        $this->leads_all                = (int)($this->leads_all                + $item->leads_all);
        $this->leads_test               = (int)($this->leads_test               + $item->leads_test);
        $this->leads_fraud              = (int)($this->leads_fraud              + $item->leads_fraud);
        $this->leads_sold               = (int)($this->leads_sold               + $item->leads_sold);
        $this->leads_pending_all        = (int)($this->leads_pending_all        + $item->leads_pending_all);
        $this->leads_pending_sold       = (int)($this->leads_pending_sold       + $item->leads_pending_sold);
        $this->leads_pending_reject     = (int)($this->leads_pending_reject     + $item->leads_pending_reject);
        $this->leads_return             = (int)($this->leads_return             + $item->leads_return);
        $this->leads_errors             = (int)($this->leads_errors             + $item->leads_errors);
        $this->leads_fatal_errors       = (int)($this->leads_fatal_errors       + $item->leads_fatal_errors);

        $this->pending_money_wm         = round($this->pending_money_wm         + $item->pending_money_wm,    2);
        $this->pending_money_pool       = round($this->pending_money_pool       + $item->pending_money_pool,  2);
        $this->pending_money_ern        = round($this->pending_money_ern        + $item->pending_money_ern,   2);
        $this->pending_money_ttl        = round($this->pending_money_ttl        + $item->pending_money_ttl,   2);

        $this->leads_money_wm           = round($this->leads_money_wm           + $item->leads_money_wm,    2);
        $this->leads_money_pool         = round($this->leads_money_pool         + $item->leads_money_pool,  2);
        $this->leads_money_costs        = round($this->leads_money_costs        + $item->leads_money_costs, 2);
        $this->leads_money_ern          = round($this->leads_money_ern          + $item->leads_money_ern,   2);
        $this->leads_money_ttl          = round($this->leads_money_ttl          + $item->leads_money_ttl,   2);

        return $this;
    }

    public function getArray(){
        $r = [];
        foreach(get_object_vars($this) as $k => $v){
            $r[$k] = $v;
        }
        return $r;
    }
}