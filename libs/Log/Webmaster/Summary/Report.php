<?php

class Log_Webmaster_Summary_Report {

    /*************************************************/

    static protected $forDestruct;
    static protected $autoCommit    = true;

    /**
     * @var Log_Webmaster_Summary_Item[]
     */
    static protected $logs          = [];

    /**
     * @var Lead_Object[]
     */
    static protected $leads         = [];

    /*************************************************/

    public function __destruct(){
        if(count(self::$logs)){
            System_Warnings::add("Unsaved data at the end of the script");
            self::commit();
        }
    }

    static public function setAutoCommit_ON(){
        self::$autoCommit = true;
    }

    /**
     * Если выкобчить автокимит, то он будет сделан в деструкторе, но это неверный подход, надо вызывать его самому до завершения скрипта
     */
    static public function setAutoCommit_OFF(){
        self::$autoCommit = false;

        // при отключении автокомита, commit вызывиться в деструкторе при завершении стркипта
        if(!self::$forDestruct) self::$forDestruct = new self();
    }

    /*************************************************/

    static protected $fakeLeadsIDS = 0;

    static public function newCall(Lead_Call_Object $call){
        self::$fakeLeadsIDS--;

        $leadCall               = new Lead_Object();
        $leadCall->id           = self::$fakeLeadsIDS;
        $leadCall->create       = $call->create;
        $leadCall->channel_type = Lead_ChannelType::CALL;
        $leadCall->product      = $call->product;
        $leadCall->webmaster    = $call->webmaster;
        $leadCall->agent        = $call->agent;
        $leadCall->region       = 0;
        $leadCall->channel      = $call->dialed_phone;
        $leadCall->form_domain  = 0;

        $item = new Log_Webmaster_Summary_Item();
        $item->call_all = 1;

        self::add($leadCall, $item);
    }

    static public function callExit(Lead_Call_Object $call){
        self::$fakeLeadsIDS--;

        $leadCall               = new Lead_Object();
        $leadCall->id           = self::$fakeLeadsIDS;
        $leadCall->create       = $call->create;
        $leadCall->channel_type = Lead_ChannelType::CALL;
        $leadCall->product      = $call->product;
        $leadCall->webmaster    = $call->webmaster;
        $leadCall->agent        = $call->agent;
        $leadCall->region       = 0;
        $leadCall->channel      = $call->dialed_phone;
        $leadCall->form_domain  = 0;

        $item = new Log_Webmaster_Summary_Item();
        $item->call_exit = 1;

        self::add($leadCall, $item);
    }

    static public function callDataError(Lead_Call_Object $call){
        self::$fakeLeadsIDS--;

        $leadCall               = new Lead_Object();
        $leadCall->id           = self::$fakeLeadsIDS;
        $leadCall->create       = $call->create;
        $leadCall->channel_type = Lead_ChannelType::CALL;
        $leadCall->product      = $call->product;
        $leadCall->webmaster    = $call->webmaster;
        $leadCall->agent        = $call->agent;
        $leadCall->region       = 0;
        $leadCall->channel      = $call->dialed_phone;
        $leadCall->form_domain  = 0;

        $item = new Log_Webmaster_Summary_Item();
        $item->call_data_err = 1;

        self::add($leadCall, $item);
    }

    static public function callLead(Lead_Call_Object $call, Lead_Object $lead){
        self::$fakeLeadsIDS--;

        $leadCall               = new Lead_Object();
        $leadCall->id           = self::$fakeLeadsIDS;
        $leadCall->create       = $call->create;
        $leadCall->channel_type = Lead_ChannelType::CALL;
        $leadCall->product      = $lead->product;
        $leadCall->webmaster    = $lead->webmaster;
        $leadCall->agent        = $lead->agent;
        $leadCall->region       = 0;
        $leadCall->channel      = $lead->channel;
        $leadCall->form_domain  = 0;

        // вычитание добавленных раныее данных без региона (со звонка)
        $item = new Log_Webmaster_Summary_Item();
        $item->call_all = -1;

        self::add($leadCall, $item);

        $leadClone = clone $lead;
        $leadClone->create       = $call->create;

        // добавление данных с регионом (с созданного лида лида)
        $item = new Log_Webmaster_Summary_Item();

        $item->call_all = 1;
        $item->call_lead = 1;

        self::add($leadClone, $item);
    }

    static public function callFinish(Lead_Call_Object $call, Lead_Object $lead){
        $leadClone = clone $lead;
        $leadClone->create       = $call->create;

        // добавление данных с регионом (с созданного лида лида)
        $item = new Log_Webmaster_Summary_Item();

        $item->call_finish = 1;

        if($lead->money_ttl > 0){
            $item->call_sold = 1;
        }
        else {
            $item->call_reject = 1;
        }

        self::add($leadClone, $item);
    }

    /***********************************************************/

    static public function newLead(Lead_Object $lead){
        $item = new Log_Webmaster_Summary_Item();

        $item->leads_all = 1;

        if($lead->is_test)  $item->leads_test  = 1;
        if($lead->is_fraud) $item->leads_fraud = 1;

        self::add($lead, $item);
    }

    static public function sold(Lead_Object $lead, $delta_solds_count, $wm, $pool, $ern, $ttl){
        $item = new Log_Webmaster_Summary_Item();
        $item->leads_sold           = $delta_solds_count;
        $item->leads_money_wm       = $wm;
        $item->leads_money_pool     = $pool;
        $item->leads_money_ern      = $ern;
        $item->leads_money_ttl      = $ttl;

        self::add($lead, $item);
    }

    static public function pending(Lead_Object $lead){
        $item = new Log_Webmaster_Summary_Item();
        $item->leads_pending_all = 1;

        self::add($lead, $item);
    }

    static public function pending_add(
        Lead_Object $lead, $wm, $pool, $ern, $ttl, $delta_solds_count, $delta_rejects_count, $delta_lead_solds_count = 0
    ){
        $item = new Log_Webmaster_Summary_Item();
        $item->leads_pending_reject = $delta_rejects_count;
        $item->leads_pending_sold   = $delta_solds_count;

        $item->leads_sold = $delta_lead_solds_count;

        $item->pending_money_wm     = $wm;
        $item->pending_money_pool   = $pool;
        $item->pending_money_ern    = $ern;
        $item->pending_money_ttl    = $ttl;

        $item->leads_money_wm       = $wm;
        $item->leads_money_pool     = $pool;
        $item->leads_money_ern      = $ern;
        $item->leads_money_ttl      = $ttl;

        self::add($lead, $item);
    }

    static public function returnLead(Lead_Object $lead, $wm, $pool, $ern, $ttl){
        $item = new Log_Webmaster_Summary_Item();
        $item->leads_return         = 1;
        $item->leads_money_wm       = $wm;
        $item->leads_money_pool     = $pool;
        $item->leads_money_ern      = $ern;
        $item->leads_money_ttl      = $ttl;

        self::add($lead, $item);
    }

    static public function fatalError($webmaster, $datetime, $channelType = 0){
        $item = new Log_Webmaster_Summary_Item();
        $item->leads_fatal_errors = 1;

        $lead = new Lead_Object();
        $lead->create       = $datetime;
        $lead->webmaster    = (int)$webmaster;
        $lead->channel_type = (int)$channelType;

        self::add($lead, $item);
    }

    static public function error($webmaster, $datetime, $channelType = 0){
        $item = new Log_Webmaster_Summary_Item();
        $item->leads_errors = 1;

        $lead = new Lead_Object();
        $lead->create       = $datetime;
        $lead->webmaster    = (int)$webmaster;
        $lead->channel_type = (int)$channelType;

        self::add($lead, $item);
    }

    /*************************************************/

    static public function costs(Lead_Object $lead, $costs){
        $item = new Log_Webmaster_Summary_Item();

        $item->leads_money_ern = -$costs;
        $item->leads_money_costs = $costs;

        self::add($lead, $item);
    }

    /*************************************************/

    /**
     * @param Lead_Object $lead
     * @param Log_Webmaster_Summary_Item $item
     */
    static protected function add(Lead_Object $lead, Log_Webmaster_Summary_Item $item){
        if(isset(self::$logs[$lead->id])){
            self::$logs[$lead->id] = $item->add(
                self::$logs[$lead->id]
            );
        }
        else {
            self::$leads[$lead->id] = $lead;
            self::$logs[$lead->id] = $item->normalize();
        }

        if(self::$autoCommit) self::commit();
    }

    /**
     * @throws Exception если проблемма с базой или невозможная ошибка если не найден лид
     */
    static public function commit(){
        if(count(self::$logs)){
            $insert = [];
            foreach(self::$logs as $leadID => $params){
                if(is_null(self::$leads[$leadID]->region)){
                    $region = (
                        self::$leads[$leadID]->id &&
                        self::$leads[$leadID]->getBody() instanceof Lead_Body_Abstract
                    ) ?
                        (int)self::$leads[$leadID]->getBody()->_get_region() :
                        0;
                }
                else {
                    $region = (int)self::$leads[$leadID]->region;
                }

                $insert[] = [
                    'lead'      => $leadID,
                    'keys'      => json_encode([
                        'lead'          => self::$leads[$leadID]->id,
                        'lead_create'   => self::$leads[$leadID]->create,
                        'product'       => self::$leads[$leadID]->product,
                        'webmaster'     => self::$leads[$leadID]->webmaster,
                        'agent'         => self::$leads[$leadID]->agent,
                        'channel_type'  => self::$leads[$leadID]->channel_type,
                        'channel'       => self::$leads[$leadID]->channel,
                        'region'        => $region,
                        'domain'        => self::$leads[$leadID]->form_domain,
                    ]),
                    'params'    => json_encode($params->getArray()),
                ];
            }
            Db::logs()->insertMulti("webmaster_summary_log", $insert);
        }

        self::$logs = [];
    }
}