<?php

abstract class Log_Abstract {

    /*************************************************/

    static protected $forDestruct;
    static protected $autoCommit = true;
    static protected $logs  = [];

    /*************************************************/

    public function __destruct(){
        if(count(self::$logs)){
            System_Warnings::add("Unsaved data at the end of the script");
            static::commit();
        }
    }

    static public function setAutoCommit_ON(){
        self::$autoCommit = true;
    }

    /**
     * Если выкобчить автокимит, то он будет сделан в деструкторе, но это неверный подход, надо вызывать его самому до завершения скрипта
     */
    static public function setAutoCommit_OFF(){
        self::$autoCommit = false;

        // при отключении автокомита, commit вызывиться в деструкторе при завершении стркипта
        $class = get_called_class();
        if(!self::$forDestruct) self::$forDestruct = new $class();
    }

    /*************************************************/

    static public function commit(){

    }
}