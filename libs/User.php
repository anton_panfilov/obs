<?php

class User extends Db_Model {
    static protected $_table = 'users';
    static protected $_structure = [
        'name',

        // не обновляються при update, если явно не указанны
        ['login'],
        ['email'],
        ['phone'],
        ['company_id'],
        ['lang'],
        ['status'],
        ['role'],
        ['create_date'],

        ['login_key'],
        ['secure_key'],
        ['password_hash'],
        ['password_temp'],
        ['contacts'],
    ];

    protected function getDatabase(){
        return Db::site();
    }

    /*********************************/

    public $id;
    public $create_date;
    public $status = User_Status::ACTIVE;
    public $role = 0; // по умолчанию не определенно, что бы исключить логические ошибки неопределения роли при создании пользователя
    public $login;
    public $email;
    public $phone;
    public $company_id;
    public $name;
    public $lang;

    /**
     * Строка для защиты сессии логирования, меянеться при подборе пароля
     *
     * @var string
     */
    protected $login_key;

    /**
     * Рандомная стрка, присваиваеться при регистрации и никогда не меняеться
     *
     * @var string
     */
    protected $secure_key;


    protected $password_hash;

    /**
     * Временный пароль или нет
     *
     * @var int
     */
    protected $password_temp = 0;

    protected $contacts = [];

    /*************************************************************************/

    /**
     * Иницилизироать массив данных в объект
     *
     * @param array $array
     */
    protected function baseToObject($array){
        parent::baseToObject($array);

        $this->contacts = Json::decode($this->contacts);

        if(!is_array($this->contacts)){
            $this->contacts = [];
        }
    }

    /**
     * Формирует из объекта массив для базы данных
     *
     * @param array $only
     * @param bool $update
     * @return array
     */
    protected function objectToBase($only = [], $update = false){
        $result = parent::objectToBase();

        if(isset($result['contacts'])){
            $result['contacts'] = Json::encode(
                is_array($result['contacts']) ? $result['contacts'] : []
            );
        }

        return $result;
    }

    /*************************************************************************/

    public function __construct(){
        // при создании нового объекта пользователя, локализация задаеться по умолчанию
        // не ставиться текущая локализация, потому что объект может создавать не обязательно этот же пользователь
        $this->lang = Conf::main()->locale_default;
    }

    /*********************************/

    public function createNew(
        $login,
        $password,
        $role,
        $email,
        $name = null,
        $company = null,
        $status = User_Status::ACTIVE,
        $phone = null,
        $lang = null,
        $tempPassword = false
    ){
        $this->login            = $login;
        $this->role             = (int)$role;
        $this->email            = $email;
        $this->phone            = (new Form_Filter_Phone())->filter($phone);
        $this->name             = $name;
        $this->company_id       = $company;

        $this->login_key        = String_Hash::randomString(16);
        $this->secure_key       = String_Hash::randomString(16);
        $this->password_hash    = '';

        if(strlen($lang) == 2 && isset(Conf::main()->locales[$lang])){
            $this->lang = $lang;
        }

        $this->status           = $status;

        $this->insert();

        $this->setPassword($password, true, $tempPassword);

        User_Log::changeStatus($this->id, $this->status, "Assigned when creating");

        static::setObject($this);
    }

    /**
     * @param $password
     * @param bool $save
     * @param bool $temp Временный пароль? (если пароль временный, пользователю при входе сразу предложит поменять его)
     * @return $this
     */
    public function setPassword($password, $save = true, $temp = false){
        $this->password_hash = String_Hash::createHash($password . $this->id . $this->secure_key . Conf::security()->user_pass);
        $this->password_temp = (int)(bool)$temp;

        if($save){
            $this->update(['password_hash', 'password_temp']);
        }

        return $this;
    }

    /**
     * @param $password
     * @return bool
     */
    public function isValidPassword($password){
        return String_Hash::isValidHash(
            $password . $this->id . $this->secure_key . Conf::security()->user_pass,
            $this->password_hash
        );
    }


    /**
     * Временный пароль?
     *
     * @return bool
     */
    public function isPasswordTemp(){
        return (bool)$this->password_temp;
    }

    /**
     * Разрешено ли этому пользователю пользоваться аккаунтом
     *
     * @return bool
     */
    public function isActive(){
        return ($this->status == User_Status::ACTIVE);
    }

    public function isVerify(){
        return ($this->status == User_Status::VERIFY);
    }

    /**
     * Получить 4 хеша,
     *  - 2 первых построенны на статичных данных пользователя и меняються только при полууспешных подобрах пароля
     *  - 2 последних построенны на тех же данных + IP + UserAgent
     *
     * @return array
     */
    public function getLoginHashes(){
        $h1 = "{$this->id}{$this->secure_key}{$this->login_key}" . "v2";
        $h2 = "{$h1}{$this->password_hash}" .
            (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "")
            /*. (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "")*/;

        return [
            crc32("{$h1}-h1_1"),
            crc32("{$h1}-h2"),
            crc32("{$h2}-h3_1"),
            crc32("{$h2}-h4"),

        ];
    }

    /**
     * Получить ссылку на аватар заданного размера
     *
     * @param int $size
     * @return string
     */
    public function getAvatarLink($size = 100){
        $size = max(16, (int)$size);
        if(isset($_SERVER['HTTPS']) && strlen($_SERVER['HTTPS'])){
            return "https://www.gravatar.com/avatar/" . md5(mb_strtolower(trim($this->email))) . "?s={$size}&d=mm";
        }
        return "https://www.gravatar.com/avatar/" . md5(mb_strtolower(trim($this->email))) . "?s={$size}&d=mm";
    }

    /**
     * Получить поолное имя имю и фамилию
     *
     * @return string
     */
    public function getFullName(){
        return $this->name;
    }

    public function verificationComplete($isAuto = true, $comment = ''){
        if($this->id && $this->status == User_Status::VERIFY){
            $this->status = User_Status::ACTIVE;
            $this->update('status');

            User_Log::changeStatus($this->id, $this->status, $comment);

            Db::site()->insert("users_verification_log", [
                "user"      => $this->id,
                "creator"   => Users::getCurrent()->id,
                "ip"        => inet_pton($_SERVER['REMOTE_ADDR']),
                "type"      => $isAuto ? "auto" : "manual",
                "comment"   => (string)$comment,
            ]);
        }
    }

    /**
     * Получить хеш с секретным кодом в соли
     *
     * @param $string
     * @return string
     */
    public function getSecureKeyHash($string){
        return md5($this->secure_key . $string);
    }

    /***************************************************/

    public function renderContactsForm(){
        Translate::addDictionary('user/contacts');

        $form = new Form();
        $form->addButton('save');

        $form->addElementStatic("userContacts_name",  $this->getFullName());
        $form->addElementStatic("userContacts_email", $this->email);

        //////////////////////////////////////////////////
        $form->addSeparator("userContacts_contacts");

        $form->addElementText("mobilePhone", "userContacts_mobilePhone")
            ->addValidator(new Form_Validator_Length(0, 255))
            ->setRequired(false);

        $form->addElementText("vk", "userContacts_vk")
            ->addValidator(new Form_Validator_Length(0, 255))
            ->setRenderTypeBigValue()
            ->setRequired(false);

        $form->addElementTextarea("other", "userContacts_other")
            ->setRenderTypeBigValue()
            ->setAutoHeight()
            ->setRequired(false);

        ////////////////////////////////

        $form->addSeparator('userContacts_postAddress');

        $form->addElementText('index', 'userContacts_index')
            ->setMask('num')
            ->setMaxLength(6)
            ->setWidth("100px")
            ->setRequired(false);

        $form->addElement_Region('region', 'userContacts_region')
            ->setRequired(false);

        $form->addElementText('city', 'userContacts_city')
            ->setComment('userContacts_city_comment')
            ->setRenderTypeBigValue()
            ->setRequired(false);

        $form->addElementTextarea('address', 'userContacts_address')
            ->setRenderTypeBigValue()
            ->setAutoHeight()
            ->setRequired(false);

        $form->setValues($this->contacts);

        if($form->isValidAndPost()){
            $form->addSuccessMessage();
            $this->setContacts($form->getValues());
        }

        return $form->render();
    }

    /**
     * @param $array
     *
     * @return $this
     */
    public function setContacts($array){
        if(!is_array($this->contacts)){
            $this->contacts = [];
        }

        if(is_array($array) && count($array)){
            foreach($array as $k => $v){
                $this->contacts[$k] = $v;
            }
        }

        Db::site()->insert("users_contacts_log", [
            "user"     => $this->id,
            "contacts" => Json::encode($this->contacts),
            "ip"       => inet_pton($_SERVER['REMOTE_ADDR']),
            "changer"  => Users::getCurrent()->id,
        ]);

        $this->update('contacts');

        return $this;
    }

    /***************************************************/

    /**
     * Тригер вызываеться при блокировке пользователя
     */
    public function lockTrigger(){
        /*
        if($this->role == User_Role::AGN_BUYER){
            User_BuyerAgents::lockBuyerAgentTrigger($this->id);
        }
        else if($this->role == User_Role::AGN_WM){
            User_WebmasterAgents::lockWebmasterAgentTrigger($this->id);
        }
        */
    }
}