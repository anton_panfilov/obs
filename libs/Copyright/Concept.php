<?php

class Copyright_Concept {
    const COOKIE = 'Copyright_Concept';

    static public function div($text, $autor = "unknown"){
        if(
            UserAccess::isAdmin() ||
            isset($_COOKIE['Copyright_Concept'])
        ) {
            return "
                <div class='todo_cpr_text'>" . $text . "<br>
                    <div style='text-align:right'><span style='color:#999; font-size:10px'>author:</span> {$autor}</div>
                </div>
            ";
        }

        return "";
    }
}