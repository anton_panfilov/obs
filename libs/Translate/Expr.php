<?php

/**
 * Class Translate_Expr
 *
 * Класс - строка, который не переводиться
 * Для случаев когда на перевод может поступать уже переведенный текст.
 *
 * пример:  echo Translate::t(new Translate_Expr("<p class='new'>" . Translate::t("Hello World") . "</p>"));
 *
 * Будет переводиться только Translate::t("Hello World")
 *
 */
class Translate_Expr {
    protected $value;

    public function __construct($value){
        $this->value = $value;
    }

    public function __toString(){
        return (string)$this->value;
    }
}