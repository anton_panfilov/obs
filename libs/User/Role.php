<?php

class User_Role extends Ui_Set {
    const ADMIN             = 1;
    const CLIENT_MANAGER    = 10;
    const CLIENT            = 20;

    public function __construct(){
        parent::__construct('user/role');

        $this->addValue_White(  self::ADMIN,            'user_role_admin');
        $this->addValue_White(  self::CLIENT_MANAGER,   'user_role_client_manager');
        $this->addValue_White(  self::CLIENT,           'user_role_client');
    }
}