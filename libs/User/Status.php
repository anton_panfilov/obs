<?php

class User_Status extends Ui_Set {
    const ACTIVE = 1;
    const VERIFY = 2;
    const LOCK   = 3;

    public function __construct(){
        parent::__construct('user/status');

        $this->addValue_Green(  self::ACTIVE,   'user_status_active'    );
        $this->addValue_Yellow( self::VERIFY,   'user_status_verify'    );
        $this->addValue_Red(    self::LOCK,     'user_status_lock'      );
    }
}