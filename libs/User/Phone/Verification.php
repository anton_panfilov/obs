<?php

class User_Phone_Verification {
    static protected $callLanguages = [
        'en-US' => 'English',
        'da-DK' => 'Dansk - Danish',
        'de-DE' => 'Deutsch - German',
        'es-ES' => 'Español - Spanish',
        'fr-FR' => 'Français - French',
        'it-IT' => 'Italiano - Italian',
        'nb-NO' => 'Norsk - Norwegian',
        'nl-NL' => 'Nederlands - Dutch',
        'pl-PL' => 'Polski - Polish',
        'pt-PT' => 'Português - Portuguese',
        'sv-SE' => 'Svenska - Swedish',
        'fi-FI' => 'Suomi - Finnish',
        'ru-RU' => 'Русский - Russian',
        'zh-CN' => '中國 - Chinese',
        'ja-JP' => '日本人 - Japanese',
        'ko-KR' => '한국의 - Korean',
    ];

    static public function getCallLanguages(){
        return self::$callLanguages;
    }

    static public function randCode(){
        return rand(10000, 99999);
    }

    /**
     * @param $phone
     * @param null $code
     * @return int|string
     * @throws Exception
     */
    static public function createVerification_SMS($phone, $code = null){
        if(count($temp = explode(".", $phone)) !== 2)      throw new Exception('Invalid Phone');

        list($phoneCountryCode, $phoneNumber) = $temp;
        $phoneCountryCode = (int)$phoneCountryCode;
        $phoneNumber = (int)$phoneNumber;

        if(!is_numeric($phoneCountryCode))  throw new Exception('Invalid Phone Country Code');
        if(!is_numeric($phoneNumber))       throw new Exception('Invalid Phone Number');


        if(strlen($code) == 5 && is_numeric($code)){
            $verificationCode = $code;
        }
        else {
            $verificationCode = self::randCode();
        }

        $request = [
            'account_sid' => Conf::twilio()->sid,
            'auth_token'  => Conf::twilio()->token,
            'data' => [
                'To'            => "+{$phoneCountryCode}{$phoneNumber}",
                'From'          => Conf::twilio()->from_number,
                'Body'          => "Activation code: {$verificationCode}",
            ]
        ];

        $response = (new Services_Twilio($request['account_sid'], $request['auth_token']))
            ->account->messages->create($request['data']);

        Db::site()->insert("users_phones_confirmations", [
            'user'      => Users::getCurrent()->id,
            'ip'        => inet_pton($_SERVER['REMOTE_ADDR']),
            'phone'     => $phone,
            'type'      => 'sms',
            'code'      => $verificationCode,
            'request'   => Json::encode($request),
            'response'  => Json::encode($response),
        ]);

        return $verificationCode;
    }

    /**
     * @param $phone
     * @param string $voiceLanguage
     * @param null $code
     * @return int|string
     * @throws Exception
     */
    static public function createVerification_Call($phone, $voiceLanguage = 'en-US', $code = null){
        if(count($temp = explode(".", $phone)) !== 2)      throw new Exception('Invalid Phone');

        list($phoneCountryCode, $phoneNumber) = $temp;
        $phoneCountryCode = (int)$phoneCountryCode;
        $phoneNumber = (int)$phoneNumber;

        if(!is_numeric($phoneCountryCode))  throw new Exception('Invalid Phone Country Code');
        if(!is_numeric($phoneNumber))       throw new Exception('Invalid Phone Number');


        if(strlen($code) == 5 && is_numeric($code)){
            $verificationCode = $code;
        }
        else {
            $verificationCode = self::randCode();
        }

        $request = [
            'account_sid' => Conf::twilio()->sid,
            'auth_token'  => Conf::twilio()->token,
            'To'          => "+{$phoneCountryCode}{$phoneNumber}",
            'From'        => Conf::twilio()->from_number,
            'Code'        => "{$verificationCode}",
            'Lang'        => "$voiceLanguage",
        ];

        $response = (new Services_Twilio($request['account_sid'], $request['auth_token']))
            ->account->calls->create(
                $request['From'],
                $request['To'],
                // "https://account.dekamarketinggroup.com/api/twilio/phone_verification?" .
                "https://" . Conf::main()->domain_account . "/api/twilio/phone_verification?" .
                "lang=" . urlencode($request['Lang']) .
                "&code=" . urlencode($request['Code']) .
                "&name=" . urlencode(Users::getCurrent()->first_name) ,
                [
                    'Method'                => 'POST',
                    'Record'                => 'false',
                ]
            );

        Db::site()->insert("users_phones_confirmations", [
            'user'          => Users::getCurrent()->id,
            'ip'            => inet_pton($_SERVER['REMOTE_ADDR']),
            'phone'         => $phone,
            'type'          => 'call',
            'code'          => $verificationCode,
            'voiceLanguage' => $voiceLanguage,
            'request'       => Json::encode($request),
            'response'      => Json::encode($response),
        ]);

        return $verificationCode;
    }
}