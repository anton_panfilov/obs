<?php

class User_Log {
    static public function changeStatus($user, $status, $reason = null){
        Db::site()->insert("users_change_status_log", [
            'user'      => (int)$user,
            'status'    => (int)$status,
            'admin'     => Users::getCurrent()->id,
            'reason'    => trim($reason),
            'ip'        => inet_pton($_SERVER['REMOTE_ADDR']),
        ]);
    }
}