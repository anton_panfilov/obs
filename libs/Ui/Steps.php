<?php

class Ui_Steps {
    protected $steps = [];
    protected $currentStep = 1;

    /**
     * @param $name
     * @return $this
     * @throws Exception
     */
    public function addStep($name){
        if(count($this->steps) < 6){
            $this->steps[count($this->steps) + 1] = Translate::t($name);
        }
        else {
            throw new Exception("Maximum 6 steps");
        }

        return $this;
    }

    /**
     * @param $stepNumber
     * @return $this
     * @throws Exception
     */
    public function setCurrentStep($stepNumber){
        $stepNumber = max(1, (int)$stepNumber);

        if(isset($this->steps[$stepNumber])){
            $this->currentStep = $stepNumber;
        }
        else {
            throw new Exception("Invalid step number, now total " . count($this->steps) . " steps");
        }

        return $this;
    }

    public function render(){
        $return = "";
        if(count($this->steps)){
            $return.= "<ul class='bootstrapWizard'>";
            $widht = floor(100 / count($this->steps));
            $step = 1;
            foreach($this->steps as $name){
                $stepUI = $step;
                if($this->currentStep == $step){
                    $class = 'active';
                }
                else if($this->currentStep > $step){
                    $class = 'complete';
                    $stepUI = "<i class='fa fa-check'></i>";
                }
                else {
                    $class = '';
                }

                $return.= "<li class='{$class}' style='width:{$widht}%'><a><span class='step'>{$stepUI}</span> <span class='title'>{$name}</span></a></li>";

                $step++;
            }
            $return.= "</ul>";
        }
        return $return;


        return $return;
    }
}