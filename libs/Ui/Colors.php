<?php
/**
* Класс для подбора цвета для произволной строки
*/


class Ui_Colors {
    // Стандатные цвета заливки
    const BACKGROUND_WHITE  = 'FFF';
    const BACKGROUND_GREEN  = 'D3F4B3';
    const BACKGROUND_YELLOW = 'FBF888';
    const BACKGROUND_RED    = 'fdb0ae';


    static protected $textColors = [
        '708090', '4169E1', '1E90FF', '4682B4', '5F9EA0', '2E8B57', '3CB371',
        '20B2AA', '32CD32', '9ACD32', '228B22', '6B8E23', 'FFD700', 'DAA520',
        'CD853F', 'FA8072', 'FF8C00', 'DB7093', 'DDA0DD', '9370DB', '9932CC',
        '8B8682', '8B7D6B', 'CD9B9B', 'CD661D', '8968CD', 'EE3A8C', 'CD1076',
    ];

    static protected $backgrounds = [
        'FFCCFF', 'FFCCCC', 'FFFFCC', 'FF99CC', 'CCFFCC', 'CCFFFF', 'CCCCFF',
        'CCCCCC', '99FFFF', '99FFCC', '99CCFF', 'FFFF99', 'FFCC99', 'CCFF99',
        'CCCC99', 'FFFF66', 'CCFF33', 'FF6666', 'D0D0D0', '00CCCC', '00CCFF',
    ];

    /**************************************************************************************/

    /**
    * Получить случайный элемент из массива, для этой строки.
    * при повтором получении эта строка всегда будет получать этот же элемент массива
    *
    * TODO: Вынести в отдельную библиотеку по работе с массивами
    *
    * @param mixed $string
    * @param mixed $array
    */
    static protected function get($string, array $array){
        return $array[(floor(sprintf("%u", crc32($string . md5('r1')))/6426) + floor(sprintf("%u", crc32($string . md5('r2')))/7123)) % count($array)];
    }

    /**************************************************************************************/

    /**
    * Подбрать цвет текста для данной строки
    *
    * @param mixed $string
    * @return string
    */
    static public function getTextColor($string){
        return self::get($string, self::$textColors);
    }

    /**
    * Подбрать цвет заливки для данной строки
    *
    * @param mixed $string
    * @return string
    */
    static public function getBackground($string){
        return self::get($string, self::$backgrounds);
    }
}