<?php

class Ui_Bool extends Ui_Set {
    public function __construct(){
        parent::__construct('ui/bool');

        $this->addValue_White(0, "ui_bool_no");
        $this->addValue_Green(1, "ui_bool_yes");
    }
}