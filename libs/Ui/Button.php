<?php

class Ui_Button extends Ui_Abstract {
    protected $img;

    /**
     * Название кнопки
     *
     * @var string
     */
    protected $label;

    /**
     * Ссылка для редиректа, при нажатии на кнопку
     *
     * @var string
     */
    protected $link;

    /**
     * Текст, подтверждения нажатия на кнопку
     *
     * @var string
     */
    protected $confirm;


    /**
     * Скрипт, вызываемый при нажатии на кнопочку
     *
     * @var string
     */
    protected $script;

    /**
     * @var Html_Attribs
     */
    protected $attr;


    /**
     * @param string $label
     * @param null $id
     */
    public function __construct($label = '', $id = null){
        $this->attr = new Html_Attribs();
        $this->attr->setAttrib('type', 'button');
        $this->attr->setAttrib('class', 'btn btn-default');
		$this->attr->setAttrib('value', ''); $label = Translate::t($label);
        $this->setLabel($label);
        $this->setID($id);
    }

    /******************************************************************************************/

    /**
     * Установить название кнопки
     *
     * @param mixed $label название кнопки
     * @return self
     */
    public function setLabel($label){
        $this->label = Translate::t($label);
        return $this;
    }

    /**
     * Установить значение отправляемое на сервер
     *
     * @param mixed $label название кнопки
     * @return self
     */
    public function setValue($label){
        $this->attr->setAttrib('value', $label);
        return $this;
    }

    /**
     * Установить ID этой кнопки
     *
     * @param mixed $id
     * @return self
     */
    protected function setID($id){
        $this->attr->setAttrib('id', $id);
        return $this;
    }

    /**
     * Установить html атрибут у кнопки
     *
     * @param mixed $name
     * @param mixed $value
     * @return self
     */
    public function setAttrib($name, $value = null){
        $this->attr->setAttrib($name, $value);
        return $this;
    }

    /**
     * @param      $name
     * @param null $value
     *
     * @return $this
     */
    public function addAttribPost($name, $value = null){
        $this->attr->addAttribPost($name, $value);
        return $this;
    }

    /**
     * @param      $name
     * @param null $value
     *
     * @return $this
     */
    public function addAttribPre($name, $value = null){
        $this->attr->addAttribPre($name, $value);
        return $this;
    }

    /**
     * Устанвоить CSS для кнопки
     *
     * @param mixed $name
     * @param mixed $value
     * @return self
     */
    public function setCSS($name, $value = null){
        $this->attr->setCSS($name, $value);
        return $this;
    }

    /******************************************************************************************/

    /**
     * Установить url, на который будет перенаправлени клиент при нажатии на кнопку
     *
     * @param mixed $link
     * @return self
     */
    public function setLink($link){
        $this->link = $link;
        return $this;
    }

    /**
     * Установить JavaScript, который выполниться при нажатии на кнопку
     *
     * @param $javaScript
     * @return self
     */
    public function setScript($javaScript){
        $this->script = $javaScript;
        return $this;
    }

    /**
     * Устанвоить текст вопроса, который будет подтверждать нажати не кнопку перед выполением скриптиа и(или) редиректа
     * Например: Вы действительно хотите ужалить этого пользователя?
     *
     * @param mixed $message
     * @return self
     */
    public function setConfirm($message){
        $this->confirm = Translate::t($message);
        return $this;
    }

    /******************************************************************************************/

    /**
     * Стиль: красная кнопка
     *
     * @return self
     */
    public function setStylePrimary(){
        $this->attr->setAttrib('class', 'btn btn-primary');
        return $this;
    }

    /**
     * Стиль: белая кнопка
     *
     * @return self
     */
    public function setStyleDefault(){
        $this->attr->setAttrib('class', 'btn btn-default');
        return $this;
    }

    /**
     * Type: button
     *
     * @return self
     */
    public function setTypeButton(){
        $this->attr->setAttrib('type', 'button');
        return $this;
    }

    /**
     * Type: submit
     *
     * @return self
     */
    public function setTypeSubmit(){
        $this->attr->setAttrib('type', 'submit');
        return $this;
    }

    /******************************************************************************************/

    /**
     * Получить HTML код кнопки
     *
     * @return string
     */
    public function render(){

        $attribs = clone $this->attr;

        // подтверждение
        if(strlen($this->confirm)){
            $attribs->setAttrib('onclick', "if(!confirm('" . addcslashes($this->confirm, "'") . "')){return false;} ");
        }

        // выполнить скрипт
        if(strlen($this->script)){
            $attribs->addAttribPost('onclick', $this->script . ";");
        }

        // перейти по ссылке
        if(strlen($this->link)){
            $attribs->addAttribPost('onclick', "document.location='" . addcslashes($this->link, "'") . "'; return false;");
        }

        if($this->attr->getAttrib('type') == 'submit'){
            $attribs->setAttrib('value', $this->label);

            return "<input  " . $attribs->render() . " />";
        }

        return "<button " . $attribs->render() . ">" . htmlspecialchars($this->label) . "</button>";
    }
}