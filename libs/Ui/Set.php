<?php

/**
 * Графическая оболочка для списка, например статуса некоторого дествия:
 * 1 - Active
 * 2 - Paused
 * 3 - Deleted
 *
 * ...
 *
 */


class Ui_Set extends Ui_Abstract {
    protected $set = array();

    // Значения по умолчанию, если по ключу не найдется настроек
    protected $defaultColor = null;
    protected $defaultLabel = null;

    /**
     * Для нерастягивающихся блочков
     *
     * @var bool
     */
    protected $inline = false;

    /**
     * Атрибуты элемента статуса (сам элемент представлен в виде ссылки <a></a>)
     *
     * @var Html_Attribs
     */
    protected $attribs;

    /**
     * @var string|null используемый словарь
     */
    protected $dictionary;

    /**
     * При переопределении конструктора, необходимо вызывать родительский конструтор
     */
    public function __construct($dictionary = null){
        $this->attribs = new Html_Attribs();
        $this->dictionary = (is_string($dictionary) && strlen($dictionary)) ? $dictionary : null;
    }

    /************************************************************/

    /**
     * Задать значение атрибута
     * Или удалить его, если занчение = NULL
     *
     * @param $name
     * @param null $value
     * @return self
     */
    public function setAttrib($name, $value = null){
        $this->attribs->setAttrib($name, $value);
        return $this;
    }

    /**
     * Переключить рендер на режим inline
     *
     * @param bool $inline
     * @return $this
     */
    public function setInLine($inline = true){
        $this->inline = (bool)$inline;
        return $this;
    }

    /**
     * Настройки списка
     */

    /**
     * Добавление значения
     *
     * @param string $key
     * @param string $label
     * @param string $color
     *
     * @return self
     */
    public function addValue($key, $label = null, $color = null){
        // Если Label не определен, он бертся из key
        if($label === null) $label = $key;

        $this->set[$key] = array(
            'label' => Translate::t($label, $this->dictionary),
            'color' => $color,
        );

        return $this;
    }

    /**
     * @param $key
     * @param null $label
     * @return Ui_Set
     */
    public function addValue_Yellow($key, $label = null){
        return $this->addValue($key, $label, 'FFFF99');
    }

    /**
     * @param $key
     * @param null $label
     * @return Ui_Set
     */
    public function addValue_Red($key, $label = null){
        return $this->addValue($key, $label, 'fdb0ae');
    }

    /**
     * @param $key
     * @param null $label
     * @return Ui_Set
     */
    public function addValue_Green($key, $label = null){
        return $this->addValue($key, $label, 'D3F4B3'); //
    }

    /**
     * @param $key
     * @param null $label
     * @return Ui_Set
     */
    public function addValue_White($key, $label = null){
        return $this->addValue($key, $label, 'FFF'); //
    }

    /**
     * Задать массив значений
     * (При этом текущие настрйоки обнуляются)
     *
     * Пример 1 (Просто названия):
     * [
     *     1 => 'Active',
     *     2 => 'Paused',
     * ]
     *
     * Пример 2 (Названия и цвета):
     * [
     *     1 => ['Active', \AP\Ui\Colors::BACKGROUND_GREEN],
     *     2 => ['Paused', \AP\Ui\Colors::BACKGROUND_RED],
     * ]
     *
     * @param array $array
     * @return Ui_Set
     */
    public function setArray(array $array){
        // отчистка текущей настройки
        $this->set = array();

        if(is_array($array) && count($array)){
            foreach($array as $key => $v){
                $label = null;
                $color = null;

                if(is_array($v)){
                    // настройка лейбла
                    if(isset($v['label'])) $label = $v['label'];
                    else if(isset($v[0]))  $label = $v[0];

                    // настрйока цвета
                    if(isset($v['color'])) $color = $v['color'];
                    else if(isset($v[1]))  $color = $v[1];
                }
                else {
                    $label = $v;
                }

                $this->addValue(
                    $key,
                    $label,
                    $color
                );
            }
        }

        return $this;
    }

    /**
     * @param $link
     * @return $this
     */
    public function setLink($link){
        $this->setAttrib('href', $link);

        return $this;
    }

    /**
     * Поулчить массив, для работы с селектами
     *
     * @param string|null $firstLabel
     * @param string $firstKey
     * @return array
     */
    public function getArrayToSelect($firstLabel = null, $firstKey = ''){
        $result = [];

        if(strlen($firstLabel)){
            $result[(string)$firstKey] = $firstLabel;
        }

        if(count($this->set)){
            foreach($this->set as $k => $el){
                $result[$k] = $el['label'];
            }
        }

        return $result;
    }

    /**
     * Проверить ключ
     *
     * @param $key
     * @return bool
     */
    public function isValid($key){
        return isset($this->set[$key]);
    }


    /**
     * Нарисовать статус
     *
     * @param string|int $value
     * @return string
     */
    public function render($value = null){
        Site::addCSS("ui/set.css");

        // Приоритет: определенный лейбел, значение по умолчанию, ключ
        $label = Translate::t($this->getLabel($value), $this->dictionary);

        $attr = clone $this->attribs;
        $attr->addAttribPre("class", "ui_set ");
        $attr->setCSS("background", "#" . $this->getColor($value));

        if($this->inline){
            $attr->setCSS('display', 'inline');
        }

        if(strlen($this->attribs->getAttrib('href'))){
            return "<a{$attr->render()}>{$label}</a>";
        }
        else {
            return "<span{$attr->render()}>{$label}</span>";
        }
    }

    /**
     * Поулчить название ключа
     *
     * @param $value
     * @return null
     */
    public function getLabel($value){
        return isset($this->set[$value]['label']) ?
            $this->set[$value]['label'] :
            (is_null($this->defaultLabel) ? $value : $this->defaultLabel);
    }

    /**
     * Поулчить цвет ключа
     * Приоритет выбора цвета: определенный цвет, цвет по умолчанию, рандомный цвет постоянный для строки
     *
     * @param $value
     * @return null
     */
    public function getColor($value){
        return isset($this->set[$value]['color']) ?
            $this->set[$value]['color'] :
            (is_null($this->defaultColor) ? Ui_Colors::getBackground($value) : $this->defaultColor);
    }


    /**
     * Описание в таблице
     *
     * @return string
     */
    public function renderComment(){
        return "";
    }
}