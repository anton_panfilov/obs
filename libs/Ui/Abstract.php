<?php


class Ui_Abstract {
    /**
     * @return string
     */
    public function render(){
        return "";
    }

    public function __toString(){
        return (string)$this->render();
    }
}