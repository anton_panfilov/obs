<?php

class Date_Config {
    static protected $cache_ts = [];

    static public function getTimestamp($timezone){
        // количесво секунд, котрое по этой строке не будут пересчитываться данные
        $cache_seconds = 30;

        if(!isset(self::$cache_ts[$timezone]) || self::$cache_ts[$timezone]['mt'] < microtime(1) - $cache_seconds){
            list($ts, $Ymd, $w, $Gi) = explode(" ", Date_Timezones::getDateForTimezone("U Ymd w Gi", null, $timezone));

            self::$cache_ts[$timezone] = [
                'mt' => microtime(1),
                'data' => [
                    'ts'    => $ts,
                    'Ymd'   => $Ymd,
                    'w'     => $w,
                    'Gi'    => $Gi,
                ]
            ];
        }

        return self::$cache_ts[$timezone]['data'];
    }

    static public function isIndividualSalesLimit($salesConfig, $postingID, $timezone){
        // если не настроенно
        if(count($salesConfig) == 0) return true;

        $postingDateYmd = Date_Timezones::getDateForTimezone('Y-m-d', null, $timezone);

        $salesLimit = Date_Config::getSalesPerDay(
            $salesConfig,
            $postingDateYmd
        );

        $sales = Posting_SalesCache::getSales($postingID, $postingDateYmd);

        return ($sales < $salesLimit);
    }

    static public function isGroupsSalesLimit($postingID, $timezone){
        $result = true;

        $postingDateYmd = Date_Timezones::getDateForTimezone('Y-m-d', null, $timezone);
        $groupsSales = Posting_SalesCache::getSalesGroupsSales($postingID, $postingDateYmd);

        foreach($groupsSales as $groupID => $groupSales){
            $groupsSaleNow = Date_Config::getSalesPerDay(
                Posting_SalesCache::getConfigByGroup($groupID),
                $postingDateYmd
            );

            if($groupSales >= $groupsSaleNow){
                $result = false;
                break;
            }
        }

        return $result;
    }


    static public function isWorktime($workflowConfig, $timezone){
        // массив
        if(is_array($workflowConfig)){
            $data = $workflowConfig;
        }
        else {
            $data = Posting_Filters_Element_Calendar::getWorkflowRules($workflowConfig);
        }

        // стандартное правило
        $currentRule = [
            // YYYYMMDD
            0 => 10000000,  // from date
            1 => 30000001,  // till date

            // 0 - 6 (Sun - Sat)
            2 => 0,         // from week day
            3 => 6,         // till week day

            // (int)HHMM
            4 => 0,         // from time
            5 => 2400,      // till time

            6 => $data['default'],
        ];

        $date = self::getTimestamp($timezone);

        if(count($data['rules'])){
            foreach($data['rules'] as $rule){
                if(
                    // проверка подходит ли правило по фильтрам
                    $date["Ymd"] >= $rule[0] &&
                    $date["Ymd"] <= $rule[1] &&

                    $date["w"] >= $rule[2] &&
                    $date["w"] <= $rule[3] &&

                    $date["Gi"] >= $rule[4] &&
                    $date["Gi"] <  $rule[5] &&

                    // сравнивание приоритета правила с текущим
                    (
                        (
                            $rule[1] - $rule[0] < $currentRule[1]  - $currentRule[0]
                        )
                        ||
                        (
                            $rule[1] - $rule[0] == $currentRule[1] - $currentRule[0] &&
                            $rule[3] - $rule[2] < $currentRule[3]  - $currentRule[2]
                        )
                        ||
                        (
                            $rule[1] - $rule[0] == $currentRule[1] - $currentRule[0] &&
                            $rule[3] - $rule[2] == $currentRule[3] - $currentRule[2] &&
                            $rule[5] - $rule[4] < $currentRule[5]  - $currentRule[6]
                        )
                    )
                ){
                    $currentRule = $rule;
                }
            }
        }

        return (bool)$currentRule[6]; // bool
    }

    /*****************************************************************************************/



    /**
     * @param string|array $config
     * @param string $date
     *
     * @return null|int null - не ограниченно
     */
    static public function getSalesPerDay($configSalesPerDay, $date){
        if(!is_array($configSalesPerDay)){
            $configSalesPerDay = Posting_Filters_Element_Calendar::getSalesPerDayRules($configSalesPerDay);
        }

        $result = null;
        $priority = 0;

        if(is_array($configSalesPerDay) && count($configSalesPerDay)){
            foreach($configSalesPerDay as $el){
                if(isset($el['priority'], $el['type'], $el['value']) && $el['priority'] >= $priority){
                    if($el['type'] == 'general'){
                        $result = $el['value'];
                    }
                    else if(
                        isset($el['week']) &&
                        $el['type'] == 'week' &&
                        $el['week'] == date('w', strtotime($date))
                    ){
                        $result = $el['value'];
                    }
                    else if(
                        isset($el['weeks']) &&
                        $el['type'] == 'weeks'
                    ){
                        $cw = date('w', strtotime($date));
                        if($cw >= $el['weeks'][0] && $cw <= $el['weeks'][1]){
                            $result = $el['value'];
                        }
                    }
                    else if(
                        isset($el['date']) &&
                        $el['type'] == 'date' &&
                        $el['date'] == date('Y-m-d', strtotime($date))
                    ){
                        $result = $el['value'];
                    }
                    else if(
                        isset($el['dates']) &&
                        $el['type'] == 'dates'
                    ){
                        $cd = substr($date, 0, 4) . substr($date, 5, 2) . substr($date, 8, 2);
                        if(
                            $cd >= substr($el['dates'][0], 0, 4) . substr($el['dates'][0], 5, 2) . substr($el['dates'][0], 8, 2) &&
                            $cd <= substr($el['dates'][1], 0, 4) . substr($el['dates'][1], 5, 2) . substr($el['dates'][1], 8, 2)
                        ){
                            $result = $el['value'];
                        }
                    }
                }
            }
        }

        return $result;
    }
}