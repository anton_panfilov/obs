<?php

class Date_Timezones {
    /**
     * @var DateTime[]
     */
    static protected $dateTimesTimezonesObjects = [];

    static protected $list_cache;

    static public function getTimezonesList(){
        if(is_null(self::$list_cache)){
            $dic = 'timezones';

            self::$list_cache = [
                'America/Los_Angeles'   => 'America / ' .   Translate::t('Pacific Time',            $dic),
                'America/Denver'        => 'America / ' .   Translate::t('Mountain Time',           $dic),
                'America/Chicago'       => 'America / ' .   Translate::t('Central Time',            $dic),
                'America/New_York'      => 'America / ' .   Translate::t('Eastern Time',            $dic),

                'Europe/London'         => 'Europe / '   .   Translate::t('London Time',             $dic),

                'Europe/Kaliningrad'    => 'Europe / MSK-1 / ' .   Translate::t('Kaliningrad Time',        $dic),
                'Europe/Moscow'         => 'Europe / '         .   Translate::t('Moscow Time',             $dic),
                'Asia/Yekaterinburg'    => 'Asia / MSK+2 / '   .   Translate::t('Yekaterinburg Time',      $dic),
                'Asia/Novosibirsk'      => 'Asia / MSK+3 / '   .   Translate::t('Omsk Time',               $dic),
                'Asia/Krasnoyarsk'      => 'Asia / MSK+4 / '   .   Translate::t('Krasnoyarsk Time',        $dic),
                'Asia/Irkutsk'          => 'Asia / MSK+5 / '   .   Translate::t('Irkutsk Time',            $dic),
                'Asia/Yakutsk'          => 'Asia / MSK+6 / '   .   Translate::t('Yakutsk Time',            $dic),
                'Asia/Vladivostok'      => 'Asia / MSK+7 / '   .   Translate::t('Vladivostok Time',        $dic),
                'Asia/Magadan'          => 'Asia / MSK+8 / '   .   Translate::t('Magadan Time',            $dic),
            ];
        }
        return self::$list_cache;
    }

    static public function getTimezoneLabel($timezone, $default = null){
        if(isset(self::getTimezonesList()[$timezone])) return self::getTimezonesList()[$timezone];
        if(!is_null($default)) return $default;
        return $timezone;
    }

    static public function getDateForTimezone($format, $time = null, $timezone = null){
        if(is_null($timezone)){
            $timezone = Conf::main()->timezone;
        }

        // Value::export($timezone);

        if(!isset(self::$dateTimesTimezonesObjects[$timezone])){
            self::$dateTimesTimezonesObjects[$timezone] = new DateTime('now', new DateTimeZone($timezone));

            if(is_numeric($time)){
                self::$dateTimesTimezonesObjects[$timezone]->setTimestamp($time);
            }
        }
        else {
            if(is_numeric($time)){
                self::$dateTimesTimezonesObjects[$timezone]->setTimestamp($time);
            }
            else {
                self::$dateTimesTimezonesObjects[$timezone]->setTimestamp(time());
            }
        }

        return self::$dateTimesTimezonesObjects[$timezone]
            ->format($format);
    }

    static public function stringToTime($string, $timezone = null){
        if(is_null($timezone)){
            $timezone = Conf::main()->timezone;
        }

        $tz = date_default_timezone_get();
        date_default_timezone_set($timezone);
        $r = strtotime($string);
        date_default_timezone_set($tz);

        return $r;
    }
}