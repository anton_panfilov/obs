<?php

class Report_Webmaster_Summary {
    static protected $cache = [
        'daily'  => [],
        'hourly' => [],
    ];

    static protected $processID;
    static protected $successFinish = 0;

    static protected $keys = [
        'date',
        'product',
        'webmaster',
        'agent',
        'channel_type',
        'channel',
        'region',
        'domain',
    ];

    static protected $params = [
        'call_all',
        'call_exit',
        'call_data_err',
        'call_lead',
        'call_finish',
        'call_sold',
        'call_reject',
        'leads_all',
        'leads_test',
        'leads_fraud',
        'leads_sold',
        'leads_pending_all',
        'leads_pending_sold',
        'leads_pending_reject',
        'leads_return',
        'leads_errors',
        'leads_fatal_errors',
        'pending_money_wm',
        'pending_money_pool',
        'pending_money_ern',
        'pending_money_ttl',
        'leads_money_wm',
        'leads_money_pool',
        'leads_money_costs',
        'leads_money_ern',
        'leads_money_ttl',
    ];

    /******************************************************************************************************************/

    /**
     * Создать последовательность данных ключей, для добавления в базу
     *
     * @param array $data - данные для создания колючей
     * @param array $modify - модификации
     * @return array
     */
    static protected function getKeysArrayForInsert($data, $modify = []){
        $result = [];

        foreach(self::$keys as $key){
            if(isset($modify[$key])){
                $result[] = $modify[$key];
            }
            else {
                $result[] = isset($data[$key]) ? $data[$key] : "";
            }
        }

        return $result;
    }

    static public function reindexPeriod($from, $till, $market = null, $delete = true){
        ini_set('memory_limit', '1024M');

        $result = null;
        $currentMarket = null;

        if(Lead_Market::isMarket($market, 1)){
            $currentMarket = Db::getDefaultMarket();
            Db::setDefaultMarket($market);
        }

        if(
            (new Validator_Date())->isValid($from) &&
            (new Validator_Date())->isValid($till) &&
            strtotime($from) < strtotime(date("Y-m-d")) &&
            strtotime($till) < strtotime(date("Y-m-d")) &&
            strtotime($from) <= strtotime($till)
        ){
            try{
                Db::reports()->transactionStart();

                // удаление старых данных за этот день. опастно если не переиндексируеться вся дата
                // и есть дейсвия выполняемые не в день получения лида
                if($delete){
                    $start = microtime(1);
                    Db::reports()->delete(
                        "webmaster_summary_days",
                        "`date` BETWEEN ? AND ?",
                        $from, $till
                    );

                    Db::reports()->delete(
                        "webmaster_summary_hours",
                        "`date` BETWEEN ? AND ?",
                        $from . " 00:00:00",
                        $till . " 23:59:59"
                    );
                    echo "delete $from to $till     -     " . round((microtime(1)-$start), 2) . " sec \r\n";
                }

                ////////////////////////////
                $date       = $from;
                $tillTime   = strtotime($till);

                while(true){
                    $start = microtime(1);
                    self::reindexDate($date, $market, false, false);
                    echo "reindex $date                  -     " . round((microtime(1)-$start), 2) . " sec \r\n";
                    //////////////

                    $ts     = strtotime("{$date} + 1Day");
                    $date   = date('Y-m-d', $ts);
                    if($ts > $tillTime){
                        break;
                    }
                }

                Db::reports()->transactionCommit();
            }
            catch(Exception $e){
                Db::reports()->transactionRollback();
            }
        }

        if($currentMarket){
            Db::setDefaultMarket($currentMarket);
        }

        return $result;
    }

    /**
     * Обновить репорт за выбранную дату
     *
     * @param string $date
     * @param int|null $market
     * @param bool $delete
     * @param bool $transaction
     *
     * @return null|array
     */
    static public function reindexDate($date, $market = null, $delete = false, $transaction = true){
        $result = null;
        $currentMarket = null;

        if(Lead_Market::isMarket($market, 1)){
            $currentMarket = Db::getDefaultMarket();
            Db::setDefaultMarket($market);
        }

        if(
            (new Validator_Date())->isValid($date) &&
            strtotime($date) < strtotime(date("Y-m-d")) // если эта дата в прошлом
        ){
            try {
                if($transaction){
                    Db::reports()->transactionStart();
                }

                // удаление старых данных за этот день. опастно при отсроченных продажах
                if($delete){
                    Db::reports()->delete(
                        "webmaster_summary_days",
                        "`date`=?",
                        $date
                    );

                    Db::reports()->delete(
                        "webmaster_summary_hours",
                        "`date` BETWEEN ? AND ?",
                        $date . " 00:00:00",
                        $date . " 23:59:59"
                    );
                }

                // переиндексация
                $result = self::run(
                    $date . " 00:00:00",
                    $date . " 23:59:59"
                );

                if($transaction){
                    Db::reports()->transactionCommit();
                }
            }
            catch(Exception $e){
                if($transaction){
                    Db::reports()->transactionRollback();
                }
            }
        }

        if($currentMarket){
            Db::setDefaultMarket($currentMarket);
        }

        return $result;
    }

    /**
     * Добавление в кеш данных от от работы системы
     */
    static public function addLastData(){
        /*
         * 1. если нет активных процессов
         * 2. берем данные между
         *    [последними загруженными, -1 минута от текущего времени]
         *      или
         *    [последними загруженными, послядняя секуна часа поседних загруженных]
         *      если первый промежуток выходит за 1 час
         *
         *     или если данных вобще нет, то грузим первую минуту текущих суток
         */

        $run            = 0;
        $period_start   = null;
        $period_finish  = null;
        $table_prefix   = null;

        $last_proccess = Db::scheduler()->fetchRow(
            "SELECT finish, period_finish FROM `webmaster_summary_processes` WHERE `error`=0 ORDER BY id DESC LIMIT 1"
        );

        if($last_proccess === false){
            if(Date_Timezones::getDateForTimezone('H:i') != '00:00'){
                $period_start = Date_Timezones::getDateForTimezone('Y-m-d 00:00:00');
                $period_finish = Date_Timezones::getDateForTimezone('Y-m-d 00:00:59');
                $run = 1;
            }
        }
        else {
            if($last_proccess['finish']){
                $startTime  = strtotime($last_proccess['period_finish']) + 1;
                $finishTime = strtotime('-1 minutes');

                $period_start = Date_Timezones::getDateForTimezone('Y-m-d H:i:', $startTime) . "00";

                if(
                    strtotime(Date_Timezones::getDateForTimezone('Y-m-d H:i:00', $finishTime)) >=
                    strtotime(Date_Timezones::getDateForTimezone('Y-m-d H:i:00', $startTime))
                ){
                    $run = 1;

                    if(
                        Date_Timezones::getDateForTimezone('Y-m-d H', $startTime) ==
                        Date_Timezones::getDateForTimezone('Y-m-d H', $finishTime)
                    ){
                        $period_finish = Date_Timezones::getDateForTimezone('Y-m-d H:i:', $finishTime) . "59";
                    }
                    else {
                        $period_finish = Date_Timezones::getDateForTimezone('Y-m-d H', $startTime) . ":59:59";
                    }
                }
            }
        }

        if($run){
            Db::reports()->transactionStart();

            Db::scheduler()->insert("webmaster_summary_processes", [
                'period_start'  => $period_start,
                'period_finish' => $period_finish,
            ]);

            self::$processID = Db::scheduler()->lastInsertId();

            if(Cron::isStart()){
                Cron::addShutdownTrigger(function(){
                    Report_Webmaster_Summary::shutdown();
                });
            }

            $runResult = self::run($period_start, $period_finish);

            $countAll       = $runResult['countAll'];
            $countDataHours = $runResult['countDataHours'];
            $countDataDays  = $runResult['countDataDays'];

            Db::reports()->transactionCommit();
            self::$successFinish = 1;

            Db::scheduler()->update("webmaster_summary_processes", [
                'count_all'    => $countAll,
                'count_hourly' => $countDataHours,
                'count_days'   => $countDataDays,
            ], 'id=?', self::$processID);

            if(!Cron::isStart()){
                Report_Webmaster_Summary::shutdown();
            }
        }
    }

    static protected function run($period_start, $period_finish){
        $data = Db::logs()->fetchAll(
            "SELECT `create`, `keys`, `params` FROM `webmaster_summary_log` WHERE `create` BETWEEN '{$period_start}' AND '{$period_finish}' "
        );

        $countDataHours = 0;
        $countDataDays  = 0;
        $countAll       = count($data);

        $separator = "\t";

        if(count($data)){
            $dataDays = [];
            $dataHours = [];

            foreach($data as $dID => $el){
                $keys   = json_decode($el['keys'], true);
                $params = json_decode($el['params'], true);
                unset($data[$dID]);

                // чинить данные, если они пришли со старой системы
                if(strtotime($el['create']) < strtotime(Boot::newVersionDate())){
                    $keys['lead_create'] = Date_Timezones::getDateForTimezone("Y-m-d H:i:s", strtotime($keys['lead_create'] . " Europe/Moscow"));
                }


                // Индекс дневных
                $keyDays = implode(
                    $separator,
                    self::getKeysArrayForInsert(
                        $keys,
                        [
                            'date' => substr($keys['lead_create'], 0, 10)
                        ]
                    )
                );

                if(!isset($dataDays[$keyDays])){
                    $dataDays[$keyDays] = [];
                    foreach(self::$params as $p){
                        $dataDays[$keyDays][$p] = 0;
                    }
                }

                if(count($params)){
                    foreach($params as $k => $v){
                        if(isset($dataDays[$keyDays][$k])){
                            $dataDays[$keyDays][$k] = round($dataDays[$keyDays][$k] + $v, 6);
                        }
                    }
                }


                // Индекс частовых
                $keyHours = implode(
                    $separator,
                    self::getKeysArrayForInsert(
                        $keys,
                        [
                            'date' => substr($keys['lead_create'], 0, 13) . ":00:00"
                        ]
                    )
                );

                if(!isset($dataHours[$keyHours])){
                    $dataHours[$keyHours] = [];
                    foreach(self::$params as $p){
                        $dataHours[$keyHours][$p] = 0;
                    }
                }

                if(count($params)){
                    foreach($params as $k => $v){
                        if(isset($dataHours[$keyHours][$k])){
                            $dataHours[$keyHours][$k] = round($dataHours[$keyHours][$k] + $v, 6);
                        }
                    }
                }

            }

            // одновление дневных
            foreach($dataDays as $k => $v){
                try {
                    $insert = [];
                    $keys = explode($separator, $k);
                    $i = 0;
                    foreach(self::$keys as $key){
                        $insert[$key] = $keys[$i];
                        $i++;
                    }

                    foreach(self::$params as $p){
                        $insert[$p] = isset($v[$p]) ? $v[$p] : 0;
                    }

                    Db::reports()->insert("webmaster_summary_days", $insert);
                }
                catch(Exception $e){
                    $update = [];
                    foreach(self::$params as $p){
                        $update[$p] = new Db_Expr("`{$p}`+" . (isset($v[$p]) ? $v[$p] : 0));
                    }

                    Db::reports()->update(
                        "webmaster_summary_days",
                        $update,
                        "`" . implode("`=? and `", self::$keys) . "`=?",
                        explode($separator, $k)
                    );
                }
            }
            $countDataDays = count($dataDays);

            unset($dataDays);

            // одновление часовых
            foreach($dataHours as $k => $v){
                try {
                    $insert = [];
                    $keys = explode($separator, $k);
                    $i = 0;
                    foreach(self::$keys as $key){
                        $insert[$key] = $keys[$i];
                        $i++;
                    }

                    foreach(self::$params as $p){
                        $insert[$p] = isset($v[$p]) ? $v[$p] : 0;
                    }

                    Db::reports()->insert("webmaster_summary_hours", $insert);
                }
                catch(Exception $e){
                    $update = [];
                    foreach(self::$params as $p){
                        $update[$p] = new Db_Expr("`{$p}`+" . (isset($v[$p]) ? $v[$p] : 0));
                    }

                    Db::reports()->update(
                        "webmaster_summary_hours",
                        $update,
                        "`" . implode("`=? and `", self::$keys) . "`=?",
                        explode($separator, $k)
                    );
                }
            }
            $countDataHours = count($dataHours);
            unset($dataHours);
        }

        return [
            'periodStart'      => $period_start,
            'periodFinish'     => $period_finish,
            'countDataHours'   => $countDataHours,
            'countDataDays'    => $countDataDays,
            'countAll'         => $countAll,
        ];
    }

    /**
     * Завершение процесса индексации данных
     */
    static public function shutdown(){
        Db::scheduler()->update("webmaster_summary_processes", [
            'finish'        => 1,
            'error'         => (int)(!self::$successFinish),
            'runtime'       => Boot::getRuntime(),
            'memory_use_mb' => memory_get_peak_usage() / 1048576,
        ], 'id=?', self::$processID);
    }

    /******************************************************************************************************************/

    /**
     * Формирование строки суммарного репорта из имеющихся данных
     *
     * @param $data
     * @return mixed
     */
    static protected function summaryElementUpdate($data){
        $not_test_count = $data['leads_all'] - $data['leads_test'];

        // обрехание нулей дробной части
        foreach(self::$params as $p){
            if(isset($data[$p])){
                $data[$p] = $data[$p]+0;
            }
        }

        // калькулируемые поля
        $data['leads_pending_wait'] = $data['leads_pending_all'] - $data['leads_pending_sold'] - $data['leads_pending_reject'];
        $data['call_wait']          = $data['call_all'] - $data['call_exit'] - $data['call_data_err'] - $data['call_finish'];

        // показатели трафика
        if($not_test_count != 0){
            $data['epl'] = round($data['leads_money_wm'] / $not_test_count, 2);
            $data['ipl'] = round($data['leads_money_ttl'] / $not_test_count, 2);
            $data['convert'] = round($data['leads_sold'] * 100 / $not_test_count, 1);
        }
        else {
            $data['epl'] = null;
            $data['ipl'] = null;
            $data['convert'] = null;
        }

        if(UserAccess::isAdmin()){
            if($data['leads_money_ttl'] != 0){
                $data['distribution_wm']      = round($data['leads_money_wm']     * 100 / $data['leads_money_ttl'], 1);
                $data['distribution_pool']    = round($data['leads_money_pool']   * 100 / $data['leads_money_ttl'], 1);
                $data['distribution_costs']   = round($data['leads_money_costs']  * 100 / $data['leads_money_ttl'], 1);
                $data['distribution_ern']     = round($data['leads_money_ern']    * 100 / $data['leads_money_ttl'], 1);
            }
            else {
                $data['distribution_wm']      = null;
                $data['distribution_pool']    = null;
                $data['distribution_costs']   = null;
                $data['distribution_ern']     = null;
            }
        }

        if(!UserAccess::isAdmin()){
            unset($data['leads_money_pool']);
            unset($data['leads_money_costs']);
            unset($data['leads_money_ern']);

            unset($data['pending_money_pool']);
            unset($data['pending_money_ern']);
        }

        if(!UserAccess::isAdmin() && !UserAccess::isAgentBuyer()){
            unset($data['leads_money_ttl']);
            unset($data['ipl']);

            unset($data['pending_money_ttl']);
        }

        if(!UserAccess::isAdmin() && !UserAccess::isAgentWebmaster() && !UserAccess::isWebmaster()){
            unset($data['epl']);
            unset($data['leads_money_wm']);

            unset($data['pending_money_wm']);
        }

        return $data;
    }

    /******************************************************************************************************************/

    /**
     * Получение сумм основных параметров по заданным дням
     *
     * @param $from
     * @param $till
     * @param null $product
     * @param null $webmaster
     * @param null $channel_type
     * @param null $channel
     * @param null $region
     * @param null $domain
     * @param null $agent
     *
     * @return array
     */
    static public function getTotal(
        $from,
        $till,
        $product        = null,
        $webmaster      = null,
        $channel_type   = null,
        $channel        = null,
        $region         = null,
        $domain         = null,
        $agent          = null
    ){
        return self::getDailyAbstract(false, $from, $till, $product, $webmaster, $channel_type, $channel, $region, $domain, $agent);
    }

    /**
     * Получение вебмастрского суммарного репорта с групировкой по дням
     *
     * @param $from
     * @param $till
     * @param null $product
     * @param null $webmaster
     * @param null $channel_type
     * @param null $channel
     * @param null $region
     * @param null $domain
     * @param null $agent
     *
     * @return array
     */
    static public function getSummaryDays(
        $from,
        $till,
        $product        = null,
        $webmaster      = null,
        $channel_type   = null,
        $channel        = null,
        $region         = null,
        $domain         = null,
        $agent          = null
    ){
        return self::getDailyAbstract(true, $from, $till, $product, $webmaster, $channel_type, $channel, $region, $domain, $agent);
    }

    /**
     * Получение вебмастрского суммарного репорта с групировкой по дням
     *
     * @param bool $details
     * @param $from
     * @param $till
     * @param null $product
     * @param null $webmaster
     * @param null $channel_type
     * @param null $channel
     * @param null $region
     * @param null $domain
     * @param null $agent
     *
     * @return array
     */
    static protected function getDailyAbstract(
        $details,
        $from,
        $till,
        $product        = null,
        $webmaster      = null,
        $channel_type   = null,
        $channel        = null,
        $region         = null,
        $domain         = null,
        $agent          = null
    ){
        $key = implode("-", [
            Db::getDefaultMarket(),
            $details, $from, $till, $product, $webmaster, $channel_type, $channel, $region, $domain, $agent
        ]);

        if(!isset(self::$cache['daily'][$key])){
            $fields = [];
            if($details){
                $fields[] = 'date';
            }
            foreach(self::$params as $p){
                $fields[$p] = new Db_Expr("ifnull(sum(`{$p}`), 0)");
            }

            $select = Db::reports()
                ->select('webmaster_summary_days', $fields)
                ->where("`date` BETWEEN ? and ?",
                    substr($from, 0, 10) . " 00:00:00",
                    substr($till, 0, 10) . " 23:59:59"
                );

            $webmaster  = UserAccess::getWebmasterAccess($webmaster);
            $agent      = UserAccess::getWebmasterAgentAccess($agent);

            if($product)        $select->where("`product`=?",       $product);
            if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
            if($agent)          $select->where("`agent`=?",         $agent);
            if($channel_type)   $select->where("`channel_type`=?",  $channel_type);
            if($channel)        $select->where("`channel`=?",       $channel);
            if($region)         $select->where("`region`=?",        $region);
            if($domain)         $select->where("`domain`=?",        $domain);

            if($details){
                $select->group('date');

                $index = $select->fetchUnique();

                //////////////

                $all   = [];
                $total = [];

                foreach(self::$params as $p){
                    $total[$p] = 0;
                }

                //////////////

                $date       = $till;
                $fromTime   = strtotime($from);

                while(true){
                    $all[$date] = [
                        'date' => $date,
                    ];

                    // формирование строки данных
                    foreach(self::$params as $p){
                        $all[$date][$p] = isset($index[$date][$p]) ? $index[$date][$p] : 0;
                    }

                    // дополнение строки данных
                    $all[$date] = self::summaryElementUpdate($all[$date]);

                    // подбивание тотала
                    foreach(self::$params as $p){
                        $total[$p]+= isset($all[$date][$p]) ? $all[$date][$p] : 0;
                    }

                    //////////////

                    $ts     = strtotime("{$date} - 1Day");
                    $date   = date('Y-m-d', $ts);
                    if($ts < $fromTime){
                        break;
                    }
                }

                // дополнение тотала данных
                $total = self::summaryElementUpdate($total);

                self::$cache['daily'][$key] = [
                    'data'  => $all,
                    'total' => $total,
                ];
            }
            else {
                self::$cache['daily'][$key] = self::summaryElementUpdate($select->fetchRow());
            }
        }

        return self::$cache['daily'][$key];
    }

    /***************************************************************/

    /**
     * Получение вебмастрского суммарного репорта с групировкой по часам
     *
     * @param $from
     * @param $till
     * @param null $product
     * @param null $webmaster
     * @param null $channel_type
     * @param null $channel
     * @param null $region
     * @param null $domain
     * @param null $agent
     *
     * @return array
     */
    static public function getSummaryHourly(
        $from,
        $till,
        $product        = null,
        $webmaster      = null,
        $channel_type   = null,
        $channel        = null,
        $region         = null,
        $domain         = null,
        $agent          = null
    ){
        $key = implode("-", [
            $from, $till, $product, $webmaster, $channel_type, $channel, $region, $domain, $agent, Db::getDefaultMarket()
        ]);

        if(!isset(self::$cache['hourly'][$key])){
            $fields = ['date'];
            foreach(self::$params as $p){
                $fields[$p] = new Db_Expr("sum(`{$p}`)");
            }

            $select = Db::reports()
                ->select('webmaster_summary_hours', $fields)
                ->where("`date` BETWEEN ? and ?",
                    substr($from, 0, 10) . " 00:00:00",
                    substr($till, 0, 10) . " 23:59:59"
                )
                ->group('date');

            $webmaster  = UserAccess::getWebmasterAccess($webmaster);
            $agent      = UserAccess::getWebmasterAgentAccess($agent);

            if($product)        $select->where("`product`=?",       $product);
            if($webmaster)      $select->where("`webmaster`=?",     $webmaster);
            if($agent)          $select->where("`agent`=?",         $agent);
            if($channel_type)   $select->where("`channel_type`=?",  $channel_type);
            if($channel)        $select->where("`channel`=?",       $channel);
            if($region)         $select->where("`region`=?",        $region);
            if($domain)         $select->where("`domain`=?",        $domain);

            $index = $select->fetchUnique();

            //////////////

            $all   = [];
            $total = [];

            foreach(self::$params as $p){
                $total[$p] = 0;
            }

            //////////////

            if($till == date('Y-m-d')){
                $date = $till . " " . date("H") . ":00:00";
            }
            else {
                $date = $till . " 23:00:00";
            }

            $fromTime = strtotime($from . " 00:00:00");

            $i = 0;
            while(true){
                $all[$date] = [
                    'date' => $date,
                ];

                // формирование строки данных
                foreach(self::$params as $p){
                    $all[$date][$p] = isset($index[$date][$p]) ? $index[$date][$p] : 0;
                }

                // дополнение строки данных
                $all[$date] = self::summaryElementUpdate($all[$date]);

                // подбивание тотала
                foreach(self::$params as $p){
                    $total[$p]+= isset($all[$date][$p]) ? $all[$date][$p] : 0;
                }

                //////////////

                $ts = strtotime("{$date} - 1Hour");
                $old_date = $date;
                $date = date('Y-m-d H:00:00', $ts);
                if($old_date == $date){
                    $ts = strtotime("{$date} - 2Hour");
                    $date = date('Y-m-d H:00:00', $ts);
                }
                $i++;

                if($i > 1000 || $ts < $fromTime){
                    break;
                }
            }

            // дополнение тотала данных
            $total = self::summaryElementUpdate($total);

            self::$cache['hourly'][$key] = [
                'data'  => $all,
                'total' => $total,
            ];
        }

        return self::$cache['hourly'][$key];
    }


    /**
     * Получить количесво фатальных ошибок сгруппированных по дням
     *
     * @param $from
     * @param $till
     * @param null $webmaster
     * @param null $agent
     * @return array
     */
    static public function getFatalErrorsDailyCounts($from, $till, $webmaster = null, $agent = null){
        $select = Db::reports()
            ->select('webmaster_summary_days', [
                'period'                => new Db_Expr("DATE_FORMAT(`date`, '%Y-%m')"),
                'leads_fatal_errors'    => new Db_Expr("sum(`leads_fatal_errors`)"),
            ])
            ->where("date BETWEEN ? and ?",
                substr($from, 0, 10) . " 00:00:00",
                substr($till, 0, 10) . " 23:59:59"
            )
            ->group('period')
            ->order('period desc');

        $webmaster = UserAccess::getWebmasterAccess($webmaster);
        $agent = UserAccess::getWebmasterAgentAccess($agent);

        if($webmaster)  $select->where("`webmaster`=?", $webmaster);
        if($agent)      $select->where("`agent`=?",     $agent);

        return $select->fetchPairs();
    }

    /**
     * Получить количесво ошибок сгруппированных по дням
     *
     * @param $from
     * @param $till
     * @param null $webmaster
     * @param null $agent
     * @return array
     */
    static public function getErrorsDailyCounts($from, $till, $webmaster = null, $agent = null){
        $select = Db::reports()
            ->select('webmaster_summary_days', [
                'period'        => new Db_Expr("DATE_FORMAT(`date`, '%Y-%m')"),
                'leads_errors'  => new Db_Expr("sum(`leads_errors`)"),
            ])
            ->where("date BETWEEN ? and ?",
                substr($from, 0, 10) . " 00:00:00",
                substr($till, 0, 10) . " 23:59:59"
            )
            ->group('period')
            ->order('period desc');

        $webmaster = UserAccess::getWebmasterAccess($webmaster);
        $agent = UserAccess::getWebmasterAgentAccess($agent);

        if($webmaster)  $select->where("`webmaster`=?", $webmaster);
        if($agent)      $select->where("`agent`=?",     $agent);

        return $select->fetchPairs();
    }

    /******************************************************************************************************************/
    /**
     * @param Db_Connection_Abstract $fromdb
     * @param Db_Connection_Abstract $todb
     * @param string $exception (Values 'show'= Value::export(Exception $e),'throw'= throws Exception,'hide' = hides Exception (DEFAULT)
     * @param array $options
     * @throws Exception
     */
    static public function replicate(
            Db_Connection_Abstract $fromdb,
            Db_Connection_Abstract $todb,
            $exception = 'hide',
            $table = 'webmaster_summary_log',
            $primary_key = 'id',
            $num_rows = NULL
        ){
        //////////////////CHECKS//////////////////
        //////////////////////////////////////////
        $to_last_pk = $todb->fetchOne("SELECT {$primary_key} FROM {$table} ORDER BY {$primary_key} DESC");
        if(!is_numeric($to_last_pk)){
            $to_last_pk = 1;
        }
        if($to_last_pk){
            if(empty($num_rows)){
                $chunk = $fromdb->fetchAll("SELECT * FROM {$table} WHERE {$primary_key} > {$to_last_pk}");
            }else{
                $chunk = $fromdb->fetchAll("SELECT * FROM {$table} WHERE {$primary_key} > {$to_last_pk} LIMIT {$num_rows}");
            }
//            Value::export($chunk);
            if(is_array($chunk) && count($chunk)){
                $todb->transactionStart();
                try{
                    $continue = true;
                    $todb->insertMulti($table,$chunk);
                }catch (Exception $e){
                    $continue = false;
                    if($exception == 'show')Value::export($e);
                    if($exception == 'throw')throw new Exception($e->getMessage(),$e->getCode());
                    if($exception == 'hide');
                }
                if($continue){
                    $todb->transactionCommit();
                }else{
                    $todb->transactionRollback();
                }

            }else{
//                echo 'failed';
            }
        }

    }
}