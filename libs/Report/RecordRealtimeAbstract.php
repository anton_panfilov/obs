<?php

abstract class Report_RecordRealtimeAbstract {
    /*************************************************************************************************************************  
    *       Abstract Values - необходимо переопределить в основном классе  
    */
    
    /**
    * Массив ключевых (индексных) полей
    * Ex: ['date', 'userid']
    * 
    * @var array
    */
    static protected $allKeys = [];
    
    /**
    * Массив возможных значений
    * Ex: ['leadsCount', 'soldsCount', ...]
    * 
    * @var array
    */
    static protected $allValues = [];
    
    
    /**
    * Название таблицы, в которой хранятся данные
    * 
    * @var string
    */
    protected $table;
    
    /**
    * До какого знака округлять при действиях со значенийми, нужно выбрать максимально необходимое значение
    * (В первую очередь это нужно что бы исключить ошибку, которая возникает при сложении дробных чисел)
    * 
    * @var int
    */
    protected $round = 5;
    
    /**************************************************************************************************************************/
    
    static protected $pool = array();
    
    /**
    * получить объект по ключам
    * 
    * @param mixed $keys
    * @return self
    */
    static protected function keys(array $keys){
        if(count(static::$allKeys)){
            if(!isset(self::$pool[get_called_class()])) self::$pool[get_called_class()] = array();
            $pool =& self::$pool[get_called_class()];
            
            $poolKeys = array();
            foreach(static::$allKeys as $keyName){
                $keyValue = isset($keys[$keyName]) ? $keys[$keyName] : 0;
                $poolKeys[$keyName] = $keyValue;
                if(!isset($pool[$keyValue])){
                    $pool[$keyValue] = array();
                }
                $pool =& $pool[$keyValue];
            }  
            
            if(!is_a($pool, get_called_class())){
                $pool = new static($poolKeys); 
            }
        }
        else {
            throw new Exception('All Keys is Empty');
        }

        return $pool;
    }
    
    /**************************************************************************************************************************/
    
    /**
    * Массив хранит ВСЕ ключи и их значения
    * 
    * @var array
    */
    protected $keys = array();
    
    /**
    * Массив хранит данные, которые необходимо обновить на данный момент
    * 
    * @var array
    */
    protected $values = array();
    
    /*****************************************************/
    
    /**  
    * Поулчить конект к базе данных
    * 
    * @return Db_Connection_Abstract
    */
    protected function getDatabase(){
        return Db::site();
    }
    
    public function __construct($keys){
        $this->keys = $keys;   
    } 
    
    public function __destruct(){
        $this->commit();        
    } 
    
    /******************************************************************/
    
    /**
    * Обновление некоторых кешевых данных 
    * 
    * @param mixed $values
    */
    public function update(array $values){
        if(count($values)){
            foreach($values as $k => $v){
                if(in_array($k, static::$allValues)){
                    if($v != 0){
                        if(!isset($this->values[$k])){
                            $this->values[$k] = $v;
                        }
                        else{
                            $this->values[$k] = round($this->values[$k] + $v, $this->round);  
                            
                            // Если после таких действий значение равно нулю, то убрать его из массива записываемых значений
                            if($this->values[$k] == 0){
                                unset($this->values[$k]);    
                            } 
                        }         
                    }
                }  
            }   
        }

        return $this;
    }
    
    public function commit(){
        if(count($this->values)){
            try {
                $this->getDatabase()->insert($this->table, $this->keys + $this->values); 
            }  
            catch(Exception $e){
                foreach($this->values as $k => $v){
                    $this->values[$k] = new Db_Expr("`{$k}`+" . (int)$v);
                }
                $this->getDatabase()->update($this->table, $this->values, $this->keys);
            }
            $this->values = array();
        }

        return $this;
    }
}