<?php

class Form {
    const METHOD_GET  = 'get';
    const METHOD_POST = 'post';

    protected $title;
    protected $description;
    protected $logo;

    protected $markRequiredFields = false;

    /**
     * Массив общих ошибок, как правило поступающих не из самой формы
     * Элементы хранят свои ошибки в себе, они не поступают в этот массив
     * (В интерефсе это текст на красном/желтом/... фоне над формой)
     *
     * @var array
     */
    protected $errorMessages = array();

    /**
     * Массив положительных действий
     * Для того что бы сообщить пользователю что произошло при заполеннии формы, а так же для того что бы дать дальнейшие указания
     * можно передать ему success messages
     * (В интерфейсе это текст над формой на зеленом/... фоне)
     *
     * @var array
     */
    protected $successMessages = array();

    /**
     * Сообщения из формы:
     * 1. предупреждения
     * 2. уведомления
     * 3. ошибки
     *
     * @var array ИЛИ Специальный класс который можно использовать где угодно
     */
    protected $messages = array();

    /**
     *
     * @var Html_Attribs
     */
    protected $attribs;

    /**
     * Массив элементов
     *
     * @var Form_Element_Abstract[]
     */
    protected $elements = [];

    /**
     * Массив зависимостей полей
     * Ключ массива соотвествует имени поля, на котрое выставлены зависимости
     *
     * @var Form_Conditional_Array[]
     */
    protected $conditionals = [];

    /**
     * Массив значений, которые будут передаваться в скрытых инпутах.
     * ключ - имя переменной, значение - её значение
     *
     * @var array
     */
    protected $hidden_elements = [];

    /**
     * Массив кнопок
     *
     * @var Ui_Button[]
     */
    protected $buttons = null;

    /**
     * Имя кнопки, по которой кликнули
     * Доступно только после isValid()
     *
     * @var string|null
     */
    protected $clicked_button_name;

    /**
     * Массив блоков
     * Необходимы для групировки элементов
     *
     * @var mixed
     */
    protected $blocks = array();

    /**
     * Текущий блок
     * Следующий элемент будет добавлн в этот блок
     *
     * @var mixed
     */
    protected $current_block = null;

    /**
     * Показатель правильности данных
     *
     * null  - неизвестно
     * true  - ошибок нет
     * false - с ошибками
     *
     * @var bool|null
     */
    protected $isValid;

    /**
     * Защита поста, данные могту быть присланны только с этой формы, при отправление и другой не будет совпадать секретный хеш
     *
     * @var bool
     */
    protected $isPostSafe = true;

    //////////////////////////////////////////////////////

    protected function init(){}

    public function __construct($title = ''){
        Translate::addDictionary('form', 5);

        $this->attribs = new Html_Attribs();
        $this->attribs->setAttrib("method", "post");
        $this->setTitle($title);

        $this->getSafeHashCookieSalt();

        $this->init();
    }

    /**
     * Порядковый номер, для быстрого получения имени случайной переменной
     *
     * @var int
     */
    protected $random_name_num = 0;

    /**
     * Получить случайное имя, которое в данный момент не используется
     *
     * @param null $name
     *
     * @return string = rand_{INT}
     */
    protected function getRandomName($name = null){
        if(is_null($name)){
            $this->random_name_num++;
            $name = "rand_{$this->random_name_num}";

            if(isset($this->elements[$name])){
                return $this->getRandomName(null);
            }
        }

        return $name;
    }
    /*****************************************************************************************************************
     * Conditionals Files Settings
     *********************************************/

    /**
     * Добавление условия, когда при определеннных значениях поля $elementName,
     * показываються или скрываются другие элементы
     *
     * @param string $elementName       Имя элемента, которое
     *
     * @param string|array $values    Значения элемента, при которых будут показываться значения
     *                                Можно передать как одно значение (string), так и массив значений (array)
     *                                При передаче массива, элементы будут отображаться
     *                                при совпадении хотябы с один значением
     *
     * @param string|array $elements  Поля, котоорые будут показываться при определенных выше значениях
     *                                Можно передать как имя одного элемента (string), так и массив имен (array)
     *
     * @throws Exception
     * @return self
     */
    public function addConditionalFileds_Array($elementName, $values, $elements){
        /**
         * 1. пометить элементы как спрятанные
         *    (перед проверкаой на ошибки или во время проверкми на ошибки или рендера если на ошибки не проверялось)
         * 2. добавить скрипт и тригеры на элемент, которые будет скрывать или показывать скрытые поля
         * 3. перед рендером пробежать и открыть скрытые поля, котоыре по условиям долны открыться
         */

        // Если кондишана еще нет, или его тип не соответсвует
        if(!isset($this->conditionals[$elementName])){
            // если кондишан еще не задан, создать его
            $this->conditionals[$elementName] = new Form_Conditional_Array($this, $elementName);
        }
        else if(!($this->conditionals[$elementName] instanceof Form_Conditional_Array)){
            // Если кондишан уже задан, ну его тип отличается
            throw new Exception("Conditionals conflict");
        }

        $this->conditionals[$elementName]->addCondition($values, $elements);

        return $this;
    }

    /**
     * Добавление условия, когда при определеннных значениях поля $elementName,
     * показываються или скрываются другие элементы
     *
     * @param string $elementName       Имя элемента, которое
     *
     * @param string|array $values    Значения элемента, при которых не будут показываться значения
     *                                Можно передать как одно значение (string), так и массив значений (array)
     *
     * @param string|array $elements  Поля, котоорые не будут показываться при определенных выше значениях
     *                                Можно передать как имя одного элемента (string), так и массив имен (array)
     *
     * @throws Exception
     * @return self
     */
    public function addConditionalFileds_NotArray($elementName, $values, $elements){
        /**
         * 1. пометить элементы как спрятанные
         *    (перед проверкаой на ошибки или во время проверкми на ошибки или рендера если на ошибки не проверялось)
         * 2. добавить скрипт и тригеры на элемент, которые будет скрывать или показывать скрытые поля
         * 3. перед рендером пробежать и открыть скрытые поля, котоыре по условиям долны открыться
         */

        // Если кондишана еще нет, или его тип не соответсвует
        if(!isset($this->conditionals[$elementName])){
            // если кондишан еще не задан, создать его
            $this->conditionals[$elementName] = new Form_Conditional_NotArray($this, $elementName);
        }
        else if(!($this->conditionals[$elementName] instanceof Form_Conditional_NotArray)){
            // Если кондишан уже задан, ну его тип отличается
            throw new Exception("Conditionals conflict");
        }

        $this->conditionals[$elementName]->addCondition($values, $elements);

        return $this;
    }

    /**
     * Получить справило зависимости для элемента
     *
     * @param string $name
     * @return Form_Conditional_Abstract | null
     */
    public function getConditional($name){
        return isset($this->conditionals[$name]) ? $this->conditionals[$name] : null;
    }

    public function removeConditional($name){
        unset($this->conditionals[$name]);
        return $this;
    }

    /*****************************************************************************************************************
     * Тексты
     *********************************************/

    /**
     * Установить название формы
     *
     * @param mixed $title
     * @return self
     */
    public function setTitle($title){
        $this->title = Translate::t($title);
        return $this;
    }

    /**
     * Установить описание формы
     *
     * @param mixed $description
     * @return self
     */
    public function setDescription($description){
        $this->description = Translate::t($description);
        return $this;
    }

    /**
     * Установить логотип формы
     *
     * @param mixed $logo
     * @return self
     */
    public function setLogo($logo){
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return $this
     */
    public function disablePostSafe(){
        $this->isPostSafe = false;
        return $this;
    }

    /**
     * @param $flag
     *
     * @return $this
     */
    public function setMarkRequiredFields($flag){
        $this->markRequiredFields = (bool)$flag;
        return $this;
    }

    /**
     * Добавить сообщение об ошибке
     *
     * Также можно загрузить все ошибки из объектов:
     * - \AP\Result
     * ...
     *
     * @param string|Result_MultiErrors|null $message
     * @return self
     */
    public function addErrorMessage($message){
        if($message instanceof Result_MultiErrors){
            if(!$message->isSuccess()){
                if(count($message->getErrors())){
                    foreach($message->getErrors() as $mess){
                        $this->addErrorMessage($mess);
                    }
                }
                else {
                    // неизвестная ошибка
                    $this->addErrorMessage("Unknown Error");
                }
            }
            return $this;
        }
        else {
            $this->errorMessages[] = Translate::t($message);
            return $this;
        }
    }

    /**
     * Добавить сообщение о соершеннии положительного действия
     *
     * @param mixed $message
     * @return self
     */
    public function addSuccessMessage($message = null){
        if(is_null($message)) $message = "<i class='fa fa-save margin-right-10'></i> Saved";

        $this->successMessages[] = Translate::t($message);
        return $this;
    }

    /*****************************************************************************************************************
    * Управление кнопками
    *********************************************/

    /**
     * @param $label
     * @param string $name
     * @param bool $generalButton
     *
     * @return Ui_Button
     */
    public function addButton($label, $name = 'submitButton', $generalButton = true){
        // Если еще не настраивалось, то сделать хранилище кнопок массивом
        if(is_null($this->buttons)) $this->buttons = array();

        if(strlen($name)){
            $id_html = "btn_{$name}";
            $name_html = "btn_{$name}";
        }
        else {
            $id_html = "btn_{$name}";
            $name_html = null;
        }

        $this->buttons[$name] = (new Ui_Button($label, $id_html))
            ->setTypeSubmit();

        if(strlen($name_html)){
            $this->buttons[$name]
                ->setAttrib('name',  $name_html);
        }

        if($generalButton){
            $this->buttons[$name]->setStylePrimary();
        }

        return $this->buttons[$name];
    }

    /**
     * @param $name
     * @return $this
     */
    public function removeButton($name){
        unset($this->buttons[$name]);
        return $this;
    }

    /**
     * Добавлеие кнопки для статистической формы
     *
     */
    public function addButtonPage($label, $action = null){
        // Если еще не настраивалось, то сделать хранилище кнопок массивом
        if(is_null($this->buttons)) $this->buttons = array();

        $btn = (new Ui_Button($label))->setTypeSubmit();

        if($action){
            $btn->setAttrib('onclick', "jQuery(this).closest('form')[0].action = '{$action}'");

            // если передаваемый экшен заканчивается на /, до дорбавить ему index, для сравнения с роутом
            // if(substr($action, strlen($action) - 1, 1) == "/") $action .= "index";

            /*
            if(\AP::site()->getCurrentRoute() == $action){
                $btn->setStylePrimary();
            }
            */
        }

        $this->buttons[] = $btn;
    }

    public function disableButtons(){
        $this->buttons = [];
        return $this;
    }

    /*****************************************************************************************************************
    * Разделители и групировка полей
    *********************************************/

    /**
     * Стартовать новый блок
     *
     * @param mixed $title
     * @param mixed $name
     *
     * @throws Exception
     * @return Form_Block
     */
    public function startBlock($title = null, $name = null){
        if($name === null) $name = "rb_" . (count($this->blocks)+1);

        if(!isset($this->blocks[$name])){
            $this->blocks[$name] = new Form_Block($name, $title);
            $this->current_block = $name;
        }
        else {
            throw new Exception("System Error: block name already use");
        }

        return $this->blocks[$name];
    }

    /**
     * Завершить блок с конечным разделителем для пользователя
     *
     * @return Form_Block
     */
    public function endBlock(){
        $c = $this->current_block;
        $this->current_block = null;
        return $this->getBlock($c);
    }

    /**
     * Завершить блок без конечного разделителя, просто завершить список полей для блока
     * Может пригодится в случае если поля при каких то условиях скрываются, в этом случае сам блок тоже не будет рисоваться
     *
     * @return Form_Block
     */
    public function breakBlock(){
        return $this->endBlock()->setNoEnded();
    }

    /**
     * Добавить разделитель
     *
     * @param mixed $title
     * @param mixed $description
     *
     * @return Form_Block
     */
    public function addSeparator($title = null, $description = null){
        $title = Translate::t($title);
        return $this->startBlock($title)->setNoEnded()->setDescription($description);
    }

    /*****************************************************************************************************************
    * Управление полями
    *********************************************/

    /**
     * Получить ссылку на элемент
     *
     * @param mixed $name
     *
     * @throws Exception
     * @return Form_Element_Abstract
     */
    public function getElement($name){
        $name = (string)$name;

        if(isset($this->elements[$name])){
            return $this->elements[$name];
        }

        throw new Exception("Element `{$name}` not found");
    }

    public function removeElement($name){
        $name = (string)$name;

        if(isset($this->elements[$name])){
            unset($this->elements[$name]);
            return true;
        }

        return false;
    }

    /************************************************/

    /**
     * Добавлет к форме скрытый элемент <input type='hidden' name='NAME' id='NAME' value='VALUE'>
     *
     * Значение этого поля нельзя получить из формы методами FORM->getValues() или FORM->getValue(name)
     *
     * @param string $name
     * @param mixed $value
     *
     * @return self
     */
    public function addElementHidden($name, $value){
        $this->hidden_elements[(string)$name] = $value;
        return $this;
    }


    /**
     * Визуальное объединение нескольких элементов в одну строчку
     *
     * @param array $elements        Массив объединяемых элементов
     * @param string|null $label           label объединенных полей
     *                              (Если label = null, он слепливается из lebels входящих элементов)
     * @param string|null $name
     * @return Form_Element_MultiElement
     */
    public function addMultiElement($elements, $label = null, $name = null){
        foreach($elements as $e){
            $this->elements[$e]->setRenderStandart(false);
        }

        // если имя не заданно, задать случайное
        if(is_null($name)) $name = self::getRandomName();

        $el = new Form_Element_MultiElement($elements, $label, $name);

        return $this->addElement($el);
    }

    /************************************************/

    /**
     * @param Form_Element_Abstract $element
     *
     * @return Form_Element_Abstract
     */
    public function addElement(Form_Element_Abstract $element){
        $this->isValid = null; // при добавлении нового элемента, правильнть данных переходит в состояние - неизвестно

        $this->elements[$element->getName()] = $element;
        $this->elements[$element->getName()]->setForm($this, $this->current_block);

        return $this->elements[$element->getName()];
    }

    /**
     *
     * @param string $label
     * @param string $text
     * @param null|string $name
     *
     * @return Form_Element_Abstract
     */
    public function addElementStatic($label, $text, $name = null){
        return $this->addElement(
            (new Form_Element_Static($this->getRandomName($name), $label))->setText($text)
        )
            ->setRequired(false);
    }

    /**
     * Простое тектовое поле Input[type=TEXT] с возможностью ввода угловых скобок
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_TextHtml
     */
    public function addElementTextHtml($name, $label = null){
        return $this->addElement(new Form_Element_TextHtml($name, $label));
    }

    /**
     * Простое тектовое поле Input[type=TEXT]
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_Text
     */
    public function addElementText($name, $label = null){
        return $this->addElement(new Form_Element_Text($name, $label));
    }

    public function addElementText_NewBuyerName($name, $label = null, $duplicateErrorMessage = 'This trade name already use'){
        $this->addElementText($name, $label)
            ->addValidator(new Form_Validator_Length(2, 32))
            ->addValidator(new Form_Validator_UniqueInDatabase(
                "SELECT id FROM `buyers_accounts` WHERE `name`=? LIMIT 1",
                Db::processing(),
                $duplicateErrorMessage
            ));
    }

    public function addElementText_NewLogin($name, $label = 'el_login'){
        Site::addJSCode("
            jQuery('#{$name}').blur(function(){
                jQuery('#{$name}_ajax_error').hide();
                jQuery('#{$name}').css('borderColor', '');

                jQuery.getJSON(
                    '/check-new-login',
                    {
                        login: jQuery('#{$name}').val()
                    },
                    function(data){
                        if(!data.valid){
                            jQuery('#{$name}').css('borderColor', '#e02222');
                            jQuery('#{$name}_ajax_error').html(data.reason).show();
                        }
                    }
                )
            });

            jQuery('#{$name}').focus(function(){
                jQuery('#{$name}_ajax_error').hide();
                jQuery('#{$name}').css('borderColor', '');
            });
        ");

        $el = $this->addElementText($name, $label);
        $el
            ->setMaxLength(32)
            ->setComment(new Translate_Expr("<div style='clear: both; padding: 5px 0 0 0; '>" .
                Translate::t("el_login_comment") .
            "</div>"))
            ->addValidator(new Form_Validator_NewLogin())
            ->setRenderTypeBigValue()
            ->setTextPreInput(
                "<div style='float:left; padding-right:15px;'>"
            )
            ->setTextPostInput(
                "</div><div id='{$name}_ajax_error' style=' display: none; color:#e02222; float:left; line-height: 100%; padding-top: 8px'></div>"
            )
            ->getAttribs()
                ->setCSS('width', '200px');

        return $el;
    }

    /**
     * Элемент IP 
     *
     * @param mixed $name
     * @param mixed $label
     * @return Form_Element_Text
     */
    public function addElementText_IP($name, $label = null){
        return $this->addElementText($name, $label)
            ->setMask("ip")
            ->addValidator(new Form_Validator_Regexp(
                '/^((25[0-5]|2[0-4]\\d|[01]?\\d\\d?)\\.){3}(25[0-5]|2[0-4]\\d|[01]?\\d\\d?)$/',
                "Invalid value for IP."
            ))
            ->setMaxLength(15);
    }
    
    /**
     * Элемент Numeric 
     *
     * @param mixed $name
     * @param mixed $label
     * @param int   $min
     * @param int   $max
     * @return Form_Element_Text
     */
    public function addElementText_Num($name, $label = null, $min = null, $max = null){
        $elem = $this->addElementText($name, $label)->setMask("num");
        $elem->getAttribs()->setAttrib('type', 'number');

        if(!is_null($min)) $elem->getAttribs()->setAttrib('min', $min);
        if(!is_null($max)) $elem->getAttribs()->setAttrib('max', $max);

        $elem->addValidator(new Form_Validator_Regexp("/^[-]?[0-9]+\$/", "Invalid numeric value"));
        $elem->addValidator(new Form_Validator_CheckNumBounds($min, $max));
        return $elem;
    }
    
    /**
     * Элемент Price 
     *
     * @param mixed $name
     * @param mixed $label
     * @param int   $maxFract
     * @param int   $min
     * @param int   $max 
     * @return Form_Element_Text
     */
    public function addElementText_Price($name, $label = null, $maxFract = 2, $min = null, $max = null){
        $elem = $this->addElementText($name, $label)->setMask("price");
        $elem->addValidator(new Form_Validator_Regexp(
            "/^[-]?[0-9]*(\\.[0-9]{0,". $maxFract ."})?\$/",
            "Invalid price value. Fractional part should have no more than " . $maxFract . " digits"));
        $elem->addValidator(new Form_Validator_CheckNumBounds($min, $max));
        return $elem;
    }

    /**
     * Имя на английском языке, максимальная длинна 128 символов
     *
     * @param mixed $name
     * @param mixed $label
     * @return Form_Element_Text
     */
    public function addElementText_Name($name, $label = null){
        return $this->addElementText($name, $label)
            ->addValidator(new Form_Validator_Regexp(
                '/^[a-zа-яё]([a-zа-яё]|\ |\-|\\\'|\.)*$/iu',
                "Name only accepts letters"
            ))
            ->addValidator(new Form_Validator_Length(null, 128))
            ->setMaxLength(128);
    }

    /**
     * @param string $first_name
     * @param string $last_name
     * @param string $label
     *
     * @return Form_Element_MultiElement
     */
    public function addElementText_FirstAndLastName(
        $first_name = 'first_name',
        $last_name = 'last_name',
        $label = 'el_fullName'
    ){
        $this->addElementText_Name($first_name, "el_firstName")
            ->setPlaceholder("placeholder_firstName");

        $this->addElementText_Name($last_name,  "el_lastName")
            ->setPlaceholder("placeholder_lastName");

        return $this->addMultiElement([$first_name, $last_name], $label);
    }

    /**
     * Email
     *
     * @param mixed $name
     * @param mixed $label
     * @param mixed $errorMessage
     * @return Form_Element_Text
     */
    public function addElementText_Email($name, $label = null, $errorMessage = null){
        return $this->addElementText($name, $label)
            ->addValidator(new Form_Validator_Email($errorMessage))
            ->addValidator(new Form_Validator_Length(null, 255))
            ->setMaxLength(255);
    }

    /**
     * Emails String
     *
     * @param mixed $name
     * @param mixed $label
     * @return Form_Element_TextHtml
     */
    public function addElementText_EmailsString($name, $label = null){
        return $this->addElementTextHtml($name, $label)
            ->addValidator(new Form_Validator_EmailsString())
            ->setRenderTypeBigValue();
    }

    /**
     * Emails String
     *
     * @param mixed $name
     * @param mixed $label
     * @return Form_Element_Textarea
     */
    public function addElementTextarea_EmailsString($name, $label = null){
        return $this->addElementTextarea($name, $label)
            ->addValidator(new Form_Validator_EmailsString())
            ->setRenderTypeBigValue();
    }

    /**
     * @param $name
     * @param null $label
     * @return Form_Element_Text
     */
    public function addElementText_PhoneMobileRus($name, $label = null){
        return $this->addElementText($name, $label)
            ->addFilter(new Form_Filter_NumbersOnly())
            ->addFilter(new Form_Filter_Function(function($value){
                if(strlen($value) && in_array(substr($value, 0, 1), ['7', '8'])){
                    return substr($value, 1);
                }
                return $value;
            }))
            ->addValidator(new Form_Validator_Regexp('/^[9][0-9]{9}$/'))
            ->addRenderFilter(new Form_Filter_Function(function($value) {
                if(strlen($value) == 10){
                    return
                        substr($value, 0, 3) . " " .
                        substr($value, 3, 3) . " " .
                        substr($value, 6, 4);
                }
                return $value;
            }))
            ->setComment("Ex: 909 338 2145");
    }

    /**
     * Phone
     *
     * @param mixed $name
     * @param mixed $label
     * @param mixed $errorMessage
     * @return Form_Element_Text
     */
    public function addElementText_Phone($name, $label = null){
        Site::addJS('form/element/phone.js');
        Site::addJSCode("
            jQuery('#{$name}').keyup(function(){
                document.ap_form_element_phone.renderCountry('{$name}');
            });
            document.ap_form_element_phone.renderCountry('{$name}');
        ");

        $el = $this->addElementText($name, $label);
        $el
            ->setWidth('220')
            ->setMaxLength(20)
            ->setTextPreInput("<div style='position: absolute; margin-left: 280px; padding-top: 3px; font-size: 120%; color: #555'>
                    <img id='form_phone_flag_{$name}' style='width: 16px; margin-right: 10px; visibility: hidden' src=''>
                    <span id='form_phone_label_{$name}'>Canada <span style='color: #999; font-size: 80%'>(+1)</span></span>
                </div>")
            ->setTextPostInput("
                <div>
                    <p style='color: #444'>" . Translate::t("Please enter a phone number with a country code") . "</p>
                    <p style='color: #888; font-size: 80%'>" . Translate::t("Example") . ": " . Translate::t("+1.8182223344") . "</p>
                </div>
            ")
            ->setRenderTypeBigValue()
            ->addFilter(new Form_Filter_Phone())
            ->addValidator(new Form_Validator_Phone());

        return $el;
    }

    /**
     * Имя в URL
     * Например http://domain.ltd/news/{VALUE_URL_NAME}/
     *
     * @param mixed $name
     * @param mixed $label
     * @return Form_Element_Text
     */
    public function addElementText_UrlName($name, $label = null){
        return $this->addElementText($name, $label)->addValidator(new Form_Validator_Regexp(
            '/^[a-z0-9]([a-z0-9]|\-|\_|\.)*$/i',
            "Invalid Name for URL. May include only latin letters, numbers and symbols: - . _"
        ));
    }

    /**
     * @param $name
     * @param null $label
     * @param array $macroses
     * @param bool $allow_self_domain
     * @return Form_Element_Text
     */
    public function addElementText_Url($name, $label = null, $macroses = [], $allow_self_domain = false){
        return $this->addElementText($name, $label)->addValidator(new Form_Validator_Url($macroses, $allow_self_domain));
    }


    /**
     * Поле пароля
     *
     * @param $name
     * @param null $label
     *
     * @return Form_Element_Password
     */
    public function addElementPassword($name, $label = null){
        return $this->addElement(new Form_Element_Password($name, $label));
    }

    /**
     * Новый пароль с подтверждением
     *
     * @param mixed $name
     * @param mixed $label
     * @param mixed $description
     * @param int $len
     *
     * @return Form_Element_Password
     */
    public function addElementPasswordNew($name, $label = 'form_btn_accountPassword', $description = '', $len = 8){
        if(strlen($label) || strlen($description)){
            $this->addSeparator($label, $description);
        }

        $el = $this->addElementPassword($name, "form_btn_password")
            ->setRenderValue(true)
            ->setComment('form_pass_rules')

            ->addValidator(new Form_Validator_Length($len, null, "Must be at least {$len} characters"))
            ->addValidator(new Form_Validator_Regexp("/[A-Z]/", "Must have at least one capital letter"))
            ->addValidator(new Form_Validator_Regexp("/[a-z]/", "Must have at least one letter"))
            ->addValidator(new Form_Validator_Regexp("/[0-9]/", "Must have at least one number"));


        $this->addElementPassword("{$name}_confirm", "form_btn_confirmPassword")
            ->setRenderValue(true)
            ->addValidator(new Form_Validator_Equal($name, "Passwords do not match"));

        return $el;
    }

    /**
     * Новый пароль с подтверждением (обязательно буквы или цифры и другое)
     *
     * @param mixed $name
     * @param mixed $label
     * @param mixed $description
     * @param int $len
     *
     * @return Form_Element_Password
     */
    public function addElementPasswordNew_LattersNumbers($name, $label = 'Account Password', $description = '', $len = 8){
        if(strlen($label) || strlen($description)){
            $this->addSeparator($label, $description);
        }

        $el = $this->addElementPassword($name, "Password")->setTooltip("<strong>Password must:</strong>
        <ul id='password_validation_ul'>
            <li>Be at least {$len} characters</li>
            <li>Have at least one letter</li>
            <li>Have at least one number</li>
        </ul>")
            ->addValidator(new Form_Validator_Length($len, null, "Be at least {$len} characters"))
            ->addValidator(new Form_Validator_Regexp("/[a-z]/i", "Have at least one letter"))
            ->addValidator(new Form_Validator_Regexp("/[0-9]/", "Have at least one number"));


        $this->addElementPassword("{$name}_confirm", "Confirm Password")
            ->addValidator(new Form_Validator_Equal($name, "Passwords do not match"));

        return $el;
    }

    /**
     * Новый пароль с подтверждением (проверка только по длинне)
     *
     * @param mixed $name
     * @param mixed $label
     * @param mixed $description
     * @param int $len
     *
     * @return Form_Element_Password
     */
    public function addElementPasswordNew_Ligths($name, $label = 'Account Password', $description = '', $len = 8){
        if(strlen($label) || strlen($description)){
            $this->addSeparator($label, $description);
        }

        $el = $this->addElementPassword($name, "Password")->setTooltip("<strong>Password must:</strong>
        <ul id='password_validation_ul'>
            <li>Be at least {$len} characters</li>
        </ul>")
            ->addValidator(new Form_Validator_Length($len, null, "Be at least {$len} characters"));


        $this->addElementPassword("{$name}_confirm", "Confirm Password")
            ->addValidator(new Form_Validator_Equal($name, "Passwords do not match"));

        return $el;
    }

    /**
     * Многострочный текст
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_Textarea
     */
    public function addElementTextarea($name, $label = null){
        return $this->addElement(new Form_Element_Textarea($name, $label));
    }

    /**
     * Выпадающий список
     *
     * @param string $name
     * @param string|null $label
     * @param array $options
     *
     * @return Form_Element_Select
     */
    public function addElementSelect($name, $label = null, $options = array()){
        $el = new Form_Element_Select($name, $label);
        $el->setOptions($options);

        return $this->addElement($el);
    }

    /**
     * @param       $name
     * @param null  $label
     * @param array $options
     *
     * @return Form_Element_Abstract
     */
    public function addElementImagePicker($name, $label = null, $options = array()){
        $el = new Form_Element_ImagePicker($name, $label);
        $el->setOptions($options);

        return $this->addElement($el);
    }

    public function addElementSelect_CatalogType($name, $label = 'form_catalogType', $firstElementLabel = null){
        return $this->addElement(
            (new Form_Element_Select($name, $label))
                ->setOptions(Catalog_Types::getTypesListForSelect(), $firstElementLabel)
        );
    }

    /**
     * Выбор US штата
     *
     * @param string $name
     * @param string $firstElementLabel - Label для первого элемента в selectbox, например: "Select One..." or "All".
     *                                    Значение первого option Всегда будет "". <option value=''>{LABEL}</option>
     * @param string $label
     *
     * @return Form_Element_Abstract
     */
    public function addElementSelect_State($name, $label = 'State', $firstElementLabel = null){
        return $this->addElement(
            (new Form_Element_Select($name, $label))
                ->setOptions(Geo::getStatesList(), $firstElementLabel)
        );
    }


    public function addElementSelect_Country($name, $label = 'Country', $firstElementLabel = null){
        return $this->addElement(
            (new Form_Element_Select($name, $label))
                ->setOptions(Geo::getCountriesList(), $firstElementLabel)
        );
    }

    /**
     * Чекбокс
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_Checkbox
     */
    public function addElementCheckbox($name, $label = null){
        return $this->addElement(new Form_Element_Checkbox($name, $label));
    }

    /**
     * Радио кнопки
     *
     * @param string $name
     * @param string|null $label
     * @param array $options
     *
     * @return Form_Element_Radio
     */
    public function addElementRadio($name, $label = null, $options = array()){
        $el = new Form_Element_Radio($name, $label);
        $el->setOptions($options);

        return $this->addElement($el);
    }

    /**
     * Массив чекбоксов
     *
     * @param string $name
     * @param string|null $label
     * @param array $options
     *
     * @return Form_Element_Checkboxes
     */
    public function addElementCheckboxes($name, $label = null, $options = array()){
        $el = new Form_Element_Checkboxes($name, $label);
        $el->setOptions($options);

        return $this->addElement($el);
    }

    /**
     * Список в возможностью выбрать несколько значений
     *
     * @param string $name
     * @param string|null $label
     * @param array $options
     *
     * @return Form_Element_SelectList
     */
    public function addElementSelectList($name, $label = null, $options = array()){
        $el = new Form_Element_SelectList($name, $label);
        $el->setOptions($options);

        return $this->addElement($el);
    }

    /**
     * Элемент загрузки файла
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_File
     */
    public function addElementFile($name, $label = null){
        $this->attribs->setAttrib("enctype", "multipart/form-data");

        return $this->addElement(new Form_Element_File($name, $label));
    }

    /**
     * TinyMCE
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_TinyMCE
     */
    public function addElementTinyMCE($name, $label = null){
        return $this->addElement(new Form_Element_TinyMCE($name, $label));
    }

    /**
     * Капча
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_Captcha
     */
    public function addElementCaptcha($name, $label = null){
        return $this->addElement(new Form_Element_Captcha('captcha_' . $name, $label))->disabledHover();
    }

    /**
     * Поле выболра даты из календаря
     *
     * @param string $name
     * @param string $label
     *
     * @return Form_Element_Date
     */
    public function addElementDate($name, $label = 'Date Range'){
        return $this->addElement(new Form_Element_Date($name, $label));
    }

    public function addElementDate3Fields($name, $label = null){
        return $this->addElement(new Form_Element_Date3Fields($name, $label));
    }

    /**
     * Выбор периода
     * Используется в репортах, в которых надо показать данные за относительно неболшой период времни в днях.
     * От 1 дня до нескольких лет
     *
     * @param $name
     * @param string $label
     *
     * @return Form_Element_DateRange
     */
    public function addElementDateRange($name, $label = 'Date Range'){
        return $this->addElement(new Form_Element_DateRange($name, $label))
            ->setRenderTypeBigValue();
    }

    /******************************************************************************/

    /**
     * @param string $name
     * @param null $firstElementLabel
     * @param string $label
     * @return Form_Element_Select
     */
    public function addElement_Timezone($name = 'timezone', $firstElementLabel = null, $label = "Timezone"){
        return $this->addElement(
            (new Form_Element_Select($name, $label))
                ->setOptions(Date_Timezones::getTimezonesList(), $firstElementLabel)
        );
    }

    /**
     * Поле выбора пользователя
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_User
     */
    public function addElement_User($name = 'user', $label = "User"){
        return $this->addElement(new Form_Element_User($name, $label));
    }

    /**
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_Product
     */
    public function addElement_ProductAjax($name = 'product', $label = "Product"){
        return $this->addElement(new Form_Element_Product($name, $label))
        ->setAutoCompliteParams([
            'market' => new Json_Expr(
                "function(){ return jQuery('#market').val() }"
            ),
        ]);
    }

    /**
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_Region
     */
    public function addElement_Region($name = 'region', $label = "Region"){
        return $this->addElement(new Form_Element_Region($name, $label));
    }

    /*****************************************************************************************/

    /**
     * Поле выбора пользователя
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_User
     */
    public function addElement_Client($name = 'user', $label = "User"){
        return $this->addElement(new Form_Element_User($name, $label));
    }

    /**
     * Поле выбора пользователя, который добавлял товары в каталог
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_CatalogUser
     */
    public function addElement_CatalogUser($name = 'user', $label = "User"){
        return $this->addElement(new Form_Element_CatalogUser($name, $label));
    }

    /**
     * Поле выбора пользователя
     *
     * @param string $name
     * @param string|null $label
     *
     * @return Form_Element_User
     */
    public function addElement_ClientManager($name = 'manager', $label = "Manager"){
        return $this->addElement(new Form_Element_ClientManager($name, $label));
    }

    /**
     * @param string $name
     * @param string $label
     *
     * @return Form_Element_Abstract
     */
    public function addElement_CatalogItem($name = 'item', $label = "Catalog Item"){
        return $this->addElement(new Form_Element_CatalogItem($name, $label))
            ->setRenderTypeBigValue();
    }

    /**
     * @param string $name
     * @param string $label
     *
     * @return Form_Element_Abstract
     */
    public function addElement_OrdersOnlyCreatings($name = 'order', $label = "form_order_auto"){
        return $this->addElement(
            (new Form_Element_Order($name, $label))->setStatusMax(Order_Status::ASSEMBLY)
        )
            ->setRenderTypeBigValue();
    }

    /**
     * @param      $name
     * @param null $label
     *
     * @return Form_Element_Textarea
     */
    public function addElementTextarea_VKComment($name, $label = null){
        Site::addJS("form/element/vk.js");
        Site::addJS("plugins/cursorPosition.js");

        $comment = [];

        foreach(Vk_Smiles::getSmiles() as $k => $v){
            $comment[] = "<a onclick='document.formVkAddText(\"{$name}\", \"({$k})\")'><img src=\"/static/img/vk/smiles/{$v}.png\"></a>";
        }

        $el = $this->addElementTextarea($name, $label)
            ->setRenderTypeBigValue()
            ->setComment(implode(" ", $comment));

        return $el;
    }

    /*****************************************************************************************************************
     * Проверка данных
     *********************************************/

    public function getMethod(){
        return mb_strtolower($this->attribs->getAttrib('method'));
    }

    /**
     * Отправлять форму методом POST
     *
     * @return Form
     */
    public function setMethodPOST(){
        $this->attribs->setAttrib('method', self::METHOD_POST);
        return $this;
    }

    /**
     * Отправлять форму методом GET
     *
     * @return Form
     */
    public function setMethodGET(){
        $this->attribs->setAttrib('method', self::METHOD_GET);
        return $this;
    }

    /**
     * Получить массив данных из GET или POST
     * Если возвращается пусто массив, то считается что запроса не было
     *
     * @return array
     */
    protected function getParamsArrayForGetOrPost(){
        $params = array();

        if($this->getMethod() == 'get'){
            // Если в GET переменных, есть хлтябы одна переменная, котораая есть в элементах формы (не считая Hidden)
            // считается что данные введедены
            $params = $_GET;
            if(count($params) && count($this->elements)){
                $match = false;
                $getParams = array_keys($params);
                foreach($this->elements as $name => $tempObj){
                    if($tempObj->isValue() && in_array($name, $getParams)){
                        $match = true;
                        break;
                    }
                }
                if(!$match) $params = array();
            }
        }
        else if($this->getMethod() == 'post'){
            // Если есть пост переменные, то считается что форма передана (даже если это не те переменные)
            // Это делает невозможным вывода на 1 страницу 2-х форм и проверку стандартной функцией isValidAndPost()
            // Ну можно проверять наличии поста отдельно и делать просто isValid
            $params = $_POST;
        }

        return $params;
    }

    /**
     * НЕ ПРОВЕРЯЕТ НА ОШИБКИ! А только получает полученный при предыдущей проверке результат
     *
     * @return bool|null
     */
    public function getIsValidCache(){
        return $this->isValid;
    }

    public function isValidAndPost($notErrors = false, $checkErrorsIfNotPost = false){
        $params = $this->getParamsArrayForGetOrPost();
        if(count($params)){
            return $this->isValid($params, $notErrors);
        }
        else if($checkErrorsIfNotPost){
            $this->isValid();
        }
        return false;
    }

    protected function getSafeHashCookieSalt(){
        if(!isset($_COOKIE['form_post_safe_salt'])){
            $_COOKIE['form_post_safe_salt'] = String_Hash::randomString(32);
            setcookie("form_post_safe_salt", $_COOKIE['form_post_safe_salt'], 0, "/");
        }

        return $_COOKIE['form_post_safe_salt'];
    }

    /**
     * @param null $salt
     *
     * @return string
     */
    public function getSafeHash($salt = null){
        return crypt(
            $this->getSafeHashCookieSalt() .
            Users::getCurrent()->id .
            Conf::security()->form_post_safe,
            $salt
        );
    }

    /**
     * Проверка формы на ошибки
     *
     * @param array|null $params
     * @param bool $notErrors Режим, при котором при нахождении ошибки она не показывается, а показывается значение по умолчанию
     * @return bool
     */
    public function isValid($params = null, $notErrors = false){
        $this->isValid = true;

        // проверка подлинности отправки, что бы нельзя было отправлять данные из другой формы
        if($this->isPostSafe()){
            if(
                !isset($_POST['_form_post_safe']) ||
                $this->getSafeHash($_POST['_form_post_safe']) != $_POST['_form_post_safe']
            ){
                $this->isValid = false;
                $this->addErrorMessage("The session has expired, please try to submit the form again");
            }
        }

        // Если не передавать параметры, то они будут получатся из окружения
        if(!is_array($params)) $params = $this->getParamsArrayForGetOrPost();

        // Value::export($params);
        // Value::export($this->buttons);

        if(count($this->buttons)){
            foreach($this->buttons as $k => $v){
                if(isset($params["btn_{$k}"])){
                    $this->clicked_button_name = $k;
                    break;
                }
            }
        }

        if(count($this->elements)){
            foreach($this->elements as $el){
                if(!$el->isValid($params, $notErrors)){
                    $this->isValid = false;
                }

                // проверить, если ли у поля зависимости, если есть, обработать их
                if(isset($this->conditionals[$el->getName()])){
                    $this->conditionals[$el->getName()]->run();
                }
            }
        }

        // вызвать тригеры элементов связанные с тем что форма заполненна правильно
        if($this->isValid){
            if(count($this->elements)){
                foreach($this->elements as $el){
                    $el->formValidationComplite();
                }
            }

        }

        return $this->isValid;
    }

    /**
     * Проверить на ошибки, при нахождении ошибки не показывать её, а поменять это поле на значение по умолчанию
     *
     * @return self
     */
    public function isValidNotErrors(){
        return $this->isValidAndPost(true);
    }

    public function getValue($name, $default = null){
        return isset($this->elements[$name]) ? $this->elements[$name]->getValue() : $default;
    }

    public function getValues(){
        $result = array();
        if(count($this->elements)){
            foreach($this->elements as $el){
                if($el->isValue() && $el->getUse() && !$el->isConditionHide()){
                    $result[$el->getName()] = $el->getValue();
                }
            }
        }
        return $result;
    }

    public function setValue($name, $value = null){
        if(isset($this->elements[$name])){
            $this->elements[$name]->setValue($value);
        }
        return $this;
    }

    public function setValues($values){
        if(is_array($values) && count($values)){
            foreach($values as $k => $v){
                $this->setValue($k, $v);
            }
        }
        return $this;
    }

    /*****************************************************************************************************************
    * Получение даннных для отрисовки
    *********************************************/

    /**
     * Получить массив элементов
     *
     * @return Form_Element_Abstract[]
     */
    public function getElementsAll(){
        return $this->elements;
    }

    /**
     * Получить массив данных для скрытых элементов
     *
     * @return array ([NAME1 => VALUE1, NAME2 => VALUE2, ...])
     */
    public function getElementsHidden(){
        return $this->hidden_elements;
    }

    /**
     * Ссылка на атрибуты формы (тега form)
     *
     * @return Html_Attribs
     */
    public function getAttribs(){
        return $this->attribs;
    }

    /**
     * Получить массив кнопок, если они не создавались, создать кнопку Submit и выдать её
     *
     * @return Ui_Button[]
     */
    public function getButtons(){
        if(is_null($this->buttons)) $this->addButton("btn_submit");
        return $this->buttons;
    }

    /**
     * Получить объект кнопки по её имени
     *
     * @param string $name
     *
     * @throws Exception
     * @return Ui_Button
     */
    public function getButton($name){
        if(!isset($this->buttons[$name])){
            throw new Exception("Button '{$name}' not found");
        }

        return $this->buttons[$name];
    }

    /**
     * Поулчить имя кнопки, на которую нажали
     * Доступно только после проверки на isValid
     *
     * @return null|string
     */
    public function getButtonClicked(){
        return $this->clicked_button_name;
    }

    /**
     * @return string
     */
    public function getTitle(){
        return (string)$this->title;
    }

    /**
     * Нужно ли защищать эту форму
     *
     * @return bool
     */
    public function isPostSafe(){
        return ($this->getMethod() == self::METHOD_POST && $this->isPostSafe);
    }

    /**
     * Получить текст описания формы или её работы
     *
     * @return string
     */
    public function getDescription(){
        return (string)$this->description;
    }

    /**
     * Ссылка на картинку
     *
     * @return string
     */
    public function getLogo(){
        return (string)$this->logo;
    }

    public function getMarkRequiredFields(){
        return $this->markRequiredFields;
    }

    /**
     * Получить массив внешних ошибок, который надо рендерить над формой
     *
     * @return array
     */
    public function getErrorMessages(){
        return $this->errorMessages;
    }

    /**
     * Получить массив положительных текстов, который надо рпенлерить над формой
     *
     * @return array
     */
    public function getSuccessMessages(){
        return $this->successMessages;
    }

    /**
     * Получить объект блока
     *
     * @param mixed $name
     * @return Form_Block | null
     */
    public function getBlock($name){
        return isset($this->blocks[$name]) ? $this->blocks[$name] : null;
    }

    /****************************************************************************************************************/

    protected function getRenderTemplate(){
        return new Form_Template_Default();
    }

    public function render(){
        // Построить список зависимости (если не было валидации) + сделать JS код
        if(count($this->conditionals)){
            foreach($this->conditionals as $condition){
                // повторынй просчет зависимостей на случай если что то поменялось. Логика с реального примера, обратно не менять
                $condition->run();

                // добавить JS код
                $condition->addJavaScriptCode();
            }
        }


        return $this->getRenderTemplate()->render($this);
    }

    public function __toString(){
        return (string)$this->render();
    }
}