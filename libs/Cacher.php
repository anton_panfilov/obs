<?php

/**
 * Cacher class
 * By default uses Memcached
 *
 * Class Cacher
 */
class Cacher {

    public static $expireTime = 0;
    public static $cacheClass = 'Memcached';

    private static $_servers = array(
        array('127.0.0.1', 11211)
    );
    private static $_instance = NULL;

    private function __construct() {}

    public static function _getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new Memcached;
            if (Conf::main()->memcached) {
                self::$_servers = Conf::main()->memcached;
            }
            self::$_instance->addServers(self::$_servers);
        }

        return self::$_instance;
    }

    public static function isInstalled() {
        if (extension_loaded(self::$cacheClass)) {
            return true;
        }

        return false;
    }

    public static function get($key) {
        if (!self::isInstalled()) {
            return false;
        }

        return self::_getInstance()->get($key);
    }

    public static function set($key, $value) {
        if (!self::isInstalled()) {
            return $value;
        }

        return self::_getInstance()->set($key, $value, self::$expireTime);
    }
}