<?php

require_once __DIR__ . "/../libs/Boot.php";
Boot::installCore();
Cron::start(__FILE__);

Db::site()->delete("cron_log", "`start`<?", date("Y-m-d H:i:s", strtotime("-1Day")));