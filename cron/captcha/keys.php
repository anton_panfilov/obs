<?php

require_once __DIR__ . "/../../libs/Boot.php";
Boot::installCore();
Cron::start(__FILE__);

Db::site()->update("captcha_keys", [
    'key1' => String_Hash::randomString(16),
    'key2' => Db::site()->fetchOne("select `key1` from `captcha_keys`"),
]);