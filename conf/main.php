<?php

return [
    'title' => 'Online Baby Shop',

    // Локализация
    'locales'               => [
        'us' => 'English',
        'ru' => 'Русский',
    ],
    'locale_default'        => 'ru',
    'localeLearningMode'    => false,

    // Домены
    'domain'                => 'obs.com',
    'domain_account'        => 'obs.dev',
    'domain_static'         => 'static.obs.dev',

    // Временная зона
    'timezone'              => 'Europe/Moscow',

    // Для SMTP
    'hostname_for_smtp'     => 'mail.online-baby-shop.ru',

    'memcached'             => [['127.0.0.1', 11211]],
];
