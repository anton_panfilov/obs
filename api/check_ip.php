<?php
require_once $_SERVER['PROJECT_ROOT'] . "/libs/Boot.php";
Boot::installCore();
$domain  = Conf::main()->domain_account;


if(isset($_GET['showIP'])){
    $result = $_SERVER['REMOTE_ADDR'];
}
else {
    $result = file_get_contents(Http_Url::getCurrentScheme() . "://" . $domain . "/api/check_ip?showIP=1");
}

if(isset($_GET['show'], $_GET['jsonp']) && $_GET['show'] == 'jsonp'){
    header("content-type: application/javascript");
    echo "{$_GET['jsonp']}(" . json_encode(['ip' => $result]) . ")";
}
else {
    echo $result;
}