<html><body><style>
    body {
        font-family: 'Courier New', serif;
        font-size: 12px;
    }
    h1{
        font-size: 16px;
        font-weight: bold;
        border-bottom: #AAA solid 1px;
        margin: 5px 0 10px 0;
        padding: 5px 10px 5px 10px;
        background: #F5F5F5;
    }

    h2{
        font-size: 14px;
        font-weight: bold;
    }

    pre {
        padding: 5px 0 5px 20px;
        font-family: 'Courier New', serif;
        font-size: 12px;
        border-left: #EEE solid 1px;
        display: block;
    }
</style>

<?php

/////////  Cron  /////////////////////////////////////

echo "<h1>Local Cron Updater</h1>";

if(is_array($h = explode(".", $_SERVER['SERVER_NAME'])) && count($h) > 1 && $h[count($h) - 1] == 'dev') {
    $file = realpath(__DIR__ . "/../conf/cron") . "/local-cron.dev.cron";

    if(is_file($file)) {
        echo "Config filename: `{$file}`<br>";

        $out = array();
        //exec("crontab -u\"www-data-t3ru\" {$file} 2>&1", $out);
        exec("crontab {$file} 2>&1", $out);

        if(count($out)) {
            $result['cron_status'] = STATUS_ERROR;
            $result['cron_reason'] = implode("; ", $out);

            echo "<h2>Update Error:</h2>";
            echo "<pre>" . implode("\r\n", $out) . "</pre>";
        }
        else {
            $result['cron_status'] = STATUS_OK;
            echo "<pre>" . file_get_contents($file) . "</pre>";
        }
    }
    else {
        $result['cron_status'] = STATUS_BLANK;
        $result['cron_reason'] = "File `{$file}` not found";
        echo "{$result['cron_reason']}<br><br>";
    }

}
else {
    echo "Invalid Server: {$_SERVER['SERVER_NAME']}. Only *.dev";
}
?>

</body></html>