<?php
require_once $_SERVER['PROJECT_ROOT'] . "/libs/Boot.php";
Boot::installCore();

$host = "." . (isset($_SERVER['HTTP_HOST']) ? explode(":", $_SERVER['HTTP_HOST'])[0] : $_SERVER['SERVER_NAME']);

if($host == ".localhost"){
    $host = "localhost";
}

setcookie(Copyright_Concept::COOKIE, 1, mktime(0,0,0,12,1,2030), "/", $host);

echo "Copyright_Concept mode ({$host}): on";