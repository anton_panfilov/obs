<?php
// Ключ: Boot::DEBUG_MODE_KEY
// ключ хранится там, а не в конфиге потому что его проверка идет до иницилизации конфигов

$key = (isset($_GET['key']) ? (string)$_GET['key'] : "unknown");

$host = "." . (isset($_SERVER['HTTP_HOST']) ? explode(":", $_SERVER['HTTP_HOST'])[0] : $_SERVER['SERVER_NAME']);

if($host == ".localhost"){
    $host = "localhost";
}

setcookie("debug_mode_key", $key, mktime(0,0,0,12,1,2030), "/", $host);

echo "Debug mode ({$host}): on (key: {$key})";