<?php

define('STATUS_OK',     'ok');
define('STATUS_ERROR',  'error');
define('STATUS_BLANK',  'blank');

define('START', microtime(1));

function updateServer($result){
    ob_start();

    /////// GIT ////////////////////////////////////

    echo "<h1>Git Update</h1>";

    // Current Linux Username
    $out = array();
    exec("whoami", $out);
    $username = isset($out[0]) ? $out[0] : "unknown";

    $dir = realpath(dirname(__FILE__) . "/../");
    chdir($dir);

    $command = 'git pull origin';
    $out = array();
    exec($command, $out);

    // todo: аналитика ответа
    if(count($out)){
        $result['git_status'] = STATUS_OK;
    }
    else {
        $result['git_status'] = STATUS_ERROR;
        $result['git_reason'] = "Blank result";
    }

    echo (
        "<p><strong>Command:</strong> {$command}" . "</p>" .
        "<p><strong>Project Directory:</strong> {$dir}" . "</p>" .
        "<pre>" .
            implode("\r\n", $out) .
        "</pre>"
    );

    /////////  Cron  /////////////////////////////////////

    echo "<h1>Cron</h1>";

    $file = realpath(__DIR__ . "/../conf/cron") . "/{$_SERVER['SERVER_NAME']}.cron";

    if(is_file($file)){
        echo "Config filename: `{$file}`<br>";

        $out = array();
        //exec("crontab -u\"www-data-t3ru\" {$file} 2>&1", $out);
        exec("crontab {$file} 2>&1", $out);

        if(count($out)){
            $result['cron_status'] = STATUS_ERROR;
            $result['cron_reason'] = implode("; ", $out);

            echo "<h2>Update Error:</h2>";
            echo "<pre>" . implode("\r\n", $out) . "</pre>";
        }
        else {
            $result['cron_status'] = STATUS_OK;
            echo "<pre>" . file_get_contents($file) . "</pre>";
        }
    }
    else {
        $result['cron_status'] = STATUS_BLANK;
        $result['cron_reason'] = "File `{$file}` not found";
        echo "{$result['cron_reason']}<br><br>";
    }

    /////////  Other  /////////////////////////////////////

    echo "<h1>Other</h1>";

    echo "<p><strong>Runtime:</strong> " . sprintf("%0.3f", microtime(1) - START) . "</p>";
    echo "<p><strong>Finished:</strong> " . date('Y-m-d H:i:s') . "</p>";
    echo "<p><strong>Linux Username:</strong> {$username}</p>";

    $result['text']   = ob_get_clean();

    return $result;
}


$result = [
    'git_status'    => STATUS_BLANK,
    'git_reason'    => '',
    'cron_status'   => STATUS_BLANK,
    'cron_reason'   => '',
    'reason'        => '',
    'text'          => '',
];

if(isset($_GET['key']) && $_GET['key'] == '9mn8I76cDFv32HFzb8'){
    $result = updateServer($result);
}
else {
    $result['text']   = "Invalid key";
    $result['reason'] = "Invalid key";
}

$result['runtime'] = microtime(1) - START;



if(isset($_GET['show']) && $_GET['show'] == 'json'){
    header("content-type: application/json");
    echo json_encode($result);
}
else if(isset($_GET['show'], $_GET['jsonp']) && $_GET['show'] == 'jsonp'){
    header("content-type: application/javascript");
    echo "{$_GET['jsonp']}(" . json_encode($result) . ")";
}
else {
    ?>
    <html><body><style>
        body {
            font-family: 'Courier New', serif;
            font-size: 12px;
        }
        h1{
            font-size: 16px;
            font-weight: bold;
            border-bottom: #AAA solid 1px;
            margin: 5px 0 10px 0;
            padding: 5px 10px 5px 10px;
            background: #F5F5F5;
        }

        h2{
            font-size: 14px;
            font-weight: bold;
        }

        pre {
            padding: 5px 0 5px 20px;
            font-family: 'Courier New', serif;
            font-size: 12px;
            border-left: #EEE solid 1px;
            display: block;
        }
    </style>
        <?php echo $result['text'] ?>
    </body></html>
    <?php
}