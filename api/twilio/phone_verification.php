<?php

/** @noinspection PhpIncludeInspection */
require_once $_SERVER['PROJECT_ROOT'] . "/libs/Boot.php";
Boot::installCore();

// выборя языка
$lang = null;
foreach(array_keys(User_Phone_Verification::getCallLanguages()) as $el){
    if(
        is_null($lang) ||
        (isset($_GET['lang']) && $_GET['lang'] == $el)
    ){
        $lang = $el;
    }
}

$code = isset($_GET['code']) ? $_GET['code'] : "";

if($lang == 'ru-RU'){
    $start = "<Say loop=\"1\" language=\"ru-RU\">Здравствуйте ". (isset($_GET['name']) ? $_GET['name'] : "") . ", ваш код: </Say>";
    $finish = "<Say loop=\"1\" language=\"ru-RU>\">Спасибо</Say>";
}
else {
    $start = "<Say loop=\"1\" language=\"en-US\">Hi ". (isset($_GET['name']) ? $_GET['name'] : "") . ", your code: </Say>";
    $finish = "<Say loop=\"1\" language=\"en-US>\">Good bye</Say>";
}

header("content-type: text/xml");
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
?>
<Response>
    <?=$start?>
    <Say loop="6" language="<?=$lang?>"><?=$code?>,</Say>
    <?=$finish?>
</Response>